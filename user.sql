-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost:8889
-- Generation Time: Mar 15, 2022 at 01:05 PM
-- Server version: 5.7.34
-- PHP Version: 7.2.34

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eyda`
--

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `Id` int(10) UNSIGNED NOT NULL,
  `FirstName` varchar(50) NOT NULL DEFAULT '',
  `LastName` varchar(50) NOT NULL DEFAULT '',
  `CompanyId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `Loginname` varchar(20) NOT NULL DEFAULT '',
  `Password` varchar(40) NOT NULL DEFAULT '',
  `HashedPassword` varchar(255) NOT NULL DEFAULT '',
  `RandomKey` varchar(20) NOT NULL DEFAULT '',
  `SessionToken` varchar(250) NOT NULL DEFAULT '',
  `2FA` varchar(250) NOT NULL DEFAULT '',
  `2FAToken` varchar(255) NOT NULL DEFAULT '',
  `Title` varchar(50) NOT NULL DEFAULT '',
  `Active` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `ProjectId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `AdminSystem` tinyint(1) UNSIGNED NOT NULL DEFAULT '0',
  `PhoneBusiness` varchar(20) NOT NULL DEFAULT '',
  `PhoneMobile` varchar(20) NOT NULL DEFAULT '',
  `PhonePrivate` varchar(20) NOT NULL DEFAULT '',
  `Email` varchar(50) NOT NULL DEFAULT '',
  `Address1` varchar(50) NOT NULL DEFAULT '',
  `Address2` varchar(50) NOT NULL DEFAULT '',
  `ZIP` varchar(20) NOT NULL DEFAULT '',
  `City` varchar(50) NOT NULL DEFAULT '',
  `Country` varchar(50) NOT NULL DEFAULT '',
  `PhoneDirect` varchar(20) NOT NULL DEFAULT '',
  `CreateDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ModifyDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `ProjectSelect` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `CreateUserId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `ModifyUserId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `TimeZoneId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `RoleId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `Login` tinyint(1) UNSIGNED NOT NULL DEFAULT '1',
  `LastNewsDate` datetime NOT NULL DEFAULT '0001-01-01 00:00:00',
  `Extern` tinyint(4) DEFAULT '0',
  `LanguageId` int(10) UNSIGNED NOT NULL DEFAULT '1',
  `Restricted` tinyint(1) DEFAULT NULL,
  `CompanyList` text,
  `UserList` text,
  `SalesRef` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `AgentCompanyId` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `AgentManager` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `AvatarId` int(10) UNSIGNED NOT NULL DEFAULT '0'
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`Id`, `FirstName`, `LastName`, `CompanyId`, `Loginname`, `Password`, `HashedPassword`, `RandomKey`, `SessionToken`, `2FA`, `2FAToken`, `Title`, `Active`, `ProjectId`, `AdminSystem`, `PhoneBusiness`, `PhoneMobile`, `PhonePrivate`, `Email`, `Address1`, `Address2`, `ZIP`, `City`, `Country`, `PhoneDirect`, `CreateDate`, `ModifyDate`, `ProjectSelect`, `CreateUserId`, `ModifyUserId`, `TimeZoneId`, `RoleId`, `Login`, `LastNewsDate`, `Extern`, `LanguageId`, `Restricted`, `CompanyList`, `UserList`, `SalesRef`, `AgentCompanyId`, `AgentManager`, `AvatarId`) VALUES
(214, 'PassOn', 'support', 787, 'kch', '$2y$10$z7c88aoPTZwLJX7mgdDzg.g5yi7/I5wa2', '$2y$10$z7c88aoPTZwLJX7mgdDzg.g5yi7/I5wa2COx1jaod6t83mQDgndiq', 'vBDEEBqnMU', '', 'UQNFMHRPPGXIOWBNYRKXFFAKHYS2LXRL6BEJJSSH', '7WTH7QThhYXk0jHrYtNXBA==', 'Support', 1, 0, 1, '+4570702504', '+4528987081', '', 'kc@passon-solutions.com', '', '', '', '', '', '+4528987081', '0001-01-01 12:00:00', '2022-03-14 10:07:32', 0, 0, 214, 339, 82, 1, '2006-03-17 07:13:40', 0, 1, 0, NULL, NULL, 0, 0, 0, 0),
(481, 'PassOn', 'support', 787, 'lsl', '$2y$10$GAgW7VxOLmbS7b416V4s5OYuONFAqj1Rj', '$2y$10$GAgW7VxOLmbS7b416V4s5OYuONFAqj1RjyB03i.EoWcHiah3WmAdq', 'kuqpIAQRi0', '', '0', '', 'Support', 1, 0, 1, '+4570702504', '+4528987082', '', 'lsl@passon-solutions.com', '', '', '7400', 'Herning', 'Denmark', '+4528987082', '2004-09-07 13:36:03', '2016-06-07 07:06:31', 0, 1, 530, 339, 82, 1, '2006-11-26 11:39:33', 0, 1, 0, NULL, NULL, 0, 0, 0, 0),
(530, 'Andreas', 'Birkebæk', 787, 'ab', '$2y$10$iRuPvTRdX1IyCzn3bDeDD.DG9ryQLYjoq', '$2y$10$iRuPvTRdX1IyCzn3bDeDD.DG9ryQLYjoqs1Qq8NHfKsmLh146m/vO', '3ydjKq6EBv', '', '0', '', 'Comm Director', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '2013-11-11 21:15:38', '2021-05-21 08:28:18', 0, 214, 530, 339, 124, 1, '2006-06-20 08:43:29', 0, 1, 0, NULL, NULL, 0, 0, 0, 0),
(2125, 'Stock', 'User', 787, 'sto', '$2y$10$PS19klO5MCBgS/aLv7yILuiRg49eDnEsf', '$2y$10$PS19klO5MCBgS/aLv7yILuiRg49eDnEsfe3D4ebeFzQKzLpEpx7/6', 'EZ9CivbbDV', '', '0', '', 'Warehouse', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '2014-07-30 13:23:38', '2014-08-05 09:24:02', 0, 481, 481, 339, 129, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2288, 'test', 'testensen', 787, 'tt', '$2y$10$u3Fyvsomq3/fMp5LrvyIne1nAsHheKFhY', '$2y$10$u3Fyvsomq3/fMp5LrvyIne1nAsHheKFhY/DGsaycnsGxj3YHdeuNC', 'jNHRlQk23q', '', '0', '', 'test', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '2016-06-03 11:04:43', '2016-06-07 07:50:06', 0, 214, 530, 339, 91, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(531, 'Casper', 'Jeppesen', 787, 'cj', '$2y$10$2hV6MPBzWtBEEMKI1HvY0eNurgNHXTu5z', '$2y$10$2hV6MPBzWtBEEMKI1HvY0eNurgNHXTu5zV7pf8ZTIQsRTN7DM75n.', 'svm7Azn35W', '', '0', '', 'CEO', 1, 0, 0, '', '', '', '', '', '', '', '', '', '', '2013-11-11 21:15:38', '2021-09-12 19:12:26', 0, 214, 530, 339, 124, 1, '2006-06-20 08:43:29', 0, 1, 0, NULL, NULL, 0, 0, 0, 0),
(2548, 'Frederik', 'Jeppesen', 787, 'fj', '$2y$10$4GFDl.chNi7aH0ecg24epeFUL35b8eFa2', '$2y$10$4GFDl.chNi7aH0ecg24epeFUL35b8eFa2EaETKPIfWp6eWeAUicG2', 'v5SQdUiCXf', '', '0', '', '', 1, 0, 0, '', '', '', 'frederik@eyda.dk', '', '', '', '', '', '', '2021-03-09 15:20:51', '2021-03-10 13:31:46', 0, 531, 531, 339, 124, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2549, 'Annemette', 'Poulsen', 787, 'am', '$2y$10$W8gCOqIf5L0vPTzLRV/hwOwlJGHcSDGVW', '$2y$10$W8gCOqIf5L0vPTzLRV/hwOwlJGHcSDGVWTKGH4WyJD8NvKdRSe7fy', 'RP9JCcg9bo', '', '0', '', '', 1, 0, 0, '', '', '', 'annemette@eyda.dk', '', '', '', '', '', '', '2021-03-24 09:39:54', '2021-07-09 09:13:04', 0, 2548, 530, 339, 124, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2550, 'Maria', 'Nielsen', 787, 'ml', '$2y$10$J5hrcB4QMp.mN5KnVV.xzOJ1OSFQqxzlk', '$2y$10$J5hrcB4QMp.mN5KnVV.xzOJ1OSFQqxzlkv8HasJNCQ4aYSmUwxya6', '98XnafL6u7', '', '0', '', '', 1, 0, 0, '', '', '', 'maria@eyda.dk', '', '', '', '', '', '', '2021-03-30 11:21:13', '2021-03-30 11:26:16', 0, 2548, 2548, 339, 101, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2551, 'Karen', 'Nielsen', 787, 'kn', '$2y$10$BSeNoDRoi7cEisMUosfsfOPMNTLXber22', '$2y$10$BSeNoDRoi7cEisMUosfsfOPMNTLXber22dEWK1vBwsZYs8cFIuhCC', '3pCQi9fpxp', '', '0', '', 'Online Marketing Specialist', 1, 0, 0, '', '', '', 'karen@eyda.dk', '', '', '', '', '', '', '2021-05-21 08:33:21', '2021-05-26 13:11:05', 0, 530, 2551, 339, 124, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2552, 'Mette', 'Merrild', 787, 'mmj', '$2y$10$C1gJLBUOsmpoi4sYtg17rOWGLyzVhoaCy', '$2y$10$C1gJLBUOsmpoi4sYtg17rOWGLyzVhoaCy0sFBlDy9WLvB9qhhPrIy', 'thEkR4Ah07', '', '', '', 'Designer', 1, 0, 0, '', '', '', 'mmj@eyda.dk', '', '', '', '', '', '', '2021-05-26 08:06:51', '2021-05-26 08:06:51', 0, 531, 531, 339, 101, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2553, 'Anna Spanner', 'Andersen', 787, 'asa', '$2y$10$8aBGJjt.BHggQeuum/052OwfK9Zh33/Iv', '$2y$10$8aBGJjt.BHggQeuum/052OwfK9Zh33/IvTYfh4g4Aw7Rry/q/oPzq', '1Jougi4MSK', '', '0', '', 'Customer Service', 1, 0, 0, '', '', '', 'asa@eyda.dk', '', '', '', '', '', '', '2021-05-27 11:40:23', '2021-06-03 08:23:46', 0, 530, 2553, 339, 101, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2554, 'Mickael', 'Rhetiér', 787, 'mrh', '$2y$10$IjyakMSuZkR0qaF/nGMGf.eqwKMiGTJl4', '$2y$10$IjyakMSuZkR0qaF/nGMGf.eqwKMiGTJl43pnsQkDHFpE.WErqP7zS', 'Kel4SZQPEz', '', '0', '', 'E-commerce Specialist', 0, 0, 0, '', '', '', '', '', '', '', '', '', '', '2021-06-03 08:41:19', '2021-07-06 07:22:16', 0, 530, 530, 339, 124, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(1, 'System automation', '', 787, 'Sys', '$2y$10$VxonF8OZlf/Qgv/hVFFZfeINROtetHzZb', '$2y$10$VxonF8OZlf/Qgv/hVFFZfeINROtetHzZb7cQZhYM.t6SxzVJqQKKe', '35PrXp4Kgk', '', '0', '', 'Support', 1, 0, 1, '', '', '', '', '', '', '', '', '', '+4528987081', '0001-01-01 12:00:00', '2016-06-07 07:06:24', 0, 0, 530, 339, 82, 1, '2006-03-17 07:13:40', 0, 1, 0, NULL, NULL, 0, 0, 0, 0),
(2555, 'Maiken', 'Madsen', 787, 'maiken', '$2y$10$Q2ADMTs4EC6z4sil14mp3eFt/jdL/wMoE', '$2y$10$Q2ADMTs4EC6z4sil14mp3eFt/jdL/wMoEy50CjzAeXlxLgGWivE4W', 'fPDukumP5N', '', '0', '', '', 1, 0, 0, '', '', '', 'maiken@logistix.dk', '', '', '', '', '', '', '2021-06-11 10:00:29', '2021-06-11 10:00:29', 0, 530, 530, 339, 129, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2556, 'Tidde Agerholm', 'Møller', 787, 'tam', '$2y$10$I6.OrBfeuh9fH4b5IZZ0deTN/T0UPhdI9', '$2y$10$I6.OrBfeuh9fH4b5IZZ0deTN/T0UPhdI9mOQs75CsKHYBheMqowXe', 'BJ4mHIYGyg', '', '0', '', 'Customer Service', 1, 0, 0, '', '', '', 'tam@eyda.dk', '', '', '', '', '', '', '2021-06-18 11:00:04', '2021-06-23 07:43:10', 0, 530, 530, 339, 124, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2557, 'Fie', 'Lykkeberg', 787, 'fly', '$2y$10$Bh/i9d/o04w.dSME00fYNuYmo0fpf76EK', '$2y$10$Bh/i9d/o04w.dSME00fYNuYmo0fpf76EKueehvPoBs65nYGBMQfO6', 'SGcmjeJUQx', '', '0', '', '', 1, 0, 0, '', '', '', 'fly@eyda.dk', '', '', '', '', '', '', '2021-08-23 11:33:26', '2021-08-23 11:40:25', 0, 530, 2557, 339, 124, 1, '0001-01-01 00:00:00', 0, 1, NULL, NULL, NULL, 0, 0, 0, 0),
(2559, 'PassOn', 'support', 787, 'tom', '$2y$10$3.7ppZBUnti.N36hSRjjmeW//BULko9x8', '$2y$10$3.7ppZBUnti.N36hSRjjmeW//BULko9x8F6utwgwvmMRJvcV4syXW', '81NzZ4rggN', '', '0', '', 'Support', 1, 0, 1, '+4570702504', '+4528987081', '', 'kc@passon-solutions.com', '', '', '', '', '', '+4528987081', '0001-01-01 12:00:00', '2016-06-07 07:06:24', 0, 0, 530, 339, 82, 1, '2006-03-17 07:13:40', 0, 1, 0, NULL, NULL, 0, 0, 0, 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Loginname` (`Loginname`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `Id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2576;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
