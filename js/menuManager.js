    function getIndexCookie (cookieName) {
    	var cookieVal = $.cookie(cookieName);
    	return cookieVal ? parseInt(cookieVal) : 0;
    };
    
var menuRenderer = {
   levels : [
             'div.subNavigation div.level-2',   
             'div.subNavigation div.level-1',            
             'div.mainNav div.level-2',
             'div.mainNav div.level-1'
         ],
            
    render : function(menuArray, menuClass, isSticked) {
        var menuString = "";

        $.each(menuArray, function(i, comp) {
            var clickHandlerString = comp[6] ? comp[6][0]  : '';
            var id                 = comp[6] ? comp[6][1] : null;
            var subMenu            = menuRenderer.renderSubMenu(menuArray[i]);
            var menuClass          = "";
            if (subMenu.length > 0) {
               menuClass = 'hasSubMenu';
            } else if ( clickHandlerString.length == 0 ) {
               menuClass = 'disabled';
            }
            
            menuString += "<li nid=" + comp[1] + " parentId=" + comp[2] + " showCart=" + comp[3] + " handler=\""+ clickHandlerString + "\" recordId=" + id + " class=\"" + menuClass + "\" >"
            	+ "<a href='#' onclick='return false;'>" + comp[0] + "</a>" 
            	+ subMenu 
            	+ "</li>";           
        });
        $('div.' + menuClass + ' div.level-1 ul').append(menuString);
        
        /*
         * Hide level2 if there are no level1 menu. (useful for popups)
         */
        if (menuString.length === 0 && !isSticked) {
           $('div.' + menuClass + ' div.level-2-bg').hide();   
           $('div.' + menuClass).parent('div.header').height('auto'); 
        }
        
        menuRenderer.initListeners(menuClass, isSticked);
    },

    renderSubMenu : function(menuArray) {
    	var result = "",
        	subMenuString = "";

        $.each(menuArray, function(i, comp) {
            if(i < 6 || (i == 6 && !comp)) return true;
            if(i == 6 && comp) {
                return false;
            }
            var clickHandlerString = comp[6] ? comp[6][0] : '';  
            var id                 = comp[6] ? comp[6][1] : null;  
            subMenuString += "<li nid=" + comp[1] + " parentId=" + comp[2] + " showCart=" + comp[3] + (comp[4] == 1 ? " isDefault=1" : "") + " LayoutPopup=" + comp[5] + " handler=\""+ clickHandlerString + "\" recordId=" + id + ">"
            	+ "<a href='#' onclick='return false;'>" + comp[0] + "</a></li>";
        });
        if (subMenuString.length > 0) {
        	result = '<div class="level-2"><ul>' + subMenuString + '</ul></div>';
        }
        return result;
    },

    initListeners : function(menuClass, isSticked) {    	
       $('div.' + menuClass + ' ul > li')
    	   .click(function(event) {
    	      if ($(this).hasClass('disabled')) {
    	         return;
    	      }
            var className  = $(this).closest('div').attr('class');
            var level      = parseInt(className.substr(className.length - 1)); 
            /*
             * show level 2
             */
            if (level ==  1) {
               var hasSubMenu = $('div.level-2 > ul > li', this).length > 0;
               if (!isSticked) {
                  if (hasSubMenu) {
                     $('div.' + menuClass + ' div.level-2-bg').show();
                  } else {
                     $('div.' + menuClass + ' div.level-2-bg').hide();
                  }
               } 
               $('div.' + menuClass + ' div.level-2').hide();  
               $('div.level-2', this).show();                  
    	      }
            /*
             * add activePage class
             */            
            $('div.' + menuClass + ' ul > li').removeClass('activePage');
            if (level > 1) {
               $(this).closest('div').parent().addClass('activePage');
            }
            $(this).addClass('activePage'); 
            
            
            /*
             * save activePage in cookies for future
             */
            var name = "div." + menuClass + " div."+className;  
            menuRenderer.saveActive(name, $(this).attr('nid'));
                        
            /*
             * search for default and
             * execute handler
             */
            var result     = false,
                hasDefault = false,
                element    = $(this),
                handler    = $(this).attr('handler');
 
            if ((handler.length == 0)) {
               var def = $("div.level-2 > ul > li[isdefault]", this)[0];
               if ($(def).length == 0 && (menuClass == "mainNav")) {
                  def = $("div.level-2 > ul > li[layoutpopup=0]:first", this);
               }
               if ($(def).length > 0) {
                  hasDefault = true;
                  element = $(def);
                  handler = $(def).attr("handler");
               }
            }
            var 
               id       = $(element).attr('recordId'),
               nid      = $(element).attr('nid'),
               parentId = $(element).attr('parentId'),
               header   = $(element).attr('header') || $(element).children('a').text(); 
            
          
            if (handler.length > 0) {
               result = appClick(event, handler, id);
               
               //do not update text if popup was opened
               if(window.hasPopup) {
            	   delete window.hasPopup;
               } else {
            	   $(".leftCaption").text(header);
               }
               
               if (header.toLowerCase() != "back") {
            	   menuRenderer.setActive(nid);
               }
            } else {
               if (menuClass == "mainNav") {
                  $(".leftCaption").text(header);
                  appLoadLegacy(true, nid);
               }
            }
            event.stopPropagation();
            return result;
    	   })
 	    ;
    },
    setEnable : function() {
       for ( var i=0; i<menuRenderer.levels.length; i++ ) {
          $(menuRenderer.levels[i] + ' > ul > li').removeClass('disabled');
       }
    },
    
    setDisable : function(nid) {
       var id  = nid;       
       for ( var i=0; i<menuRenderer.levels.length; i++ ) {
          var item = $(menuRenderer.levels[i] + ' li[nid='+nid+']');
          if ($(item).length > 0) {
             $(item).addClass("disabled");
          }
       }
    },
    
    setActive : function(nid) {
       var id     = nid;       

    	var hasNid = false;
    	
    	var activate = function(menuClass, nid) {
    	  var item = $(menuClass + ' li[nid='+nid+']');
        if (item.length > 0) {
           $(menuClass + '-bg').show();
           $(menuClass + '> ul > li').removeClass('activePage');
           $(menuClass).hide();
           $(item).closest('div').show();
           $(item).addClass('activePage');
           menuRenderer.saveActive(menuClass, nid);
           return item;
        } else {
           return null;
        }   	   
    	};
    	
    	for ( var i=0; i<menuRenderer.levels.length; i++ ) {
    	   var item = activate(menuRenderer.levels[i], id);
    	   if (item != null) {
    	     hasNid = true;
    	     id = $(item).attr('ParentId').match(/\d+/);
    	   }
    	}
    	
    	if (!hasNid && (id > 1)) {
         var activeMenus = $.cookie('menu');
         if (activeMenus == null ||activeMenus.length == 0) {
            return;
         }    	   
         for ( var i=0; i<menuRenderer.levels.length; i++ ) {
            id = hasNid ? id : activeMenus[menuRenderer.levels[i]];
            if (id > 0 ) {
               var item = activate(menuRenderer.levels[i], id);
               if (item != null) {
                  hasNid = true;
                  id = $(item).attr('ParentId').match(/\d+/);                  
               }
            }
         }
    	}
    	
    	//set header
    	for ( var i=0; i<menuRenderer.levels.length; i++ ) {
    	   var active = $(menuRenderer.levels[i] + ' li[nid='+nid+']');
    	   if ($(active).length > 0) {
    	      var header   = $(active).attr('header') || $(active).children('a').text();
    	      $(".leftCaption").text(header);  
    	      break;
    	   }    	   
    	}    	
    },
    
    saveActive : function(menuClass, nid) {
       var activeMenus = $.cookie('menu');
       if (activeMenus == null) {
          activeMenus = {};
       }
       activeMenus[menuClass] = parseInt(nid);      
       $.cookie('menu', activeMenus);       
    }
    
};
