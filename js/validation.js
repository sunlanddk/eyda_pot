
function validateElem(el, e, validator) {
	var event = e || window.event;
	var eventTarget = event.target || event.srcElement;
	var startCurPos = el.selectionStart;
	var endCurPos = el.selectionEnd;
	
	var keyCode = event.charCode || event.keyCode || event.which;
	//allow Enter, Backspace, Tab, Delete, Arrow Left, Arrow Right, Ctrl+?
	if(keyCode == 13 || keyCode == 8 || keyCode == 9 || (event.keyCode == 46 && window.getBrowserInfo().name == "Firefox") || keyCode == 37 || keyCode == 39 || event.ctrlKey) {
		return true;
	}
	var character = String.fromCharCode(keyCode);
	var str=el.value;

	if (eventTarget == el) {
		str = str.substring(0,startCurPos)+character+str.substring(endCurPos,str.length);
	}
	
	var result = false;
	if (validator) result = validator(str);
	if(!result && 'cancelable' in event) event.preventDefault();
	else event.returnValue = result;
}

function validatePasteElem(obj, validator, e){
	var event = e || window.event;
	var startCurPos = obj.selectionStart;
	var endCurPos = obj.selectionEnd;
	
	var str=obj.value;
	if(window.clipboardData && window.clipboardData.getData){
		var data = clipboardData.getData("text");
		if (document.selection.createRange().parentElement() == obj)
			str = str.substring(0,startCurPos)+data+str.substring(endCurPos,str.length);
	} else {
		if(!validatePasteElem.flag) {
			validatePasteElem.flag = true;
			obj.prevValue = str; //saves prev value
			setTimeout(function(){
				validatePasteElem(obj, validator, e);
			}, 50);
			return;
		}
		validatePasteElem.flag = false;
	}
	
	var result = false;
	if (validator) result = validator(str);
	if(!result && obj.prevValue!=null){
		// restores prev value
		obj.value = obj.prevValue;
		obj.prevValue = null;
	}
	if(!result && 'cancelable' in event) event.preventDefault();
	else event.returnValue = result;
	
	return result;
}

function isInteger(expr) {
	var reg = /^\*?\d*\*?$/;

	return reg.test(expr);
}

function isFloat(expr) {
	var reg = /^\*?([0-9]+(\.|\,)?)?([0-9][0-9]?)?$/;
	return reg.test(expr);
}

function isPercentage(expr) {
   var int = parseInt(expr);
   return isInteger(expr) && int >= 0 && int <=100;
}

function maskElement(el, maskFunction){
	$(el).bind("keypress", function(e){validateElem(this, e, maskFunction);});
	$(el).bind("paste", function(e){validatePasteElem(this, maskFunction, e);});
	
}
