

function disableNavigationBtns(btns){
	var nbtns = [];
	for (var i=0;i<btns.length;i++){
		var name = btns[i][1];
		if(name!=="First" && name!=="Previous" && name!=="Next" && name!=="Last"){
			nbtns.push(btns[i]);
		}
	}
	return nbtns;
}