<?php 


	require_once 'C:\inetpub\wwwroot\eyda/lib/config.inc';
	require_once _LEGACY_LIB . '/lib/db.inc';
	require_once _LEGACY_LIB . '/lib/table.inc';


	for($_i = 1; $_i < 220; $_i++) {

		$_querystr = "SELECT count(Shipments) AS Shipments, sum(ShipmentPcs) AS ShippedPcs FROM (
							SELECT COUNT(s.Id) AS Shipments, sum(pl.PackedQuantity) as ShipmentPcs 
  							FROM (stock s, pickorder p, pickorderline pl) 
  					 			    WHERE s.DepartedDate > '%s 00:00:00' 
				 					AND s.DepartedDate < '%s 23:59:59' 
     			 	   			    AND s.Type !='fixed' 
    			 				    AND s.active=1
    			 				    AND s.Id=p.toId
    			 				    AND p.active=1
    			 				    AND pl.pickorderId=p.Id
    			 				    AND pl.active=1
    			 				    AND p.fromid=14321
    			 				    AND p.ownercompanyid=787
    			 				    AND p.handlercompanyid=787
				 					GROUP BY s.Id) dayupdate;" ;


		$_datefilter = date("Y-m-d", strtotime("-" . $_i . " day")) ;
	//	$_datefilter = '"2020-12-22"' ;
		$_query =  sprintf($_querystr, $_datefilter, $_datefilter);
		$_result = dbQuery($_query);
		$_row=dbFetch($_result);
		$_row['producedDate']=date("Y-m-d", strtotime("-" . $_i . " day"));
		tableWrite('kpiday', $_row); 
		dbQueryFree($_result) ;

		}

?>