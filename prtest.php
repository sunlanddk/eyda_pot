<?php

$prhdl = printer_open('CAB M4 203DPI');
$doc = "J\nH 100\nO R\nS l1;0,0,68,70,100\nT 10,10,0,5,pt20;sample\nB 10,20,0,EAN-13,SC2;401234512345\nG 8,4,0;R:30,9,0.3,0.3\nA 1\n";
print printer_get_option($prhdl, PRINTER_MODE);
printer_set_option($prhdl, PRINTER_MODE, 'RAW');
printer_write($prhdl, $doc);

printer_close($prhdl);

?>