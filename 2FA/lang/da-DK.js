export default {
  "dashboard": "Dashboard",
  "orders": "Ordre",
  "items": "Produkter",
  "positions": "Positioner",
  "login": "Log ind",
  "positiontype": "Position type",
  "gtins": "Gtins",
  "position": "Position",
  "name": "Navn",
  "update": "Opdater",
  "edit": "Ændre",
  "user": "Bruger"
}