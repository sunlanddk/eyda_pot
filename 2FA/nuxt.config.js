export default {
  // target: 'static',
  // Disable server-side rendering: https://go.nuxtjs.dev/ssr-mode
  ssr: false,
  server: {
    port: 4500,
    host: 'lokalpot.dk'
  },
  mode: 'spa',

  loading: '~/components/Loader.vue',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Eyda - WMS',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' },

    ], 
    script: [
      {
        src: "/eyda/login/script/jquery-3.3.1.slim.min.js",
        type: "text/javascript"
      },
      {
        src:
          "/eyda/login/script/popper.js",
        type: "text/javascript"
      },
      {
        src:
          "/eyda/login/script/bootstrap.min.js",
        type: "text/javascript"
      },
      {
        src:
          "/eyda/login/BarcodeParser.js",
        type: "text/javascript"
      }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
      '@/styles/fontawesome.min.css',
      '@/styles/_style.scss',

  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '@/plugins/bootstrap-vue',
    { src: '~/plugins/persistedState.client.js' },
    "~/plugins/vee-validate.js"
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/eslint
    //'@nuxtjs/eslint-module'
  ],

  router: {
    // middleware: ['isadmin','isloggedin']
    base: '/eyda/login'
  },

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    '@nuxtjs/i18n',
  ],

  i18n: {
    strategy: 'prefix_except_default',
    locales: [
      { code: 'en', iso: 'en-US', file: 'en-US.js' }
      // { code: 'da', iso: 'da-DK', file: 'da-DK.js' }
    ],
    langDir: 'lang/',
    defaultLocale: 'en',
    vueI18n: {
      fallbackLocale: 'en',
    }
  },

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {
    baseURL: 'https://eyda.passon.dk/eyda/s4/public/'
    // baseURL: 'https://eyda.passon.dk/eyda/s4/public/'
    // baseURL: 'https://tradeexact-wmsserver.gs1.dk/api/public'
  },

  publicRuntimeConfig: {
    VERSION_NO: '1.2.0'
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    transpile: ["vee-validate/dist/rules"],
  },

  generate: {
    dir: 'login'
  }

  // generate: {
  //   routes() {
  //     let self = this;
  //     return axios.get(
  //         'http://wms7.gs1.dk/public/user/get/all', { 
  //           headers: { 'Passonauth': 'iamadmin' } 
  //         }
  //       ).then(res => {
  //       return res.data.data.map(order => {
  //         return '/orders/' + order.id
  //       })
  //     })
  //   }
  // }
}
