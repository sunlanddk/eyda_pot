export default {
	data() {
		return {
		}
	},
	computed: {
	},
	methods: {
		test(name, birth) {
			// return 'Hello ' + name + ', born: ' + birth;
		},
		tony: function (text) {
        	// console.log("It's a me, " + text);
    	},
    	addErrorsToList(errortext, type){
    		let self = this;
    		let idUnique = Math.floor(Math.random() * 10000);

    		self.$store.commit('errors/addError', {id: idUnique, text: errortext, errortype: type});
    		let myVar = setTimeout(function(){ 
		    	self.$store.commit('errors/removeError', idUnique);
		      	clearTimeout(myVar)
		    }, 10000);
    	},
    	pad(num, size) {
    		var s = "00000000000000" + num;
    		return s.substr(s.length-size);
		},
    	testFunction(){
    		this.$axios.$get(
		        	'/test',
		        )
		        .catch(error => {
		        	// console.log(error);
    			})
		        .then((response) => {
		        	// console.log(response);
		        });
    	},
    	returnData(data){
    		return data; 
    	},
    	postData(url, data, callback){
    		let self = this;
    		self.$axios.post(
		        	url, 
		        	data,
		        	{ 
		            //   headers: { 'Passonauth': self.$store.state.user.authToken } 
		            }
		        ).then((response) => {
		        	if(response.status === 200){						
						if(response.data.error == false){
		        			callback(response.data.data, false);
		        		}
		      			else{
		      				callback(response.data.errormessage, true);	
		            		// console.log('error 2');
		        			// console.log(response);
		            	}
		        	}
		        	else{
		        		// console.log('error 3');
		        		// console.log(response);
						callback(response.data.errormessage, true);	
		        	}
		        }).catch(error => {
					// console.log("why")
					// console.log(error.response);
					
					if(error.response.status === 403){
						self.$store.commit("loader/Stop")
					}
					else{
						self.$store.commit("loader/Stop")
					}
    			});
    	},
		deleteData(url, data, callback){
    		let self = this;

    		self.$axios.delete(
		        	url, 
		        	{
						headers: { 'Passonauth': self.$store.state.user.authToken },
						data
					}
		        ).then((response) => {
					self.$store.commit("user/hideCriticalErrorWindow")
		        	if(response.status === 200){
						if(response.data.error == false){
		        			callback(response.data.data, false);
		        		}
		      			else{
		      				callback(response.data.errormessage, true);	
		            		// console.log('error 2');
		        			// console.log(response);
		            	}
		        	}
		        	else{
		        		// console.log('error 3');
		        		// console.log(response);
						callback(response.data.errormessage, true);	
		        	}
		        }).catch(error => {
		        	if (typeof error.response === 'undefined') {
		        		callback('The server stopped responding, please try again.', true);
		        	}
					if(error.response.status === 403){
						self.$store.commit("loader/Stop")
						self.$store.commit("user/logOut")
						self.$store.commit("user/showWindow")	
					}else {
						self.$store.commit("loader/Stop")
						self.$store.commit("user/showCriticalErrorWindow")
					}
    			});
		        // return;
    	},
	    getData(url, callback){
	    	let self = this;
			self.$axios.get(
	        	url, 
	        	{ 	
	            //   headers: { 'Passonauth': self.$store.state.user.authToken }
	            },
	        ).then((response) => {
	        	if(response.status === 200){

					if(response.data.error === false){
						callback(response.data.data, false);
					}
	      			else{
	      				callback(response.data.errormessage, true);	
	      				// console.log('error 1');
	      				// console.log(response);
	            	}
	        	}
	        	else{
	        		// console.log('error 2');
	        		callback(response.data.errormessage, true);
	        	}
	        }).catch(error => {
	        	if (typeof error.response === 'undefined') {
	        		callback('The server stopped responding, please try again.', true);
	        	}

				if(error.response.status === 403){
					self.$store.commit("loader/Stop")
				}else {
					self.$store.commit("loader/Stop")
				}
			})
		},
		async waitGetData(url, callback){
	    	let self = this;
			await self.$axios.get(
	        	url, 
	        	{ 
	              headers: { 'Passonauth': self.$store.state.user.authToken } 
	            }
	        )
	        .catch(error => {
	        	// do some error handling here
	        	// console.log('error');
	        	// console.log(error);
	        	return false;
			})
	        .then((response) => {
	        	if(response.status === 200){
					if(response.data.error === false){
						// console.log("fine")
						callback(response.data.data, false);
					}
	      			else{
	      				// console.log('error 1');
	      				// console.log(response);
						callback(response.data.errormessage, true);	
	            	}
	        	}
	        	else{
	        		// console.log('error 2');
	        		// console.log(response);
					callback(response.data.errormessage, true);	
	        	}
	        });

			return;
		},
		parseEan128(str){
			let barcodeObj;
			let barcode = '';
			try {
			    barcodeObj = parseBarcode(str);
			} catch (e) {
				return 'no valid AI';
			}

			barcodeObj.parsedCodeItems.forEach(function(e, i){
				if((e.ai == '01' || e.ai == '02') && barcode == ''){
					barcode = e.data;
				}
			});
			return barcode;
		}
	}
}
