
export const state = () => ({
  // authToken: 'uOiupApttEoVXDszFDzwg9P+JN2g/xf5XwMlmA6BB2g=',
  active: false,
  errorlist: []
})

export const mutations = {
  Start (state) {
    state.active = true;
  },
  Stop (state) {
    state.active = false;
  },
  addError (state, error) {
    state.errorlist.push(error);
  },
  removeAllErrors(state){
    state.errorlist = [];
  },
  removeError(state, id){
    let index = 0;
    state.errorlist.forEach(function(e,i){
      if(e.id == id){
        state.errorlist.splice(i, 1);
      }
    });
    
  }
}