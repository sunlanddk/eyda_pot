
export const state = () => ({
  // authToken: 'uOiupApttEoVXDszFDzwg9P+JN2g/xf5XwMlmA6BB2g=',
  active: '',
})

export const mutations = {
  Start (state) {
    state.active = true;
  },
  Stop (state) {
    state.active = false;
  },
}