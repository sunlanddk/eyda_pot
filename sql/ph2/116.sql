SELECT Id into @viewCart_id from navigation where ParentId = 	0 AND Mark = 'sales.ordercart.view';
SELECT Id into @navtype FROM `navigationtype` WHERE (Mark = 'ajax');


DELETE FROM `navigation` WHERE Mark = 'sales.ordercart.changecurrency';
DELETE FROM `navigation` WHERE Mark = 'sales.ordercart.changecurrencylink';

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Operation`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES
	('Change Currency',	@viewCart_id, 3, '', '', 'changeCurrency()', 't', 'r', '', 38, 17, 'sales.ordercart.changecurrencylink', 1, 1, 0),
	('changeCurrency',	0, 0, 'sales/ordercart', '', 'change_currency', 'l', 'c', '', 38, @navtype, 'sales.ordercart.changecurrency', 1, 1, 0);
	
