SELECT Id into @sales_id from navigation   where Mark = 'sales.new';

DELETE FROM `navigation` WHERE Mark = 'sales.file_upload';

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
   ('File Upload', @sales_id, 7, 'sales/fileupload', '', 'main', 'm', 'File Upload', 21, 1, 'sales.fileupload', 1, 1, 0)