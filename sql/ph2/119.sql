SELECT Id into @shop_id from navigation   where Mark = 'sales.order.create';	
SELECT Id into @navtype_ajax from `navigationtype` where Mark = 'ajax';


DELETE FROM `navigation` WHERE Mark = 'sales.shop.top_countries';

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES
	('Top 5 Selling Country', @shop_id, 4, 'sales/shop', '', 'get_top_countries', 't', 'Top 5 Selling Country', 21, @navtype_ajax, 'sales.shop.top_countries', 1, 1, 0);