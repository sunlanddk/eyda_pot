SELECT SUM(o.ProductionMinutes * q.Quantity) AS OutMinutes
from Production p INNER JOIN  StyleOperation o ON p.StyleId=o.StyleID
INNER JOIN OutSourcingPlan s ON s.ProductionId = p.Id 
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
INNER JOIN ProductionQuantity q ON q.ProductionId=p.Id
	AND (q.ArticleColorId = s.ArticleColorId OR s.ArticleColorId is null)
	AND (q.ArticleSizeId = s.ArticleSizeId OR s.ArticleSizeId is null)  
WHERE p.Id=1802;

SELECT coalesce(SUM(o.ProductionMinutes * b.Quantity), 0) AS InMinutes
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN StyleOperation o ON o.StyleId = p.StyleId
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
WHERE p.Id=1802 AND s.InDate is not null;

SELECT coalesce(SUM(o.ProductionMinutes * b.Quantity), 0) AS OutMinutes
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN StyleOperation o ON o.StyleId = p.StyleId
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
WHERE p.Id=1802 AND s.InDate is null;

SELECT p.Id, COUNT(c.ActualDate) AS DayCount, YEARWEEK(c.ActualDate) 
AS WeekNo
FROM Production p INNER JOIN (SELECT DISTINCT ActualDate FROM Capacity) c
ON p.ProductionStartDate <= c.ActualDate AND p.ProductionEndDate>=c.ActualDate
AND YEARWEEK(c.ActualDate) > YEARWEEK('2005-07-01')  
where p.Id=821
GROUP BY YEARWEEK(c.ActualDate); 