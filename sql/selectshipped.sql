SELECT s.Id, s.Name,i.ArticleSizeId, i.ArticleColorId, SUM(i.Quantity) AS Quantity
FROM Item i INNER JOIN Container c ON i.ContainerId = c.Id
INNER JOIN Stock s ON s.Id = c.StockId
INNER JOIN OrderLine o ON i.OrderLineId = o.Id
WHERE s.Type = 'shipment'
AND o.OrderId = 38
GROUP BY s.Id, s.Name, i.ArticleSizeId, i.ArticleColorId
ORDER BY i.ArticleColorId, i.ArticleSizeId, s.Id;


