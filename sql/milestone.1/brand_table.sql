DROP TABLE IF EXISTS `brand`;

CREATE TABLE `brand` (
	`Id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
	`Name` VARCHAR(100) NOT NULL DEFAULT '',
	`OwnerId` INT(10) UNSIGNED NULL,
	`Active` TINYINT(1) UNSIGNED NOT NULL DEFAULT '1',
	`CreateDate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`CreateUserId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	`ModifyDate` DATETIME NOT NULL DEFAULT '0000-00-00 00:00:00',
	`ModifyUserId` INT(10) UNSIGNED NOT NULL DEFAULT '0',
	PRIMARY KEY (`Id`)
)
COLLATE='latin1_swedish_ci'
ENGINE=MyISAM;

SELECT Id into @owner_id from `user` where loginname = 'kch';

insert into `brand` (`Name`, `OwnerId`)
values 	('Freds World', @owner_id),
			('New Brand', @owner_id),
			('Test Brand', @owner_id);
/*
SELECT EXISTS ( 
	SELECT * 
	FROM information_schema.columns 
	WHERE table_name = 'collection' AND column_name = 'BrandId' -- AND table_schema = DATABASE() 
) THEN
    ALTER TABLE `collection` DROP COLUMN `BrandId`;
END IF;
*/
ALTER TABLE `collection` DROP COLUMN `BrandId`;
ALTER TABLE `collection` 
	ADD COLUMN `BrandId` TINYINT(1) ZEROFILL NOT NULL DEFAULT '0';
	
	
SELECT Id into @brand_id from brand order by Id limit 1;
UPDATE `collection` SET `BrandId` = @brand_id;