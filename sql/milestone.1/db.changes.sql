ALTER TABLE `navigation`
	CHANGE COLUMN `Mark` `Mark` VARCHAR(40) NOT NULL DEFAULT '' AFTER `PermInternal`,
	DROP COLUMN `ShowCart`,
	ADD COLUMN `ShowCart` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0',	
	DROP COLUMN `isDefault`,
	ADD COLUMN `isDefault` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';		

ALTER TABLE `order`
	DROP COLUMN `Proposal`;	
ALTER TABLE `order` 
	ADD COLUMN `Proposal` TINYINT(1) ZEROFILL NOT NULL DEFAULT '0' AFTER `WebShopNo`;
	
ALTER TABLE `articlecategory` DROP COLUMN `TopBottom`;	
ALTER TABLE `articlecategory` ADD COLUMN `TopBottom` ENUM ('Top', 'Bottom') DEFAULT NULL;

ALTER TABLE `collectionmember` DROP COLUMN `Focus`;	
ALTER TABLE `collectionmember` ADD COLUMN `Focus` TINYINT(1) UNSIGNED NOT NULL DEFAULT '0';


update navigation  set isDefault = 1 where mark = "orderlist";
INSERT INTO `maillayout` 
	(`ModifyUserId`, `Mark`, `Subject`, `Header`, `Body`) 
VALUES 
	(1, 'ordercreated',      'Order Created',       'Order Created',       '{$param[\'body\']}'),
   (1, 'claimcreated',      'Claim Created',       'Claim Created',       '{$param[\'body\']}'),	
	(1, 'creditnotecreated', 'Credit Note Created', 'Credit Note Created', '{$param[\'body\']}');
	
ALTER TABLE `company` 
	DROP INDEX `FK_COMPANY_SalesUserId`;
ALTER TABLE `company` 
	ADD INDEX `FK_COMPANY_SalesUserId` (`SalesUserId`);