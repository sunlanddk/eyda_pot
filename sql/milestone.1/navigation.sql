#DECLARE @menu_name VARCHAR(25);
SET @menu_name = 'Sales';

SELECT Id into @menu_old FROM navigation where `Mark` = 'sales.new' and `ParentId` = 0 and `RefType` = 'm';
DELETE FROM  `navigation` WHERE Id >= @menu_old;

DELETE FROM `navigation` WHERE (Module like 'whsales/%') OR (Mark like 'whsales%');
DELETE FROM `navigation` WHERE (Module like 'sales/%') OR (Mark like 'sales.%');
DELETE FROM `navigationtype` WHERE (Mark = 'ajax');

INSERT INTO `navigationtype`
	(`Name`,`Mark`, `LayoutRaw`)
VALUES
   ('Ajax', 'ajax', 1);
	
SELECT LAST_INSERT_ID() into @navtype_ajax;	
	
#create main menu
INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`) 
VALUES 
	(@menu_name, 0, 2, '', '', '', 'm', @menu_name, 21, 1, 'sales.new', 1, 1); 

SELECT LAST_INSERT_ID() into @last_id;

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
   ('Customer', @last_id, 1, '', '', '', 'm', 'Customer', 21, 1, 'sales.customer', 1, 1, 0),
   ('Budget',   @last_id, 2, '', '', '', 'm', 'Budget', 21, 1, 'sales.budget', 1, 0, 0),
##   ('Season',   @last_id, 3, 'sales/season', '', 'main', 'm', 'Season', 21, 1, 'sales.season', 1, 0, 0),
	('Order',    @last_id, 4, '', '', '', 'm', 'Order', 21, 1, 'sales.order', 1, 1, 1),
##	('Invoice',  @last_id, 5, 'sales/invoice', '', 'main', 'm', 'Invoice', 21, 1, 'sales.invoice', 1, 0, 0),
	('Claim',    @last_id, 6, '', '', '', 'm', 'Claims', 21, 1, 'sales.claim', 1, 1, 0),
	('viewCart', 0, 0, 'sales/ordercart', '', 'view_cart', 'l', 'Orders Cart', 21, 1, 'sales.ordercart.view', 1, 1, 0),	
	('cartInfo', 0, 0, 'sales/ordercart', '', 'get_info', 'l', 'Orders Cart Info', 21, @navtype_ajax, 'sales.ordercart.info', 1, 1, 0),
	('addToCart',  0, 0, 'sales/ordercart', '', 'add_to_cart', 'l', 'Add To Orders Cart', 21, @navtype_ajax, 'sales.ordercart.add', 1, 1, 0),
	('deleteFromCart',  0, 0, 'sales/ordercart', '', 'delete_from_cart', 'l', 'deleteFromCart', 21, @navtype_ajax, 'sales.ordercart.delete', 1, 1, 0),
	('updateLots',  0, 0, 'sales/ordercart', '', 'update_lots', 'l', 'updateLots', 21, @navtype_ajax, 'sales.ordercart.updatelots', 1, 1, 0),
	('viewCart', 0, 0, 'sales/claimcart', '', 'view_cart', 'l', 'Claims Cart', 21, 1, 'sales.claimcart.view', 1, 1, 0),	
	('cartInfo', 0, 0, 'sales/claimcart', '', 'get_info', 'l', 'Claims Cart Info', 21, @navtype_ajax, 'sales.claimcart.info', 1, 1, 0),
	('addToCart',  0, 0, 'sales/claimcart', '', 'add_to_cart', 'l', 'Add to Claims Cart', 21, @navtype_ajax, 'sales.claimcart.add', 1, 1, 0),
	('deleteFromCart',  0, 0, 'sales/claimcart', '', 'delete_from_cart', 'l', 'deleteFromCart', 21, @navtype_ajax, 'sales.claimcart.delete', 1, 1, 0);
	
 	
SELECT Id into @orders_id from navigation where ParentId = 	@last_id AND Mark = 'sales.order';
SELECT Id into @claims_id from navigation where ParentId = 	@last_id AND Mark = 'sales.claim';
SELECT Id into @viewCart_id from navigation where ParentId = 	0 AND Mark = 'sales.ordercart.view';
SELECT Id into @viewClaimCart_id from navigation where ParentId = 	0 AND Mark = 'sales.claimcart.view';

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
	('Create',    @orders_id, 1, '',            '', '',                   't', 'Create',    21, 8,             'sales.order.create',    1, 1, 1),
	('Draft',  	  @orders_id, 2, 'sales/order', '', 'get_draft_list',     't', 'Draft',     21, @navtype_ajax, 'sales.order.draft',     1, 1, 0),
	('Proposal',  @orders_id, 3, 'sales/order', '', 'get_proposal_list',  't', 'Proposal',  21, @navtype_ajax, 'sales.order.proposal',  1, 1, 0),
	('Confirmed', @orders_id, 4, 'sales/order', '', 'get_confirmed_list', 't', 'Confirmed', 21, @navtype_ajax, 'sales.order.confirmed', 1, 1, 0),
	('Shipped',   @orders_id, 5, 'sales/order', '', 'get_shipped_list',   't', 'Shipped',   21, @navtype_ajax, 'sales.order.shipped',   1, 1, 0),
	('Cancelled', @orders_id, 6, 'sales/order', '', 'get_cancelled_list', 't', 'Cancelled', 21, @navtype_ajax, 'sales.order.cancelled', 1, 1, 0),
	('All',       @orders_id, 7, 'sales/order', '', 'get_all_list',       't', 'All',       21, @navtype_ajax, 'sales.order.all',       1, 1, 0);
	
	
SELECT Id into @claims_id from navigation where ParentId = 	@last_id AND Mark = 'sales.claim';
INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `RefIdInclude`, `Legacy`, `ShowCart`)
VALUES 
	('Create',   	@claims_id, 1, 'sales/claim', '', 'create',             't', 'Create',     21, @navtype_ajax, 'sales.claim.create',       0, 1, 1),
	('Request',   	@claims_id, 2, 'sales/claim', '', 'get_request_list',   't', 'Request',    21, @navtype_ajax, 'sales.claim.request',      0, 1, 1),
	('Running',     @claims_id, 3, 'sales/claim', '', 'get_running_list',   't', 'Running',    21, @navtype_ajax, 'sales.claim.running',     0, 1, 1),
	('Confirmed',   @claims_id, 4, 'sales/claim', '', 'get_confirmed_list', 't', 'Confirmed',  21, @navtype_ajax, 'sales.claim.confirmed',    0, 1, 1),
	('Cancelled', 	@claims_id, 5, 'sales/claim', '', 'get_cancelled_list', 't', 'Cancelled',  21, @navtype_ajax, 'sales.claim.cancelled',    0, 1, 1),
	('Claim View', 	@claims_id, 6, 'sales/claim', '', 'get_claim_details',  'l', 'Claim',      38, 1,             'sales.claim.request.view', 1, 1, 1),
	('Claim View', 	@claims_id, 7, 'sales/claim', '', 'get_claim_details',  'l', 'Claim',      38, 1,             'sales.claim.view',         1, 1, 1),
	('Claim Line', 	@claims_id, 8, 'sales/claim', '', 'get_claim_line',     'l', 'Claim Line', 38, 1,             'sales.claim.line.view',    1, 1, 1),
	('Details', 	@claims_id, 9, 'sales/claim', '', 'show_details',       'l', 'Details',    21, @navtype_ajax, 'sales.claim.details',      1, 1, 1);
	
SELECT Id into @claim_view_id from navigation where ParentId = @claims_id and Mark = 'sales.claim.request.view';
INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`,
	 `ShowCart`, `isDefault`, `RefIdInclude`, `Width`, `Height`, `RefConfirm`)
VALUES
    ('Back',                @claim_view_id, 1, '',            '', '',                    't', 'Back',                21, 10, '',                          1, 1, 0, 0, 0,   0,   ''),
	('Full Credit Note', 	@claim_view_id, 2, 'sales/claim', '', 'full_credit_note',    't', 'Full Credit Note',    21, 9,  'sales.full.credit.note',    1, 1, 0, 1, 0,   0,   'Do you really want to make a Credit Note for this Claim ?'),
	('Partial Credit Note', @claim_view_id, 3, 'sales/claim', '', 'partial_credit_note', 't', 'Partial Credit Note', 21, 3,  'sales.partial.credit.note', 1, 1, 0, 1, 650, 700, ''),
	('Pick order',          @claim_view_id, 4, 'sales/claim', '', 'create_pickorder',    't', 'Pick Order',          21, 3,  'sales.claim.pick.order',    1, 1, 0, 1, 650, 700, '');
	
SELECT Id into @partial_credit_note_id from navigation where ParentId = @claim_view_id and Mark = 'sales.partial.credit.note';
INSERT INTO `navigation`
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`, `RefIdInclude`, `RefConfirm`)
VALUES
	('Create', @partial_credit_note_id, 1, 'sales/claim', 'submit_partial_note', 't', 'Create partial Credit Note', 37, 9, 'sales.claim.partial.create', 1, 0, 0, 1, 'Do you really want to make a Credit Note for this Claim ?');
	
SELECT Id into @pick_order_id from navigation where ParentId = @claim_view_id and Mark = 'sales.claim.pick.order';
INSERT INTO `navigation`
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`, `RefIdInclude`, `RefConfirm`)
VALUES
	('Create', @pick_order_id, 1, 'sales/claim', 'submit_pickorder', 't', 'Create Pick Order', 37, 9, 'sales.claim.pickorder.create', 1, 0, 0, 1, 'Do you really want to make a Pick Order for this Claim ?');
	
	
SELECT Id into @shop_id from navigation   where ParentId = 	@orders_id AND Mark = 'sales.order.create';	
INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES
	('All', 			@shop_id, 1, 'sales/shop', '', 'get_all_list', 't', 'All', 21, @navtype_ajax, 'sales.shop.all', 1, 1, 1),
	('Top 10 Styles',   @shop_id, 2, 'sales/shop', '', 'get_top_ten_style', 't', 'Top 10 Styles', 21, @navtype_ajax, 'sales.shop.top_styles', 1, 1, 0),
	('Top 5 types', 	@shop_id, 3, 'sales/shop', '', 'get_top_five_types', 't', 'Top 5 types', 21, @navtype_ajax, 'sales.shop.top_types', 1, 1, 0),
##	('Top & Bottom', 	@shop_id, 4, 'sales/shop', '', 'get_top_bottom', 't', 'Top & Bottom', 21, @navtype_ajax, 'sales.shop.top_bottom', 1, 1, 0),
	('Focus',   		@shop_id, 5, 'sales/shop', '', 'get_focus_articles', 't', 'Focus', 21, @navtype_ajax, 'sales.shop.focus', 1, 1, 0),
	('Details', 	    @shop_id, 6, 'sales/shop', '', 'show_details', 'l', 'Details', 21, @navtype_ajax, 'sales.shop.details', 1, 1, 0);

SELECT Id into @budget_id from navigation where ParentId = 	@last_id AND Mark = 'sales.budget';

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `isDefault`)
VALUES 
	('Customer',@budget_id, 2, 'sales/budget', '', 'get_customer', 't', 'Customer', 21, @navtype_ajax, 'sales.budget.customer', 1, 1),
	('Sales Responsible',  	@budget_id, 3, 'sales/budget', '', 'get_sales', 't', 'Sales Responsible', 21, @navtype_ajax, 'sales.budget.sales', 1, 0),
	('Add/Edit Budget',	@budget_id, 0, 'sales/budget', '', 'edit_budget', 'l', 'Add/Edit Budget', 21, 3, 'sales.budget.editbudget', 1, 0);

SELECT Id into @edit_budget_id from navigation where ParentId = @budget_id AND Mark = 'sales.budget.editbudget';

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `isDefault`)
VALUES 
	('Save',	@edit_budget_id, 1, 'sales/budget', '', 'save_cmd', 't', 'Save', 21, 9, 'sales.budget.create', 1, 0);

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
	('New Order',	@viewCart_id, 1, 'sales/ordercart', '', 'edit_defaults', 't', 'Create New Order', 21, 1, 'sales.ordercart.neworder', 1, 1, 0),
	('New Proposal',	@viewCart_id, 1, 'sales/ordercart', '', 'edit_proposal', 't', 'Create New Proposal', 21, 1, 'sales.ordercart.newproposal', 1, 1, 0),	
	('Add to Existing',	@viewCart_id, 2, 'sales/ordercart', '', 'add_to_draft', 't', 'Add to Existing Order', 21, 1, 'sales.ordercart.add_to_dratf', 1, 1, 0),
	('New Claim',	@viewClaimCart_id, 1, 'sales/claimcart', '', 'edit_defaults', 't', 'Create New Claim', 21, 1, 'sales.claimcart.createnew', 1, 1, 0),
	('Add to Existing',	@viewClaimCart_id, 2, 'sales/claimcart', '', 'add_to_draft', 't', 'Add to Existing Claim', 21, 1, 'sales.claimcart.add_to_dratf', 1, 1, 0);			
	

SELECT Id into @new_id from navigation where ParentId = 	@viewCart_id AND Mark = 'sales.ordercart.neworder';
SELECT Id into @new_prop_id from navigation where ParentId = 	@viewCart_id AND Mark = 'sales.ordercart.newproposal';
SELECT Id into @add_id from navigation where ParentId = 	@viewCart_id AND Mark = 'sales.ordercart.add_to_dratf';	
SELECT Id into @new__cc_id from navigation where ParentId = 	@viewClaimCart_id AND Mark = 'sales.claimcart.createnew';
SELECT Id into @add__cc_id from navigation where ParentId = 	@viewClaimCart_id AND Mark = 'sales.claimcart.add_to_dratf';	

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `RefParameters`, `Mark`, `RefConfirm`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
	('Save', @new_id, 0, 'sales/ordercart', '', 'save_new', 'l', 'Save Order', 21, @navtype_ajax, '', 'sales.ordercart.savenew', '', 1, 1, 0),
	('Save', @new_id, 0, 'sales/claimcart', '', 'save_new', 'l', 'Save Claim', 21, @navtype_ajax, '', 'sales.claimcart.savenew', '', 1, 1, 0),
	
	('Back',	@new_id, 1, '', '', '', 't', 'Back', 21, 10, '', '', '', 1, 1, 0),
	('Confirm & Save',	@new_id, 2, 'sales/ordercart', '', 'saveForm()', 't', 'Confirm & Save', 21, 17, '', '', '',1, 1, 0),			
	('Back',	@new_prop_id, 1, '', '', '', 't', 'Back', 21, 10, '', '', '', 1, 1, 0),
	('Confirm & Save',	@new_prop_id, 2, 'sales/ordercart', '', 'saveForm()', 't', 'Confirm & Save', 21, 17, '', '', '',1, 1, 0),			
	('Back',	@add_id, 1, '', '', '', 't', 'Back', 21, 10, '', '', '', 1, 1, 0),
	('Confirm & Save',	@add_id, 2, 'sales/ordercart', '', 'update_draft', 't', 'Confirm & Save', 21, @navtype_ajax, '', '', 'checkSelection', 1, 1, 0),			
	('Back',	@new__cc_id, 1, '', '', '', 't', 'Back', 21, 10, '', '', '', 1, 1, 0),
	('Confirm & Save',	@new__cc_id, 2, 'sales/claimcart', '', 'saveForm()', 't', 'Confirm & Save', 21, 17, '', '', '',1, 1, 0),			
	('Back',	@add__cc_id, 1, '', '', '', 't', 'Back', 21, 10, '', '', '', 1, 1, 0),
	('Confirm & Save',	@add__cc_id, 2, 'sales/claimcart', '', 'update_draft', 't', 'Confirm & Save', 21, @navtype_ajax, '', '', 'checkSelection', 1, 1, 0);			


SELECT Id into @customer_id from navigation where ParentId = @last_id AND Mark = 'sales.customer';
INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
##	('Create',    @customer_id, 1, 'sales/customer', '', 'create',        't', 'Create',    21, @navtype_ajax, 'sales.customer.create',    1, 1, 0),
	('All',  	  @customer_id, 2, '', '', '',  't', 'All',       21, 8, 'sales.customer.all',       1, 1, 1),
	('Prospects', @customer_id, 3, 'sales/customer', '', 'get_prospects', 't', 'Prospects', 21, @navtype_ajax, 'sales.customer.prospects', 1, 1, 0);
	
SELECT Id into @customer_all_id from navigation where ParentId = @customer_id AND Mark = 'sales.customer.all';
INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`, `isDefault`)
VALUES 
	('List', @customer_all_id, 1, 'sales/customer', '', 'get_all_list', 't', 'List', 21, @navtype_ajax, 'sales.customer.all.download', 1, 1, 1),
	('Download', @customer_all_id, 2, 'sales/customer', '', 'download', 't', 'Download', 21, 14, 'sales.customer.all.download', 1, 1, 0);