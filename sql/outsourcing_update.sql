CREATE TABLE OutSourcing
(Id INT AUTO_INCREMENT PRIMARY KEY,
BundleId INT,
ProductionId INT,
OutDate DATETIME NULL,
InDate DATETIME NULL,
ArticleSizeId INT,
ArticleColorId INT,
OperationFromNo INT,
OperationToNo INT,
UNIQUE KEY (BundleId, ProductionId,OperationFromNo,OperationToNo),
LocationId INT);

CREATE TABLE OutSourcingPlan
( Id INT AUTO_INCREMENT PRIMARY KEY,
ProductionId INT,
ArticleSizeId INT NULL,
ArticleColorId INT NULL,
OperationFromNo INT,
OperationToNo INT,
LocationId INT,
Position INT);