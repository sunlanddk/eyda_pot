UPDATE navigation set Active = 1, `Disable` = 0, DisplayOrder = 12 where Id = 193;
UPDATE navigation set Active = 1, `Disable` = 1 where Id in (909, 519);

INSERT INTO `navigation` 
	(`Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, `Header`, `FunctionId`, `NavigationTypeId`, `Mark`, `Legacy`, `ShowCart`) 
VALUES 
	('Development', 0, 1, '', '', '', 'm', 'Development', 21, 1, '', 1, 0),
	('Purchase', 0, 3, '', '', '', 'm', 'Purchase', 21, 1, '', 1, 0);
	
SELECT Id into @purch_id from navigation where ParentId = 0 AND Name = 'Purchase';
SELECT Id into @dev_id from navigation where ParentId = 0 AND Name = 'Development';



UPDATE navigation set ParentId=@purch_id, Legacy = 0 where Id = 1664;##Purchase->Purchase Orders
UPDATE navigation set ParentId=@purch_id, Legacy = 1 where Id = 10;


UPDATE navigation set ParentId=@dev_id, Name='Case' where Id = 915;##Develpment->Case
UPDATE navigation set ParentId=@dev_id where Id In (1926, 2061, 2075);
#UPDATE navigation set ParentId=@dev_id, Name='Season' where Id = 1994;##Develpment->Season
UPDATE navigation set ParentId=@dev_id, `Disable` = 0 where Id = 913;##Develpment->Article
##UPDATE navigation set `Disable` = 1 where Id in (1994);

##QEMS
UPDATE navigation set Active = 1, `Disable` = 0, DisplayOrder = 11 where Id = 745;	

##after navigation.sql
SELECT Id into @sales_id from navigation where ParentId = 0 AND Name = 'Sales' AND Mark = 'sales.new';
UPDATE navigation set ParentId=@sales_id, `Disable` = 0, DisplayOrder = 3 where Id = 1994;##Sales->Season
UPDATE navigation set ParentId=@sales_id, DisplayOrder = 5 where Id = 1043;

##Sales-Customer-Create
SELECT Id into @cust_id from navigation where  Mark = 'sales.customer';
UPDATE navigation set ParentId=@cust_id, Name='Create', DisplayOrder = 1 where Id = 27;

##
## !!!!!!
##not for production
UPDATE navigation set Legacy = 1 where Id IN (913, 915, 1994);