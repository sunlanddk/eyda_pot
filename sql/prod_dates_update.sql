ALTER TABLE Production ADD COLUMN MaterialDate_Act DATE NULL;
ALTER TABLE Production ADD COLUMN ProductionStartDate_Act DATE NULL;
ALTER TABLE Production ADD COLUMN ProductionEndDate_Act DATE NULL;
ALTER TABLE Production ADD COLUMN SubDeliveryDate_Act DATE NULL;
ALTER TABLE Production ADD COLUMN DeliveryDate_Act DATE NULL;
