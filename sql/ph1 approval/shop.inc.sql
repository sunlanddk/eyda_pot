ALTER TABLE collectionlot
ADD COLUMN `PreSale` INT(10) UNSIGNED NOT NULL DEFAULT '1';

UPDATE collectionlot SET `PreSale` = 0 where id in (71,72,73,76,77,78,82,83,84);