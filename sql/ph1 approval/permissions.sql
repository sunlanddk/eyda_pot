INSERT INTO `function` 
	(Description, Name) 
VALUES 
	('Customer budget', 'Budget customer'),
	('Sales budget', 'Budget sales'),
	('Customer handling', 'Customer'),
	('Claim handling', 'Claim'),
	('Sales', 'Sales');
	
INSERT INTO `role`
 	(Name, Description)
VALUES
	('WH sales', 'WH sales'),
	('WH management', 'WH management');

INSERT INTO `permission` 
	(`Active`, `FunctionId`, `RoleId`, `PermRead`, `PermWrite`, `PermCreate`, `PermReadOther`, `PermWriteOther`, `PermDelete`, `PermDeleteOther`, `CreateUserId`, `ModifyUserId`) 
VALUES 
	(1, 47, 91, 1, 1, 1, 1, 1, 1, 1, 481, 481),
	(1, 47, 123, 1, 1, 1, 1, 1, 0, 0, 481, 481),
	(1, 47, 124, 1, 1, 1, 1, 1, 1, 1, 481, 481),
	(1, 47, 82, 1, 1, 1, 1, 1, 1, 1, 214, 214),
	(1, 48, 91, 0, 0, 0, 0, 0, 0, 0, 481, 481),
	(1, 48, 123, 0, 0, 0, 0, 0, 0, 0, 481, 481),
	(1, 48, 124, 1, 1, 1, 1, 1, 1, 1, 481, 481),
	(1, 48, 82, 0, 0, 0, 0, 0, 0, 0, 214, 214),
	(1, 49, 91, 1, 1, 1, 1, 1, 1, 1, 481, 481),
	(1, 49, 123, 1, 1, 1, 1, 1, 0, 0, 481, 481),
	(1, 49, 124, 1, 1, 1, 1, 1, 1, 1, 481, 481),
	(1, 49, 82, 1, 1, 1, 1, 1, 1, 1, 214, 214),
	(1, 50, 91, 0, 0, 0, 0, 0, 0, 0, 481, 481),
	(1, 50, 124, 0, 0, 0, 0, 0, 0, 0, 481, 481),
	(1, 50, 123, 0, 0, 0, 0, 0, 0, 0, 481, 481),
	(1, 50, 82, 1, 1, 1, 1, 1, 1, 1, 214, 214),
	(1, 51, 91, 1, 1, 1, 1, 1, 0, 0, 481, 481),
	(1, 51, 124, 1, 1, 0, 1, 0, 0, 0, 481, 481),
	(1, 51, 123, 1, 1, 0, 1, 0, 0, 0, 481, 481),
	(1, 51, 82, 1, 1, 1, 1, 1, 1, 1, 214, 214),
	(1, 38, 123, 0, 0, 0, 0, 0, 0, 0, 481, 481),
	(1, 38, 124, 1, 1, 1, 1, 1, 1, 1, 481, 481);
	
	
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.new';
UPDATE `navigation` SET `FunctionId`=49, `Operation`='w' where `Mark`='sales.customer';
UPDATE `navigation` SET `FunctionId`=47, `Operation`='w' where `Mark`='sales.budget';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.view';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.info';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.add';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.delete';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.updatelots';

UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.view';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.info';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.add';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.delete';

UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.create';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.draft';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.proposal';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.confirmed';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.shipped';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.cancelled';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.order.all';

UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.create';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.request';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.running';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.confirmed';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.cancelled';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.request.view';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.view';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.line.view';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.details';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.full.credit.note';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.partial.credit.note';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.pick.order';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.partial.create';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claim.pickorder.create';

UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.shop.all';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.shop.top_styles';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.shop.top_types';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.shop.focus';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.shop.details';

UPDATE `navigation` SET `FunctionId`=47, `Operation`='w' where `Mark`='sales.budget.customer';
UPDATE `navigation` SET `FunctionId`=48, `Operation`='w' where `Mark`='sales.budget.sales';
UPDATE `navigation` SET `FunctionId`=47, `Operation`='w' where `Mark`='sales.budget.editbudget';
UPDATE `navigation` SET `FunctionId`=47, `Operation`='w' where `Mark`='sales.budget.create';

UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.neworder';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.newproposal';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.add_to_dratf';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.ordercart.savenew';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.customer.all';
UPDATE `navigation` SET `FunctionId`=38, `Operation`='w' where `Mark`='sales.customer.prospects';

UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.createnew';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.add_to_dratf';
UPDATE `navigation` SET `FunctionId`=50, `Operation`='w' where `Mark`='sales.claimcart.savenew';


select * from navigation where id = 5041
select * from navigation where mark = 'sales.new';	
select n.functionid, n.* from navigation n where id >= 5038;


49;w;companybasic;2228;Create;2165;1;company;new;basic;t;New Customer;49;1;0001-01-01 12:00:00;2005-04-04 08:11:16;w;Number;0;1;3;;600;700;0;0;0;companybasic;0;add.gif;;0;;0;0;n;1;0;0
46;w;;2229;Season;2164;3;season;;seasonlist;m;Season list;46;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;filter;1;1;13;;0;0;0;0;0;;0;component.gif;;1;;0;0;;1;0;0
37;w;;2230;Invoice;2164;5;invoice;all;invoicelist;m;Invoices;37;1;2004-12-21 19:37:09;2005-05-11 08:42:21;w;filter;1;1;13;;0;0;0;0;0;;0;invoice.gif;;0;;0;0;;1;0;0
21;w;;2216;Back;2209;1;;;;t;Back;21;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;10;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
38;w;;2217;Confirm & Save;2209;3;sales/ordercart;;saveForm();t;Confirm & Save;38;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;17;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
21;w;;2218;Back;2210;1;;;;t;Back;21;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;10;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
38;w;;2219;Confirm & Save;2210;3;sales/ordercart;;saveForm();t;Confirm & Save;38;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;17;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
21;w;;2220;Back;2211;1;;;;t;Back;21;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;10;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
38;w;;2221;Confirm & Save;2211;2;sales/ordercart;;update_draft;t;Confirm & Save;38;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;19;;0;0;0;0;0;;0;;checkSelection;0;;0;0;;1;1;0
21;w;;2222;Back;2212;1;;;;t;Back;21;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;10;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
50;w;;2223;Confirm & Save;2212;2;sales/claimcart;;saveForm();t;Confirm & Save;50;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;17;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
21;w;;2224;Back;2213;1;;;;t;Back;21;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;10;;0;0;0;0;0;;0;;;0;;0;0;;1;1;0
50;w;;2225;Confirm & Save;2213;2;sales/claimcart;;update_draft;t;Confirm & Save;50;1;0001-01-01 12:00:00;0001-01-01 12:00:00;w;;0;0;19;;0;0;0;0;0;;0;;checkSelection;0;;0;0;;1;1;0




