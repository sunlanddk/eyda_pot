<?php

    require_once 'lib/perm.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    $query = sprintf ("SELECT User.Id AS Id, User.Loginname AS Loginname, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, User.Title AS Title, Company.Name AS CompanyName, Role.Name AS RoleName FROM User LEFT JOIN Company ON User.CompanyId=Company.Id LEFT JOIN Role ON User.RoleId=Role.Id%s WHERE User.Active=1 ORDER BY Loginname", permClauseOwner('User')) ;
	    $Result = dbQuery ($query) ;
	    return 0 ;

	case 'online' :
	    $query = sprintf ("SELECT Login.Id, User.Loginname, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, User.Title, Company.Name AS CompanyName, Login.LoginDate, Login.AccessDate FROM (User, Login) LEFT JOIN Company ON User.CompanyId=Company.Id WHERE Login.UserId=User.Id AND Login.Active=1%s ORDER BY Name", permClauseOwner('User')) ;
	    $Result = dbQuery ($query) ;
	    return 0 ;

	case 'onlinedetail' :
	    $query = sprintf ("SELECT Login.Id, User.Loginname, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, User.Title, Company.Name AS CompanyName, Login.LoginDate, Login.AccessDate, Login.AcceptedMimeTypes, Session.UID, Session.IP, Session.Language, Session.Agent FROM (User, Login, Session) LEFT JOIN Company ON User.CompanyId=Company.Id WHERE Session.Id=Login.SessionId AND Login.UserId=User.Id AND Login.Active=1 AND Login.Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch($result) ;
	    dbQueryFree ($result) ;
	    return 0 ;
    }

    switch ($Navigation["Parameters"]) {
	case "new":
	    return 0 ;
	    
	case "my":
	    $Id = $User["Id"] ;
	    break ;
    }
    
//    if ($Id <= 0) return 0 ;
    $Id = $User["Id"] ;
    $query = sprintf ("SELECT * FROM User WHERE Active=1 AND Id=%d", $Id) ;
    $result = dbQuery ($query) ;
    $Record = dbFetch ($result) ;
    dbQueryFree ($result) ;
    
    return 0 ;
?>
