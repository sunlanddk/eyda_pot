<?php
   
    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/perm.inc' ;
    
    $my = false ;
    switch ($Navigation["Parameters"]) {
	case "new":
	    unset ($Record) ;
	    $Id = -1 ;
	    $Record["TimeZoneId"] = tableDefaultId ("TimeZone") ;
	    break ;

	case "my":
	    $my = true ;
	    break ;
	    
	case "userid":
	    break ;
	
	default:
	    return sprintf ("%s(%d), invalid parameter, value '%s'", __FILE__, __LINE__, $Navigation["Parameters"]) ;
    }

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    if (!$my) {
	print htmlItemHeader() ;
	printf ("<tr><td><p>Loginname</p></td><td><input type=text name=Loginname size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["Loginname"])) ;
	printf ("<tr><td><p>Password</p></td><td><input type=password name=Password size=20></td></tr>\n") ;
	printf ("<tr><td><p>Login</p></td><td><input type=checkbox name=Login %s></td></tr>\n", ($Record["Login"])?"checked":"") ;
    }
    print htmlItemSpace() ;
    printf ("<tr><td><p>Title</p></td><td><input type=text name=Title size=40 value=\"%s\"></td></tr>\n",  htmlentities($Record["Title"])) ;
    printf ("<tr><td><p>Firstname</p></td><td><input type=text name=FirstName size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["FirstName"])) ;
    printf ("<tr><td><p>Lastname</p></td><td><input type=text name=LastName size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["LastName"])) ;
    print htmlItemSpace() ;
    if (!$my) {
	printf ("<tr><td><p>Company</p></td><td>%s</td></tr>\n", htmlDBSelect ("CompanyId style='width:250px'", $Record["CompanyId"], "SELECT Id, Name AS Value FROM Company WHERE Active=1 ORDER BY Name")) ;  
    } else {
	printf ("<tr><td><p>Company</p></td><td><p>%s</p></td></tr>\n", htmlentities(tableGetField("Company", "Name", (int)$Record["CompanyId"]))) ;  
    }	
    print htmlItemSpace() ;
    printf ("<tr><td><p>E-mail</p></td><td><input type=text name=Email size=30 value=\"%s\"></td></tr>\n",  htmlentities($Record["Email"])) ;    
    print htmlItemSpace() ;
    printf ("<tr><td><p>Timezone</p></td><td>%s</td></tr>\n", htmlDBSelect ("TimeZoneId style='width:200px;'", $Record["TimeZoneId"], sprintf("SELECT Id, Name AS Value FROM TimeZone WHERE Active=1 ORDER BY Name"))) ;   
    if (!$my and permAdminSystem()) {
        print htmlItemSpace() ;
	printf ("<tr><td><p>Administrator</p></td><td><input type=checkbox name=AdminSystem %s></td></tr>\n", ($Record["AdminSystem"])?"checked":"") ;
    } else {
	printf ("<input type=hidden name=AdminSystem value='%s'>\n", ($Record["AdminSystem"])?"on":"") ;
    }
    if (!$my) {
        print htmlItemSpace() ;
	printf ("<tr><td><p>Role</p></td><td>%s</td></tr>\n", htmlDBSelect ("RoleId style='width:150px;'", $Record["RoleId"], 'SELECT Role.Id, Role.Name AS Value FROM Role WHERE Role.ProjectId=0 AND Role.Active=1 ORDER BY Role.Name')) ;
        if ($Config['proEnable']) {
	    print htmlItemSpace() ;
	    printf ("<tr><td><p>Select project</p></td><td><input type=checkbox name=ProjectSelect %s></td></tr>\n", ($Record["ProjectSelect"])?"checked":"") ;
	    printf ("<tr><td><p>Project</p></td><td>%s</td></tr>\n", htmlDBSelect ("ProjectId style='width:200px;'", $Record["ProjectId"], sprintf('SELECT Project.Id, Project.Name AS Value FROM Project, UserRole WHERE UserRole.ProjectId=Project.Id AND UserRole.Active=1 AND Project.Active=1 AND UserRole.UserId=%d ORDER BY Project.Name', $Record['Id']))) ;
	}
    }
    print htmlItemSpace() ;
    printf ("<tr><td><p>Business</p></td><td><input type=text name=PhoneBusiness size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhoneBusiness"])) ;
    printf ("<tr><td><p>Direct</p></td><td><input type=text name=PhoneDirect size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhoneDirect"])) ;
    printf ("<tr><td><p>Mobile</p></td><td><input type=text name=PhoneMobile size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhoneMobile"])) ;
    printf ("<tr><td><p>Private</p></td><td><input type=text name=PhonePrivate size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhonePrivate"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Address</p></td><td><input type=text name=Address1 size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["Address1"])) ;
    printf ("<tr><td></td><td><input type=text name=Address2 size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["Address2"])) ;
    printf ("<tr><td><p>ZIP</p></td><td><input type=text name=ZIP size=15 value=\"%s\"></td></tr>\n",  htmlentities($Record["ZIP"])) ;
    printf ("<tr><td><p>City</p></td><td><input type=text name=City size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["City"])) ;
    printf ("<tr><td><p>Country</p></td><td><input type=text name=Country size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["Country"])) ; 
printf ("<tr><td class=itemlabel>Extern</td><td><input type=checkbox name=Extern size=50 %s></td></tr>\n", $Record["Extern"]?'checked':'') ;

	
print htmlItemInfo($Record) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
