<?php

    require "lib/save.inc" ;

    function checkfield ($fieldname, $value) {
	global $User, $fields ;
	switch ($fieldname) {
	    case "Password1":
		return false ;

	    case "Password":
		if ($fields["Password1"]["value"] != $value) return "You have not re-entered the same password" ;
		if ($value == $fields["Password"]["db"]) return "You have entered the existing password" ;
		if (strlen ($value) < 5) return "Your password must contain at least 5 characters" ;
		if ($value == $User["Loginname"]) return "Your password must be different from your Loginname" ;
		return true ;
	}
	return false ;
    }

    $fields = array (
	"Password1"	=> array ("check" => true),
	"Password"	=> array ("check" => true)
    ) ;

    return saveFields ("User", $User["Id"], $fields) ;
?>
