<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of ProductionLocations
	    $query = sprintf ('SELECT ProductionLocation.*, Company.Name AS Company FROM ProductionLocation LEFT JOIN Company ON Company.Id=ProductionLocation.CompanyId WHERE ProductionLocation.Active=1 ORDER BY ProductionLocation.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query ProductionLocation
	    $query = sprintf ('SELECT ProductionLocation.* FROM ProductionLocation WHERE ProductionLocation.Id=%d AND ProductionLocation.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
