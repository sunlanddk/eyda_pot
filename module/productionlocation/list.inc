<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 150) ;
    listHead ('Description') ;
    listHead ('Company') ;
    listHead ('Production', 60, 'align=right') ;
    listHead ('Sample', 60, 'align=right') ;
    listHead ('Sew', 60, 'align=right') ;
    listHead ('Bundles', 60, 'align=right') ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['Company']) ;
 	listField (($row['TypeProduction']) ? 'Yes' : 'No', 'align=right') ;
 	listField (($row['TypeSample']) ? 'Yes' : 'No', 'align=right') ;
 	listField (($row['TypeSew']) ? 'Yes' : 'No', 'align=right') ;
 	listField (($row['DoBundles']) ? 'Yes' : 'No', 'align=right') ;
   }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No ProductionLocations', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
