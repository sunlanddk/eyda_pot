<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    $Record['IndexStart'] = 1 ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Company</td><td>%s</td></tr>\n", htmlDBSelect ('CompanyId style="width:300px"', $Record['CompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE Active=1 AND TypeSupplier=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>IndexStart</td><td><input type=text name=IndexStart maxlength=6 size=6 value=%d></td></tr>\n", $Record['IndexStart']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Production</td><td><input type=checkbox name=TypeProduction %s></td></tr>\n", ($Record['TypeProduction'])?'checked':'') ;
    printf ("<tr><td class=itemfield>Sample</td><td><input type=checkbox name=TypeSample %s></td></tr>\n", ($Record['TypeSample'])?'checked':'') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Sew</td><td><input type=checkbox name=TypeSew %s></td></tr>\n", ($Record['TypeSew'])?'checked':'') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Bundles</td><td><input type=checkbox name=DoBundles %s></td></tr>\n", ($Record['DoBundles'])?'checked':'') ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
