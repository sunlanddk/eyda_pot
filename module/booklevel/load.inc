<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Allways use Book.Id=1
	    $Id = 1 ;
	    
	    // Query Book
	    $query = sprintf ("SELECT Book.* FROM Book WHERE Book.Id=%d", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Query list of BookLevels
	    $query = sprintf ("SELECT BookLevel.* FROM BookLevel WHERE BookLevel.BookId=%d AND BookLevel.Active=1 ORDER BY BookLevel.Level, BookLevel.DisplayOrder, BookLevel.Name", $Id) ;
	    $Result = dbQuery ($query) ;
	    break ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    // Allways use Book.Id=1
		    $Id = 1 ;
	    
		    // Query Book
		    $query = sprintf ("SELECT Book.* FROM Book WHERE Book.Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;
		    return 0 ;
	    }

	    // Fall through
	    
	case 'view' :
	    // Get record
	    if ($Id <= 0) break ;
	    
	    $query = sprintf ("SELECT BookLevel.*, Book.Name AS BookName, Book.Description AS BookDescription FROM BookLevel, Book WHERE BookLevel.Id=%d AND BookLevel.Active=1 AND Book.Id=BookLevel.BookId", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Query list of Documents
	    $query = sprintf ("SELECT DocVersion.Id, DocVersion.Version, DocVersion.Released, DocVersion.CreateDate, Doc.Id as DocId, Doc.Name, Doc.Description, User.Loginname FROM DocType, Doc, DocVersion LEFT JOIN User ON User.Id=DocVersion.CreateUserId WHERE DocType.Mark='book' AND DocType.Active=1 AND Doc.DocTypeId=DocType.Id AND Doc.ParentId=%d AND Doc.Active=1 AND DocVersion.DocId=Doc.Id AND DocVersion.Active=1 ORDER BY Doc.Name, DocVersion.Released DESC, DocVersion.Version DESC", $Id) ;
	    $Result = dbQuery ($query) ;
	    break ;

	case 'search' :
	    // Allways use Book.Id=1
	    $Id = 1 ;
	    
	    // Get filter parameter
	    $FilterValue = stripslashes ($_GET['filter']) ;
	    $FilterClause = ($FilterValue) ? sprintf (' AND Doc.KeyWords LIKE "%%%s%%"', addslashes($FilterValue)) : "" ;

	    // Query Book
	    $query = sprintf ("SELECT Book.Name AS BookName, Book.Description AS BookDescription FROM Book WHERE Book.Id=%d AND Book.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    if ($FilterValue) {
		// Query list of Documents
		$query = sprintf ("SELECT DocVersion.Id, Doc.Id as DocId, Doc.Name, Doc.Description, BookLevel.Level, BookLevel.Delimiter AS BookLevelDelimiter, BookLevel.Name AS BookLevelName FROM DocType, Doc, DocVersion, BookLevel WHERE DocType.Mark='book' AND DocType.Active=1 AND Doc.DocTypeId=DocType.Id AND BookLevel.BookId=%d AND BookLevel.Id=Doc.ParentId AND Doc.Active=1 AND DocVersion.DocId=Doc.Id AND DocVersion.Active=1 AND DocVersion.Released=1 %s ORDER BY BookLevel.Level, Doc.Name, DocVersion.Version DESC", $Id, $FilterClause) ;
	    } else {
		// Empty query
		$query = 'SELECT * FROM Doc WHERE 1=0' ;
	    }
	    $Result = dbQuery ($query) ;
	    break ;	    
    }

    return 0 ;
?>
