<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Book</td><td class=itemfield>%s, %s</td></tr>\n", htmlentities($Record['BookName']), htmlentities($Record['BookDescription'])) ;
    printf ("<tr><td class=itemlabel>Level</td><td class=itemfield>%d: %s</td></tr>\n", htmlentities($Record['Level']), htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 90) ;
    listHead ('Description') ;
    listHead ('Version', 50, 'align=right') ;
    listHead ('Released', 55) ;
    listHead ('User', 80) ;
    listHead ('Created', 120) ;
    $lastDocId = 0 ;
    while ($row = dbFetch($Result)) {
	if ($lastDocId == $row['DocId']) continue ;
	$lastDocId = $row['DocId'] ;
        listRow () ;
	listFieldIcon ('doc.gif', 'bookview', $row['Id']) ;
	listField ($row["Name"]) ;
	listField ($row["Description"]) ;
	listField ($row["Version"], 'align=right') ;
	listField (($row['Released']) ? 'yes' : 'no') ;
	listField ($row["Loginname"]) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row["CreateDate"]))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No documents', 'colspan=3') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
