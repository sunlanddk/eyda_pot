<?php

    require_once 'lib/list.inc' ;

    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Name', 80) ;
    listHead ('Description') ;
    listHead ('Category', 90) ;
    listHead ('Weight (Kg)', 90, 'align=right') ;
    listHead ('Unit', 80, 'align=right') ;
    listHead ('Quantity', 80,  'align=right') ;
    listHead ('Value (Kr)', 80, 'align=right') ;
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['CustomsCategoryName']) ;
	listField ($row['Weight'], 'align=right') ;
	listField ($row['UnitName'], 'align=right') ;
	listField (number_format ((float)$row['YearQuantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;
	listField ($row['YearValue'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No CustomsPositions', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
