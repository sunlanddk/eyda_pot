<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Name'			=> array ('mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'CustomsCategoryId'	=> array ('type' => 'integer',	'mandatory' => true),
	'Weight'		=> array ('type' => 'decimal',	'format' => '8.3'),
	'UnitId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'YearQuantity'		=> array ('type' => 'decimal'),
	'YearValue'		=> array ('type' => 'decimal',	'format' => '10.2')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $fields ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM CustomsPosition WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'CustomsPosition allready existing' ;
		return true ;

	    case 'UnitId' :
		if (!$changed) {
		    $fields['YearQuantity']['format'] = sprintf ('9.%d', (int)$Record['UnitDecimals']) ;
		    return false ;
		}
		
		// Get Unit
		$query = sprintf ('SELECT Id AS UnitId, Name AS UnitName, Decimals AS UnitDecimals FROM Unit WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['UnitId'] != $value) return sprintf ('%s(%d) unit not found, id %d', __FILE__, __LINE__, $value) ;

		// Set new quantity format
		$fields['YearQuantity']['format'] = sprintf ('9.%d', (int)$row['UnitDecimals']) ;
		return true ;		
	}
	return false ;
    }
     
    return saveFieldsStrict ('CustomsPosition', $Id, $fields) ;
?>
