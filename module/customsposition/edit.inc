<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    unset ($Record) ;
	    break ;
    }
 
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Name</td><td><input type=text name=Name maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%;' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Category</td><td>%s</td></tr>\n", htmlDBSelect ('CustomsCategoryId style="width:150px;"', $Record['CustomsCategoryId'], 'SELECT Id, Name AS Value FROM CustomsCategory WHERE Active=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Weight</td><td><input type=text name=Weight maxlength=8 size=8 value=\"%s\"></td></tr>\n", htmlentities($Record['Weight'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Unit</td><td>%s</td></tr>\n", htmlDBSelect ('UnitId style="width:100px;"', $Record['UnitId'], 'SELECT Id, Name AS Value FROM Unit WHERE Active=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Year Quantity</td><td><input type=text name=YearQuantity maxlength=6 size=6 value=\"%s\" style='text-align:right;'> %s</td></tr>\n", number_format ($Record['YearQuantity'], (int)$Record['UnitDecimals'], ',', ''), htmlentities($Record['UnitName'])) ;
    printf ("<tr><td class=itemfield>Year Value</td><td><input type=text name=YearValue maxlength=14 size=14 value=\"%s\" style='text-align:right;'> Kr</td></tr>\n", htmlentities($Record['YearValue'])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
    
    return 0 ;
?>
