<?php

    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Name',		'db' => 'CustomsPosition.Name'),
		array ('name' => 'Description',		'db' => 'CustomsPosition.Description'),
		array ('name' => 'Category',		'db' => 'CustomsCategory.Name'),
		array ('name' => 'Weight (Kg)',		'db' => 'CustomsPosition.Weight'),
		array ('name' => 'Unit',		'db' => 'Unit.Name'),
		array ('name' => 'Quantity',		'db' => 'CustomsPosition.YearQuantity'),
		array ('name' => 'Value (Kr)',		'db' => 'CustomsPosition.YearValue')
	    ) ;
	    $queryFields = 'CustomsPosition.*, Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, CustomsCategory.Name AS CustomsCategoryName' ;
	    $queryTables = 'CustomsPosition LEFT JOIN CustomsCategory ON CustomsCategory.Id=CustomsPosition.CustomsCategoryId LEFT JOIN Unit ON Unit.Id=CustomsPosition.UnitId' ;
	    $queryClause = 'CustomsPosition.Active=1' ;	    
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through
		
	case 'delete-cmd' :
	    // Get Article
	    $query = sprintf ("SELECT CustomsPosition.*, Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals FROM CustomsPosition LEFT JOIN Unit ON Unit.Id=CustomsPosition.UnitId WHERE CustomsPosition.Id=%d AND CustomsPosition.Active=1", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return 0 ;
    }
 
    return 0 ;
?>
