 <script type="text/javascript" >
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
		var e = col[n] ;
		if (e.type != 'checkbox') continue ;
		e.checked = true ;
	}
}
</script>

<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;

	itemStart() ;
	itemField('Variant', $Record['VariantCode'] . ' - ' . $Record['VariantDescription']) ;
	itemField('PickPosition', $Record['Position']) ;
	itemSpace() ;
	itemEnd() ;
	
    formStart () ;
    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Print', 30) ;
    listHead ('Order', 45)  ;
    listHead ('Customer', 150) ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldRaw (formCheckbox (sprintf('PrintFlag[%d]', (int)$row['PickOrderId']), 0, '')) ;
    	listField ($row["Reference"]) ;
    	listField ($row["CompanyName"]) ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No PickOrders', 'colspan=5') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
