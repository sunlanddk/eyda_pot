<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;
    require_once 'lib/barcode128.inc';
    $xmas = false;

    class BARCODEPDF extends PDF_Code128 {

    	function getWebshipperOrder($WebshipperId){
    		$ch = curl_init('https://eyda-aps.api.webshipper.io/v2/orders/'.$WebshipperId);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. _WEBSHIPPER_TOKEN_ , 'Content-Type: application/vnd.api+json'));
	        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($webshipper));
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        $result = curl_exec($ch);    
	        $resultObj = json_decode($result, true);
	        if(isset($resultObj['data']) === true){
	        	return $webshipperdata = array(
		        	'shipping_method' => $resultObj['data']['attributes']['original_shipping']['shipping_name'],
		        	'name' => $resultObj['data']['attributes']['delivery_address']['att_contact'],
		        	'address1' => $resultObj['data']['attributes']['delivery_address']['address_1'],
		        	'address2' => $resultObj['data']['attributes']['delivery_address']['address_2'],
		        	'zip' => $resultObj['data']['attributes']['delivery_address']['zip'],
		        	'city' => $resultObj['data']['attributes']['delivery_address']['city'],
		        	'country_code' => $resultObj['data']['attributes']['delivery_address']['country_code'],
		        	'phone' => $resultObj['data']['attributes']['delivery_address']['phone'],
		        	'email' => $resultObj['data']['attributes']['delivery_address']['email'],
		        );
	        }
	        return false;
    	}
	
	function Header() {
	    global $Record, $Company, $CompanyMy, $Now, $headerInformation, $prefix, $Navigation, $LastPage, $noHeaderInfo, $xmas ;
	    $this->SetAutoPageBreak(false) ; 

	    $query = sprintf ("SELECT oa.*, c.Name as countryName FROM orderadresses oa LEFT JOIN country c on c.Id=oa.CountryId WHERE oa.Active=1 AND oa.Type='shipping' AND oa.OrderId=%d", $Record['OrderId']) ;
	    $res = dbQuery ($query) ;
	    $orderaddress = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		if($Navigation["Parameters"] === "preview"){
			$orderaddress = array(
				"countryName" => "DK",
				"AltCompanyName" => "CompanyName",
				"Address1" => "test address 1",
				"Address2" => "test address 2",
				"ZIP" 		=> "2300",
				"City"      => "Copnehagen",
				"PackageShop" => "package shop"
			);
			$Record["WebshipperId"]	= "test";
			$Record["OrderReference"]	= "test order ref";
			$Record["oEmail"]	= "test email";
			$Record["oPhone"]	= "test phone";
			$Record["ShippingShop"] = "test shippingshop";

		}


	    // $webshipperData = $this->getWebshipperOrder($Record['WebshipperId']);
	    // if($webshipperData === false){
	    // 	echo 'Webshipper not responding.';
	    // 	die();
	    // }


        if ($noHeaderInfo) {

            $ImageString = sprintf ('image/logo.jpg') ;
            $this->Image ($ImageString, 15, 10, 64) ;
            $this->Code128(148, 10, $Record['OrderReference'], 40, 14);
            $this->SetMargins(146,0,0) ;
            $this->SetFont('Arial','',8);
            $this->setXY(148, 25);
            $this->Cell(40, 4, $Record['WebshipperId'], '', '', 'C') ;
            $this->SetY (34) ;
            $this->SetX(10);
            $this->SetMargins(15,51,6) ;
            //$this->SetY (117) ;
            $this->SetAutoPageBreak(true, 30) ; 
            return;
        }

        if ($LastPage) {

            $this->SetY (11) ;
            $this->SetFont('Arial','',16);
            $this->Cell(10, 4, $headerInformation[$prefix]['order'].' # '.$Record['OrderReference'], 0, 1) ;

            $this->Code128(148, 10, $Record['WebshipperId'], 40, 14);
            $this->SetMargins(146,0,0) ;
            $this->SetFont('Arial','',8);
            $this->setXY(148, 25);
            $this->Cell(40, 4, $Record['WebshipperId'], '', '', 'C') ;
            $this->SetY (34) ;
            $this->SetFont('Arial','',8);


            $this->SetFont('Arial','',11);
            $this->SetMargins(15,51,6) ;
            //$this->SetY (117) ;
            $this->SetAutoPageBreak(true, 30) ; 
            return;
        }


	    $ImageString = sprintf ('image/logo.jpg') ;
	    if ($xmas) {
	    	$this->Image ($ImageString, 15, 10, 36) ;
	    } else {
	    	$this->Image ($ImageString, 15, 10, 64) ;
	    }
	    
	    $this->Code128(148, 10, $Record['WebshipperId'], 40, 14);
	    $this->SetMargins(146,0,0) ;
	    $this->SetFont('Arial','',8);
	    $this->setXY(148, 25);
	    $this->Cell(40, 4, $Record['WebshipperId'], '', '', 'C') ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);

	    // cell felter: feltbredde, felthøjde, indhold, ramme, linieskifte, Align
		
	    // Title text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(14,0,0) ;
	    if ($xmas) {
	    	$this->SetY (23) ;
	    } else {
	    	$this->SetY (45) ;
	    }
	    
		// if (($Record['Packed']) AND ($Record['ToId']>0)) 
		//     $this->Cell(30, 8, 'PICK ORDER CLOSED', 0, 1) ;
		// else
		//     $this->Cell(30, 8, 'PICK ORDER', 0, 1) ;
	 //    $this->SetFont('Arial','',9);
	 //    $this->SetMargins(20,0,0) ;

	    $this->SetFont('Arial','',16);
	    if ($xmas) {
	    	$this->Cell(50, 3, $headerInformation[$prefix]['order'].' # '.$Record['OrderReference'], 0, 1) ;
	    } else {
	    	$this->Cell(50, 4, $headerInformation[$prefix]['order'].' # '.$Record['OrderReference'], 0, 1) ;
	    }   
	    $this->SetMargins(17,0,0) ;

	    // Header
	    if ($xmas) {
	    	$y = $this->getY();
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->setXY(15, $y+=5);
		    $headerY = $y;
		    $this->SetFont('Arial','B',10);
		    $this->MultiCell(90, 5, $headerInformation[$prefix]['receiver'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',10);
		    $this->SetTextColor(0,0,0) ;

		    $this->setXY(15,$y+=6);
		    $this->Cell(30, 2, $orderaddress['AltCompanyName']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 2, $orderaddress['Address1']) ;
		    $this->setXY(15,$y+=4);
		    if($orderaddress['Address2'] !== ''){
		    	$this->Cell(30, 2, $orderaddress['Address2']) ;
		    	$this->setXY(15,$y+=4);
		    }
		    $this->Cell(30, 2, $orderaddress['ZIP'].' '.$orderaddress['City']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 2, $orderaddress['countryName']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 2, $Record['oEmail']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 3, $Record['oPhone']) ;


		    $this->setXY(105,$headerY);
		    $y = $headerY;
		    $this->SetFont('Arial','B',10);
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->MultiCell(90, 5, $headerInformation[$prefix]['details'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',10);
		    $this->SetTextColor(0,0,0) ;
		    $this->setXY(105,$y+=7);
		    $this->Cell(30, 2, $headerInformation[$prefix]['date'].': '.date('Y-m-d H:i:s', strtotime($Record['CreateDate']))) ;
		    $this->setXY(105,$y+=4);
		    $this->MultiCell(80, 4, $headerInformation[$prefix]['shipping'].': '.$Record['ShippingShop']) ;


		    $this->SetMargins(15,0,0) ;
		    $this->SetX(15) ;
		    $this->SetY(65) ;
		    
		    $this->SetFillColor(150,150,150) ;
		    $this->SetDrawColor(150,150,150) ;
		    $this->MultiCell(180, 5, '', 1, '', 1) ;
		    $this->SetX(15) ;
		    $this->SetY(65.5) ;
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFont('Arial','B',10);
		    $this->Cell(35, 4, $headerInformation[$prefix]['sku']) ;
		    $this->Cell(95, 4, $headerInformation[$prefix]['product']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['position']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['amount'], 0, 0, 'C') ;
		    $this->SetTextColor(0,0,0) ;

		    // Initialize for main page
		    $this->SetFont('Arial','',11);
		    $this->SetMargins(15,51,6) ;
		    $this->SetY (70) ;
		    $this->SetAutoPageBreak(true, 30) ; 
	    } else {
	    	$y = $this->getY();
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->setXY(15, $y+=5);
		    $headerY = $y;
		    $this->SetFont('Arial','B',12);
		    $this->MultiCell(90, 7, $headerInformation[$prefix]['receiver'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',12);
		    $this->SetTextColor(0,0,0) ;

		    $this->setXY(15,$y+=8);
		    $this->Cell(30, 4, $orderaddress['AltCompanyName']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $orderaddress['Address1']) ;
		    $this->setXY(15,$y+=5);
		    if($orderaddress['Address2'] !== ''){
		    	$this->Cell(30, 4, $orderaddress['Address2']) ;
		    	$this->setXY(15,$y+=5);
		    }
		    $this->Cell(30, 4, $orderaddress['ZIP'].' '.$orderaddress['City']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $orderaddress['countryName']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $Record['oEmail']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $Record['oPhone']) ;


		    $this->setXY(105,$headerY);
		    $y = $headerY;
		    $this->SetFont('Arial','B',12);
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->MultiCell(90, 7, $headerInformation[$prefix]['details'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',12);
		    $this->SetTextColor(0,0,0) ;
		    $this->setXY(105,$y+=8);
		    $this->Cell(30, 4, $headerInformation[$prefix]['date'].': '.date('Y-m-d H:i:s', strtotime($Record['CreateDate']))) ;
		    $this->setXY(105,$y+=5);
		    $this->MultiCell(80, 4.5, $headerInformation[$prefix]['shipping'].': '.$Record['ShippingShop']) ;


		    $this->SetMargins(15,0,0) ;
		    $this->SetX(15) ;
		    $this->SetY(110) ;
		    
		    $this->SetFillColor(150,150,150) ;
		    $this->SetDrawColor(150,150,150) ;
		    $this->MultiCell(180, 7, '', 1, '', 1) ;
		    $this->SetX(15) ;
		    $this->SetY(111.5) ;
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFont('Arial','B',12);
		    $this->Cell(35, 4, $headerInformation[$prefix]['sku']) ;
		    $this->Cell(95, 4, $headerInformation[$prefix]['product']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['position']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['amount'], 0, 0, 'C') ;
		    $this->SetTextColor(0,0,0) ;

		    // Initialize for main page
		    $this->SetFont('Arial','',11);
		    $this->SetMargins(15,51,6) ;
		    $this->SetY (117) ;
		    $this->SetAutoPageBreak(true, 30) ; 
	    }
	    
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

	    $this->SetY(-15);
        $this->SetX(180);
        $this->SetFont('Arial','',8);
        $this->Cell(173,10, ' Side '. $this->GroupPageNo() .' af ' . $this->PageGroupAlias(), 0, 0);
	}
	
	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}

	}

	$contentDa = json_decode(tableGetField('pickorder_texts', 'content', 1), true);
	$contentEu = json_decode(tableGetField('pickorder_texts', 'content', 2), true);
	$contentSe = json_decode(tableGetField('pickorder_texts', 'content', 4), true);
	$contentNo = json_decode(tableGetField('pickorder_texts', 'content', 3), true);
	$contentAm = json_decode(tableGetField('pickorder_texts', 'content', 5), true);
	
    $headerInformation = array(
    		'da' => array(
    		'order' => 'Ordre',
    		'receiver' => 'Modtager',
    		'details' => 'Detaljer',
    		'date' => 'Ordre dato',
    		'shipping' => 'Fragtmetode',
    		'sku' => 'Varenummer',
    		'product' => 'Vare',
    		'position' => 'Position',
    		'amount' => 'Antal',
    		'header1' => utf8_decode($contentDa["header1"]),
    		'footer1' => utf8_decode($contentDa["line1"]),
    		'header2' => utf8_decode($contentDa["header2"]),
    		'footer2' => utf8_decode($contentDa["line2"]),
    		'header3' => utf8_decode($contentDa["header3"]),
    		'footer3' => utf8_decode($contentDa["line3"]),
    		'header4' => utf8_decode($contentDa["header4"]),
    		'footer4' => utf8_decode($contentDa["line4"]),
    		'header5' => utf8_decode($contentDa["header5"]),
    		'footer5' => utf8_decode($contentDa["line5"]),
    		'header6' => utf8_decode($contentDa["header6"]),
    		'footer6' => utf8_decode($contentDa["line6"]),
    		'header7' => utf8_decode($contentDa["header7"]),
    		'footer8' => utf8_decode($contentDa["linegift"]),
    	),
    	'eu' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => utf8_decode($contentEu["header1"]),
    		'footer1' => utf8_decode($contentEu["line1"]),
    		'header2' => utf8_decode($contentEu["header2"]),
    		'footer2' => utf8_decode($contentEu["line2"]),
    		'header3' => utf8_decode($contentEu["header3"]),
    		'footer3' => utf8_decode($contentEu["line3"]),
    		'header4' => utf8_decode($contentEu["header4"]),
    		'footer4' => utf8_decode($contentEu["line4"]),
    		'header5' => utf8_decode($contentEu["header5"]),
    		'footer5' => utf8_decode($contentEu["line5"]),
    		'header6' => utf8_decode($contentEu["header6"]),
    		'footer6' => utf8_decode($contentEu["line6"]),
    		'header7' => utf8_decode($contentEu["header7"]),
    		'footer8' => utf8_decode($contentEu["linegift"]),
    	),
    	'se' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => utf8_decode($contentSe["header1"]),
    		'footer1' => utf8_decode($contentSe["line1"]),
    		'header2' => utf8_decode($contentSe["header2"]),
    		'footer2' => utf8_decode($contentSe["line2"]),
    		'header3' => utf8_decode($contentSe["header3"]),
    		'footer3' => utf8_decode($contentSe["line3"]),
    		'header4' => utf8_decode($contentSe["header4"]),
    		'footer4' => utf8_decode($contentSe["line4"]),
    		'header5' => utf8_decode($contentSe["header5"]),
    		'footer5' => utf8_decode($contentSe["line5"]),
    		'header6' => utf8_decode($contentSe["header6"]),
    		'footer6' => utf8_decode($contentSe["line6"]),
    		'header7' => utf8_decode($contentSe["header7"]),
    		'footer8' => utf8_decode($contentSe["linegift"]),
    	),
    	'no' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
			'header1' => utf8_decode($contentNo["header1"]),
    		'footer1' => utf8_decode($contentNo["line1"]),
    		'header2' => utf8_decode($contentNo["header2"]),
    		'footer2' => utf8_decode($contentNo["line2"]),
    		'header3' => utf8_decode($contentNo["header3"]),
    		'footer3' => utf8_decode($contentNo["line3"]),
    		'header4' => utf8_decode($contentNo["header4"]),
    		'footer4' => utf8_decode($contentNo["line4"]),
    		'header5' => utf8_decode($contentNo["header5"]),
    		'footer5' => utf8_decode($contentNo["line5"]),
    		'header6' => utf8_decode($contentNo["header6"]),
    		'footer6' => utf8_decode($contentNo["line6"]),
    		'header7' => utf8_decode($contentNo["header7"]),
    		'footer8' => utf8_decode($contentNo["linegift"]),
    	),
    	'am' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => utf8_decode($contentAm["header1"]),
    		'footer1' => utf8_decode($contentAm["line1"]),
    		'header2' => utf8_decode($contentAm["header2"]),
    		'footer2' => utf8_decode($contentAm["line2"]),
    		'header3' => utf8_decode($contentAm["header3"]),
    		'footer3' => utf8_decode($contentAm["line3"]),
    		'header4' => utf8_decode($contentAm["header4"]),
    		'footer4' => utf8_decode($contentAm["line4"]),
    		'header5' => utf8_decode($contentAm["header5"]),
    		'footer5' => utf8_decode($contentAm["line5"]),
    		'header6' => utf8_decode($contentAm["header6"]),
    		'footer6' => utf8_decode($contentAm["line6"]),
    		'header7' => utf8_decode($contentAm["header7"]),
    		'footer8' => utf8_decode($contentAm["linegift"]),
    	)
    );
    // Variables
    $LastPage = false ;
	$first= 1 ;
	
    // Make PDF
    $pdf=new BARCODEPDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->SetAutoPageBreak(true, 15) ; 
	foreach ($_POST['PrintFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$noHeaderInfo = false;
	    $query = 
	     sprintf ("SELECT if((Container.`Position` is null) or (Container.`Position`=''), 
	                               if(VariantCode.PickContainerId>0,'X-NoPos+C','X-NoPos'), 
	                               if (sum(item.quantity)>0,Container.`Position`,concat(Container.`Position`,'-X'))) AS `Position`,
				sum(Item.Quantity) as ItemQty, 
				PickOrderLine.Id AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, 
				Article.Number as ArticleNumber, VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, 
				PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, 
				PickOrderLine.VariantSize as VariantSize, VariantCode.VariantModelRef as VariantModelRef,
				PickOrderLine.OrderedQuantity as OrderedQuantity, PickOrderLine.PackedQuantity as PackedQuantity, 
				PickOrderLine.PickedQuantity as PickedQuantity
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId  and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
		 LEFT JOIN Item on Item.containerid=Container.Id and item.articleid=variantcode.articleid and item.articlecolorid=variantcode.articlecolorid and item.articlesizeid=variantcode.articlesizeid and Item.active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity 
		 GROUP BY VariantCode.id ORDER BY `Position`, variantcode.articleid, variantcode.articlecolorid, variantcode.articlesizeid", $id) ;

	    $query = 
	     sprintf ("SELECT if((Container.`Position` is null) or (Container.`Position`=''), 
	                               concat(if(VariantCode.PickContainerId>0,'X-NoPos+C','X-NoPos'),''), Container.`Position`
	                               ) AS `Position`,
							(select sum(Item.Quantity) from Item where Item.containerid=Container.Id and item.articleid=variantcode.articleid and item.articlecolorid=variantcode.articlecolorid and item.articlesizeid=variantcode.articlesizeid and Item.active=1) as ItemQty,
							group_concat(PickOrderLine.Id) AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, 
							Article.Number as ArticleNumber, VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, 
							PickOrderLine.VariantCodeId AS VariantCodeId,
							
							PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, 
							PickOrderLine.VariantSize as VariantSize, VariantCode.VariantModelRef as VariantModelRef,
							PickOrderLine.BoxNumber as BoxNumber, 
							sum(PickOrderLine.OrderedQuantity) as OrderedQuantity, 
							sum(PickOrderLine.PackedQuantity) as PackedQuantity, 
							sum(PickOrderLine.PrePackedQuantity) as PrePackedQuantity, 
							sum(PickOrderLine.PickedQuantity) as PickedQuantity,
							VariantCode.SKU,
							VariantCode.VariantCode
					FROM (PickOrderLine)
					LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId  and variantcode.active=1
					LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
					LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
					WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity 
					GROUP BY VariantCode.id 
					ORDER BY `Position`, variantcode.articleid, variantcode.articlecolorid, variantcode.articlesizeid", $id) ;



		 	$Result = dbQuery ($query) ;


		 	$query = sprintf ("SELECT PickOrder.* , o.CarrierId as CarrierId, o.WebshipperId, o.Id AS OrderId, ss.Name as ShopifyName, o.Reference as OrderReference, o.Email as oEmail, o.Phone as oPhone, o.ShippingShop
							FROM (PickOrder) 
							LEFT JOIN `order` o ON PickOrder.referenceid=o.id
							LEFT JOIN shopifyshops ss ON ss.Id=o.WebShopNo
							WHERE PickOrder.Active=1 and  PickOrder.Id=%d", $id) ;

	    	$res = dbQuery ($query) ;
	    	$Record = dbFetch ($res) ;
	    	dbQueryFree ($res) ;

	    	$prefix = $Record['ShopifyName'];
	    	$pdf->StartPageGroup();
		 	$pdf->AddPage() ;    
	    $Street = ''; 
	    $hl=0 ;
	    $_oneQtyPerLine=0 ;
	    $pdf->setDrawColor(150,150,150);
	    if ($xmas) {
	    	$lineheight = 7;
	    } else {
	    	$lineheight = 8;
	    }
	    while ($row = dbFetch($Result)) {
	      $dl = 45;
	      	$qty = $row['OrderedQuantity']-$row['PickedQuantity']-$row['PackedQuantity'];
	      	while ($qty > 0) {
		      	$pdf->Cell(35, $lineheight, $row['VariantCode'],1) ;
				$_desc = $row['VariantDescription'] . ' ' . $row['VariantColor'] . ' / ' . $row['VariantSize'];
				$pdf->Cell(95, $lineheight, substr($_desc,0,$dl),1) ;
				$pdf->Cell(25, $lineheight, $row['Position'],1) ;
				if ($_oneQtyPerLine==1) {
					$pdf->Cell(25, $lineheight, 1, 1, 0, 'C') ;
					$pdf->Ln () ;	
					$qty--;
				} else {
					$pdf->Cell(25, $lineheight, $qty, 1, 0, 'C') ;
					$pdf->Ln () ;	
					$qty=0;
				}
	      	}
	    }


	    $pdf->Ln () ;
	    if ($xmas) {
	    	$noHeaderInfo = true;
	    }
	 	
	    if($headerInformation[$prefix]['header1'] != ''){
	    	$pdf->SetFont('Arial','B',11);
	    	$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['header1']), 0,'C') ;
	    	$pdf->SetFont('Arial','',11);
	    }
	    if($headerInformation[$prefix]['footer1'] != ''){
	    	$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['footer1']), 0,'C') ;
	    }
	    if($headerInformation[$prefix]['header2'] != ''){
	    	$pdf->Ln () ;
	    	$pdf->SetFont('Arial','B',11);
	    	$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['header2']), 0,'C') ;
	    	$pdf->SetFont('Arial','',11);
	    }
	    else{
	    	$pdf->Ln () ;	
	    }
	    if($headerInformation[$prefix]['footer2'] != ''){
	    	$pdf->MultiCell(180, 4.5, ($headerInformation[$prefix]['footer2']), 0,'C') ;
	    }
	    if($headerInformation[$prefix]['header3'] != ''){
	    	$pdf->Ln () ;
	    	$pdf->SetFont('Arial','B',11);
	    	$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['header3']), 0,'C') ;
	    	$pdf->SetFont('Arial','',11);
	    }
	    else{
	    	$pdf->Ln () ;	
	    }
	    if($headerInformation[$prefix]['footer3'] != ''){
	    	$pdf->MultiCell(180, 4.5, ($headerInformation[$prefix]['footer3']), 0,'C') ;
	    }
	    if($headerInformation[$prefix]['header4'] != ''){
	    	$pdf->Ln () ;
	    	$pdf->SetFont('Arial','B',11);
	    	$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['header4']), 0,'C') ;
	    	$pdf->SetFont('Arial','',11);
	    }
	    else{
	    	$pdf->Ln () ;	
	    }
	    if($headerInformation[$prefix]['footer4'] != ''){
		    $pdf->MultiCell(180, 4.5, ($headerInformation[$prefix]['footer4']), 0,'C') ;
			}
			if($headerInformation[$prefix]['header5'] != ''){
				$pdf->Ln () ;
				$pdf->SetFont('Arial','B',11);
		  	$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['header5']), 0,'C') ;
		  	$pdf->SetFont('Arial','',11);
		  }
		  else{
		  	$pdf->Ln () ;	
		  }
			if($headerInformation[$prefix]['footer5'] != ''){
			    $pdf->MultiCell(180, 4.5, ($headerInformation[$prefix]['footer5']), 0,'C') ;
			}
			if ($xmas) {
				if($headerInformation[$prefix]['header6'] != ''){
					$LastPage = true;
					$noHeaderInfo = false;
	            	$pdf->AddPage();
	            	$LastPage = false;
					$pdf->Ln () ;

					//xmas
					if($headerInformation[$prefix]['footer8'] != ''){
		                $pdf->SetFont('Arial','B',16);
		                $currY = $pdf->getY();
		                $pdf->setY($currY-26);
		                $pdf->MultiCell(180, 18, utf8_decode($headerInformation[$prefix]['footer8']), 0,'L') ;
		                $pdf->setY($currY);
		            }

					$pdf->SetFont('Arial','B',11);
			  		$pdf->MultiCell(180, 4, ($headerInformation[$prefix]['header6']), 0,'C') ;
			  		$pdf->SetFont('Arial','',11);
			  } else{
			  		$pdf->Ln () ;	
			  }
			}
		if ($xmas) {
			if($headerInformation[$prefix]['footer6'] != ''){
			    //$pdf->Ln () ;
			    $pdf->MultiCell(180, 4.5, ($headerInformation[$prefix]['footer6']), 0,'C') ;
			}

			if($headerInformation[$prefix]['header7'] != ''){
	            $pdf->Ln () ;
	            $pdf->SetFont('Arial','B',11);
	        	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header7']), 0,'C') ;
	        	$pdf->SetFont('Arial','',11);
	        } else {
	        	$pdf->Ln () ;
	      	}
		}	
		
		// Update PickOrder.
		$PickOrder = array (
			'Printed' => 1,
			'Updated' => 0
		) ;
		tableWrite ('PickOrder', $PickOrder, (int)$id) ;

	}
    // Last Page generated
    $LastPage = True ;
    	
    // Generate PDF document
    $pdfdoc = $pdf->Output('', 'S') ;

    // Download
    if (headers_sent()) return 'stop' ;
    httpNoCache ('pdf') ;
    httpContent ('application/pdf', sprintf('PickOrder%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
    print ($pdfdoc) ; 

	dbQueryFree ($Result) ;
	


    return 0 ;
?>
