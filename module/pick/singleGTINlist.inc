<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHead ('Id', 40)  ;
    listHead ('Reference', 50)  ;
    listHead ('Company', 150) ;
    listHead ('Customer', 100) ;
    listHead ('Country', 50) ;
    listHead ('Date', 80) ;
    listHead ('Instructions', 100) ;
    listHead ('Variant', 100) ;
    listHead ('VariantDescription', 300) ;
    listHead ('Batch', 40) ;
    listHeadIcon () ;
    listHead ('', 60) ;

    while ($row = dbFetch($Result)) {
        listRow () ;
        listField ($row["PickOrderId"]) ;
        listField ($row["Reference"]) ;
        listField ($row["CompanyName"]) ;
        listField ($row["DeliveryCompanyName"]) ;
        listField ($row["DeliveryCountryName"]) ;
        listField (date('Y-m-d', dbDateDecode($row['DeliveryDate']))) ;
        listField ($row["Instructions"]) ;
        listField ($row["VariantCode"]) ;
        listField ($row["VariantDescription"]) ;
        listField (($row["BatchId"]>0?$row["BatchId"]:'')) ;  // . ' - ' . $row["PickQty"]
        listField ('') ;
        listField ('') ;
    }

    listRow () ;
    listField ('', 'colspan=5 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['PackQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;  // Quantity
    listField ('', 'colspan=2 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['GtinPackQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;  // Quantity
//    listField ($total['LineQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;    // Quantity
    listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
