<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
	listHead ('Batch', 50)  ;
    listHead ('Variant', 120)  ;
    listHead ('Order Qty', 80, 'align="right"') ;
    listHead ('State', 200) ;
	listHead('') ;
//    listHeadIcon () ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
		if ($row["BatchId"]>0)
			listFieldIcon ('pack.png', 'packbatchview', (int)$row["BatchId"]) ;
		else
			listFieldIcon ('add.gif', 'packbatchnew', (int)$row["VariantCodeId"]) ;
    	listField ($row["BatchId"]) ;
    	listField ($row["VariantCode"]) ;
    	listField ($row["QrderQty"], 'align="right"') ;
    	listField ($row["QrderQty"]>$row["PackedQty"]? "Missing " . ($row["QrderQty"]-$row["PackedQty"]) . " to complete batch": "Complete") ;
    	listField ('') ;
//		listFieldIcon ('print.gif', 'packbatchprint', (int)$row["BatchId"]) ;

    }

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
