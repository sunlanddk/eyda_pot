<script type="text/javascript" >
	function CheckAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			e.checked = true ;
		}
	}
</script>
<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;

	itemStart() ;
//    itemFieldRaw ('Gross Weight', formText ('GrossWeight', '', 10, 'text-align:right;') . ' kg') ;
//	itemField('Variant', $Record['VariantCode'] . ' - ' . $Record['VariantDescription']) ;
//	itemField('PickPosition', $Record['Position']) ;
	itemSpace() ;
	itemEnd() ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Id', 30)  ;
    listHead ('Reference', 100)  ;
    listHead ('Variant', 100) ;
    listHead ('VariantDescription', 200) ;
    listHead ('Customer', 150) ;
    listHead ('Country', 50) ;
    listHead ('Date', 80) ;
    listHead ('Batch', 40) ;
    listHeadIcon () ;
    listHead ('', 60) ;

    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldRaw (formCheckbox (sprintf('PrintFlag[%d]', (int)$row['PickOrderId']), 0, '')) ;
    	listField ($row["Reference"]) ;
    	listField ($row["VariantCode"]) ;
    	listField ($row["VariantDescription"]) ;
    	listField ($row["DeliveryCompanyName"]) ;
    	listField ($row["DeliveryCountryName"]) ;
    	listField (date('Y-m-d', dbDateDecode($row['DeliveryDate']))) ;
    	listField ($row["BatchId"]>0?$row["BatchId"]:'') ;
    	listField ('') ;
    	listField ('') ;
    }

    listRow () ;
    listField ('', 'colspan=5 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['PackQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ('', 'colspan=2 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['GtinPackQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
//    listField ($total['LineQuantity'], 'align=right style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;

    listEnd () ;
    dbQueryFree ($Result) ;
    formEnd () ;
   
    return 0 ;
?>
