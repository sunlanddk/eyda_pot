<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Order', 100)  ;
    listHeadIcon () ;
//    listHead ('Owner', 120) ;
    listHead ('Customer', 200) ;
//    listHead ('Number', 60) ;
    listHead ('Country', 40) ;
    listHead ('Delivery', 80) ;
    listHead ('Total Qty', 70) ;
    listHead ('Packed', 60) ;
    listHead ('Pick Lines', 70) ;
    listHead ('State', 60) ;
    listHead ('', 60) ;
 //   listHead ('', 60) ;
 //   listHead ('ImportState') ;
 //   listHead ('Last Depart', 70) ;
 //   listHead ('Picker', 95) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
		listFieldIcon ('pack.png', 'packlist', (int)$row['Id']) ;
//    	listField ($row["OrderReference"].' ('.$row["Reference"].')') ;
        listField ($row["OrderReference"]) ;
    	listFieldIcon ('cart.gif', 'picklist', $row['Id']) ;
//    	listField ($row["OwnerCompanyName"]) ;
    	listField ($row["CompanyName"] . ' ' .$row["OrderReference"]) ;
//    	listField ($row["CompanyNumber"]) ;
    	listField ($row["CountryName"]) ;
    	listField (date('Y-m-d', dbDateDecode($row['DeliveryDate']))) ;
    	listField ($row["Qty"]) ;
    	listField ($row["PackedQty"]) ;
    	listField ($row["NoLines"]) ;
        listField ($row["State"]) ;
        listField ('') ;
/*
      listField ($row["ImportError"]) ;
		if ($row['DepartedDate']>'2001-01-01')
			listField (date('Y-m-d', dbDateDecode($row['DepartedDate']))) ;
		else
			listField ('') ;
    	listField ($row["PickUser"]) ;
*/
	$total['PackQuantity'] += $row["Qty"] ;
	$total['PackedQuantity'] += $row["PackedQty"] ;
	$total['LineQuantity'] += $row["NoLines"] ;
    }

    listRow () ;
    listField ('', 'colspan=7 style="border-top: 1px solid #cdcabb;"') ;
    listField ($total['PackQuantity'], 'align=left style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ($total['PackedQuantity'], 'align=left style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ($total['LineQuantity'], 'align=left style="border-top: 1px solid #cdcabb;"') ;	// Quantity
    listField ('', 'colspan=1 style="border-top: 1px solid #cdcabb;"') ;

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
