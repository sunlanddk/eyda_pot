<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'lib/csvftp.inc' ;

	global $User, $Navigation, $Record, $Size, $Id, $csv_data_list ;

    switch ($Navigation['Parameters']) {
		case 'season' :
			// First create product data to Alpi.
			$query = sprintf ("Select 
									Variantcode.id as Varenummer,
									concat(article.description,' - ', Color.Description, ' - ', ArticleSize.Name) as Beskrivelse,
									Variantcode.variantcode as EAN
								FROM
									(Collection, CollectionMember)
													 LEFT JOIN VariantCode ON CollectionMember.articleid=VariantCode.articleid AND CollectionMember.articleColorid=VariantCode.articleColorid AND VariantCode.Active=1
													 LEFT JOIN Article ON CollectionMember.ArticleId=Article.Id and Article.Active=1 
													 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
													 LEFT JOIN ArticleColor ON CollectionMember.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
													 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
								WHERE
									Collection.SeasonId=%d AND CollectionMember.CollectionId=Collection.Id AND CollectionMember.Active=1", $Id) ;
			// Target filename
			$_datetime = date('Ymd_His'); 
			$ftp_file = 'ITEM_' . $_datetime . '_' . $Id .  '.txt';
			if ($_res = CreateCSV($query, $ftp_file)) {
				echo 'Information transfer failed, Close and try again later.';
			} else {
				$_season = array (
					VarListTransferTime => dbDateEncode(time()) 
				);
				tableWrite('Season', $_season, $Id) ;
				echo 'Information transfer succeeded, you can now close the window.<br><br>';
				
				echo $csv_data_list ;
			}
			break; 
		case 'po' :
			// First create product data to Alpi.
			$query = sprintf ("Select 
									Variantcode.id as Varenummer,
									concat(ArticleSize.Name,' - ', Color.Description, ' - ', article.description) as Beskrivelse,
									Variantcode.variantcode as EAN,
									Article.Number
								FROM
									(requisitionline rl)
													 LEFT JOIN VariantCode ON VariantCode.articleid=rl.articleid and VariantCode.articleColorid=rl.articleColorid AND VariantCode.Active=1
													 LEFT JOIN Article ON rl.ArticleId=Article.Id and Article.Active=1 
													 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
													 LEFT JOIN ArticleColor ON rl.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
													 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
								WHERE
									rl.RequisitionId=%d AND rl.Active=1
								GROUP BY VariantCode.articleid, VariantCode.articlecolorid, VariantCode.articlesizeid
							  ", $Id) ;
			// Target filename
			$_datetime = date('Ymd_His'); 
			$ftp_file = 'ITEM_' . $_datetime . '_PO' . $Id .  '.txt';
			if ($_res = CreateCSV($query, $ftp_file)) {
				echo 'Information transfer failed, Close and try again later.';
			} else {
				// Next create received PO  data to Alpi.
				$query = sprintf ("Select 
										rq.id as LinjeID,
										rl.RequisitionId as ordrenummer,
										Variantcode.id as Varenummer,
										format(rq.quantityreceived-rq.quantityprepacked,0) as Antal
									FROM
										(requisitionline rl, requisitionlinequantity rq)
									LEFT JOIN VariantCode ON VariantCode.articleid=rl.articleid and VariantCode.articleColorid=rl.articleColorid and VariantCode.articlesizeid=rq.articlesizeid AND VariantCode.Active=1
									WHERE
										rl.RequisitionId=%d AND rl.Active=1 AND rq.requesitionlineid=rl.id and rq.quantityreceived>0 and rq.active=1
								  ", $Id) ;
				// Target filename
				$_datetime = date('Ymd_His'); 
				$ftp_file = 'PURCHASE_' . $_datetime . '_PO' . $Id .  '.txt';
				if ($_res = CreateCSV($query, $ftp_file)) {
					echo 'Information transfer failed, Close and try again later.';
				} else {
					$_record = array (
						'VarListTransferTime' => dbDateEncode(time()) 
					);
					tableWrite('requisition', $_record, $Id) ;
					echo 'Information transfer succeeded, you can now close the window.<br><br>';
					
					echo $csv_data_list ;
				}
			}
			break; 
		case 'soprepack' :
		case 'sopick' :
			if ($Navigation['Parameters']=='soprepack') {
				$_qtyvalue = 'oq.quantityreceived' ;
				$_whereclause = 'and oq.quantityreceived>0 and oq.boxnumber>0' ;
			} else {
				$_qtyvalue = 'oq.quantity' ;
				$_whereclause = '' ;
			}
			// Create received prepacked qty from SO and send data to Alpi as incomming (unpack to stock).
			$query = sprintf ("Select 
										oq.id as LinjeID,
										ol.OrderId as ordrenummer,
										Variantcode.id as Varenummer,
										format(%s,0) as Antal
									FROM
										(`order` o, orderline ol, orderquantity oq)
									LEFT JOIN VariantCode ON VariantCode.articleid=ol.articleid and VariantCode.articleColorid=ol.articleColorid and VariantCode.articlesizeid=oq.articlesizeid AND VariantCode.Active=1
									WHERE
										o.consolidatedid=%d and ol.OrderId=o.id AND ol.Active=1 AND oq.orderlineid=ol.id %s and oq.active=1  and ol.active=1 and o.active=1
							  ", $_qtyvalue, $Id, $_whereclause) ;
//									inner Join article ON article.id=ol.articleid and article.suppliercompanyid=6656
			// Target filename
			$_datetime = date('Ymd_His'); 
			$ftp_file = 'PURCHASE_' . $_datetime . '_SO' . $Id .  'Unpack.txt';
			if ($_res = CreateCSV($query, $ftp_file)) {
				return 'Information transfer failed, Close and try again later.';
			} else {
				$_record = array (
					'LogText' => 'Unpacked ' . $Navigation['Parameters'] . ' ' . dbDateEncode(time()) 
				);
				tableWrite('order', $_record, $Id) ;

				$result = dbQuery ($query) ;
				while ($row = dbFetch ($result)) {
					$_record = array (
						'QuantityReceived' => 0,
						'BoxNumber' => 0
					);
					tableWrite('orderquantity', $_record, $row['LinjeID']) ;
				}
				dbQueryFree ($result) ;
				
				$_txt = 'Information transfer to Alpi succeeded and prepacked data reset, you can now close the window.<br><br>';
				
				$_txt .= $csv_data_list ;
			}
			return $_txt ;
		break; 
		default:
			return 'invalid navigation parameter ' . $Navigation['Parameters'] ;
	}
	

?>
