<?php

	global $User, $Navigation, $Record, $Size, $Id ;

	function array_2_csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {

//				$p = ereg_replace("\r\n?" "\n", $item);
//				$p = str_replace("\n", ' ', $p);
//				$csv[] = $p;
				if (substr($item,-1)==',') $item=substr($item,0,-1) ;
				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
	
//  (select BoxNumber from variantcode vc, orderquantity oq, orderline ol where vc.id=pl.variantcodeid AND ol.orderid=o.id and ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.orderlineid=ol.id and oq.articlesizeid=vc.articlesizeid) as BoxNumber,
	function CreatePickConsolidatedCSV($_orderid) {
		global $User, $Navigation, $Record, $Size, $Id, $csv_data_list, $Config ;

		$query = sprintf ("SELECT pl.id as LineId, o.id as OrderId, 
								c.name as Carrier, 
								co.name as CompanyName, 	
								o.Address1 as DelCompanyName, o.Address2 as DelAdr, o.zip as delzip, o.city as delcity, '' as delstate, oc.DisplayName as delcountry,
								'' as PickedUpBy,
								pl.orderedquantity,
								concat(VariantSize, ' - ', VariantColor, ' - ', VariantDescription) as Description, 
								pl.variantcodeid as SKU, 
								if(isnull(o.Email),co.Mail,o.Email) as Email, 
								if(isnull(o.Phone),co.PhoneMain,o.Phone) as Phone, 
								pl.variantcode as EAN, 
								o.ShippingShop as shippingshop,
								p.Instructions,
								pl.BoxNumber as BoxNumber
							FROM (pickorder p, pickorderline pl, `order` o, company co, country oc, country cc)
							LEFT JOIN carrier c ON c.id=o.carrierid
							where
							p.ConsolidatedId=%d and p.active=1
							and pl.pickorderid=p.id and pl.active=1  and pl.orderedquantity>0
							and o.Id=p.referenceid and p.packed=0
							and co.id=o.companyid and oc.id=o.countryid  and cc.id=co.countryid
							ORDER BY pl.id", $_orderid) ;
		$result = dbQuery ($query) ;
//	return ($query) ;
		$_datetime = date('Ymd_His'); 
		$ftp_file = 'SALES_' . $_datetime . '_SO' . $_orderid .  '.txt';
		$local_file = $Config['fileStore'] .'/export/' . $ftp_file ; //'order.txt';
		$file = fopen($local_file,"w");
		$n = 0 ;
		while ($row = dbFetch ($result)) {
			if ((int)$row['BoxNumber'] > 0) {
				$row['SKU'] = 100 ;
//				$_tmp = sprintf('%09d', $row['BoxNumber']) ;
//				$row['EAN'] = '0005712199' . $_tmp  . '0' ;
				$row['EAN'] = sprintf('%d', $row['BoxNumber']) ;
				$row['Description'] = 'Prepacked Box' ;
				$row['orderedquantity'] = 1 ;
				$boxes[$row['BoxNumber']] = $row;
			} else {
				$n++ ;  
				$row['BoxNumber'] = '' ;
				$csv_data = array_2_csv($row);
				//		$csv_data .= "\r\n" ;
				fwrite($file,$csv_data.PHP_EOL);
				//		print_r($csv_data);
			}
		}
		foreach ($boxes as $boxnumber => $row) {
			$n++ ;  
			$csv_data = array_2_csv($row);
			fwrite($file,$csv_data.PHP_EOL);
		}
		
		dbQueryFree ($result) ;
		fclose($file) ;

		if ($Config['Instance'] == 'Test') return 0 ; // dont transfer file if test instance.
		return 0 ;
		//return $csv_data . ' ' . $n ;	

		// FTP access parameters
		$host = 'alpiftp.alpi.dk';
		$usr = 'fub';
		$pwd = 'duJS72CD';
		 
		// file to move:
		$ftp_path = '/prod/inbox/' . $ftp_file ;
		 
		// connect to FTP server (port 21)
		$conn_id = ftp_connect($host, 21) or die ("Cannot connect to host");
		 
		// send access parameters
		ftp_login($conn_id, $usr, $pwd) or die("Cannot login");
		 
		// turn on passive mode transfers (some servers need this)
		ftp_pasv ($conn_id, true);
		 
		// perform file upload
		$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_ASCII);
		 
		// check upload status:
		if (!$upload) print('Cannot upload ' . ftp_path) ;
//		print (!$upload) ? 'Cannot upload: ' . $upload : 'Upload complete';
//		print "\n";
		 
		/*
		** Chmod the file (just as example)
		*/
		 
		// If you are using PHP4 then you need to use this code:
		// (because the "ftp_chmod" command is just available in PHP5+)
		if (!function_exists('ftp_chmod')) {
		   function ftp_chmod($ftp_stream, $mode, $filename){
				return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
		   }
		}
		 
		// try to chmod the new file to 666 (writeable)
		if (ftp_chmod($conn_id, 0666, $ftp_path) !== false) {
			$_prut = 1 ;
//			print $ftp_path . " chmod successfully to 666<br>";
		} else {
			print "could not chmod $file\n";
		}
		 
		// close the FTP stream
		ftp_close($conn_id);
		
//		print ('Order csv file succesfully created<br>') ;
		return 0 ;	
	}
	function CreatePickCSV($_orderid) {
		global $User, $Navigation, $Record, $Size, $Id, $csv_data_list, $Config ;

		$query = sprintf ("SELECT pl.id as LineId, o.id as OrderId, 
								c.name as Carrier, 
							co.name as CompanyName, 
								o.Address1 as DelCompanyName, o.Address2 as DelAdr, o.zip as delzip, o.city as delcity, '' as delstate, oc.DisplayName as delcountry,
								'' as PickedUpBy,
								pl.orderedquantity,
								concat(VariantSize, ' - ', VariantColor, ' - ', VariantDescription) as Description, 
								pl.variantcodeid as SKU, 
								if(isnull(o.Email),co.Mail,o.Email) as Email, 
								if(isnull(o.Phone),co.PhoneMain,o.Phone) as Phone, 
								pl.variantcode as EAN, 
								o.ShippingShop as shippingshop,
								p.Instructions,
								pl.BoxNumber as BoxNumber
							FROM (pickorder p, pickorderline pl, `order` o, company co, country oc, country cc)
							LEFT JOIN carrier c ON c.id=o.carrierid
							where
							p.referenceid=%d and p.active=1
							and pl.pickorderid=p.id and pl.active=1  and pl.orderedquantity>0
							and o.id=p.referenceid 
							and co.id=o.companyid and oc.id=o.countryid  and cc.id=co.countryid
							ORDER BY pl.id", $_orderid) ;
		$result = dbQuery ($query) ;
//	return ($query) ;
		$_datetime = date('Ymd_His'); 
		$ftp_file = 'SALES_' . $_datetime . '_SO' . $_orderid .  '.txt';
		$local_file = $Config['fileStore'] .'/export/' . $ftp_file ; //'order.txt';
		$file = fopen($local_file,"w");
		$n = 0 ;
		while ($row = dbFetch ($result)) {
			if ((int)$row['BoxNumber'] > 0) {
				$row['SKU'] = 1 ;
//				$_tmp = sprintf('%09d', $row['BoxNumber']) ;
//				$row['EAN'] = '0005712199' . $_tmp  . '0' ;
				$row['EAN'] = sprintf('%d', $row['BoxNumber']) ;
				$row['Description'] = 'Prepacked Box' ;
				$row['orderedquantity'] = 1 ;
				$boxes[$row['BoxNumber']] = $row;
			} else {
				$n++ ;  
				$row['BoxNumber'] = '' ;
				$csv_data = array_2_csv($row);
				//		$csv_data .= "\r\n" ;
				fwrite($file,$csv_data.PHP_EOL);
				//		print_r($csv_data);
			}
		}
		foreach ($boxes as $boxnumber => $row) {
			$n++ ;  
			$csv_data = array_2_csv($row);
			fwrite($file,$csv_data.PHP_EOL);
		}
		
		dbQueryFree ($result) ;
		fclose($file) ;

		if ($Config['Instance'] == 'Test') return 0 ; // dont transfer file if test instance.
return 0;
		//return $csv_data . ' ' . $n ;	

		// FTP access parameters
		$host = 'alpiftp.alpi.dk';
		$usr = 'fub';
		$pwd = 'duJS72CD';
		 
		// file to move:
		$ftp_path = '/prod/inbox/' . $ftp_file ;
		 
		// connect to FTP server (port 21)
		$conn_id = ftp_connect($host, 21) or die ("Cannot connect to host");
		 
		// send access parameters
		ftp_login($conn_id, $usr, $pwd) or die("Cannot login");
		 
		// turn on passive mode transfers (some servers need this)
		ftp_pasv ($conn_id, true);
		 
		// perform file upload
		$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_ASCII);
		 
		// check upload status:
		if (!$upload) print('Cannot upload ' . ftp_path) ;
//		print (!$upload) ? 'Cannot upload: ' . $upload : 'Upload complete';
//		print "\n";
		 
		/*
		** Chmod the file (just as example)
		*/
		 
		// If you are using PHP4 then you need to use this code:
		// (because the "ftp_chmod" command is just available in PHP5+)
		if (!function_exists('ftp_chmod')) {
		   function ftp_chmod($ftp_stream, $mode, $filename){
				return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
		   }
		}
		 
		// try to chmod the new file to 666 (writeable)
		if (ftp_chmod($conn_id, 0666, $ftp_path) !== false) {
			$_prut = 1 ;
//			print $ftp_path . " chmod successfully to 666<br>";
		} else {
			print "could not chmod $file\n";
		}
		 
		// close the FTP stream
		ftp_close($conn_id);
		
//		print ('Order csv file succesfully created<br>') ;
		return 0 ;	
	}
	
//	CreatePickCSV($Id) ;

?>
