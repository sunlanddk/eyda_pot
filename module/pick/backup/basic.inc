<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, 
	    sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), 
	    tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }
    function flagRevert (&$Record, $field) {
	if ($Record[$field]) {
	    itemFieldRaw ($field, 
					   formCheckbox ($field, 1, 'margin-left:4px;') . date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])) . ', ' . 
					                       tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', 
					                       $Record[$field.'UserId'])) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    // Default values
	    $Record['DeliveryDate'] = dbDateEncode(time()) ;
	    $Record['OwnerCompanyId'] = $User['CompanyId'] ;
	    break ;
	case 'pick' : 
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('PickOrder', $Record['Reference']) ;
	    itemSpace () ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;


    switch ($Navigation['Parameters']) {
	case 'new' :
	    break ;
    }
    itemFieldRaw ('Picker', formDBSelect ('PickUserId', (int)$Record['PickUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.TypeSupplier=1 AND Company.Active=1 AND User.CompanyId=Company.id AND User.Active=1 AND User.Login=1 ORDER BY Value'), 'width:250px;', array(0 => '--none--'))) ;  
    itemSpace () ;
    itemFieldRaw ('Delivery', formDate ('DeliveryDate', dbDateDecode($Record['DeliveryDate']))) ;
    itemSpace () ;
    flagRevert ($Record, 'Packed') ;
    flagRevert ($Record, 'Picked') ;

	itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
