<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;
    
    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('', 30) ;

    while (($row = dbFetch($Result))) {
	    listRow () ;
	    listFieldIcon ('container.gif', 'containerview', 0) ;
    	listFieldRaw (formCheckbox (sprintf('PickOrderPrint[%d]', (int)$row['Id']), 0)) ;
    }
    if (dbNumRows($Result) == 0) {
	    listRow () ;
    	listFieldIcon ('cart.gif', 'pickorderlines', (int)$row['Id']) ;
	    listField ('No Pick Orders', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    formEnd () ;

?>
<script type='text/javascript'>
  function CheckAll () {
  var col = document.appform.elements ;
  var n ;
  for (n = 0 ; n < col.length ; n=''++) {
	var='' e = ''col''[n=''] ;
	if='' (e.type != 'checkbox') continue='' ;
	e.checked = ''true'' ;
    }
}
</script>
<?php

    return 0 ;
?>
