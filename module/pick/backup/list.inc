<?php

    require "lib/dropmenu.inc" ;

    if (dbNumRows($Result) == 0) return "No lines to pack" ;
    
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("%s", dropmenuTDHeader()) ;
    printf ("<td class=listhead width=90><p class=listhead>Position</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Variant</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Color</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Size</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Ordered Qty</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Prev Packed</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Scanned</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Remaining</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("%s", dropmenuTDList($row["Id"], 'image/toolbar/user.gif')) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Position"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["VariantCode"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["VariantDescription"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["VariantColor"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["VariantSize"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["OrderedQuantity"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["PackedQuantity"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["PickedQuantity"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["OrderedQuantity"]-$row["PackedQuantity"]-$row["PickedQuantity"])) ;
	printf ("</tr>\n") ;
    }
    printf ("</table>") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
