<?php

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    switch ($Navigation['Function']) {
	case 'ziplist' :
		$query = "SELECT * FROM PickOrder WHERE Active=1 AND Id=1" ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id=1 ;
		$Record['Id']=1 ;
		
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;

 	case 'setprint' :
	    // Overload the Navigation Parameter with the one supplied in the URL
 		$_navigationParam = $Navigation['Parameters'] ;
        if (($Navigation['Parameters']=='all') or ($Navigation['Parameters']=='norway'))
            $showAll = 1;
        else 
            $showAll = 0;
	    $Navigation['Parameters'] = $_GET['param'] ;

 	case 'list' :
		// List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Order',			'db' => 'o.Reference'),
			array ('name' => 'Owner',			'db' => 'oc.Name'),
			array ('name' => 'Customer',		'db' => 'oa.AltCompanyName'),
//			array ('name' => 'CustomerNumber',	'db' => 'c.Number'),
			array ('name' => 'Country',			'db' => 'ct.Name'),
			array ('name' => 'Delivery',		'db' => 'po.DeliveryDate'),
//			array ('name' => 'State',			'db' => 'po.state')
	    ) ;

//if(po.Packed>0,"Done",if(po.ToId>0,if(s.ready>0,"Part shipped","Packing"),if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")))) as State,   
	    require_once 'lib/list.inc' ;
		$queryFields =  'po.Id as Id,CONCAT(u.FirstName," ",u.LastName," (",u.Loginname,")") as PickUser, 
					if(po.Packed>0,"Done", if (po.updated, "Updated", if(po.printed>0, if(po.ToId>0,	
					if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId=0,"","Assigned"))))) as State, 
					po.Reference as Reference, po.ImportError as ImportError,
					oa.AltCompanyName as CompanyName, c.Number as CompanyNumber, ct.Name as CountryName,
					oc.Name as OwnerCompanyName, po.DeliveryDate as DeliveryDate, count(pol.id) as NoLines, 
					if (po.ToId>0,DepartedDate,"2001-01-01") as DepartedDate, 
					sum(pol.PackedQuantity) as PackedQty, o.reference as OrderReference,
					sum(pol.OrderedQuantity) as Qty' ;
	    
		$queryTables = '(PickOrder po, PickOrderLine pol, Company c, Company oc )
					LEFT JOIN Stock s ON po.toid=s.id
					LEFT JOIN User u ON po.PickUserId=u.Id
					LEFT JOIN `Order` o On o.id=po.referenceid
 					LEFT JOIN orderadresses oa ON oa.Active=1 AND oa.Type="shipping" AND oa.OrderId=o.Id
					LEFT JOIN Country ct ON oa.countryid=ct.id
					' ;


	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Specific season
		    $query = sprintf ('SELECT season.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Season, Company WHERE Season.Id=%d AND Season.CompanyId=Company.Id AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
//		    if ($Record['TypeCustomer']) {
//			navigationEnable ('new') ;
//		    }

		    $queryTables = $queryTables . 
					 ' LEFT JOIN `Order` ON `Order`.Id=po.ReferenceId' ;
				
		    // Clause
		    $queryClause = sprintf ('`Order`.SeasonId=%d', $Id) ;
				$queryClause = sprintf ('po.Active=1 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and pol.Active=1 and po.Active=1 and `Order`.SeasonId=%d
						GROUP BY po.Id', $Id) ;
		    break ;

		default :
			if ($Navigation['Parameters']=='norway' or $_navigationParam=='norway') {
				$_queryClauseAdd=' and oa.countryid=15';
			}  else {
				$_queryClauseAdd=' and !(oa.countryid=15)';
			}
			if ($Navigation['Function'] == 'list' or $Navigation['Function'] == 'setprint') {
				$queryClause = sprintf ('po.Active=1 And po.Packed=0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and po.handlerCompanyId=%d and pol.Active=1 and po.Active=1 %s
						GROUP BY po.Id', $User['CompanyId'], $_queryClauseAdd) ;
				// Navigation
				// Pass all parameters for the list function to the zip function
				$param = 'param=' . $Navigation['Parameters'] ;
				if ($listParam != '') $param .= '&' . $listParam ;
				navigationSetParameter ('ziplist', $param) ;
				navigationSetParameter ('setprint', $param) ;
			} else {
				$queryClause = sprintf ('po.Active=1 And po.ToId>0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id 
						and po.OwnerCompanyId=oc.Id and po.handlerCompanyId=%d and pol.Active=1 and po.Active=1
						GROUP BY po.Id', $User['CompanyId']) ;
			}
		    break ;
	    }

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $query = 
	     sprintf ("SELECT Container.`Position` AS `Position`, PickOrderLine.Id AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, 
				Article.Number as ArticleNumber, PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, PickOrderLine.VariantSize as VariantSize,
				PickOrderLine.OrderedQuantity as OrderedQuantity, PickOrderLine.PackedQuantity as PackedQuantity, PickOrderLine.PickedQuantity as PickedQuantity
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity ORDER BY Container.`Position`", $Id) ;
	    $Result2 = dbQuery ($query) ;
    
	    $query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    return listView ($Result2, 'packlist') ;
	case 'varview' :
		$query = sprintf ("SELECT * FROM VariantCode WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		return 0 ;

	case 'varlist' :
	    $DescriptionField = 'if (VariantCode.VariantDescription="" or isnull(VariantCode.VariantDescription), Article.Description, VariantCode.VariantDescription)' ;
	    $ColorField = 'if (VariantCode.VariantColorDesc="" or isnull(VariantCode.VariantColorDesc), Color.Description,VariantCode.VariantColorDesc)' ;
	    $ColorCodeField = 'if (VariantCode.VariantColorCode="" or isnull(VariantCode.VariantColorCode), Color.Number,VariantCode.VariantColorCode)' ;
	    $SizeField = 'if (VariantCode.VariantSize="" or isnull(VariantCode.VariantSize), ArticleSize.Name,VariantCode.VariantSize)' ;

		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Article',		'db' => 'Article.Number'),
		array ('name' => 'Variant',		'db' => 'VariantCode'),
		array ('name' => 'Position',		'db' => 'Position'),
		array ('name' => 'Description',	'db' => $DescriptionField),
		array ('name' => 'Color',		'db' => $ColorCodeField),
		array ('name' => 'ColorDesc',		'db' => $ColorField),
		array ('name' => 'Size',		'db' => $SizeField),
		array ('name' => 'Stock',		'db' => 'Stock.Name'),
		array ('name' => 'Container',		'db' => 'Container.Id')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
					VariantCode.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode, VariantCode.Disabled AS Disabled,' .
					$DescriptionField . ' as VariantDescription,' .
					$ColorField . ' as VariantColor,' .
					$ColorCodeField . ' as VariantNumber,' .
					$SizeField . ' as VariantSize,
					Article.Number as ArticleNumber';
		$queryTables = 'VariantCode
			 LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
			 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
			 LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
			 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
			 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
			 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
		$queryOrderBy = '' ;

	    switch ($Navigation['Parameters']) {
		case 'season' :
		    // Specific season
		    $query = sprintf ('SELECT season.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Season, Company WHERE Season.Id=%d AND Season.CompanyId=Company.Id AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
//		    if ($Record['TypeCustomer']) {
//			navigationEnable ('new') ;
//		    }
			if ($Record['ExplicitMembers']==1) {
				$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
							CollectionMember.ArticleId as ArticleId, VariantCode.VariantCode AS VariantCode,' .
							$DescriptionField . ' as VariantDescription,' .
							$ColorField . ' as VariantColor,' .
							$ColorCodeField . ' as VariantNumber,' .
							$SizeField . ' as VariantSize,
							Article.Number as ArticleNumber';
				$queryTables = '(Collection, CollectionMember)
					 LEFT JOIN VariantCode ON CollectionMember.articleid=VariantCode.articleid AND CollectionMember.articleColorid=VariantCode.articleColorid AND VariantCode.Active=1
					 LEFT JOIN Article ON CollectionMember.ArticleId=Article.Id and Article.Active=1 
					 LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
					 LEFT JOIN ArticleColor ON CollectionMember.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
					 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
					 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
					 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;

					// Clause
					$queryClause = sprintf ('Collection.SeasonId=%d AND CollectionMember.CollectionId=Collection.Id AND CollectionMember.Active=1', $Id) ;
			} else {
				$queryFields =  'VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, Stock.Name as StockName,
							Article.Id as ArticleId, VariantCode.VariantCode AS VariantCode,' .
							$DescriptionField . ' as VariantDescription,' .
							$ColorField . ' as VariantColor,' .
							$ColorCodeField . ' as VariantNumber,' .
							$SizeField . ' as VariantSize,
							Article.Number as ArticleNumber';
				$queryTables = '(`order` o, orderline ol, article)
					 LEFT JOIN ArticleSize ON article.Id=ArticleSize.ArticleId and ArticleSize.Active=1 
					 LEFT JOIN VariantCode ON ol.articleid=VariantCode.articleid AND ol.articleColorid=VariantCode.articleColorid AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.Active=1
					 LEFT JOIN ArticleColor ON ol.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
					 LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
					 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
					 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1' ;
				$queryClause = sprintf ('o.seasonid=%d and ol.orderid=o.id and ol.articleid=article.id
								and o.active=1 and ol.active=1 and article.active=1
								', $Id) ;
			}  
			$queryOrderBy = 'ArticleNumber, VariantColor, ArticleSize.DisplayOrder' ;
			break ;

		default :
		    // Display
		    $HideSeason = true ;

		    // Clause
		    $queryClause = "VariantCode.Active=1" ;
		    break ;
	    }
//return 'select ' . $queryFields . ' from ' . $queryTables . ' where ' . $queryClause ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrderBy) ;
		
	    if (is_string($Result)) return $Result ;
		$query='Select * from parameter' ;
	    $Result2 = dbQuery ($query) ;
	    return listView ($Result2, 'view') ;


	case 'picklist' :
		$query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
 
		if ($Record['Type']=='SalesOrder') {
			$query = sprintf ("SELECT * FROM `Order` WHERE Active=1 AND Id=%d", $Record['ReferenceId']) ;
			$result = dbQuery ($query) ;
			$SalesOrder = dbFetch ($result) ;
			dbQueryFree ($result) ;
			$Record['SeasonId'] = $SalesOrder['SeasonId'] ;
 		}
 		
 
		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Position',		'db' => 'Position'),
		array ('name' => 'Variant',			'db' => 'VariantCode.VariantCode'),
		array ('name' => 'Description',		'db' => 'PickOrderLine.VariantDescription')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'Container.`Position` AS `Position`, 
					(select sum(Item.Quantity) from Item where Item.containerid=Container.Id and Item.articleId=VariantCode.ArticleId and Item.articleColorId=VariantCode.ArticleColorId and Item.articleSizeId=VariantCode.ArticleSizeId and Item.active=1) as ItemQty,
					VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, 
					VariantCode.VariantUnit as VariantUnit, VariantCode.Reference as VariantCodeReference, PickOrderLine.Id AS PickOrderLineId, VariantCode.VariantCode AS VariantCode, PickOrderLine.VariantCodeId AS VariantCodeId,
					PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, PickOrderLine.VariantSize as VariantSize,
					PickOrderLine.OrderedQuantity as OrderedQuantity, PickOrderLine.PackedQuantity as PackedQuantity, PickOrderLine.PickedQuantity as PickedQuantity, PickOrderLine.QlId';
	    
/*	    
		$queryTables = '(PickOrderLine, Season) 
			 LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId and variantcode.active=1
			 LEFT JOIN Container ON Container.StockId=Season.PickStockId  and Container.Active=1
 			 LEFT JOIN Item on Item.containerid=Container.Id and Item.articleId=VariantCode.ArticleId and Item.articleColorId=VariantCode.ArticleColorId and Item.articleSizeId=VariantCode.ArticleSizeId and Item.active=1' ;
*/
			 $queryTables = '(PickOrderLine) 
			 LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId and variantcode.active=1
			 LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1' ;
// 			 LEFT JOIN Item on Item.containerid=Container.Id and Item.active=1' ;

		$queryClause = sprintf ("PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 ", $Id) ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;
	    return listView ($Result, 'pickorderlines') ;
	    
	case 'pickorderlist-cmd' :
	case 'ziplist-cmd' :
		$query = "SELECT * FROM PickOrder WHERE Active=1 AND Id=1" ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id=1 ;
		$Record['Id']=1 ;
		return 0 ;
 
	case 'pickorderlist' :
		// List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Reference',		'db' => 'Reference'),
		array ('name' => 'CompanyName',			'db' => 'CompanyName'),
		array ('name' => 'State',		'db' => 'State')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  'po.Id as Id,CONCAT(u.FirstName," ",u.LastName," (",u.Loginname,")") as PickUser, 
							if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")) as State, po.Reference as Reference, 
							c.Name as CompanyName, po.DeliveryDate as DeliveryDate, pol.id as NoLines, 
							pol.OrderedQuantity as Qty';
	    
		$queryTables = '(PickOrder po, PickOrderLine pol, Company c)
					LEFT JOIN User u ON po.PickUserId=u.Id' ;

		$queryClause = 'po.Active=1 And po.Packed=0 And pol.PickOrderId=po.Id And po.CompanyId=c.Id' ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    return listView ($Result, 'picklist') ;
	    
 	case 'basic' :
	case 'basic-cmd' :
	case 'delete-cmd' :
		$query = sprintf ("SELECT * FROM PickOrder WHERE Active=1 AND Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    if((int)$Record['Packed'] > 0) {
	    	navigationPasive('shipfrombasic');
	    }

		return 0 ;
		
	case 'generatecon' :
	case 'generatecon-cmd' :
		$query = sprintf ('SELECT o.*, o.ConsolidatedId as Id ,Season.PickStockId as SeasonPickStockId
, if(po.Packed>0,"Done", if(po.printed>0, if(po.ToId>0,if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId>0,"Assigned","")))) as PickOrderState
					FROM `Order` o
					LEFT JOIN Season ON o.SeasonId=Season.Id AND Season.Active=1
 					LEFT JOIN PickOrder po ON po.ReferenceId=O.Id and po.Type="SalesOrder" and po.packed=0 and po.Active=1 
					LEFT JOIN Stock s ON po.toid=s.id 
					WHERE o.Active=1 AND o.ConsolidatedId=%d', $Id) ;
	      $res = dbQuery ($query) ;
	      $Record = dbFetch ($res) ;
	      // dbQueryFree ($res) ;

		if (($Record['PickOrderState']=="Done")) { // or ($Record['Ready']==0)) {
			navigationPasive ('createpickorder') ;		
	    	}

		return 0 ;

 	case 'generate' :
	case 'generate-cmd' :
		$query = sprintf ('SELECT o.*, Season.PickStockId as SeasonPickStockId, Season.InitialPickInstructions as InitialPickInstructions, 
								  if(po.Packed>0,"Done", if(po.printed>0, if(po.ToId>0,if(s.ready>0,"Printed","Packing"),"Printed"),if(po.ToId>0,if(s.ready>0,"Part Shipped","Packing"),if(po.PickUserId>0,"Assigned","")))) as PickOrderState,
								  pi.OrderId as InstructionOrderId
					FROM `Order` o
					LEFT JOIN Season ON o.SeasonId=Season.Id AND Season.Active=1
 					LEFT JOIN PickOrder po ON po.ReferenceId=O.Id and po.Type="SalesOrder" and po.packed=0 and po.Active=1 
					LEFT JOIN Stock s ON po.toid=s.id 
					LEFT JOIN pickinstruction pi ON pi.customerid=o.companyid and pi.seasonid=o.seasonid 
					WHERE o.Active=1 AND o.Id=%d', $Id) ;
	      $res = dbQuery ($query) ;
	      $Record = dbFetch ($res) ;
	      dbQueryFree ($res) ;

		if (($Record['PickOrderState']=="Done")) { // or ($Record['Ready']==0)) {
			navigationPasive ('createpickorder') ;		
	    	}

		return 0 ;

 	case 'printorder' :
 	case 'packlist' :
	case 'packlist-cmd' :
logPrintTime ('packlist load 1') ;
	    $query = 
	     sprintf ("SELECT if((Container.`Position` is null) or (Container.`Position`=''), 
	                               if(VariantCode.PickContainerId>0,'X-NoPos+C','X-NoPos'), 
	                               Container.`Position`) AS `Position`,
				(select sum(Item.Quantity) from Item where Item.containerid=Container.Id and Item.articleId=VariantCode.ArticleId and Item.articleColorId=VariantCode.ArticleColorId and Item.articleSizeId=VariantCode.ArticleSizeId and Item.active=1) as ItemQty,
				group_concat(PickOrderLine.Id) AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, group_concat(PickOrderLine.ImportError) AS ImportError, 
				Article.Number as ArticleNumber, VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, VariantCode.ArticleSizeId as ArticleSizeId, 
				PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, 
				PickOrderLine.VariantSize as VariantSize, VariantCode.VariantModelRef as VariantModelRef,
				sum(PickOrderLine.OrderedQuantity) as OrderedQuantity, 
				sum(PickOrderLine.PackedQuantity) as PackedQuantity, 
				PickOrderLine.BoxNumber as BoxNumber, 
				sum(PickOrderLine.PrePackedQuantity) as PrePackedQuantity, 
				sum(PickOrderLine.PickedQuantity) as PickedQuantity,
				VariantCode.SKU,
				VariantCode.VariantCode
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId  and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity 
		GROUP BY VariantCode.id ORDER BY `Position`, article.number, variantcode.articlecolorid, variantcode.articlesizeid", $Id) ;
//		GROUP BY VariantCode.id ORDER BY `Position`, variantcode.articleid, variantcode.articlecolorid, variantcode.articlesizeid", $Id) ;
	    $Result = dbQuery ($query) ;
	    if ($Id <= 0) return 0 ;
    
	    $query = sprintf ("SELECT PickOrder.*, o.reference as OrderReference, o.WebshipperId, o.Id as OrderId, ss.Name as ShopifyName FROM PickOrder LEFT JOIN `Order` o ON o.id=PickOrder.ReferenceId LEFT JOIN shopifyshops ss ON ss.Id=o.WebShopNo WHERE PickOrder.Active=1 AND PickOrder.Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
 
		if ($Record['Type']=='SalesOrder') {
			$query = sprintf ("SELECT * FROM `Order` WHERE Active=1 AND Id=%d OR ConsolidatedId=%d", $Record['ReferenceId'], $Record['ConsolidatedId']) ;
			$result = dbQuery ($query) ;
			$SalesOrder = dbFetch ($result) ;
			dbQueryFree ($result) ;
			$Record['SeasonId'] = $SalesOrder['SeasonId'] ;
			$Record['CurrencyId'] = $SalesOrder['CurrencyId'] ;
			$Record['Country'] = tableGetField('Country','Description', $SalesOrder['CountryId']) ;
 		}
 		if (($Record['Packed']==1)) {
			navigationPasive ('packlist_pack') ;		
			navigationPasive ('packlist_ship') ;		
			navigationPasive ('packlist_partship') ;		
	    	}

	    // Lines
	    $query = 
	     sprintf ("SELECT if((Container.`Position` is null) or (Container.`Position`=''), 
	                               concat(if(VariantCode.PickContainerId>0,'X-NoPos+C#','X-NoPos#'),PickOrderLine.Id), Container.`Position`
	                               ) AS `Position`,
							(select sum(Item.Quantity) from Item where Item.containerid=Container.Id and item.articleid=variantcode.articleid and item.articlecolorid=variantcode.articlecolorid and item.articlesizeid=variantcode.articlesizeid and Item.active=1) as ItemQty,
							group_concat(PickOrderLine.Id) AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, 
							Article.Number as ArticleNumber, VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, 
							PickOrderLine.VariantCodeId AS VariantCodeId,
							PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, 
							PickOrderLine.VariantSize as VariantSize, VariantCode.VariantModelRef as VariantModelRef,
							cat.name as CategoryName,
							PickOrderLine.BoxNumber as BoxNumber, 
							sum(PickOrderLine.OrderedQuantity) as OrderedQuantity, 
							sum(PickOrderLine.PackedQuantity) as PackedQuantity, 
							sum(PickOrderLine.PrePackedQuantity) as PrePackedQuantity, 
							sum(PickOrderLine.PickedQuantity) as PickedQuantity
					FROM (PickOrderLine)
					LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId  and variantcode.active=1
					LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
					LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
				    LEFT JOIN `articlecategory` cat on cat.Category=substring(Article.Number,2,3)
					WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 And PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity 
					GROUP BY VariantCode.id 
					ORDER BY substring(container.position,1,6), cast(substring(container.position,7,2) as unsigned), variantcode.articleid, variantcode.articlecolorid, variantcode.articlesizeid", $Id) ;
	    $Result = dbQuery ($query) ;
		$_openPickLines = (int)dbNumRows($Result) ;
	    if ($Id <= 0) return 0 ;
		if (($_openPickLines>0) and ($Record['Packed']==0)) { // and toId>0 ???
			navigationPasive ('packlist_ship') ;		
			navigationPasive ('packlist_partship') ;		
		}
//logPrintTime ('packlist load 2') ;

		return 0 ;

	case 'setbatch' :
	    $query = sprintf ("SELECT variantcode.*, Container.Position FROMnot ( variantcode LE)FT JOIN container ON container.id=variantcode.pickcontainerid WHERE variantcode.Id=%d", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		$_query = sprintf('	Select 
					 tab.PickOrderId,  tab.CompanyName, tab.Reference
					From
					 (select pickorder.id as PickOrderId, pickorder.reference as Reference, pickorder.CompanyName as CompanyName, count(pickorder.id) as NoLines, pickorder.batchid as BatchId,
								sum(pickorderline.orderedquantity) as Qty, pickorderline.id as PickOrderLineId, PickOrderLine.Variantcodeid as VariantcodeId
						 from
						 (PickOrder, PickOrderLine)
						 where PickOrder.Packed=0 and PickOrder.active=1 and PickOrder.Type="SalesOrder" 
						 and (PickOrderLine.OrderedQuantity>PickOrderLine.PackedQuantity or PickOrderLine.PackedQuantity is null) 
						 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Done=0 and PickOrderLine.Active=1 
						 group by pickorder.id
					 ) tab
					 inner join variantcode On variantcode.id=tab.Variantcodeid
					Where tab.NoLines=1 and tab.variantcodeid=%d and tab.Qty=1', $Id) ; //die($_query) ;
		$Result = dbQuery($_query) ;
		
		break ;	 
					
	case 'batchlist':
		// List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Batch',			'db' => 'tab.BatchId',		'desc' => true),
			array ('name' => 'Variant',			'db' => 'VariantCode.VariantCode')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  ' variantcode.VariantCode, variantcode.Id as VariantCodeId, count(tab.PickOrderId) as QrderQty, tab.packed as Packed, group_concat(cast(tab.PickOrderId as char(6))), tab.BatchId, sum(tab.PackedQty) as PackedQty';
		$queryTables = '(select pickorder.id as PickOrderId, count(pickorder.id) as NoLines, pickorder.batchid as BatchId, sum(if(pickorder.packed=1,1,0)) as PackedQty, pickorder.packed as packed,
								sum(pickorderline.orderedquantity) as Qty, pickorderline.id as PickOrderLineId, PickOrderLine.Variantcodeid as VariantcodeId
						 from
						 (PickOrder, PickOrderLine)
						 where PickOrder.BatchId>0 and PickOrder.active=1 and PickOrder.Type="SalesOrder"
						 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Active=1 
						 group by pickorder.id
						) tab
						inner join variantcode On variantcode.id=tab.Variantcodeid' ;
		$queryClause = "tab.BatchId>0" ;
		if ($Navigation['Parameters']=='all') {
			$queryClause .= "" ;
		} else {
			$queryClause .= " and tab.Packed=0" ;
		} 
		$queryOrder = 'tab.BatchId' ;
		$queryGroup = 'tab.BatchId' ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrder, $queryGroup) ;
	    if (is_string($Result)) return $Result ;
//	    return listView ($Result, 'pickorderlines') ;
		return 0 ;

	case 'batchvariantlist':
		// List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Variant',			'db' => 'VariantCode.VariantCode'),
			array ('name' => 'Order Qty',		'db' => 'count(tab.PickOrderId)',		'desc' => true, 'nofilter' => true),
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  ' variantcode.VariantCode, variantcode.Id as VariantCodeId, tab.VariantDescription as VariantDescription, count(tab.PickOrderId) as QrderQty, group_concat(cast(tab.PickOrderId as char(6))), tab.BatchId  ';
		$queryTables = '(select pickorder.id as PickOrderId, count(pickorder.id) as NoLines, pickorder.batchid as BatchId,pickorderline.VariantDescription,
								sum(pickorderline.orderedquantity) as Qty, pickorderline.id as PickOrderLineId, PickOrderLine.Variantcodeid as VariantcodeId
						 from
						 (PickOrder, PickOrderLine)
						 where PickOrder.Packed=0 and PickOrder.active=1 and PickOrder.Type="SalesOrder" and PickOrder.batchid=0
						 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Done=0 and PickOrderLine.Active=1 
						 group by pickorder.id
						) tab
						inner join variantcode On variantcode.id=tab.Variantcodeid' ;
		$queryClause = "tab.NoLines=1 and tab.Qty=1" ;
		$queryOrder = '' ;
		$queryGroup = 'VariantCode.Id, tab.BatchId' ;

	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrder, $queryGroup) ;
	    if (is_string($Result)) return $Result ;
	    return listView ($Result, 'pickorderlines') ;
		return 0 ;
		
	case 'batchview':
		$_query = sprintf('	Select pickorder.*, pickorderline.VariantCode, o.reference as OrderReference,
								company.Name as CompanyName, o.AltCompanyName as DeliveryCompanyName,Country.Name as DeliveryCountryName, Company.InvoicePerEmail
							from (pickorder, pickorderline)
						 left join `Order` o On o.id=pickorder.referenceId
						 left join country on country.id=o.countryid
						 left join company on company.id=o.companyid
							where batchid=%d and PickOrder.active=1 and PickOrder.Type="SalesOrder"  and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Active=1 
							order by pickorder.packed, pickorder.Reference', $Id) ; //die ()
		$Result = dbQuery ($_query) ;

		if (isset($_GET['shipmentid'])) {
			$_query = sprintf ("SELECT InvoicePerEmail FROM stock, company WHERE stock.Id=%d and Company.Id=Stock.CompanyId", (int)$_GET['shipmentid']) ;
			$_res = dbQuery ($_query) ;
			$_row = dbFetch ($_res) ;
			dbQueryFree ($_res) ;
			if ($InvoicePerEmail==0) {
				navigationHide('printdocuments');
				$DisableScan = 0 ;
			} else {
				navigationPasive('storeitemscmd');
				navigationPasive('printpickinglist');
				$DisableScan = 1 ;
			}
		} else {
			navigationHide('printdocuments');
			$DisableScan = 0 ;
		}
		
	case 'printbatch':
	case 'batchpack-cmd':
	    $query = sprintf ("SELECT variantcode.*, Container.Position, pickorder.BatchId, count(pickorder.id) as NoPickOrders, 
							sum(if(pickorder.packed=0,1,0)) as NoRemaining, 
							article.Number as ArticleNumber,
							concat(article.Description, ' - ' , color.Description,  ' - ', articlesize.Name)  as Description
							FROM (pickorder, pickorderline, variantcode)
							LEFT JOIN container ON container.id=variantcode.pickcontainerid 
							LEFT JOIN Article ON Article.Id=VariantCode.ArticleId
							LEFT JOIN ArticleColor ON ArticleColor.Id=VariantCode.ArticleColorId
							LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
							LEFT JOIN ArticleSize ON ArticleSize.Id=VariantCode.ArticleSizeId
							WHERE pickorder.BatchId=%d 
							 and PickOrder.active=1 and PickOrder.Type='SalesOrder'
							 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Active=1
							 and variantcode.id=pickorderline.variantcodeid 
							Group By variantcode.id 
							Order By substring(container.position,1,6), cast(substring(container.position,7,2) as unsigned)", $Id) ;
	    $VariantCodeResult = dbQuery ($query) ;
		return 0  ;	 
		
  	case 'setSingleGTINPack' :
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;

	case 'singleGTINlist':
		// List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Variant',			'db' => 'VariantCode.VariantCode'),
			array ('name' => 'Company',			'db' => 'tab.companyname'),
			array ('name' => 'DeliveryDate',	'db' => 'tab.DeliveryDate'),
			array ('name' => 'Instructions',	'db' => 'tab.Instructions')
	    ) ;

	    require_once 'lib/list.inc' ;
		$queryFields =  ' variantcode.VariantCode, variantcode.Id as VariantCodeId, tab.VariantDescription, tab.* ';
		$queryTables = '(select pickorder.id as PickOrderId, count(pickorder.id) as NoLines, sum(pickorderline.orderedquantity) as PickQty, pickorder.batchid as BatchId, PickOrder.Instructions,
								sum(pickorderline.orderedquantity) as Qty, pickorderline.id as PickOrderLineId, PickOrderLine.Variantcodeid as VariantcodeId,pickorderline.VariantDescription,
								PickOrder.Reference as Reference, PickOrder.Type as PickType,
								company.Name as CompanyName, o.AltCompanyName as DeliveryCompanyName,Country.Name as DeliveryCountryName,
								PickOrder.DeliveryDate as DeliveryDate, oa.countryid AS OrderAddressCountry
						 from
						 (PickOrder, PickOrderLine )
						 left join `Order` o On o.id=PickOrder.referenceId
	 					LEFT JOIN orderadresses oa ON oa.Active=1 AND oa.Type="shipping" AND oa.OrderId=o.Id
						 left join country on country.id=o.countryid
						 left join company on company.id=o.companyid
						 where PickOrder.Packed=0 and PickOrder.active=1 and PickOrder.Type="SalesOrder" 
						 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Done=0 and PickOrderLine.Active=1 
						 group by pickorder.id
						) tab
						inner join variantcode On variantcode.id=tab.Variantcodeid' ;
		
		$queryClause = "tab.NoLines=1 and tab.PickQty=1 and tab.OrderAddressCountry != 15" ;
		if ($_COOKIE['norway'] == 'true') {
			$queryClause = "tab.NoLines=1 and tab.PickQty=1 and tab.OrderAddressCountry = 15" ;
		}
		//$queryClause = "tab.NoLines=1 and tab.PickQty=1" ;
		$queryOrder = '' ;
		$queryGroup = '' ;

		//echo sprintf('SELECT ' . $queryFields . ' FROM ' . $queryTables . ' WHERE ' . $queryClause);

		$param = 'param=' . $Navigation['Parameters'] ;
		if ($listParam != '') $param .= '&' . $listParam ;
		navigationSetParameter ('setprint', $param) ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, $queryOrder, $queryGroup) ;
	    if (is_string($Result)) return $Result ;
//	    return listView ($Result, 'pickorderlines') ;
		return 0 ;		
		
    }

    if ($Id <= 0) return 0 ;
    

    return 0 ;
?>
