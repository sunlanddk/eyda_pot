<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Variant', 120)  ;
    listHead ('Description', 300)  ;
    listHead ('Order Qty', 60, "align='right'") ;
    listHead ('') ;
//    listHead ('State', 60) ;
//    listHeadIcon () ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
		if ($row["BatchId"]>0)
			listFieldIcon ('pack.png', 'packbatchview', (int)$row["BatchId"]) ;
		else
			listFieldIcon ('add.gif', 'packbatchnew', (int)$row["VariantCodeId"]) ;
    	listField ($row["VariantCode"]) ;
    	listField ($row["VariantDescription"]) ;
    	listField ($row["QrderQty"], "align='right'") ;
    	listField ('') ;
//    	listField ($row["State"]) ;
//		listFieldIcon ('print.gif', 'packbatchprint', (int)$row["BatchId"]) ;

    }

    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
