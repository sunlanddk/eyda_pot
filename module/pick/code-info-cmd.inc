<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

	$sku=$_GET["sku"];
	
	// Get Variant Information
	$query = sprintf ('SELECT a.Number as ArtNumber, a.Description as ArtDesc, co.Description as Color, az.name as Size  
				FROM (VariantCode vc, Article a, ArticleSize az, ArticleColor ac, Color co) 
				WHERE vc.VariantCode="%s" and vc.articleid=a.id and vc.articlesizeid=az.id and vc.articlecolorid=ac.id and ac.colorid=co.id
				AND vc.Active=1 AND a.Active=1 AND az.Active=1 AND ac.Active=1 AND co.Active=1', 
				$sku) ;
	$res = dbQuery ($query) ;
	$VariantCodeInfo = dbFetch ($res) ;
	dbQueryFree ($res) ;
		
    echo("<table>
		  <tr><td class=itemlabel>
				<b>Last Code:  
			</td>
			<td class=itemfield>" .
				 $sku . "<br>
			</td>
		  </tr>
		  <tr><td>
			</td>
		    <td class=itemfield><b> " . 
        	  $VariantCodeInfo['ArtDesc'] . " (" .  $VariantCodeInfo['ArtNumber'] . "); " . $VariantCodeInfo['Color'] . "; " . $VariantCodeInfo['Size'] . "</b>
			</td>
        	 
		  </tr>
		  </table>"
        );

    die();
    return 0 ;
?>
