<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHead ('Edit') ;
    listHead ('Store', 100)  ;
    listHead ('Preview') ;
    // listHead ('Content filled', 200) ;

    while ($row = dbFetch($Result)) {
        // $content = (($row['Content'] !== null && $row['Content'] !== '')  ? 'Yes' : 'No');
    	listRow () ;
		listFieldIcon ('edit.gif', 'editprinttext', (int)$row['Id']) ;
        listField (strtoupper($row["Store"])) ;
    	listFieldIcon ('print.gif', 'showprintpreview', 1, 'store='.$row['Store']) ; //740
    	// listField ($content) ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
