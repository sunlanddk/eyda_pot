<?php 
if (!isset($_POST['VariantCode'])) {
	return navigationCommandMark ('packlist') ;
}

$query = sprintf('select p.* from (pickorder p, `order` o) where o.WebshipperId="%s" and o.active=1 and p.active=1 AND p.packed=0 AND p.ReferenceId=o.Id', $_POST['VariantCode']) ;
$res = dbQuery($query) ;
if (dbNumRows($res)>0) {
	$row = dbFetch($res) ;
	$Params = sprintf('&pickorderid=%d', $row['Id']) ;
	return navigationCommandMark ('packlist',(int)$row['Id'], $Params) ;
} else {
	return 'No such unpacked pick order: ' . $_POST['VariantCode'] ;
}

?>