
const url = weburl; 
const appstore = new Vue({
	el: "#appRefund",
	data: {
		str: '',
		pickOrder: '',
		view: {
			findOrder: true,
			scanItems: false,
			chooseFrom: false,
		},
		orderid: 0,
		pickorderid: 0,
		stockid: 0,
		line_items: [],
		manuelerror: '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		scanned: 0,
		preloader: false
	},
	beforeMount(){
		var vm = this;
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str != '' && vm.view.findOrder == true){
	        		vm.findOrder(vm.str);
	        		vm.str = '';
	        		return;
	        	}

	        	if(vm.str.substring(0,6) == '99send'){
	        		// create refund record
	        		vm.CreateRefund();
	        		vm.str = '';
	        		return;
	        	}
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		// create refund record
	        		vm.CreateRefund();
	        		vm.str = '';
	        		return;
	        	}

	        	if(vm.str != '' && vm.view.scanItems == true){
	        		vm.checkVariant(vm.str);
	        		vm.str = '';
	        		return;	
	        	}

	        	vm.str = '';

	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		reload(){
			location.reload();
		},
		removeitem: function (index){
			this.items.splice(index, 1);
		},
	    reset(){
	    	var vm = this;
	    	vm.view.findOrder = true;
	    	vm.orderid = 0;
			vm.line_items = [];
			vm.preloader = false;
			vm.scanned = [];
			vm.choosefrom = [];
			vm.pickOrder = '';
			vm.pickorderid = 0;
			vm.stockid = 0;
			vm.orderid = 0;
	    	vm.str = '';
			vm.view.scanItems = false;
	    	vm.view.choosefrom = false;
	    },
	    CreateRefund(){
	    	let self = this;
	    	self.preloader = true;
	    	let data = {line_items: self.scanned, orderid: self.orderid, userid: $('.userid').val(), ws: $('.ws').val()};
	    	let path = window.location.pathname;
	    	if(self.scanned.length == 0){
	    		self.preloader = false;
	    		self.message = 'Please scan items before creating.';
	    		return;
	    	}

	    	// self.postData('http://eyda.pot.dk/s4/public/refund/create/passon/refund', data, self.CreateRefundResponse);
			self.postData(weburlNew+'refund/create/passon/refund', data, self.CreateRefundResponse);
	    },
	    CreateRefundResponse(response){
	    	let self = this;
	    	self.done = 'Refund created.';
	    	self.reset();
	    },
	    pack(){

	    }
	    closeChoosen(){
	    	let self = this;
	    	self.view.chooseFrom = false;
	    	self.chooseFrom = [];
	    },
	    playSound() {
			const url = '//eyda.passon.dk/eyda/image/thumbnails/errorMsgSound.wav'; 
			const audio = new Audio(url);
			audio.play();
		},
	    checkVariant(variantcode){
	    	let self = this;
	    	let found = false;

	    	self.line_items.forEach(function(e,i){
	    		if(e.variantcode == variantcode){
	    			if(parseInt(e.orderedquantity) >= (parseInt(e.packedquantity) + 1 + parseInt(e.scanned))){
	    				e.scanned ++;
	    				found = true;
	    			}
	    		}
	    	});

	    	if(found == false){
	    		// self.playSound() ;
	    		self.message = 'Variant ('+variantcode+') not found or all has been scanned.';
	    		// alert('Variant not found or total qty has been reached.')
	    	}
	    	return;
	    },
	    findOrderResponse(response){
	    	let self = this;
	    	console.log(response);
	    		
	    	self.orderid = response.pickorder.orderid;
	    	self.pickorderid = response.pickorder.id;
			self.line_items = response.pickorder.line_items;
			self.pickOrder = response.pickorder.id;
			self.scanned = [];
			self.choosefrom = [];
			self.preloader = false;
			self.view.findOrder = false;
			self.view.scanItems = true;
			self.view.choosefrom = false;
	    	return;
	    },
	    findOrder(str){
	    	let self = this;
	    	let path = window.location.pathname;
	    	self.message = '';
			self.done = '';
			self.donesecond = '';
	    	self.preloader = true;
	    	self.getData(weburlNew+'pick/find/order/'+encodeURIComponent(str), self.findOrderResponse);
	    	// self.getData('http://eyda.pot.dk/s4/public/refund/find/order/'+str, self.findOrderResponse);
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    headers: {
                	'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
                	'X-Header-Userid': $('.userid').val(),
                	'Accept': 'application/json',
					'Content-Type': 'application/json'
            	},
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {

			    	if(data.status == 'error'){
			    		vm.message = data.response;
			    		vm.preloader = false;
			    	}
			    	else{
			    		callback(data.response);
			    	}
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	console.log(errorThrown, textStatus, jqXHR);
			    	vm.message = 'An error occured, try by scanning again. Error message: ' +textStatus;
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
	    	let vm  = this;
			$.ajax({
			    url : url,
			    type: "GET",
			    headers: {
                	'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
                	'X-Header-Userid': $('.userid').val(),
                	'Accept': 'application/json',
					'Content-Type': 'application/json'
            	},
			    success: function(data, textStatus, jqXHR)
			    {

			    	if(data.status == 'error'){
			    		vm.message = data.response;
			    		vm.preloader = false;
			    	}
			    	else{
			    		callback(data.response);
			    	}
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});



