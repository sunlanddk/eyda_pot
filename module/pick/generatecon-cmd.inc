<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'module/pick/packcsv-cmd.inc' ;
	require_once 'generatePickorder.inc' ;
    require_once 'module/order/stockExportToWeb.php';

	global $User, $Navigation, $Record, $Size, $Id ;

	if ($_POST['OnlyFile'] == 'on') {
		return CreatePickConsolidatedCSV($Id) ;
		return 0 ;
	}

	if ($_POST['ReadyAll'] == 'on') {
		$query = 'SELECT * FROM `order` WHERE ready=0 and active=1 and ConsolidatedId='.$Id;
    	$res = dbQuery($query);

		$Order = array (	
			'Ready'			=>  1,
			'ReadyUserId'	=>  $User['Id'],
			'ReadyDate'		=>  dbDateEncode(time())
		) ;

		while ($orders = dbFetch($res)) {
	    	tableWrite('Order', $Order, $orders['Id']);
	    }
	}

	if (isset($_POST['MaxDeliveryDate'])) $MaxDeliveryDate = $_POST['MaxDeliveryDate'] ;

	$ordersIds = array();
	foreach ($_POST['Ready'] as $key => $value) {
		array_push($ordersIds, $key);
		sendStockDataToWebshop($key) ;
	}
	if(empty($ordersIds) == true) return 'No orders have been choosen';

	return createPickorders($ordersIds, $MaxDeliveryDate, $_POST['HandlerId'], $_POST['Instructions'], $_POST['PickStockId'], $_POST['TransferFile']);

?>
