<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	$PickingStock[787] = 2059 ;
	$PickingStock[1430] = 9587 ;

	$first=1 ;
	$query = sprintf(
		"SELECT o.id as OrderId, o.companyid as CompanyId, 
				ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid, 
				oq.articlesizeid, oq.quantity as quantity,
				vc.id as VariantCodeId, vc.VariantCode as VariantCode, 
				c.description as color, az.name as size
		FROM `order` o, orderline ol, orderquantity oq, articlecolor ac, color c, articlesize az, variantcode vc
		WHERE ol.orderid=o.id and oq.orderlineid=ol.id and o.active=1 and ol.active=1 and oq.active=1
		and o.done=0 and ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid
		and ol.articlecolorid=ac.id and ac.colorid=c.id and oq.articlesizeid=az.id and o.id=%s order by o.id", $Id);

	$res = dbQuery ($query) ;	
	while ($row = dbFetch ($res)) {
  		// Initialize
		$PickOrder = array (	
			'Type'			=>  "SalesOrder",					// Pick order from internal customer
			'Reference'			=>  0,						// Order number 
			'FromId'			=> $PickingStock[$Record['ToCompanyId']],	
			'CompanyId'			=>  0,						
			'OwnerCompanyId'		=>  $Record['ToCompanyId'],				
			'HandlerCompanyId'	=>  $_POST['HandlerId'],		
			'Packed'			=>  0,
			'DeliveryDate'		=>  ""			// Expected pick and pack complete
		) ;

		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		// Generate PickOrders
		$PickOrder['Reference'] = $row['OrderId'] ;
		$PickOrder['ReferenceId'] = $row['OrderId'] ;
		$PickOrder['CompanyId'] = $row['CompanyId'] ;
		$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
		
		$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
		$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
		$PickOrderLine['OrderedQuantity'] = $row['quantity'] ;
		$PickOrderLine['VariantDescription'] = $row['ArticleDescription'] ;
		$PickOrderLine['VariantColor'] = $row['color'] ;
		$PickOrderLine['VariantSize'] = $row['size'] ;

		// Update or insert into PickOrder table.
		if ($first) {
			$first=0 ;
			$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d and Active=1", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
		}

		// Update or insert into PickOrderLine table.
		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$query = sprintf ("SELECT * FROM PickOrderLine WHERE Active=1 AND VariantCodeId=%d AND PickOrderId=1", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
		$result = dbQuery ($query) ;
		if (dbNumRows()==0) {
			$Line['Id'] = 0 ;
		} else {
			$Line = dbFetch ($result) ;
			if ($Line['PackedQuantity']>0) {
				if ($Line['PackedQuantity']>$PickOrderLine['OrderedQuantity']) {
					$ErrorTxt =	$ErrorTxt . sprintf('Allready packed %d order cant be set to %d for %s %s %s', 
						$Line['PackedQuantity'], $PickOrderLine['OrderedQuantity'], $PickOrderLine['VariantDescription'], $PickOrderLine['VariantColor'], $PickOrderLine['VariantSize']) ;
				}
			}
		}
		dbQueryFree ($result) ;

		tableWrite ('PickOrderLine', $PickOrderLine, $Line['Id']) ;


		// Update or insert into PickOrderLine table.
/*		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$whereclause = sprintf ("VariantCodeId=%d and PickOrderId=%d and Active=1", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
		$PickOrderLineId=tableGetFieldWhere ('PickOrderLine', 'Id', $whereclause ) ;
		tableWrite ('PickOrderLine', $PickOrderLine, $PickOrderLineId) ;
*/
    }
    dbQueryFree ($res) ;
    return 0 ;
?>
