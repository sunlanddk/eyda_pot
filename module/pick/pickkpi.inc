
<?php
	require_once 'lib/list.inc' ;
	require_once 'lib/item.inc' ;
	require_once 'lib/table.inc' ;


	
	// Ordered Today
	$_query = "
	select count(pickorder.id) as cnt 
	from pickorder 
	LEFT JOIN pickorderline pl ON pl.Id=(SELECT MAX(pickorderline.Id) as Id FROM pickorderline WHERE pickorderline.PickOrderId=pickorder.Id AND pickorderline.Active=1)
	where pickorder.createdate>CURRENT_DATE() and pickorder.active=1 AND pl.Id IS NOT NULL" ;
	$_result = dbQuery($_query);
	$_row=dbFetch($_result) ;
	dbQueryFree($_result) ;
	$_newOrders = $_row['cnt'] ;
	$_query = "select sum(pl.orderedquantity) as cnt from (pickorder p, pickorderline pl) 
				  where p.createdate>CURRENT_DATE() and p.active=1  and p.ownercompanyid=787 and p.handlercompanyid=787 
						and pl.pickorderid=p.id and pl.active=1 AND pl.Id IS NOT NULL" ;
	$_result = dbQuery($_query);
	$_row=dbFetch($_result) ;
	dbQueryFree($_result) ;
	$_newOrderPcs = $_row['cnt'] ;

	// Open orders
	$_query = "select count(pickorder.id) as cnt 
				from pickorder 
				LEFT JOIN pickorderline pl ON pl.Id=(SELECT MAX(pickorderline.Id) as Id FROM pickorderline WHERE pickorderline.PickOrderId=pickorder.Id AND pickorderline.Active=1)
				where pickorder.packed=0 and pickorder.active=1 AND pl.Id IS NOT NULL" ; //die($_query);
	$_result = dbQuery($_query);
	$_row=dbFetch($_result) ;
	dbQueryFree($_result) ;
	$_openOrders = $_row['cnt'] ;
	$_query = "select sum(pl.orderedquantity-pl.packedquantity) as cnt from (pickorder p, pickorderline pl) 
				  where p.packed=0 and p.active=1 and p.ownercompanyid=787 and p.handlercompanyid=787 
						and pl.pickorderid=p.id and pl.active=1 AND pl.Id IS NOT NULL" ;
	$_result = dbQuery($_query);
	$_row=dbFetch($_result) ;
	dbQueryFree($_result) ;
	$_openOrderPcs = $_row['cnt'] ;



	$_querystr = "SELECT dayupdate.packed, count(Shipments) as OrderQty, sum(ShipmentPcs) as PackedQty FROM (
							SELECT %s, COUNT(s.Id) AS Shipments, sum(pl.PackedQuantity) as ShipmentPcs 
  							FROM (stock s, pickorder p, pickorderline pl) 
  					 			    WHERE s.DepartedDate >= %s
     			 	   			    AND s.Type !='fixed' AND s.Departed=1 
    			 				    AND s.active=1
    			 				    AND s.Id=p.toId
    			 				    AND p.active=1
    			 				    AND pl.pickorderId=p.Id
    			 				    AND pl.active=1
    			 				    AND p.fromid=14321
    			 				    AND p.ownercompanyid=787
    			 				    AND p.handlercompanyid=787
				 					GROUP BY s.Id) dayupdate
				 					GROUP BY dayupdate.packed 
				 					ORDER BY dayupdate.packed ASC" ;



	// Today list per hour
	$_dateformat = "date_format(Date_add(s.DepartedDate, interval 1 hour),'%H') as packed" ;
	$_datefilter = "date_format(CURRENT_DATE(), '%Y-%m-%d')" ;
	$_query =  sprintf($_querystr, $_dateformat, $_datefilter);
//die($_query) ;
	$_result = dbQuery($_query);


	$_packedArray[]		= [];
	$_packedQtyArray[]	= [];
	$_orderQtyArray[]	= [];

	while ($_row=dbFetch($_result)) {
		$_packedArray[] 	= $_row['packed'] ;
		$_packedQtyArray[]	= $_row['PackedQty'];
		$_orderQtyArray[]	= $_row['OrderQty'] ;
		$_packedQtyToday   += $_row['PackedQty'];
		$_orderQtyToday    += $_row['OrderQty'];
	}
	dbQueryFree($_result) ;
?>


	
	<tr><td>&nbsp;<br><br></td></tr>
	<tr><td>&nbsp;<br><br></td></tr>
	</table></center>
	<center><table
    style="margin: 10px;
    border-collapse: separate;
    border-spacing: 0px;">
	<tr>
		<td></td>
		<td width=200px align="center" style="border-radius: 20px;background-color:#C2C2C2;color: white;border: 5px solid white;padding: 10px;">
			<p style="font-size:20px"><br><B>Packed today:</B><br></p>
			<p style="color:green;font-size:45px"><?php Echo '<B>'.(int)$_orderQtyToday . '</B><br>';?></p>
			<p style="color:green;font-size:20px"><?php Echo '(' . (int)$_packedQtyToday . ' pcs)<br><br>';?></p>
		</td>
		<td width=100px>&nbsp;</td>
		<td width=200px align="center" style="border-radius: 20px;background-color:#C2C2C2;padding: 10px;">
			<p style="font-size:20px"><br><B>Open Orders:</B><br></p>
			<p style="color:red;font-size:45px"><?php Echo '<B>'. (int)$_openOrders . '</B><br>';?></p>
			<p style="color:red;font-size:20px"><?php Echo '(' . (int)$_openOrderPcs . ' pcs)<br><br>';?></p>
		</td>
		<td width=100px>&nbsp;</td>
		<td width=200px align="center" style="border-radius: 20px;background-color:#C2C2C2;padding: 10px;">
			<p style="font-size:20px"><br><B>Ordered Today:</B><br></p>
			<p style="color:blue;font-size:45px"><?php Echo '<B>'. (int)$_newOrders . '</B><br>';?></p>
			<p style="color:blue;font-size:20px"><?php Echo '(' . (int)$_newOrderPcs . ' pcs)<br><br>';?></p>
		</td>
		<td width=100px>&nbsp;</td>
		<td width=200px align="center" style="border-radius: 20px;background-color:#C2C2C2;padding: 10px;">
			<p style="font-size:20px"><br><B>Open Returns:</B><br></p>
			<p style="color:blue;font-size:45px"><?php Echo '<B>'. (int)tableGetFieldWhere('`return`','count(Id)','refundId IS NULL and active=1') . '</B><br>';?></p>
			<p style="color:blue;font-size:20px"><?php Echo '(' . (int)tableGetFieldWhere('`return`, returnline','sum(returnline.quantity)','return.refundId IS NULL and returnline.returnid=return.id and `return`.active=1') . ' pcs)<br><br>';?></p>
		</td>
		<td></td>
	</tr>
	</table></center>




<?php

	echo '<center><table width=99%><tr><td>' ;
	echo '<br><br></td></tr><tr><td>' ;
	
	listStart() ;
	listHeader('Packed Today') ;
	listHead('Time', 100) ;
	listHead('Shipped pcs', 100) ;
	listHead('Shipments', 100) ;
	listHead('') ;
	for($_i = 1; $_i < count($_orderQtyArray); $_i++) {
		listRow() ;
		listField($_packedArray[$_i]) ;
		listField($_packedQtyArray[$_i]) ;
		listField($_orderQtyArray[$_i]) ;
	}
	dbQueryFree($_result) ;
	listEnd() ;



	$_querystr = "SELECT 
					%s,
					sum(Shipments) as OrderQty,
					sum(ShippedPcs) as PackedQty
					FROM kpiday
					WHERE active=1
					and %s
					AND Shipments>0
					GROUP BY packed order by packed ASC";



	
	echo '</td><td>' ;
	// Last 7 days list
	$_dateformat = "date_format(producedDate,'%Y-%m-%d') as packed" ;
	$_datefilter = "producedDate>=DATE_ADD(CURRENT_DATE(), INTERVAL -7 DAY)" ;
	$_query =  sprintf($_querystr, $_dateformat, $_datefilter);
	$_result = dbQuery($_query);
	listStart () ;
	listHeader('Last 7 days') ;
	listHead('Date', 100) ;
	listHead('Shipped pcs', 100) ;
	listHead('Shipments', 100) ;
	listHead('') ;
	while ($_row=dbFetch($_result)) {
		listRow() ;
		listField($_row['packed']) ;
		listField($_row['PackedQty']) ;
		listField($_row['OrderQty']) ;
	}
	dbQueryFree($_result) ;
	listEnd() ;
	
	echo '</td><td>' ;
	// Last 4 weeks list
	$_dateformat = "date_format(producedDate,'%x-%v') as packed" ;
	$_datefilter = "producedDate>=DATE_ADD(DATE_ADD(date_format(CURRENT_DATE(), '%Y-%m-%d'), INTERVAL (-WEEKDAY(date_format(CURRENT_DATE(), '%Y-%m-%d'))) DAY), INTERVAL -4 Week)" ;
	$_query =  sprintf($_querystr, $_dateformat, $_datefilter);
	$_result = dbQuery($_query);
	listStart () ;
	listHeader('Last 4 weeks') ;
	listHead('Week', 100) ;
	listHead('Shipped pcs', 100) ;
	listHead('Shipments', 100) ;
	listHead('') ;
	while ($_row=dbFetch($_result)) {
		listRow() ;
		listField($_row['packed']) ;
		listField($_row['PackedQty']) ;
		listField($_row['OrderQty']) ;
	}
	dbQueryFree($_result) ;
	listEnd() ;
	
	echo '</td><td>' ;
	// Last 13 month list
	$_dateformat = "date_format(producedDate,'%Y-%m') as packed" ;
	$_datefilter = "date_format(producedDate, '%Y%m')>=date_format(DATE_ADD(CURRENT_DATE(), INTERVAL -12 Month), '%Y%m')" ;
	$_query =  sprintf($_querystr, $_dateformat, $_datefilter); //die($_query) ;
	$_result = dbQuery($_query);
	listStart () ;
	listHeader('Last 13 month') ;
	listHead('Month', 100) ;
	listHead('Shipped pcs', 100) ;
	listHead('Shipments', 100) ;
	listHead('Return pcs', 100) ;
	listHead('Return Shipments', 100) ;
	listHead('') ;
	while ($_row=dbFetch($_result)) {
		listRow() ;
		listField($_row['packed']) ;
		listField($_row['PackedQty']) ;
		listField($_row['OrderQty']) ;

		$_returnPcs = tableGetFieldWhere('refundline','sum(quantity)',"date_format(CreateDate, '%Y-%m')='".$_row['packed']."'");
		listField($_returnPcs . ' ('. number_format((100*$_returnPcs)/$_row['PackedQty'],0).'%)') ;
//		listField(tableGetFieldWhere('refundline','sum(quantity)',"date_format(CreateDate, '%Y-%m')='".$_row['packed']."'")) ;
//		listField(tableGetFieldWhere('refundline','sum(quantity)',"active=1 and orderquantityid>0 and date_format(CreateDate, '%Y-%m')='".$_row['packed']."'") ;
		listField(tableGetFieldWhere('refund','count(Id)',"date_format(CreateDate, '%Y-%m')='".$_row['packed']."'")) ;


	}
	dbQueryFree($_result) ;
	listEnd() ;
	echo '</td><td>' ;

	// Last 6 years list
	$_dateformat = "date_format(producedDate,'%Y') as packed" ;
	$_datefilter = "date_format(producedDate, '%Y')>=date_format(DATE_ADD(CURRENT_DATE(), INTERVAL -5 year), '%Y%m')" ;
	$_query =  sprintf($_querystr, $_dateformat, $_datefilter); //die($_query) ;
	$_result = dbQuery($_query);
	listStart () ;
	listHeader('Last 6 years') ;
	listHead('Month', 100) ;
	listHead('Shipped pcs', 100) ;
	listHead('Shipments', 100) ;
	listHead('Return pcs', 100) ;
	listHead('Return Shipments', 100) ;
	listHead('') ;
	while ($_row=dbFetch($_result)) {
		listRow() ;
		listField($_row['packed']) ;
		listField($_row['PackedQty']) ;
		listField($_row['OrderQty']) ;
		$_returnPcs = tableGetFieldWhere('refundline','sum(quantity)',"date_format(CreateDate, '%Y')='".$_row['packed']."'");
		listField($_returnPcs . ' ('. number_format((100*$_returnPcs)/$_row['PackedQty'],0).'%)') ;
//		listField(tableGetFieldWhere('refundline','sum(quantity)',"active=1 and orderquantityid>0 and date_format(CreateDate, '%Y-%m')='".$_row['packed']."'") ;
		listField(tableGetFieldWhere('refund','count(Id)',"date_format(CreateDate, '%Y')='".$_row['packed']."'")) ;


	}
	dbQueryFree($_result) ;
	listEnd() ;
	
	echo '</td></tr></table></center>' ;

	
?> 

<?php
return 0;
?>