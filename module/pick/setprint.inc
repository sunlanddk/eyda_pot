<script type="text/javascript" >
    function CheckAll (elem) {
        var col = document.appform.elements ;
        var n ;
        for (n = 0 ; n < col.length ; n++) {
            var e = col[n] ;
            if (e.type != 'checkbox') continue ;
            e.checked = elem.checked ;
        }
    }
</script>

<?php


    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
//    listHead ('Print', 30) ;
    ListHeadRaw (formCheckbox('FillAll', 0, '', 'onchange="CheckAll(this)"'),30,'align=left') ;
    listHead ('Order', 45)  ;
    listHead ('Owner', 120) ;
    listHead ('Customer', 150) ;
    listHead ('Number', 60) ;
    listHead ('Country', 40) ;
    listHead ('Delivery', 80) ;
    listHead ('Ordered', 70) ;
    listHead ('Packed', 60) ;
    listHead ('Remaining', 60) ;
	listHead ('Pick Lines', 70) ;
    listHead ('State', 60) ;
    listHead ('Last Depart', 70) ;
    listHead ('Picker', 95) ;
    $n = 0 ;
    while (($row = dbFetch($Result)) and $n<30) {
//    while (($row = dbFetch($Result))) {
        if (($_navigationParam=='all')) { // or ($_navigationParam=='norway')
            $showAll = 1;
        } else if ($_navigationParam=='all') {
            if ($row["State"]=='Printed') continue ;
            $_query = sprintf("select sum(Item.Quantity) as ItemQty from Item where Item.containerid=%d and Item.articleId=VariantCode.ArticleId and Item.articleColorId=VariantCode.ArticleColorId and Item.articleSizeId=VariantCode.ArticleSizeId and Item.active=1", (int)$row['PickContainerId'] );
            $_res = dbQuery($_query) ;
            $_incomplete = 0 ;
            while ($_row = dbFetch($_res)) {
                if ($row[OrderedQuantity]>$_row['ItemQty'])
                    $_incomplete = 1 ;
            }
            dbQueryFree($_res) ;
            if ($_incomplete) continue ;
            $n++ ;
        } else {
            if ($row["State"]=='Printed') continue ;
            $n++ ;
        }
		listRow () ;
		listFieldRaw (formCheckbox (sprintf('PrintFlag[%d]', (int)$row['Id']), 0, '')) ;
    	listField ($row["Reference"]) ;
    	listField ($row["OwnerCompanyName"]) ;
    	listField ($row["CompanyName"]) ;
    	listField ($row["CompanyNumber"]) ;
    	listField ($row["CountryName"]) ;
    	listField (date('Y-m-d', dbDateDecode($row['DeliveryDate']))) ;
    	listField ($row["Qty"]) ;
    	listField ($row["PackedQty"]) ;
		listField ($row["Remaining"]) ;
    	listField ($row["NoLines"]) ;
    	listField ($row["State"]) ;
		if ($row['DepartedDate']>'2001-01-01')
	    	listField (date('Y-m-d', dbDateDecode($row['DepartedDate']))) ;
		else
	    	listField ('') ;
    	listField ($row["PickUser"]) ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No PickOrders', 'colspan=5') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
