<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;
    require_once 'lib/barcode128.inc';
    $xmas = false;

    class BARCODEPDF extends PDF_Code128 {

    	function getWebshipperOrder($WebshipperId){
    		$ch = curl_init('https://eyda-aps.api.webshipper.io/v2/orders/'.$WebshipperId);
	        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '. _WEBSHIPPER_TOKEN_ , 'Content-Type: application/vnd.api+json'));
	        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($webshipper));
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	        $result = curl_exec($ch);    
	        $resultObj = json_decode($result, true);
	        if(isset($resultObj['data']) === true){

	        	return $webshipperdata = array(
		        	'shipping_method' => $resultObj['data']['attributes']['original_shipping']['shipping_name'],
		        	'name' => $resultObj['data']['attributes']['delivery_address']['att_contact'],
		        	'address1' => $resultObj['data']['attributes']['delivery_address']['address_1'],
		        	'address2' => $resultObj['data']['attributes']['delivery_address']['address_2'],
		        	'zip' => $resultObj['data']['attributes']['delivery_address']['zip'],
		        	'city' => $resultObj['data']['attributes']['delivery_address']['city'],
		        	'country_code' => $resultObj['data']['attributes']['delivery_address']['country_code'],
		        	'phone' => $resultObj['data']['attributes']['delivery_address']['phone'],
		        	'email' => $resultObj['data']['attributes']['delivery_address']['email'],
		        );
	        }
	        
	        return false;
    	}
	
	function Header() {
	    global $Record, $Company, $CompanyMy, $Now, $headerInformation, $prefix, $Navigation, $LastPage, $noHeaderInfo, $xmas ;
	    $this->SetAutoPageBreak(false) ; 

	    $query = sprintf ("SELECT oa.*, c.Name as countryName FROM orderadresses oa LEFT JOIN country c on c.Id=oa.CountryId WHERE oa.Active=1 AND oa.Type='shipping' AND oa.OrderId=%d", $Record['OrderId']) ;
	    $res = dbQuery ($query) ;
	    $orderaddress = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		if($Navigation["Parameters"] === "preview"){
			$orderaddress = array(
				"countryName" => "DK",
				"AltCompanyName" => "CompanyName",
				"Address1" => "test address 1",
				"Address2" => "test address 2",
				"ZIP" 		=> "2300",
				"City"      => "Copnehagen",
				"PackageShop" => "package shop"
			);
			$Record["WebshipperId"]	= 'test';
			// $Record["OrderReference"]	= "test order ref";
			$Record["oEmail"]	= "test email";
			$Record["oPhone"]	= "test phone";
			$Record["ShippingShop"] = "test shippingshop";

		}


	    // $webshipperData = $this->getWebshipperOrder($Record['WebshipperId']);
	    // if($webshipperData === false){
	    // 	echo 'Webshipper not responding.';
	    // 	die();
	    // }


        if ($noHeaderInfo) {

            $ImageString = sprintf ('image/logo.jpg') ;
            $this->Image ($ImageString, 15, 10, 64) ;
            $this->Code128(148, 10, $Record["OrderReference"], 40, 14);
            $this->SetMargins(146,0,0) ;
            $this->SetFont('Arial','',8);
            $this->setXY(148, 25);
            $this->Cell(40, 4, $Record['WebshipperId'], '', '', 'C') ;
            $this->SetY (34) ;
            $this->SetX(10);
            $this->SetMargins(15,51,6) ;
            //$this->SetY (117) ;
            $this->SetAutoPageBreak(true, 30) ; 
            return;
        }

        if ($LastPage) {

            $this->SetY (11) ;
            $this->SetFont('Arial','',16);
            $this->Cell(10, 4, $headerInformation[$prefix]['order'].' # '.$Record['OrderReference'], 0, 1) ;

            $this->Code128(148, 10, $Record['WebshipperId'], 40, 14);
            $this->SetMargins(146,0,0) ;
            $this->SetFont('Arial','',8);
            $this->setXY(148, 25);
            $this->Cell(40, 4, $Record['WebshipperId'], '', '', 'C') ;
            $this->SetY (34) ;
            $this->SetFont('Arial','',8);


            $this->SetFont('Arial','',11);
            $this->SetMargins(15,51,6) ;
            //$this->SetY (117) ;
            $this->SetAutoPageBreak(true, 30) ; 
            return;
        }


	    $ImageString = sprintf ('image/logo.jpg') ;
	    if ($xmas) {
	    	$this->Image ($ImageString, 15, 10, 36) ;
	    } else {
	    	$this->Image ($ImageString, 15, 10, 64) ;
	    }
	    
	    $this->Code128(148, 10, $Record['WebshipperId'], 40, 14);
	    $this->SetMargins(146,0,0) ;
	    $this->SetFont('Arial','',8);
	    $this->setXY(148, 25);
	    $this->Cell(40, 4, $Record['WebshipperId'], '', '', 'C') ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);

	    // cell felter: feltbredde, felthøjde, indhold, ramme, linieskifte, Align
		
	    // Title text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(14,0,0) ;
	    if ($xmas) {
	    	$this->SetY (23) ;
	    } else {
	    	$this->SetY (45) ;
	    }
	    
		// if (($Record['Packed']) AND ($Record['ToId']>0)) 
		//     $this->Cell(30, 8, 'PICK ORDER CLOSED', 0, 1) ;
		// else
		//     $this->Cell(30, 8, 'PICK ORDER', 0, 1) ;
	 //    $this->SetFont('Arial','',9);
	 //    $this->SetMargins(20,0,0) ;

	    $this->SetFont('Arial','',16);
	    if ($xmas) {
	    	$this->Cell(50, 3, $headerInformation[$prefix]['order'].' # '.$Record['OrderReference'], 0, 1) ;
	    } else {
	    	$this->Cell(50, 4, $headerInformation[$prefix]['order'].' # '.$Record['OrderReference'], 0, 1) ;
	    }   
	    $this->SetMargins(17,0,0) ;

	    // Header
	    if ($xmas) {
	    	$y = $this->getY();
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->setXY(15, $y+=5);
		    $headerY = $y;
		    $this->SetFont('Arial','B',10);
		    $this->MultiCell(90, 5, $headerInformation[$prefix]['receiver'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',10);
		    $this->SetTextColor(0,0,0) ;

		    $this->setXY(15,$y+=6);
		    $this->Cell(30, 2, $orderaddress['AltCompanyName']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 2, $orderaddress['Address1']) ;
		    $this->setXY(15,$y+=4);
		    if($orderaddress['Address2'] !== ''){
		    	$this->Cell(30, 2, $orderaddress['Address2']) ;
		    	$this->setXY(15,$y+=4);
		    }
		    $this->Cell(30, 2, $orderaddress['ZIP'].' '.$orderaddress['City']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 2, $orderaddress['countryName']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 2, $Record['oEmail']) ;
		    $this->setXY(15,$y+=4);
		    $this->Cell(30, 3, $Record['oPhone']) ;


		    $this->setXY(105,$headerY);
		    $y = $headerY;
		    $this->SetFont('Arial','B',10);
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->MultiCell(90, 5, $headerInformation[$prefix]['details'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',10);
		    $this->SetTextColor(0,0,0) ;
		    $this->setXY(105,$y+=7);
		    $this->Cell(30, 2, $headerInformation[$prefix]['date'].': '.date('Y-m-d H:i:s', strtotime($Record['CreateDate']))) ;
		    $this->setXY(105,$y+=4);
		    $this->MultiCell(80, 4, $headerInformation[$prefix]['shipping'].': '.$Record['ShippingShop']) ;


		    $this->SetMargins(15,0,0) ;
		    $this->SetX(15) ;
		    $this->SetY(65) ;
		    
		    $this->SetFillColor(150,150,150) ;
		    $this->SetDrawColor(150,150,150) ;
		    $this->MultiCell(180, 5, '', 1, '', 1) ;
		    $this->SetX(15) ;
		    $this->SetY(65.5) ;
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFont('Arial','B',10);
		    $this->Cell(35, 4, $headerInformation[$prefix]['sku']) ;
		    $this->Cell(95, 4, $headerInformation[$prefix]['product']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['position']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['amount'], 0, 0, 'C') ;
		    $this->SetTextColor(0,0,0) ;

		    // Initialize for main page
		    $this->SetFont('Arial','',11);
		    $this->SetMargins(15,51,6) ;
		    $this->SetY (70) ;
		    $this->SetAutoPageBreak(true, 30) ; 
	    } else {
	    	$y = $this->getY();
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->setXY(15, $y+=5);
		    $headerY = $y;
		    $this->SetFont('Arial','B',12);
		    $this->MultiCell(90, 7, $headerInformation[$prefix]['receiver'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',12);
		    $this->SetTextColor(0,0,0) ;

		    $this->setXY(15,$y+=8);
		    $this->Cell(30, 4, $orderaddress['AltCompanyName']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $orderaddress['Address1']) ;
		    $this->setXY(15,$y+=5);
		    if($orderaddress['Address2'] !== ''){
		    	$this->Cell(30, 4, $orderaddress['Address2']) ;
		    	$this->setXY(15,$y+=5);
		    }
		    $this->Cell(30, 4, $orderaddress['ZIP'].' '.$orderaddress['City']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $orderaddress['countryName']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $Record['oEmail']) ;
		    $this->setXY(15,$y+=5);
		    $this->Cell(30, 4, $Record['oPhone']) ;


		    $this->setXY(105,$headerY);
		    $y = $headerY;
		    $this->SetFont('Arial','B',12);
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFillColor(150,150,150) ;
		    $this->MultiCell(90, 7, $headerInformation[$prefix]['details'], 0, 'L', 1) ;
		    $this->SetFont('Arial','',12);
		    $this->SetTextColor(0,0,0) ;
		    $this->setXY(105,$y+=8);
		    $this->Cell(30, 4, $headerInformation[$prefix]['date'].': '.date('Y-m-d H:i:s', strtotime($Record['CreateDate']))) ;
		    $this->setXY(105,$y+=5);
		    $this->MultiCell(80, 4.5, $headerInformation[$prefix]['shipping'].': '.$Record['ShippingShop']) ;


		    $this->SetMargins(15,0,0) ;
		    $this->SetX(15) ;
		    $this->SetY(110) ;
		    
		    $this->SetFillColor(150,150,150) ;
		    $this->SetDrawColor(150,150,150) ;
		    $this->MultiCell(180, 7, '', 1, '', 1) ;
		    $this->SetX(15) ;
		    $this->SetY(111.5) ;
		    $this->SetTextColor(255,255,255) ;
		    $this->SetFont('Arial','B',12);
		    $this->Cell(35, 4, $headerInformation[$prefix]['sku']) ;
		    $this->Cell(95, 4, $headerInformation[$prefix]['product']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['position']) ;
		    $this->Cell(25, 4, $headerInformation[$prefix]['amount'], 0, 0, 'C') ;
		    $this->SetTextColor(0,0,0) ;

		    // Initialize for main page
		    $this->SetFont('Arial','',11);
		    $this->SetMargins(15,51,6) ;
		    $this->SetY (117) ;
		    $this->SetAutoPageBreak(true, 30) ; 
	    }
	    
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

        $this->SetY(-15);
        $this->SetX(180);
        $this->SetFont('Arial','',8);
        $this->Cell(173,10, ' Side '. $this->PageNo() .' af {nb}', 0, 0);

	}
	
	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }

	$contentDa = json_decode(tableGetField('pickorder_texts', 'content', 1), true);
	$contentEu = json_decode(tableGetField('pickorder_texts', 'content', 2), true);
	$contentSe = json_decode(tableGetField('pickorder_texts', 'content', 4), true);
	$contentNo = json_decode(tableGetField('pickorder_texts', 'content', 3), true);
	$contentAm = json_decode(tableGetField('pickorder_texts', 'content', 5), true);
	$contentNl = json_decode(tableGetField('pickorder_texts', 'content', 6), true);



    $headerInformation = array(
    	// 'da' => array(
    	// 	'order' => 'Ordre',
    	// 	'receiver' => 'Modtager',
    	// 	'details' => 'Detaljer',
    	// 	'date' => 'Ordre dato',
    	// 	'shipping' => 'Fragtmetode',
    	// 	'sku' => 'Varenummer',
    	// 	'product' => 'Vare',
    	// 	'position' => 'Position',
    	// 	'amount' => 'Antal',
    	// 	'header1' => '',
    	// 	'footer1' => 'Mange tak for din bestilling.',
    	// 	'header2' => '',
    	// 	'footer2' => 'Vi håber, du bliver glad for dine EYDA-styles. Hvis du har spørgsmål, er du meget velkommen til at skrive til os på hello@eyda.com, så hjælper vi dig på vej.',
    	// 	'header3' => 'Returnering:',
    	// 	'footer3' => 'For gratis at returnere dine varer, skal du besøge vores returportal www.eyda.dk/retur. Hav dit ordrenummer og din e-mail klar for at registrere din returforsendelse. Returnering skal ske senest 30 dage efter modtagelse, og varerne skal være ubrugte og i original emballage.',
    	// 	'header4' => '',//'Er din ordre en julegave?',
    	// 	'footer4' => '',//'Vi har udvidet returmulighederne for julegaver købt efter 1. november 2021. Hvis julegaven skal byttes, har gavemodtager to valg: ombytning til gavekort eller returnering.',
    	// 	'header5' => '',
    	// 	'footer5' => '',//'Ved ombytning til gavekort, skal gavemodtageren skrive direkte til os på hello@eyda.com senest 15. januar 2022. Skal gaven ombyttes til gavekort, skal det altså ikke registreres i vores returportal. ',
    	// 	'header6' => '',
    	// 	'footer6' => '',//'Skal gaven returneres og beløbet gives retur, skal det gøres via vores returportal på www.eyda.dk/retur. Beløbet vil blive tilbageført til betalingskortet, der er brugt ved købet.',
    	// ),
		'da' => array(
    		'order' => 'Ordre',
    		'receiver' => 'Modtager',
    		'details' => 'Detaljer',
    		'date' => 'Ordre dato',
    		'shipping' => 'Fragtmetode',
    		'sku' => 'Varenummer',
    		'product' => 'Vare',
    		'position' => 'Position',
    		'amount' => 'Antal',
    		'header1' => $contentDa["header1"],
    		'footer1' => $contentDa["line1"],
    		'header2' => $contentDa["header2"],
    		'footer2' => $contentDa["line2"],
    		'header3' => $contentDa["header3"],
    		'footer3' => $contentDa["line3"],
    		'header4' => $contentDa["header4"],
    		'footer4' => $contentDa["line4"],
    		'header5' => $contentDa["header5"],
    		'footer5' => $contentDa["line5"],
    		'header6' => $contentDa["header6"],
    		'footer6' => $contentDa["line6"],
            'header7' => $contentDa["header7"],
            'footer8' => $contentDa["linegift"],
    	),
    	'eu' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => $contentEu["header1"],
    		'footer1' => $contentEu["line1"],
    		'header2' => $contentEu["header2"],
    		'footer2' => $contentEu["line2"],
    		'header3' => $contentEu["header3"],
    		'footer3' => $contentEu["line3"],
    		'header4' => $contentEu["header4"],
    		'footer4' => $contentEu["line4"],
    		'header5' => $contentEu["header5"],
    		'footer5' => $contentEu["line5"],
    		'header6' => $contentEu["header6"],
    		'footer6' => $contentEu["line6"],
            'header7' => $contentEu["header7"],
            'footer8' => $contentEu["linegift"],
    	),
    	'se' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => $contentSe["header1"],
    		'footer1' => $contentSe["line1"],
    		'header2' => $contentSe["header2"],
    		'footer2' => $contentSe["line2"],
    		'header3' => $contentSe["header3"],
    		'footer3' => $contentSe["line3"],
    		'header4' => $contentSe["header4"],
    		'footer4' => $contentSe["line4"],
    		'header5' => $contentSe["header5"],
    		'footer5' => $contentSe["line5"],
    		'header6' => $contentSe["header6"],
    		'footer6' => $contentSe["line6"],
            'header7' => $contentSe["header7"],
            'footer8' => $contentSe["linegift"],
    	),
    	'no' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
			'header1' => $contentNo["header1"],
    		'footer1' => $contentNo["line1"],
    		'header2' => $contentNo["header2"],
    		'footer2' => $contentNo["line2"],
    		'header3' => $contentNo["header3"],
    		'footer3' => $contentNo["line3"],
    		'header4' => $contentNo["header4"],
    		'footer4' => $contentNo["line4"],
    		'header5' => $contentNo["header5"],
    		'footer5' => $contentNo["line5"],
    		'header6' => $contentNo["header6"],
    		'footer6' => $contentNo["line6"],
            'header7' => $contentNo["header7"],
            'footer8' => $contentNo["linegift"],
    	),
    	'am' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => $contentAm["header1"],
    		'footer1' => $contentAm["line1"],
    		'header2' => $contentAm["header2"],
    		'footer2' => $contentAm["line2"],
    		'header3' => $contentAm["header3"],
    		'footer3' => $contentAm["line3"],
    		'header4' => $contentAm["header4"],
    		'footer4' => $contentAm["line4"],
    		'header5' => $contentAm["header5"],
    		'footer5' => $contentAm["line5"],
    		'header6' => $contentAm["header6"],
    		'footer6' => $contentAm["line6"],
            'header7' => $contentAm["header7"],
            'footer8' => $contentAm["linegift"],
    	),
    	'nl' => array(
    		'order' => 'Order',
    		'receiver' => 'Recipient',
    		'details' => 'Details',
    		'date' => 'Order date',
    		'shipping' => 'Shipping method',
    		'sku' => 'SKU',
    		'product' => 'Product',
    		'position' => 'Position',
    		'amount' => 'Quantity',
    		'header1' => $contentNl["header1"],
    		'footer1' => $contentNl["line1"],
    		'header2' => $contentNl["header2"],
    		'footer2' => $contentNl["line2"],
    		'header3' => $contentNl["header3"],
    		'footer3' => $contentNl["line3"],
    		'header4' => $contentNl["header4"],
    		'footer4' => $contentNl["line4"],
    		'header5' => $contentNl["header5"],
    		'footer5' => $contentNl["line5"],
    		'header6' => $contentNl["header6"],
    		'footer6' => $contentNl["line6"],
            'header7' => $contentNl["header7"],
            'footer8' => $contentNl["linegift"],
    	)
    );
    $prefix = $Record['ShopifyName'];

	if($Navigation["Parameters"] === "preview") {
		$prefix = $_GET["store"];
	}
    // Variables
    $LastPage = false ;
    // Make PDF
    $pdf=new BARCODEPDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->SetAutoPageBreak(true, 15) ; 
    $pdf->AddPage() ;
	
    $Street = ''; 
    $hl=0 ;
	$_oneQtyPerLine=0 ;
    $pdf->setDrawColor(150,150,150);

    if ($xmas) {
		$lineheight = 7;
	} else {
		$lineheight = 8;
	}

	if($Navigation["Parameters"] === "preview") {
        while ($i < 10) {
        	
            $pdf->Cell(35, $lineheight,'no variant',1) ;
            $_desc = 'test black / s ';
            $pdf->Cell(95, $lineheight, substr($_desc,0,45),1) ;
            $pdf->Cell(25, $lineheight, 'test',1) ;
            $pdf->Cell(25, $lineheight, 1, 1, 0, 'C') ;
            $pdf->Ln () ;
            $i++;
        }
			

	}else {
		while ($row = dbFetch($Result)) {
			$dl = 45;
			$qty = $row['OrderedQuantity'] ; // 2022-11-19 Allways print lines -$row['PickedQuantity']-$row['PackedQuantity'];
			while ($qty > 0) {
				$pdf->Cell(35, $lineheight, $row['VariantCode'],1) ;
				$_desc = $row['VariantDescription'] . ' ' . $row['VariantColor'] . ' / ' . $row['VariantSize'];
				$pdf->Cell(95, $lineheight, substr($_desc,0,$dl),1) ;
				$pdf->Cell(25, $lineheight, $row['Position'],1) ;
				if ($_oneQtyPerLine==1) {
					$pdf->Cell(25, $lineheight, 1, 1, 0, 'C') ;
					$pdf->Ln () ;	
					$qty--;
				} else {
					$pdf->Cell(25, $lineheight, $qty, 1, 0, 'C') ;
					$pdf->Ln () ;	
					$qty=0;
				}

			}
		}
	}

    $pdf->Ln () ;
    if ($xmas) {
    	$noHeaderInfo = true;
    }
    
    
    if($headerInformation[$prefix]['header1'] != ''){
    	$pdf->SetFont('Arial','B',11);
    	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header1']), 0,'C') ;
    	$pdf->SetFont('Arial','',11);
    }
    if($headerInformation[$prefix]['footer1'] != ''){
    	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['footer1']), 0,'C') ;
    }
    if($headerInformation[$prefix]['header2'] != ''){
    	$pdf->Ln () ;
    	$pdf->SetFont('Arial','B',11);
    	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header2']), 0,'C') ;
    	$pdf->SetFont('Arial','',11);
    }
    else{
    	$pdf->Ln () ;	
    }
    if($headerInformation[$prefix]['footer2'] != ''){
    	$pdf->MultiCell(180, 4.5, utf8_decode($headerInformation[$prefix]['footer2']), 0,'C') ;
    }
    if($headerInformation[$prefix]['header3'] != ''){
    	$pdf->Ln () ;
    	$pdf->SetFont('Arial','B',11);
    	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header3']), 0,'C') ;
    	$pdf->SetFont('Arial','',11);
    }
    else{
    	$pdf->Ln () ;	
    }
    if($headerInformation[$prefix]['footer3'] != ''){
    	$pdf->MultiCell(180, 4.5, utf8_decode($headerInformation[$prefix]['footer3']), 0,'C') ;
    }
    if($headerInformation[$prefix]['header4'] != ''){
    	$pdf->Ln () ;
    	$pdf->SetFont('Arial','B',11);
    	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header4']), 0,'C') ;
    	$pdf->SetFont('Arial','',11);
    }
    else{
    	$pdf->Ln () ;	
    }
    if($headerInformation[$prefix]['footer4'] != ''){
	    $pdf->MultiCell(180, 4.5, utf8_decode($headerInformation[$prefix]['footer4']), 0,'C') ;
		}
		if($headerInformation[$prefix]['header5'] != ''){
			$pdf->Ln () ;
			$pdf->SetFont('Arial','B',11);
	  	$pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header5']), 0,'C') ;
	  	$pdf->SetFont('Arial','',11);
	  }
	  else{
	  	$pdf->Ln () ;	
	  }
		if($headerInformation[$prefix]['footer5'] != ''){
		    $pdf->MultiCell(180, 4.5, utf8_decode($headerInformation[$prefix]['footer5']), 0,'C') ;
		}

		if ($xmas) {
			if($headerInformation[$prefix]['header6'] != ''){
            $LastPage = true;
            $noHeaderInfo = false;
            $pdf->AddPage();

			$pdf->Ln () ;
			
            //xmas thing
            if($headerInformation[$prefix]['footer8'] != ''){
                //$pdf->Ln () ;
                //$pdf->SetFont('Arial','',11);
                $pdf->SetFont('Arial','B',16);
                $currY = $pdf->getY();
                $pdf->setY($currY-26);
                $pdf->MultiCell(180, 18, utf8_decode($headerInformation[$prefix]['footer8']), 0,'L') ;
                $pdf->setY($currY);
            }
            $pdf->SetFont('Arial','B',11);
	  	    $pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header6']), 0,'C') ;
	  	    $pdf->SetFont('Arial','',11);
		}
		
	  }
	  else{
	  	$pdf->Ln () ;	
	  }
		if ($xmas) {
				if($headerInformation[$prefix]['footer6'] != ''){
			    //$pdf->Ln () ;
			    $pdf->MultiCell(180, 4.5, utf8_decode($headerInformation[$prefix]['footer6']), 0,'C') ;
			}

	        if($headerInformation[$prefix]['header7'] != ''){
	            $pdf->Ln () ;
	            $pdf->SetFont('Arial','B',11);
	        $pdf->MultiCell(180, 4, utf8_decode($headerInformation[$prefix]['header7']), 0,'C') ;
	        $pdf->SetFont('Arial','',11);
	      } else {
	        $pdf->Ln () ;
	      }
		} 

     
    // Last Page generated
    $LastPage = True ;
    // Generate PDF document
  	// die('test '. $pdf->PageNo()) ;  
    $pdfdoc = $pdf->Output('', 'S') ;
	if ($_singleGTINBatch==1) {
		$file = fileName ('pick', (int)$Record['Id'], true) ;
		$resource = fopen ($file, 'w') ;
		fwrite ($resource, $pdfdoc) ;
		fclose ($resource) ;
		chmod ($file, 0640) ;

		$fileinfo['localePath'] = $file;
		$_wsid = (int)TableGetField('login','WorkstationId', (int)$Login['Id']) ;
		//$_wsid = 1 ;

		if ($_wsid>0) {
			if ($Config['Instance']=='Production') {
				$_folder = 'NetSolution/CU1' ;
			} else {
				$_folder = 'Test/Alex';
			}
			
			$_ws = TableGetField('Workstation','Name', $_wsid) ;

			$fileinfo['ftpPath'] = $_folder.'/'. $_ws .'_Delivery' . substr($_ws, 2, 2) . '_' . (int)$Record['Id'].'.pdf';
//			$fileinfo['ftpPath'] = $_folder.'/'. $_ws .'_Delivery' . substr($_ws, 2, 2) . '_' . (int)$Id.'.pdf';
			$logininfo['host'] = 'vion.dk';
			$logininfo['usr'] = 'vion.dk04';
			$logininfo['pwd'] = 'Nutrinic1' ;
			ftpUpload($logininfo, $fileinfo, true) ;
		}

	} else {
	    // Download
	    if (headers_sent()) return 'stop' ;
	    httpNoCache ('pdf') ;
	    httpContent ('application/pdf', sprintf('PickOrder%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
	    print ($pdfdoc) ; 
	}

	dbQueryFree ($Result) ;
	if($Navigation["Parameters"] === "preview") {

	}else {
		// Update PickOrder.
		$PickOrder = array (
			'Printed' => 1,
			'Updated' => 0
		) ;
		tableWrite ('PickOrder', $PickOrder, (int)$Record['Id']) ;

	}
	


    return 0 ;
?>
