<?php

use function GuzzleHttp\json_encode;

require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;

    $content = json_decode($Record['Content'], true);

    itemFieldRaw ('Header1', formText ('header1', utf8_decode($content['header1']),400)) ;
    itemFieldRaw ('Line1', formText ('line1',utf8_decode($content['line1']),400)) ;
    itemSpace () ;


    itemFieldRaw ('Header2', formText ('header2', utf8_decode($content['header2']), 400)) ;
    itemFieldRaw ('Line2', formText ('line2', utf8_decode($content['line2']), 400)) ;
    itemSpace () ;


    itemFieldRaw ('Header3', formText ('header3', utf8_decode($content['header3']), 400)) ;
    itemFieldRaw ('Line3', formText ('line3',utf8_decode($content['line3']), 400)) ;
    itemSpace () ;


    itemFieldRaw ('Header4', formText ('header4', utf8_decode($content['header4']), 400)) ;
    itemFieldRaw ('Line4', formText ('line4', utf8_decode($content['line4']), 400)) ;
    itemSpace () ;


    itemFieldRaw ('Header5', formText ('header5',utf8_decode($content['header5']), 400)) ;
    itemFieldRaw ('Line5', formText ('line5', utf8_decode($content['line5']), 400)) ;
    itemSpace () ;


    itemFieldRaw ('Header6', formText ('header6', utf8_decode($content['header6']),400)) ;
    itemFieldRaw ('Line6', formText ('line6', utf8_decode($content['line6']), 400)) ;


    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
