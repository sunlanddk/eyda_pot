<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/variant.inc' ;
    require_once 'lib/webshipper.inc' ;
    

	global $User, $Navigation, $Record, $Size, $Id ;

	$_pickOrderId = $Id;

	$_salesOrderId = $Record['ReferenceId'];

	$_singleGTINBatch = (int)$Record['singleGTINBatch'] ;
	$_BatchId = (int)$Record['BatchId'] ;

	$_preinvoiceid = tableGetField('`order`','PreInvoiceId', $Record['ReferenceId']) ;
	$_reforderid = $Record['ReferenceId'] ;

	$query = 'SELECT Id FROM `order` WHERE `order`.Active=1 AND `order`.ConsolidatedId='.$Record['ConsolidatedId'];
	$row = dbQuery($query);
	$ordersArray = array();
	while ($orders = dbFetch($row)) {
		$ordersArray[] = $orders['Id'];
	}
	$_orderid = implode(',', $ordersArray);
	
	// Is anything scanned?
	switch ($Navigation['Parameters']) {
		case 'ship' :
		case 'partship' :
			break ;
		default:
		if (!is_array($_POST['PickQty'])) return 'No items to be packed' ;
	}

	$TotalQuantity=0;
	foreach ($_POST['PickQty'] as $VariantCode => $qty) {
		$TotalQuantity += $qty ;
	}

    // Get Fields
    $fields = array (
		'ContainerId'		=> array ('type' => 'integer'),
		'StockId'			=> array ('type' => 'integer'),
		'ContainerTypeId'	=> array ('type' => 'integer'),
		'Position'			=> array (),
		'GrossWeight'		=> array ('type' => 'decimal',	'format' => '12.3'),
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
	set_time_limit (300) ;
	if ($TotalQuantity>0) { // Only do anything with items and containers if something is scanned.
		// Container creation and validation
		if ($fields['ContainerId']['value'] > 0) {
			// Existing Container !
			// Lookup information
			$query = sprintf ('SELECT Container.*, Stock.Name AS StockName, Stock.Ready AS StockReady FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $fields['ContainerId']['value']) ;
			$res = dbQuery ($query) ;
			$Container = dbFetch ($res) ;
			dbQueryFree ($res) ;
			if ((int)$Container['Id'] != $fields['ContainerId']['value']) return sprintf ('Container %d not found', $fields['ContainerId']['value']) ;
			if ($Container['StockReady']) return sprintf ('no operations allowed on Container %d', (int)$Container['Id']) ;
			// Any Gross Weight?
			if ($fields['GrossWeight']['value'] == 0) return 'please type new Gross weight for Container after adding items' ;
			$Container_update = array (
				'GrossWeight' => $fields['GrossWeight']['value'],
			) ;
			tableWrite ('Container', $Container_update, (int)$Container['Id']) ;
			// 	$fields['StockId']['value'] = $Container['StockId'] ;
	 
		} else {
			// New Container
			// Any Stock location ?
			// if ($fields['StockId']['value'] == 0) return 'please select new Location' ;
			if ($fields['StockId']['value'] == 0) {
				// Make new shipment
				if ($Record['Type']=='SalesOrder') {
					$query = sprintf ('SELECT * FROM `Order` WHERE Id=%d AND Active=1', $Record['ReferenceId']) ;
					$result = dbQuery ($query) ;
					$Order = dbFetch ($result) ;
					dbQueryFree ($result) ;
					$shipment = array (
						'Name'				=> sprintf('%s SO: %s CO: %s', $Record['Type'], $Record['Reference'], $Record['ConsolidatedId']),
						'Description'		=> $Record['OrderTypeId']==10?'@':'',
						'DepartureDate'		=> $Record['DeliveryDate'],			    
						'CompanyId'			=> $Record['CompanyId'],
						'FromCompanyId'		=> $Record['OwnerCompanyId'],
						'Type'				=> 'shipment',
						'DeliveryTermId'	=> $Order['DeliveryTermId'],
						'CarrierId'			=> $Order['CarrierId'],
						'AltCompanyName'	=> $Order['AltCompanyName'],
						'Address1'			=> $Order['Address1'],
						'Address2'			=> $Order['Address2'],
						'ZIP'				=> $Order['ZIP'],
						'City'				=> $Order['City'],
						'CountryId'			=> $Order['CountryId']
					) ;
				} else {
					$shipment = array (
						'Name'				=> sprintf('%s SO: %s CO: %s', $Record['Type'], $Record['Reference'], $Record['ConsolidatedId']),
						'Description'		=> '',
						'DepartureDate'		=> $Record['DeliveryDate'],			    
						'CompanyId'			=> $Record['CompanyId'],
						'FromCompanyId'		=> $Record['OwnerCompanyId'],
						'Type'				=> 'shipment',
						'DeliveryTermId'	=> TableGetField('Company','DeliveryTermId',$Record['CompanyId']),
						'CarrierId'			=> TableGetField('Company','CarrierId',$Record['CompanyId']),
						'Address1'			=> TableGetField('Company','DeliveryAddress1',$Record['CompanyId']),
						'Address2'			=> TableGetField('Company','DeliveryAddress2',$Record['CompanyId']),
						'ZIP'				=> TableGetField('Company','DeliveryZip',$Record['CompanyId']),
						'City'				=> TableGetField('Company','DeliveryCity',$Record['CompanyId']),
						'CountryId'			=> TableGetField('Company','DeliveryCountryId',$Record['CompanyId'])
					) ;
				}
				$fields['StockId']['value']=tableWrite ('Stock', $shipment) ;
			}	
			// Get Stock
			$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
			$result = dbQuery ($query) ;
			$Stock = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
			if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on Stock for new Container, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

			// Any ContainerType ?
			if ($fields['ContainerTypeId']['value'] == 0) return 'please select type for new Container' ;

			// Any Gross Weight?
			if ($fields['GrossWeight']['value'] == 0) 
				$fields['GrossWeight']['value'] = 1 ;
				//return 'please type Gross weight for new Container' ;

			// Get ContainerType
			$query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $fields['ContainerTypeId']['value']) ;
			$result = dbQuery ($query) ;
			$ContainerType = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ((int)$ContainerType['Id'] != $fields['ContainerTypeId']['value']) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $fields['ContainerTypeId']['value']) ;

			// Create new Container
			$Container = array (
				'StockId' => (int)$Stock['Id'],
				'ContainerTypeId' => (int)$ContainerType['Id'],
				'Position' => $fields['Position']['value'],
				'TaraWeight' => $ContainerType['TaraWeight'],
				'GrossWeight' => $fields['GrossWeight']['value'],
				'Volume' => $ContainerType['Volume']
			) ;
			$Container['Id'] = tableWrite ('Container', $Container) ;
			$Container['StockName'] = $Stock['Name'] ;
		}
		// END CONTAINER VALIDATION AND CREATION

		// Create ItemOperation referance
		$ItemOperation = array (
			'Description' => sprintf ('Items picked to Container %d at %s', (int)$Container['Id'], $Container['StockName'])
		) ;
		$ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
			
		$Error_txt=0 ;

		$transaction = array (
			'Type'				=> 'Ship',
			'ObjectId'			=> (int)$Stock['Id'] ,
			'FromStockId'		=> 14321,
			'ToStockId'			=> (int)$Stock['Id'],
			'Value'				=> 0, 
			'Quantity'			=> 0 
		) ;

		$_transactionid = tableWrite('transaction', $transaction) ;

		// Process scanned quantities for each pick line
		foreach ($_POST['PickQty'] as $VariantCode => $_consolidatedqty) {
			if ($_consolidatedqty==0) continue ; // Nothing picked: next line
			// multible consolidated picklines per variant
			$_consolidated=explode(',',$_POST['PickOrderLineId'][$VariantCode]) ;
			$_consolidatedlines = count($_consolidated) ;
			foreach ($_consolidated as $_pickorderlineid) {
				if ($_consolidatedqty<=0) continue ; // No need on this line: next line

				// Get Variant Information
				$query = sprintf ('SELECT * FROM pickorderline WHERE Id=%d', $_pickorderlineid) ;
				$res = dbQuery ($query) ;
				$_pickorderline = dbFetch ($res) ;
				dbQueryFree ($res) ;
				$_pickorderlineqty = (int)$_pickorderline["OrderedQuantity"]-(int)$_pickorderline["PackedQuantity"] ; // -(int)$_pickorderline["PickedQuantity"] ;
				if ($_pickorderlineqty<=0) continue ; // No need on this line: next line
			
				if ($_consolidatedlines > 1) {
					$qty = ($_consolidatedqty>$_pickorderlineqty)? $_pickorderlineqty : $_consolidatedqty ;
					$_consolidatedqty -= $qty ;
				} else {
					$qty = $_consolidatedqty ; // Allow surplus on last line
				}
				$_consolidatedlines-- ;
					// New code end			
				// process each consolidated pickline.
				if ($VariantCode <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;

				// Get Variant Information
				$query = sprintf ('SELECT VariantCode.*, Container.StockId as PickStockId
								FROM VariantCode 
								LEFT JOIN Container ON VariantCode.PickContainerId=Container.Id AND Container.Active=1
								WHERE VariantCode.VariantCode="%s" AND VariantCode.Active=1', $VariantCode) ;
				$res = dbQuery ($query) ;
				$VariantCodeInfo = dbFetch ($res) ;
				dbQueryFree ($res) ;
			
				// Is the PickStock the items main pick stock? If not use aleternative pickstock
				if (!($VariantCodeInfo['PickStockId'] == $Record['FromId']) AND !($Record['FromId']==0)) 
					$VariantCodeInfo['PickContainerId'] = 0;	// Future opt: Use containerid from pickcontainer table (variantid, container)		

				// decrease item qty in pick location (in pick container) or make new item.
				if (($VariantCodeInfo['PickContainerId'] == 0) and ($Record['Type']=='SalesOrder'))
					$query = sprintf ('SELECT Item.* FROM Item, Container WHERE Item.ContainerId=Container.Id And Container.StockId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Item.Active=1 order by createdate',
								 $Record['FromId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
				else
					$query = sprintf ('SELECT * FROM Item WHERE ContainerId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Active=1 order by createdate',
								 $VariantCodeInfo['PickContainerId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
				$res = dbQuery ($query) ;

				$PickedQuantity = 0;						
				if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
					$RemScannedQty = $qty * $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']] ;
				else
					$RemScannedQty = $qty ;
				$ReqQty=$RemScannedQty ;

				// Allocate Items to sales orderlines for pickorders based on salesorders.
				if ($Record['Type']=='SalesOrder') {
					If ((int)$_pickorderline["QlId"]>0) {
						$query = sprintf ('SELECT OrderQuantity.OrderLineId as Id 
											FROM OrderQuantity 
											WHERE OrderQuantity.Id=%d',(int)$_pickorderline["QlId"]) ;
					} else {
						$query = sprintf ('SELECT OrderLine.Id as Id 
											FROM OrderLine
											LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.id And OrderQuantity.ArticleSizeId=%d AND OrderQuantity.Active=1 And OrderQuantity.Quantity>0
											WHERE	OrderLine.OrderId IN(%s) And OrderLine.ArticleId=%d AND 
													OrderLine.ArticleColorId=%d AND OrderLine.Active=1  AND OrderLine.Done=0',
													$VariantCodeInfo['ArticleSizeId'],$_orderid,
													$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'] ) ;
					}
					$olres = dbQuery ($query) ;
					$OrderLine = dbFetch ($olres) ;
					dbQueryFree ($olres) ;
					if ($OrderLine['Id']>0) {
						$IOrderLineId = $OrderLine['Id'] ;	
					} else {
						return sprintf('SKU %s in pickorder but not found in salesorder %d',
									$VariantCode, $Record['ReferenceId'],
									$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId']) ;
					}
				}
			
				// Find items on stock to fulfil registered qty.
				while ($Item = dbFetch ($res)) {
					if ($Item['Id'] > 0) {
						if (($Item['Quantity'] > $RemScannedQty) or ($Item['Quantity'] == $RemScannedQty)) {
							$PickedQuantity += $RemScannedQty ;

							// Split Item and create new item with complete qty.
							$Item['Quantity'] -= $RemScannedQty ;
							tableWrite ('Item', $Item, $Item['Id']) ;
							If ($Item['Quantity'] == 0) 
								tableDelete ('Item', $Item['Id']) ;
							
							$Item['PreviousContainerId']=$Item['ContainerId'] ;
							$Item['ContainerId']=$Container['Id'] ;
							$Item['ParentId']=$Item['Id'] ;
							$Item['Quantity'] = $RemScannedQty ;
							$Item['OrderLineId'] = $IOrderLineId ;

							unset ($Item['Id']) ;
							$_itemId = tableWrite ('Item', $Item) ;

							$transaction_item = array (
								'TransactionId'				=> $_transactionid,
								'ObjectLineId'				=> $IOrderLineId ,
								'ObjectQuantityId'			=> 0 ,
								'ArticleSizeId'				=> $Item['ArticleSizeId'] ,
								'Quantity'					=> $RemScannedQty ,
								'Value'						=> $Item['Price'], 
								'ActualPrice'				=> 0, 
								'LogisticCost'				=> 0, 
								'FromStockId'				=> 14321,
								'ToStockId'					=> (int)$Stock['Id']
							) ;
							$_transactionitemid = tableWrite('transactionitem', $transaction_item) ;
							$transaction['Value'] 	 += $RemScannedQty * $Item['Price'] ;				
							$transaction['Quantity'] += $RemScannedQty ;				
							tableWrite('transaction', $transaction, $_transactionid) ;

							break ;
						} else {
							$RemScannedQty -= $Item['Quantity'] ;
							$PickedQuantity += $Item['Quantity'] ;

							// delete Item on stock and create new item with item qty.
							$ItemQty = $Item['Quantity'];
							$Item['Quantity'] = 0 ;
							tableWrite ('Item', $Item, $Item['Id']) ;
							tableDelete ('Item', $Item['Id']) ;
							
							$Item['PreviousContainerId']=$Item['ContainerId'] ;
							$Item['ContainerId']=$Container['Id'] ;
							$Item['ParentId']=$Item['Id'] ;
							$Item['Quantity'] = $ItemQty ;
							$Item['OrderLineId'] = $IOrderLineId ;

							unset ($Item['Id']) ;
							tableWrite ('Item', $Item) ;

							$transaction_item = array (
								'TransactionId'				=> $_transactionid,
								'ObjectLineId'				=> $IOrderLineId ,
								'ObjectQuantityId'			=> 0 ,
								'ArticleSizeId'				=> $Item['ArticleSizeId'] ,
								'Quantity'					=> $ItemQty ,
								'Value'						=> $Item['Price'], 
								'ActualPrice'				=> 0, 
								'LogisticCost'				=> 0, 
								'FromStockId'				=> 14321,
								'ToStockId'					=> (int)$Stock['Id']
							) ;
							$_transactionitemid = tableWrite('transactionitem', $transaction_item) ;
							$transaction['Value'] 	 += $ItemQty * $Item['Price'] ;				
							$transaction['Quantity'] += $ItemQty ;				
							tableWrite('transaction', $transaction, $_transactionid) ;
						}
					} else {
						return sprintf ('Invalid Item fetch') ;
					}
					$Clone_Item = $Item ;
				}		
				// Check if enough items found on stock to fulfil registration
				if ($PickedQuantity < $ReqQty) {
					// Nothing found at all: Make item from scratch.
					if ($PickedQuantity == 0) {
						$Item = array (
							'ArticleId' 	=> (int)$VariantCodeInfo['ArticleId'],
							'ArticleColorId'=> (int)$VariantCodeInfo['ArticleColorId'],
							'ArticleSizeId' => (int)$VariantCodeInfo['ArticleSizeId'],
							'ContainerId' 	=> (int)$Container['Id'],
							'OrderLineId'  	=> (int)$IOrderLineId ,
							'BatchNumber' 	=> 'PICK' ,
							'Sortation' 	=> 1,
							'OwnerId' 	=> 787,
							'Quantity' 		=> ($ReqQty - $PickedQuantity),
							'Price'			=> 0,
						) ;
						unset ($Item['Id']) ;
						tableWrite ('Item', $Item) ;

						$transaction_item = array (
							'TransactionId'				=> $_transactionid,
							'ObjectLineId'				=> $IOrderLineId ,
							'ObjectQuantityId'			=> 0 ,
							'ArticleSizeId'				=> $Item['ArticleSizeId'] ,
							'Quantity'					=> ($ReqQty - $PickedQuantity) ,
							'Value'						=> $Item['Price'], 
							'ActualPrice'				=> 0, 
							'LogisticCost'				=> 0, 
							'FromStockId'				=> 14321,
							'ToStockId'					=> (int)$Stock['Id']
						) ;
						$_transactionitemid = tableWrite('transactionitem', $transaction_item) ;
						$transaction['Quantity'] += ($ReqQty - $PickedQuantity) ;				
						tableWrite('transaction', $transaction, $_transactionid) ;


					} else {
						// something found but not enough: Clone last item.
						$Clone_Item['Quantity'] 	= $ReqQty - $PickedQuantity ;
						$Clone_Item['BatchNumber'] 	= 'PICK' ;
						unset ($Clone_Item['Id']) ;
						tableWrite ('Item', $Clone_Item) ;

						$transaction_item = array (
							'TransactionId'				=> $_transactionid,
							'ObjectLineId'				=> $Clone_Item['OrderLineId'] ,
							'ObjectQuantityId'			=> 0 ,
							'ArticleSizeId'				=> $Clone_Item['ArticleSizeId'] ,
							'Quantity'					=> ($ReqQty - $PickedQuantity) ,
							'Value'						=> $Clone_Item['Price'], 
							'ActualPrice'				=> 0, 
							'LogisticCost'				=> 0, 
							'FromStockId'				=> 14321,
							'ToStockId'					=> (int)$Stock['Id']
						) ;
						$_transactionitemid = tableWrite('transactionitem', $transaction_item) ;
						$transaction['Value'] 	 += ($ReqQty - $PickedQuantity) * $Clone_Item['Price'] ;		
						$transaction['Quantity'] += ($ReqQty - $PickedQuantity) ;				
						tableWrite('transaction', $transaction, $_transactionid) ;
					}
					if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
						$qty=(int)($ReqQty / $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']]) ;
					else
						$qty=$ReqQty;
				}
				dbQueryFree ($res) ;
			
				// Update qty in PickOrderLine				
				$PickOrderLine['PackedQuantity'] = $_POST['PackQty'][$VariantCode] + $qty  ;
				$PickOrderLine['PickedQuantity'] = 0  ;
				tableWrite ('PickOrderLine', $PickOrderLine, $_pickorderlineid) ;
			} // END consolidated variants.
		} // END of each pickline.
	} // END TotalQuantity >0
	
	switch ($Navigation['Parameters']) {
		case 'ship' :
		case 'partship' :
			// Update PickOrder.
			if ($TotalQuantity==0) {
				if ($Record['LastContainerId']==0) {
					return 'Nothing to ship' ;
				} else { 
					$Container['Id']=(int)$Record['LastContainerId'];
					$PickOrder = array (
						'Packed'			=> 1,
						'PackedUserId'	=> $User['Id'],
						'PackedDate'		=> dbDateEncode(time()),
					) ;
				}
			} else {
				if ($Record['LastContainerId']==0) {
					$Record['ToId']				= (int)$fields['StockId']['value'] ;
				}
				$PickOrder = array (
					'Packed'			=> 1,
					'PackedUserId'		=> $User['Id'],
					'PackedDate'		=> dbDateEncode(time()),
					'LastContainerId' 	=> (int)$Container['Id'],
					'ToId'				=> (int)$fields['StockId']['value']
				) ;
			}
			if ($Record['singleGTINBatch']==1) {
				$PickOrder['LastContainerId'] = (int)$Container['Id'] ;
				$PickOrder['ToId'] 			= (int)$fields['StockId']['value'] ;
				$Record['ToId']				= (int)$fields['StockId']['value'] ;
			}
			if ($Navigation['Parameters'] == 'partship') {
				$PickOrder['Packed']=0 ;
				$PickOrder['Printed']=0 ;
			}
			tableWrite ('PickOrder', $PickOrder, $Id) ;
			
			if ($Record['Type']=='SalesOrder') {
				$shipment = array (
					'Ready'			=> 1,
					'ReadyUserId'	=> $User['Id'],
					'ReadyDate'		=> dbDateEncode(time()),
					'Departed'			=> 1,
					'DepartedUserId'	=> $User['Id'],
					'DepartedDate'		=> dbDateEncode(time()),
					'PartShip'			=> 0
				) ;
				if ($Navigation['Parameters'] == 'partship') $shipment['PartShip'] = 1 ; 
			} else {
				$shipment = array (
					'Ready'			=> 1,
					'ReadyUserId'	=> $User['Id'],
					'ReadyDate'		=> dbDateEncode(time()),
					'Departed'			=> 0,
					'DepartedUserId'	=> $User['Id'],
					'DepartedDate'		=> dbDateEncode(time()),
					'Invoiced'			=> 1,
					'InvoicedUserId'	=> $User['Id'],
					'InvoicedDate'		=> dbDateEncode(time()),
					'Done'			=> 0,
					'DoneUserId'	=> $User['Id'],
					'DoneDate'		=> dbDateEncode(time())
				) ;
			}
//			$_shipmentid = tableWrite ('Stock', $shipment, (int)$fields['StockId']['value']) ;
			$_shipmentid = tableWrite ('Stock', $shipment, (int)(int)$Record['ToId']) ;
			
			/*
			// Genrate invoice
			$_ordertypeid = $Record['OrderTypeId'] ;
			$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $_shipmentid) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			if ($Record['OrderTypeId']==10) {
				$Record['SetReady'] = 0 ;
			} else {
				$Record['SetReady'] = 0 ;
			}
			$Record['SetReady'] = 1 ;
			$Record['GoToShipment'] = $_shipmentid ;
			$Record['PreInvoiceId'] = $_preinvoiceid ;
			$Record['PreInvoiceOrderId'] = $_reforderid ;
			$Record['OrderTypeId']=$_ordertypeid ;
			require_once 'module/invoice/generate-cmd.inc' ;*/

			$orderdata = array(
				'Done'			=> 1,
				'DoneUserId'	=> $User['Id'],
				'DoneDate'		=> dbDateEncode(time())
			);
			tableWrite('order', $orderdata, (int)$_salesOrderId);

			if ($Config['Instance'] == 'Production') {
				$webship = shipItemsInWebshipper($_shipmentid, true);
				if($webship !== true){
					return $webship;
				}
			}

			if ($_singleGTINBatch==1) {
	    $query = 
	     sprintf ("SELECT if((Container.`Position` is null) or (Container.`Position`=''), 
	                               if(VariantCode.PickContainerId>0,'X-NoPos+C','X-NoPos'), 
	                               Container.`Position`) AS `Position`,
				(select sum(Item.Quantity) from Item where Item.containerid=Container.Id and Item.articleId=VariantCode.ArticleId and Item.articleColorId=VariantCode.ArticleColorId and Item.articleSizeId=VariantCode.ArticleSizeId and Item.active=1) as ItemQty,
				group_concat(PickOrderLine.Id) AS PickOrderLineId, PickOrderLine.VariantCode AS VariantCode, group_concat(PickOrderLine.ImportError) AS ImportError, 
				Article.Number as ArticleNumber, VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, VariantCode.ArticleSizeId as ArticleSizeId, 
				PickOrderLine.VariantCodeId AS VariantCodeId,
				PickOrderLine.VariantDescription as VariantDescription, PickOrderLine.VariantColor as VariantColor, 
				PickOrderLine.VariantSize as VariantSize, VariantCode.VariantModelRef as VariantModelRef,
				sum(PickOrderLine.OrderedQuantity) as OrderedQuantity, 
				sum(PickOrderLine.PackedQuantity) as PackedQuantity, 
				PickOrderLine.BoxNumber as BoxNumber, 
				sum(PickOrderLine.PrePackedQuantity) as PrePackedQuantity, 
				sum(PickOrderLine.PickedQuantity) as PickedQuantity,
				VariantCode.SKU,
				VariantCode.VariantCode
	     FROM PickOrderLine 
	     LEFT JOIN VariantCode ON VariantCode.Id=PickOrderLine.VariantCodeId  and variantcode.active=1
	     LEFT JOIN Article ON VariantCode.ArticleId=Article.Id
	     LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId  and Container.Active=1
	     WHERE PickOrderLine.PickOrderId=%d and PickOrderLine.Done=0 and PickOrderLine.Active=1 
		GROUP BY VariantCode.id ORDER BY `Position`, article.number, variantcode.articlecolorid, variantcode.articlesizeid", $_pickOrderId) ; //die($query) ;
//		GROUP BY VariantCode.id ORDER BY `Position`, variantcode.articleid, variantcode.articlecolorid, variantcode.articlesizeid", $Id) ;
	    $Result = dbQuery ($query) ;
    
	    $query = sprintf ("SELECT PickOrder.*, o.reference as OrderReference, o.WebshipperId, o.Id as OrderId, ss.Name as ShopifyName FROM PickOrder LEFT JOIN `Order` o ON o.id=PickOrder.ReferenceId LEFT JOIN shopifyshops ss ON ss.Id=o.WebShopNo WHERE PickOrder.Active=1 AND PickOrder.Id=%d", $_pickOrderId) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
				require_once "module\pick\printorder.inc" ;

				$Params = sprintf('&BatchId=%d&shipmentid=%d',$_BatchId, $_shipmentid) ;
				return navigationCommandMark ('packbatchview',$_BatchId, $Params) ;
			} else {
				return navigationCommandMark ('packlist', $_pickOrderId) ;
			}
			
		    // return navigationCommandMark ('shipmentview', $_shipmentid) ;
			break;
		default :
			if ($TotalQuantity==0) return 'No items to be packed' ;

			// Update PickOrder.
			$PickOrder = array (
				'LastContainerId' => (int)$Container['Id'],
				'ToId' => (int)$fields['StockId']['value']
			) ;
			tableWrite ('PickOrder', $PickOrder, $Id) ;
			break ;
    }

	return $Error_txt;	
?>
	
