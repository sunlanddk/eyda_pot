<?php

	$_query = 'select max(batchid) as maxBatchId from pickorder' ;
	$_result = dbQuery($_query) ;
	$_batch = dbFetch($_result) ;
	dbQueryFree($_result) ;

	foreach ($_POST['PrintFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		
//		$_pickOrder['Packed'] = 1;
		$_pickOrder = array (
			'Printed' => 1,
			'Updated' => 0,
			'BatchId' => $_batch['maxBatchId'] + 1
		) ;
		tableWrite ('pickorder', $_pickOrder, (int)$id) ;
	}
	
	return navigationCommandMark ('packbatchview', $_batch['maxBatchId']+1) ;

?>
