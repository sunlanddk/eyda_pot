<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
 
	if (!isset($_POST['Code']) or $_POST['Code']=='') {
			if (dbNumRows($VariantCodeResult)==1) {
				$_row = dbFetch($VariantCodeResult) ;
				$_POST['Code'] = $_row["VariantCode"] ;
			} else {
				return 'Batch has different variants. Please enter variantcode ' ;
			}
	}
	dbQueryFree($VariantCodeResult) ;

	$BatchId = $Id ;
	$query = sprintf ("SELECT PickOrder.Id as PickOrderId, pickorder.BatchId, PickOrderLine.Id as PickOrderLineId, VariantCode.Id as VariantCodeId, VariantCode.VariantCode as VariantCode
						FROM (pickorder, pickorderline, variantcode)
						WHERE pickorder.BatchId=%d 
						 and PickOrder.active=1 and PickOrder.Type='SalesOrder' and PickOrder.Packed=0
						 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrderLine.Active=1
						 and variantcode.id=pickorderline.variantcodeid and variantcode.variantcode='%s'", $Id,  $_POST['Code']) ; //die($query) ;
	$res = dbQuery($query) ;
	if (dbNumRows($res)==0)
		return 'VariantCode ' .  $_POST['Code'] . ' not found for a not packed PickOrder. <br><br>All PickOrders already packed for this variant or variant not in batch' ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	
//	return 'PickOrderId ' . $row['PickOrderId'] . ' variantcode' . $row['VariantCode'];
	$_POST['PickQty'][$_POST['Code']] = 1 ;
	$_POST['PickOrderLineId'][$_POST['Code']] = $row['PickOrderLineId'];
	$Id = $row['PickOrderId'] ;
	// PickOrder
	$query = sprintf ("SELECT PickOrder.*, o.reference as OrderReference FROM PickOrder LEFT JOIN `Order` o ON o.id=PickOrder.ReferenceId WHERE PickOrder.Active=1 AND PickOrder.Id=%d", $Id) ;
	$res = dbQuery ($query) ;
	$Record = dbFetch ($res) ;
	dbQueryFree ($res) ;
	$Navigation['Parameters'] = 'ship' ;
	
	$Record['singleGTINBatch'] = 1 ;
	$Record['BatchId'] = $BatchId ;
    require_once 'module/pick/packlist-cmd.inc' ;

    return 0 ; 

?>
