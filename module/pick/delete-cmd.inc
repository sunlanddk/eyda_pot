<?php

    require_once 'lib/table.inc' ;

    // delete pick lines
    $query = sprintf ("UPDATE PickOrderLine SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE PickOrderId=%d", dbDateEncode(time()), $User["Id"], $Id) ;
    dbQuery ($query) ;

    // Do delete pickorder
    tableDelete ('PickOrder', $Id) ;

    $query = sprintf("SELECT sum(PackedQuantity+PickedQuantity) as Qty FROM PickOrderLine WHERE PickOrderId=%d AND Active=1", $Id) ;
    $result=dbQuery($query) ;
    $Qty=dbFetch($result) ;
    if ($Qty['Qty']>0) return 'Packing was already started - Remember to move items back from shipment(s)' ;

    return 0
?>
