<script type='text/javascript'>
function Update (nav, id) {
	var key = event.keyCode || event.charCode;
	if (key != 13)  return ; // CR is 13, tab is 9
	appFocus ('Code') ;
	appLoaded () ;

//	appLoad (nav,id,''); // reload
	appSubmit(2937,id,null); // execute pack command.
}
</script>  
<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

//	echo '<table><tr><td>' ;
	formStart() ;
	itemStart() ;
	itemField('Batch', $Id) ;
	itemSpace() ;
	if ($DisableScan) {
		itemField('No Scan', 'Print shipping documents before proceeding to next order/scanning') ;
		itemSpace() ;
	} else {
		$NoVariants = dbNumRows($VariantCodeResult) ;
		if ($NoVariants > 1) {
			$singleVariantCodeTxt = '';
		} else {
			$singleVariantCodeTxt = 'Only one variantcode in batch: Enter barcode or press Pack next without barcode';
		}
		itemFieldRaw ('Variant Code', formText ('Code', '', 14, '', sprintf('onkeypress="Update(%d,%d);"',(int)$Navigation['Id'], $Id)) . $singleVariantCodeTxt) ;
		itemSpace () ;
		itemFieldRaw ('Gross Weight', formText ('GrossWeight', '', 14, 'text-align:right;') . ' kg') ;
		itemFieldRaw ('Type', formDBSelect ('ContainerTypeId', 22, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:120px;')) ;
		itemSpace () ;
	}
	$_shipmentid=0 ;
	if (isset($_GET['shipmentid'])) {
		$_shipmentid=(int)$_GET['shipmentid'] ;
	}
	if (isset($_GET['printdocuments'])) {
		$_shipmentid=(int)$_GET['printdocuments'] ;
	}
	if ($_shipmentid>0) {
		itemSpace () ;
	    itemFieldIcon ('Last shipment', TableGetField('Stock','Name',$_shipmentid), 'shipment.gif', 'shipmentview', $_shipmentid) ;
	}
	itemEnd() ;
	formEnd() ;
	echo '<br><br>' ;

//	echo '</td><td>' ;
//	echo '<iframe src="C:\inetpub\wwwroot\logistikcentralen\develstore\packinglist\00\04\02\81" height="200" width="400"></iframe>' ;
//	echo '</td></tr></table>' ;
	
    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Variant', 150)  ;
    listHead ('VariantDescription', 300)  ;
    listHead ('Remaining Quantity', 150) ;
    listHead ('Ordered Quantity', 150) ;
    listHead ('PickPosition', 150) ;
	listHead ('') ;

    $n = 0 ;
    while ($row = dbFetch($VariantCodeResult)) {
		listRow () ;
    	listField ($row["VariantCode"]) ;
    	listField ($row["Description"]) ;
    	listField ($row["NoRemaining"]) ;
    	listField ($row["NoPickOrders"]) ;
    	listField ($row["Position"]) ;
		if ($NoVariants == 1) {
			listFieldRaw(formHidden('SingleCode', $row["VariantCode"], 8,'','')) ;
		} else
			listField ('') ;
    }
    if (dbNumRows($VariantCodeResult) == 0) {
		listRow () ;
		listField ('No Variants', 'colspan=2') ;
    }
    listEnd () ;
    dbQueryFree ($VariantCodeResult) ;
	echo '<br><br>' ;
	
    // PickOrder list
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('PickOrder Reference', 150)  ;
    listHead ('PickOrder Id', 150)  ;
    listHead ('Customer', 300) ;
    listHead ('Variant', 150)  ;
    listHead ('State', 150) ;
    listHeadIcon ('Shipment', 150) ;
	listHead ('') ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
		listRow () ;
    	listField ($row["OrderReference"]) ;
    	listField ($row["Id"]) ;
    	listField ($row["CompanyName"]) ;
    	listField ($row["VariantCode"]) ;
    	listField ($row["Packed"]==1?'Packed':'') ;
		if ($row["Packed"])
			listFieldIcon ('shipment.gif', 'batchshipment', (int)$row['ToId']) ;
		else 
			listField('') ;
		listField ('') ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField ('No PickOrders', 'colspan=2') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

	if (isset($_GET['printdocuments'])) {
		//echo sprintf("<script type='text/javascript'>window.open('index.php?nid=2940&id=%s')</script>", $_GET['printdocuments']) ;
	}

    return 0 ;
?>
