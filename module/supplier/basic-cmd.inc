<?php

    require 'lib/save.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new':
	    unset ($Record) ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id ;
	switch ($fieldname) {
	    case 'Number':
		if (!$changed) return false ;
		
		// Check that number does not allready exist
		$query = sprintf ('SELECT Id FROM Company WHERE Active=1 AND Number="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Company allready existing' ;
		return true ;
	}
	return false ;
    }
     
    $fields = array (
		'Number'	=> array ('mandatory' => true,	'check' => true),
		'Name'		=> array ('mandatory' => true),
	'Address1'	=> array (),
	'Address2'	=> array (),
	'ZIP'		=> array (),
	'City'		=> array (),
	'CountryId'	=> array ('type' => 'integer'),
		'PhoneMain'	=> array (),
		'PhoneFax'	=> array (),
		'Mail'		=> array (),
		'RegNumber'	=> array (),
		'TypeSupplier'	=> array ('type' => 'set'),
		'ListHidden'	=> array ('type' => 'checkbox'),
		'Comment'		=> array (),
		'CompanyFooter'		=> array (),
		'PurchaseRefId'		=> array ('type' => 'integer'),
		'PurchaseDeliveryTermId'	=> array ('type' => 'integer'),
		'PurchaseCarrierId'		=> array ('type' => 'integer'),
		'PurchaseCurrencyId'		=> array ('type' => 'integer'),
		'PurchasePaymentTermId'		=> array ('type' => 'integer'),
		'PurchaseAgent'		=> array ()
    ) ;
  
	
    switch ($Navigation['Parameters']) {
		case 'new' :
			if ($_POST['Number'] == '') {
				$query = sprintf ('select min(l.number + 1) as start
									from company as l
									left outer join company as r on r.tocompanyid=%d and r.number>500000 and r.number<510000 and l.number + 1 = r.number
									where r.number is null and l.tocompanyid=%d and l.number>500000 and l.number<510000', $User['CompanyId'], $User['CompanyId']) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
				$_POST['Number'] = $row['start'] ;
				dbQueryFree ($result) ;

			}
			$fields['tocompanyid']['value'] = $User['CompanyId'] ;
			$fields['TypeSupplier']['value'] = 1 ;
			break;
		default:
			break ;
	}  
  
    // Save record
    $res = saveFields ('Company', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new article
	    require_once 'lib/navigation.inc' ;
	    return navigationCommandMark ('supplierview', $Record['Id']) ;
    }
    
    return 0 ;    

    
?>
