<?php

    require_once 'lib/list.inc' ;

    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 80) ;
    listHead ('Name') ;
    listHead ('City') ;
    listHead ('Country') ;
//    listHead ('Customer', 80, 'align=right') ;
//    listHead ('Supplier', 70, 'align=right') ;
//    listHead ('Carrier', 70, 'align=right') ;
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
		if ($row['ListHidden'] == 0) {
			listRow () ;
			listFieldIcon ($Navigation['Icon'], 'supplierview', $row['Id']) ;
			listField ($row['Number']) ;
			listField ($row['Name']) ;
			listField ($row['City']) ;
			listField ($row['CountryName']) ;
	//		listField ($row['TypeCustomer'] ? 'yes' : 'no', 'align=right') ;
	//		listField ($row['TypeSupplier'] ? 'yes' : 'no', 'align=right') ;
	//		listField ($row['TypeCarrier'] ? 'yes' : 'no', 'align=right') ;
		}
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Companies', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
