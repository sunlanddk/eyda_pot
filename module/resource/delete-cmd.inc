<?php

    require_once 'lib/table.inc' ;

    // Validate Id
    if ($Id != utsDateOnly($Id)) return sprintf ("%s(%d) invalid referance, id %s", __FILE__, __LINE__, $Id) ;
 
    // Delete capacitites
    tableDeleteWhere ('Capacity', sprintf ("ActualDate='%s'", dbDateOnlyEncode($Id))) ;

    return 0 ;
?>
