<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;

    if ($Navigation['Parameters'] == 'new') {
	// Get CapacityDate
	$Id = dbDateDecode ($_POST['CapacityDate']) ;

	// Verify that the capacity day not allready has been created
	$query = sprintf ("SELECT COUNT(Capacity.Id) AS CapacityCount FROM Capacity WHERE Capacity.Active=1 AND Capacity.ActualDate='%s'", dbDateOnlyEncode($Id)) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ($row['CapacityCount'] > 0) return ('capacities for this day allready exists') ;
	
    } else {
	// Validate Id
	if ($Id != utsDateOnly($Id)) return sprintf ("%s(%d) invalid referance, id %s", __FILE__, __LINE__, $Id) ;
    }

    // Get workgroups and current capacitites for the day selected
    $Capacity = array() ;
    $query = sprintf ("SELECT WorkGroup.Id AS WorkGroupId, WorkGroup.Number, Capacity.Id AS CapacityId, Capacity.Operators, Capacity.ProductionMinutes FROM WorkGroup LEFT JOIN Capacity ON Capacity.WorkgroupId=WorkGroup.Id AND Capacity.Active=1 AND Capacity.ActualDate='%s' WHERE WorkGroup.Active=1 ORDER BY WorkGroup.Number", dbDateOnlyEncode($Id)) ;
    $result = dbQuery ($query) ;    
    while ($row = dbFetch($result)) $Capacity[(int)$row['WorkGroupId']] = $row ;
    dbQueryFree ($result) ;

    // Walk through label lines to get information
    foreach ($Capacity as $n => $line) {
	// Get and validate Operators
	$s = $_POST['Operators'][$n] ;
	if (is_null($s) or $s == '') {
	    if ($line['Operators'] > 0) $Capacity[$n]['Operators'] = 0 ;
	} else {
	    $v = (int)$s ;
	    $s = ltrim(trim($s), '0') ;
	    if ($s == '') $s = '0' ;
	    if ((string)$v !== $s) return sprintf ("invalid number of Operators, WorkGroup %s", $line['Number']) ;
	    if ($line['Operators'] == $v) {
		unset ($Capacity[$n]['Operators']) ;	
	    } else {   		
		$Capacity[$n]['Operators'] = $v ;
	    }
	}
	
	// Get and validate ProductionMinutes
	$s = $_POST['ProductionMinutes'][$n] ;
	if (is_null($s) or $s == '') {
	    if ($line['ProductionMinutes'] > 0) $Capacity[$n]['ProductionMinutes'] = 0 ;
	} else {
	    // Generat and verify format
	    $format = "(^[0-9]{1,4}$)|(^[0-9]{0,4}[\\,\\.][0-9]{0,2}$)" ;
	    if (!ereg($format,$s)) return sprintf ("invalid number of Minutes, WorkGroup %s", $line['Number']) ;

	    // Substitute , -> .
	    $s = str_replace (',', '.', $s) ;

	    if ($line['ProductionMinutes'] == $s) {
		unset ($Capacity[$n]['ProductionMinutes']) ;	
	    } else {   		
		$Capacity[$n]['ProductionMinutes'] = $s ;
	    }
 	}
    }

//  Debug
//    foreach ($Capacity as $n => $line)  {
//	printf ("workgroupid %d, number %s, id %d, operators %s, minutes %s<br>\n", $n, $line['Number'], $line['CapacityId'], $line['Operators'], $line['ProductionMinutes']) ;
//    }
  
    // Opdate capacities
    foreach ($Capacity as $n => $line)  {
	$fields = array() ;
	if (isset($line['Operators'])) $fields['Operators'] = array ('type' => 'set', 'value' => $line['Operators']) ;
	if (isset($line['ProductionMinutes'])) $fields['ProductionMinutes'] = array ('type' => 'set', 'value' => $line['ProductionMinutes']) ;
	if (count($fields) == 0) continue ;
	
	if ($line['CapacityId'] > 0) {
	    $id = $line['CapacityId'] ;
	} else {
	    $id = -1 ;
	    $fields['ActualDate'] = array ('type' => 'set', 'value' => dbDateOnlyEncode($Id)) ;
	    $fields['WorkGroupId'] = array ('type' => 'set', 'value' => $n) ;
	}
// printf ("<br>id %d<br>", $id) ;
// foreach ($fields as $name => $a) printf ("field %s, type %s, value %s<br>\n", $name, $a['type'], $a['value']) ;
	$res = saveFields ('Capacity', $id, $fields) ;
	if ($res) return $res ;
    }

    return 0 ;
?>
