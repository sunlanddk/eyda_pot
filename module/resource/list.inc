<?php

    require_once 'lib/navigation.inc' ;

    printf ("<table class=list>") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>Date</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>WorkGroups</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Operators</p></td>") ;
    printf ("<td class=listhead width=100 align=right><p class=listhead>Minutes</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;
    $n = 0 ;
    while ($n++ < LINES and ($row = dbFetch($Result))) {
	$t = dbDateDecode($row["ActualDate"]) ;
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", 'image/toolbar/resources.gif', navigationOnClickLink ('edit', $t)) ;
	printf ("<td><p class=list>%s</p></td>", date("Y-m-d", $t)) ;
	printf ("<td align=right><p class=list>%d</p></td>", $row['WorkGroupCount']) ;
	printf ("<td align=right><p class=list>%d</p></td>", $row['OperatorCount']) ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $row['ProductionMinutes']) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
	printf ("<tr><td></td><td colspan=3><p class=list>No capacities</p></td></tr>\n") ;
    }
    printf ("</table>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
