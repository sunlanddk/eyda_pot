<?php

    require_once "lib/list.inc" ;
    require_once "lib/navigation.inc" ;

    // Article header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Name', 100) ;
    listHead ('CustomsPos', 100) ;
    listHead ('DisplayOrder', 100, 'align=right') ;
    listHead ('') ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
		listRow () ;
		listFieldIcon ($Navigation['Icon'], 'articlesizeedit', $row['Id']) ;
		listField ($row['Name']) ;
		listField ($row['CustomsPositionName']) ;
		listField ($row['DisplayOrder'], 'align=right') ;
		listField ('') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Sizes', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
