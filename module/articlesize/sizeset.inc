<?php

    require_once 'lib/html.inc' ;

    // Article header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Size Range</td><td>%s</td></tr>\n", htmlDBSelect ("SizeSetId style='width:280px'", 0, 'SELECT Id, CONCAT(Name, ", ", Description) AS Value FROM SizeSet WHERE Active=1 ORDER BY Name')) ;  
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
