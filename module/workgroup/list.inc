<?php

    require_once "lib/navigation.inc" ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>Number</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Efficiency(%%)</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p>Sewing</p></td>") ;
    printf ("<td class=listhead width=8></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/'.$Navigation['Icon'], navigationOnClickLink ("edit", $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Number'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("<td align=right><p class=list>%d</p></td>", $row['Efficiency']) ;
	printf ("<td align=right><p class=list>%s</p></td>", ($row['Sewing']) ? 'yes' : 'no') ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=3><p class=list>No WorkGroups</p></td></tr>\n") ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
