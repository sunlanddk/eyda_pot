<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    unset ($Record) ;

	    // Default values
	    $Record['Efficiency'] = 50 ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Number</td><td><input type=text name=Number maxlength=10 size=10' value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Efficiency</td><td><input type=text name=Efficiency maxlength=3 style='width:30px' value=\"%s\">%%</td></tr>\n", htmlentities($Record['Efficiency'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Sewing</td><td><input type=checkbox name=Sewing%s>", ($Record['Sewing'])?' checked':'') ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
