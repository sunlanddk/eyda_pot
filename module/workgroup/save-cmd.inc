<?php

    require 'lib/save.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }
     
    $fields = array (
	'Number'		=> array ('mandatory' => true),
	'Description'		=> array (),
	'Sewing'		=> array ('type' => 'checkbox'),
	'Efficiency'		=> array ('type' => 'integer', 'mandatory' => true, 'check' => true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Efficiency':
		if (!$changed) return true ;
		if ($value < 0 or $value > 100) return "The Efficiency persentage must be in the interval from 0 to 100" ;
		break ;
	}
    }		

    return saveFields ('WorkGroup', $Id, $fields) ;
?>
