<?php

    require_once "lib/table.inc" ;
    require_once 'lib/perm.inc' ;

    switch ($Navigation['Function']) {
	case 'list':
	    switch ($Navigation["Parameters"]) {
		case "global" :
		    // Global role
		    unset ($Record) ;
		    $Id = 0 ;
		    break ;
	
		case 'project':
		    // Id: Project.Id
		    // Roles for specific project
		    $query = sprintf ("SELECT * FROM Project WHERE Active=1 AND Id=%d", $Id) ;
		    $result = dbQuery ($query) ;
		    $Record = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    break ;
		    
		case 'selected':
		    // Roles for selected project
		    $Id = $Project["Id"] ;
		    $Record = $Project ;
		    break ;
		    
		case "all" :
		    // Roles for all projects
		    if (!permAdminSystem()) return "You do not have sufficient permission" ;
		    unset ($Record) ;
		    $Id = -1 ;
		    break ;
	    }

	    // Get projects to list
	    $query = "SELECT Role.Id AS Id, Role.Name AS Name, Role.Description AS Description" ;
	    if ($Id == -1) $query .= ", Project.Name AS ProjectName" ;
	    $query .= " FROM Role" ;
	    if ($Id == -1) $query .= " LEFT JOIN Project ON Role.ProjectId=Project.Id" ;
	    $query .= " WHERE Role.Active=1" ;
	    if ($Id >= 0) $query .= sprintf(" AND Role.ProjectId=%d", $Id) ;
	    $query .= permClauseOwner ('Role') ;
	    $query .= " ORDER BY" ;
	    if ($Id == -1) $query .= " Project.Name," ;
	    $query .= " Role.Name" ;    
	    $Result = dbQuery ($query) ;
	    return 0 ;
    }

    switch ($Navigation["Parameters"]) {
	case "global" :
    	case "globalnew" :
	    // Global role
	    $Id = 0 ;
	    return 0 ;
	
	case 'project':
	case 'projectnew':
	    // Id: Project.Id
	    // Roles for specific project
	    $query = sprintf ("SELECT * FROM Project WHERE Active=1 AND Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    return 0 ;
	    
	case 'selected':
	case 'selectednew':
	    // Roles for selected project
	    $Id = $Project["Id"] ;
	    $Record = $Project ;
	    return 0 ;

	case "all" :
	    // Roles for all projects
	    if (!permAdminSystem()) return "You do not have sufficient permission" ;
	    $Id = -1 ;
	    return 0 ;
    }

    // Get current entry
    if ($Id > 0) {
	$query = sprintf ("SELECT * FROM Role WHERE Active=1 AND Id=%d", $Id) ;
	$result = dbQuery ($query) ;
	$Record = dbFetch ($result) ;
	dbQueryFree ($result) ;
    }

    return 0 ;
?>
