<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
        
    // $Id is ProjectId, 0 => global roles, -1 => all roles
    
    // When specific project selected -> show project information
    if ($Record['Id'] > 0) {
	printf ("<table class=item>\n") ;
	print htmlItemSpace () ;
	printf ("<tr><td class=itemlabel>Project</td><td class=itemfield>%s</td></tr>\n", $Record['Name']) ;
	printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", $Record['Description']) ;
	printf ("</table>\n") ;  

    	printf ("<H3>Roles</H3>\n") ;
    }
       
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    if ($Id == -1) printf ("<td class=listhead width=90><p class=listhead>Project</p></td>") ;
    printf ("<td class=listhead width=90><p class=listhead>Role</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($Result)) {
	printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", 'image/toolbar/role.gif', navigationOnClickLink ('edit', $row["Id"])) ;
	if ($Id == -1) printf ("<td><p class=list>%s</p></td>", htmlentities($row["ProjectName"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Name"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Description"])) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
	printf ("<tr><td></td><td colspan=2><p class=list>No roles</p></td></tr>") ;
    }
    printf ("</table>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
