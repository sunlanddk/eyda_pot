<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
		$s = ($Record[$field]) ? sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) : 'No' ;
		itemField ($field, $s) ;
    }

    itemStart () ;
    itemHeader () ;
    itemFieldIcon ('Purchase Order', (int)$Record['OrderId'], 'requisition.png', 'purchaseview', (int)$Record['RequisitionId']) ;     

    itemSpace () ;
//	itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
//    itemField ('Name', $Record['CompanyName']) ;     
 
    itemSpace () ;
    itemFieldIcon ('Article', $Record['ArticleNumber'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;     
    itemField ('Description', $Record['ArticleDescription']) ;     
    
    itemSpace () ;
    itemField ('Pos', (int)$Record['No']) ;
    itemField ('Description', $Record['Description']) ;

    itemSpace () ;
//	itemField ('Lot', $Record['CollectionLotName']) ;
	itemField ('Requested',($Record['RequestedDate']>'2002-01-01') ? date('Y-m-d',dbDateDecode($Record['RequestedDate'])) : '') ;
	itemField ('Confirmed',($Record['ConfirmedDate']>'2002-01-01') ? date('Y-m-d',dbDateDecode($Record['ConfirmedDate'])) : '') ;
	itemField ('Available',($Record['AvailableDate']>'2002-01-01') ? date('Y-m-d',dbDateDecode($Record['AvailableDate'])) : '') ;
	
	
	itemSpace();
    if ($Record['VariantColor']) {
		$field = '' ;
		if ((int)$Color['Id'] > 0) {
			$field .= sprintf ('<p class=list style="display:inline;">%s (%s)</p>', $Color['ColorNumber'], $Color['ColorDescription']) ;
			if ((int)$Color['ColorGroupId'] > 0) {
			$field .= sprintf ('<div style="width:80px;height:15px;margin-left:8px;margin-top:1px;background:#%02x%02x%02x;display:inline;"></div>', $Color['ValueRed'], $Color['ValueGreen'], $Color['ValueBlue']) ;
			}
		}
		itemSpace () ;
		itemFieldRaw ('Colour', $field) ;
		}
    itemSpace () ;

    if (($Record['VariantSize'])or($Record['VariantDimension'])) {
		$field = '<table><tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : ' width=60') . '>' . htmlentities($s['Name']) . '</td>' ;
		$field .= '<td align=right width=70></td>' ;
		$field .= '</tr>' ;
		$field .= '<tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : '') . '>' . number_format ((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
		$field .= '<td align=right style="padding-left=8px">' . ' ' . htmlentities($Record['UnitName']) . ' ordered</td>' ;
		$field .= '</tr>' ;	
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : '') . '>' . number_format ((float)$s['QuantityReceived'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
		$field .= '<td align=right style="padding-left=8px">' . ' ' . htmlentities($Record['UnitName']) . ' received</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Quantity', $field) ; 
		itemSpace () ;

		itemField ('Quantity Total', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName'] . ' (' . number_format((float)$Record['QuantityReceived'], (int)$Record['UnitDecimals'], ',', '.') . ' received)') ;
    } else {
		itemField ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ;
    }

    itemSpace () ;
//    itemField ('Cost Price', number_format ((float)$Record['PriceCost'], 2, ',', '.') . ' Kr') ;
    itemField ('Purchase Price', number_format ((float)$Record['PurchasePrice'], 2, ',', '.') . ' ' . $Record['CurrencySymbol']) ;
//	itemField ('Discount', (int)$Record['Discount'] . ' %') ;
    itemField ('Actual Price', number_format ((float)$Record['ActualPrice'], 2, ',', '.') . ' ' . $Record['CurrencySymbol']) ;
    itemField ('Logistic Costs', number_format ((float)$Record['LogisticCost'], 2, ',', '.') . ' ' . $Record['CostCurrencySymbol']) ;

    itemSpace () ;
//    if (!$User['Extern'] AND !$User['SalesRef']) 
//		itemFieldText ('CustArtRef', $Record['CustArtRef']) ;
    if (!$User['Extern'] AND !$User['SalesRef']) 
		itemFieldText ('LineFooter', $Record['InvoiceFooter']) ;
    itemSpace () ;
    flag ($Record, 'Done') ; 

    if (!$User['Extern']) 
		itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
