<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }
    function flagRevert (&$Record, $field) {
		if ($Record[$field]) {
			itemFieldRaw ($field, 
						   formCheckbox ($field, 1, 'margin-left:4px;') . date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])) . ', ' . 
											   tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', 
											   $Record[$field.'UserId'])) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }
   
	$_articlecolorid = 0 ;
	if (isset($_GET['articlecolorid'])) {
		$_articlecolorid = (int)$_GET['articlecolorid'] ;
	} else {
	}
	if ($_articlecolorid>0) {
		$query = sprintf("SELECT cmps.PurchasePrice, cmps.LogisticCost FROM collectionmember cm, collectionmemberprice cmp, collectionmemberpricesize cmps 
							WHERE cm.collectionid=1 and cm.active=1 and cm.articleid=%d and cm.articlecolorid=%d
									and cmp.collectionmemberid=cm.id and cmp.currencyid=%d
									and cmps.collectionmemberpriceid=cmp.id
							", 
							$Record['ArticleId'], $_articlecolorid, $Record['CurrencyId']); //die($query) ;
		$res = dbQuery ($query) ;
	    $_defaults = dbFetch ($res) ;
		dbQueryFree ($res) ;
		$Record["PurchasePrice"] = $_defaults['PurchasePrice'] ;
		$Record["LogisticCost"] = $_defaults['LogisticCost'] ;
		$Record['ArticleColorId'] = $_articlecolorid ;
	}
    itemStart () ;
    itemSpace () ;
    itemField ('Purchase Order', (int)$Record['OrderId']) ;     
//   itemSpace () ;
//   itemField ('Customer', sprintf ('%s (%s)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;     
	if ((int)$Record['CaseId'] > 0) itemField ('Case', (int)$Record['CaseId']) ;     
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Pos', formText ('No', (int)$Record['No'], 2)) ;
    itemSpace () ;
//	itemFieldRaw ('Country', formDBSelect ('CountryId', (int)$Record[$prefix."CountryId"], $query_c, 'width:140px;', NULL, 
// sprintf('onchange="appLoad(%d,%d,\'countryid=\'+document.appform.CountryId.options[document.appform.CountryId.selectedIndex].value);"', (int)$Navigation['Id'], $Id)) . '(always select Country first)') ; 
	$_query = sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Description," (",Color.Number,")") AS Value 
											FROM (ArticleColor, Color)
											WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId 
											GROUP BY ArticleColor.Id
											ORDER BY Value', $Record['ArticleId']) ;
	itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], $_query, 'width:200px;', NULL, 
								sprintf('onchange="appLoad(%d,%d,\'articlecolorid=\'+document.appform.ArticleColorId.options[document.appform.ArticleColorId.selectedIndex].value);"', (int)$Navigation['Id'], $Id)) . '(always select Color first)') ; 
    itemSpace () ;

  
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
/*	itemFieldRaw ('Req. Lot', formDBSelect ('LotId', (int)$Record['LotId'], 
								sprintf ('SELECT CollectionLot.Id, CollectionLot.Name AS Value 
										FROM CollectionLot
										WHERE SeasonId=%d AND Active=1 
										ORDER BY CollectionLot.`Date`', $Record['SeasonId']), 'width:200px')) ; 
*/
	itemFieldRaw ('RequestedDate', formDate ('RequestedDate', $Record['RequestedDate']>'2002-01-01'? dbDateDecode($Record['RequestedDate']) :'')) ;
	itemFieldRaw ('ConfirmedDate', formDate ('ConfirmedDate', $Record['ConfirmedDate']>'2002-01-01'? dbDateDecode($Record['ConfirmedDate']) :'')) ;
//	itemFieldRaw ('ExpectedDate', formDate ('ExpectedDate', dbDateDecode($Record['ExpectedDate']))) ;
	itemFieldRaw ('AvailableDate', formDate ('AvailableDate', $Record['AvailableDate']>'2002-01-01'? dbDateDecode($Record['AvailableDate']) :'')) ;
    if ($Record['VariantColor']) {
		itemSpace () ;
/*		itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], 
									sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value 
											FROM (ArticleColor, Color)
											LEFT JOIN orderline ol ON ol.orderid=%d and ol.articlecolorid=ArticleColor.Id and ol.active=1
											WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId AND (isnull(ol.id) or ol.id=0 or ArticleColor.Id=%d)
											GROUP BY ArticleColor.Id
											ORDER BY Value', $Record['OrderId'], $Record['ArticleId'],(int)$Record['ArticleColorId'] ), 'width:200px')) ; 
*/
    }
    itemSpace () ;
    if ($Record['VariantSize']) {
		$field = '<table><tr>' ;
		foreach ($Size as $s) $field .= '<td width=60 align=right><p style="margin-right=6px;">' . htmlentities($s['Name']) . '</p></td>' ;
		$field .= '<td width=60></td>' ;
		$field .= '</tr>' ;
		$field .= '<tr>' ;
		foreach ($Size as $s) $field .= '<td>' . formText (sprintf('Quantity[%d]', (int)$s['Id']), number_format((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 6, 'width:50px;text-align:right;margin-right:0;') . '</td>' ;
		$field .= '<td style="padding-top:6px;padding-left:4px;">' . htmlentities ($Record['UnitName']) . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Quantity', $field) ; 
    } else {
		itemFieldRaw ('Quantity', formText ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 6, 'text-align:right;') . ' ' . htmlentities ($Record['UnitName'])) ;
    }
    itemSpace () ;
    itemFieldRaw ('Purchase Price', formText ('PurchasePrice', number_format((float)$Record['PurchasePrice'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
//    itemFieldRaw ('Actual Price', formText ('ActualPrice', number_format((float)$Record['PurchasePrice'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
    itemFieldRaw ('Logistic Cost', formText ('LogisticCost', number_format((float)$Record['LogisticCost'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $Record['CostCurrencySymbol']) ;
//    itemFieldRaw ('Discount', formText ('Discount', (int)$Record['Discount'], 2, 'text-align:right;') . ' %') ;
    itemSpace () ;
//    if (!$User['SalesRef']) 
//		itemFieldRaw ('Customer ref.', formtext ('CustArtRef', $Record['CustArtRef'], 12, 'text-align:right;')) ;
    if (!$User['SalesRef']) 
		itemFieldRaw ('Line Footer', formtextArea ('InvoiceFooter', $Record['InvoiceFooter'], 'width:100%;height:100px;')) ;
    if (!$User['SalesRef']) 
		flagRevert ($Record, 'Done') ; 
    itemInfo ($Record) ;
/*
	if (!$User['SalesRef']) 
		itemFieldRaw('Case- not active', formDBSelect('CaseId', (int)$Record['CaseId'], sprintf("select Id, Id AS Value from `Case` where ArticleId=%d and CompanyId=%d and Active=1",$Record['ArticleId'],$Record['CompanyId']), '', array(0 => '--none--'),  $disable));
    itemSpace () ;
*/
    if (!$User['SalesRef']) 
		itemSpace () ;
 
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
