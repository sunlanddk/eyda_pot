<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
	
	function array_2_csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {
				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
	

	// prepacked quantities
	foreach ($_POST['PackQty'] as $orderquantityid => $_packqty) {
		$OrderQuantity = array (	
			'QuantityReceived'		=>  (float)$_packqty,
			'BoxNumber'				=>  $_POST['PackBox'][$orderquantityid] 
		) ;
//		return 'test ' . $_POST['PackData[3773]["PackedQuantity"]'] . ' - ' . $_row["BoxQuantity"] . ' - ' . $orderquantityid;
		tableWrite('orderquantity',$OrderQuantity,$orderquantityid) ;
		
		if ((int)$OrderQuantity['BoxNumber']>0) {
			$_boxes[$OrderQuantity['BoxNumber']] = (int)$OrderQuantity['BoxNumber'] ;
		}
	}

	$_datetime = date('Ymd_His'); 
	$ftp_file = 'PURCHASE_' . $_datetime . '_PO' . $Id . '_Boxes.txt';
	$local_file = $Config['fileStore'] .'/export/' . $ftp_file ;
	$file = fopen($local_file,"w");
	
	foreach ($_boxes as $boxno => $_box) {
		$_line['LinjeID'] = '' ;
		$_line['ordrenummer'] = (int)$Id ;
		$_line['Varenummer'] =  1 ;
		$_line['Antal'] = 1 ;
		$_line['Box'] = $boxno ;
		$csv_data = array_2_csv($_line);
		fwrite($file,$csv_data.PHP_EOL);
	}
		
	fclose($file) ;
	return navigationCommandMark ('purchaseview', $Id) ;
	
	if ($Config['Instance'] == 'Test')  return navigationCommandMark ('purchaseview', $Id) ;

	// FTP access parameters
	$host = 'alpiftp.alpi.dk';
	$usr = 'fub';
	$pwd = 'duJS72CD';
	 
	// file to move:
	$ftp_path = '/prod/inbox/' . $ftp_file ;
	 
	// connect to FTP server (port 21)
	$conn_id = ftp_connect($host, 21) or die ("Cannot connect to host");
	 
	// send access parameters
	ftp_login($conn_id, $usr, $pwd) or die("Cannot login");
	 
	// turn on passive mode transfers (some servers need this)
	ftp_pasv ($conn_id, true);
	 
	// perform file upload
	$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_ASCII);
	 
	// check upload status:
	if (!$upload) print('Cannot upload ' . ftp_path) ;
//		print (!$upload) ? 'Cannot upload: ' . $upload : 'Upload complete';
//		print "\n";
	 
	/*
	** Chmod the file (just as example)
	*/
	 
	// If you are using PHP4 then you need to use this code:
	// (because the "ftp_chmod" command is just available in PHP5+)
	if (!function_exists('ftp_chmod')) {
	   function ftp_chmod($ftp_stream, $mode, $filename){
			return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
	   }
	}
	 
	// try to chmod the new file to 666 (writeable)
	if (ftp_chmod($conn_id, 0666, $ftp_path) !== false) {
		$_prut = 1 ;
//			print $ftp_path . " chmod successfully to 666<br>";
	} else {
		print "could not chmod $file\n";
	}
	 
	// close the FTP stream
	ftp_close($conn_id);

	
    return navigationCommandMark ('purchaseview', $Id) ;
?>
