<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;

    function flag (&$Record, $field) {
		$s = ($Record[$field]) 
			? sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), 
								 tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])
						) 
			 : 'No' ;
		if ($field == 'Done' and $Record['Done'] and (int)$Record['OrderDoneId'] > 0) {
			$s .= ', ' . htmlentities($Record['OrderDoneName']) ;
		}
		itemField ($field, $s) ;
    }

    itemStart () ;
    itemHeader () ;
    itemField ('Purchase Order', (int)$Record['Id']) ;
    itemField ('Description', $Record['Description']) ;
    if ($Record['SeasonId']>0)
	    itemFieldIcon ('Season', tableGetField('season','Name',$Record['SeasonId']), 'season.png', 'seasonview', $Record['SeasonId']) ;
    itemSpace () ;
    itemField ('State', $Record['State']) ;
    itemFieldIcon ('Owner', tableGetField ('Company', 'Company.Name', (int)$Record['FromCompanyId']), 'company.gif', 'companyview', $Record['FromCompanyId']) ;
    itemSpace () ;
    flag ($Record, 'Ready') ;
    flag ($Record, 'Done') ; 
    itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;

    printf ("<table class=item><tr><td>\n") ;

    itemStart () ;
    itemHeader ('Supplier info') ;
	$query = sprintf("SELECT * FROM Company where id=%d", (int)$Record['CompanyId']) ;
	$res = dbQuery ($query) ;
	$row = dbFetch($res) ;
	dbQueryFree ($res) ;

    itemFieldIcon ('Supplier', $row['Number'], 'company.gif', 'companyview', $Record['CompanyId']) ;
    itemField ('Name', $row['Name']) ;    
    itemSpace () ;
    itemField ('Reference', $Record['Reference']) ;
    itemSpace () ;
    itemField ('Puchase Ref', tableGetField ('User', 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', (int)$Record['PurchaseUserId'])) ;  
    itemSpace () ;
    itemEnd () ;

    printf ("</td>") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
    itemHeader ('Purchase Order information') ;
    itemField ('Currency', tableGetField ('Currency', 'CONCAT(Description," (",Name,")")', (int)$Record['CurrencyId'])) ;  
    itemSpace () ;
    itemFieldText ('CustomRate', $Record['CustomRate'] . ' %') ;
    itemSpace () ;
    itemField ('Payment Term', tableGetField ('PaymentTerm', 'CONCAT(Description," (",Name,")")', (int)$Record['PaymentId'])) ;  
    itemSpace () ;
    itemFieldText ('Header', $Record['RequisitionHeader']) ;
    itemFieldText ('Footer', $Record['RequisitionFooter']) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
    
    itemStart () ;
    itemHeader ('Delivery information') ;
    itemField ('Delivery Terms', tableGetField ('DeliveryTerm', 'CONCAT(Name," (",Description,")")', (int)$Record['DeliveryTermId'])) ;  
    itemSpace () ;
    itemField ('Carrier', tableGetField ('Carrier', 'Name', (int)$Record['CarrierId'])) ;  
    itemSpace () ;
	
	$query = sprintf("SELECT * FROM Company where id=%d", (int)$Record['DeliveryId']) ;
	$res = dbQuery ($query) ;
	$row = dbFetch($res) ;
	dbQueryFree ($res) ;
	
    itemField ('Number', $row['Number']) ;
    itemField ('Name', $row['Name']) ;
    itemField ('Street', $row['Address1']) ;
    itemField ('ZIP', $row['ZIP']) ;
    itemField ('City', $row['City']) ;
    itemField ('Country',  tableGetField ('Country', 'CONCAT(Name," - ",Description)', (int)$row['CountryId'])) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("</tr></table>\n") ;

    listStart () ;
//    listHeader ('Lines') ;
    listRow () ;
    listHeadIcon () ;
    listHeadIcon () ;
    listHead ('Pos', 30) ;
//    listHead ('Case', 50) ;
    listHead ('Article', 100) ;
    listHead ('Description',200) ;
    listHead ('Colour', 140) ;
    listHead ('Sizes', 100) ;
//    listHead ('Lot', 40) ;
    listHead ('Requested', 70) ;
    listHead ('Confirmed', 70) ;
//    listHead ('Adjusted', 70) ;
    listHead ('Available', 70) ;
    listHead ('ReceiveDone', 70) ;
    listHead ('Quantity', 70, 'align=right') ;
    listHead ('Received', 70, 'align=right') ;
    listHead ('Deviation', 70, 'align=right') ;
    listHead ('Price', 90, 'align=right') ;
 //   listHead ('Discount', 70, 'align=right') ;
    listHead ('ActualPrice', 90, 'align=right') ;
    listHead ('ReceivedTotal', 90, 'align=right') ;
    listHead ('Total', 90, 'align=right') ;
    listHead ('', 8) ;
//    listHeadIcon ('Edit', 15) ;

	$_currencysymbol = tableGetField('Currency','Symbol', $Record['CurrencyId']) ;
	$query = sprintf( 
			"SELECT rl.Id as Id, rl.No as No, rl.CaseId as CaseId, rl.Done as Done, rl.DoneDate as DoneDate, group_concat(az.Name order by az.displayorder) as Sizes,
					a.Number as ArticleNumber, a.Description as ArticleDescription, c.number as ColorNumber,c.description as ColorName, CollectionLot.Name as CollectionLotName,
					 rl.RequestedDate as RequestedDate, rl.ConfirmedDate as ConfirmedDate, rl.ExpectedDate as DelayedDate, rl.AvailableDate as AvailableDate,
					 rl.ArticlePUName as UnitName, rl.ArticlePUDecimals as UnitDecimals, rl.PurchasePrice as PurchasePrice, rl.ActualPrice as ActualPrice, rl.Discount,
					 sum(rlq.QuantityReceived) as QuantityReceived, sum(rlq.quantity) as Quantity
			  FROM (requisitionline rl, article a)
			  LEFT JOIN  requisitionlinequantity rlq ON rlq.requesitionlineid=rl.id and rlq.active=1
			  LEFT JOIN articlesize az ON az.id=rlq.articlesizeid
			  LEFT JOIN articlecolor ac ON ac.id=rl.articlecolorid
			  LEFT JOIN color c ON c.id=ac.colorid
			  LEFT JOIN delayreason ON delayreason.id=rl.delayreasonid
			  LEFT JOIN defectreason ON defectreason.id=rl.defectreasonid
			  LEFT JOIN CollectionLot ON CollectionLot.Id=rl.LotId
			  WHERE a.Id=rl.articleid and rl.RequisitionId=%d and rl.Active=1 
			  GROUP BY rl.id	
			  ORDER BY No", $Id) ;
	$res = dbQuery ($query) ;

    while ($row = dbFetch($res)) {
		listRow () ;
		listFieldIcon ('requisitionline.gif', 'purchaselineview', (int)$row['Id']) ;
        listFieldIcon ('pen.gif', 'purchlineedit', (int)$row['Id']) ;
		listField ((int)$row['No']) ;
//		listField (((int)$row['CaseId'] > 0) ? (int)$row['CaseId'] : '') ;
		listField ($row['ArticleNumber']) ;
		listField ($row['ArticleDescription']) ;

		$field = ($row['VariantColor']) ? $row['ColorNumber'] : '' ;
		listFieldRaw ($row['ColorName'] . ' - ' . $row['ColorNumber']) ;
		listFieldRaw ($row['Sizes']) ;

//		listField ($row['CollectionLotName']) ;
		listField (($row['RequestedDate']>'2001-01-01') ? date('Y-m-d', dbDateDecode($row['RequestedDate'])) : '') ;
		listField (($row['ConfirmedDate']>'2001-01-01') ? date('Y-m-d', dbDateDecode($row['ConfirmedDate'])) : '') ;
		listField (($row['AvailableDate']>'2001-01-01') ? date('Y-m-d', dbDateDecode($row['AvailableDate'])) : '') ;
		$s = ($row['Done']) ? sprintf ('%s', date('Y-m-d', dbDateDecode($row['DoneDate']))) : 'No' ;
		listfield ($s);
		listField (number_format ((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
		listField ((float)$row['QuantityReceived']>0 ? number_format ((float)$row['QuantityReceived'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName']: '', 'align=right') ;
		listField ((float)$row['QuantityReceived']>0 ? number_format ((float)$row['QuantityReceived']-(float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName']: '', 'align=right') ;
		listField (number_format((float)$row['PurchasePrice'], 2, ',', '.') . ' ' . $_currencysymbol, 'align=right') ;
//		listField ((int)$row['Discount'] . ' %', 'align=right') ;
		listField (number_format((float)$row['ActualPrice'], 2, ',', '.') . ' ' . $_currencysymbol, 'align=right') ;
		$v = (float)$row['ActualPrice'] * (float)$row['QuantityReceived'] ;
		$rectotal += $v ;
		listField (number_format($v, 2, ',', '.') . ' ' . $_currencysymbol, 'align=right') ;
		$v = (float)$row['ActualPrice'] * (float)$row['Quantity'] ;
		$total += $v ;
		$qtytotal += (float)$row['Quantity'] ;
		$qtyrectotal += (float)$row['QuantityReceived'] ;
		listField (number_format($v, 2, ',', '.') . ' ' . $_currencysymbol, 'align=right') ;
//		listFieldIcon ('pen.gif', 'orderlinedirectedit', (int)$row['Id']) ;
	}
	if (dbNumRows($res) == 0) {
		listRow () ;
		listField () ;
		listField ('No OrderLines', 'colspan=2') ;
	} else {
		// Total
		listRow () ;
		listField ('', 'colspan=11 style="border-top: 1px solid #cdcabb;"') ;
		listField (number_format ($qtytotal, 0, ',', '.') , 'align=right style="border-top: 1px solid #cdcabb;"') ;
		listField (number_format ($qtyrectotal, 0, ',', '.') , 'align=right style="border-top: 1px solid #cdcabb;"') ;
		listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;
		listField (number_format ($rectotal, 2, ',', '.') . ' ' . $_currencysymbol, 'align=right style="border-top: 1px solid #cdcabb;"') ;
		listField (number_format ($total, 2, ',', '.') . ' ' . $_currencysymbol, 'align=right style="border-top: 1px solid #cdcabb;"') ;
    }
	dbQueryFree ($res) ;
    listEnd () ;

    return 0 ;
?>
