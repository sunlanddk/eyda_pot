<script type="text/javascript" >
	function validateNumber(elem,maxvalue,qtylinename,totalname)
	{
		//alert ('hallo ') ;
		if(elem.value > maxvalue) {
			alert ('Quantity is limited to maximum available of'+' '+maxvalue) ;
			elem.value=maxvalue ;
		}
		
		var elmnts = document.getElementsByTagName("input");
		var totqty = parseInt(0);
		for(var i=0; i< elmnts.length; i++) {
			if (!elmnts[i].name.indexOf(qtylinename)) {
				totqty = parseInt(totqty) + parseInt(elmnts[i].value);
			}
		}
		document.getElementsByName(totalname)[0].value = totqty;
		//alert ('hallo ' ) ;
	}
</script>
<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

	if (isset($_GET['ArticleId'])) {
		$_articleid = (int)$_GET['ArticleId'] ;
	} else {
		$_articleid = 0 ;
	}

    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['Id']) ;     
    itemField ('Company', $Record['CompanyName']) ;     
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
	$reload = sprintf('onchange="appLoad(%d,%d,\'ArticleId=\'+document.appform.ArticleId.options[document.appform.ArticleId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
	if ($Record['SeasonId']>0) {
		$query = sprintf('SELECT Article.Id, concat(Article.Description, " (", Article.Number,")") AS Value 
						FROM collection c, collectionmember cm, Article 
						LEFT JOIN Requisitionline ol ON ol.id=%d and ol.articleid=Article.Id and ol.active=1
						WHERE c.seasonid=%d AND cm.collectionid=c.id AND Article.id=cm.articleid 
							AND c.Active=1 AND cm.Active=1 AND Article.Active=1 
							AND (isnull(ol.id) or ol.id=0)
						GROUP BY Article.Id
						ORDER BY Value', $Id, $Record['SeasonId']) ;
	} else {
		$query = 'SELECT Article.Id, concat(Article.Description, " (", Article.Number,")") AS Value FROM Article WHERE Active=1 And ArticleTypeId=1 ORDER BY Value' ;
	}
	itemFieldRaw ('Article', formDBSelect ('ArticleId', $_articleid, $query, 'width:250px;', NULL, $reload)) ; 
    itemSpace () ;
	if ($_articleid>0) {
		itemFieldRaw ('Description', formText ('Description', tableGetField('article','Description',$_articleid), 100, 'width:100%;')) ;
		itemSpace () ;
	/*	itemFieldRaw ('Req. Lot', formDBSelect ('LotId', (int)$Record['LotId'], 
										sprintf ('SELECT CollectionLot.Id, CollectionLot.Name AS Value 
											FROM CollectionLot
											WHERE SeasonId=%d AND Active=1 
											ORDER BY CollectionLot.`Date`', $Record['SeasonId']), 'width:200px')) ; 
	*/
		itemFieldRaw ('Request Date', formDate ('RequestedDate', $Record['RequestedDate']>'2002-01-01'? dbDateDecode($Record['RequestedDate']) :'')) ;
		itemFieldRaw ('Confirm Date', formDate ('ConfirmedDate', $Record['ConfirmedDate']>'2002-01-01'? dbDateDecode($Record['ConfirmedDate']) :'')) ;
	//	itemFieldRaw ('ExpectedDate', formDate ('ExpectedDate', dbDateDecode($Record['ExpectedDate']))) ;
		itemFieldRaw ('AvailableDate', formDate ('AvailableDate', $Record['AvailableDate']>'2002-01-01'? dbDateDecode($Record['AvailableDate']) :'')) ;
		itemSpace () ;
		itemFieldRaw ('Price', formText ('PurchasePrice', number_format((float)$Record['PurchasePrice'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
	//  itemFieldRaw ('Actual Price', formText ('ActualPrice', number_format((float)$Record['PurchasePrice'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
		$query = sprintf("select Symbol as CostCurrencySymbol from article, currency where article.id=%d and currency.id=article.costcurrencyid",  (int)$_articleid) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		itemFieldRaw ('Logistic Cost', formText ('LogisticCost', number_format((float)$Record['LogisticCost'], 2, ',', ''), 12, 'text-align:right;') . ' ' . $row['CostCurrencySymbol']) ;
		dbQueryFree($result) ;
	//	itemFieldRaw ('Discount', formText ('Discount', (int)$Record['Discount'], 12, 'text-align:right;') . ' %') ;
		itemSpace () ;
		if (1) { //($Record['VariantSize']) {
			$query = sprintf ("SELECT ac.id as ArticleColorId, c.Description as ColorName, c.Number as ColorNumber
								FROM (articlecolor ac, color c)
                                left join requisitionline rl ON rl.requisitionid=%d and rl.active=1 and rl.ArticleColorId=ac.id
								where ac.articleid=%d and ac.active=1 and c.id=ac.colorid and isnull(rl.id)", $Id, (int)$_articleid) ;

			$result = dbQuery ($query) ;
			$Color = array() ;
			while ($row = dbFetch ($result)) {
				$Color[$row['ArticleColorId']] = $row ;
			}
			dbQueryFree ($result) ;

			$Size = array() ;
			$sizequery = sprintf ("SELECT az.id as ArticleSizeId, az.DisplayOrder as DisplayOrder, az.name as Name
								FROM (articlesize az)
								where az.articleid=%d and az.active=1 order by DisplayOrder", (int)$_articleid) ;
			$sizeresult = dbQuery ($sizequery) ;
			while ($sizerow = dbFetch ($sizeresult)) {
				$Size[$sizerow['DisplayOrder']] = $sizerow ;		
			}
			dbQueryFree ($sizeresult) ;
		


			listStart () ;
		//	listHeader ('Quantities') ;
			listRow () ;
			listHead('Color', 30) ;
			foreach ($Size as $i => $s) {
				listHead ('', 1) ;
				listHead ($s['Name'], 20) ;
			}
			listHead('Total', 40) ;
			listRow () ;
			foreach ($Color as $j => $c) {
				$_total = 0 ;
				listField (substr($c['ColorName'],0,21)) ;
				foreach ($Size as $i => $s) {
					listField('') ;
					// already ordered qty
					$qnty = 999 ;
					 $_qty = 0 ;
					listFieldRaw (formText('Quantity['.$j.']['.$s['ArticleSizeId'].']', $_qty, 3, 'text-align:right;','maxvalue="' . $qnty . '"' . ' ' . 'onchange="validateNumber(this,'.$qnty.', \'Quantity['.$j.']\',\'TotalQuantity['.$j.']\')"')) ;
				}
				ListFieldRaw (formText('TotalQuantity['.$j.']', $_total, 3,'text-align:right;','disabled')) ;
				listRow () ;
			}
			listEnd () ;



		} else {
			itemFieldRaw ('Quantity', formText ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 12, 'text-align:right;') . ' ' . htmlentities ($Record['UnitName'])) ;
		}
		itemEnd () ;itemStart() ;
		itemSpace () ;
	//    if (!$User['SalesRef']) 
	//		itemFieldRaw ('Customer ref.', formtext ('CustArtRef', $Record['CustArtRef'], 12, 'text-align:right;')) ;
		if (!$User['SalesRef']) 
			itemFieldRaw ('Line Footer', formtextArea ('InvoiceFooter', $Record['InvoiceFooter'], 'width:100%;height:100px;')) ;
	}
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
