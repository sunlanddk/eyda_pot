<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
   	require_once 'lib/reader.inc';
    require_once 'lib/save.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

    $fields = array (    
		'Something'		=> array ('type' => 'checkbox','check' => true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'PickPosGenerated':
				// Ignore if not setting flag
				if (!$changed) return false ;
				return true ;
		}
		if (!$changed) return false ;
		return true ;	
    }
    
    $res = saveFields ('Season', $Id, $fields, true) ;
    if ($res) return $res ;

	$Error = 0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);
		
		$k = 0 ;

		$First = 1 ;
		$NextLineNo = 1 ;

		// Process lines
		for ($i = 8; $i <= $data->sheets[0]['numRows']; $i++) {
			if ($data->sheets[$k]['cells'][$i][1] == '') continue ; // Ignore line if no articlenumber 

			// Get article information
			$query = sprintf ("SELECT * FROM Article WHERE Active=1 AND Number='%s'", $data->sheets[$k]['cells'][$i][1]) ;
		      $res = dbQuery ($query) ;
			$Article = dbFetch ($res) ;
			dbQueryFree ($res) ;
			if ($Article['Id'] == 0) return sprintf('Article %s in line %d dosnt exist',$data->sheets[$k]['cells'][$i][1], $i) ;

// HACK to handle transition to Global tags				
if($Article['SupplierCompanyId']==516) $Article['SupplierCompanyId']=5060 ;


			if ($PurchaseOrders[$Article['SupplierCompanyId']] > 0 ) {
				$PurchaseOrderId = $PurchaseOrders[$Article['SupplierCompanyId']] ;
			} else {
				// Create new PurchaseOrder for supplier of article
				$First = 0 ;
				// Get supplier information
				$query = sprintf ("SELECT * FROM Company WHERE Active=1 AND Id=%d", $Article['SupplierCompanyId']) ;
				$res = dbQuery ($query) ;
				$Supplier = dbFetch ($res) ;
				dbQueryFree ($res) ;
				if ($Supplier['Id'] == 0) return sprintf('Supplier for Article %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
				
				// Create new Purchase Order
				$PurchaseOrder = array (
//					'Id' => 0,
					'CompanyId' => $Supplier['Id'],
					'Description' => sprintf('%s - %s', $Record['Name'], tableGetField('Company','Name',$Record['CompanyId'])) ,
					'Reference' => '' ,
					'PurchaseUserId' => $Supplier['PurchaseRefId'],
					'CurrencyId' => $Supplier['PurchaseCurrencyId'],
					'PaymentId' => $Supplier['PurchasePaymentTermId'],
					'DeliveryTermId' => $Supplier['PurchaseDeliveryTermId'],
					'CarrierId' => $Supplier['PurchaseCarrierId'],
					'DeliveryId' => 2, // Novotex
					'RequisitionHeader' => $data->sheets[$k]['cells'][3][2],
					'RequisitionFooter' => $data->sheets[$k]['cells'][4][2],
					'FromCompanyId' => $User['CompanyId']
				) ;
				$PurchaseOrderId = tableWrite ('requisition', $PurchaseOrder) ;
				$PurchaseOrders[$Article['SupplierCompanyId']] = $PurchaseOrderId ;
			}

			// Get articleColor information
			if ($Article['VariantColor']) {
				$Color = $data->sheets[$k]['cells'][$i][3] ;
				$ColorId = tableGetFieldWhere('Color','Id',sprintf("Number='%s' and Active=1",$data->sheets[$k]['cells'][$i][3]));
				if ($ColorId == 0) return sprintf('Color %s, in line %d dosnt exist', $data->sheets[$k]['cells'][$i][3], $i) ;
				$ArticleColorId = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d And Active=1",$Article['Id'],$ColorId));
				if ($ArticleColorId == 0) return sprintf('ArticleColor <%s>, in line %d dosnt exist %d,%d', $data->sheets[$k]['cells'][$i][3], $i,$Article['Id'],$ColorId ) ;

				$queryWhereClause = sprintf('RequisitionId=%d AND ArticleId=%d AND ArticleColorId=%d AND Active=1',$PurchaseOrderId,$Article['Id'],$ArticleColorId);
			} else {
				$ArticleColorId = 0 ;

				$queryWhereClause = sprintf('RequisitionId=%d AND ArticleId=%d AND Active=1',$PurchaseOrderId,$Article['Id']);
			}
			
			// Get article size info
			if ($Article['VariantSize']) {
				$ArticleSizeId = tableGetFieldWhere ('articlesize','Id', sprintf("Active=1 And ArticleId=%d AND Name='%s'", $Article['Id'],$data->sheets[$k]['cells'][$i][5])) ;
				//return sprintf('%d - %s', $ArticleSizeId, sprintf("Active=1 And ArticleId=%d AND Name='%s'", $Article['Id'],$data->sheets[$k]['cells'][$i][5])) ;
			}


			// Get allocation information
			$CaseId = (int)($data->sheets[$k]['cells'][$i][6]) ;
			$ProductionId = (int)tableGetFieldWhere ('production', 'Id', sprintf("active=1 AND caseid=%d AND Number=%d", (int)$data->sheets[$k]['cells'][$i][6], (int)$data->sheets[$k]['cells'][$i][7])) ;
			$queryAllocWhereClause = sprintf(' AND CaseId=%d AND ProductionId=%d', $CaseId, $ProductionId) ;

			// Get certificate information.
/*
			if ($data->sheets[$k]['cells'][$i][16]) {
				$ProductArticleCertificateId = tableGetFieldWhere('Case', 'ArticleCertificateId', sprintf("Active=1 And Id=%d", $CaseId)) ;				
				$CertificateId = tableGetFieldWhere('ArticleCertificate', 'CertificateId' sprintf("Active=1 And Id=%d", $ProductArticleCertificateId)) ;				
				$ArticleCertificateId = tableGetFieldWhere('ArticleCertificate', 'Id', sprintf("Active=1 And ArticleId=%d  And CertificateId=%d", $Article['Id'], $CertificateId)
			}
*/
			//$CertificateId = tableGetFieldWhere('Certificate', 'Id', sprintf("Active=1 And Name=%s", $data->sheets[$k]['cells'][$i][16])) ;
			$ArticleCertificateId = 0 ; //tableGetFieldWhere('ArticleCertificate', 'Id', sprintf("Active=1 And ArticleId=%d  And CertificateId=%d", $Article['Id'], $CertificateId)

			// Create quantity and/or line
			$PurchaseOrderLineId = tableGetFieldWhere ('requisitionline', 'Id', $queryWhereClause . $queryAllocWhereClause) ;
			if ($PurchaseOrderLineId>0) {
				// Purchase OrderLine allready exist.
				$LineExist = 1;
				if ($Article['VariantSize']) {
					if (tableGetFieldWhere ('requisitionlinequantity', 'Id', sprintf("Active=1 And ArticleSizeId=%d And RequesitionLineId=%d", $ArticleSizeId, $PurchaseOrderLineId)) > 0)
						return sprintf('Same size variant for the 2nd time in line %d', $i) ;
				} else if ($Article['VariantDimension']) {
					if (tableGetFieldWhere ('requisitionlinequantity', 'Id', sprintf("Active=1 And Dimension=%s And RequesitionLineId=%d", $data->sheets[$k]['cells'][$i][5], $PurchaseOrderLineId)) > 0)
						return sprintf('Same size variant for the 2nd time in line %d', $i) ;
				} else 
					return sprintf('Same variant for the 2nd time in line %d', $i) ;
			} else {
				$query = sprintf('select max(`No`) as LineNo from requisitionline where requisitionid=%d', $PurchaseOrderId ) ;
			      $res = dbQuery ($query) ;
				$Line = dbFetch ($res) ;
				dbQueryFree ($res) ;

				if ($data->sheets[$k]['cellsInfo'][$i][12]['type']) {
					$PricePurchase=$data->sheets[$k]['cells'][$i][12] ;
				} else {
					$PriceArray = explode(',', $data->sheets[$k]['cells'][$i][12]) ;
					$PricePurchase=$PriceArray[0] . '.' . $PriceArray[1] ;
				}
				if ($data->sheets[$k]['cellsInfo'][$i][14]['type']) {
					$PriceLogistic=$data->sheets[$k]['cells'][$i][14] ;
				} else {
					$PriceArray = explode(',', $data->sheets[$k]['cells'][$i][14]) ;
					$PriceLogistic=$PriceArray[0] . '.' . $PriceArray[1] ;
				}

				// Create new Purchase Order Line
				unset ($PurchaseOrderLine) ;
				$ResCompanyId = $CaseId>0 ? tableGetField('`Case`','CompanyId', $CaseId) : 0 ;
				$PurchaseOrderLine = array (
					'Id' => 0,
					'RequisitionId' => $PurchaseOrderId,
					'ArticleId' => $Article['Id'],
					'No' => (int)$Line['LineNo']+1,
					'Description' => $Article['Description'],
					'ArticleCertificateId' => $ArticleCertificateId,
					'Surplus' => $Supplier['PurchaseSurplus'],
					'PurchasePriceConsumption' => $PricePurchase,
					'ActualPriceConsumption' => $PricePurchase,
					'LogisticCostConsumption' => $PriceLogistic,
					'PurchasePrice' => $PricePurchase,
					'ActualPrice' => $PricePurchase,
					'LogisticCost' => $PriceLogistic,
					'ReservationId' => $ResCompanyId ,
//					'PurchaseForId' => $ResCompanyId ,
					'CaseId' => $CaseId,
					'ProductionId' => $ProductionId,
//					'RequestedDate' => $data->sheets[$k]['cells'][$i][8],
//					'ConfirmedDate' => ,
//					'ExpectedDate' => 0,
//					'AvailableDate' => $data->sheets[$k]['cells'][$i][9],
					'LineFooter' => $data->sheets[$k]['cells'][$i][15],
					'ArticleComment' => $Article['Comment'],
					'Group' => 1
//					'ArticlePurchaseUnitId' => $ArticlePurchaseUnitId,								
//					'ArticlePURatio' => 1,								
//					'ArticlePUName' => tableGetField('Unit','Name',$Article['UnitId']),								
//					'ArticlePUDecimals' => tableGetField('Unit','Decimals',$Article['UnitId'])				
				) ;

				// Set Purchase Unit
				$AriclePurchaseUnitId = 0 ;
				$UnitId = tableGetFieldWhere ('Unit','Id',sprintf('name="%s"', $data->sheets[$k]['cells'][$i][11])) ;
				if ($UnitId > 0)
	 				$ArticlePurchaseUnitId = tableGetFieldWhere('articlepurchaseunit','Id',sprintf('ArticleId=%d AND UnitId=%d',$Article['Id'], $UnitId)) ;
				if ($ArticlePurchaseUnitId > 0) {
					$PurchaseOrderLine['ArticlePurchaseUnitId'] 	= $ArticlePurchaseUnitId ;
					$PurchaseOrderLine['ArticlePURatio']		= tableGetField('articlepurchaseunit','Ratio',$ArticlePurchaseUnitId) ;
					$PurchaseOrderLine['ArticlePUName']			= tableGetField('Unit','Name',$UnitId) ;
					$PurchaseOrderLine['ArticlePUDecimals']		= tableGetField('Unit','Decimals',$UnitId) ;
				} else {  
	 				$PurchaseOrderLine['ArticlePurchaseUnitId']	= tableGetFieldWhere('articlepurchaseunit','Id',sprintf('ArticleId=%d AND UnitId=%d',$Article['Id'], $Article['UnitId'])) ;
					$PurchaseOrderLine['ArticlePURatio']		= 1 ;
					$PurchaseOrderLine['ArticlePUName']			= tableGetField('Unit','Name',$Article['UnitId']) ;
					$PurchaseOrderLine['ArticlePUDecimals']		= tableGetField('Unit','Decimals',$Article['UnitId']) ;
				}


				if ($ArticleColorId>0)
					$PurchaseOrderLine['ArticleColorId'] = $ArticleColorId ;
				if (ereg ("^([0-9]{4})-([0-9]{2})-([0-9]{2})$", $data->sheets[$k]['cells'][$i][8], $regs))
					$PurchaseOrderLine['RequestedDate'] = $data->sheets[$k]['cells'][$i][8];
				if (ereg ("^([0-9]{4})-([0-9]{2})-([0-9]{2})$", $data->sheets[$k]['cells'][$i][9], $regs))
					$PurchaseOrderLine['AvailableDate'] = $data->sheets[$k]['cells'][$i][9];
				$PurchaseOrderLineId = tableWrite ('requisitionline', $PurchaseOrderLine) ;
				$NextLineNo ++ ;
			}
			
			// Create new Purchase Order Line Quantity
			unset ($PurchaseOrderLineQty) ;
			$PurchaseOrderLineQty = array (
				'Id' => 0,
				'RequesitionLineId' => $PurchaseOrderLineId,
				'ArticleSizeId' => 0,
//				'Dimension' => NULL,
				'Quantity' => $data->sheets[$k]['cells'][$i][10],
			) ;
			if ($Article['VariantSize']) {
				$PurchaseOrderLineQty['ArticleSizeId'] =  $ArticleSizeId;
				if ($PurchaseOrderLineQty['ArticleSizeId'] == 0) return sprintf('ArticleSize <%s>, in line %d dosnt exist %d, %d', $data->sheets[$k]['cells'][$i][5], $i,$Article['Id'],$ArticleSizeId) ;
			}
			if ($Article['VariantDimension']) $PurchaseOrderLineQty['Dimension'] = $data->sheets[$k]['cells'][$i][5];
			$PurchaseOrderLineQtyId = tableWrite ('requisitionlinequantity', $PurchaseOrderLineQty) ;
		}
	}
	if (sizeof($PurchaseOrders) > 1)
		return navigationCommandMark ('reqseasonlist', (int)$Id) ;
	else if (sizeof($PurchaseOrders)== 1)
		return navigationCommandMark ('requisitionview', (int)$PurchaseOrderId) ;
	else return 'No Purchase Orders created' ;
?>
