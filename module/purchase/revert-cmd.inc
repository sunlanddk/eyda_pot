<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    
    $fields = array (
		'Done'			=> array ('type' => 'checkbox',	'check' => true),
		'Ready'			=> array ('type' => 'checkbox',	'check' => true),
		) ;

    $flaglist = '' ;
    $flagcount = 0 ;
    
    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	global $flagcount, $flaglist ;
	switch ($fieldname) {
	    case 'Ready' :
		if (!$changed or $value) return false ;
		if ($Record['Done']) return 'Ready can not be reverted when still Done' ;
		break ;
	}
	
	if (!$changed or $value) return false ;
		$flaglist .= sprintf ("        %s\n", $fieldname) ;
		$flagcount++ ;
		return true ;
    }

    $_transactionId = tableGetFieldWhere('transaction', 'Id', '`Type`="Receive" and ObjectId='.$Id) ;
    if ($_transactionId) return 'Revert not allowed when already received some items' ;
    
    $res = saveFields ('Requisition', $Id, $fields, true) ;
    if ($res) return $res ;

    // Remove stored pdf-document and items in purchase.
    if (!$fields['Ready']['value'] and $fields['Ready']['db']) {
		$file = fileName ('purchase', (int)$Record['Id']) ;
		if (is_file($file)) unlink ($file) ;
		
		// remove all items belonning to PO.
	    $query = sprintf ("UPDATE Item SET Active=0, ContainerId=0, RequisitionId=0, RequisitionLineId=0 WHERE RequisitionId=%d", (int)$Id) ;
	    dbQuery ($query) ;

		//  set all received quantities to zero.
	    $query = sprintf ("SELECT Id FROM requisitionline WHERE RequisitionId=%d", (int)$Id) ;
	    $result=dbQuery ($query) ;
		while ($row = dbFetch ($result)) {
			$query = sprintf ("UPDATE requisitionlinequantity SET QuantityReceived=0 WHERE RequesitionLineId=%d", (int)$row['Id']) ;
			dbQuery ($query) ;
		}
		dbQueryFree($result) ;
		

		//  set all lines undone.
	    $query = sprintf ("UPDATE requisitionline SET Done=0, DoneUserId=0 WHERE RequisitionId=%d", (int)$Id) ;
	    dbQuery ($query) ;
		
		// Update Stock transaction
    }
    
    // Generate news
    if ($flagcount > 0) {
		unset ($row) ;
		$row['Header'] = sprintf ("Purchase Order %d (%s) reverted", $Record['Id'], $Record['Description']) ;
		$row['Text'] = sprintf ("The flag%s:\n\n%s\nhas been reverted by %s (%s) at %s", ($flagcount > 1) ? 's' : '', $flaglist, $User['FullName'], $User['Loginname'], date('Y-m-d H:i:s', dbDateDecode($Record['ModifyDate']))) ;
		tableWrite ('News', $row) ;
    }

    return 0 ;	
?>
