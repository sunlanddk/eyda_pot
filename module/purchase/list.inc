<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;
    switch ($Navigation['Parameters']) {
		case 'company' :
			itemStart () ;
			itemSpace () ;
			itemFieldIcon ('Company', tableGetField('Company','Name',$Id), 'company.gif', 'companyview', $Id) ;
			itemEnd () ;
			printf ('<br>') ;
			break ;	    
		case 'season' :
			itemStart () ;
			itemSpace () ;
			itemFieldIcon ('Season', tableGetField('Season','Name',$Id), 'project.gif', 'seasonview', $Id) ;
			itemEnd () ;
			printf ('<br>') ;
			break ;	    
    }
   
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 70) ;
    listHead ('State', 70) ;
    if (!$HideCompany) {
		listHead ('Company', 140) ;
    }
//    listHead ('Season', 140) ;
    listHead ('Reference', 90) ;
//    listHead ('Description') ;
    listHead ('Products') ;
    listHead ('Quantity', 60, 'align=right') ;
	listHead ('Received', 60, 'align=right') ;
    listHead ('Price', 80, 'align=right') ;
    listHead ('', 40) ;
    listHead ('Purchase resp', 160) ;
    listHead ('Created', 120) ;
    if (!$HideDone) {
		listHead ('Done', 75) ;
    }

    while ($row = dbFetch($Result)) {
		listRow () ;
		if ($Navigation['Parameters']=='season') 
			listFieldIcon ($Navigation['Icon'], 'poviewseason', (int)$row['Id']) ;
		else
			listFieldIcon ($Navigation['Icon'], 'purchaseview', (int)$row['Id']) ;
		listField ((int)$row['Id']) ;
		listField ($row['State']) ;
		if (!$HideCompany) {
			listField (sprintf ('%s (%s)', $row['CompanyName'], $row['CompanyNumber'])) ;
		}
//		listField ($row['SeasonName']) ;
		listField ($row['Reference']) ;
//		listField ($row['Description']) ;
		$_query = sprintf("Select group_concat(Description separator ', ') as Products
							From (select Description from requisitionline rl where requisitionid=%d and active=1 group by ArticleId) tab",(int)$row['Id']) ; 
		$_res = dbQuery($_query) ;
		$_row = dbFetch($_res) ;
		dbQueryFree($_res) ;
		listField ($_row['Products']) ;

		$_query = sprintf("SELECT sum(rlq.quantity) as Quantity, sum(rlq.quantityreceived) as Received, sum(rlq.quantity*rl.PurchasePrice) as Price
							FROM (requisitionline rl,requisitionlinequantity rlq)
							Where rl.requisitionid=%d and rl.active=1 and rlq.RequesitionLineId=rl.id", (int)$row['Id']) ; 
		$_res = dbQuery($_query) ;
		$_row = dbFetch($_res) ;
		listField ((int)$_row['Quantity'], 'align=right') ;
		listField ((int)$_row['Received'], 'align=right') ;
		listField (number_format((float)$_row['Price'], 2, ',', '.'), 'align=right') ;
		listField ($row['CurrencySymbol']) ;
		dbQueryFree($_res) ;
		listField ($row['PurchaseUserName']) ;
		listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
		if (!$HideDone) {
			listField (($row['Done']) ? date ("Y-m-d", dbDateDecode($row['DoneDate'])) : 'No') ;
		}	
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Orders', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
