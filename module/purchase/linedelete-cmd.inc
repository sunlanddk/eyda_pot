<?php

    require_once 'lib/table.inc' ;

    // Order Ready ?
    if ($Record['OrderDone']) return 'the Order is Done' ;
    if ($Record['OrderReady']) return 'the Order is Ready' ;
 
    // Do delete  
    tableDelete ('RequisitionLine', $Id) ;
    tableDeleteWhere ('RequisitionLineQuantity', sprintf ('RequesitionLineId=%d', $Id)) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM RequisitionLine WHERE RequisitionId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['RequisitionId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
		while ($row = dbFetch ($result)) {
		if ($i != (int)$row['No']) {
			$query = sprintf ("UPDATE RequisitionLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
			dbQuery ($query) ;
		}
		$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
