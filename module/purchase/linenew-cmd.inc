<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;

    // Validate
    if ($Record['Done']) return 'the OrderLine can not be added when Order is Done' ;
    if ($Record['Ready']) return 'the OrderLine can not be added when Order is Ready' ;
 
    // Find next position
    $query = sprintf ('SELECT MAX(No) AS No, MAX(RequestedDate) AS RequestedDate, MAX(AvailableDate) AS AvailableDate 
							FROM RequisitionLine 
							WHERE RequisitionId=%d AND Active=1', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $No = (int)$row['No'] ;

    $fields = array (
		'No'						=> array ('type' => 'set'),
		'RequisitionId'				=> array ('type' => 'set',		'value' => $Id),
		'ArticleId'					=> array ('check' => true),
		'ArticleColorId'			=> array ('type' => 'set'),
		'UnitName'					=> array ('type' => 'set'),
		'UnitDecimals'				=> array ('type' => 'set'),

		'Description'				=> array (),

		'RequestedDate'				=> array ('type' => 'date',								'check' => true),
		'ConfirmedDate'				=> array ('type' => 'date',								'check' => true),
		'AvailableDate'				=> array ('type' => 'date',								'check' => true),

		'PurchasePrice'				=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'ActualPrice'				=> array ('type' => 'set'), 
		'LogisticCost'				=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
		'PurchasePriceConsumption'	=> array ('type' => 'set'),
		'ActualPriceConsumption'	=> array ('type' => 'set'),
		'LogisticCostConsumption'	=> array ('type' => 'set'),
//		'Discount'					=> array ('type' => 'integer',	'mandatory' => true),

		'InvoiceFooter'		=> array ()
    ) ;
 
   function checkfield ($fieldname, $value, $changed) {
		global $Id, $fields, $Record, $ArticleTypeFields ;
		switch ($fieldname) {
			case 'ArticleId':
				// Validate
				// Get Article
				$query = sprintf ('SELECT Article.Id, Article.Number, Article.Description, Unit.Name as UnitId, Unit.Name as UnitName, Unit.Decimals as UnitDecimals FROM (Article, Unit) WHERE Article.Id="%s" AND Article.Active=1 AND Unit.Id=Article.UnitId', (int)$value) ;
				$result = dbQuery ($query) ;
				$row = dbFetch ($result) ;
				dbQueryFree ($result) ;
				if ((int)$row['Id'] == 0) return 'Article not found - value '. $value ;

 				$ArticlePurchaseUnitId = tableGetFieldWhere('articlepurchaseunit','Id',sprintf('ArticleId=%d AND UnitId=%d',$row['Id'], $row['UnitId'])) ;
				if ($ArticlePurchaseUnitId > 0) {
					$PurchaseOrderLine['ArticlePurchaseUnitId'] = $ArticlePurchaseUnitId ;
					$PurchaseOrderLine['ArticlePURatio']		= tableGetField('articlepurchaseunit','Ratio',$ArticlePurchaseUnitId) ;
					$PurchaseOrderLine['ArticlePUName']			= $row['UnitName'] ; //tableGetField('Unit','Name',$UnitId) ;
					$PurchaseOrderLine['ArticlePUDecimals']		= $row['UnitDecimals'] ; //tableGetField('Unit','Decimals',$UnitId) ;
				} else {  
	 				$PurchaseOrderLine['ArticlePurchaseUnitId']	= tableGetFieldWhere('articlepurchaseunit','Id',sprintf('ArticleId=%d AND UnitId=%d',$row['Id'], $row['UnitId'])) ;
					$PurchaseOrderLine['ArticlePURatio']		= 1 ;
					$PurchaseOrderLine['ArticlePUName']			= $row['UnitName'] ; //tableGetField('Unit','Name',$Article['UnitId']) ;
					$PurchaseOrderLine['ArticlePUDecimals']		= $row['UnitDecimals'] ; //tableGetField('Unit','Decimals',$Article['UnitId']) ;
				}

				// ArticleNumber are not to be used
				return true ;

			case 'ConfirmedDate' :
				if (!$changed) return false ;

				// Validate
				$t = dbDateDecode ($value) ;
	//			if ($t < utsDateOnly(time())) return 'Confirmed Date can not be in the past' ;
				return true ;
				
			case 'AvailableDate' :
				if (!$changed) return false ;

				// Validate
				$t = dbDateDecode ($value) ;
	//			if ($t < utsDateOnly(time())) return 'Available Date can not be in the past' ;
				return true ;
			
			case 'RequestedDate' :
				if ((!$changed) or ($value=='')) return false ;
				// Validate
				$t = dbDateDecode ($value) ;
	//			if ($t <= 0) return 'Requested Date must be set' ;
				return true ;
		}
		return false ;
    } 

    // Update field list
	$fields['ActualPrice']['value'] 				= (100-(int)$_POST['Discount'])*(float)str_replace(',','.',$_POST['PurchasePrice'])/100 ;

	$fields['PurchasePriceConsumption']['value'] 	= (float)str_replace(',','.',$_POST['PurchasePrice']) ;
	$fields['ActualPriceConsumption']['value'] 		= (100-(int)$_POST['Discount'])*(float)str_replace(',','.',$_POST['PurchasePrice'])/100 ;  //$_POST['ActualPrice'] ; 
	$fields['LogisticCostConsumption']['value'] 	= (float)str_replace(',','.',$_POST['LogisticCost']); 										//$_POST['LogisticCost'] ;
	
	// Valdate qty entries
	$Record['UnitDecimals'] = 0 ;
	$format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", 9-(int)$Record['UnitDecimals'], 9-(int)$Record['UnitDecimals'], (int)$Record['UnitDecimals']) ;
	foreach($_POST['Quantity'] as $cid => $color) {
		$_quantity = 0 ;
		foreach($color as $sid => $value_string) {
			$value_string = trim($value_string) ;
			if ($value_string == '') $value_string = '0' ;

			// verify format
			if (!ereg($format,$value_string)) return sprintf ("invalid quantity for %s", $s['Name']) ;

			// Substitute , -> .
			$value_string = str_replace (',', '.', $value_string) ;
			
			$_quantity += (float)$value_string ;
		}
		$_totals[$cid] = (int)$_quantity ;
	}
	foreach($_POST['Quantity'] as $cid => $color) {
		if ($_totals[$cid]==0) continue ;
		$n++ ;
		$fields['ArticleColorId']['value'] = $cid ;
		$fields['No']['value'] = $No + $n ;
		$res = saveFields ('RequisitionLine', -1, $fields, true) ;
		if ($res) return $res ;
		foreach($color as $sid => $value) {
			$value = str_replace (',', '.', $value) ;
			$rlq_fields = array (
				'RequesitionLineId'	=> array ('type' => 'set',	'value' => (int)$Record['Id']),
				'ArticleSizeId'		=> array ('type' => 'set',	'value' => (int)$sid),
				'Quantity'			=> array ('type' => 'set',	'value' => $value) 
			) ;
			$res = saveFields ('RequisitionLineQuantity', -1, $rlq_fields) ;
		}
	}

	return 0 ;   
    return navigationCommandMark ('purchaselineedit', dbInsertedId ()) ;

?>
