<?php

    require_once 'lib/navigation.inc' ;

	$PurchasePrice = (float)str_replace(',','.',$_POST['PurchasePrice']);
	$Discount = (int)$_POST['Discount'];
	$RequestedDate = $_POST['RequestedDate'] ;
	$AvailableDate = $_POST['AvailableDate'] ;
	$ConfirmedDate = $_POST['ConfirmedDate'] ;
	

	// Update lines
    foreach ($_POST['SelectFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		$SomethingSet = 1 ;
		
		// Make some validations?
		if ($Record['Ready']==0) {
			if (isset($Discount) and isset($PurchasePrice)){
				$RequisitionLine['Discount'] = $Discount ;
				$RequisitionLine['PurchasePrice'] = $PurchasePrice ;
				$RequisitionLine['PurchasePriceConsumption'] = $PurchasePriceConsumption ;
				$RequisitionLine['ActualPrice'] = ((100-$Discount)*$PurchasePrice)/100 ;
				$RequisitionLine['ActualPriceConsumption'] = ((100-$Discount)*$PurchasePrice)/100 ;
			} else if (isset($PurchasePrice)) {
				$RequisitionLine['PurchasePrice'] = $PurchasePrice ;
				$RequisitionLine['PurchasePriceConsumption'] = $PurchasePriceConsumption ;
				$_query = 'select * from requisitionline where id=' . $id ;
				$_res = dbQuery($_query) ;
				$_row = dbFetch($_res) ;
				$RequisitionLine['ActualPrice'] = ((100-$_row['Discount'])*$PurchasePrice)/100 ;
				$RequisitionLine['ActualPriceConsumption'] = ((100-$_row['Discount'])*$PurchasePrice)/100 ;
			} else if (isset($Discount)) {
				$RequisitionLine['Discount'] = $Discount ;
				$_query = 'select * from requisitionline where id=' . $id ;
				$_res = dbQuery($_query) ;
				$_row = dbFetch($_res) ;
				$RequisitionLine['ActualPrice'] = ((100-$Discount)*$_row['PurchasePrice'])/100 ;
				$RequisitionLine['ActualPriceConsumption'] = (100-$Discount)*$RequisitionLine['PurchasePriceConsumption']/100 ;
			}
		}
		if ($RequestedDate>'2001-01-02') 
			$RequisitionLine['RequestedDate'] = $RequestedDate ;
		if ($AvailableDate>'2001-01-02') 
			$RequisitionLine['AvailableDate'] = $AvailableDate ;
		if ($ConfirmedDate>'2001-01-02') 
			$RequisitionLine['ConfirmedDate'] = $ConfirmedDate ;

		tableWrite ('RequisitionLine', $RequisitionLine, $id) ;
	}

	if ($SomethingSet)
	    return 0 ;
	else
		return "No lines selected - Nothing changed" ;
?>
