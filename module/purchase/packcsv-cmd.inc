<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	function array_2_csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {
				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
	
	
	global $User, $Navigation, $Record, $Size, $Id ;

	$ErrorTxt = 0 ;

    // header
    // header('Content-Length: '.$size); 
    header ('Content-Type: text/plain') ;
    header ('Content-Disposition: attachment; filename=PrePackInfo.csv') ;
    header ('Last-Modified: '.date('D, j M Y G:i:s T')) ;

//	$query = sprintf ('SELECT o.id as Ordrenummer, c.number as Kundenr, co.Description as KundeLand,c.name,if(c.id=7547,o.Address1,c.name) as LeveringsNavn,
//	$query = sprintf ('SELECT o.consolidatedid as Ordrenummer, c.number as Kundenr, co.Description as KundeLand,c.name,if(c.id=7547,o.Address1,c.name) as LeveringsNavn,
	$query = sprintf ('SELECT (select min(`order`.id) from `order` where `order`.consolidatedid=o.consolidatedid) as Ordrenummer, 
								c.number as Kundenr, co.Description as KundeLand,c.name,if(c.id=7547,o.Address1,c.name) as LeveringsNavn,
								oq.Id as OqId, a.number as Varenr,a.description as VareNavn,color.description as Farve,az.name as Str, oq.quantity as Antal, oq.quantity as PackAntal
						FROM (requisition r, `order` o, orderline ol, company c, orderquantity oq)
						INNER join collection col ON col.seasonid=o.seasonid and col.active=1
						INNER join collectionmember cm ON cm.collectionid=col.id and cm.articleid=ol.articleid and cm.articlecolorid=ol.articlecolorid and cm.active=1 and cm.cancel=0
						INNER JOIN article a ON a.id=ol.articleid AND a.suppliercompanyid=r.companyid
						LEFT JOIN articlesize az ON az.id=oq.articlesizeid
						LEFT JOIN variantcode v ON v.articleid=ol.articleid and v.articlecolorid=ol.articlecolorid and v.articlesizeid=oq.articlesizeid and v.active=1
						LEFT JOIN country co ON c.countryid=co.id
						LEFT JOIN `Company` AS `Owner` ON o.ToCompanyId=Owner.Id
						LEFT JOIN articlecolor ac ON ac.id=ol.articlecolorid
						LEFT JOIN color ON color.id=ac.colorid
						WHERE r.id = %d AND o.seasonid=r.seasonid AND ol.orderid = o.id
							and oq.orderlineid=ol.id and c.id=o.companyid and oq.quantity>0
							and o.done=0 and o.proposal=0 and o.ready=1 and ol.done=0 
							and o.active=1 and ol.active=1 and oq.active=1 
						ORDER BY co.id, o.consolidatedid, ol.id, az.displayorder', $Id) ;
//						ORDER BY co.id, o.id, ol.id, az.displayorder', $Id) ;
	$result = dbQuery ($query) ;

//	$file = fopen("test.txt","w");
	$n = 0 ;
	while ($row = dbFetch ($result)) {
		$csv_data = array_2_csv($row);
		$csv_data .= "; \r\n" ;
//		echo $n . ":;; <pre>";
//		fwrite($file,$csv_data.PHP_EOL);
		print_r($csv_data);
//		echo '</pre>'   ; 
		$n++ ;  
	}
	dbQueryFree ($result) ;
//	fclose($file);
	
	return 0; // navigationCommandMark ('purchaseview', (int)$Record['Id']) ;

?>
