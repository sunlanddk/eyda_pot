<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;
//    global $Record, $Company, $CompanyMy, $Line, $Now, $ProformaInvoice, $ProformaCustoms, $LOT, $_l_margin, $top_margin, $bottom_margin ;

	$_l_margin = 15 ;
	$top_margin = 39 ;
	$bottom_margin = 10 ;

    class PDF extends FPDF {

		function Header() {
			global $Record, $Company, $CompanyMy, $CompanyDelivery, $Now, $ProformaInvoice, $ProformaCustoms, $LOT, $_l_margin, $top_margin, $bottom_margin ;
			$this->SetAutoPageBreak(false) ;

			$this->SetFont('Arial','',16);
			$this->SetMargins($_l_margin,0,0) ;
			$this->SetY ($top_margin+5) ;
			$orderType = '';
			if ($ProformaInvoice == 0 and $ProformaCustoms == 0) {
				if ($Record['Proposal']) {
					$orderType = 'PROPOSAL';
				} else {
					$orderType = sprintf('PURCHASE ORDER %s', !$Record['Ready'] ? '(draft)' : '');
				}
			} else {
				$orderType = 'Proforma Invoice';
			}
			$this->Cell(30, 8, $orderType, 0, 1) ;

			// My Company Header
		    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['FromCompanyId']) ;
	    	$this->Image ($ImageString, 167, 10, 30 ) ;

			// My Company Info
			$this->SetFont('Arial','',40);
			$this->SetMargins(170,0,0) ;
			$this->SetY (23) ; //($top_margin+10) ; // 14
	//		$this->Cell(30, 8, FUB, 0, 1) ;
	//	    $this->Cell(49, 4, sprintf ('Reg. No %s', $CompanyMy['RegNumber']), 0, 1, 'R') ;
	//	    $this->Cell(49, 4, sprintf ('Telephone %s', $CompanyMy['PhoneMain']), 0, 1, 'R') ;
	//	    $this->Cell(49, 4, sprintf ('Email %s', $CompanyMy['Mail']), 0, 1, 'R') ;

			// supplier Address
			$this->SetFont('Arial','B',9);
			$this->SetMargins($_l_margin,0,0) ;
			$this->SetY ($top_margin+29) ;
			$this->Cell(80, 4, $Company['Name'], 0, 1) ;
			$this->SetFont('Arial','',9);
			$this->Cell(80, 4, $Company['Address1'], 0, 1) ;
			if ($Company['Address2']) $this->Cell(80, 4, $Company['Address2'], 0, 1) ;
			$this->Cell(80, 4, sprintf ('%s %s', $Company['ZIP'], $Company['City']), 0, 1) ;
			$this->Cell(80, 4, $Company['CountryDescription'], 0, 1) ;
/*
			$this->SetFont('Arial','',9);
			$this->Cell(13, 4, 'VAT No:') ;
			$this->Cell(74, 4, $Company['VATNumber'],0,1, 'L') ;
*/
			$this->Ln () ;

			// Delivery Address
			$this->SetFont('Arial','',9);
			$this->SetMargins(90,0,0) ;
			$this->SetY ($top_margin+29) ;
			$this->Cell(80, 4, 'Delivery address:', 0, 1) ;
			$this->Cell(80, 4, $CompanyDelivery['Name'], 0, 1) ;
			$this->Cell(80, 4, $CompanyDelivery['Address1'], 0, 1) ;
			if ($CompanyDelivery['Address2']) $this->Cell(80, 4, $CompanyDelivery['Address2'], 0, 1) ;
			$this->Cell(80, 4, sprintf ('%s %s', $CompanyDelivery['ZIP'], $CompanyDelivery['City']), 0, 1) ;
			$this->Cell(80, 4, $CompanyDelivery['CountryDescription'], 0, 1) ;


			// delivery and payment terms
			$this->SetMargins(134,0,0) ;
			$this->SetY ($top_margin+24) ;
			$this->SetFont('Arial','',7);
			$_headwidth = 38 ;
			$this->Cell($_headwidth, 4, 'PO Number:') ;
			$this->Cell(25, 4, $Record['Id'],0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Reference:') ;
			$this->Cell(25, 4, $Record['Reference'],0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Date:') ;
			$this->Cell(25, 4, date('Y.m.d', ($Record['Ready']>0) ? dbDateDecode($Record['ReadyDate']) : ''),0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Your Ref.:') ;
			$this->Cell(25, 4, $Record['PurchaseUserName'],0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Terms of Delivery:') ;
			$this->Cell(25, 4, $Record['DeliveryTermDescription'],0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Shipped By:',0,0) ;
			$this->Cell(25, 4, $Record['CarrierName'],0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Terms of Payment:') ;
			$this->Cell(25, 4, $Record['PaymentTermDescription'],0,1, 'R') ;
//			$this->Cell(25, 4, date('Y.m.d', $Record['ConfirmedDate']>'2002-01-01' ? dbDateDecode($Record['ConfirmedDate']) : ( $Record['RequestedDate']>'2002-01-01' ? dbDateDecode($Record['RequestedDate']): '')),0,1, 'R') ;
//			$this->Cell($_headwidth, 4, 'Supplier Number:') ;
//			$this->Cell(25, 4, $Company['Number'],0,1, 'R') ;
			$this->Cell($_headwidth, 4, 'Page') ;
			$this->Cell(28, 4, $this->PageNo().' of {nb}', 0, 1, 'R') ;

		  // Order Invoice Header
		  if ($Record['RequisitionHeader'] <> '' ) {
			 $this->SetMargins($_l_margin,0,0) ;
			 $this->Ln () ;

			 $this->SetFont('Arial','',7);
			 $this->MultiCell (186, 4, $Record['RequisitionHeader']) ;
			 $this->Ln (2) ;
			 $Record['RequisitionHeader']='' ;
		  } else {
			 $this->SetMargins($_l_margin,0,0) ;
			 $this->Ln () ;
		  }
	//      $this->SetMargins(71,0,0) ;
			$headlineY=$top_margin+55;
//			$this->SetY ($headlineY) ;
//			$this->SetMargins($_l_margin,0,0) ;

//			$this->Ln () ;
			$this->SetFont('Arial','',7);
	 //   	$this->Cell(8, 4, 'Pos ') ;
//			$this->Cell(15, 4, 'Image') ;
			$this->Cell(57, 4, 'Article') ;
			$this->Cell(37, 4, 'Colour') ;
			$this->Cell(18, 4, 'Quantity', 0, 0, 'R') ;
			$this->Cell(18, 4, 'Delivery', 0, 0, 'R') ;
			$this->Cell(20, 4, 'Unit Price', 0, 0, 'R') ;
			$this->Cell(12, 4, 'Discount', 0, 0, 'R') ;
			$this->Cell(20, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
			$headlineY=	$this->GetY() ;
			$this->Line($_l_margin,$headlineY-1,214-$_l_margin,$headlineY-1) ;
			$this->Line($_l_margin,$headlineY+4,214-$_l_margin,$headlineY+4) ;

			$this->SetFont('Arial','',7);
			$this->SetMargins($_l_margin,118,6) ;
			$this->Ln () ;
			$this->SetY ($headlineY+10) ;
			$this->SetAutoPageBreak(true, 10) ;
			$this->SetFillColor(230,230,230) ;
			$CompanyMy['FirstRecord'] = true ;
		}

		function Footer () {
			global $Record, $Total, $LastPage, $Company, $CompanyMy, $_l_margin, $top_margin, $bottom_margin ;

			$this->SetFont('Arial','',7);
	/*
			if (!$CompanyMy['LastPage']) {
				$this->SetXY ($_l_margin+103, 280-$bottom_margin) ;
				$this->Line($_l_margin,280-$bottom_margin,214-$_l_margin,280-$bottom_margin) ;
				$this->Cell(75, 4, 'Subtotal continued, next page ... ', 0, 0, 'R') ;
				$this->SetXY ($_l_margin+103, 285-$bottom_margin) ;
				$this->SetFont('Arial','',9);
				$this->Cell(77, 4,number_format ($CompanyMy['Total']['Quantity'], 0, ',', '.') .  ' Pcs., ' . $Record['CurrencyName'] . ' ' . number_format ($Total['Amount'], 2, ',', '.'), 0, 0, 'R') ;
			} else {

	//	    if ($CompanyMy['LastPage']) {
				// Summary
				$this->RequireSpace (7) ;
				$this->SetXY (127, 245-$bottom_margin) ;
				$this->Line($_l_margin,$this->GetY(),214-$_l_margin,$this->GetY()) ; $this->Ln () ;
				$this->Cell($_l_margin+138, 4, 'Quantity:',0,0,'R') ;
				$this->Cell(28, 4, number_format ($CompanyMy['Total']['Quantity'], 0, ',', '.') . ' Pcs',0,1,'R') ;
				$this->Cell($_l_margin+138, 4, 'Subtotal:',0,0,'R') ;
				$this->Cell(28, 4, number_format ($CompanyMy['Total']['Amount'], 2, ',', '.') ,0,1,'R') ; // . ' ' . $Record['CurrencyName']
				$this->Cell($_l_margin+138, 4, number_format ($Company['VATPercentage'], 2, ',', '.') . '% VAT:',0,0,'R') ;
				$_vat = ($CompanyMy['Total']['Amount']*$Company['VATPercentage'])/100 ;
				$this->Cell(28, 4, number_format ($_vat, 2, ',', '.') ,0,1,'R') ; // . ' ' . $Record['CurrencyName']
				$this->SetFont('Arial','B',7);
				$this->Cell($_l_margin+138, 4, 'Total amount '. $Record['CurrencyName'] . ':',0,0,'R') ;
				$this->SetFont('Arial','',7);
				$this->Cell(28, 4, number_format ($CompanyMy['Total']['Amount']+$_vat, 2, ',', '.') ,0,1,'R') ;
				$this->Ln(2) ;
				$this->Line($_l_margin+130,$this->GetY()+2,214-$_l_margin,$this->GetY()+2) ;
				$this->Ln(2) ;
				$this->Cell(8, 4, '') ;
				$this->Cell(100, 4, 'Terms of Payment: ' . $Record['PaymentTermDescription'],0,1, 'L') ;
				//Insert Country InvoiceFooter
				if ($Company['CountryInvoiceFooter'] != '') {
					$this->SetFont('Arial','',7);
					$this->Cell(8, 4, '') ;
					$this->MultiCell (186, 4, $Company['CountryInvoiceFooter']) ;
					$this->SetFont('Arial','',7);
				}
				$this->Cell(8, 4, '') ;
				$this->Cell(160, 4, 'When paying by bank transfer, please state order number: ' . $Record['Id'],0,1, 'L') ;
			}
	*/
			$this->Line($_l_margin,293-$bottom_margin,214-$_l_margin,293-$bottom_margin) ;

			$this->SetXY (100, 292-$bottom_margin) ;

			$this->SetFont('Arial','',7);
			$this->Ln () ;
			$this->Cell(190, 4,
					$CompanyMy['Name'] .  '  -  ' .
					$CompanyMy['Address1'] .
					sprintf ('  -  %s', $CompanyMy['ZIP']) .
					sprintf ('  -  %s', $CompanyMy['City']) .
					sprintf ('  -  %s', $CompanyMy['CountryDescription']) .
					sprintf ('  -  %s', $CompanyMy['PhoneMain']) .
					sprintf ('  -  %s', $CompanyMy['Web']) .
					sprintf ('  -  VAT No %s', $CompanyMy['RegNumber']),
					 0, 0, 'C') ;
		}

		function RequireSpace ($space) {
			if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
		}

		function TruncString ($s, $w) {
			// Truncate string to specified width
			$s = (string)$s ;
			$w *= 1000/$this->FontSize ;
			$cw = &$this->CurrentFont['cw'] ;
			$l = strlen ($s) ;
			for ($i = 0 ; $i < $l ; $i++) {
			$w -= $cw[$s{$i}] ;
			if ($w < 0) break ;
			}
			return substr($s, 0, $i) ;
		}
    }

	function printorder() {
		global $Line, $Record, $Company, $CompanyMy, $CompanyDelivery, $Config;
		global $_l_margin, $top_margin, $bottom_margin;
//return 'test' ;
		// Use previously generated pdf-file ?
		if ($Record['Ready'] and $ProformaInvoice==0 and $ProformaCustoms==0) {
			$file = fileName ('purchase', (int)$Record['Id']) ;
			if (is_file($file)) {
				if ($Config['savePdfToFile'] !== true) {
					$pdfdoc = file_get_contents($file) ;
						httpNoCache ('pdf') ;
					httpContent ('application/pdf', sprintf('Order%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
					print ($pdfdoc) ;
				}
				return 0 ;
			}
		}

		// Variables
		$CompanyMy['Total'] = array () ;
		$CustomsPosition = array () ;
		$ProformaCustomsFactor = 0.1 ;
		$CompanyMy['LastPage'] = false ;
		$CompanyMy['FirstRecord'] = true ;
		$Now = time () ;

		// Get this orders Company information
		$query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription 
							FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['FromCompanyId']) ;
		$result = dbQuery ($query) ;
		$CompanyMy = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$CompanyMy['Id'] <= 0) return 'CompanyMy not found Id: ' . (int)$Record['FromCompanyId']. '(' . (int)$Record['CompanyId'] . ')';

		// Get this orders Delivery Company information
		$query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription 
							FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['DeliveryId']) ;
		$result = dbQuery ($query) ;
		$CompanyDelivery = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$CompanyDelivery['Id'] <= 0) return 'CompanyDelivery not found Id: ' . (int)$Record['DeliveryId']. '(' . (int)$Record['CompanyId'] . ')';

		// Insert tripple-space in name
		if ($CompanyMy['Name'] != '') {
		$s = $CompanyMy['Name']{0} ;
		$n = 1 ;
		while ($CompanyMy['Name']{$n}) $s .= '  ' . $CompanyMy['Name']{$n++} ;
		$CompanyMy['NameExpanded'] = strtoupper($s) ;
		}

		// Get Full Company information
		// Insert Country invoiceFooter
		$query = sprintf ('SELECT Company.*, Country.Id as CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage, 
							FooterCountry.InvoiceFooter AS CountryInvoiceFooter FROM Company 
							LEFT JOIN Country  ON Country.Id=Company.CountryId 
							LEFT JOIN Country FooterCountry ON FooterCountry.Id=1 
							WHERE Company.Id=%d', (int)$Record['CompanyId']) ;
							//  
		//						WHERE Company.Id=%d and PaymentInformation.FromCompanyId=%d', (int)$Record['CompanyId'], (int)$Record['FromCompanyId']) ;
		$result = dbQuery ($query) ;
		$Company = dbFetch ($result) ;
		dbQueryFree ($result) ;
		/*
		if ($Company['CountryInvoiceFooter'] == '') {
			$query = sprintf ('SELECT Company.*, Country.Id as CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage, PaymentInformation.Description AS CountryInvoiceFooter FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId LEFT JOIN PaymentInformation ON PaymentInformation.CountryId=Country.Id WHERE Company.Id=%d and PaymentInformation.FromCompanyId=%d', (int)$Record['CompanyId'], (int)$Record['FromCompanyId']) ;
			$result = dbQuery ($query) ;
			$Company = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ($Company['CountryInvoiceFooter'] == '')
				$query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage,PaymentInformation.Description AS CountryInvoiceFooter FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId LEFT JOIN PaymentInformation ON PaymentInformation.CountryId=9999 WHERE Company.Id=%d and PaymentInformation.FromCompanyId=%d', (int)$Record['CompanyId'], (int)$Record['FromCompanyId']) ;
			$result = dbQuery ($query) ;
			$Company = dbFetch ($result) ;
			dbQueryFree ($result) ;
		}
		*/
		// Get Country information for Record
		$Record['CountryDescription'] = tableGetField ('Country', 'Description', (int)$Record['CountryId']) ;
		$Record['CarrierName'] = tableGetField ('Carrier', 'Name', (int)$Record['CarrierId']) ;
		$Record['DeliveryTermDescription'] = tableGetField ('DeliveryTerm', 'Description', (int)$Record['DeliveryTermId']) ;
		$Record['PaymentTermDescription'] = tableGetField ('PaymentTerm', 'Description', (int)$Record['PaymentId']) ;
		$Record['CurrencyName'] = tableGetField ('Currency', 'Name', (int)$Record['CurrencyId']) ;
		$Record['PurchaseUserName'] = tableGetField ('User', 'CONCAT(FirstName," ",LastName)', (int)$Record['PurchaseUserId']) ;


			// Loop lines to do calculations
		if ($ProformaCustoms==1) {
		$CustomsPosition[0][''] = array ('Id' => 0, 'Name' => 'None', 'CustomsPositionDesc' => 'None', 'Amount' => 0, 'Quantity' => 0) ;
			foreach ($Line as $id => $line) {
				if ($line['CustomsPos'] == '99999999') continue ;
				// Set Unit for Total
				if ($line['No'] == 1) {
					$CompanyMy['Total']['UnitName'] = $line['UnitName'] ;
					$CompanyMy['Total']['UnitDecimals'] = (int)$line['UnitDecimals'] ;
				} else {
					if ($CompanyMy['Total']['UnitName'] != $line['UnitName']) $CompanyMy['Total']['UnitName'] = '' ;
				}

				$quantity = $line['Quantity'] ;
				$amount = $quantity * $line['PriceSale'] ;
				if ((int)$line['Discount'] > 0) $amount -= $amount * (int)$line['Discount'] / 100 ;

				// Save Totals
				$CompanyMy['Total']['Amount'] += $amount * $ProformaCustomsFactor;
				$CompanyMy['Total']['Quantity'] += $quantity ;

				// Handle Customs Positions
				$j = (int)$line['CustomsPositionId'] ;
				$o = $line['WorkCountryId'] ;
				if (!isset($CustomsPosition[$j][$o])) {
					$CustomsPosition[$j][$o] = array ('Id' => $j, 'Name' => $line['CustomsPos'], 'CustomsPositionDesc' => $line['CustomsPositionDesc'], 'Amount' => 0, 'Quantity' => 0, 'Origin' => $line['WorkCountryName']) ;
				}
				$CustomsPosition[$j][$o]['Quantity'] += $quantity ;
				$CustomsPosition[$j][$o]['Amount'] += $amount ;
				$CustomsPosition[$j][$o]['UnitName'] = $line['UnitName'] ;
				$CustomsPosition[$j][$o]['UnitDecimals'] = (int)$line['UnitDecimals'] ;
			}
		}

		$FirstPage = 1 ;
		// Make PDF
		$pdf=new PDF('P', 'mm', 'A4') ;
		$pdf->AliasNbPages() ;
		$pdf->SetAutoPageBreak(true, 10) ;
		//    $pdf->AddPage() ;

		if ($ProformaCustoms==0) {

		// init LOTs
		$LOT['new'] = 1 ;
		$LOT['Date'] = '0001-01-01' ;

		foreach ($Line as $line) {
		// Delivery time
		if ($LOT['Date'] != $line['DeliveryDate'] or $LOT['new'] == 1) {
			$LOT['new'] = 1 ;
			$LOT['Date'] = $line['DeliveryDate'] ;
			$pdf->AddPage() ;
			$LOT['new'] = 0 ;
		}
		// Set Unit for Total
		if ($line['No'] == 1) {
			$CompanyMy['Total']['UnitName'] = $line['UnitName'] ;
			$CompanyMy['Total']['UnitDecimals'] = (int)$line['UnitDecimals'] ;
			$CompanyMy['Total']['UnitName'] = $line['No'] ;
		} else {
			if ($CompanyMy['Total']['UnitName'] != $line['UnitName']) $CompanyMy['Total']['UnitName'] = '' ;
		}
		//
		$FirstPage = 0 ;

		// Compute required space for the Position
		$space = 8 + $line['NoGroupLines']*2; // 14 ;
		if ((int)$line['CaseId'] > 0) $space += ((int)$line['ArticleCertificateId'] > 0) ? 8 : 4 ;
		if ($line['VariantSize']) $space += (int)((((count($line['Size'])+5)/9)*10)+2) ;
		if ($line['InvoiceFooter']) $space += 10 ;
		//	if ($Record['VariantCodes'] and $line['VariantSize']) $space += (int)count($line['Size']) + 2;
		$pdf->RequireSpace ($space) ;

		//	$pdf->Cell(8, 4, $line['NoGroup']) ;
		$pdf->SetFont('Arial','B',7);
		if ($CompanyMy['FirstRecord'] == false) {
			if ($Record['VariantCodes']) {
				$pdf->SetXY ($pdf->GetX(), $ean_pos) ;
			} else {
				$pdf->Ln (4) ;
			}
			$pdf->Line($_l_margin,$pdf->GetY(),193-$_l_margin,$pdf->GetY()) ;
			$pdf->Ln (2) ;
		} 
			
		//Generating image for article
/*		$article_image_link = sprintf("%s\\thumbnails\\%s_%s.jpg", $Config['imageStore'], $line['ArticleNumber'], $line['ColorNumber']);
		if (file_exists($article_image_link)) {
			$pdf->Image($article_image_link, $pdf->GetX(), $pdf->GetY(), 15, 15);
		}
		$pdf->Cell(15, 15, '');
*/
		$pdf->SetFont('Arial','B',7);

		$pdf->Cell(57, 4, $pdf->TruncString($line['Description'], 55)) ; //.' ('.$line['ArticleNumber'].')') ;
		$pdf->SetFont('Arial','',7);

		// Color
		if ($line['VariantColor']) {
			if ((int)$line['ArticleColorId'] > 0) {
			$pdf->SetFont('Arial','B',7);
			$pdf->Cell(40, 4, $pdf->TruncString($line['ColorDescription'], 30)); //.' ('.$line['ColorNumber'].')') ;
			$pdf->SetFont('Arial','',7);
			} else {
			$pdf->Cell(40, 4, '-') ;
			}
		} else
			$pdf->Cell(40, 4, '') ;

		// Prices
		if ($ProformaCustoms==1) $line['PriceSale']=$line['PriceSale']*$ProformaCustomsFactor ;
		$pdf->Cell(17, 4, number_format((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', '.') . ' ' . $line['UnitName'], 0, 0, 'R') ;
		$CompanyMy['Total']['Quantity'] += $line['Quantity'] ;
		$pdf->Cell(17, 4,  date('Y.m.d',dbDateDecode($line['RequestedDate'])), 0, 0, 'R') ;
		$pdf->Cell(15, 4, '', 0, 0, 'R') ;
		$pdf->Cell(15, 4, ((int)$line['Discount'] > 0) ? ($line['Discount'] . ' %') : '', 0, 0, 'R') ;
		$v = $line['PriceSaleSubTotal'] ;
//		if ((int)$line['Discount'] > 0) $v -= $v * (int)$line['Discount'] / 100 ;
		$CompanyMy['Total']['Amount'] += $v ;
		$pdf->Cell(22, 4, number_format ($v, 2, ',', '.'), 0, 0, 'R') ;
		$pdf->Ln () ;

		if ((int)$line['CaseId'] > 0) {
			$pdf->Cell(15, 4, 'Case') ;
			$pdf->Cell(26, 4, $line['CaseId']) ;
			if ($line['CustomerReference'] != '') {
			$pdf->Cell(100, 4, 'Customer reference: ' . $line['CustomerReference']) ;
			}
			$pdf->Ln () ;
		}

		// Article
		// CustomsPosition
		if ($ProformaCustoms==1) {
			$pdf->Cell(27, 4, 'Customs Posit�on:') ;
			$pdf->Cell(18, 4, $line['CustomsPos']) ;
			$pdf->Cell(12, 4, 'Origin:') ;
			$pdf->Cell(15, 4, $line['WorkCountryName']) ;
			$pdf->Ln () ;
		}

		// Certificate
		if ((int)$line['ArticleCertificateId'] > 0) {
			$pdf->Cell(15, 4, 'Certificate') ;
			$pdf->Cell(70, 4, tableGetFieldWhere (
			'ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId',
			'CertificateType.Name',
			sprintf ('ArticleCertificate.Id=%d', (int)$line['ArticleCertificateId'])
			)) ;
			$pdf->Ln () ;
		}

		// Sizes
		if ($line['VariantSize']) {
			$n = 0 ; $_sublineprice = array () ;
			$_lineno = 0 ; $_subline = 0 ; $_headlineYpos = 0 ; $_ean = 0 ;
			foreach ($line['Size'] as $size) {
				if (!($_lineno==$size['OrderLineNo'])) {
					$_subline++ ;
					$_sublineprice[$_subline]['price'] = $size['PurchasePrice'] ;
					$_sublineprice[$_subline]['actualprice'] = $size['ActualPrice'] ;
					$_sublineprice[$_subline]['discount'] = $size['Discount'] ;
					$_lineno=$size['OrderLineNo'] ;
				}
		//		if ($size['Quantity']==0) {
		//			continue;
		//		}
				if ($Record['VariantCodes'] and $line['VariantSize']) {
					$SizeSpace['NoColomns'] = 9 ; // 7
					$SizeSpace['NoLines'] = 8 ;
					$SizeSpace['Colomn'] = 16 ;
				} else {
					$SizeSpace['NoColomns'] = 12 ;
					$SizeSpace['NoLines'] = 4 ;
					$SizeSpace['Colomn'] = 12 ;
				}
				if (($n % $SizeSpace['NoColomns']) == 0) {
					if ($n > 0) $pdf->Ln () ;
					$pdf->Ln (2) ;
		//				$pdf->RequireSpace($SizeSpace['NoLines']) ;
					$pdf->Cell(23, 4, '') ;
					$pdf->Cell(15, 4, '') ;
					$pdf->Ln () ;
//					$pdf->Cell(8, 4, '') ;
//					$pdf->Cell(15, 4, '') ;
					if ($Record['VariantCodes'] and $line['VariantSize']) {
						$pdf->ln(4*($line['NoGroupLines'])) ;
						$ean_pos = $pdf->GetY()+4*$line['NoGroupLines'] ;
		//					$pdf->SetXY ($pdf->GetX(), $pdf->GetY()+4*($line['NoGroupLines'])) ;
		//					$pdf->Cell(8, 4, '') ;
						$pdf->Cell(15, 4, 'EAN codes') ;
						$_ean = 4 * ($line['NoGroupLines']-1) ;
					}
				}

				if ($_headlineYpos==0) $_headlineYpos = $pdf->GetY()-$SizeSpace['NoLines'] - $_ean; //-4*($line['NoGroupLines']-1)
				$pdf->SetXY ($pdf->GetX(), $_headlineYpos) ;
				$pdf->Cell($SizeSpace['Colomn'], 4, $size['Name']) ;
				$pdf->SetXY ($pdf->GetX()-$SizeSpace['Colomn'], $pdf->GetY()+(4*$_subline)) ;
				$pdf->Cell($SizeSpace['Colomn'], 4, number_format ((float)$size['Quantity'], (int)$line['UnitDecimals'], ',', '.')) ;
				if ($Record['VariantCodes'] and $line['VariantSize']) {
					$pdf->SetXY ($pdf->GetX()-$SizeSpace['Colomn'], $pdf->GetY()+4*($line['NoGroupLines']-$_subline+1)) ;
					$pdf->SetFont('Arial','',5);
					$pdf->Cell($SizeSpace['Colomn'], 4, $size['VariantCode']) ;
					$pdf->SetFont('Arial','',7);
				}
				$n++ ;
			}
			$pdf->SetXY (160, $_headlineYpos) ;
			$pdf->Cell($SizeSpace['Colomn'], 4, '') ;
			$t = 4;
			foreach ($_sublineprice as $_price) {
				$pdf->SetXY ($_l_margin+138, $pdf->GetY()+$t) ;
				$pdf->Cell($SizeSpace['Colomn'], 4, number_format ((float)$_price['price'], 2, ',', '.'),0,0,'R') ;
				if ((int)$_price['discount']>0)
					$pdf->Cell($SizeSpace['Colomn'], 4, number_format ((int)$_price['discount'], 0, ',', '.') . '%',0,0,'R') ;
			}
		//        $pdf->Ln (4*$_subline) ;
			$pdf->Ln () ;
			$CompanyMy['FirstRecord'] = false ;
		}
		// OrderLine Invoice Footer
		if ($line['InvoiceFooter']) {
			$pdf->Ln (2) ;
			$pdf->MultiCell (186, 4, $line['InvoiceFooter']) ;
		}
		/*
		$pdf->Ln (2+$_ean) ;
		$pdf->Line($_l_margin+16,$pdf->GetY(),193-$_l_margin,$pdf->GetY()) ;
		$pdf->Ln (2) ;
		*/
		}

		// Order Invoice Footer
		if ($Record['RequisitionFooter']) {
			$pdf->Ln (2) ;
			$pdf->MultiCell (186, 4, $Record['RequisitionFooter']) ;
		}

		} // END Normal not Proforma customs!!!

		if ($ProformaInvoice==1) {
			// VAT
			if ((int)$Company['VATPercentage'] > 0) {
				$pdf->RequireSpace (10) ;
				$v = $CompanyMy['Total']['Amount'] * $Company['VATPercentage'] / 100 ;
				$CompanyMy['Total']['Amount'] += $v ;
				$TCompanyMy['Total']['CarryOver'] += $v ;
				$pdf->Cell(188, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
				$pdf->Ln () ;
				$pdf->Cell(100, 4, sprintf ('VAT %s %%', $Company['VATPercentage'])) ;
				$pdf->Cell(88, 4, number_format ($v, 2, ',', '.'), 0, 0, 'R') ;
				$pdf->Ln () ;
				$pdf->Ln (4) ;
			}
			// VAT info
			$pdf->RequireSpace (10);
			if ((int)$Company['EUMember'] && ($Company['CountryName'] !== 'DK')) {
			  //$pdf->RequireSpace (10) ;
				$pdf->Cell(20, 4, 'Customer') ;
				$pdf->Cell(80, 4, 'VAT No.: '. $Company['VATNumber']) ;
				$pdf->Ln () ;
			} //20060602 jle always show Novotex VAT No
			$pdf->Cell(20, 4, $CompanyMy['Name'] . ' VAT No.: '. $CompanyMy['VATNumber']) ;
			$pdf->Ln () ;
			$pdf->Ln (4) ;
		}

		// CustomsPositions
		if ($ProformaCustoms==1) {
			$pdf->RequireSpace (16) ;
			$pdf->Cell (80, 4, 'Customs information') ;
				$pdf->Ln () ;
			foreach ($CustomsPosition as $position) {
				foreach ($position as $pos) {
					if ((int)$pos['Quantity'] == 0) continue ;
					if ((int)$pos['Name'] == '99999999') continue ;
					$pdf->Cell (10, 4, 'Pos') ;
					$pdf->Cell (20, 4, $pos['Name'], 0, 0, 'R') ;
					$pdf->Cell (58, 4, $pos['CustomsPositionDesc'], 0, 0) ;
					$pdf->Cell (12, 4, $pos['Origin'], 0, 0) ;
					$pdf->Cell (10, 4, 'Qty', 0, 0) ;
					$pdf->Cell (25, 4, number_format ((float)$pos['Quantity'], $pos['UnitDecimals'], ',', '.') . ' ' . $pos['UnitName'], 0, 0, 'R') ;
					$pdf->Cell (30, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
					$pdf->Cell (20, 4, number_format ((float)$pos['Amount']*$ProformaCustomsFactor, 2, ',', '.'), 0, 0, 'R') ;
					$pdf->Ln () ;
				}
			}
			$pdf->Ln(4) ;
		}

		// Summary on last page
		$pdf->RequireSpace (20) ;
		$pdf->SetXY (127, 275-$bottom_margin) ;
		$pdf->Line($_l_margin,$pdf->GetY()+2,214-$_l_margin,$pdf->GetY()+2) ; $pdf->Ln () ;
		$pdf->Cell($_l_margin+138, 4, 'Quantity:',0,0,'R') ;
		$pdf->Cell(28, 4, number_format ($CompanyMy['Total']['Quantity'], 0, ',', '.') . ' Pcs',0,1,'R') ;
		
/*
		$pdf->Cell($_l_margin+138, 4, 'Subtotal:',0,0,'R') ;
		$pdf->Cell(28, 4, number_format ($CompanyMy['Total']['Amount'], 2, ',', '.') ,0,1,'R') ; // . ' ' . $Record['CurrencyName']
		$pdf->Cell(8, 4, '') ;
//		$pdf->Cell(60, 4, 'Terms of Payment: ' . $Record['PaymentTermDescription'],0,0, 'L') ;
		$pdf->Cell(60, 4, '',0,0, 'L') ;
		$pdf->Cell($_l_margin+70, 4, number_format ($Company['VATPercentage'], 2, ',', '.') . '% VAT:',0,0,'R') ;
		$_vat = ($CompanyMy['Total']['Amount']*$Company['VATPercentage'])/100 ;
		$pdf->Cell(28, 4, number_format ($_vat, 2, ',', '.') ,0,1,'R') ; // . ' ' . $Record['CurrencyName']
		$pdf->SetFont('Arial','B',7);
*/
		$pdf->Cell(8, 4, '') ;
		$pdf->Cell(60, 4, '',0,0, 'L') ;
		$pdf->Cell($_l_margin+70, 4, 'Total amount '. $Record['CurrencyName'] . ':',0,0,'R') ;
		$pdf->SetFont('Arial','',7);
		$pdf->Cell(28, 4, number_format ($CompanyMy['Total']['Amount']+$_vat, 2, ',', '.') ,0,1,'R') ;
/*
		//	$pdf->Ln(2) ;
		$pdf->Line($_l_margin+130,$pdf->GetY()+2,214-$_l_margin,$pdf->GetY()+2) ;
		//	$pdf->Ln(2) ;
		//Insert Country InvoiceFooter
		if ($Company['CountryInvoiceFooter'] != '') {
			$pdf->SetFont('Arial','',7);
			$pdf->Cell(8, 4, '') ;
			$pdf->MultiCell (186, 4, $Company['CountryInvoiceFooter']) ;
			$pdf->SetFont('Arial','',7);
		}
*/
		// Last Page generated
		$CompanyMy['LastPage'] = True ;
		//	$pdf->Cell (10, 4, 'test '.$WHLastPage, 0, 0) ;

		// Generate PDF document
		$pdfdoc = $pdf->Output('', 'S') ;

		// Download
		if ($Config['savePdfToFile'] !== true) {
			if (headers_sent()) return 'stop' ;
			httpNoCache ('pdf') ;
			httpContent ('application/pdf', sprintf('Order%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
			print ($pdfdoc) ;
		}

		// Save file
		if (($Record['Ready'] and $ProformaInvoice==0 and $ProformaCustoms==0) or $Config['savePdfToFile'] == true){
			$file = fileName ('purchase', (int)$Record['Id'], true) ;
			$resource = fopen ($file, 'w') ;
			fwrite ($resource, $pdfdoc) ;
			fclose ($resource) ;
			chmod ($file, 0640) ;
		}
//		return 'her' ;	 ;

		return 0 ;
	}
?>
