<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/html.inc' ;

	// Get lines
	$Lines = array () ;
	$_currencysymbol = tableGetField('Currency','Symbol', $Record['CurrencyId']) ;
	$query = sprintf( 
			"SELECT rl.Id as Id, rl.No as No, a.Number as ArticleNumber, a.Description as ArticleDescription, c.number as ColorNumber,
					rl.PurchasePrice as PurchasePrice, rl.ActualPrice as ActualPrice, rl.Discount as Discount,
					rl.RequestedDate as RequestedDate, rl.ConfirmedDate as ConfirmedDate, rl.AvailableDate as AvailableDate, rl.LotId as LotId, cl.Name as LotName
			  FROM (requisitionline rl, article a)
			  LEFT JOIN articlecolor ac ON ac.id=rl.articlecolorid
			  LEFT JOIN color c ON c.id=ac.colorid
			  LEFT JOIN collectionLot cl ON cl.id=rl.LotId
			  WHERE a.Id=rl.articleid
					and rl.RequisitionId=%d and rl.Active=1 
			  ORDER BY No", $Id) ;
	$res = dbQuery ($query) ;

	while ($row = dbFetch($res)) {
		$Lines[$row['No']] = $row ;
	}
	dbQueryFree ($res) ;
	unset($row) ;

    formStart () ;

    itemStart () ;
    itemHeader () ;
//    printf ("<tr><td class=itemlabel>Lot</td><td>%s</td></tr>\n", htmlDBSelect ("DelayreasonId style='width:200px'", $Record['LotId'], 'SELECT Id, Name AS Value FROM Delayreason WHERE Active=1 ORDER BY Name')) ;  
/* 	itemFieldRaw ('LOT', formDBSelect ('LotId', (int)0, 
								sprintf ('SELECT CollectionLot.Id, CollectionLot.Name AS Value 
										FROM CollectionLot
										WHERE SeasonId=%d AND Active=1 
										ORDER BY CollectionLot.`Date`', $Record['SeasonId']), 'width:200px')) ; 

*/    itemSpace () ;
	if ($Record['Ready']==0) {
		itemFieldRaw ('Requested', formDate ('RequestedDate',dbDateDecode($Record['RequestedDate']))) ;
		itemSpace () ;
	}
	itemFieldRaw ('Confirmed', formDate ('ConfirmedDate',dbDateDecode($Record['ConfirmedDate']))) ;
    itemSpace () ;
    itemFieldRaw ('Available', formDate ('AvailableDate',dbDateDecode($Record['AvailableDate']))) ;
    itemSpace () ;
	if ($Record['Ready']==0) {
		itemFieldRaw ('Price', formText ('PurchasePrice','', 12, 'text-align:right;') . ' ' . $Record['CurrencySymbol']) ;
	    itemSpace () ;
	    itemFieldRaw ('Discount', formText ('Discount','', 2, 'text-align:right;') . ' %') ;
	    itemSpace () ;
	}
    itemEnd () ;

    listStart () ;
    listRow () ;
    listHead ('Select', 8) ;
    listHead ('No', 6) ;
    listHead ('Article', 20) ;
    listHead ('Description', 60) ;
    listHead ('Color', 20) ;
	listHead ('Price', 20, 'align=right') ;
	listHead ('Discount', 20, 'align=right') ;
	listHead ('ActualPrice', 20, 'align=right') ;
//  listHead ('LOT', 30) ;
    listHead ('Requested', 25) ;
	listHead ('Confirmed', 25) ;
    listHead ('Available', 25) ;
    listHead ('', 1) ;

   foreach ($Lines as $lineno => $row) {
		listRow () ;
		listFieldRaw (formCheckbox (sprintf('SelectFlag[%d]', (int)$row['Id']), 0, '')) ;
		listField ($row['No']) ;
		listField ($row['ArticleNumber']) ;
		listField ($row['ArticleDescription']) ;
		listField ($row['ColorNumber']) ;
//		listField ($row['LotName']) ;
		listField (number_format((float)$row['PurchasePrice'], 2, ',', '.') . ' '. $_currencysymbol, 'align=right') ;
		$t = ($row['Discount']) ;
		listField (($t > 0) ? $t . ' %' : '', 'align=right') ;
		listField (number_format((float)$row['ActualPrice'], 2, ',', '.') . ' '. $_currencysymbol, 'align=right') ;
		$t = dbDateDecode($row['RequestedDate']) ;
		listField (($t > 978307200) ? date ('Y-m-d', $t) : '') ;
		$t = dbDateDecode($row['ConfirmedDate']) ;
		listField (($t > 978307200) ? date ('Y-m-d', $t) : '') ;
		$t = dbDateDecode($row['AvailableDate']) ;
		listField (($t > 978307200) ? date ('Y-m-d', $t) : '') ;
//		listFieldRaw (formHidden(sprintf('Lines[%d]', (int)$row['Id']), $row['Id'])) ;
    }
    listEnd () ;
    formEnd () ;

?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
function UnCheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = false ;
    }
}
</script>
<?php
    
    return 0 ;
?>
