<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 10)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Decimals', formText ('Decimals', $Record['Decimals'], 1)) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
