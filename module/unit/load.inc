<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT Unit.* FROM Unit WHERE Unit.Active=1 ORDER BY Unit.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query Unit
	    $query = sprintf ('SELECT Unit.* FROM Unit WHERE Unit.Id=%d AND Unit.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
