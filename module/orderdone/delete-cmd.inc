<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ('SELECT Id FROM `Order` WHERE OrderDoneId=%d AND Active=1', $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ('the OrderDone condition is in use') ;

    // Do deleting
    tableDelete ('OrderDone', $Id) ;

    return 0
?>
