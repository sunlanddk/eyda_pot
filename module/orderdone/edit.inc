<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 20)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%')) ;
    itemInfo ($Record) ;
    itemEnd () ;    
    formEnd () ;
    
    return 0 ;
?>
