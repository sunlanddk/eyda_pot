<?php
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Project', $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemHeader () ;
    itemFieldRaw ('Handler', formDBSelect ('HandlerId',  $Record['OwnerId'], 
		"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company WHERE Internal=1 AND Active=1 AND listhidden=0 ORDER BY Name", 'width:250px;')) ;  
//    itemFieldRaw ('Handler', formDBSelect ('HandlerId', 2, 
//											"SELECT Id, CONCAT(Company.Name,', ',Company.Number) AS Value FROM Company WHERE Internal=1 AND Active=1 AND listhidden=0 ORDER BY Name", 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Add to Instructions of first order per season per customer', formtextArea ('Instructions', $Record['InitialPickInstructions'], 'width:100%;height:100px;')) ;
    itemSpace () ;
    itemFieldRaw ('Include deliveries until', formDate  ('MaxDeliveryDate', '')) ;
    itemSpace () ;
//    itemFieldRaw ('Only Create File to Alpi', formCheckbox ('OnlyFile', 0)) ;
    itemSpace () ;
    itemEnd () ;

    listStart () ;
    listRow () ;
    listHead ('LOT', 70) ;
    listHead ('Planned Date', 90) ;
    listHead ('Generated until', 90) ;

	$query = sprintf ('Select Name, date(`Date`) as LOTDate,GeneratedPick,date(GeneratedDate) as GeneratedDate From CollectionLOT Where SeasonId=%d and active=1 Order By LOTDate', $Record['id']) ;

	$res = dbQuery ($query) ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
		listField ($row['Name']) ;
		listField ($row['LOTDate']) ;
		if ($row['GeneratedPick']) {
			listField ($row['GeneratedDate']>'2001-01-01'? $row['GeneratedDate'] : 'ALL') ;
		} else {
			listField ('') ;
		}

//		listFieldRaw (formCheckbox (sprintf('LOTFlag[%d]', (int)$row['Id']), (int)$row['GeneratedPick'], '')) ;
	}
	listEnd() ;
    formEnd () ;
    dbQueryFree ($res) ;
	
	echo '<br>' ;
    listStart () ;
    listHeader ('Orders') ;
    listRow () ;
    listHead ('Cons Order #', 50) ;
    listHead ('Customer', 90) ;
 //   listHead ('Email', 90) ;
    listHead ('', 90) ;

	$query = sprintf("select o.ConsolidatedId, c.Name as CompanyName from (`order` o, paymentterm pt, company c)
							where o.done=0 and o.active=1 and o.seasonid=%d and pt.id=o.paymenttermid and not pt.prepay>0 and c.id=o.companyid and c.IncludeBatchPick=1
							group by o.ConsolidatedId
					", $Id) ;
	$res = dbQuery ($query) ;
	$_consolidated = dbFetch ($res) ;
	while ($row = dbFetch ($res)) {
		if ($row['CompanyName']=='Privat') continue ;
		$Lines[$row['ConsolidatedId']] = $row ;
	}
    dbQueryFree ($res) ;
	
	foreach($Lines as $orderid => $line) {
		listRow () ;
		listField($line['ConsolidatedId']) ;
		listField($line['CompanyName']) ;
//		listField($line['Email']) ;
		listField('') ;
//		echo 'Order #' . $line['Id'] . ' to Customer: ' . $line['CompanyName'] . ' Email: ' . $line['Email'] . '<br>';
	}
	ListEnd() ;

    return 0 ;
?>
