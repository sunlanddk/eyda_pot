<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/webshop.inc' ;

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id, $WebOrders ;

    switch ($Navigation['Parameters']) {
		case 'picklist' :
			$WebOrders = array () ;
			foreach ($Webshop as $key => $web_row) {
				dbReConnect($web_row['Ip'], $web_row['name']) ;

				$query = "SELECT * FROM " . $web_row['name'] . ".ws_order o, " . $web_row['name'] . ".ws_customer c where  o.status<5 and o.customerid=c.id order by status, webid" ;
				$res = dbQuery ($query) ;

				while ($row = dbFetch($res)) {
					$WebOrders[$row['WEBID']] = $row ;
					$WebOrders[$row['WEBID']]['ShopName'] = $web_row['name'] ;
					$test = $row['WEBID'] ;
//return $WebOrders[$test]['ShopName'] . ' ' . $WebOrders[$test]['WEBID'];
				}
				dbQueryFree ($res) ;
				unset($row) ;
			}
			break;
		default: // from season
			if ($Id <> 97) return 'No webshop connected to this project' .  $Navigation['Parameters'] ;
			break;
	}
	$State = array (1 => "WEB Ordered", 2 => "NA", 3 => "WEB payed", 4 => "Imported to POT", 5 => "Delivered", 6 => "Cancelled") ;


    dbReConnect('localhost', 'devel') ;

    listStart () ;
    listRow () ;
    listHead ('Webshop', 70) ;
    listHead ('Web Order Number', 100) ;
    listHead ('State', 100) ;
    listHead ('Customer', 90) ;
    listHead ('City', 70) ;
    listHead ('Amount', 90) ;
    listHead ('POT Order Number', 70) ;
    listHeadIcon () ;
    listHead ('PickOrder') ;

    foreach ($WebOrders as $webid => $row) {
		listRow () ;
		listField ($row['ShopName']) ;
		listField ($row['WEBID']) ;
		listField ($State[$row['STATUS']]) ;
		listField ($row['LASTNAME']) ;
		listField ($row['CITY']) ;
		listField ($row['TOPAY']) ;
		if ($row['STATUS']<4) {
			listField ('') ;
			listField ('') ;
			listField ('') ;
		} else {
			$OrderId = tableGetFieldWhere ('`order`', 'Id', 'Active=1 AND Reference='. '"'.$row['WEBID'].'"') ;
//			listField($OrderId) ;
			listField($row['WEBID']) ;
			listFieldIcon ($Navigation['Icon'], 'orderview2', $OrderId) ;
			$PickOrderId = tableGetFieldWhere ('`pickorder`', 'Id', 'ReferenceID='. $OrderId) ;
			if ($PickOrderId>0)
				listField ('Yes') ;
			else
				listField ('No') ;
		}
    }
    listEnd () ;

    return 0 ;
?>
