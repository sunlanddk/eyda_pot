<?php 

$seasonId = $_GET['id'];

if(WEBSHOP_API_URL != ''){
	//// Update or add products from season to magento database.
	$potdata = json_encode(array('fromPot' => array('request' => 'delete_season','data'    => $seasonId)));
	$ch = curl_init(WEBSHOP_API_URL);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_POSTFIELDS, $potdata);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
	$result = curl_exec($ch);
	$result = json_decode($result);
	echo 'Request to delete all products in this season from webshop is sent to the webshop<br><br>You can now close the window.';
	echo '<br><br>Respons from Webshop: ' . $result ;
}
else{
	echo 'Remote endpoint not specified';	
}


return 0;


?>