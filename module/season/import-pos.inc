<?php
    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Project', $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;

    printf ("<table>\n") ;
    // Form
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

	printf ("<input type=hidden name=MAX_FILE_SIZE value=20000000>\n") ;
	printf ("<tr><td><p>File</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;  
//	 printf (" <td><input type='submit' value='Upload' /></td>\n") ;
  
    itemSpace () ;
    itemFieldRaw ('Pos. Ready', formCheckbox ('PickPosGenerated', $Record['PickPosGenerated'])) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
