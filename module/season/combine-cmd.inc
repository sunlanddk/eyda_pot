<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    
	$old_seasonid = $Id ;
	$new_seasonid = (int)$_POST['SeasonId'] ;
	if (($new_seasonid == 0) or ($old_seasonid == 0)) {
		return ' Undefined season to move from ' . $old_seasonid . ' or to ' . $new_seasonid;
    }
	if ($old_seasonid==$new_seasonid) {
		return 'No change in season selected' ;
    }
	// Update all collections
    $query = sprintf ("UPDATE Collection SET `SeasonId`=%d, `ModifyDate`='%s', `ModifyUserId`=%d WHERE SeasonId=%d", $new_seasonid, dbDateEncode(time()), $User["Id"], $old_seasonid) ;
    dbQuery ($query) ;
	// Also update programs, orders and collection LOTs
    $query = sprintf ("UPDATE Collectionlot SET `SeasonId`=%d, `ModifyDate`='%s', `ModifyUserId`=%d WHERE SeasonId=%d", $new_seasonid, dbDateEncode(time()), $User["Id"], $old_seasonid) ;
    dbQuery ($query) ;
    $query = sprintf ("UPDATE `order` SET `SeasonId`=%d, `ModifyDate`='%s', `ModifyUserId`=%d WHERE SeasonId=%d", $new_seasonid, dbDateEncode(time()), $User["Id"], $old_seasonid) ;
    dbQuery ($query) ;
	
    $query = sprintf ("UPDATE `program` SET `SeasonId`=%d, `ModifyDate`='%s', `ModifyUserId`=%d WHERE SeasonId=%d", $new_seasonid, dbDateEncode(time()), $User["Id"], $old_seasonid) ;
    dbQuery ($query) ;


    return 0 ;    
?>
