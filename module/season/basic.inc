<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

 //return sprintf ('oc %d', (int)$Record['OwnerCompanyId']) ;
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    // Default values
	    $Record['CompanyId'] = $User['CompanyId'] ;
	    $Record['OwnerId'] = $User['CompanyId'] ;
	    $Record['BaseVarCode'] = parameterGet ('BaseSKU' . $User['CompanyId']) ;
	    $Record['PickStockId'] = parameterGet ('DefPickStock' . $User['CompanyId']) ;
	    $Record['FromStockId'] = parameterGet ('DefFromStock' . $User['CompanyId']) ;
	    $Record['CollectionTypeId'] = 1 ;
		$Record['ExplicitMembers'] = 1 ; 
	    $Record['RestrictAccess'] = 1 ;
		break ;
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('Season', $Record['Name']) ;
	    itemSpace () ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;
	
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 40)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    itemFieldRaw ('Customer', formDBSelect ('CompanyId', (int)$Record['CompanyId'], sprintf("SELECT Id, CONCAT(Name,' (',Number,')') AS Value FROM Company WHERE TypeCustomer=1 AND ((ToCompanyId!=1169 AND ToCompanyId=0) OR ToCompanyId=%d) AND Active=1 AND ListHidden=0 ORDER BY Name", $User['CompanyId']), 'width:250px;')) ;  
	    itemSpace () ;
	    break ;
    }
    itemFieldRaw ('Collection Type', formDBSelect ('CollectionTypeId', $Record['CollectionTypeId'] , sprintf ('SELECT Id, Name AS Value FROM CollectionType ORDER BY Id'), 'width:250px;')) ;
    itemFieldRaw ('Owner Company', formDBSelect ('OwnerId', (int)$Record['OwnerId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    itemFieldRaw ('EAN Base Code', formText ('BaseVarCode', $Record['BaseVarCode'], 100, 'width:100%;')) ;
    itemSpace () ;
//    itemFieldRaw ('Wholesale', formCheckbox ('ExplicitMembers', $Record['ExplicitMembers'])) ;
    itemFieldRaw ('RestrictAccess', formCheckbox ('RestrictAccess', $Record['RestrictAccess'])) ;
    itemFieldRaw ('RestrictAssort', formCheckbox ('RestrictAssort', $Record['RestrictAssort'])) ;
    itemFieldRaw ('ShowOnBudget', formCheckbox ('ShowOnBudget', $Record['ShowOnBudget'])) ;
    itemFieldRaw ('ShowOnB2B', formCheckbox ('ShowOnB2B', $Record['ShowOnB2B'])) ;
    itemFieldRaw ('ShowOnWebshop', formCheckbox ('ShowOnWebshop', ($Record['WebshopId']>0)?1:0)) ;
    itemFieldRaw ('Done', formCheckbox ('Done', $Record['Done'])) ;
    itemFieldRaw ('Displayorder', formText ('SeasonDisplayOrder', $Record['SeasonDisplayOrder'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Pick Stock', formDBSelect ('PickStockId', $Record['PickStockId'] , sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1 ORDER BY Value'), 'width:250px;')) ;
    itemFieldRaw ('From Stock', formDBSelect ('FromStockId', $Record['FromStockId'], sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1 ORDER BY Value'), 'width:250px;')) ;
    itemSpace () ;
    itemFieldRaw ('Initial Pick Instructions', formtextArea ('InitialPickInstructions', $Record['InitialPickInstructions'], 'width:100%;height:100px;')) ;

    itemSpace () ;
	    itemField('LOTs:', '') ;
    itemSpace () ;
	echo '<table><tr>' ;
	
	$query = sprintf ("SELECT * FROM CollectionLot WHERE Active=1 AND SeasonId=%d", $Id) ;
	$res = dbQuery ($query) ;
	for ($i=1; $i<5; $i++)  {
	    $row = dbFetch ($res) ;	
		echo '<td><table>' ;
		itemFieldRaw ('', formHidden('LotId['.$i.']', $row['Id'], 8,'','')) ;
		itemFieldRaw ('Name', formText ('LotName['.$i.']', $row['Name'], 8, 'text-align:left;')) ;
		itemFieldRaw ('Date', formDate ('Date['.$i.']', dbDateDecode($row['Date']), 12, 'text-align:right;')) ;	
		itemFieldRaw ('PreSale', formCheckbox ('PreSale['.$i.']', $row['PreSale'], 12, 'text-align:right;')) ;	
		itemFieldRaw ('NoRetail', formCheckbox ('NoRetail['.$i.']', $row['NoRetail'], 12, 'text-align:right;')) ;	
		echo '</table></td>' ;
	}
	echo '</tr><tr>' ;
    itemSpace () ;
	for ($i=5; $i<9; $i++)  {
	    $row = dbFetch ($res) ;	
		echo '<td><table>' ;
		itemFieldRaw ('', formHidden('LotId['.$i.']', $row['Id'], 8,'','')) ;
		itemFieldRaw ('Name', formText ('LotName['.$i.']', $row['Name'], 8, 'text-align:left;')) ;
		itemFieldRaw ('Date', formDate ('Date['.$i.']', dbDateDecode($row['Date']), 12, 'text-align:right;')) ;	
		itemFieldRaw ('PreSale', formCheckbox ('PreSale['.$i.']', $row['PreSale'], 12, 'text-align:right;')) ;	
		itemFieldRaw ('NoRetail', formCheckbox ('NoRetail['.$i.']', $row['NoRetail'], 12, 'text-align:right;')) ;	
		echo '</table></td>' ;
	}
	echo '</tr></table>' ;
	dbQueryFree ($res) ;
    
    itemSpace () ;


    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
