<?php 
	
	$seasonId = $_GET['id'];

	$query = sprintf('select count(c.id) as No from collection c, collectionmember cm where c.seasonid=%d and c.active=1 and cm.collectionid=c.id and cm.active=1 and cm.cancel=0 and cm.notonwebshop=0', $seasonId) ;
	$result = dbquery($query) ;
	$row = dbFetch($result) ;
	if ((int)($row['No'] == 0))
		return 'No products is selected for this season to be shown on webshop. Please select by "set LOT on webshop" or "edit" on collectionmember' ;

	if(WEBSHOP_API_URL != ''){
		//// Update or add products from season to magento database.
		$potdata = json_encode(array('fromPot' => array('request' => 'create_season','data'    => $seasonId)));
		$ch = curl_init(WEBSHOP_API_URL);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
		curl_setopt($ch, CURLOPT_POSTFIELDS, $potdata);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		curl_setopt($ch, CURLOPT_TIMEOUT, 600);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);
		$result = json_decode($result);
		echo 'Request to retrieve ' . $row['No'] . ' selected products from this season is send to the webshop<br><br>You can now close the window.<br><br>';
		echo 'Respons from Webshop: ' . $result ;
	}
	else{
		echo 'Remote endpoint not specified';	
	}

	return 0;


?>