<?php

    require_once 'lib/webshop.inc' ;

	set_time_limit (1200) ;

    // Find orders from POT to import
	$ShopNo = 0 ;
//	dbReConnect($Webshop[$ShopNo]['Ip'], $Webshop[$ShopNo]['name']) ;
    $WEB_SO = array () ;
    $SO = array () ;
	$SO_lines = array () ;

    foreach ($_POST['ImportFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		
		if ($_POST['ShopNo'][$id] <> $ShopNo) {
			$ShopNo = $_POST['ShopNo'][$id] ;
			dbReConnect($Webshop[$ShopNo]['Ip'], $Webshop[$ShopNo]['name']) ;
		}
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;

		$WEB_SO[$id] = 1 ;
		
		// Get WEB Orderdata
		$query = sprintf ("SELECT WEBID, LASTNAME, ADDRESS, ZIP, CITY, PHONE, ALTLASTNAME, ALTADDRESS, ALTZIP, ALTCITY, o.SHIPPING as Carrier, 
									ws_sw.PRICE AS SHIPPINGPRICE, s.description as SHIPPINGNAME, o.DISCOUNT_CODE as DISCOUNT_CODE, 
									o.DISCOUNT_PERCENTAGE as DISCOUNT_PERCENTAGE, o.DISCOUNT_AMOUNT as DISCOUNT_AMOUNT, o.CURRENCY as CURRENCY
							FROM ws_order o, ws_customer c, ws_shipping s, ws_shipping_weight ws_sw
							WHERE o.id=%d and c.id=o.customerid and s.id=o.shipping and ws_sw.shippingid=s.id ", $id) ;
		$res = dbQuery ($query) ;
		$row = dbFetch($res) ;
		$row['WebShopNo'] = $ShopNo ;
		$SO[$id] = $row ;
		switch ($row['CURRENCY']) {
			case 'DKK':
				$product_tab = '' ;
				$currid=1 ;
				break ;
			case 'EUR':
				$product_tab = 'de_' ;
				$currid=2 ;
				break ;
			case 'GBP':
				$product_tab = 'uk_' ;
				$currid=3 ;
				break ;
			default:
				$product_tab = '' ;
				$currid=1 ;
				break ;
		}
		$product_tab .= "ws_product p" ;
		dbQueryFree ($res) ;
		unset($row) ;
		// Get WEB orderlines.
		$query = sprintf ("SELECT b.QTY as QTY, b.PRICE AS PRICE, p.productid as PRODUCTID, p.description as PRODDESC, p.colorname as COLORNAME, 
									b.FEATURES as FEATURES, p.ArticleColorId as ArticleColorId 
							FROM ws_order o, ws_basket b, %s 
							where o.id=%d and b.orderid=o.id and b.productid=p.id", $product_tab, $id) ;
		$res = dbQuery ($query) ;
		$rowno = 0 ;
		while ($row = dbFetch($res)) {
			$rowno++ ;
			$SO_lines[$id][$rowno] = $row ;
		}
		dbQueryFree ($res) ;
		unset($row) ;
	}

	dbReConnect('localhost', 'devel') ;
    foreach ($SO as $id => $row) {
		// Create POT Order
		unset($Order) ;
		$currrate = tableGetField('currency','Rate',$currid) ;
		$Order = array (
			'CompanyId'			=> $Webshop[$row['WebShopNo']]['CustomerId'],
			'ToCompanyId'		=> $Webshop[$row['WebShopNo']]['ToCompanyId'],

			'Description'		=> 'WEB order',
			'Reference'			=> $row['WEBID'],  // . '#' . $row['DISCOUNT_CODE'],
			'ReceivedDate' 		=> dbDateEncode(time()),
//				'DesiredDate' 	=> dbDateEncode(time()),
			'OrderTypeId'		=> 10,

			'SalesUserId'		=> 0,
			'CurrencyId'		=> $currid, // 1 ;
			'PaymentTermId'		=> 52, // Consumer Company
			'DeliveryTermId'	=> 9, // Consumer Company
			'CarrierId'			=> $Carrier[$row['Carrier']]['Id'], // POST DK (28), GLS (37)

			'AltCompanyName'=> $row['LASTNAME'],
			'Address1'		=> $row['ADDRESS'],
			'Address2'		=> '',
			'ZIP'			=> $row['ZIP'],
			'City'			=> $row['CITY'],
			'Phone'			=> $row['PHONE'],
			'CountryId'		=> $Carrier[$row['Carrier']]['countryid'], 
			'SeasonId'		=> tableGetFieldWhere ('Season', 'Id', sprintf('WebShopId=%d',  $row['WebShopNo'])), // 97,
			
			'InvoiceHeader'		=> '', // 'web ordre reference ' . $row['WEBID'] ,
			'InvoiceFooter'		=> '',
			'Instructions'		=> '',

			'ExchangeRate'		=> $currrate,	

			
			'Ready'			=> 1,
			'ReadyUserId'	=> $User['Id'],
			'ReadyDate'		=> dbDateEncode(time()),
			
			'WebShopNo'		=> $row['WebShopNo'] 
		) ;
		
		// alternative delivery adress
		if (isset($row['ALTLASTNAME']) and $row['ALTLASTNAME']<>'') {
			$Order['AltCompanyName'] = $row['ALTLASTNAME'] ;
		}
		if (isset($row['ALTADDRESS']) and $row['ALTADDRESS']<>'') {
			$Order['Address1'] = $row['ALTADDRESS'] ;
		}
		if (isset($row['ALTZIP']) and $row['ALTZIP']<>'') {
			$Order['ZIP'] = $row['ALTZIP'] ;
		}
		if (isset($row['ALTCITY']) and $row['ALTCITY']<>'') {
			$Order['City'] = $row['ALTCITY'] ;
		}
		
		if (isset($row['DISCOUNT_CODE']) or $row['DISCOUNT_CODE']<>'') {
			if ($currid==1) 
				$rabat='Rabatkode: ' ;
			else
				$rabat='Rabattcode: ' ;
			
			$Order['InvoiceHeader'] = $rabat . $row['DISCOUNT_CODE'];
		}
			$OrderId = tableWrite ('order', $Order) ;

		// Generate Pick Order 
//		if ($_POST['GenPickOrders']) {
			$PickOrder = array (	
				'Type'				=>  "SalesOrder",			// Pick order from internal customer
				'Reference'			=>  $OrderId,			
				'ReferenceId'		=>  $OrderId,			
				'FromId'			=> 14321,				// $_POST['PickStockId'],		
				'CompanyId'			=>  $Order['CompanyId'],	// Shop Code	
				'OwnerCompanyId'		=>  $Order['ToCompanyId'],		
				'HandlerCompanyId'	=>  $Order['ToCompanyId'], 					// $_POST['HandlerId'],					
				'Instructions'		=>  "",		
				'Packed'			=>  0,
				'DeliveryDate'		=>  dbDateEncode(time())	// Expected pick and pack complete
			) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder) ;
//		}

		// Create POT orderlines
		$OrderLineNo = 0;
		$TotalPrice = (float) 0 ;
	    foreach ($SO_lines[$id] as $rowno => $web_row) {
			// Article Number
			$Numbers = explode('_',$web_row['PRODUCTID']) ;
			$ArticleNumber = $Numbers[0] ;
			$ArtDescs = explode("@", $web_row['PRODDESC']);
			$ArtDesc = $ArtDescs[0] ;
			
			$allfeatures = $web_row['FEATURES'];
			if (empty($allfeatures)) return 'Missing Color or size for article' ;

			$features = explode(",", $allfeatures);
//			if (strpos($features[$0],":")===FALSE) return 'Missing Color for article' ;
		    
		    // Color
		    $feature = explode(":", $features[0]);
			$ColorText = $web_row['COLORNAME']; //substr($feature[1],1) ;

		    // Size
		    $feature = explode(":", $features[1]);
			$SizeText = substr($feature[1],1) ;

			$OrderLineNo++;
			$query = sprintf ("SELECT
								a.id as ArticleId, a.Description as ArticleDescription, 
								ac.id as ArticleColorId, c.description as color,
								az.id as ArticleSizeId, az.Name as size, 
								v.id as VariantCodeId, v.VariantCode as VariantCode
								FROM
								(article a, articlecolor ac, color c, articlesize az)
								LEFT JOIN
								VariantCode v ON v.articleid=a.id and v.articlecolorid=ac.id and v.articlesizeid=az.id and v.active=1
								WHERE
								a.number=%s and a.active=1
								and ac.articleid=a.id and az.articleid=a.id and ac.active=1 and az.active=1
								and ac.id=%d and c.id=ac.colorid
								and az.name='%s'", 
								$ArticleNumber,$web_row['ArticleColorId'],$SizeText) ;
			$res = dbQuery ($query) ;
			if (!(dbNumRows ($res)>0)) return 'didnt find article for: ' . $web_row['PRODUCTID'] . ' - ' . $ColorText . ' - ' . $SizeText  . ' - '. $query;
			$ol_row = dbFetch($res) ;
			
			
			$Price = (float)$web_row['PRICE'] ; //*0.8 ; VAT ???
			if ((int)$row['DISCOUNT_PERCENTAGE']>0) $_discount = (int)$row['DISCOUNT_PERCENTAGE'] ;
			else $_discount = 100 ;
			$TotalPrice += (float)(($Price * (int)$web_row['QTY'] * (int)$_discount)/100);
			
			// Set article, color and price info
			$OrderLineNew = array (
				'No' => $OrderLineNo,
				'Description' => $ArtDesc, //$web_row['PRODDESC'], //$ol_row['ArticleDescription'],
				'ArticleId' => $ol_row['ArticleId'],
				'ArticleColorId' => $ol_row['ArticleColorId'],
				'AltColorName' => $ColorText,
				'OrderId' => (int)$OrderId,
				'PriceCost' => 0,
				'PriceSale' =>  $Price,
				'DeliveryDate' => dbDateEncode(time()) ,
				'Quantity' => (int)$web_row['QTY'],
				'Surplus'	=> 0,
				'Discount'	=> (int)$row['DISCOUNT_PERCENTAGE'],
				'CustArtRef' => '',
				'InvoiceFooter' => ''
			) ;
			$OrderLineId = tableWrite ('OrderLine', $OrderLineNew) ;

		    // Create size quantity
			$OrderQuantity = array (
				'OrderLineId'		=> (int)$OrderLineId,
				'ArticleSizeId'		=> (int)$ol_row['ArticleSizeId'],
				'Quantity'			=> (int)$web_row['QTY']
			) ;
			$OrderQuantityId = tableWrite ('OrderQuantity', $OrderQuantity) ;
		    dbQueryFree ($res) ;
		    
			// Generate Pick Order Line!!
//		if ($_POST['GenPickOrders']) {
			$PickOrderLine = array (	
				'VariantCodeId'		=>  $ol_row['VariantCodeId'],
				'VariantCode'		=>  $ol_row['VariantCode'],			// EAN code 
				'OrderedQuantity'	=>  $web_row['QTY'],	
				'PickedQuantity'	=>  0,			
				'PackedQuantity'	=>  0,			
				'PickOrderId'		=>	$PickOrderId,
				'VariantDescription'=>	substr($ol_row['ArticleDescription'],0,48),
				'VariantColor'		=>	$ol_row['color'],
				'VariantSize'		=>	$ol_row['size'],
				'Done'				=>  0
			) ;
			tableWrite ('PickOrderLine', $PickOrderLine) ;
//		}
			
		} // End orderlines

		// Generate Orderline for freight!		
			// Set article, color and price info
			$OrderLineNew = array (
				'No' => $OrderLineNo+1,
				'Description' => 'Fragt ' . $row['SHIPPINGNAME'],
				'ArticleId' => 23107,
				'ArticleColorId' => 0,
				'OrderId' => (int)$OrderId,
				'PriceCost' => 0,
				'PriceSale' =>  $row['SHIPPINGPRICE'],
				'DeliveryDate' => dbDateEncode(time()) ,
				'Quantity' => 1,
				'Surplus'	=> 0,
				'CustArtRef' => '',
				'InvoiceFooter' => ''
			) ;
			$OrderLineId = tableWrite ('OrderLine', $OrderLineNew) ;
			$TotalPrice += (float)$row['SHIPPINGPRICE'] ;
		if ($row['DISCOUNT_PERCENTAGE']>0) {
			$DoNothing = 1;
		} else if ($row['DISCOUNT_AMOUNT']>0) {
			if ($row['DISCOUNT_AMOUNT'] > $TotalPrice) $row['DISCOUNT_AMOUNT'] = $TotalPrice ;
			$OrderLineNew = array (
				'No' => $OrderLineNo+2,
				'Description' => 'Gavekort rabat ' . $row['DISCOUNT_CODE'],
				'ArticleId' => 23107,
				'ArticleColorId' => 0,
				'OrderId' => (int)$OrderId,
				'PriceCost' => 0,
				'PriceSale' =>  -$row['DISCOUNT_AMOUNT'],
				'DeliveryDate' => dbDateEncode(time()) ,
				'Quantity' => 1,
				'Surplus'	=> 0,
				'CustArtRef' => '',
				'InvoiceFooter' => ''
			) ;
			$OrderLineId = tableWrite ('OrderLine', $OrderLineNew) ;
		}

	} // Next order

	// Set state in weborder.
	$ShopNo = 0 ;
    foreach ($SO as $id => $row) {
		if ($_POST['ShopNo'][$id] <> $ShopNo) {
			$ShopNo = $_POST['ShopNo'][$id] ;
			dbReConnect($Webshop[$ShopNo]['Ip'], $Webshop[$ShopNo]['name']) ;
		}
	
		$query = sprintf ("UPDATE ws_order SET STATUS=4 Where ID=%s", $id) ;
		$res = dbQuery ($query) ;
    }
    
    return 0 ;

?>
