<?php
    require_once 'lib/table.inc' ;

    // delete Collection member
	$SeasonId = tableGetField('SeasonAllowUser', 'SeasonId',(int)$Id) ;
	
    tableDelete ('SeasonAllowUser', $Id) ;

    return navigationCommandMark ('seasonview', (int)$SeasonId) ;

?>
