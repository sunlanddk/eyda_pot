<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

	$query = "SELECT Id, Name FROM collectionlot where seasonid=" . $Id . " order by Date" ;
	$res = dbQuery ($query) ;
	
    formStart () ;
    listStart () ;
    listRow () ;
    listHead ('LOT', 70) ;
    listHead ('Set all on Webshop', 30) ;
    listHead ('', 1) ;

	while ($row = dbFetch($res)) {
		listRow () ;
		listField ($row['Name']) ;
		listFieldRaw (formCheckbox (sprintf('ImportFlag[%d]', (int)$row['Id']), 0, '')) ;
    }
    listEnd () ;
    formEnd () ;
	dbQueryFree ($res) ;

    return 0 ;
?>
