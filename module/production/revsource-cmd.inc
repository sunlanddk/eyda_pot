<?php
require_once 'lib/db.inc';

$bundlelist = $_POST['bundles'];
if ($bundlelist) {
foreach($bundlelist as $bid => $value) {
  if(!$_POST['outcheck'][$bid]) {
    $query = sprintf("UPDATE OutSourcing SET OutDate = NULL, InDate=NULL where Id= %d", $bid);
    dbQuery($query);
  }
  if (!$_POST['incheck'][$bid]) {
    $query = sprintf("UPDATE OutSourcing SET InDate = NULL where Id= %d", $bid);
    dbQuery($query);
  }
}
}

return 0;
?>