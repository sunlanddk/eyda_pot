<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
require_once 'lib/navigation.inc';
require_once 'lib/form.inc';
    
    // Header
$sqlstm = sprintf('SELECT DISTINCT LocationId AS Id, c.Name AS Value FROM OutSourcingPlan p INNER JOIN ProductionLocation c ON p.LocationId = c.Id WHERE p.ProductionId =%d', $Id); 
    printf ('<div class="barsuper"><form method="post">') ;
printf ('<input type="hidden" name="nid" value="%d">', $nid);
printf ('<input type="hidden" name="id" value="%d">', $Id);

    printf ("Location ");
print htmlDbSelect('Filtervalue',$_POST['Filtervalue'],$sqlstm);
printf ('<input type=submit value="Go">');
printf("</form>\n") ;
     printf ("</div>\n") ;

    // Output Bundles
formStart();
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=65><p class=listhead>Number</p></td>") ;
   
    printf ("<td class=listhead width=120><p class=listhead>Color</p></td>") ;
    printf ("<td class=listhead width=70 ><p class=listhead>Size</p></td>") ;
    printf ("<td class=listhead width=90 ><p class=listhead>Operations</p></td>") ;
printf ("<td class=listhead width=105 ><p class=listhead>Location</p></td>") ;
    printf ("<td class=listhead width=90 align=right><p class=listhead>Out</p></td>") ;
    printf ("<td class=listhead width=90 align=right><p class=listhead>In</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;
    foreach ($SBundle as $row) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' style='cursor:pointer;' %s></td>", './image/toolbar/bundle.gif', navigationOnClickMark ('bundle', $row["BundleId"])) ;
	printf ("<td><p class=list>%08d</p></td>", $row['BundleId']) ;

	printf ("<td><p class=list>%s</p></td>", htmlentities($row['ColorNumber'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['SizeName'])) ;
	printf ("<td ><p class=list>%d - %d</p></td>", $row['OperationFromNo'], $row['OperationToNo']) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities(tableGetField('ProductionLocation','Name',$row['LocationId']))) ;
	$isout = $row['OutDate']?'':'disabled';

	$in = $row['InDate']? $row['InDate']:'<input type=checkbox name="incheck['.$row['Id'].']" '.$isout. ' >';
	$out = $row['OutDate']? $row['OutDate']:'<input type=checkbox checked name="outcheck['.$row['Id'].']" >';
	printf ("<td align=right><p class=list>%s</p></td>", $out) ;
	printf ("<td align=right><p class=list>%s</p></td>", $in) ;


	printf ("</tr>\n") ;
    }
    if (count($SBundle) == 0) {
        printf ("<tr><td></td><td colspan=2><p class=list>No bundles</p></td></tr>\n") ;
    } 
	
    printf ("</table>\n") ;
formEnd();
?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox' || e.disabled) continue ;
	e.checked = true ;
    }
}
</script>
<?php
    return 0 ;
?>
