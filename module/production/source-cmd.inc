<?php

// load disbled from db
$locks = $_POST['Locked']; 
// Inspect Conflicts
$locations = $_POST['LocationId'];
$from = $_POST['OperationFromNo'];
$to = $_POST['OperationToNo'];
$color = $_POST['ArticleColorId'];
$size = $_POST['ArticleSizeId'];
$ids = $_POST['OutSourcingPlanId'];

for ($i = 1; $i <= 25; $i++) {
  if ($locks[$i]) {
    $query='SELECT * FROM OutSourcingPlan WHERE Id = ' . $ids[$i];
    $res = dbQuery($query);
    $lrow = dbFetch($res);
    dbQueryFree($res);
$locations[$i] = $lrow['LocationId'];
$from[$i]  = $lrow['OperationFromNo'];
$to[$i]  = $lrow['OperationToNo'];
$color[$i]  = $lrow['ArticleColorId'];
$size[$i]  = $lrow['ArticleSizeId'];
$ids[$i]  = $lrow['OutSourcingPlanId'];

  }
}

for($i = 1; $i< 25; $i++) {
  if (!$locations[$i])continue;
  for($j = $i+1; $j<=25; $j++) {
    if (!$locations[$j])continue;
    if (($from[$i] <= $from[$j] && $to[$i] >= $from[$j]) || ($from[$j] <= $from[$i] && $to[$j] >= $from[$i])) {
      //operations overlap
      if($color[$i] == $color[$j] || $color[$i] == 0 || $color[$j] == 0) {
	if($size[$i] == $size[$j] || $size[$i] == 0 || $size[$j] == 0) {
	  return sprintf('Numbers %d and %d in conflict', $i, $j);
      
	}
      }
    }
  }
}

//update records

for($i = 1; $i<=25; $i++) {
  if ($locks[$i]) continue;
  $query = '';
  $set = sprintf('SET LocationId=%d, OperationFromNo=%d, OperationToNo=%d, %s %s Position=%d, ProductionId=%d',
		 $locations[$i], $from[$i], $to[$i], 'ArticleColorId='.($color[$i]?$color[$i]:'NULL'). ',', 
		 'ArticleSizeId='.($size[$i]?$size[$i]:'NULL').',',$i, $Id); 
  if(!$locations[$i] && $ids[$i]){
    $query = sprintf('DELETE FROM OutSourcingPlan WHERE Id=%d', $ids[$i]);
    dbQuery($query);
  } elseif ($ids[$i]) {
    //update
    $query = sprintf('UPDATE OutSourcingPlan %s WHERE Id=%d', $set, $ids[$i]);
    dbQuery($query);
  } elseif ($locations[$i]) {
    //create
    $query = sprintf('INSERT INTO OutSourcingPlan %s', $set);
    dbQuery($query);
  }
  $dbg .= $query;
  
}
return 0;

?>
