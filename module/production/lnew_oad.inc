<?php

    require_once 'module/style/include.inc' ;

    global $styleStateField ;    
    $StateField = "IF(Production.Done,'Done',
						IF(NOT Production.Fabrics,'Defined',
							IF(NOT Production.Allocated,'Request',
								IF(NOT Production.Ready,'Allocated',
									IF(Production.Packed,'Packed',
										IF(Production.Cut,'Prod-cut',
											IF(Production.Approved OR ProductionLocation.TypeSample,'Prod',
												'Ready')))))))" ;
    $StateField = "IF(Production.Done,'Done',
						IF(Production.Packed,'Packed',
							IF(Production.Cut,'Prod-Cut',
								IF(Production.Approved OR ProductionLocation.TypeSample,'Prod',
									IF(Production.Allocated,if(Production.Ready,'Ready-Alloc','Allocated'),
										IF(Production.Request,if(Production.Ready,'Ready-Request','Request'),
											IF(Production.Ready,'Ready-Unreq',
												'Draft')))))))" ;
    $queryFields = 'Production.*, CONCAT(Case.Id,"/",Production.Number) AS Number, Case.Id AS CaseId, Case.Description AS CaseDescription, Case.CustomerReference AS CaseCustomerReference, Case.ArticleCertificateId, Company.Id AS CompanyId, Company.Number AS CompanyNumber, Company.Name AS CompanyName, Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.HasStyle AS ArticleHasStyle, Article.Description AS ArticleDescription, Article.Comment AS ArticleComment, FabricArticle.Id AS FabricArticleId, FabricArticle.Number AS FabricArticleNumber, FabricArticle.Description AS FabricArticleDescription, ProductionLocation.Name AS ProductionLocationName, ProductionLocation.TypeProduction, ProductionLocation.TypeSample, ProductionLocation.DoBundles, Style.Id AS StyleId, Style.Version AS StyleVersion, Style.Ready AS StyleReady, Style.Canceled AS StyleCanceled, Style.Approved AS StyleApproved, ' . $StateField . ' AS State, ' . $styleStateField . ' AS StyleState' ;
    $queryTables = 'Production LEFT JOIN `Case` ON Case.Id=Production.CaseId LEFT JOIN Article ON Article.Id=Case.ArticleId LEFT JOIN Article AS FabricArticle ON FabricArticle.Id=Article.FabricArticleId LEFT JOIN Company ON Company.Id=Case.CompanyId LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId LEFT JOIN Style ON Style.Id=Production.StyleId ';
/*
function makeOutSourcings() {
  global $Id;
   $query =sprintf ('DELETE FROM OutSourcing WHERE OutDate IS NULL AND ProductionId = %d', $Id);
      dbQuery($query);
      $query = sprintf ('INSERT IGNORE INTO OutSourcing (OperationFromNo, OperationToNo,ArticleColorId, ArticleSizeId, ProductionId, BundleId, LocationId) SELECT OperationFromNo, OperationToNo, q.ArticleColorId, q.ArticleSizeId, p.ProductionId, b.Id AS BundleId , p.LocationId FROM (OutSourcingPlan p, ProductionQuantity q, Bundle b) WHERE p.ProductionId=%d AND q.ProductionId = p.ProductionId AND b.ProductionQuantityId = q.Id AND (p.ArticleColorId IS NULL OR p.ArticleColorId=q.ArticleColorId) AND (p.ArticleSizeId IS NULL OR p.ArticleSizeId=q.ArticleSizeId)', $Id);
      dbQuery($query);
      //set flags
      $query = sprintf('SELECT COUNT(*)AS cnt FROM OutSourcing WHERE ProductionId = %d AND OutDate IS NOT NULL', $Id);
      $res = dbRead($query);

      if ($res[0]['cnt']){
	$query = sprintf('UPDATE Production 
SET SourceOut = 1 , 
SourceOutDate = (SELECT MIN(OutDate) FROM OutSourcing WHERE ProductionId = %d)
WHERE Id = %d AND SourceOut = 0', $Id, $Id);
	dbQuery($query);
      }

$query = sprintf('SELECT COUNT(*)AS cnt FROM OutSourcing WHERE ProductionId = %d AND InDate IS NULL', $Id);
      $res = dbRead($query);
      if (!$res[0]['cnt']){
	$query = sprintf('UPDATE Production 
SET SourceIn = 1 , 
SourceInDate = (SELECT COALESCE(MAX(InDate), \'1980-01-01\') FROM OutSourcing WHERE ProductionId = %d)
WHERE Id = %d', $Id, $Id);
	dbQuery($query);
      } else {
	$query = sprintf('UPDATE Production 
SET SourceIn = 0
WHERE Id = %d', $Id);
	dbQuery($query);
      }
}
*/

    switch ($Navigation['Function']) {
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Case.Id'),
		array ('name' => 'Location',		'db' => 'ProductionLocation.Name'),
		array ('name' => 'State',		'db' => $StateField),
		array ('name' => 'Style',		'db' => $styleStateField),
		array ('name' => 'Article',		'db' => 'Article.Number'),
		array ('name' => 'Ver',			'db' => 'Style.Version'),
		array ('name' => 'Description',		'db' => 'Article.Description'),
		array ('name' => 'Customer',		'db' => 'Company.Name'),
		array ('name' => 'Quantity',		'db' => 'Production.Quantity'),
		array ('name' => 'Material',		'db' => 'Production.MaterialDate'),
		array ('name' => 'Start',		'db' => 'Production.ProductionStartDate'),
		array ('name' => 'End',			'db' => 'Production.ProductionEndDate'),
		array ('name' => 'Subdel.',		'db' => 'Production.SubDeliveryDate'),
		array ('name' => 'Delivery',		'db' => 'Production.DeliveryDate')
	    ) ;

	    switch ($Navigation['Parameters']) {
		case 'case' :
		    // Specific case
		    $query = sprintf ('SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $queryClause = sprintf ('Production.CaseId=%d AND Production.Active=1', $Id) ;

		    // Navigation
		    require_once 'lib/navigation.inc' ;
		    navigationEnable ('productionordernew') ;
		    navigationEnable ('sampleordernew') ;

		    if ($Record['Done']) {
			navigationPasive ('productionordernew') ;
			navigationPasive ('sampleordernew') ;
		    }
		    
		    break ;

		case 'season' :
		    $queryTables = sprintf('Production 
						Inner Join (select ol.productionid as Id from `Order` O, Orderline ol where ol.orderid=o.Id and ol.active=1 and o.active=1 and o.seasonId=%d group by Id) ProdOrder on ProdOrder.Id=Production.Id
						LEFT JOIN `Case` ON Case.Id=Production.CaseId LEFT JOIN Article ON Article.Id=Case.ArticleId LEFT JOIN Article AS FabricArticle ON FabricArticle.Id=Article.FabricArticleId LEFT JOIN Company ON Company.Id=Case.CompanyId LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId LEFT JOIN Style ON Style.Id=Production.StyleId',
						$Id) ;
		    $queryClause = 'Production.Active=1' ;
		    break ;

		default :
		    $queryClause = 'Production.Done=0 AND Production.Active=1' ;
		    break ;
	    }
	    
//return 'SELECT' . $queryFields . 'FROM' . $queryTables . 'WHERE' . $queryClause ;
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, 'Production.Number') ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'productionview') ;
	    return $res ;

	case 'view' :
	case 'quantity' :
	case 'quantity-cmd' :
	case 'makeitem' :
	case 'makeitem-cmd' :
	    global $Color, $Size, $Quantity ;		// For direct view only
	    // makeOutSourcings();
   $query =sprintf ('DELETE FROM OutSourcing WHERE OutDate IS NULL AND ProductionId = %d', $Id);
      dbQuery($query);
      $query = sprintf ('INSERT IGNORE INTO OutSourcing (OperationFromNo, OperationToNo,ArticleColorId, ArticleSizeId, ProductionId, BundleId, LocationId) SELECT OperationFromNo, OperationToNo, q.ArticleColorId, q.ArticleSizeId, p.ProductionId, b.Id AS BundleId , p.LocationId FROM (OutSourcingPlan p, ProductionQuantity q, Bundle b) WHERE p.ProductionId=%d AND q.ProductionId = p.ProductionId AND b.ProductionQuantityId = q.Id AND (p.ArticleColorId IS NULL OR p.ArticleColorId=q.ArticleColorId) AND (p.ArticleSizeId IS NULL OR p.ArticleSizeId=q.ArticleSizeId)', $Id);
      dbQuery($query);
      //set flags
      $query = sprintf('SELECT COUNT(*)AS cnt FROM OutSourcing WHERE ProductionId = %d AND OutDate IS NOT NULL', $Id);
      $res = dbRead($query);

      if ($res[0]['cnt']){
	$query = sprintf('UPDATE Production 
SET SourceOut = 1 ,
SourceOutUserId = %d, 
SourceOutDate = (SELECT MIN(OutDate) FROM OutSourcing WHERE ProductionId = %d)
WHERE Id = %d AND SourceOut = 0',$User['Id'], $Id, $Id);
	dbQuery($query);
      } else {
$query = sprintf('UPDATE Production 
SET SourceOut = 0
WHERE Id = %d ', $Id);
	dbQuery($query);
      }

$query = sprintf('SELECT COUNT(*)AS cnt FROM OutSourcing WHERE ProductionId = %d AND InDate IS NULL', $Id);
      $res = dbRead($query);
      if (!$res[0]['cnt']){
	$query = sprintf('UPDATE Production 
SET SourceIn = 1 , 
SourceInUserId = %d,
SourceInDate = (SELECT COALESCE(MAX(InDate), \'1980-01-01\') FROM OutSourcing WHERE ProductionId = %d)
WHERE Id = %d AND SourceIn = 0', $User['Id'],$Id, $Id);
	dbQuery($query);
      } else {
	$query = sprintf('UPDATE Production 
SET SourceIn = 0
WHERE Id = %d', $Id);
	dbQuery($query);
      }

	    
	    // Get Production
	    $query = sprintf ('SELECT %s FROM %s WHERE Production.Id=%d AND Production.Active=1', $queryFields, $queryTables, $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    //Get OutSourcingPlan
	    $query = sprintf('SELECT COUNT(*) as cnt FROM OutSourcingPlan WHERE ProductionId = %d', $Id);
	    $res = dbRead($query);
	    if( $res[0]['cnt']) {
	      $Record['SourceLocationId'] = 1;
	      $Record['SourceFromNo'] = 1;
	    }

	    // Get colors
	    $query = sprintf ("SELECT ArticleColor.Id, Color.Number AS ColorNumber, Color.Description AS ColorDescription, CONCAT(Color.Number,' (',Color.Description,')') AS Number, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM (ArticleColor, Color) LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Color.Number", $Record['ArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Color = array() ;
	    while ($row = dbFetch ($result)) {
		$Color[(int)$row['Id']] = $row ;
	    }
	    dbQueryFree ($result) ;

	    // Get sizes
	    $query = sprintf ("SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Record['ArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Size = array() ;
	    while ($row = dbFetch ($result)) {
		$Size[(int)$row['Id']] = $row['Name'] ;
	    }
	    dbQueryFree ($result) ;
    
	    // Get counts (Ordered and Bundeled)
	    $query = sprintf ("SELECT ProductionQuantity.*, SUM(Bundle.Quantity) AS BundleQuantity FROM ProductionQuantity LEFT JOIN Bundle ON ProductionQuantity.Id=Bundle.ProductionQuantityId AND Bundle.Active=1 WHERE ProductionQuantity.ProductionId=%d AND ProductionQuantity.Active=1 GROUP BY ProductionQuantity.Id", $Record['Id']) ;
	    $result = dbQuery ($query) ;
//	    unset ($Quantity) ;		// This unset will clear the global referance as well !!!!
	    while ($row = dbFetch ($result)) {
		$ArticleColorId = (int)$row['ArticleColorId'] ;
		$ArticleSizeId = (int)$row['ArticleSizeId'] ;
		if (!isset($Color[$ArticleColorId])) continue ;
		if (!isset($Size[$ArticleSizeId])) continue ;
		if (isset($Quantity[$ArticleColorId][$ArticleSizeId])) continue ;
		$Quantity[$ArticleColorId][$ArticleSizeId] = $row ;
	    }
	    dbQueryFree ($result) ;

	    // Get counts (Produced by Sortation)
	    $query = sprintf ("SELECT Item.* FROM Item WHERE Item.FromProductionId=%d AND Item.Active=1", $Record['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
		$ArticleColorId = (int)$row['ArticleColorId'] ;
		$ArticleSizeId = (int)$row['ArticleSizeId'] ;
		$Sortation = (int)$row['Sortation'] ;
		if (!isset($Color[$ArticleColorId])) continue ;
		if (!isset($Size[$ArticleSizeId])) continue ;
		if ($Sortation < 1 or $Sortation > 3) continue ;
		$Quantity[$ArticleColorId][$ArticleSizeId]['Sortation'][$Sortation] += (int)$row['Quantity'] ;
	    }
	    dbQueryFree ($result) ;

	    // Navigation
	    require_once './lib/navigation.inc' ;
	    if ($Record['TypeSample']) {
		navigationPasive ('productionoperations') ;
		navigationPasive ('productionbundles') ;
		navigationPasive ('sales') ;
		navigationPasive ('source') ;
	    }

	    if (!$Record['DoBundles']) {
		navigationPasive ('productionbundles') ;
		navigationPasive ('source') ;
	    }

	    if ($Record['Ready']) {
		navigationPasive ('update') ;
	    }

	    return 0 ;

	case 'bundles' :
	    // Get Production
	    $query = sprintf ('SELECT %s FROM %s WHERE Production.Id=%d AND Production.Active=1', $queryFields, $queryTables, $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;
if ($Record['ArticleHasStyle']) {
	    // Get real production minutes from newcurrentstyleoperations
	    $query = sprintf ("SELECT SUM(newcurrentstyleoperation.ProductionMinutes) AS RealMinutes FROM newcurrentstyleoperation WHERE newcurrentstyleoperation.StyleId=%d AND newcurrentstyleoperation.Active=1", $Record['StyleId']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch($result) ;	    
	    dbQueryFree ($result) ;
	    $Record['RealMinutes'] = (float)$row['RealMinutes'] ;

	    // Get Bundles
	    $Bundle = array () ;
	    $query = sprintf ("SELECT Bundle.*, CONCAT(Color.Description,' (',Color.Number,')') AS ColorNumber, ArticleSize.Name AS SizeName FROM (Bundle, ProductionQuantity) LEFT JOIN ArticleColor ON ArticleColor.Id=ProductionQuantity.ArticleColorId LEFT JOIN Color ON Color.Id=ArticleColor.ColorId LEFT JOIN ArticleSize ON ArticleSize.Id=ProductionQuantity.ArticleSizeId WHERE Bundle.ProductionId=%d AND Bundle.Active=1 AND ProductionQuantity.Id=Bundle.ProductionQuantityId ORDER BY Bundle.Id", $Record['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['Id'] ;
		$Bundle[$i] = $row ;
	    }
	    
	    // Get work done for Bundles
	    $query = sprintf ("SELECT BundleWork.BundleId, SUM(BundleWork.Quantity*newcurrentstyleoperation.ProductionMinutes) AS UsedMinutes FROM (BundleWork, newcurrentstyleoperation) WHERE BundleWork.ProductionId=%d AND newcurrentstyleoperation.Id=BundleWork.StyleOperationId AND BundleWork.Active=1", $Record['Id']) ;
	    if ($Record['SourceLocationId'] > 0) $query .= sprintf (' AND (newcurrentstyleoperation.No<%d OR newcurrentstyleoperation.No>%d)', (int)$Record['SourceFromNo'], (int)$Record['SourceToNo']) ; 
	    $query .= " GROUP BY BundleWork.BundleId" ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['BundleId'] ;
		if (!isset($Bundle[$i])) {
		    printf ("BundleWork Bundle (id %d) no found", $i) ;
		    continue ;
		}
		$Bundle[$i]['UsedMinutes'] = $row['UsedMinutes'] ;
	    }

	    // Get time for outsourced work
	    $query = sprintf ("SELECT SUM(newcurrentstyleoperation.ProductionMinutes) AS SourcedMinutes FROM newcurrentstyleoperation WHERE newcurrentstyleoperation.StyleId=%d AND newcurrentstyleoperation.No>=%s AND newcurrentstyleoperation.No<=%d AND newcurrentstyleoperation.Active=1 ", (int)$Record['StyleId'],  (int)$Record['SourceFromNo'], (int)$Record['SourceToNo']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch($result) ;
	    dbQueryFree ($result) ;
	    $Record['SourcedMinutes'] = (float)$row['SourcedMinutes'] ;

	    //get outsourced minutes for bundles
	    $query = sprintf("select sum(o.ProductionMinutes) AS DoneMinutesOut, b.BundleId from OutSourcing b inner join Production p on p.Id=b.ProductionId
			inner join Style s on p.StyleId= s.Id
			inner join newcurrentstyleoperation o on o.StyleId = s.Id
			and o.No >= b.OperationFromNo and o.No <= b.OperationToNo
			where p.Id = %d and b.InDate is not null group by b.BundleId", $Id);
} else {
	    // Get real production minutes from StyleOperations
	    $query = sprintf ("SELECT SUM(StyleOperation.ProductionMinutes) AS RealMinutes FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1", $Record['StyleId']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch($result) ;	    
	    dbQueryFree ($result) ;
	    $Record['RealMinutes'] = (float)$row['RealMinutes'] ;

	    // Get Bundles
	    $Bundle = array () ;
	    $query = sprintf ("SELECT Bundle.*, CONCAT(Color.Description,' (',Color.Number,')') AS ColorNumber, ArticleSize.Name AS SizeName FROM Bundle, ProductionQuantity LEFT JOIN ArticleColor ON ArticleColor.Id=ProductionQuantity.ArticleColorId LEFT JOIN Color ON Color.Id=ArticleColor.ColorId LEFT JOIN ArticleSize ON ArticleSize.Id=ProductionQuantity.ArticleSizeId WHERE Bundle.ProductionId=%d AND Bundle.Active=1 AND ProductionQuantity.Id=Bundle.ProductionQuantityId ORDER BY Bundle.Id", $Record['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['Id'] ;
		$Bundle[$i] = $row ;
	    }
	    
	    // Get work done for Bundles
	    $query = sprintf ("SELECT BundleWork.BundleId, SUM(BundleWork.Quantity*StyleOperation.ProductionMinutes) AS UsedMinutes FROM BundleWork, StyleOperation WHERE BundleWork.ProductionId=%d AND StyleOperation.Id=BundleWork.StyleOperationId AND BundleWork.Active=1", $Record['Id']) ;
	    if ($Record['SourceLocationId'] > 0) $query .= sprintf (' AND (StyleOperation.No<%d OR StyleOperation.No>%d)', (int)$Record['SourceFromNo'], (int)$Record['SourceToNo']) ; 
	    $query .= " GROUP BY BundleWork.BundleId" ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['BundleId'] ;
		if (!isset($Bundle[$i])) {
		    printf ("BundleWork Bundle (id %d) no found", $i) ;
		    continue ;
		}
		$Bundle[$i]['UsedMinutes'] = $row['UsedMinutes'] ;
	    }

	    // Get time for outsourced work
	    $query = sprintf ("SELECT SUM(StyleOperation.ProductionMinutes) AS SourcedMinutes FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.No>=%s AND StyleOperation.No<=%d AND StyleOperation.Active=1 ", (int)$Record['StyleId'],  (int)$Record['SourceFromNo'], (int)$Record['SourceToNo']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch($result) ;
	    dbQueryFree ($result) ;
	    $Record['SourcedMinutes'] = (float)$row['SourcedMinutes'] ;

	    //get outsourced minutes for bundles
	    $query = sprintf("select sum(o.ProductionMinutes) AS DoneMinutesOut, b.BundleId from OutSourcing b inner join Production p on p.Id=b.ProductionId
			inner join Style s on p.StyleId= s.Id
			inner join StyleOperation o on o.StyleId = s.Id
			and o.No >= b.OperationFromNo and o.No <= b.OperationToNo
			where p.Id = %d and b.InDate is not null group by b.BundleId", $Id);
}
  $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['BundleId'] ;
		if (!isset($Bundle[$i])) {
		  // printf ("BundleWork Bundle (id %d) no found", $i) ;
		    continue ;
		}
		$Bundle[$i]['OutMinutes'] = $row['DoneMinutesOut'] ;
	    }
	    return 0 ;

	case 'operations' :
	    // Get Production
	    $query = sprintf ('SELECT %s FROM %s WHERE Production.Id=%d AND Production.Active=1', $queryFields, $queryTables, $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Get real bundled quantity
	    $query = sprintf ("SELECT SUM(Bundle.Quantity) AS BundleQuantity FROM Bundle WHERE Bundle.ProductionId=%d AND Bundle.Active=1", $Record['Id']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch($result) ;	    
	    dbQueryFree ($result) ;
	    $Record['BundleQuantity'] = (int)$row['BundleQuantity'] ;

	    // Operations
	    $Operation = array () ;
    if ($Record['ArticleHasStyle']) {
	    $query = sprintf ("SELECT newcurrentstyleoperation.*, CONCAT(MachineGroup.Number,Operation.Number) AS OperationNumber FROM newcurrentstyleoperation LEFT JOIN Operation ON newcurrentstyleoperation.OperationId=Operation.Id LEFT JOIN MachineGroup ON Operation.MachineGroupId=MachineGroup.Id WHERE newcurrentstyleoperation.StyleId=%d AND newcurrentstyleoperation.Active=1 ORDER BY newcurrentstyleoperation.No", $Record['StyleId']) ;
    } else {
	    $query = sprintf ("SELECT StyleOperation.*, CONCAT(MachineGroup.Number,Operation.Number) AS OperationNumber FROM StyleOperation LEFT JOIN Operation ON StyleOperation.OperationId=Operation.Id LEFT JOIN MachineGroup ON Operation.MachineGroupId=MachineGroup.Id WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No", $Record['StyleId']) ;
    }
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['Id'] ;
		$Operation[$i] = $row ;
		if ($Record['SourceLocationId'] > 0 and $Record['SourceFromNo'] <= $row['No'] and $Record['SourceToNo'] >= $row['No']) $total['SourceMinutes'] += $row['ProductionMinutes'] ;
	    }
	    dbQueryFree ($result) ;

	    // Get work done for Operations
	    $query = sprintf ("SELECT BundleWork.StyleOperationId, SUM(BundleWork.Quantity) AS BundleWorkQuantity FROM BundleWork WHERE BundleWork.ProductionId=%d AND BundleWork.Active=1 GROUP BY BundleWork.StyleOperationId", $Record['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['StyleOperationId'] ;
		if (!isset($Operation[$i])) {
		    printf ("BundleWork StyleOperation (id %d) no found in Style (id %d)<br>", $i, $Record['StyleId']) ;
		    continue ;
		}
		$Operation[$i]['BundleWorkQuantity'] = (int)$row['BundleWorkQuantity'] ;
	    }
	    dbQueryFree ($result) ;

	    // Get Name of Source Location
	    require_once 'lib/table.inc' ;
	    $SourceLocationName = tableGetField ('ProductionLocation', 'Name', $Record['SourceLocationId']) ;
	    return 0 ;
	    
	case 'generate' :
	case 'generate-cmd' :
        switch ($Navigation['Parameters']) {
	    case 'season':
	    case 'seasonwithsizes':
			$Record['SeasonId'] = $Id ;
			break ;
	    default:
			// Get record
	    		$query = sprintf ('SELECT `Order`.*, Company.Number AS CustomerNumber, Company.Name AS CustomerName FROM `Order` LEFT JOIN Company ON Company.Id=Order.CompanyId WHERE `Order`.Id=%d and `Order`.Active=1', $Id) ;
	    		$result = dbQuery ($query) ;
	    		$Record = dbFetch ($result) ;
	    		dbQueryFree ($result) ;
			break ;
	    }
	    return 0 ;
	    
    case 'sourcedbundles':
    case 'revertsourcedbundles':

      $SBundle = array();
	    $query = sprintf ("SELECT OutSourcing.*, CONCAT(Color.Description,' (',Color.Number,')') AS ColorNumber, ArticleSize.Name AS SizeName FROM OutSourcing LEFT JOIN ArticleColor ON ArticleColor.Id=OutSourcing.ArticleColorId LEFT JOIN Color ON Color.Id=ArticleColor.ColorId LEFT JOIN ArticleSize ON ArticleSize.Id=OutSourcing.ArticleSizeId WHERE OutSourcing.ProductionId = %d %s ORDER BY OperationFromNo", $Id, $_POST['Filtervalue']?' AND OutSourcing.LocationId = ' . $_POST['Filtervalue']:'') ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$i = (int)$row['Id'] ;
		$SBundle[$i] = $row ;
	    }

      return 0;

	default:
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ("SELECT Case.Id, Case.Id AS CaseId, Case.Description, Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription FROM `Case` LEFT JOIN Article ON Article.Id=Case.ArticleId WHERE Case.Id=%d AND Case.Active=1", $Id) ;
		    $result = dbQuery ($query) ;
		    $Record = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    return 0 ;
	    }

	    // Get record
	    if ($Id <= 0) return 0 ;
	    $query = sprintf ('SELECT %s FROM %s WHERE Production.Id=%d AND Production.Active=1', $queryFields, $queryTables, $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    return 0 ;  
    }


?>
