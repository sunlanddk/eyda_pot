<?php
   
    function tableErase ($table) {
	global $Id ;
	$query = sprintf ("DELETE FROM %s WHERE ProductionId=%d", $table, $Id) ;
	dbQuery ($query) ;
    }

    // Erase related records    
    tableErase ("ProductionQuantity") ;
    tableErase ("Bundle") ;
    tableErase ("BundleWork") ;
    tableErase ("BundleQuantity") ;

    // Finaly the ProductionOrder   
    $query = sprintf ("DELETE FROM Production WHERE Id=%d", $Id) ;
    dbQuery ($query) ;

    return 0 ;
?>
