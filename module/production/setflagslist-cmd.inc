<?php

//    if (!is_array($_POST['AllocFlag'])) return 'please select flag(s) to set' ;
    $PO = array () ;
    foreach ($_POST['AllocFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$PO[$id]['Allocated'] = 1 ;
	}
    foreach ($_POST['ReadyFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$PO[$id]['Ready'] = 1 ;
	}
    foreach ($_POST['PackedFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$PO[$id]['Packed'] = 1 ;
	}

    foreach ($PO as $id => $row) {
		unset ($PO_update) ;
		if ($row['Allocated']) {
			$PO_update['Allocated'] = $row['Allocated'];
			$PO_update['AllocatedUserId'] = $User['Id'];
			$PO_update['AllocatedDate'] = dbDateEncode(time());
		}
		if ($row['Ready']) {
			$PO_update['Ready'] = $row['Ready'];
			$PO_update['ReadyUserId'] = $User['Id'];
			$PO_update['ReadyDate'] = dbDateEncode(time());
		}
		if ($row['Packed']) {
			$PO_update['Packed'] = $row['Packed'];
			$PO_update['PackedUserId'] = $User['Id'];
			$PO_update['PackedDate'] = dbDateEncode(time());
		}
		tableWrite ('production', $PO_update, (int)$id) ;
    }
    return 0 ;
?>
