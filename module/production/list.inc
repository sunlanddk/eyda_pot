<?php


    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;

    // Filter Bar
    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'case' :
	    require_once 'lib/item.inc' ;
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Case', $Record['Id'], 'case.gif', 'caseview', $Record['Id']) ;
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;
    }
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 65) ;
    switch ($Navigation['Parameters']) {
	  case 'devel': 
	  case 'seasondevel': 
	    listHead ('Location', 120) ;
	    listHead ('State', 85) ;
	    listHead ('Description', 120) ;
	    break ;
     case 'sample' :
     case 'seasonsample' :
	    listHead ('Location', 120) ;
	    listHead ('State', 85) ;
	    listHead ('Description', 120) ;
	    listHead ('Style', 55) ;
	    break ;
     default:
	    listHead ('Location', 70) ;
	    listHead ('State', 85) ;
	    listHead ('Fabric', 55) ;
	    listHead ('Acc.', 55) ;		
	    listHead ('Style', 55) ;
     }
    listHead ('Article', 70) ;
    listHead ('Ver', 45) ;
    listHead ('Description') ;
    listHead ('Customer') ;
    if (($Navigation['Parameters'] == 'devel') or ($Navigation['Parameters'] == 'seasondevel')) {
	    listHead ('plan') ;
	    listHead ('missing') ;
	    listHead ('comment') ;
	}
    listHead ('Quantity', 65, 'align=right') ;
    switch ($Navigation['Parameters']) {
	  case 'devel': 
	  case 'seasondevel': 
	    listHead ('Ship', 80, 'align=right') ;
	    listHead ('Approval', 80, 'align=right') ;
	    break ;
	  default:
	    listHead ($Navigation['Parameters']=='case'?'Mat/Ship':'Material', 80, 'align=right') ;
	    listHead ('Start', 70, 'align=right') ;
	    listHead ('End', 70, 'align=right') ;
	    listHead ($Navigation['Parameters']=='case'?'SubDel/Approve':'SubDel.', 80, 'align=right') ;
	    listHead ('Delivery', 70, 'align=right') ;
	}
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	switch ($Navigation['Parameters']) {
		case 'devel':
		case 'sample':
		      $ListViewMark = $Navigation['Parameters'] . 'view' ;
			break;
		default:
		      $ListViewMark = 'productionview' ;
			break ;
	}
	listFieldIcon ($Navigation['Icon'], $ListViewMark, $row['Id']) ;
	listField ($row['Number']) ;
	listField ($row['ProductionLocationName']) ;
	listField ($row['State']) ;

	// added 2007-02-22: States for new styles
	if ($row['ArticleId']) {
		$HasStyle = tableGetField ('Article', 'HasStyle', $row['ArticleId']) ;
	} else {
		$HasStyle = 0 ;
	}
	if ($HasStyle) {
		if ($row['StyleCanceled'] == 1)
			$row['StyleState'] = 'Canceled' ;
		else
			$row['StyleState'] = StyleState_NewStyles ($row['StyleId']) ;
	}
	// added 2007-02-22: States for new styles

/*
    if (($Navigation['Parameters'] <> 'devel') and ($Navigation['Parameters'] <> 'seasondevel')) {
		listField ($row['Fabrics']?'Alloc':($row['MaterialSent']?'Req':'')) ;
		listField ($row['Accessories']?'Alloc':($row['AccSent']?'Req':'')) ;		
		listField ($row['StyleState']) ;
      }
*/

    switch ($Navigation['Parameters']) {
	  case 'devel': 
	  case 'seasondevel': 
		listField ($row['Description']) ;
	    break ;
     case 'sample' :
     case 'seasonsample' :
		listField ($row['Description']) ;
		listField ($row['StyleState']) ;
	    break ;
     default:
		listField ($row['Fabrics']?'Alloc':($row['MaterialSent']?'Req':'')) ;
		listField ($row['Accessories']?'Alloc':($row['AccSent']?'Req':'')) ;		
		listField ($row['StyleState']) ;
     }


	
	listField ($row['ArticleNumber']) ;
	listField ((int)$row['StyleVersion']) ;
	listField ($row['ArticleDescription']) ;
	listField ($row['CompanyName']) ;
    if (($Navigation['Parameters'] == 'devel') or ($Navigation['Parameters'] == 'seasondevel')) {
		listField ($row['PlanText']) ;
		listField ($row['MissingText']) ;
		listField ($row['Comment']) ;
	}
	listField (number_format ((float)$row['Quantity'], 0, ',', '.'), 'align=right') ;
//	if (dbDateDecode($row["MaterialDate_Act"])>0) {
//		listField ('A ' . date ("Y-m-d", dbDateDecode($row["MaterialDate_Act"])), 'align=right') ;
//		listField (date ("Y-m-d", dbDateDecode($row["ProductionStartDate_Act"])), 'align=right') ;
//		listField (date ("Y-m-d", dbDateDecode($row["ProductionEndDate_Act"])), 'align=right') ;
//		listField (date ("Y-m-d", dbDateDecode($row["SubDeliveryDate_Act"])), 'align=right') ;
//		listField (date ("Y-m-d", dbDateDecode($row["DeliveryDate_Act"])), 'align=right') ;
    if (($Navigation['Parameters'] <> 'devel') and ($Navigation['Parameters'] <> 'seasondevel')) {
		listField (date ("Y-m-d", dbDateDecode($row["MaterialDate"])), 'align=right') ;
		listField ($row['POType']=='devel'? '' : date ("Y-m-d", dbDateDecode($row["ProductionStartDate"])), 'align=right') ;
		listField ($row['POType']=='devel'? '' : date ("Y-m-d", dbDateDecode($row["ProductionEndDate"])), 'align=right') ;
		listField (date ("Y-m-d", dbDateDecode($row["SubDeliveryDate"])), 'align=right') ;
		listField ($row['POType']=='devel'? '' : date ("Y-m-d", dbDateDecode($row["DeliveryDate"])), 'align=right') ;
	} else {
		listField (date ("Y-m-d", dbDateDecode($row["MaterialDate"])), 'align=right') ;
		listField (date ("Y-m-d", dbDateDecode($row["SubDeliveryDate"])), 'align=right') ;
	}
	printf ("</tr>\n") ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No ProductionOrders', 'colspan=5') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
