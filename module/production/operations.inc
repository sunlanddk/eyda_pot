<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    
    // Header
    print productionHeader ($Record) ;

    // Output Operation
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=30><p class=listhead>No</p></td>") ;
    printf ("<td class=listhead width=60><p class=listhead>Operation</p></td>") ;
    printf ("<td class=listhead width=225><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=90><p class=listhead>Sourced</p></td>") ;
    printf ("<td class=listhead align=right width=60><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead align=right width=90><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead align=right width=90><p class=listhead>Total</p></td>") ;
    printf ("<td class=listhead align=right width=90><p class=listhead>Remaining</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;
    $sum = array() ;
    foreach ($Operation as $row) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list style='cursor:default;' src='%s'></td>", './image/toolbar/operation.gif') ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['No'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['OperationNumber'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;

	if ($Record['SourceLocationId'] > 0 and $Record['SourceFromNo'] <= $row['No'] and $Record['SourceToNo'] >= $row['No']) {
	    // Operation Outsourced    
	    printf ("<td><p class=list>%s</p></td>", htmlentities($SourceLocationName)) ;
	    $row['BundleWorkQuantity'] = ($Record['SourceIn']) ? $Record['BundleQuantity'] : 0 ;
	} else {
	    printf ("<td></td>") ;
	}
	    
	$v = (float)$row['ProductionMinutes'] ;
	$sum['ProductionMinutes'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;

	printf ("<td align=right><p class=list>%d</p></td>", $row['BundleWorkQuantity']) ;
	
	$v = $Record['BundleQuantity']*$row['ProductionMinutes'] ;	
	$sum['Total'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;
	
	$v = ($Record['BundleQuantity']-$row['BundleWorkQuantity'])*$row['ProductionMinutes'] ;
	if ($v < 0) $v = 0 ;
	$sum['Remaining'] += $v ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;
	
	printf ("</tr>\n") ;
    }
    if (count($Operation) == 0) {
        printf ("<tr><td></td><td colspan=2><p class=list>No operations</p></td></tr>\n") ;
    } else {
	// Total
	printf ("<tr>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;' colspan=3></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>Total</p></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['ProductionMinutes']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $Record['BundleQuantity']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Total']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Remaining']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("</tr>\n") ;

	if (!$Record['Cut']) {
	    print htmlItemSpace () ;
	    
	    // Not Cut
	    printf ("<tr>") ;
	    printf ("<td colspan=3></td>") ;
	    printf ("<td><p class=list>Not yet Cut</p></td>") ;
	    printf ("<td></td>") ;
	    printf ("<td align=right><p class=list>%01.2f</p></td>", $sum['ProductionMinutes']) ;
	    $v = $Record['Quantity']-$Record['BundleQuantity'] ;
	    if ($v < 0) $v = 0 ;
	    $sum['Quantity'] = $Record['BundleQuantity'] + $v ;
	    printf ("<td align=right><p class=list>%d</p></td>", $v) ;
	    $v *= $sum['ProductionMinutes'] ;
	    $sum['Total'] += $v ;
	    printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;
	    $sum['Remaining'] += $v ;
	    printf ("<td align=right><p class=list>%01.2f</p></td>", $v) ;
	    printf ("</tr>\n") ;

	    // Total
	    printf ("<tr>") ;
	    printf ("<td style='border-top: 1px solid #cdcabb;' colspan=3></td>") ;
	    printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>Total</p></td>") ;
	    printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	    printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['ProductionMinutes']) ;
	    printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%d</p></td>", $sum['Quantity']) ;
	    printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Total']) ;
	    printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Remaining']) ;
	    printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	    printf ("</tr>\n") ;
	}
    }
    printf ("</table>\n") ;
    
    return 0 ;
?>
