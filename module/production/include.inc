<?php

    require_once 'lib/table.inc' ;
/*
    function htmlFlag (&$Record, $field) {
	$s = sprintf ("<tr><td class=itemfield>%s</td>", $field) ;
	if ($Record[$field]) {
	    $s .= sprintf ("<td class=itemfield>%s, %s</td>", date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    $s .= sprintf ("<td><input type=checkbox name=%s%s></td>", $field, ($Record[$field])?' checked':'') ;
	}
	$s .= "</tr>\n" ;
	return $s ;
    }
*/
    function htmlFlag (&$Record, $field, $name=NULL) {
	if (is_null($name)) $name = $field ;
	$s = sprintf ("<tr><td class=itemfield>%s</td>", $name) ;
	if ($Record[$field]) {
	    $s .= sprintf ("<td class=itemfield>%s, %s</td>", date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    $s .= sprintf ("<td><input type=checkbox name=%s%s></td>", $field, ($Record[$field])?' checked':'') ;
	}
	$s .= "</tr>\n" ;
	return $s ;
    }


    
    
    function productionHeader (&$Record) {
        $s = "<br>" ;
	$s .= "<table class=item>\n" ;
	if (isset($Record['Number'])) {
	    $s .= sprintf ("<tr><td class=itemlabel>Production</td><td width=100 class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
	} else {
	    $s .= sprintf ("<tr><td class=itemlabel>Case</td><td width=100 class=itemfield>%d</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Id']), htmlentities($Record['Description'])) ;
	}
	$s .= sprintf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ArticleNumber']), htmlentities($Record['ArticleDescription'])) ;
	$s .= "</table>\n" ;
	$s .= "<br>" ;
	return $s ;
    }   
?>
