<?php


    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
   require_once 'lib/table.inc' ;

    $fields = array (
	'ProductionLocationId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Number'		=> array ('type' => 'set'),
	'StyleId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Description'		=> array (),
	'Quantity'		=> array ('type' => 'integer',	'mandatory' => true, 	'check' => true),
	'ProductionMinutes'	=> array ('type' => 'decimal',	'format' => '6.2'),
	'MaterialDate'		=> array ('type' => 'date',	'check' => true),
	'SubDeliveryDate'	=> array ('type' => 'date',	'check' => true),
	'DeliveryDate'		=> array ('type' => 'date'),
	'MaterialDate_Act'	=> array ('type' => 'date',	'check' => true),
	'Sample'		=> array ('type' => 'checkbox'),
	'SampleText'		=> array (),
	'SampleDate'		=> array ('type' => 'date'),
	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate'		=> array ('type' => 'set'),
	'Fabrics'		=> array ('type' => 'checkbox',	'check' => true),
	'FabricsUserId'		=> array ('type' => 'set'),
	'FabricsDate'		=> array ('type' => 'set'),
	'Accessories'		=> array ('type' => 'checkbox',	'check' => true),
	'AccessoriesUserId'	=> array ('type' => 'set'),
	'AccessoriesDate'	=> array ('type' => 'set'),
	'MaterialSent'		=> array ('type' => 'checkbox',	'check' => true),
	'MaterialSentUserId'	=> array ('type' => 'set'),
	'MaterialSentDate'	=> array ('type' => 'set'),
	'AccSent'		=> array ('type' => 'checkbox',	'check' => true),
	'AccSentUserId'	=> array ('type' => 'set'),
	'AccSentDate'	=> array ('type' => 'set'),
	'Done'			=> array ('type' => 'checkbox',	'check' => true),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set'),
	'Request'			=> array ('type' => 'checkbox',	'check' => true),
	'RequestUserId'		=> array ('type' => 'set'),
	'RequestDate' 		=> array ('type' => 'set'),
	'ProductionStartDate'	=> array ('type' => 'set'),
	'ProductionEndDate'	=> array ('type' => 'set'),
	'Allocated'             => array ('type' => 'set'),
	'Efficiency'		=> array ('type' => 'integer',	'mandatory' => true),
	'Comment'		=> array ()
    ) ;

    if ($Record['POType'] == 'devel') {
	$fields['DeliveryDate']['type'] = 'set' ;
	$fields['Ready']['type'] = 'set' ;
	$fields['StyleId']['type'] = 'set' ;
	$fields['StyleId']['mandatory'] = 'false' ;
	$fields['StyleId']['check'] = 'false' ;
	$fields['ProductionMinutes']['type'] = 'set' ;
	$fields['Efficiency']['type'] = 'set' ;
	$fields['Efficiency']['mandatory'] = 'false' ;
	$fields['Sample']['type'] = 'set' ;
	$fields['SampleText']['type'] = 'set' ;
	$fields['SampleDate']['type'] = 'set' ;
    $xfields = array (
		'SeasonId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
		'PlanText'		=> array (),
		'MissingText'		=> array ()
	) ;
	$fields = array_merge ((array)$fields,(array)$xfields) ;
    }
    
    if ($Record['TypeSample']) {
      $xfields = array (
		'SeasonId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
		'PlanText'		=> array (),
		'MissingText'		=> array ()
  	  ) ;
	  $fields = array_merge ((array)$fields,(array)$xfields) ;
    }
    
    

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'ProductionLocationId' :
		if (!$changed) return false ;
		if ($value == 0) return 'no ProductionLocation selected' ;

		// Reject when allocated
		if ($Record['Allocated']) return "The ProductionLocation can not be changed after Allocation" ;

		// Validate referance to ProductionLocation
		$query = sprintf ("SELECT ProductionLocation.Id AS ProductionLocationId, ProductionLocation.Name AS ProductionLocationName, ProductionLocation.IndexStart, ProductionLocation.TypeProduction, ProductionLocation.TypeSample, ProductionLocation.DoBundles FROM ProductionLocation WHERE ProductionLocation.Id=%d AND ProductionLocation.Active=1", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['ProductionLocationId'] <= 0) return sprintf ('%s(%d) ProductionLocation not found, id %d', __FILE__, __LINE__, $value) ;

		// Save new ProductionLocation information in Record
		$Record = array_merge ($Record, $row) ;

		if ($Navigation['Parameters'] != 'new') { // Get old Production location type and check if identical
			$OldProdLocId = tableGetField ('Production', 'ProductionLocationId', $Id) ;
			$OldTypeSample = tableGetField ('ProductionLocation', 'TypeSample', $OldProdLocId) ;
		}
		if (($Navigation['Parameters'] == 'new') or ($OldTypeSample!=$Record['TypeSample'] )) {
			// Get start index
			$i = (int)$row['IndexStart'] ;
	
			// Get next index to use
			$query = sprintf ("SELECT MAX(Production.Number) AS Number FROM Production WHERE Production.CaseId=%d AND Production.Id<>%d AND Production.Active=1 AND Production.Number>=%d AND Production.Number<%d", $Record['CaseId'], $Id, $i, $i+49) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if (((int)$row['Number']) >= $i) $i = ((int)$row['Number']) + 1 ;
			$fields['Number']['value'] = $i ;
	    } 
		
		return true ;
		
	    case 'SeasonId' :
		if (!$changed) return false ;
		if ($value == 0) return 'no Project selected' ;

		// Save new ProductionLocation information in Record
		$Record['SeasonId'] = $value ;

		return true ;

	    case 'StyleId' :
		if (!$changed) return false ;

	      if ($Record['POType'] == 'devel')  return false ;

		// Reject when ready
		if ($Record['Approved']) return "Style version can not be changed when the ProductionOrder is Approved" ;
		
		// Check if any bundles made
		if ($Id > 0) {
		    $query = sprintf ("SELECT Id FROM Bundle WHERE ProductionId=%d AND Active=1", $Id) ;
		    $result = dbQuery ($query) ;
		    $count = dbNumRows ($result) ;
		    dbQueryFree ($result) ;
		    if ($count > 0) return "Style version can not be changed when Bundles has been made" ;
		}

		// Validate referance to Style and Article
		$query = sprintf ("SELECT Style.ArticleId FROM Style WHERE Style.Id=%d AND Style.Active=1", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if (((int)$Record['ArticleId']) != ((int)$row['ArticleId'])) return sprintf ("%s(%d) invalid style referance, id %d", __FILE__, __LINE__, $value) ;
		
		return true ;

	    case 'Quantity':
		if (!$changed) return false ;
		if ($value == 0) return 'no Quantity specified' ;

		// Reject when allocated
		if ($Record['Allocated']) return "The Quantity can not be changed after Allocation" ;
		
		return true ;

	    case 'Ready':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

	      if ($Record['POType'] == 'devel')  return false ;
		
		// Check for Allocation
		// if (!$Record['Allocated']) return "The ProductionOrder has to be Allocated before it can be set Ready" ;	

		// Set tracking information
		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Request':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;
		
		// Set tracking information
		$fields['RequestUserId']['value'] = $User['Id'] ;
		$fields['RequestDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Fabrics':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;
		
		// Set tracking information
		$fields['FabricsUserId']['value'] = $User['Id'] ;
		$fields['FabricsDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Accessories':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;
		
		// Set tracking information
		$fields['AccessoriesUserId']['value'] = $User['Id'] ;
		$fields['AccessoriesDate']['value'] = dbDateEncode(time()) ;
		return true ;

		// Request Fabric
	    case 'MaterialSent':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;
		
		// Validate
//		if (!$Record['Fabrics']) return 'Fabrics has to be ordered before Fabric Sent' ;

		// Set tracking information
		$fields['MaterialSentUserId']['value'] = $User['Id'] ;
		$fields['MaterialSentDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    //Request Acc.
	    case 'AccSent':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

//		if (!$Record['Accessories']) return 'Accessories has to be ordered before Accessories Sent' ;
		
		// Set tracking information
		$fields['AccSentUserId']['value'] = $User['Id'] ;
		$fields['AccSentDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Done':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Set tracking information
		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;

		if ($Record['POType'] == 'devel') return true ;

		// Check for Allocation
		if (!$Record['Packed']) return "the ProductionOrder has to be Packed before it can be set Done" ;	

		// Check that Style is ready
	    $HasStyle = tableGetField ('Article', 'HasStyle', $Record['ArticleId']) ;
		// Added 2007-02-22: if HasStyle dont make this check with new style module
		if (!$HasStyle) {
			if (!$Record['StyleReady'])
			if (dbDateDecode($Record['CreateDate']) > dbDateDecode('2005-02-25 00:00:00')) 
				return "the Style has to be approved by Constructor before the ProductionOrder can be set Done" ;	
		}
		// Set tracking information
		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'MaterialDate':
		if (!$changed) return false ;
	      if ($Record['Allocated']) { //return 'Material Date can not be changed when Allocated' ;
	    		$fields['Allocated']['value'] = 0;
			unset ($row) ;
			$row['ChannelId'] = 3 ;
			$row['Header'] = sprintf ("Material date in ProductionOrder %s changed from %s to %s", $Record['Number'],date('Y-m-d', dbDateDecode($Record['MaterialDate'])), date('Y-m-d', dbDateDecode($value)) ) ;
			$row['Text'] = sprintf ("%s (%s) has at %s changed the material date from %s to %s in the already allocated production order %s, 
						and as a consequence the allocation flag is reset", $User['FullName'], $User['Loginname'], date('Y-m-d H:i:s', time()),date('Y-m-d', dbDateDecode($Record['MaterialDate'])), date('Y-m-d', dbDateDecode($value)), $Record['Number']) ;
			tableWrite ('News', $row) ;
		}
	      if ($Record['POType'] == 'devel') {
		  $fields['ProductionStartDate']['value'] = $value ;
		} else {
		  if ($Id <= 0) $fields['ProductionStartDate']['value'] = $value ;
		}
		return true ;

	case 'MaterialDate_Act': return false;

      case 'SubDeliveryDate':
	    if (!$changed) return false ;

	    if ($Record['POType'] == 'devel') {
		 $fields['DeliveryDate']['value'] = $value ;
		 $fields['ProductionEndDate']['value'] = $value ;
	    } else {
    		if ($Record['Allocated']) return 'SubDelivery Date can not be changed when Allocated' ;
		if ($Id <= 0) $fields['ProductionEndDate']['value'] = $value ;
	    }
	    return true ;
	}
	return false ;
    }

    function saveValidate ($changed) {
	global $User, $Record, $Id, $fields ;

	// Only validate if changed
	if (!$changed) return false ;

	// Validate time
	if ($Record['TypeSample'] == 0) {
	    $query = sprintf ("SELECT currentstyleoperation.StyleId, SUM(currentstyleoperation.ProductionMinutes) AS ProductionMinutes FROM currentstyleoperation WHERE currentstyleoperation.StyleId=%d AND currentstyleoperation.Active=1 GROUP BY currentstyleoperation.StyleId", $Record['StyleId']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if ($row['ProductionMinutes'] > $Record['ProductionMinutes']) return sprintf ("ProductionTime in Style (%01.2f) exceeds the specified ProductionTime (%01.2f)", $row['ProductionMinutes'], $Record['ProductionMinutes']) ;
	}
	
	// Validate count
	if ($Record['Ready']) {
	    $query = sprintf ("SELECT ProductionQuantity.ProductionId AS Id, SUM(ProductionQuantity.Quantity) AS Quantity FROM ProductionQuantity WHERE ProductionQuantity.ProductionId=%d AND ProductionQuantity.Active=1 GROUP BY ProductionQuantity.ProductionId", $Id) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if (((int)$Record['Quantity']) != ((int)$row['Quantity'])) return sprintf ("Quantity in Production Order (%d) do not match detailed specified quantities (%d)", $Record['Quantity'], $row['Quantity']) ;
	}

	// Validate dates
    if ($Record['POType'] == 'devel') {
		if (dbDateDecode($Record['SubDeliveryDate']) < dbDateDecode ($Record['MaterialDate'])) return "Approval date is before Shipping Date" ;
    } else {
		//	if (dbDateDecode($Record['MaterialDate']) > dbDateDecode ($Record['SubDeliveryDate'])) return "SubDelivery date is before Material date" ;
		//	if (dbDateDecode($Record['SubDeliveryDate']) > dbDateDecode ($Record['DeliveryDate'])) return "Delivery date is before SubDelivery date" . ;
			if (dbDateDecode($Record['SubDeliveryDate']) > dbDateDecode ($Record['DeliveryDate'])) return "Delivery date is before SubDelivery date" . $Record['DeliveryDate'] . " < " . $Record['SubDeliveryDate'] ;
		//	if (dbDateDecode($Record['SubDeliveryDate_Act']) > dbDateDecode ($Record['DeliveryDate_Act'])) return "Actual Delivery date is before Actual SubDelivery date" ;
		//	if (dbDateDecode($Record['MaterialDate']) > dbDateDecode ($Record['ProductionStartDate'])) return "Material date is after Production Start date" ;
			if (dbDateDecode($Record['SubDeliveryDate']) < dbDateDecode ($Record['ProductionEnd'])) return "SubDelivery date is before ProductionEnd date" ;
		//	if (dbDateDecode($Record['SubDeliveryDate_Act']) < dbDateDecode ($Record['MaterialDate_Act'])) return "Actual SubDelivery date is before Actual Material date" ;
	}	
	return true ;
    }

    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['CaseId'] = array ('type' => 'set', 'value' => $Record['CaseId']) ;
	    $Id = -1 ;
	    break ;
    }

    if ($Record['Done']) return "The ProductionOrder is Done and can not be modified" ;
     
    $res = saveFields ('Production', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Production
	    return navigationCommandMark ('productionview', $Record['Id']) ;
    }

    return 0 ;    
?>
