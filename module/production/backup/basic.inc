<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Header
    print productionHeader ($Record) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Get Id for default Style
	    $query = sprintf ("SELECT Style.Id AS StyleId, Style.ArticleId FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 ORDER BY Style.Approved DESC, Style.Version DESC LIMIT 1", $Record['ArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch($result) ;	    
	    dbQueryFree ($result) ;
	    
	    // Default values
	    $Record['MaterialDate'] = dbDateEncode(time()+7*24*60*60) ;
	    $Record['SampleDate'] = dbDateEncode(time()+10*24*60*60) ;
	    $Record['SubDeliveryDate'] = dbDateEncode(time()+14*24*60*60) ;
	    $Record['DeliveryDate'] = dbDateEncode(time()+21*24*60*60) ;
	    $Record['Quantity'] = 0 ;
	    $Record['Efficiency'] = 100 ;
	    $Record['ProductionMinutes'] = 0 ;
	    break ;
    }
 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Location', formDBSelect ('ProductionLocationId', (int)$Record['ProductionLocationId'], 'SELECT Id, Name AS Value FROM ProductionLocation WHERE TypeProduction=1 AND Active=1 ORDER BY Name', 'width:150px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Style version', formDBSelect ('StyleId', (int)$Record['StyleId'], sprintf ('SELECT Id, Version AS Value FROM Style WHERE ArticleId=%d AND Active=1 ORDER BY Version', $Record['ArticleId']), 'width:50px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Quantity', formText ('Quantity', $Record['Quantity'], 5)) ;
    itemFieldRaw ('Minutes', formText ('ProductionMinutes', $Record['ProductionMinutes'], 7)) ;
    itemFieldRaw ('Efficiency (%)', formText ('Efficiency', $Record['Efficiency'], 5)) ; 
    itemSpace () ;
	itemFieldRaw ('Base Material', formDate('MaterialDate', dbDateDecode($Record["MaterialDate"]), null, 'tR')) ;
    itemFieldRaw ('Base SubDeliv', formDate('SubDeliveryDate', dbDateDecode($Record["SubDeliveryDate"]), null, 'tR')) ;
    itemFieldRaw ('Base Delivery', formDate('DeliveryDate', dbDateDecode($Record["DeliveryDate"]), null, 'tR')) ;
    itemSpace () ;
	itemFieldRaw ('Act Material', formDate('MaterialDate_Act', dbDateDecode($Record["MaterialDate_Act"]), null, 'tR')) ;
	itemFieldRaw ('Act SubDeliv.', formDate('SubDeliveryDate_Act', dbDateDecode($Record["SubDeliveryDate_Act"]), null, 'tR')) ;
	itemFieldRaw ('Act Delivery', formDate('DeliveryDate_Act', dbDateDecode($Record["DeliveryDate_Act"]), null, 'tR')) ;
    itemSpace () ;
    itemFieldRaw ('Sample', formCheckbox ('Sample', $Record['Sample'])) ;
    itemFieldRaw ('SampleDate', formDate('SampleDate', dbDateDecode($Record["SampleDate"]), null, 'tR')) ;
    itemFieldRaw ('SampleText', formText ('SampleText', $Record['SampleText'], 100, 'width:100%;')) ;
    if ($Record['Id'] > 0) {
	itemSpace () ;
	print htmlFlag ($Record, 'Ready') ;
	print htmlFlag ($Record, 'Fabrics') ;
	print htmlFlag ($Record, 'Accessories') ;
	print htmlFlag ($Record, 'MaterialSent', 'Fabric sent') ;
	print htmlFlag ($Record, 'AccSent', 'Acc. sent') ;
	print htmlFlag ($Record, 'Done') ;
    }	
    itemSpace () ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:150px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;
    
    return 0 ;
?>
