<?php


    require 'lib/save.inc' ;
    require 'lib/parameter.inc' ;

    $fields = array (
	'PlanText'		=> array (),
	'MissingText'		=> array (),
	'BundleText'		=> array (),
	'Allocated'		=> array ('type' => 'checkbox',	'check' => true),
	'AllocatedUserId'	=> array ('type' => 'set'),
	'AllocatedDate' 	=> array ('type' => 'set'),
	'ProdDocRec'			=> array ('type' => 'checkbox',	'check' => true),
	'ProdDocRecUserId'		=> array ('type' => 'set'),
	'ProdDocRecDate' 		=> array ('type' => 'set'),
	'Cut'			=> array ('type' => 'checkbox',	'check' => true),
	'CutUserId'		=> array ('type' => 'set'),
	'CutDate' 		=> array ('type' => 'set'),
	'Packed'		=> array ('type' => 'checkbox',	'check' => true),
	'PackedUserId'		=> array ('type' => 'set'),
	'PackedDate' 		=> array ('type' => 'set'),
	'ProductionStartDate'	=> array ('type' => 'date'),
	'ProductionEndDate'	=> array ('type' => 'date'),
//	'SubDeliveryDate'	=> array ('type' => 'date'),
	'MaterialDate_Act'	=> array ('type' => 'set'),
	'SubDeliveryDate_Act'	=> array ('type' => 'set'),
	'DeliveryDate_Act'	=> array ('type' => 'set'),
	'ProductionStartDate_Act'	=> array ('type' => 'set'),
	'ProductionEndDate_Act'	=> array ('type' => 'set')
    ) ;
    if ($Record['POType'] == 'devel') {
		$fields['DeliveryDate']['type'] = 'set' ;
		$fields['ProductionStartDate']['type'] = 'set' ;
		$fields['ProductionEndDate']['type'] = 'set' ;
	     $xfields = array (
			'Ready'		=> array ('type' => 'checkbox',	'check' => true),
			'ReadyUserId'	=> array ('type' => 'set'),
			'ReadyDate'		=> array ('type' => 'set'),
		) ;
		$fields = array_merge ((array)$fields,(array)$xfields) ;
    }
    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Ready':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'ProdDocRec':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		$fields['ProdDocRecUserId']['value'] = $User['Id'] ;
		$fields['ProdDocRecDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Allocated':
		// Ignore if not setting
		if (!$changed or !$value) return false ;
		
		if (!$Record['Request']) return "The ProductionOrder must be Requested before Allocating" ;

		// Set tracking information
		$fields['AllocatedUserId']['value'] = $User['Id'] ;
		$fields['AllocatedDate']['value'] = dbDateEncode(time()) ;
	      if (($Record['MaterialDate_Act'])< '2001-01-01') {
			$fields['MaterialDate_Act']['value'] = $Record['MaterialDate'] ;
			$fields['SubDeliveryDate_Act']['value'] = $Record['SubDeliveryDate'] ;
			$fields['DeliveryDate_Act']['value'] = $Record['DeliveryDate'] ;
	      	$fields['ProductionStartDate_Act']['value'] = $_POST['ProductionStartDate'] ;
			$fields['ProductionEndDate_Act']['value'] = $_POST['ProductionEndDate'] ;
		}
		return true ;

	    case 'Cut':
		// Ignore if not setting
		if (!$changed or !$value) return false ;

		// Validate Approval
		if (!$Record['Approved']) return "The ProductionOrder must be Approved before finishing Cutting" ;

		// Validate that some bundles has been made
		if ($Record['DoBundles']) {
		    $query = sprintf ('SELECT COUNT(Id) AS Count FROM Bundle WHERE ProductionId=%d AND Active=1', $Id) ;
		    $result = dbQuery ($query) ;
		    $row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    if ($row['Count'] == 0) return ('you can not set cut-flag when no bundles has been made') ;
		} else {

			// Make "In Production" items
			// First Get production quantities
			$query = sprintf ("SELECT ProductionQuantity.*
								FROM ProductionQuantity
								WHERE ProductionQuantity.ProductionId=%d AND ProductionQuantity.Active=1 
								GROUP BY ProductionQuantity.Id", $Id) ;
			$pqresult = dbQuery ($query) ;

			$InProdContainer = (int)ParameterGet ('ProdContainerId') ;
			// Run though ordered quantity and create items "in production"
			while ($row = dbFetch ($pqresult)) {
				// Get allready created items
				$query = sprintf ("SELECT * FROM Item 
									WHERE ArticleColorId=%d AND ArticleSizeId=%d AND ContainerId=%d AND FromProductionId=%d AND Active=1", 
									 $row['ArticleColorId'], $row['ArticleSizeId'], $InProdContainer, $Id) ;
				$result = dbQuery ($query) ;
				$itemrow = dbFetch ($result) ;
				dbQueryFree ($result) ;

				if ($itemrow['Id'] <= 0) {
					// Insert new item into production container and stock
					$item['Active'] = 1 ;
					$item['Sortation'] = 0 ;
					$item['ContainerId'] = $InProdContainer ;
					// From row
					$item['FromProductionId'] = $Id ;
					$item['ArticleColorId'] = $row['ArticleColorId'] ;
	    			$item['ArticleSizeId'] = $row['ArticleSizeId'] ;
					$item['Quantity'] = $row['Quantity'] ;
					// From production record
					$item['StyleId'] = $Record['StyleId'] ;
					$item['CaseId'] = 0 ;
					$item['ArticleId'] = $Record['ArticleId'] ;
					$item['ArticleCertificateId'] = $Record['ArticleCertificateId'] ;
					tableWrite ('Item', $item) ;
				} else {
					// Update item: add quantity
					$query = sprintf ("UPDATE Item SET Quantity=%d WHERE Id=%d", $row['Quantity'], $itemrow['Id']) ;
					$tmp = dbQuery ($query) ;
				}
			}
			dbQueryFree ($pqresult) ;
		}
		    
		// Set tracking information
		$fields['CutUserId']['value'] = $User['Id'] ;
		$fields['CutDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Packed':
		// Ignore if not setting
		if (!$changed or !$value) return false ;

	      if ($Record['POType'] <> 'devel') {

		// Validate allocated
		if (!$Record['Allocated']) return "the ProductionOrder must be Allocated before Packed" ;

		// Validate ready
		if (!$Record['Ready']) return "the ProductionOrder must be Ready before Packed" ;

		if (!$Record['AccSent']) return "Accessories has to be sent before Packed" ;
		
		if (!$Record['TypeSample']) {
		    // Production ProductionOrder (not Sample)
		    // Validate Cut
		    if ($Record['DoBundles'] and !$Record['Cut']) return "the ProductionOrder must be Cut before Packed" ;

		    // Validate sourcing
		    if ($Record['SourceOut'] and !$Record['SourceIn']) return 'Sourcing must be ended before Packing' ; 
		    // if (!$Record['SourceIn']) return 'Sourcing must be ended before Packing' ; 
		}
		
		// Validate all items is packed
		$query = sprintf ("SELECT sum(Quantity) as SumQty FROM Item WHERE ContainerId=165413 AND FromProductionId=%d AND Active=1 GROUP BY FromProductionId", $Id) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['SumQty']>0) return 'All items not yet packed'  ;
		}
		// Set tracking information
		$fields['PackedUserId']['value'] = $User['Id'] ;
		$fields['PackedDate']['value'] = dbDateEncode(time()) ;
		return true ;
		
 	  case 'ProductionStartDate_Act': return false;
		  if(!$changed) return false;
		  
	      if ((dbDateDecode($_POST['ProductionEndDate_Act'])>0)) {
	      	return true;
	      } else {
	      	return 'Complete schedule incl. Production end date should be set before saving' ;
	      }

 	  case 'ProductionEndDate_Act': return false;
		  if(!$changed) return false;
		  
	      if ((dbDateDecode($_POST['ProductionStartDate_Act'])>0)) {
	      	return true;
	      } else {
	      	return 'Complete schedule incl. Production start date should be set before saving' ;
	      }

	}
	return false ;
    }

    function saveValidate ($changed) {
	global $User, $Record, $Id, $fields ;

	// Only validate if changed
	if (!$changed) return false ;

	// Validate state
    	if ($Record['Done']) return "No modifications to a Done Production Order" ;
	
	// Validate dates

    if ($Record['POType'] <> 'devel') {
	if (dbDateDecode($Record['MaterialDate']) > dbDateDecode ($Record['ProductionStartDate'])) return "ProductionStart is before Material" ;
	if (dbDateDecode($Record['SubDeliveryDate']) > dbDateDecode ($Record['DeliveryDate'])) return "Delivery date is before SubDelivery date" ;

//	if (dbDateDecode($Record['MaterialDate_Act']) > dbDateDecode ($Record['ProductionStartDate_Act'])) return "Act. ProductionStart is before Act. Material" ;

	// Only check ProductionDates if allocated
	if ($Record['Allocated']) {
		if (dbDateDecode($Record['ProductionStartDate']) > dbDateDecode ($Record['ProductionEndDate'])) return "ProductionEnd is before ProductionStart" ;
		if (dbDateDecode($Record['ProductionEndDate']) > dbDateDecode ($Record['SubDeliveryDate'])) return "ProductionEnd is after SubDelivery" ;
//		if (dbDateDecode($Record['ProductionStartDate_Act']) > dbDateDecode ($Record['ProductionEndDate_Act'])) return "Act. ProductionEnd is before act. ProductionStart" ;
//		if (dbDateDecode($Record['ProductionEndDate_Act']) > dbDateDecode ($Record['SubDeliveryDate_Act'])) return "Act. ProductionEnd is after act. SubDelivery" ;
	}
    }
	return true ;
    }

    // No cutting for samples
    if ($Record['TypeSample']) {
	unset ($fields['Cut']) ;
    }

    return saveFields ('Production', $Id, $fields, true) ;
?>
