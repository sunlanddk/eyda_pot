<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    
    require_once 'lib/log.inc' ;
    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    $fields = array (
	'ProductionLocationId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Sample'		=> array ('type' => 'checkbox'),
	'SampleText'		=> array (),
	'SampleDate'		=> array ('type' => 'date'),
	'Comment'		=> array (),
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'ProductionLocationId' :
		if ($value == 0) return 'no ProductionLocation selected' ;

		// Validate referance to ProductionLocation
		$query = sprintf ("SELECT ProductionLocation.Id AS ProductionLocationId, ProductionLocation.Name AS ProductionLocationName, ProductionLocation.IndexStart, ProductionLocation.TypeProduction, ProductionLocation.TypeSample, ProductionLocation.DoBundles FROM ProductionLocation WHERE ProductionLocation.Id=%d AND ProductionLocation.Active=1", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['ProductionLocationId'] <= 0) return sprintf ('%s(%d) ProductionLocation not found, id %d', __FILE__, __LINE__, $value) ;

		// Save new ProductionLocation information in Record
		$Record = array_merge ($Record, $row) ;

		break ;
	}
	return true ;
    }
if (!$Preview) {
    // Validate Order
    //if (!$Record['Ready']) return 'The Order must be Ready to generate ProductionOrders' ;
    if ($Navigation['Parameters'] == '') 
	    if ($Record['Done']) return 'The Order can not be Done when generating ProductionOrders' ;
    
    // Process parameters
 
    $res = saveFields (NULL, NULL, $fields, true) ;
    if ($res) return $res ;
}

    set_time_limit (120) ;
    // Get OrderLines structured into Productions
    switch ($Navigation['Parameters']) {
    case 'season':
    case 'seasonwithsizes':
	    $query = sprintf ("
		SELECT OrderLine.*, OrderQuantity.ArticleSizeId, SUM(OrderQuantity.Quantity) AS PartQuantity, o.Description as OrderDescription
		FROM (OrderLine, `Order` o) 
		INNER JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.Id AND OrderQuantity.Active=1 
		WHERE OrderLine.OrderId=o.id AND o.seasonid=%d AND o.active=1 AND OrderLine.Active=1 AND OrderLine.CaseId>0 AND (OrderLine.ProductionId IS NULL or OrderLine.ProductionId=0)
		GROUP BY OrderLine.DeliveryDate, OrderLine.CaseId, OrderLine.ArticleColorId, OrderQuantity.ArticleSizeId
		ORDER BY OrderLine.DeliveryDate, OrderLine.CaseId, OrderLine.ArticleColorId, OrderQuantity.ArticleSizeId", $Id) ;
	    break;
	
    default :
	    $query = sprintf ("
		SELECT OrderLine.*, OrderQuantity.ArticleSizeId, SUM(OrderQuantity.Quantity) AS PartQuantity 
		FROM OrderLine 
		INNER JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.Id AND OrderQuantity.Active=1 
		WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 AND OrderLine.CaseId>0 AND (OrderLine.ProductionId IS NULL or OrderLine.ProductionId=0)
		GROUP BY OrderLine.DeliveryDate, OrderLine.CaseId, OrderLine.ArticleColorId, OrderQuantity.ArticleSizeId
		ORDER BY OrderLine.DeliveryDate, OrderLine.CaseId, OrderLine.ArticleColorId, OrderQuantity.ArticleSizeId", $Record['Id']) ;
	    break;
	}
	$result = dbQuery ($query) ;
	$ProductionList = array () ;
	$Production = array () ;
//$rows=1;
	while ($row = dbFetch ($result)) {
//if($Preview) {
//  printf("row %d", $rows ); $rows++ ;
//}	
	$DeliveryDate = dbDateDecode ($row['DeliveryDate']) ;
	$CaseId = (int)$row['CaseId'] ;
	if ($Production['DeliveryDate'] != $DeliveryDate or $Production['CaseId'] != $CaseId) {
	    switch ($Navigation['Parameters']) {
		    case 'season':
		    case 'seasonwithsizes':
			$Description = $row['OrderDescription'] ;
			break ;
		    default:
			$Description = $Record['Description'] ;
			break ;
	    }
	    // New Production
	    $ProductionList[] = array (
		'CaseId' => $CaseId,
		'DeliveryDate' => $DeliveryDate,
		'Description' => $Description,
		'Quantity' => 0,
		'Part' => array ()
	    ) ;
	    
	    $Production = &$ProductionList[count($ProductionList)-1] ;
	    $Surplus = (int)$row['Surplus'] ;
	}

	// Get Quantity for this Color and Size
	$Quantity = (int)(round(((float)$row['PartQuantity']*(100+$Surplus))/100)) ;
	
	// Set Quantities
	$Production['Part'][] = array (
	    'ArticleSizeId' => (int)$row['ArticleSizeId'],
	    'ArticleColorId' => (int)$row['ArticleColorId'],
	    'Quantity' => $Quantity
	) ;

	// Update total Quantity for Production
	$Production['Quantity'] += $Quantity ;
    }
    dbQueryFree ($result) ;
//if($Preview) {
//  printf("gryf %d", count($ProductionList) );
//}

    // Load and calculate additional information
    for ($i = 0 ; $i < count($ProductionList) ; $i++) {
	$Production = &$ProductionList[$i] ;

	// Find style
	// Either approved or just highest version number
	$query = sprintf ('SELECT Style.Id, Style.Version, Style.Approved FROM `Case` INNER JOIN Style ON Style.ArticleId=`Case`.ArticleId AND Style.Active=1 AND Style.Canceled=0 WHERE `Case`.Id=%d', $Production['CaseId']) ;
	$result = dbQuery ($query) ;
	$StyleId = 0 ;
	while ($row = dbFetch ($result)) {
	    $StyleId = (int)$row['Id'] ;
	    if ($row['Approved']) break ;
	}
	dbQueryFree ($result) ;
	if ($StyleId <= 0) return sprintf ('no style found, CaseId %d', $Production['CaseId']) ;
	$Production['StyleId'] = $StyleId ;

	// Find ProductionMinutes for Style and add 10%
	$query = sprintf ('SELECT SUM(currentstyleoperation.ProductionMinutes) AS ProductionMinutes FROM currentstyleoperation WHERE currentstyleoperation.StyleId=%d AND currentstyleoperation.Active=1 GROUP BY currentstyleoperation.StyleId', $StyleId) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	$Production['ProductionMinutes'] = (float)$row['ProductionMinutes'] ;	

	// User supplied information
	foreach (array('ProductionLocationId','Sample','SampleText','SampleDate','Comment') as $field) {
	    $Production[$field] = $fields[$field]['value'] ;
	}
	
	// Compute dates
	$now = utsDateOnly(time ()) ;
	if ($now > $Production['DeliveryDate']) $now = $Production['DeliveryDate'] ;

	$Production['MaterialDate'] = utsDateOnly ($now + ($Production['DeliveryDate'] - $now) * 0.3) ;
	if ($Prduction['MaterialDate'] - $now > 60*60*24*14) $Production['MaterialDate'] = $now + 60*60*24*14 ; 

	$Production['SubDeliveryDate'] = utsDateOnly ($now + ($Production['DeliveryDate'] - $now) * 0.9) ;
	if ($Prduction['DeliveryDate'] - $Production['SubDeliveryDate'] > 60*60*24*5) $Production['SubDeliveryDate'] = $Prduction['DeliveryDate'] - 60*60*24*5 ; 

	$Production['ProductionStartDate'] = $Production['MaterialDate'] ;

	$Production['ProductionEndDate'] = $Production['SubDeliveryDate'] ;

	// Convert Dates into DataBase format
	foreach (array('MaterialDate','ProductionStartDate','ProductionEndDate','SubDeliveryDate','DeliveryDate') as $field) {
	    $Production[$field] = dbDateOnlyEncode ($Production[$field]) ;
	}
	if (is_null($Production['SampleDate']) || $Production['SampleDate'] === '0000-00-00') 
   		$Production['SampleDate'] = $Production['SubDeliveryDate'] ;
    }

//logPrintVar ($ProductionList, 'Production') ;
if($Preview) {
  printf("Generate New Production Orders: <br>");
}

// fetch  productions to be updated later
    switch ($Navigation['Parameters']) {
    case 'season':
    case 'seasonwithsizes':
		$query =sprintf('SELECT DISTINCT ProductionId AS Id FROM (OrderLine, `Order` o)
		INNER JOIN Production ON Production.Id = OrderLine.ProductionId WHERE o.active=1 AND OrderLine.Active=1 AND Orderline.OrderId=o.id AND o.seasonid=%d AND Production.Ready=0 AND Production.Active=1', $Id);
	    break;
	
    default :
		$query =sprintf('SELECT DISTINCT ProductionId AS Id FROM OrderLine 
		INNER JOIN Production ON Production.Id = OrderLine.ProductionId WHERE OrderId = %d AND Production.Ready=0 AND Production.Active=1', $Record['Id']);
	    break;
	}
	$UpdateList= dbRead($query);

    // Create ProductionOrders
  for ($i = 0 ; $i < count($ProductionList) ; $i++) {
	$Production = &$ProductionList[$i] ;

	// Get next index to use
	$n = (int)$Record['IndexStart'] ;
	$query = sprintf ('SELECT MAX(Production.Number) AS Number FROM Production WHERE Production.CaseId=%d AND Production.Active=1 AND Production.Number>=%d AND Production.Number<%d', $Production['CaseId'], $n, $n+49) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if (((int)$row['Number']) >= $n) $n = ((int)$row['Number']) + 1 ;
	$Production['Number'] = $n ;
	if($Preview) {
	  printf("Case: %d, Quantity: %d <br>", $Production['CaseId'], 
		  $Production['Quantity']);
	} else {
	  $ProductionId = tableWrite ('Production', $Production) ;
//logPrintVar ($Production, 'Production') ;


	//Attach orderline links
	    switch ($Navigation['Parameters']) {
    case 'season':
    case 'seasonwithsizes':
		$query =sprintf('SELECT OrderLine.Id as Id, o.Id as OrderId, OrderLine.No, OrderLine.DeliveryDate FROM OrderLine, `Order` o
			WHERE orderline.OrderId = o.id AND o.seasonid=%d AND (ProductionId IS NULL or ProductionId=0) AND CaseId = %d AND DeliveryDate=\'%s\' AND o.active=1 AND OrderLine.Active=1', 
			$Id, $Production['CaseId'], $Production['DeliveryDate']);
		$result = dbQuery($query);
		while ($row = dbFetch ($result)) {
			$OrderLineRow['ProductionId'] = $ProductionId  ;
			tableWrite('OrderLine',$OrderLineRow, $row['Id']) ;
			printf("  Order: %d, Line: %d with delivery \'%s\' <br>", $row['OrderId'], $row['No'], $row['DeliveryDate']);
		}
		dbQueryFree ($result) ;
	    break;
	
    default :
		$query =sprintf('UPDATE OrderLine SET ProductionId=%d 
			WHERE OrderId = %d AND (ProductionId IS NULL or ProductionId=0) AND CaseId = %d AND DeliveryDate=\'%s\' AND Active=1', 
			$ProductionId, $Record['Id'], $Production['CaseId'], $Production['DeliveryDate']);
		dbQuery($query);
		break ;
	}	
	// Generate Quantities for each combination of Color and Size	  
	  if (($Navigation['Parameters'] == 'withsizes') or ($Navigation['Parameters'] == 'seasonwithsizes')) {
	   $nocolors = dbRead(sprintf('SELECT count(*) as cnt FROM OrderLine WHERE ProductionId = %s AND ArticleColorId = 0 AND Active = 1', $ProductionId));
	   if ($nocolors[0]['cnt'] == 0) {	
	    foreach ($Production['Part'] as $Part) {
	      $Part['ProductionId'] = $ProductionId ;
	      tableWrite ('ProductionQuantity', $Part) ;

//logPrintVar ($Part, 'Part') ;
	    }
	    }
	  }
	}
    }
// update production size breakdown for exixting productions
if (($Navigation['Parameters'] != 'withsizes' and $Navigation['Parameters'] != 'seasonwithsizes') && !$Preview) {
  return 0;
}

if($Preview) {
  print("Updating Quantities:<br>");
}
foreach ($UpdateList as $prod) {
  $nobundle = dbRead(sprintf('SELECT count(*) as cnt FROM bundle WHERE ProductionId = %s AND Active = 1', $prod['Id']));
  if($Preview) {
    if ($nobundle[0]['cnt'] > 0)
	    printf("WARNING!<br>NO UPDATE for Production: %d / %d since bundles created <br>",tableGetField('Production','CaseId',$prod['Id']),tableGetField('Production','Number',$prod['Id']));
    else
	    printf("Production: %d / %d <br>",tableGetField('Production','CaseId',$prod['Id']),tableGetField('Production','Number',$prod['Id']));
  } else {
    if ($nobundle[0]['cnt'] > 0) continue ;
   unset ($nocolors);
   $nocolors = dbRead(sprintf('SELECT count(*) as cnt FROM OrderLine WHERE ProductionId = %s AND ArticleColorId = 0 AND Active = 1', $prod['Id']));
   if ($nocolors[0]['cnt'] == 0) {
    	$Consumption=tableGetFieldWhere('ProductionQuantity','Consumption',sprintf('ProductionId=%d', $prod['Id'])) ;
	dbQuery(sprintf("DELETE FROM ProductionQuantity WHERE ProductionId = %d", $prod['Id']));
    
    dbQuery(sprintf("INSERT INTO ProductionQuantity (ProductionId, Quantity, ArticleColorId, ArticleSizeId, Consumption)
SELECT o.ProductionId, round((SUM(q.Quantity) *(100 + o.Surplus))/100) AS Quantity, o.ArticleColorId, q.ArticleSizeId, %f as Consumption
FROM OrderLine o INNER JOIN OrderQuantity q ON o.Id = q.OrderLineId and q.active=1
WHERE o.ProductionId = %d and o.active=1
GROUP BY o.ProductionId, o.ArticleColorId, q.ArticleSizeId ",$Consumption, $prod['Id']));
    dbQuery(sprintf("UPDATE Production SET Quantity = (SELECT SUM(Quantity) AS q FROM ProductionQuantity WHERE
ProductionId = %d) WHERE Id = %d", $prod['Id'], $prod['Id']));
  }
  }
}
    return 0 ;

?>














