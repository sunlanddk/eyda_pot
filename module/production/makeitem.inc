<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    
    require_once 'module/production/include.inc' ;
    
    // Header
    print productionHeader ($Record) ;

    if (count($Size) == 0) {
	printf ("<p>No Sizes assigned</p>\n") ;
	return 0 ;
    }
    if (count($Color) == 0) {
	printf ("<p>No Colors assigned</p>\n") ;
	return 0 ;
    }

    if (!$Record['Allocated']) {
	printf ("<p>The ProductionOrder has not been Allocated</p><br>\n") ;
	return 0 ;
    }
    if (!$Record['Ready']) {
	printf ("<p>The ProductioOrder has not been set Ready</p><br>\n") ;
	return 0 ;
    }
    if (!$Record['Approved'] and !$Record['TypeSample']) {
	printf ("<p>The ProductionOrder has not been Approved</p><br>\n") ;
	return 0 ;
    }
    if ($Record['Packed']) {
	printf ("<p>The Production order has allready been Packed</p><br>\n") ;
	return 0 ;
    }
    
    // Form
    formStart () ;
    
    // Container
    itemStart () ;
    itemHeader() ;
    switch ($Navigation['Parameters']) {
	case 'stock' :
	    itemFieldRaw ('Stock', formDBSelect ('StockId', 0, 'SELECT Id, Name AS Value FROM Stock WHERE Active=1 AND Type="fixed" ORDER BY Value', 'width:200px;')) ;
	    itemFieldRaw ('Type', formDBSelect ('ContainerTypeId', 0, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:150px;')) ;
	    itemFieldRaw ('Gross Weight', formText ('GrossWeight', '', 10) . ' kg') ;
	    break ;

	case 'container' :
	    itemFieldRaw ('Container', formText ('ContainerId', '', 8)) ; 
	    break ;
    }
    itemFieldRaw ('Sortation', formText ('Sortation', 1, 1)) ; 
    itemSpace() ;
    itemEnd () ;
    listStart () ;

    // Headline
    listRow () ;
    listHeadIcon () ;
    listHead ('Color/Size', 150) ;
    foreach ($Size as $s) listHead ($s, 55, 'align=right') ;
    listHead () ;
    
    // Quantitys
    foreach ($Color as $cid => $c) {
	listRow () ;
	if ((int)$c['ColorGroupId'] > 0) {
	    listFieldRaw (sprintf ('<div style="width:17px;height:15px;margin-left:1px;margin-top:1px;background:#%02x%02x%02x;display:inline;"></div>', (int)$c['ValueRed'], (int)$c['ValueGreen'], (int)$c['ValueBlue'])) ;
	} else {
	    listField () ;
	}
    	listField ($c['Number']) ;
	foreach ($Size as $sid => $s) {
	    printf ("<td align=right><input type=text style='width:45px;text-align:right;margin-right:0;' name=Quantity[%d][%d] maxlength=5 size=5></td>", $cid, $sid) ;
	}
	listRow () ;
	listField () ;
    }

    listEnd () ;

    formEnd () ;
    
    return 0 ;
?>
