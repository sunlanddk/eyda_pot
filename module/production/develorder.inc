<?php

    require_once 'lib/html.inc' ;
    require_once 'module/production/include.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/parameter.inc' ;
    
    // Validate Case
    if ($Record['Done']) return 'Case has status Done' ;
    
    // Get colors
    $query = sprintf ("SELECT ArticleColor.Id, Color.Number AS ColorNumber, Color.Description AS ColorDescription, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM ArticleColor, Color LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Color.Number", $Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Color = array() ;
    while ($row = dbFetch ($result)) $Color[(int)$row['Id']] = $row ;
    dbQueryFree ($result) ;
    if (count($Color) == 0) return 'no Colours assigned' ;

    // Get sizes
    $query = sprintf ("SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Size = array() ;
    while ($row = dbFetch ($result)) $Size[(int)$row['Id']] = $row ;
    dbQueryFree ($result) ;
    if (count($Size) == 0) return 'no Sizes assigned' ;
   
    // Header
    print productionHeader ($Record) ;

    // Get Id for newest Style
    $query = sprintf ('SELECT Style.Id AS StyleId, Style.ArticleId FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 ORDER BY Style.Version DESC LIMIT 1', (int)$Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Record = dbFetch($result) ;	    
    dbQueryFree ($result) ;
	    
    // Default values
    $Record['MaterialDate'] = dbDateEncode(time()+7*24*60*60) ;
    $Record['SubDeliveryDate'] = dbDateEncode(time()+14*24*60*60) ;
    $Record['DeliveryDate'] = dbDateEncode(time()+21*24*60*60) ;
    $Record['ProductionLocationId'] = parameterGet ('DevelLocationId') ;
 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Location', formDBSelect ('ProductionLocationId', (int)$Record['ProductionLocationId'], 'SELECT Id, Name AS Value FROM ProductionLocation WHERE TypeProduction=1 AND TypeSample=1 AND Active=1 ORDER BY Name', 'width:150px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Project', formDBSelect ('SeasonId', (int)$Record['SeasonId'], sprintf('SELECT Id, Name AS Value FROM Season WHERE CompanyId=%d AND Done=0 AND Active=1 ORDER BY Name', tableGetField('`case`','CompanyId',$Id)), 'width:150px;')) ;  
	itemSpace () ;
    itemFieldRaw ('Ship', formDate('MaterialDate', dbDateDecode($Record["MaterialDate"]), null, 'tR')) ;
    itemFieldRaw ('Approval', formDate('SubDeliveryDate', dbDateDecode($Record["SubDeliveryDate"]), null, 'tR')) ;
    itemSpace () ;
    print htmlFlag ($Record, 'Request') ;
    itemSpace () ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:100px;')) ;
	printf ("<tr><td class=itemfield>Plan</td><td><input type=text name=PlanText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['PlanText'])) ;
	printf ("<tr><td class=itemfield>Missing</td><td><input type=text name=MissingText maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['MissingText'])) ;
    itemInfo ($Record) ;
    itemEnd () ;

    printf ("<br>\n") ;

    listStart () ;
    listHeader ('Quantity') ;
    listRow () ;
    listHead ('Colour', 75) ;
    listHead ('', 40) ;
    listHead ('', 6) ;
    listHead ('', 80) ;
    foreach ($Size as $s) listHead ($s['Name'], 60, 'align=right') ;
    listHead () ;
    foreach ($Color as $c) {
	listRow () ;
	listField ($c['ColorNumber']) ;
	if ($c['ColorGroupId'] > 0) {
	    listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$c['ValueRed'], (int)$c['ValueGreen'], (int)$c['ValueBlue'])) ;
	} else {
	    listField ('') ;
	}
	listField ('') ;
	listField ($c['ColorDescription']) ;
	foreach ($Size as $s) listFieldRaw (formText (sprintf('Quantity[%d][%d]', (int)$s['Id'], (int)$c['Id']), '', 3, 'text-align:right;width:50px;margin-right:0;'), 'align=right') ;
    }
    listEnd () ;
    printf ("<br></td>") ;

    formEnd () ;
    
    return 0 ;
?>
