<?php

    require_once 'lib/file.inc' ;
    
    // Make filename
    $file = fileName ('pdm', $Id) ;

    // No caching
    switch ($_SERVER["SERVER_PROTOCOL"]) {
	case "HTTP/1.1":
	    header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	    break ;
    
	case "HTTP/1.0":
	    header ("Pragma: no-cache") ; 
	    break ;

	default:
	    return sprintf ('invalid server protocol, type "%s"', $_SERVER['SERVER_PROTOCOL']) ;
    }
    
    // HTTP Headers
//  $size = filesize($file) ;
//  header ('Content-Length: '.$size) ;		// Can't be used when compressing
    header ('Content-Type: '.$Record['Type']) ;
    header (sprintf ('Content-Disposition: %s; filename="%sv%d.xls"', ($Navigation['Parameters'] == 'newest download') ? 'attachment' : 'inline', $Record['StyleNumber'], $Record['Version']));
    header ('Last-Modified: '.date("D, j M Y G:i:s T", dbDateDecode($Record['CreateDate'])));

    // Output file
    readfile ($file) ;

    return 0 ;

?>
