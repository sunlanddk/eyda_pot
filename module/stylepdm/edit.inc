<?php
   
    require_once "lib/html.inc" ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
   
    // Form
    formStart (NULL, 'multipart/form-data') ;
//    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
//    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
//    printf ("<input type=hidden name=nid>\n") ;

    itemStart () ;
    itemHeader () ;
    print formHidden ('MAX_FILE_SIZE', '2000000') ;
//    printf ("<input type=hidden name=MAX_FILE_SIZE value=2000000>\n") ;
    printf ("<tr><td><p>File</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;    
    itemSpace() ;
    itemFieldRaw ('Approve', formCheckbox ('Approve', 0) . ' (Style Approved by Constructor)') ;
    itemSpace() ;
    itemFieldRaw ('Log Text', formTextArea ('Text', '', 'width:100%;height:200px;')) ;
    itemEnd () ;
    formEnd () ;
    
    return 0 ;
?>
