<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;

    // Style header
    printf ("<table class=item>\n") ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield width=80>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Style Version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Locked</td><td class=itemfield>%s</td></tr>\n", ($Record['TaskId'] > 0) ? sprintf ('%s at %s', htmlentities(tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['TaskUserId'])), date('Y-m-d H:i:s', dbDateDecode($Record['TaskCreateDate']))) : 'No') ;
    if ($Record['TaskId'] > 0) printf ("<tr><td class=itemlabel>Release</td><td class=itemfield%s>%s</td></tr>\n", (dbDateDecode($Record['TaskEndDate']) < time()) ? ' style="color: red;"' : '', date('Y-m-d H:i', dbDateDecode($Record['TaskEndDate']))) ;
    print htmlItemSpace () ;
    printf ("</table>\n") ;

    // Log list
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('PDM Version') ;
    listHead ('User', 180) ;
    listHead ('Created', 110) ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'view', $row['Id']) ;
	listField ((int)($row['Version'])) ;
	listField ($row['UserName']) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No PDMs', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;


    return 0 ;
?>
