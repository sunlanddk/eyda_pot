<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/form.inc' ;

    function htmlTimeSelect ($fieldname, $value, $style='') {
	$html = sprintf ("<select size=1 name=%s", $fieldname) ;
	if ($style != '') $html .= sprintf (" style='%s'", $style) ;
	$html .= ">\n" ;
	for ($hours = 0 ; $hours < 24 ; $hours++) {
	    foreach (array(0,30) as $minutes) {
		$s = sprintf ("%02d:%02s", $hours, $minutes) ;
		$html .= sprintf ("<option%s value='%s'>%s\n", ($s == $value)?' selected':'', $s, $s) ;
	    }
	}
	$html .= sprintf ("</select>\n") ;
	return $html ;
    }

    // Default time for ending
    $t = time() ;
    $t = (int)($t/(60*60)) ;
    $t = ($t+5)*60*60 ;

    // Style header
    printf ("<table class=item>\n") ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield width=80>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    print htmlItemSpace () ;
    printf ("</table>\n") ;

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<p><b>Expected new release:</b></p>\n") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader () ;
//    printf ("<tr><td class=itemlabel>Date</td><td><input type=text id=IdEndDate name=EndDate size=10 maxlength=10 value=\"%s\" style='width:68px' readonly=1><img src='image/date.gif' id=IdEndDateButton style='vertical-align:bottom;margin-left:0px;margin-right:15px;cursor: pointer;'></td></tr>\n", date('Y-m-d',$t)) ;
    printf ("<tr><td class=itemlabel>Date</td><td>%s</td></tr>\n", formDate('EndDate', $t, null, 'tR')) ;
    printf ("<tr><td class=itemlabel>Time</td><td>%s</td></tr>\n", htmlTimeSelect('EndTime', date('H:i',$t))) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
    
    return 0 ;
?>
