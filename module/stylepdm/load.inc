<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/perm.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query records to list
	    $query = sprintf ("SELECT StylePDM.*, CONCAT(User.FirstName,' ',User.LastName,' (',User.Loginname,')') AS UserName FROM StylePDM LEFT JOIN User ON User.Id=StylePDM.CreateUserId WHERE StylePDM.StyleId=%d AND StylePDM.Active=1 ORDER BY StylePDM.Id DESC", $Id) ;
	    $Result = dbQuery ($query) ;

	    // Fall through
	    
	case 'edit' :
	case 'save-cmd' :
	case 'lock' :
	case 'lock-cmd' :
	case 'unlock-cmd' :
	    // Get Id for PDMLock TaskType
	    $TaskTypeId = tableMark2Id ('TaskType', 'pdmlock') ;

	    // Get Style
	    $query = sprintf ("SELECT Style.*, Article.Number, Article.Description, Task.Id AS TaskId, Task.UserId AS TaskUserId, Task.CreateDate AS TaskCreateDate, Task.EndDate AS TaskEndDate FROM Style LEFT JOIN Article ON Article.Id=Style.ArticleId LEFT JOIN Task ON Task.RefId=Style.Id AND Task.TaskTypeId=%d AND Task.Active=1 AND Task.Done=0 WHERE Style.Id=%d AND Style.Active=1", $TaskTypeId, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Navigation
	    if ($Record['TaskId'] > 0) {
		// Lock does exist
		if ($Record['TaskUserId'] == $User['Id'] or permAdmin()) {
		    navigationEnable ('new') ;
		    navigationEnable ('unlock') ;
		    navigationEnable ('download') ;
		}
	    } else {
		// No Locks
		navigationEnable ('lock') ;

		if ($Record['Ready']) {
		    navigationPasive ('lock') ;
		}
	    }

	    return 0 ;

	case 'view' :
	    switch ($Navigation['Parameters']) {
		case 'newest' :
		case 'newest download' :
		    // Find newest version number		
		    $query = sprintf ('SELECT MAX(Version) as Version FROM StylePDM WHERE StyleId=%d AND Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $row = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    $version = $row['Version'] ;
		    if ($version <= 0) return 'no PDM for this Style' ;

		    // Get Id from version number
		    $query = sprintf ("SELECT Id FROM StylePDM WHERE StyleId=%d AND Version=%d AND Active=1", $Id, $version) ;
		    $res = dbQuery ($query) ;
		    $row = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    $Id = (int)$row['Id'] ;
		    break ;
	    }
	    
	    // Get PDM Version
	    $query = sprintf ('SELECT StylePDM.*, CONCAT(Article.Number,"-",Style.Version) AS StyleNumber FROM StylePDM LEFT JOIN Style ON Style.Id=StylePDM.StyleId LEFT JOIN Article ON Article.Id=Style.ArticleId WHERE StylePDM.Id=%d AND StylePDM.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;	    
    }
    
    return 0 ;
?>
