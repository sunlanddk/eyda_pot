<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/perm.inc' ;

    $fields = array (
	'Done'		=> array ('type' => 'set',	'value' => 1),
	'DoneUserId'	=> array ('type' => 'set',	'value' => $User['Id']),
	'DoneDate'	=> array ('type' => 'set',	'value' => dbDateEncode(time()))
    ) ;
   
    // PDM Locked ?
    if ($Record['TaskId'] <= 0) return 'PDM is not locked' ;

    // User rights
    if (!permAdmin()) {
	if ($User['Id'] != $Record['TaskUserId']) return 'you can not unlock a PDM locked by an other user' ;
    }
    
    return saveFields ('Task', $Record['TaskId'], $fields) ;
    
?>
