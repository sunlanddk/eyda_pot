<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
	global $User, $Navigation, $Record, $Size, $Id ;

    itemStart () ;
    itemSpace () ;
    itemField ('Article', $Record['ArticleDescription'].' ('.$Record['ArticleNumber'].')') ;
    itemField ('Color', $Record['ColorDescription'].' ('.$Record['ColorNumber'].')') ;
    itemSpace () ;
    itemEnd () ;

    // Form
//    formStart () ;
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;
	
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Season', formText ('SeasonName', $Record['SeasonName'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Cancel', formCheckbox ('Cancel', $Record['Cancel'])) ;
    itemFieldRaw ('Cancel Reason', formText ('CancelDescription', $Record['CancelDescription'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('LOT', formDBSelect ('CollectionLOTId', $Record['CollectionLOTId'], 
											sprintf("SELECT Id, Concat(Name,' - ', Date) AS Value FROM Collectionlot WHERE SeasonId=%d AND Active=1 ORDER BY Date", $Record['SeasonId']), 'width:250px;')) ;  
    itemSpace () ;
//    itemFieldRaw ('Program', formDBSelect ('ProgramId', $Record['ProgramId'], 
//											sprintf("SELECT Id, Concat(Name,' - ', Date) AS Value FROM Collectionlot WHERE SeasonId=%d AND Active=1 ORDER BY Date", $Record['SeasonId']), 'width:250px;')) ;  
// 	itemFieldRaw ('Program', formDBSelect ('ProgramId', $Record['ProgramId'], sprintf('Select 0 as Id, "-Select-" as Value Union SELECT Program.Id, Program.Name AS Value FROM Program WHERE SeasonId=%d AND Active=1 ORDER BY Value', $Record['SeasonId']), 'width:200px')) ; 
    itemSpace () ;
    itemFieldRaw ('Show On B2B', formCheckbox ('ShowOnB2B', $Record['ShowOnB2B'])) ;
    itemFieldRaw ('B2B Focus style', formCheckbox ('Focus', $Record['Focus'])) ;
    itemSpace () ;
    itemFieldRaw ('Not on webshop', formCheckbox ('notonwebshop', $Record['notonwebshop'])) ;
    itemFieldRaw ('Frontpage webshop', formCheckbox ('WebFrontpage', $Record['WebFrontpage'])) ;
    itemSpace () ;

	printf ("<input type=hidden name=MAX_FILE_SIZE value=10000000>\n") ;
	printf ("<tr><td><p>Photo</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;    

    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
