<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;

	function createthumb($name,$filename,$new_w,$new_h){
		$system=explode('.',$name);
		if (preg_match('/jpg|jpeg/',$system[1])){
			$src_img=imagecreatefromjpeg($name);
		}
		if (preg_match('/png/',$system[1])){
			$src_img=imagecreatefrompng($name);
		}

		// Check dimensions
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
		if ($old_x > $old_y) {
			$thumb_w=$new_w;
			$thumb_h=$old_y*($new_h/$old_x);
		}
		if ($old_x < $old_y) {
			$thumb_w=$old_x*($new_w/$old_y);
			$thumb_h=$new_h;
		}
		if ($old_x == $old_y) {
			$thumb_w=$new_w;
			$thumb_h=$new_h;
		}
		
		$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		if (preg_match("/png/",$system[1]))
		{
			imagepng($dst_img,$filename); 
		} else {
			imagejpeg($dst_img,$filename); 
		}
		imagedestroy($dst_img); 
		imagedestroy($src_img); 
	}
	
   $fields = array (    
	'Cancel'			=> array ('type' => 'checkbox','check' => true),
	'Focus'				=> array ('type' => 'checkbox','check' => true),
	'notonwebshop'		=> array ('type' => 'checkbox','check' => true),
	'WebFrontpage'		=> array ('type' => 'checkbox','check' => true),
	'ShowOnB2B'			=> array ('type' => 'checkbox','check' => true),
	'SeasonName'		=> array (),
	'CancelDescription'	=> array ('check' => true),
	'CollectionLOTId'	=> array ('check' => true)
   ) ;         
   
   $ArticleNumber=$Record['ArticleNumber'] ;
   $ColorNumber=$Record['ColorNumber'] ;
// return $ArticleNumber . '_' . $ColorNumber . '.jpg' ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'Cancel' :
			case 'Focus' :
			case 'notonwebshop' :
			case 'WebFrontpage' :
			case 'ShowOnB2B' :
			case 'CancelDescription' :
			case 'CollectionLOTId' :
				if (!$changed) return false ;
				break ;
		}

		if (!$changed) return false ;
		return true ;	
    }

	$Program = array (
			'CollectionmemberId' => $Id,
			'ProgramId' => $_POST['ProgramId']
	) ;
/*
	if ($Record['CollectionmemberProgramId']>0) {
//		return 'her ' . $Record['CollectionmemberProgramId'] ;
			tableWrite ('CollectionMemberProgram', $Program, $Record['CollectionmemberProgramId']) ; 
	} else {
			$CollectionMemberProgramId = tableWrite ('CollectionMemberProgram', $Program) ;
	}
*/

	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return 'photo upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return 'invalid photo size' ;
		if ($_FILES['Doc']['size'] > 10000000)
		    return 'photo too big, max 10 MB' ;

		// Set flag for photo upload	
		$photoUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$photoUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}
	
	
    $res = saveFields ('CollectionMember', $Id, $fields, true) ;
    if ($res) return $res ;

    if ($photoUpload) {
		// Archive photo
		require_once 'lib/file.inc' ;
		$file =	'C:/inetpub/wwwroot/fub/image/thumbnails/' . $ArticleNumber . '_' . $ColorNumber . '_upl.jpg' ;
//		$file =	'C:/inetpub/wwwroot/legacynew/image/thumbnails/' . $ArticleNumber . '_' . $ColorNumber . '.jpg' ;
		if (!move_uploaded_file ($_FILES['Doc']['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive document %s', __FILE__, __LINE__, $file) ;
		chmod ($file, 0777) ;

		// thumpnails
		$file_thump =	'C:/inetpub/wwwroot/fub/image/thumbnails/' . $ArticleNumber . '_' . $ColorNumber . '.jpg' ;
		$file_mini =	'C:/inetpub/wwwroot/fub/image/thumbnails/' . $ArticleNumber . '_' . $ColorNumber . '_mini.jpg' ;
        createthumb($file,$file_thump,600,600);
        createthumb($file,$file_mini,100,100);
		chmod ($file_thump, 0777) ;
		chmod ($file_mini, 0777) ;
    }

	
     return 0 ;    
?>
