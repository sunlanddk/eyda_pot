<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    global $User, $Navigation, $Record, $Result2, $Size, $Id ;

    formStart();
    itemStart () ;

    $query = sprintf (
    	"SELECT 
    		ss.Id,
			ss.Name,
			iss.Active
		FROM (shopifyshops ss)
			LEFT JOIN inshopifyshops iss ON iss.ShopifyShopId=ss.Id AND iss.Active=1 AND iss.CollectionMemberId=%d
		WHERE ss.Active=1 ORDER BY ss.Id;
    	", 
    	(int)$Id
    ) ;
    $result = dbQuery ($query) ;

    $Size = array() ;
    itemField ('Shop', 'Selected') ;
    itemSpace () ;
    while ($row = dbFetch ($result)) {
        itemFieldRaw ($row['Name'], '<input type=hidden name="shop['.$row['Id'].']" value="'.$row['Id'].'">'.formCheckbox('inshop_'.$row['Id'], (int)$row['Active'], '', ((int)$row['Active'] === 1 ? 'disabled' : ''))) ;
        itemSpace () ;
    }
    dbQueryFree ($result) ;

    
    itemSpace () ;
    itemSpace () ;
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
