<?php 

	require_once 'lib/table.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;


    function getVcs($id) {
    	$query = sprintf("SELECT vc.Id AS VariantcodeId, vc.Variantcode, vc.ArticleSizeId, vc.Presale FROM collectionmember cm LEFT JOIN variantcode vc ON vc.ArticleId = cm.ArticleId AND vc.ArticleColorId = cm.ArticleColorId AND vc.Active = 1 WHERE cm.Id = %d AND cm.Active = 1", (int)$id);
    	$result = dbQuery($query);
    	$output = [];
    	while ($row = dbFetch($result)){
    		$output[] = $row;
    	}
    	dbQueryFree($result);
    	return $output;
    }

    formStart();
    listStart();

    listRow();

    listHead('Variantcode', 150);
    listHead('Size', 100);
    listHead('Set presale', 100);
    listHead('Delete presale', 100);
    listHead('');

    $vcs = getVcs($Id);

    foreach($vcs as $vc) {
    	listRow();
    	listField($vc['Variantcode']);
    	listField(tableGetField('articlesize', 'Name', $vc['ArticleSizeId']));
    	if((bool)$vc['Presale']) {
    		listField('');
    		listFieldIcon('delete.gif', 'deletepresale', (int)$vc['VariantcodeId'], 'CmId='.$Id) ;
    	} else {
    		listFieldIcon('add.gif', 'savepresale', (int)$vc['VariantcodeId'], 'CmId='.$Id) ;
    		listField('');
    	}
    	listField();
    	
    	

    	
    	
    	
    }

    
    listEnd();
    formEnd();




	return 0;
?>