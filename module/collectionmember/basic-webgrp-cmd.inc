<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

   $fields = array (    
	'WebFitGroup'		=> array ('check' => true),
	'WebCampaign'		=> array ('check' => true)
   ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'WebFitGroup' :
			case 'WebCampaign' :
				if (!$changed) return false ;
				break ;
		}

		if (!$changed) return false ;
		return true ;	
    }

    $res = saveFields ('CollectionMember', $Id, $fields, true) ;
    if ($res) return $res ;

     return 0 ;    
?>
