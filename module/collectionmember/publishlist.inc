<script type="text/javascript" >
	function CheckAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
//			if (!e.name.indexOf('UpdateSetFlag')) continue ;
			e.checked = true ;
		}
	}
	function CheckAllOnWebshop () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			if (!e.name.indexOf('UpdateSetFlag')) continue ;
			e.checked = true ;
		}
	}
	function ClearAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			e.checked = false ;
		}
	}
</script>

<?php
    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;
	
    formStart () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Select', 30) ;
    listHead ('Article', 45)  ;
    listHead ('Description', 120) ;
    listHead ('Color', 45)  ;
    listHead ('Description', 120) ;
    listHead ('On Webshop', 120) ;
    listHead ('', 95) ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
    	if((int)$row['CronDone'] > 0){
    		continue;
    	}
		listRow () ;
		if ($row["Cancel"]) {
			listField ('Cancel') ;
		} else {
			if ($row["notonwebshop"]==1) {
				listFieldRaw (formCheckbox (sprintf('UpdateSetFlag[%d]', (int)$row['Id']), 0, '')) ;
			} else {
				listFieldRaw (formCheckbox (sprintf('UpdateFlag[%d]', (int)$row['Id']), 0, '')) ;
			}
		}
    	listField ($row["ArticleNumber"]) ;
    	listField ($row["Description"]) ;
    	listField ($row["ColorNumber"]) ;
    	listField ($row["ColorName"]) ;
    	listField (($row["notonwebshop"]==1)?'No':'Yes') ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Articles', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
