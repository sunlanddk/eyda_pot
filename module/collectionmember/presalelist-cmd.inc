<?php
	
	require_once 'lib/table.inc' ;

	if (!isset($_POST['Variantcodeid'])) {
		return 'No variants selected';
	}

	// release variants
	foreach($_POST['Variantcodeid'] as $vcId => $value ) {
		if ($value == 'on') {
			//generate pickorder where vcId
			tableWrite('variantcode', array('Presale' => 0), (int)$vcId);
			$_variantcodeIds[] = (int)$vcId ;
		}
	}

//	return 0 ;

	// generate pickorders
	$_varIds = implode(',', $_variantcodeIds) ;
	$_query = sprintf('select o.CountryId, o.id as OrderId, o.ToCompanyId as OwnerId, o.CompanyId, o.ConsolidatedId,
								v.Variantcode, v.Id as VariantcodeId, 
								ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid,
								oq.articlesizeid, oq.quantity as Quantity,  oq.QuantityReceived, oq.Id as QlId,
								c.description as color, az.name as size								
						from (variantcode v, orderline ol, orderquantity oq, `order` o, articlecolor ac, color c, articlesize az)
	                    left join pickorderline pol on pol.qlid=oq.Id and pol.PackedQuantity>0 and pol.active=1
						where v.id in (%s) 
						and isnull(pol.id)
						and ol.articlecolorid=v.articlecolorid and ol.active=1 
						and oq.orderlineid=ol.id and oq.articlesizeid=v.articlesizeid and oq.active=1
						and o.id=ol.orderid
						and ol.articlecolorid=ac.id and  ac.colorid=c.id and oq.articlesizeid=az.id
						and ol.done=0
						 ',$_varIds) ;
//						and not o.CountryId=15 and ol.deliverydate>"2023-06-21"
//						 and o.id=174524',$_varIds) ;

	$_res = dbQuery ($_query) ;
	$CurrentOrderId = 0 ;
	while ($row = dbFetch ($_res)) {
		// Insert into PickOrder table.
		if ($CurrentOrderId <> $row['OrderId']) {
 			// Initialize
			$PickOrder = array (	
				'Type'				=>  "SalesOrder",			// Pick order from internal customer
				'Reference'			=>  0,			
				'FromId'			=>  14321, 					//$Record['PickStockId'],		
				'CompanyId'			=>  0,						// Shop Code	
				'OwnerCompanyId'	=>  $row['OwnerId'],		
				'HandlerCompanyId'	=>  $row['OwnerId'],					
				'Instructions'		=>  "",		
				'Packed'				=>  0,
				'DeliveryDate'		=>  ""						// Expected pick and pack complete
			) ;
				// Generate PickOrders
			$PickOrder['Reference'] 	= $row['OrderId'] ;
			$PickOrder['ReferenceId'] 	= $row['OrderId'] ;
			$PickOrder['CompanyId'] 	= $row['CompanyId'] ;
			$PickOrder['ConsolidatedId'] 	= $row['CompanConsolidatedIdyId'] ;
			$PickOrder['DeliveryDate'] 	= $row['DeliveryDate'] ;
			$PickOrder['Instructions'] 	='' ; // $row['Instructions'] ;
		
			$CurrentOrderId = $row['OrderId'] ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder) ;
		}
 		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		$PickOrderLine['PickOrderId'] 		= $PickOrderId ;
		$PickOrderLine['VariantCode'] 		= $row['Variantcode'] ;
		$PickOrderLine['VariantCodeId'] 	= $row['VariantcodeId'] ;
		$PickOrderLine['OrderedQuantity'] 	= $row['Quantity'] ;
		$PickOrderLine['VariantDescription'] = substr($row['ArticleDescription'],0,48) ;
		$PickOrderLine['VariantColor'] 		= $row['color'] ;
		$PickOrderLine['VariantSize'] 		= $row['size'] ;
		$PickOrderLine['QlId'] 				= $row['QlId'] ;
		tableWrite ('PickOrderLine', $PickOrderLine) ;
	}
	return 0;
?>