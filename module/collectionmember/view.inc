<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/variant.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;

	global $User, $Navigation, $Record, $Result2, $Size, $Id ;

	function getActiveSites($colMemberId){
    	$query = sprintf (
	    	"SELECT 
				ss.Name
			FROM (shopifyshops ss, inshopifyshops iss)
			WHERE ss.Active=1 AND ss.Id=iss.ShopifyShopId AND iss.Active=1 AND iss.CollectionMemberId=%d ORDER BY ss.Id;
	    	", 
	    	(int)$colMemberId
	    ) ;
	    $results = dbQuery ($query) ;
	    $shops = [];
		while ($row = dbFetch ($results)) {
	        array_push($shops, $row['Name']);
	    }
	    return $shops;
    }

//    if (dbNumRows($Result) == 0) return "No Collections assigned to this Season" ;
    printf ("<table><tr><td width=250>") ; 	
    itemStart () ;
    itemSpace () ;
//    itemFieldIcon ('Season', $Record['SeasonName'], 'season.png', 'seasonview', (int)$Record['SeasonId']) ; 
//    itemFieldIcon ('Collection', $Record['CollectionName'],  'collection.png', 'collmemberlist', (int)$Record['CollectionId']) ; 
//    itemField ('Program', $Record['ProgramName']) ;
//    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
//    itemFieldIcon ('Company Coll.', ALL,  'collection.png', 'collcompanylst', (int)$Record['CompanyId']) ; 
    itemFieldIcon ('Article', tableGetField('Article','Number',$Record['ArticleId']) . ' ' . tableGetField('Article','Description',$Record['ArticleId']) , 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
    if ($Record['ArticleColorId'] > 0) {
	    $ColorId = tableGetField('ArticleColor', 'ColorId',$Record['ArticleColorId']) ;
	    itemField ('Color', tableGetField('Color','Number', $ColorId) . '  ' . tableGetField('Color','Description', $ColorId)) ;
	} else {
	    itemField ('Color', 'No Color') ;
	}
    itemField ('Season', $Record['SeasonName']) ;
    itemField ('Core', $Record['Core']>0?'Yes':'No') ;
    if ($Record['Cancel']) {
	    itemSpace () ;
	    itemField ('Cancelled', $Record['CancelDescription']) ;
    } else
	    itemSpace () ;
//	    itemField ('LOT', $Record['LOT']) ;
	    itemField ('On Web?', $Record['notonwebshop']>0?'No':'Yes') ;
	    itemField ('Focus style?', $Record['Focus']>0?'Yes':'No') ;
	    itemField ('Front page?', $Record['Frontpage']>0?'No':'Yes') ;
    itemSpace () ;
    $colorLight = CheckForShopifyId((int)$Record['ArticleId'], (int)$Record['ArticleColorId'], (int)$Id);
    itemFieldRaw ('Active shops','<p>'.implode(' / ', getActiveSites($Id)).'</p>') ;
    itemSpace () ;
    itemFieldRaw ('Export status','<div class="lights '.$colorLight.'"><div class="red"></div><div class="yellow"></div><div class="green"></div></div>') ;
    if($colorLight == 'red'){
    	itemFieldRaw ('Error','<p>'.tableGetFieldWhere('cron_jobs', 'Error', sprintf("InternalId=%d AND type='shopify' ORDER BY id DESC LIMIT 1", $Id)).'</p>') ;
    }
    itemSpace () ;
/*
	printf ("<tr><td class=itemlabel>Icon</td><td class=itemfield>") ;
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_icon.jpg' ;
    printf ('<img src="' . $photo .'" style="border-radius:50%%;">') ;
	printf ("</td></tr>") ;
*/
	itemInfo ($Record) ;
	itemEnd () ;
	printf ("</td><td>") ;
//	class="item-img" src="image/thumbnails/
    printf ('<table cellspacing="3"><tr><td>') ; 	
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '.jpg' ;
    printf ('<img width=250px src="' . $photo .'">') ;

	printf ("</td>") ;
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_icon.jpg' ;
	if (file_exists($photo)) {
		printf ("<td width=100>") ;
		printf ('<img src="' . $photo .'" style="border-radius:50%%;">') ;
		printf ("</td>") ;
	}
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_d1.jpg' ;
	if (file_exists($photo)) {
		printf ("<td width=260>") ;
		printf ('<img width=250px src="' . $photo .'">') ;
		printf ("</td>") ;
	}
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_d2.jpg' ;
	if (file_exists($photo)) {
		printf ("<td width=260>") ;
		printf ('<img width=250px src="' . $photo .'">') ;
		printf ("</td>") ;
	}
	printf ("</tr></table>") ; 	

	printf ("</td>") ;
	printf ("</tr><tr>") ;


   
	// Assortments
	$query = sprintf ("SELECT ArticleSize.*, CollectionmemberAssortment.AssortmentId, CollectionmemberAssortment.Quantity FROM ArticleSize
						INNER JOIN CollectionmemberAssortment ON CollectionmemberAssortment.ArticleSizeId=ArticleSize.Id AND CollectionmemberAssortment.CollectionMemberId=%d AND CollectionmemberAssortment.Active=1
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY CollectionmemberAssortment.AssortmentId, ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;

	$Size = array() ;
	while ($row = dbFetch ($result)) {
		$Size[(int)$row['AssortmentId']][$row['DisplayOrder']] = $row ;
	}
	dbQueryFree ($result) ;
/*
    printf ("<td colspan=2><table><tr><td width=750>") ;
	listStart () ;
	listRow () ;
	listHead ('Assortments', 60) ;
	if (count($Size) > 0) {
		foreach (current($Size) as $i => $s) 
			listHead (htmlentities($s['Name']), 60) ;
		listhead ('Variantcode',60) ;
		listhead ('Edit',20) ;
		listhead ('Delete',20) ;
		listhead ('PickQuantity',50) ;
		listhead ('Position',50) ;
		foreach ($Size as $j => $sizes) {
			listRow () ;
			listField ($j) ;
			foreach ($sizes as $i => $s) {
				listField (number_format ((int)$s['Quantity'], (int)0, ',', '.')) ;
			}
			$query = sprintf ("SELECT VariantCode.VariantCode, VariantCode.PickQuantity, Container.Position FROM VariantCode, Container 
								WHERE Container.Id=VariantCode.PickContainerId and Reference=%d and `Type`='assort' and VariantCode.Active=1", $j) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result);
			listField ($row['VariantCode']) ;
			dbQueryFree ($result) ;
//			listField (tableGetFieldWhere('variantcode','VariantCode',sprintf("Reference=%d and `Type`='assort' and Active=1", $j))) ;
			listFieldIcon ('edit.gif', 'editassort', (int)$j) ;
			listFieldIcon ('delete.gif', 'deleteassort', (int)$j) ;
			listField ($row['PickQuantity']) ;
			listField ($row['Position']) ;
		}
	}
	listRow () ;
	listFieldIcon ('add.gif', 'addassort', (int)$Id) ;
	listEnd () ;
	printf ("</td></tr></table><br><br>") ;
	printf ("</td></tr></table>") ;
*/
    // List quatities
    printf ("<table><tr><td width=750>") ;
    listStart () ;
    listRow () ;
    listHead ('Quantities', 95) ;
    listHead ('Available date', 95) ;

	$query = sprintf ("SELECT ArticleSize.*, ascd.id as articlesizecolldisableId 
						FROM ArticleSize
						LEFT JOIN articlesizecolldisable ascd on ascd.articlesizeid=articlesize.id and ascd.collectionmemberid=%d
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY ArticleSize.DisplayOrder", $Id, (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
		if ($row['articlesizecolldisableId']>0) continue ;
		$_sizes[$row['DisplayOrder']] = $row ;
	}
	dbQueryFree ($result) ;
	foreach ($_sizes as $i => $s)
			listHead (htmlentities($s['Name']), 80) ;
//    listHead ('On pick stock', 95) ;
//    listHead ('In order', 75) ;
//    listHead ('In purchase', 75) ;
    listHead ('Total', 95) ;
	
	$query = sprintf ("select s.name as SeasonName, s.done as SeasonDone, cl.name as LOTName,  cl.PreSale, tab.lotid, 
								tab.articlesizeid, sum(tab.rqnty) as rqnty, sum(tab.oqnty) as oqnty,sum(tab.iqnty) as iqnty 
						from (
						# QTY in req
						select rl.lotid as lotid, i.articlesizeid, sum(i.Quantity) as rqnty, 0 as oqnty, 0 as iqnty
						from container c inner join item i on i.ContainerId=c.Id 
						left join requisitionline rl on rl.id=i.requisitionlineid and rl.active=1 and (rl.done=0 or isnull(rl.done)) 
						where c.Id=135400 and i.ArticleId=%d and i.articlecolorId=%d  and i.Active=1
						group by rl.lotid, i.articlesizeid
						UNION
						# in order
						select l.lotid as lotid, q.articlesizeid, 0 as rqnty,  
						sum(q.Quantity)-sum(coalesce((select sum(quantity) from item i where i.orderlineid=l.id and i.articlesizeid=q.articlesizeid and active=1),0)) as oqnty, 0 as iqnty
						from (`order` o, season s) left join `orderline` l on l.OrderId=o.Id left join `orderquantity` q on q.OrderLineId=l.Id and q.active=1 
						where o.Active=1 and l.active=1 and o.Done=0 and o.proposal=0 and l.Done=0 and o.ToCompanyId = 787 and s.id=o.seasonid and l.active=1 
						and l.ArticleId=%d and l.ArticleColorId=%d  
						group by l.lotid, q.articlesizeid
						UNION
						# Onstock
						select 0 as lotid, i.articlesizeid, 0 as rqnty, 0 as oqnty, sum(i.Quantity) as iqnty 
						from `season` s 
						left join `container` c on c.StockId=s.pickstockid or c.StockId=s.fromstockid 
						left join `item` i on i.ContainerId=c.Id 
						where i.ArticleId=%d and i.articlecolorId=%d and i.Active=1 and s.id=%d
						group by i.ArticleSizeId) tab
						left join collectionlot cl on cl.id=tab.lotid 
						left join season s on s.id=cl.seasonid
						left join articlesize az on az.id=tab.articlesizeid
						group by az.displayorder
						order by cl.Date", 
						$Record['ArticleId'], $Record['ArticleColorId'], $Record['ArticleId'], $Record['ArticleColorId'], $Record['ArticleId'], $Record['ArticleColorId'], $Record['SeasonId']) ;

	$query = sprintf ("select s.name as SeasonName, s.done as SeasonDone, cl.name as LOTName,  cl.PreSale, tab.lotid, 
								tab.articlesizeid, sum(tab.rqnty) as rqnty, sum(tab.oqnty) as oqnty,sum(tab.iqnty) as iqnty, 
								sum(tab.onqty) as onqty, sum(tab.holdqty) as holdqty, sum(tab.returnqty) as returnqty
						from (
						# QTY in req
						select rl.lotid as lotid, i.articlesizeid, sum(i.Quantity) as rqnty, 0 as oqnty, 0 as iqnty, 0 as onqty, 0 as holdqty, 0 as returnqty
						from container c inner join item i on i.ContainerId=c.Id 
						left join requisitionline rl on rl.id=i.requisitionlineid and rl.active=1 and (rl.done=0 or isnull(rl.done)) 
						where c.Id=135400 and i.ArticleId=%d and i.articlecolorId=%d  and i.Active=1
						group by rl.lotid, i.articlesizeid
						UNION
                        # On Reciving
						select 0 as lotid, i.articlesizeid,  0 as rqnty, 0 as oqnty, 0 as iqnty, sum(i.Quantity) as onqty, 0 as holdqty, 0 as returnqty
						from container c inner join item i on i.ContainerId=c.Id 
						where c.StockId=32805 AND c.Active and i.ArticleId=%d and i.articlecolorId=%d  and i.Active=1
						group by i.articlesizeid
						UNION
                        # On Hold
						select 0 as lotid, i.articlesizeid,  0 as rqnty, 0 as oqnty, 0 as iqnty, 0 as onqty, sum(i.Quantity) as holdqty, 0 as returnqty
						from container c inner join item i on i.ContainerId=c.Id 
						where c.StockId=40519 AND c.Active and i.ArticleId=%d and i.articlecolorId=%d  and i.Active=1
						group by i.articlesizeid
						UNION
                        # On Returns
						select 0 as lotid, i.articlesizeid,  0 as rqnty, 0 as oqnty, 0 as iqnty,  0 as onqty, 0 as holdqty, sum(i.Quantity) as returnqty
						from container c inner join item i on i.ContainerId=c.Id 
						where c.StockId=32800 AND c.Active and i.ArticleId=%d and i.articlecolorId=%d  and i.Active=1
						group by i.articlesizeid
						UNION
						# in order
						select l.lotid as lotid, q.articlesizeid, 0 as rqnty,  
						sum(q.Quantity)-sum(coalesce((select sum(quantity) from item i where i.orderlineid=l.id and i.articlesizeid=q.articlesizeid and active=1),0)) as oqnty, 0 as iqnty, 0 as onqty, 0 as holdqty, 0 as returnqty
						from (`order` o, season s) left join `orderline` l on l.OrderId=o.Id left join `orderquantity` q on q.OrderLineId=l.Id and q.active=1 
						where o.Active=1 and l.active=1 and o.Done=0 and o.proposal=0 and l.Done=0 and o.ToCompanyId = 787 and s.id=o.seasonid and l.active=1 
						and l.ArticleId=%d and l.ArticleColorId=%d  
						group by l.lotid, q.articlesizeid
						UNION
						# Onstock
						select 0 as lotid, i.articlesizeid, 0 as rqnty, 0 as oqnty, sum(i.Quantity) as iqnty,  0 as onqty, 0 as holdqty, 0 as returnqty
						from `season` s 
						left join `container` c on c.StockId=s.pickstockid
						left join `item` i on i.ContainerId=c.Id 
						where i.ArticleId=%d and i.articlecolorId=%d and i.Active=1 and s.id=%d
						group by i.ArticleSizeId) tab
						left join collectionlot cl on cl.id=tab.lotid 
						left join season s on s.id=cl.seasonid
						left join articlesize az on az.id=tab.articlesizeid
						group by az.displayorder
						order by cl.Date", 
						$Record['ArticleId'], $Record['ArticleColorId'], 
						$Record['ArticleId'], $Record['ArticleColorId'], 
						$Record['ArticleId'], $Record['ArticleColorId'], 
						$Record['ArticleId'], $Record['ArticleColorId'], 
						$Record['ArticleId'], $Record['ArticleColorId'], 
						$Record['ArticleId'], $Record['ArticleColorId'], 
						$Record['SeasonId']) ;

//echo $query ;
	$result = dbQuery ($query) ;
	$_total = 0 ;
    while ($row = dbFetch($result)) {
		$_listrows[$row['lotid']]['SeasonName'] = $row['SeasonName'] ;
		$_listrows[$row['lotid']]['SeasonDone'] = $row['SeasonDone'] ;
		$_listrows[$row['lotid']]['lotid'] = $row['lotid'] ;
		$_listrows[$row['lotid']]['LOTName'] = $row['LOTName'] ;
		$_listrows[$row['lotid']]['PreSale'] = $row['PreSale'] ;
		$_listrows[$row['lotid']]['Sizes'][$row['articlesizeid']] = $row ;
	}
	dbQueryFree ($result) ;
	$_total=0 ;
	foreach($_listrows as $k => $lot) {
    	listRow () ;
		listField ('On pick stock'); 
		listField (''); 
		reset($_sizes) ;
		foreach ($_sizes as $i => $s) {
			listField (number_format((int)$lot['Sizes'][$s['Id']]['iqnty'], (int)0, ',', '.')) ;
			$_ilinetotal += (int)$lot['Sizes'][$s['Id']]['iqnty'] ;
		}
		listField (number_format((int)$_ilinetotal, (int)0, ',', '.')) ;

		listRow () ;
		listField ('On Receiving'); 
		listField (''); 
		reset($_sizes) ;
		$onreceivetotal = 0;
		foreach ($_sizes as $i => $s) {
			listField (number_format((int)$lot['Sizes'][$s['Id']]['onqty'], (int)0, ',', '.')) ;
			$onreceivetotal += (int)$lot['Sizes'][$s['Id']]['onqty'] ;
		}
		listField (number_format((int)$onreceivetotal, (int)0, ',', '.')) ;
		
		
    	listRow () ;
		listField ('- In order'); 
		listField (''); 
		reset($_sizes) ;
		foreach ($_sizes as $i => $s) {
			listField (number_format((int)$lot['Sizes'][$s['Id']]['oqnty'], (int)0, ',', '.')) ;
			$_olinetotal += (int)$lot['Sizes'][$s['Id']]['oqnty'] ;
		}
		listField (number_format((int)$_olinetotal, (int)0, ',', '.')) ;
		
    	listRow () ;
		listField ('Available'); 
		listField (''); 
		reset($_sizes) ;
		foreach ($_sizes as $i => $s) {
			$_subtotal = (int)$lot['Sizes'][$s['Id']]['iqnty'] - (int)$lot['Sizes'][$s['Id']]['oqnty'] + (int)$lot['Sizes'][$s['Id']]['onqty'];
			listField (number_format((int)$_subtotal, (int)0, ',', '.')) ;
			$_linetotal += (int)$_subtotal ;
		}
		listField (number_format((int)$_linetotal, (int)0, ',', '.')) ;
		
    }

	listRow (1) ; listField() ;
	foreach($_listrows as $k => $lot) {
    	listRow () ;
		listField ('On Returns'); 
		listField (''); 
		reset($_sizes) ;
		foreach ($_sizes as $i => $s) {
			listField (number_format((int)$lot['Sizes'][$s['Id']]['returnqty'], (int)0, ',', '.')) ;
			$onreturntotal += (int)$lot['Sizes'][$s['Id']]['returnqty'] ;
		}
		listField (number_format((int)$onreturntotal, (int)0, ',', '.')) ;

		listRow () ;
		listField ('On hold'); 
		listField (''); 
		reset($_sizes) ;
		$onholdtotal = 0;
		foreach ($_sizes as $i => $s) {
			listField (number_format((int)$lot['Sizes'][$s['Id']]['holdqty'], (int)0, ',', '.')) ;
			$onholdtotal += (int)$lot['Sizes'][$s['Id']]['holdqty'] ;
		}
		listField (number_format((int)$onholdtotal, (int)0, ',', '.')) ;
	}

	listRow (1) ; listField() ;

	// In Purchase in details
	$_query = sprintf("select rl.requisitionid as requisitionid, rl.id as id, rl.AvailableDate, rl.RequestedDate, i.articlesizeid, sum(i.Quantity) as rqnty, 0 as oqnty, 0 as iqnty
					from container c inner join item i on i.ContainerId=c.Id 
					left join requisitionline rl on rl.id=i.requisitionlineid and rl.active=1 and (rl.done=0 or isnull(rl.done)) 
					where c.Id=135400 and i.ArticleId=%d and i.articlecolorId=%d  and i.Active=1
					group by rl.id, i.articlesizeid order by rl.requisitionid", $Record['ArticleId'], $Record['ArticleColorId']) ; 
//die($_query) ;
	$_result = dbQuery ($_query) ;
	$dates = array();
	while ($row = dbFetch($_result)) {
		$_rows[$row['requisitionid']][$row['articlesizeid']] = $row ;
		$dates[$row['requisitionid']] = $row['AvailableDate'] ;
		$req_dates[$row['requisitionid']] = $row['RequestedDate'] ;
	}
	dbQueryFree($_result) ;
	reset($_sizes) ;
	foreach ($_rows as $_rid => $_row) {
		listRow () ;
		listField ('+ PO '. $_rid); 
		if ($dates[$_rid]>'0001-01-01 01:01:01')
			listField (date('Y-m-d', strtotime($dates[$_rid]))); 
		else
			listField (date('Y-m-d', strtotime($req_dates[$_rid]))); 
		$_polinetotal = 0 ;
		foreach ($_sizes as $i => $s) {
			listField (number_format((int)$_row[$s['Id']]['rqnty'], (int)0, ',', '.')) ;
			$_polinetotal += (int)$_row[$s['Id']]['rqnty'] ;
		}
		listField (number_format((int)$_polinetotal, (int)0, ',', '.')) ;
		$_po = 1 ;
	}

	// PO Total
	if (!$_po) {
		listRow () ;
		listField ('+ In Purchase'); 
		listField (''); 
	}
	reset($_sizes) ;
	foreach ($_sizes as $i => $s) {
		if (!$_po) 
			listField (number_format((int)$lot['Sizes'][$s['Id']]['rqnty'], (int)0, ',', '.')) ;
		$_rlinetotal += (int)$lot['Sizes'][$s['Id']]['rqnty'] ;
	}
	if (!$_po) 
		listField (number_format((int)$_rlinetotal, (int)0, ',', '.')) ;
	listRow () ;
	listField ('Total'); 
	listField (''); 
	reset($_sizes) ;
	$_linetotal = 0;
	foreach ($_sizes as $i => $s) {
		$_subtotal = (int)$lot['Sizes'][$s['Id']]['iqnty'] + (int)$lot['Sizes'][$s['Id']]['rqnty'] - (int)$lot['Sizes'][$s['Id']]['oqnty'] 
					+ (int)$lot['Sizes'][$s['Id']]['onqty'] + (int)$lot['Sizes'][$s['Id']]['returnqty'] + (int)$lot['Sizes'][$s['Id']]['holdqty'];
		listField (number_format((int)$_subtotal, (int)0, ',', '.')) ;
		$_linetotal += (int)$_subtotal ;
	}
	listField (number_format((int)$_linetotal, (int)0, ',', '.')) ;
// 	listRow () ;
//	listField ('') ;
   	listRow () ;
    listEnd () ;

    printf ("</td></tr></table><br>") ;

    $_persize=1 ;
    // List prices
if ($_persize==0) {
    printf("<td colspan=2><table><tr><td width=250>") ;
    listStart () ;
    listRow () ;

    listHead ('PriceGroup', 95) ;
    listHead ('Currency', 95) ;
    listHead ('Wholesale', 95) ;
    listHead ('Retail', 75) ;
    listHead ('Campaign', 75) ;
	listhead ('Edit',30) ;
	listhead ('Delete',45) ;
	
	$query = sprintf ("SELECT cmp.id as CollectionMemberPriceId, cpg.Name as PriceGroupName, c.Name as CurrencyName, cmp.WholeSalePrice, cmp.RetailPrice, cmp.CampaignPrice 
						from (CollectionMemberPrice cmp, Currency c )
						left join CollectionPriceGroup cpg ON cmp.collectionpricegroupid=cpg.id 
						Where cmp.CollectionMemberId=%d and cmp.CurrencyId=c.Id and cmp.Active=1 order by PriceGroupName, CurrencyName", $Id) ;
	$result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
    	listRow () ;
    	listField ($row["PriceGroupName"]) ;
    	listField ($row["CurrencyName"]) ;
    	listField (number_format ((float)$row["WholeSalePrice"], 2, ',', '.')) ;
    	listField (number_format ((float)$row["RetailPrice"], 2, ',', '.')) ;
    	listField ($row["CampaignPrice"]) ;
		listFieldIcon ('edit.gif', 'addcmprice', (int)$row['CollectionMemberPriceId']) ;
		listFieldIcon ('delete.gif', 'deleteprice', (int)$row['CollectionMemberPriceId']) ;
    }
   	listRow () ;
	listFieldIcon ('add.gif', 'addprice', (int)$Id) ;
	dbQueryFree ($result) ;
    listEnd () ;
} else {
    printf ("<td colspan=2><table><tr><td width=250><br><br>") ;
    listStart () ;
//	listHeader ('Prices', 60) ;
    listRow () ;
  
	$query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder, CollectionmemberPrice.Id as CollectionmemberPriceId, 
								CollectionmemberPriceSize.WholeSalePrice, CollectionmemberPriceSize.RetailPrice, CollectionmemberPriceSize.CampaignPrice, 
								CollectionmemberPriceSize.PurchasePrice, CollectionmemberPriceSize.LogisticCost, 
								Currency.Name as Currency, ascd.id as articlesizecolldisableId
						FROM ArticleSize
						INNER JOIN CollectionmemberPrice ON CollectionmemberPrice.CollectionMemberId=%d  AND CollectionmemberPrice.Active=1
						INNER JOIN Currency ON Currency.Id=CollectionmemberPrice.CurrencyId
						INNER JOIN CollectionmemberPriceSize ON CollectionmemberPriceSize.ArticleSizeId=ArticleSize.Id AND CollectionmemberPriceSize.CollectionMemberPriceId=CollectionMemberPrice.Id AND CollectionmemberPriceSize.Active=1
						LEFT JOIN articlesizecolldisable ascd on ascd.articlesizeid=articlesize.id and ascd.collectionmemberid=%d
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY Currency.Id, ArticleSize.DisplayOrder, ArticleSize.Name", $Id, $Id, (int)$Record['ArticleId']) ;
	$result = dbQuery ($query) ;
	$Size = array() ;
	while ($row = dbFetch ($result)) {
		if ($row['articlesizecolldisableId']>0) continue ;
		$Size[$row['Currency']][$row['DisplayOrder']] = $row ;
	}
	dbQueryFree ($result) ;
		if (count($Size) > 0) {
			listHead('Prices', 60) ;
			listHead('', 60) ;
			foreach (current($Size) as $i => $s) 
				listHead (htmlentities($s['Name']), 110) ;
			listhead ('Edit',30) ;
			listhead ('Delete',45) ;
			foreach ($Size as $j => $sizes) {
				$tot = 0 ;
				foreach ($sizes as $i => $k) {
					$tot += (float)$k['WholeSalePrice'] ;
				}
				listRow () ;
				listField ($j) ;
				listField ('Sale') ;
				$tot = 0 ;
				foreach ($sizes as $i => $k) {
					if ((float)$k['CampaignPrice']>(float)0)
						$_campaign = ' / ' . number_format((float)$k['CampaignPrice'], (int)2, ',', '.') ;
					else
						$_campaign = '' ;
				
					listField (number_format((float)$k['RetailPrice'], (int)2, ',', '.') . $_campaign) ;
					$tot += (float)$k['PurchasePrice'] ;
				}
				if ($tot>0) {
					listRow () ;
					listField ('') ;
					listField ('Purchase') ;
					foreach ($sizes as $i => $k) 
						listField (number_format((float)$k['PurchasePrice'], (int)2, ',', '.') . ' / ' . number_format((float)$k['LogisticCost'], (int)2, ',', '.')) ;
				}
//				listField ($k['CollectionmemberPriceId']) ;
				$tmp = (int)$k['CollectionmemberPriceId'] ;
				listFieldIcon ('edit.gif', 'addcmprice', $tmp) ;
				listFieldIcon ('delete.gif', 'deleteprice', $tmp) ;
			}
			listRow () ;
		} else {
			listHead('Prices', 60) ;
		}
		listFieldIcon ('add.gif', 'addprice', (int)$Id) ;
		listEnd () ;
		printf ("</td></tr></table><br><br>") ;
		printf ("</td></tr><tr>") ;
}



    printf ("</td></tr></table><br>") ;


	
    // List sizes
    listStart () ;
    listRow () ;
    listHead ('Size',15) ;
    listHeadIcon () ;
    listHead ('Active',15) ;
    listHeadIcon () ;
    // listHead ('Variant', 80)  ;
    listHead ('Variant', 60)  ;
    listHead('Transactions', 20);
    listHead ('Weight (g)', 20)  ;
    listHeadIcon () ;
    listHead ('PickStock', 25) ;
    listHead ('Position', 25) ;
    listHead ('PickContainer',25) ;
    listHead ('Presale?',20) ; //janis
    listHeadIcon ();

    while ($row = dbFetch($Result2)) {
    	listRow () ;
		navigationSetParameter ('storesingle',sprintf('&Ean=%s&StockId=%d&CollMemberId=%d', $row["VariantCode"],(int)$Record['FromStockId'],$Id)) ;
    	listField ($row["VariantSize"]) ;
    	if ($row['Id']>0)
			listFieldIcon ('move.gif', 'storesingle', (int)$Record['PickStockId']) ;
		else
			listField ('') ;
		if ($row['VarDisabled']) {
			$form_var[$row['ArticleSizeId']] = 0 ;
		   	listField ('No') ;
			listFieldIcon ('add.gif', 'varcolldis', (int)$row['ArticleSizeId']) ;
		} else {
			$form_var[$row['ArticleSizeId']] = 1 ;
		   	listField ('Yes') ;
			listFieldIcon ('delete.gif', 'varcolldis', (int)$row['ArticleSizeId']) ;
		}
    	listField ($row["VariantCode"]) ;
    	listFieldIcon ('table.gif', 'viewtransactionsreport', (int)$row['Id']) ;
    	listField (number_format($row["Weight"],0)) ;
    	if ($row['Id']>0)
			listFieldIcon ('stock.gif', 'variantinv', (int)$row['Id']) ;
		else
			listField ('') ;
    	listField ($row["StockName"]) ;
    	listField ($row["Position"]) ;
    	listField ($row["ContainerId"]) ;
    	if ((int)$row['Presale'] == 0) { //janis
    		listField('No');
    		listFieldIcon ('add.gif', 'savepresalecm', (int)$row['Id']);
    	} else {
    		listField('Yes');
    		listFieldIcon ('delete.gif', 'deletepresalecm', (int)$row['Id']);
    	}
    }
    listEnd () ;
	
	formStart () ;
	itemFieldRaw ('', formHidden('CollMemberId', $Id, 8,'','')) ;
	itemFieldRaw ('', formHidden('disable', 1, 8,'','')) ;
	if (is_array($form_var)) {
	foreach ($form_var as $varid => $value)
		itemFieldRaw ('', formHidden(sprintf('disable[%d]',(int)$varid), $value, 1,'','')) ;
	}
    formEnd () ;
    dbQueryFree ($Result2) ;
    
    function CheckForTransferInProgress($colMemberId){
        $query = sprintf ("
            SELECT 
            COUNT(cj.Id) as count 
            FROM cron_jobs cj
            WHERE 
            cj.done=0 AND 
            cj.type='shopify' AND 
            cj.internalid=%d
            ",
            $colMemberId
        ) ;
        $res = dbQuery ($query) ;
        $return = dbFetch ($res) ;
        dbQueryFree ($res) ;

        if((int)$return['count'] > 0){
            return 'yellow';
        }

        return true;
    }

    function CheckForCron($colMemberId, $count){
        $query = sprintf ("
            SELECT 
            cj.id
            FROM cron_jobs cj
            WHERE 
            cj.done=0 AND 
            cj.type='shopify' AND 
            cj.internalid=%d
            ORDER BY Id DESC
            LIMIT 1
            ",
            $colMemberId
        ) ;
        $res = dbQuery ($query) ;
        $return = dbFetch ($res) ;
        dbQueryFree ($res) ;
        if((int)$return['id'] > 0){
            return 'yellow';
        }

        $query = sprintf ("
            SELECT 
            cj.id,
            cj.error
            FROM cron_jobs cj
            WHERE 
            cj.done=1 AND
            cj.type='shopify' AND 
            cj.internalid=%d
            ORDER BY Id DESC
            LIMIT 1
            ",
            $colMemberId
        ) ;
        $res = dbQuery ($query) ;
        $returnsecond = dbFetch ($res) ;
        dbQueryFree ($res) ;
        if((int)$returnsecond['id'] > 0){
            if((string)$returnsecond['error'] !== ''){
                return 'red';
            }
        }

        if($count == 0){
            return 'red';
        }

        return 'green';
    }

    function CheckForShopifyId($articleId, $articleColorId, $colMemberId){
        $query = sprintf ("
            SELECT 
            COUNT(v.Id) as count
            FROM (variantcode v, shopifyvariant sv)
            WHERE 
            v.Active=1 AND 
            v.ArticleId=%d AND 
            v.ArticleColorId=%d AND
            sv.VariantCodeId=v.Id AND sv.ShopifyProductId != ''
            GROUP BY v.ArticleId, v.ArticleColorId, sv.ShopifyshopId LIMIT 1
            ", 
            $articleId, 
            $articleColorId
        ) ;
        $res = dbQuery ($query) ;
        $return = dbFetch ($res) ;
        dbQueryFree ($res) ;

        if((int)$return['count'] > 0){
            if(CheckForTransferInProgress($colMemberId) !== true){
            	return CheckForTransferInProgress($colMemberId);
            }
        }

        return CheckForCron($colMemberId, (int)$return['count']);
    }

    return 0 ;

?>
