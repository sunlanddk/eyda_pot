<?php

    require_once 'lib/table.inc' ;
	
//$actions = $_POST['disable'] ;
//die('gryf ' . $_POST['disable'][$Id]);


    // Enable/disable variant as part of collectionmemberisable 
	$_articlecolorid = tableGetField('collectionmember','ArticleColorId',(int)($_POST['CollMemberId'])) ;
	$_whereclause = sprintf('articlecolorid=%d and articlesizeid=%s and active=1', $_articlecolorid, $Id);
	$_variantcodeid = (int)tableGetFieldWhere('variantcode', 'Id', $_whereclause) ;

    if ((int)($_POST['disable'][$Id])==1) {
		$query = sprintf ("INSERT INTO `articlesizecolldisable` SET `articlesizeid`=%d, `collectionmemberid`=%d", $Id,(int)($_POST['CollMemberId'])) ;
		dbQuery ($query) ;
		if ($_variantcodeid>0)
			tableWrite('variantcode',array('disabled'=>1),$_variantcodeid) ;
	} else {
		$query = sprintf ("DELETE FROM `articlesizecolldisable` WHERE `articlesizeid`=%d and `collectionmemberid`=%d", $Id,(int)($_POST['CollMemberId'])) ;
		dbQuery ($query) ;
		if ($_variantcodeid>0)
			tableWrite('variantcode',array('disabled'=>0),$_variantcodeid) ;
	}

    return navigationCommandMark ('collmemberview', (int)($_POST['CollMemberId'])) ;
?>
