<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (    
		'SeasonId'			=> array ('type' => 'set'),
		'Name'				=> array ('check' => true),
		'Description'		=> array ('check' => true),
		'BrandId'			=> array ('check' => true),
		'RetailDiscount'	=> array ('type' => integer)
   ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'Name' :
			case 'Description' :
			case 'BrandId' :
				if (!$changed) return false ;
				break ;
	}

	if (!$changed) return false ;
		return true ;	
    }
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['SeasonId']['value']=(int)$Id ;
	    $Id = -1 ;
	    break ;

	default :
	    break ;
    }
     
    $res = saveFields ('Collection', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Collection
	    return navigationCommandMark ('collmemberlist', (int)$Record['Id']) ;
    }


    return 0 ;    
?>
