<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/variant.inc' ;
    require_once 'lib/form.inc' ;

	global $User, $Navigation, $Record, $Result2, $Size, $Id ;

//    if (dbNumRows($Result) == 0) return "No Collections assigned to this Season" ;
    printf ("<table><tr><td width=250>") ;
    itemStart () ;
    itemSpace () ;
    itemFieldIcon ('Season', $Record['SeasonName'], 'component.gif', 'seasonview', (int)$Record['SeasonId']) ; 
    itemFieldIcon ('Collection', $Record['CollectionName'],  'component.gif', 'collmemberlist', (int)$Record['CollectionId']) ; 
//    itemField ('Program', $Record['ProgramName']) ;
    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
    itemFieldIcon ('Company Coll.', ALL,  'component.gif', 'collcompanylst', (int)$Record['CompanyId']) ; 
    itemFieldIcon ('Article', tableGetField('Article','Number',$Record['ArticleId']) . ' ' . tableGetField('Article','Description',$Record['ArticleId']) , 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
	if ($Record['FabricArticleId']>0)
		itemFieldIcon ('Fabric', tableGetField('Article','Number',$Record['FabricArticleId']) . ' ' . tableGetField('Article','Description',$Record['FabricArticleId']) , 'article.gif', 'articleview', (int)$Record['FabricArticleId']) ;
    if ($Record['ArticleColorId'] > 0) {
	    $ColorId = tableGetField('ArticleColor', 'ColorId',$Record['ArticleColorId']) ;
	    itemField ('Colour', tableGetField('Color','Number', $ColorId) . '  ' . tableGetField('Color','Description', $ColorId)) ;
	} else {
	    itemField ('Colour', 'No Color') ;
	}
    if ($Record['Cancel']) {
	    itemSpace () ;
	    itemField ('Cancelled', $Record['CancelDescription']) ;
    } else
	    itemField ('LOT', $Record['LOT']) ;
	    itemField ('Focus style?', $Record['Focus']>0?'Yes':'No') ;
	    itemField ('On Web?', $Record['notonwebshop']>0?'No':'Yes') ;
    itemSpace () ;
    itemEnd () ;

    // List prices
    printf ("<table><tr><td width=250>") ;
    listStart () ;
    listRow () ;

    listHead ('PriceGroup', 95) ;
    listHead ('Currency', 95) ;
    listHead ('Wholesale', 95) ;
    listHead ('Retail', 95) ;
    listHead ('Campaign', 95) ;
	listhead ('Edit',30) ;
	
	$query = sprintf ("SELECT cmp.id as CollectionMemberPriceId, cpg.Name as PriceGroupName, c.Name as CurrencyName, cmp.WholeSalePrice, cmp.RetailPrice, cmp.CampaignPrice 
						from (CollectionMemberPrice cmp, Currency c )
						left join CollectionPriceGroup cpg ON cmp.collectionpricegroupid=cpg.id 
						Where cmp.CollectionMemberId=%d and cmp.CurrencyId=c.Id and cmp.Active=1 order by PriceGroupName, CurrencyName", $Id) ;
	$result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
    	listRow () ;
    	listField ($row["PriceGroupName"]) ;
    	listField ($row["CurrencyName"]) ;
    	listField ((float)$row["WholeSalePrice"]) ;
    	listField ($row["RetailPrice"]) ;
    	listField ($row["CampaignPrice"]) ;
		listFieldIcon ('edit.gif', 'addcmprice', (int)$row['CollectionMemberPriceId']) ;
    }
   	listRow () ;
	dbQueryFree ($result) ;
    listEnd () ;
    printf ("</td></tr></table><br>") ;

    printf ("</td><td>") ;
//	class="item-img" src="image/thumbnails/
	$photo =	'image/thumbnails/' . $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '.jpg' ;
    printf ('<img width=250px src="' . $photo .'">') ;
    printf ("</td></tr></table>") ;
    
    // List sizes
    listStart () ;
    listRow () ;
//    listHeadIcon ('Store') ;
//    listHead ('Article', 95) ;
//    listHead ('Description', 95) ;
//    listHead ('Color', 50) ;
//    listHead ('ColorDesc', 95) ;
    listHead ('Size', 15) ;
    listHeadIcon () ;
    listHead ('Active', 15) ;
    listHeadIcon () ;
    listHead ('Variant', 30)  ;
    listHeadIcon ('Store') ;
    listHead ('PickStock', 95) ;
    listHead ('Position', 45) ;
    listHead ('PickContainer', 95) ;

    while ($row = dbFetch($Result2)) {
    	listRow () ;
		navigationSetParameter ('storesingle',sprintf('&Ean=%s&StockId=%d&CollMemberId=%d', $row["VariantCode"],(int)$Record['FromStockId'],$Id)) ;
    	listField ($row["VariantSize"]) ;
    	if ($row['Id']>0)
			listFieldIcon ('move.gif', 'storesingle', (int)$Record['PickStockId']) ;
		else
			listField ('') ;
//    	listField ($row["ArticleNumber"]) ;
//    	listField ($row["VariantDescription"]) ;
//    	listField ($row["VariantNumber"]) ;
//    	listField ($row["VariantColor"]) ;
		if ($row['VarDisabled']) {
			$form_var[$row['ArticleSizeId']] = 0 ;
		   	listField ('No') ;
			listFieldIcon ('add.gif', 'varcolldis', (int)$row['ArticleSizeId']) ;
		} else {
			$form_var[$row['ArticleSizeId']] = 1 ;
		   	listField ('Yes') ;
			listFieldIcon ('delete.gif', 'varcolldis', (int)$row['ArticleSizeId']) ;
		}
    	listField ($row["VariantCode"]) ;
    	if ($row['Id']>0)
			listFieldIcon ('stock.gif', 'variantinv', (int)$row['Id']) ;
		else
			listField ('') ;
    	listField ($row["StockName"]) ;
    	listField ($row["Position"]) ;
    	listField ($row["ContainerId"]) ;
    }
    listEnd () ;
	formStart () ;
	itemFieldRaw ('', formHidden('CollMemberId', $Id, 8,'','')) ;
	itemFieldRaw ('', formHidden('disable', 1, 8,'','')) ;
	if (is_array($form_var)) {
	foreach ($form_var as $varid => $value)
		itemFieldRaw ('', formHidden(sprintf('disable[%d]',(int)$varid), $value, 1,'','')) ;
	}
    formEnd () ;
    dbQueryFree ($Result2) ;

    return 0 ;

?>
