<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
//return 'her ' . $Id ;
	$_persize = 1 ;
	if ($_persize) {
		switch ($Navigation['Parameters']) {
		case 'new' :
			unset ($Record) ;
			itemStart () ;
			itemSpace () ;
			$query = sprintf ("SELECT ArticleId, ArticleColorId FROM CollectionMember WHERE Id=%d AND Active=1", $Id) ;
			$res = dbQuery ($query) ;
			$row = dbFetch ($res) ;
			dbQueryFree ($res) ;
			itemField ('Article', tableGetField('Article','Number',$row['ArticleId']) . ' ' . tableGetField('Article','Description',$row['ArticleId'])) ;
			if ($row['ArticleColorId'] > 0) {
				$ColorId = tableGetField('ArticleColor', 'ColorId',$row['ArticleColorId']) ;
				itemField ('Color', tableGetField('Color','Number', $ColorId) . '  ' . tableGetField('Color','Description', $ColorId)) ;
			} else {
				itemField ('Color', 'No Color') ;
			}
			itemSpace () ;

			formStart () ;
			itemFieldRaw ('Currency', formDBSelect ('CurrencyId', 0, sprintf("Select 0 as Id, '-Select-' as Value Union SELECT c.Id, c.Name AS Value
																		FROM Currency c
																		left join collectionmemberprice cmp on cmp.currencyid=c.id and cmp.collectionmemberid=%d and cmp.active=1
																		where isnull(cmp.id) and c.active=1
																		ORDER BY Value", $Id), 'width:200px')) ; 
			itemSpace () ;
			itemFieldRaw ('Set prices for all colors ', formCheckbox ('setall', 0) . ' (of same article in this collection)') ;
			itemSpace () ;
			itemEnd () ;

			$query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1", tableGetField('CollectionMember','ArticleId',$Id)) ;
			break ;
			
		  default:
			itemStart () ;
			itemSpace () ;
			itemField ('Currency', TableGetField('Currency','Name',$Record['CurrencyId'])) ;
			itemSpace () ;
			formStart () ;
			itemFieldRaw ('Set prices for all colors ', formCheckbox ('setall', 0) . ' (of same article in this collection)') ;
			itemSpace () ;
			itemEnd () ;
			$query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder 
								FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1",$Record['ArticleId']) ;

			$query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder, CollectionmemberPrice.Id as CollectionmemberPriceId, 
										CollectionmemberPriceSize.WholeSalePrice, CollectionmemberPriceSize.RetailPrice, CollectionmemberPriceSize.CampaignPrice, CollectionmemberPriceSize.PurchasePrice, CollectionmemberPriceSize.LogisticCost
						FROM ArticleSize
						INNER JOIN CollectionmemberPrice ON CollectionmemberPrice.Id=%d  AND CollectionmemberPrice.Active=1
						LEFT JOIN CollectionmemberPriceSize ON CollectionmemberPriceSize.ArticleSizeId=ArticleSize.Id AND CollectionmemberPriceSize.CollectionMemberPriceId=CollectionMemberPrice.Id AND CollectionmemberPriceSize.Active=1
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
								
			
		}
		$result = dbQuery ($query) ;

		$Size = array() ;
		while ($row = dbFetch ($result)) {
			$Size[$row['DisplayOrder']] = $row ;
		}
		// Form
		listStart () ;
		listHeader ('Prices') ;
		listRow () ;
		if (count($Size) > 0) {
			listHead('', 60) ;
			foreach ($Size as $i => $s) 
				listHead (htmlentities($s['Name']), 60) ;
			listRow () ;
			listField ('Wholesale') ;
			foreach ($Size as $i => $s) 
				listFieldRaw (formText('WholeSalePrice['.$s['Id'].']', number_format ((float)$s['WholeSalePrice'], (int)2, ',', ''), 6, 'text-align:right;')) ;
			listRow () ;
			listRow () ;
			listField ('Retail') ;
			foreach ($Size as $i => $s) 
				listFieldRaw (formText('RetailPrice['.$s['Id'].']', number_format ((float)$s['RetailPrice'], (int)2, ',', ''), 6, 'text-align:right;')) ;
			listRow () ;
			listRow () ;
			listField ('Campaign') ;
			foreach ($Size as $i => $s) 
				listFieldRaw (formText('CampaignPrice['.$s['Id'].']', number_format ((float)$s['CampaignPrice'], (int)2, ',', ''), 6, 'text-align:right;')) ;
			listRow () ;
			listRow () ;
			listField ('Purchase Price') ;
			foreach ($Size as $i => $s) 
				listFieldRaw (formText('PurchasePrice['.$s['Id'].']', number_format ((float)$s['PurchasePrice'], (int)2, ',', ''), 6, 'text-align:right;')) ;
			listRow () ;
			listRow () ;
			if ((int)$Record['CostCurrencyId']>0) {
				$_currencySymbol = tableGetField('Currency','Symbol',$Record['CostCurrencyId']); 
			} else
				$_currencySymbol = 'undef' ;
			listField ('L Cost (' . $_currencySymbol.')') ;
			foreach ($Size as $i => $s) 
				listFieldRaw (formText('LogisticCost['.$s['Id'].']', number_format ((float)$s['LogisticCost'], (int)2, ',', ''), 6, 'text-align:right;')) ;
		}
		printf ("</td></tr></table><br><br>") ;

		itemSpace () ;
		itemFieldRaw ('Show 0 on B2B ', formCheckbox ('ShowZeroB2B', $Record['ShowZeroB2B']) . ' (if set then style is shown for sale in B2B shop in this currency even if price is zero)') ;
		printf ("<br><br>") ;
		itemSpace () ;

		itemInfo ($Record) ;
		
		itemEnd () ;
		formEnd () ;

	} else {
		switch ($Navigation['Parameters']) {
		case 'new' :
			unset ($Record) ;
			formStart () ;
			itemStart () ;
			itemHeader () ;
			itemFieldRaw ('Currency', formDBSelect ('CurrencyId', 0, sprintf("Select 0 as Id, '-Select-' as Value Union SELECT c.Id, c.Name AS Value
																		FROM Currency c
																		left join collectionmemberprice cmp on cmp.currencyid=c.id and cmp.collectionmemberid=%d and cmp.active=1
																		where isnull(cmp.id) and c.active=1
																		ORDER BY Value", $Id), 'width:200px')) ; 
			break ;
			
		  default:
			itemStart () ;
			itemSpace () ;
			itemField ('Currency', TableGetField('Currency','Name',$Record[CurrencyId])) ;
			itemSpace () ;
			itemEnd () ;
			
			formStart () ;
			itemStart () ;
			itemHeader () ;
		}

		itemSpace () ;

		itemFieldRaw ('Wholesale Price', formText ('WholeSalePrice', $Record[WholesalePrice], 12, 'text-align:right;')) ;
		itemFieldRaw ('Retail Price', formText ('RetailPrice', $Record[RetailPrice], 12, 'text-align:right;')) ;	
		itemFieldRaw ('Campaign Price', formText ('CampaignPrice', $Record[CampaignPrice], 12, 'text-align:right;')) ;	
		itemFieldRaw ('Pricegoup', formDBSelect ('PriceGroupId', $Record[PriceGroupId], "Select 0 as Id, '-Select-' as Value Union SELECT CollectionPriceGroup.Id, CollectionPriceGroup.Name AS Value FROM CollectionPriceGroup ORDER BY Value", 'width:200px')) ; 
		
		itemSpace () ;

		itemInfo ($Record) ;
		
		itemEnd () ;
		formEnd () ;
	}
	
    return 0 ;
?>
