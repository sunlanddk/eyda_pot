<?php
	require_once('lib/table.inc');

	$colIds = [];

    foreach ($_POST['UpdateFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		array_push($colIds, $id);
		// Insert code below for inserting records in cronjobs for collectionmember defined by $id.
		$post = array(
			'type'				=> 'shopify',
			'internalid' 		=> $id,
			'cron_name' 		=> 'Update product',
			'cron_function' 	=> 'publishProduct',
			'cron_value' 		=> $id,
			'cron_scheduled_at'	=> dbDateEncode(time()),
		);
		tableWriteCron('cron_jobs', $post);

		$post = array(
			'Cancel' => 0,
			'notonwebshop' => 0
		);
		tableWrite('collectionmember', $post, $id);
	}

    foreach ($_POST['UpdateSetFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		// Insert code below for inserting records in cronjobs for collectionmember defined by $id.
		array_push($colIds, $id);
		$post = array(
			'type'				=> 'shopify',
			'internalid' 		=> $id,
			'cron_name' 		=> 'Update product',
			'cron_function' 	=> 'publishProduct',
			'cron_value' 		=> $id,
			'cron_scheduled_at'	=> dbDateEncode(time()),
		);
		tableWriteCron('cron_jobs', $post);

		$post = array(
			'Cancel' => 0,
			'notonwebshop' => 0
		);
		tableWrite('collectionmember', $post, $id);
	}

    return 0 ;
?>
