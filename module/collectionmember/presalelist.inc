<?php 

	require_once 'lib/list.inc' ;
	require_once 'lib/form.inc' ;
	require_once 'lib/table.inc' ;


	function getAllPresaleVcs() {
		$sql = sprintf("SELECT vc.*  FROM variantcode vc WHERE vc.Active = 1 AND vc.Presale = 1");
		$result = dbQuery($sql);
		$out = [];
		while($row = dbFetch($result)) {
			$out[] = $row;
		}
		dbQueryFree($result);
		return $out;
	}

	formStart();
	listStart();
	listRow();
	listHead('Select', 50);
	listHead('Variantcode', 100);
	listHead('Article', 80);
	listHead('Description', 200);
	listHead('Color', 120);
	listHead('Size', 80);
	listHead();

	$vcs = getAllPresaleVcs();
	if (empty($vcs)) {
		listRow();
		listField('No variants currently in presale');
	} else {

		foreach($vcs as $vc) {
			listRow();
			listFieldRaw(formCheckbox('Variantcodeid['. $vc['Id'] .']'));
			listField($vc['VariantCode']);
			listField( tableGetField('article', 'Number', $vc['ArticleId']) );
			listField( tableGetField('article', 'Description', $vc['ArticleId']) );
			listField( tableGetField('color', 'Description', tableGetField('articlecolor', 'ColorId', $vc['ArticleColorId'])) );
			listField( tableGetField('articlesize', 'Name', $vc['ArticleSizeId']) );
			listField('');
		}
			

	}
	

	listEnd();
	formEnd();

?>
<script type="text/javascript">
	function CheckAll() {
		$('[name*="Variantcodeid["').prop('checked', true);
		console.log('whoop');
	}
</script>
<?php
	return 0;
?>