<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    // Default values
	    $Record['SeasonId'] = $Id ;
	    break ;
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('Collection', $Record['Name']) ;
	    itemSpace () ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 30)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
 	itemFieldRaw ('Brand', formDBSelect ('BrandId', $Record['BrandId'], 'SELECT Brand.Id, Brand.Name AS Value FROM Brand WHERE Active=1 ORDER BY Value', 'width:205px')) ; 
    itemFieldRaw ('RetailDiscount', formText ('RetailDiscount', $Record['RetailDiscount'], 30) . '%') ;
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
