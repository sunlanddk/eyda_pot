<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
	require_once 'lib/variant.inc' ;

	// return 'scr ' ; 
	
	if ($Navigation['Parameters'] == 'new') {
		// Create Assortment record
		$Assortment['CollectionMemberId'] = (int)$Id;
		$AssortmentId = tableWrite('Assortment',$Assortment) ;
		
		// Create Collmemberassortment records.
		foreach($_POST['Qty'] as $key => $value) {
			$CollectionmemberAssortment['CollectionMemberId'] = (int)$Id;
			$CollectionmemberAssortment['AssortmentId'] = (int)$AssortmentId;
			$CollectionmemberAssortment['ArticleSizeId'] = (int)$key;
			$CollectionmemberAssortment['Quantity'] = (int)$value;
			tableWrite('CollectionmemberAssortment',$CollectionmemberAssortment) ;
		}
		
		// Generate EAN for assortment
		// Find last ean according seasen basecode.
	    $query = sprintf("SELECT BaseVarCode FROM Season, Collection, CollectionMember where  CollectionMember.Id=%d and Collection.Id=CollectionMember.CollectionId and Season.Id=Collection.SeasonId", $Id);
		$res = dbQuery ($query) ;
		$baserow = dbFetch ($res) ;
	    $query = "SELECT max(variantcode) as MaxCode FROM variantcode where active=1 and variantcode like '" . $baserow['BaseVarCode'] . "%'" ;
		$res = dbQuery ($query) ;
		if (dbNumRows($res)==0) {
			$baserow['BaseVarCode'] = $baserow['BaseVarCode'] . '00001' ; // First code in new serie!
		} else {
			$row = dbFetch ($res) ;
			$baserow['BaseVarCode'] = substr($row['MaxCode'],0,12) ;		// Continue from last number ion series
		}
		dbQueryFree ($res) ;
		// if no Basecode defined then just find last (max) ean
		if (strlen($baserow['BaseVarCode'])==0){
			$query = sprintf("SELECT max(variantcode) as VariantCode FROM variantcode") ;
			$res = dbQuery ($query) ;
			$row = dbFetch ($res) ;
			dbQueryFree($res) ;
		
			$BaseVariantCode = (float)substr($row['VariantCode'],0,12) ;
		} else {
			$BaseVariantCode = $baserow['BaseVarCode'] ;		// 
		}
		$VariantCode = array (	
			'VariantCode'		=>  "",			// EAN code
			'VariantUnit'		=>  "Pcs",		// 			
			'VariantmodelRef'	=>  "",			
			'VariantDescription'	=> "",			
			'VariantColorDesc'	=>  "",		
			'VariantColorCode'	=>  "",			
			'VariantSize'		=>  "",			
			'ArticleId'			=>	53569,			// Article id for pseudo article 9999000000 Assortments
			'ArticleColorId'	=>	0,				// 
			'ArticleSizeId'		=>	0,				// 
			'Reference'			=>	$AssortmentId,	// Connected assortmentid
			'Type'				=>	"assort"
		) ;
 		
		$barcode = sprintf("%12.0f", ($BaseVariantCode + 1)) ;
		$barcode .= GetCheckDigit($barcode); 			// Add control digit
		$VariantCode['VariantCode'] = $barcode;
		tableWrite ('VariantCode', $VariantCode) ;

		return navigationCommandMark ('collmemberview', (int)$Id) ;
	} else {
		foreach($_POST['Qty'] as $key => $value) {
			$CollectionmemberAssortment['Quantity'] = (int)$value;
			tableWrite('CollectionmemberAssortment',$CollectionmemberAssortment,$key) ;
		}
	}
 
 return 0 ;    

?>
