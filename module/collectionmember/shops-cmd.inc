<?php 
require_once('lib/table.inc');

foreach ($_POST['shop'] as $key => $shop) {
	$shopColId = (int)tableGetFieldWhere('inshopifyshops', 'Id', sprintf("CollectionMemberId=%d AND ShopifyShopId=%d", $Id, (int)$shop));
	if(isset($_POST['inshop_'.$shop]) === true){
		$post = array(
			'Active' => 1,
			'ShopifyShopId' => (int)$shop,
			'CollectionMemberId' => $Id,
		);
		tableWriteSimple('inshopifyshops', $post, $shopColId);
	} 
	else{
		// $post = array(
		// 	'Active' => 0,
		// 	'ShopifyShopId' => (int)$shop,
		// 	'CollectionMemberId' => $Id,
		// );
		// tableWriteSimple('inshopifyshops', $post, $shopColId);
	}

}

return 0;