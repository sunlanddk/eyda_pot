<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;

	function createthumb($name,$filename,$new_w,$new_h){
		$system=explode('.',$name);
		if (preg_match('/jpg|jpeg/',$system[1])){
			$src_img=imagecreatefromjpeg($name);
		}
		if (preg_match('/png/',$system[1])){
			$src_img=imagecreatefrompng($name);
		}

		// Check dimensions
		$old_x=imageSX($src_img);
		$old_y=imageSY($src_img);
//die('her ' . $name . ' - ' . $src_img . ' - ' . $old_x) ;
		if ($new_w>0) {
			if ($old_x > $old_y) {
				$thumb_w=$new_w;
				$thumb_h=$old_y*($new_h/$old_x);
			}
			if ($old_x < $old_y) {
				$thumb_w=$old_x*($new_w/$old_y);
				$thumb_h=$new_h;
			}
			if ($old_x == $old_y) {
				$thumb_w=$new_w;
				$thumb_h=$new_h;
			}
		} else {
			$thumb_w=$old_x*($new_h/$old_x);
			$thumb_h=$new_h;
		}
		
		$dst_img=ImageCreateTrueColor($thumb_w,$thumb_h);
		imagecopyresampled($dst_img,$src_img,0,0,0,0,$thumb_w,$thumb_h,$old_x,$old_y);

		if (preg_match("/png/",$system[1]))
		{
			imagepng($dst_img,$filename); 
		} else {
			imagejpeg($dst_img,$filename); 
		}
		imagedestroy($dst_img); 
		imagedestroy($src_img); 
	}
	
	function createcrop($name,$filename,$new_w,$new_h, $adj_x, $adj_y) {
		$system=explode('.',$name);
		if (preg_match('/jpg|jpeg/',$system[1])){
			$src_img=imagecreatefromjpeg($name);
		}
		if (preg_match('/png/',$system[1])){
			$src_img=imagecreatefrompng($name);
		}

		// Check dimensions
		$x=(imageSX($src_img)/2) - ($old_x/2) + $adj_x ;
		$y=(imageSY($src_img)/2) - ($old_y/2) + $adj_y ;
/*		
		$_arr = array (
			'x' 	=> $x,
			'y' 	=> $y,
			'width' 	=> $new_w,
			'height' 	=> $new_h
		) ;
		$dst_img=imagecrop($src_img,$_arr) ;
*/
		$dst_img = imagecreatetruecolor($new_w, $new_h );
		imagecopyresampled($dst_img, $src_img, 0, 0, $x, $y, $new_w, $new_h, $new_w, $new_h );		

		if (preg_match("/png/",$system[1]))
		{
			imagepng($dst_img,$filename); 
		} else {
			imagejpeg($dst_img,$filename); 
		}
		imagedestroy($dst_img); 
		imagedestroy($dst_img1); 
		imagedestroy($src_img); 
	}
   $fields = array (    
		'Core'			=> array ('type' => 'checkbox','check' => true),
		'Cancel'			=> array ('type' => 'checkbox','check' => true),
		'Discontinued'		=> array ('type' => 'checkbox','check' => true),
		'Focus'				=> array ('type' => 'checkbox','check' => true),
		'notonwebshop'		=> array ('type' => 'set'),
		'WebFrontpage'		=> array ('type' => 'checkbox','check' => true),
		'ShowOnB2B'			=> array ('type' => 'checkbox','check' => true),
		'SeasonName'		=> array (),
		'CancelDescription'	=> array ('check' => true),
		'CollectionLOTId'	=> array ('check' => true),
	//	'WebCampaign'		=> array ('check' => true),
		'WebFitGroup'		=> array ('check' => true),
		'IconAdjX'			=> array ('type' => 'integer'),
		'IconAdjY'			=> array ('type' => 'integer')
   ) ;         
   
   $ArticleNumber=$Record['ArticleNumber'] ;
   $ColorNumber=$Record['ColorNumber'] ;
// return $ArticleNumber . '_' . $ColorNumber . '.jpg' ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'Cancel' :
			case 'Discontinued' :
			case 'Focus' :
			case 'notonwebshop' :
			case 'WebFrontpage' :
			case 'ShowOnB2B' :
			case 'CancelDescription' :
			case 'WebFitGroup' :
			case 'WebCampaign' :
			case 'CollectionLOTId' :
				if (!$changed) return false ;
				break ;
		}

		if (!$changed) return false ;
		return true ;	
    }

	$fields['notonwebshop']['value'] = $_POST['onwebshop']?0:1 ;
	
	$Program = array (
			'CollectionmemberId' => $Id,
			'ProgramId' => $_POST['ProgramId']
	) ;
/*
	if ($Record['CollectionmemberProgramId']>0) {
//		return 'her ' . $Record['CollectionmemberProgramId'] ;
			tableWrite ('CollectionMemberProgram', $Program, $Record['CollectionmemberProgramId']) ; 
	} else {
			$CollectionMemberProgramId = tableWrite ('CollectionMemberProgram', $Program) ;
	}
*/

    $res = saveFields ('CollectionMember', $Id, $fields, true) ;
    if ($res) return $res ;

	function upload_my($upload_file_name, $target_name) {
		switch ($_FILES[$upload_file_name]['error']) {
			case 0: 
			if (!is_uploaded_file($_FILES[$upload_file_name]['tmp_name'])) {
				return 'photo upload failed, file not uploaded' ;
			}
			if ($_FILES[$upload_file_name]['size'] != filesize($_FILES[$upload_file_name]['tmp_name']))
				return 'invalid photo size' ;
			if ($_FILES[$upload_file_name]['size'] > 10000000)
				return 'photo too big, max 50 MB' ;

			// Set flag for photo upload	
			$photoUpload = true ;		    
			break ;
			
			case 4: 		// No file specified
			$photoUpload = false ;
			break ;
			
			default: return sprintf ('Doc1ument upload failed, code %d', $_FILES[$upload_file_name]['error']) ;
		}
		
		if ($photoUpload) {
			// Archive photo
			require_once 'lib/file.inc' ;
			$file =	'image/thumbnails/' . $target_name . '.jpg' ;
			if (!move_uploaded_file ($_FILES[$upload_file_name]['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive Doc1ument %s', __FILE__, __LINE__, $file) ;
			chmod ($file, 0777) ;
		}
		return $photoUpload ;
	}
	$_dir = $Config['photoLib']; 

	// Upload main picture
	$_file = $ArticleNumber . '_' . $ColorNumber . '_upl' ;
	$file = $_dir . $_file . '.jpg' ;
	$file_thump =	$_dir . $ArticleNumber . '_' . $ColorNumber . '.jpg' ;
	$res=upload_my('Doc', $_file) ;
	if ($res) {
//return 'her' ;
		// thumpnails
		$file_thump =	$_dir . $ArticleNumber . '_' . $ColorNumber . '.jpg' ;
		$file_mini =	$_dir . $ArticleNumber . '_' . $ColorNumber . '_mini.jpg' ;
		createthumb($file,$file_thump,600,600);
		createthumb($file,$file_mini,100,100);
		chmod ($file_thump, 0777) ;
		chmod ($file_mini, 0777) ;
		$file_icon =	$_dir . $ArticleNumber . '_' . $ColorNumber . '_icon.jpg' ;
		createcrop($file_thump,$file_icon,80,80, (int)$_POST['IconAdjX']-75, (int)$_POST['IconAdjY']-30);
		chmod ($file_icon, 0777) ;
	}

	if ($_POST['DeleteMain']=='on') {
		unlink($file) ;
		unlink($file_thump) ;
	} 
	
	// Upload detail picture 1
	$_file = $ArticleNumber . '_' . $ColorNumber . '_upl_d1' ;
	$res=upload_my('Doc1', $_file) ;

	$file = $_dir . $_file . '.jpg' ;
	$file_thump =	$_dir . $ArticleNumber . '_' . $ColorNumber  . '_d1' . '.jpg' ;
	if ($res) {
		createthumb($file,$file_thump,600,600);
		chmod ($file_thump, 0777) ;
	}
	if ($_POST['DeleteD1']=='on') {
		unlink($file) ;
		unlink($file_thump) ;
	} 

	// Upload detail picture 2
	$_file = $ArticleNumber . '_' . $ColorNumber . '_upl_d2' ;
	$file = $_dir . $_file . '.jpg' ;
	$file_thump =	$_dir . $ArticleNumber . '_' . $ColorNumber  . '_d2' . '.jpg' ;
	$res=upload_my('Doc2', $_file) ;
	if ($res) {
		createthumb($file,$file_thump,600,600);
		chmod ($file_thump, 0777) ;
	}
	if ($_POST['DeleteD2']=='on') {
		unlink($file) ;
		unlink($file_thump) ;
	} 

	
	$file_thump =	$_dir . $ArticleNumber . '_' . $ColorNumber . '.jpg' ;
	$file_icon =	$_dir . $ArticleNumber . '_' . $ColorNumber . '_icon.jpg' ;
	createcrop($file_thump,$file_icon,80,80, (int)$_POST['IconAdjX']-75, (int)$_POST['IconAdjY']-30);
	chmod ($file_icon, 0777) ;

    return 0 ;    
?>
