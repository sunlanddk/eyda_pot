<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    itemStart () ;
    itemSpace () ;
    itemFieldIcon ('Project', $Record['Name'], 'component.gif', 'seasonview', $Id) ; 
//    itemField ('Collection', $Record['CollectionName']) ; 
    itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
    itemSpace () ;
    itemEnd () ;


    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Article', 70)  ;
    listHead ('Description', 150) ;
    listHead ('Supplier', 70) ;
    listHead ('Color', 70) ;
    listHead ('ColorDesc', 120) ;
    listHead ('ColorRef', 70)  ;
    listHead ('SizeDim', 40)  ;
    if ($ShowProduction) {
	listHead ('Production', 70) ;
	listHead ('Material', 70) ;
    }
    listHead ('Unit', 30)  ;
    listHead ('Consumption', 50)  ;
    listHead ('Allocated', 50)  ;
    listHead ('', 12)  ;
    if ($ShowProduction) {
      listHead ('Free', 42, 'align=right')  ;
      listHead ('ThisCase', 45)  ;
      listHead ('AllCases', 45)  ;
    }
    $Article = '' ;
    $Color = '';
    $SizeDim = '';
    while ($row = dbFetch($Result)) {
	// Get quantity information
//       				round(coalesce(sum(if (productionid=0 and (caseid=0 or caseid=%d) and containerid>0, quantity, 0)),0)) As FreeQty,
//       				round(coalesce(sum(if ((caseid=0 or caseid=%d) and containerid>0 and not productionid=%d, quantity, 0)),0)) As FreeQty,
	$query = sprintf ("select articleid,articlecolorid,articlesizeid, dimension,
		round(coalesce(sum(if(Item.ConsumeProductionId=%d,Item.Quantity,0)) + sum(if(productionid=%d and caseid>0 and Item.containerid>0,Item.Quantity,0)),0)) as AllocQty,
		round(coalesce(sum(if ((caseid=0) and containerid>0, quantity, 0)),0)) As FreeQty,
       	round(coalesce(sum(if (productionid=0 and caseid=%d and containerid>0, quantity, 0)),0)) As FreeCaseAllocQty,
       	round(coalesce(sum(if (productionid=0 and caseid>0 and containerid>0, quantity, 0)),0)) As CaseAllocQty
		FROM item
				 left join container on item.ContainerId=container.Id
				 left join stock on Container.StockId=Stock.Id
				 where articleid=%d and articlecolorid=%d and articlesizeid=%d and dimension=%d
					and Item.active=1
					and (not Stock.type='Shipment' or stock.id is null)
					and (productionid=0 or (productionid=%d and caseid>0) or caseid=0)
				 group by articleid,articlecolorid,articlesizeid, dimension", 
				$row['ProdId'],$row['ProdId'],$row['CaseId'],$row['ArticleId'],$row['FinalMaterialArticleColorid'], $row['MaterialArticleSizeId'], $row['MaterialArticleDimension'],$row['ProdId']) ;
//If ($row['ArticleId']==23910 and $row['ProdId']==23603) return $query . "!" .  $row['MaterialSizeDimensions'] . "!" .  $row['MaterialArticleSizeid'] ;
	$res1 = dbQuery ($query) ;
	$qty_row = dbFetch ($res1) ;
	dbQueryFree ($res1) ;

	if ((int)$qty_row['AllocQty'] < (int)$row['TotalNeed']) {					
		if ((int)$qty_row['FreeQty'] > 0 or (int)$qty_row['FreeCaseAllocQty'] > 0) {	
			$ActionRequired = '#' ;						// Available on stock 
		} else {											
			$ActionRequired = '-' ;						// Need purchase
		}
	} else {
		if ((int)$qty_row['AllocQty'] > (int)(1.1*$row['TotalNeed'])) {					
			$ActionRequired = '!' ;							// Surplus above 10%
		} else {											
			$ActionRequired = '' ;							// Fullfilled
		}
	}

	// Case allocated "free" items
	if ($qty_row['CaseAllocQty'] > 0) {
		$CaseAllocQty = $qty_row['CaseAllocQty'] ;
	} else {
		$CaseAllocQty = '' ;
	}					
	if ($qty_row['FreeCaseAllocQty'] > 0 ) {
		$FreeCaseAllocQty = $qty_row['FreeCaseAllocQty'] ;
	} else {
		$FreeCaseAllocQty = '' ;
	}	
	if ($qty_row['FreeQty'] > 0 ) {
		$FreeQty = $qty_row['FreeQty'] ;
	} else {
		$FreeQty = '' ;
	}	

    	listRow () ;
	if ($Article == $row["MaterialArticleNumber"] and $Color == $row["ColorNumber"] and $SizeDim == $row["MaterialSizeDimensions"]) {
		listFieldIcon ('article.gif', 'artrawmatview', (int)$row['ArticleId']) ;
	    	listField ('') ;
	    	listField ('') ;
	    	listField ('') ;
	    	listField ('') ;
	    	listField ('') ;
	    	listField ('') ;
	    	listField ('') ;
		if ($ShowProduction) {
		 listField ($row["Production"]) ;
		 listField ($row["MaterialDate"]) ;
		}
	      listField ($row["UnitName"]) ;
	      listField ($row["TotalNeed"], 'align=right') ;
	      listField ($qty_row["AllocQty"], 'align=right') ;
	      listField ($ActionRequired) ;
	    if ($ShowProduction) {
	      listField ($FreeQty, 'align=right') ;
	      listField ($FreeCaseAllocQty, 'align=right') ;
	      listField ($CaseAllocQty, 'align=right') ;
	    }
	} else {
		listFieldIcon ('article.gif', 'artrawmatview', (int)$row['ArticleId']) ;
	    	listField ($row["MaterialArticleNumber"]) ;
	    	listField ($row["MaterialArticleDescription"]) ;
	    	listField ($row["suppliercompany"]) ;
	    	listField ($row["ColorNumber"]) ;
	    	listField ($row["ColorDescription"]) ;
	    	listField ($row["ArticleColorReference"]) ;
	    	listField ($row["MaterialSizeDimensions"]) ;
		if ($ShowProduction) {
		 listField ($row["Production"]) ;
		 listField ($row["MaterialDate"]) ;
		}
	      listField ($row["UnitName"]) ;
	      listField ($row["TotalNeed"], 'align=right') ;
	      listField ($qty_row["AllocQty"], 'align=right') ;
	      listField ($ActionRequired) ;
	    if ($ShowProduction) {
	      listField ($FreeQty, 'align=right') ;
	      listField ($FreeCaseAllocQty, 'align=right') ;
	      listField ($CaseAllocQty, 'align=right') ;
	    }
	}
      $Article = $row["MaterialArticleNumber"] ;
      $Color = $row["ColorNumber"];
      $SizeDim = $row["MaterialSizeDimensions"];
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
