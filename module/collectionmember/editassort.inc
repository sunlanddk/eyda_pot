<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
	require_once 'lib/navigation.inc' ;

	if ($Navigation['Parameters'] == 'new') {
		$query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1", tableGetField('CollectionMember','ArticleId',$Id)) ;
	} else {
		itemStart () ;
		itemSpace () ;
		itemField ('Assortment', $Id) ;
		itemSpace () ;
		itemEnd () ;

		// Assortments
		$query = sprintf ("SELECT CollectionmemberAssortment.Id, ArticleSize.Name, ArticleSize.DisplayOrder, CollectionmemberAssortment.AssortmentId, CollectionmemberAssortment.Quantity 
							FROM CollectionmemberAssortment, ArticleSize
							WHERE CollectionmemberAssortment.AssortmentId=%d AND CollectionmemberAssortment.Active=1 AND
								 ArticleSize.Id=CollectionmemberAssortment.ArticleSizeId AND ArticleSize.Active=1
							ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id) ;
	}
	$result = dbQuery ($query) ;

	$Size = array() ;
	while ($row = dbFetch ($result)) {
		$Size[$row['DisplayOrder']] = $row ;
	}
	dbQueryFree ($result) ;

    // Form
    formStart () ;
	listStart () ;
	listRow () ;
	if (count($Size) > 0) {
		foreach ($Size as $i => $s) 
			listHead (htmlentities($s['Name']), 60) ;
		listRow () ;
		foreach ($Size as $i => $s) 
			listFieldRaw (formText('Qty['.$s['Id'].']', number_format ((int)$s['Quantity'], (int)0, ',', '.'), 6, 'text-align:right;')) ;
	}
	listRow () ;
	listEnd () ;
	printf ("</td></tr></table><br><br>") ;

	
    itemStart () ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;

    itemFieldRaw ('Wholesale Price', formText ('WholeSalePrice', $Record[WholesalePrice], 12, 'text-align:right;')) ;
    itemFieldRaw ('Retail Price', formText ('RetailPrice', $Record[RetailPrice], 12, 'text-align:right;')) ;	
    itemFieldRaw ('Campaign Price', formText ('CampaignPrice', $Record[CampaignPrice], 12, 'text-align:right;')) ;	
 	itemFieldRaw ('Pricegoup', formDBSelect ('PriceGroupId', $Record[PriceGroupId], "Select 0 as Id, '-Select-' as Value Union SELECT CollectionPriceGroup.Id, CollectionPriceGroup.Name AS Value FROM CollectionPriceGroup ORDER BY Value", 'width:200px')) ; 
    
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
