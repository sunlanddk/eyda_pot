<script type="text/javascript" >
	function CheckAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			if (!e.name.indexOf('inshop_')) continue ;
			e.checked = true ;
		}
	}
	function CheckAllOnWebshop () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			if (!e.name.indexOf('UpdateSetFlag')) continue ;
			if (!e.name.indexOf('inshop_')) continue ;
			if($(e).parent().find('.cancelled').val() == 1) continue;
			e.checked = true ;
		}
	}

	function ClearAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			e.checked = false ;
		}
	}
</script>

<?php
    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;
	
    formStart () ;
    itemStart () ;

    function getActiveSites($colMemberId){
    	$query = sprintf (
	    	"SELECT 
				ss.Name
			FROM (shopifyshops ss, inshopifyshops iss)
			WHERE ss.Active=1 AND ss.Id=iss.ShopifyShopId AND iss.Active=1 AND iss.CollectionMemberId=%d ORDER BY ss.Id;
	    	", 
	    	(int)$colMemberId
	    ) ;
	    $results = dbQuery ($query) ;
	    $shops = [];
		while ($row = dbFetch ($results)) {
	        array_push($shops, $row['Name']);
	    }
	    return $shops;
    }

    $query = sprintf (
    	"SELECT 
    		ss.Id,
			ss.Name
		FROM (shopifyshops ss)
		WHERE ss.Active=1 ORDER BY ss.Id;
    	", 
    	(int)$Id
    ) ;
    $result = dbQuery ($query) ;


    listClear () ;
    listStart () ;
    listRow () ;

    listHead ('Shopify Shops', 30) ;
    $collectionmembers = array() ;
    $html = '';
    while ($row = dbFetch ($result)) {
    	listHead ($row['Name'], 30) ;
        array_push($collectionmembers, $row);
    }

    listRow () ;
    listField ('Active') ;
    foreach ($collectionmembers as $key => $row) {
    	listFieldRaw ('<input type=hidden name="shop['.$row['Id'].']" value="'.$row['Id'].'">'.formCheckbox('inshop_'.$row['Id'], (int)$row['Active'])) ;
    }

    dbQueryFree ($result) ;

    listEnd () ;

    printf('<br/>');
    printf('<br/>');
    printf('<br/>');

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Select', 30) ;
    listHead ('Article', 45)  ;
    listHead ('Description', 120) ;
    listHead ('Color', 45)  ;
    listHead ('Description', 120) ;
    listHead ('On Webshop', 95) ;
    listHead ('Selected shops', 95) ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldRaw ('<input type=hidden name="cancelled" class="cancelled" value="'.(int)$row["notonwebshop"].'">'.formCheckbox (sprintf('products[%d]', (int)$row['Id']), 0, '')) ;
    	listField ($row["ArticleNumber"]) ;
    	listField ($row["Description"]) ;
    	listField ($row["ColorNumber"]) ;
    	listField ($row["ColorName"]) ;
    	listField (($row["notonwebshop"]==1)?'No':'Yes') ;
    	listField (implode(' / ', getActiveSites($row['Id']))) ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Articles', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
