<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    global $User, $Navigation, $Record, $Result2, $Size, $Id ;

    formStart();
    itemStart () ;

    $query = sprintf ("SELECT ArticleSize.*, CollectionmemberAssortment.AssortmentId, CollectionmemberAssortment.Quantity FROM ArticleSize
                        INNER JOIN CollectionmemberAssortment ON CollectionmemberAssortment.ArticleSizeId=ArticleSize.Id AND CollectionmemberAssortment.CollectionMemberId=%d AND CollectionmemberAssortment.Active=1
                        WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
                        ORDER BY CollectionmemberAssortment.AssortmentId, ArticleSize.DisplayOrder, ArticleSize.Name", $Id, (int)$Record['ArticleId']) ;
    $result = dbQuery ($query) ;

    $Size = array() ;
    while ($row = dbFetch ($result)) {
        $Size[(int)$row['AssortmentId']][$row['DisplayOrder']] = $row ;
    }
    dbQueryFree ($result) ;

    while ($row = dbFetch($Result2)) {
        itemField ('Size', 'Weight') ;
        itemFieldRaw ($row['VariantSize'], formText ('Weight['.$row["Id"].']', number_format ((float)$row['Weight'], (int)4, ',', ''), 40)) ;
        itemSpace () ;
    }
    
    itemSpace () ;
    itemSpace () ;
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
