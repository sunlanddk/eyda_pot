<?php 

	require_once 'lib/table.inc' ;
	require_once 'lib/navigation.inc' ;
	
	$vc = array(
		'Presale' => 0
	);
	tableWrite('variantcode', $vc, $Id);

	if($Navigation['Parameters'] == 'cmview') {
		return navigationCommandMark('collmemberview', (int)$_GET['id']);
	}

	return navigationCommandMark('presaleview', (int)$_GET['id']);
?>