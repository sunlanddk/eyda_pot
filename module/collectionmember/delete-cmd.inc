<?php

    require_once 'lib/table.inc' ;

	// Insert code below for inserting records in cronjobs for collectionmember defined by $Id.

	$post = array(
		'type'				=> 'shopify',
		'internalid' 		=> (int)$Id,
		'cron_name' 		=> 'Delete product',
		'cron_function' 	=> 'deleteProduct',
		'cron_value' 		=> (int)$Id,
		'cron_scheduled_at'	=> dbDateEncode(time()),
	);
	tableWriteCron('cron_jobs', $post);

	$post = array(
		'notonwebshop' => 1
	);
	tableWrite('collectionmember', $post, $Id);

	return navigationCommandMark ('collmemberview', (int)$Id) ;

    return 0 ;
?>
