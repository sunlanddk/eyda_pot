<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

	
	switch ($Navigation['Parameters']) {
		case 'default' :
			break ;
		default :
			itemStart () ;
			itemSpace () ;
			itemFieldIcon ('Season', $Record['Name'], 'season.png', 'seasonview', (int)$Record['Id']) ; 
			itemField ('Collection', $Record['CollectionName']) ; 
			itemField ('Brand', $Record['BrandName']) ; 
			itemFieldIcon ('Company', tableGetField('Company','Name',(int)$Record['CompanyId']), 'company.gif', 'companyview', (int)$Record['CompanyId']) ; 
			itemSpace () ;
			itemEnd () ;
	}

    function getActiveSites($colMemberId){
        $query = sprintf (
            "SELECT 
                ss.Name
            FROM (shopifyshops ss, inshopifyshops iss)
            WHERE ss.Active=1 AND ss.Id=iss.ShopifyShopId AND iss.Active=1 AND iss.CollectionMemberId=%d ORDER BY ss.Id;
            ", 
            (int)$colMemberId
        ) ;
        $results = dbQuery ($query) ;
        $shops = [];
        while ($row = dbFetch ($results)) {
            array_push($shops, $row['Name']);
        }
        return $shops;
    }


    // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
    listHead ('Status', 20)  ;
    listHeadIcon (40) ;
    listHeadIcon (10) ;
    listHead ('Article', 70)  ;
    listHeadIcon (10) ;
    listHead ('Description', 80) ;
    //listHead ('DK', 60) ;
    //listHead ('DE', 60) ;
    //listHead ('UK', 60) ;
//    listHead ('Fabric', 100) ;
    listHead ('ColorNumber', 50) ;
//    listHeadIcon (10) ;
    listHead ('ColorName', 70) ;
//    listHead ('DK', 45) ;
//    listHead ('DE', 45) ;
//    listHead ('UK', 45) ;
    listHead ('#sizes', 25) ;
    listHead ('Prices (Max)', 70) ;
//		listHeadIcon () ;
//		listHead ('FitsWith', 60) ;
//		listHead ('Campaign', 60) ;
//    listHead ('Program', 40) ;
//    listHead ('Gender', 25) ;
//    listHead ('LOT', 25) ;
//    listHead ('Delivery', 80) ;
//    listHead ('Case', 70)  ;
    if ($ShowSeason)
    	listHead ('Collection', 150) ;
    if ($ShowCollection)
    	listHead ('Collection', 150) ;
    //listHead ('Focus', 30)  ;
    listHead ('Canceled', 70);
    listHead ('On Web', 70)  ;
    listHead ('Selected shops', 70)  ;
    listHead ('On presale', 50) ; 

    while ($row = dbFetch($Result)) {
    	listRow () ;
        $colorLight = CheckForShopifyId((int)$row['articleId'], (int)$row['ArticleColorId'], (int)$row['Id']);
        listFieldRaw ('<div class="lights '.$colorLight.'"><div class="red"></div><div class="yellow"></div><div class="green"></div></div>') ;
//		listFieldIcon ('component.gif', 'collmemberview', (int)$row['Id']) ;
		listStyleIcon ('collmemberview', $row) ;
		listFieldIcon ('article.gif', 'colllistartview', (int)$row['articleId']) ;
    	listField ($row["Article"]) ;
 		listFieldIcon ('pen.gif', 'langedit', (int)$row['articleId']) ;
		listField ($row["Description"], '', 56) ;
		//$description = tableGetFieldWhere('article_lang','concat(Description, ": ",WebDescription)','LanguageId=4 and ArticleId='.(int)$row['articleId']) ;// explode ('#', $row["AltDesc"]) ; // 'concat(Description, ": ",WebDescription)'
   		//listField ($description, '', 56) ;
		//$description = tableGetFieldWhere('article_lang','WebDescription','LanguageId=5 and ArticleId='.(int)$row['articleId']) ;
   		//listField ($description, '', 56) ;
		//$description = tableGetFieldWhere('article_lang','WebDescription','LanguageId=6 and ArticleId='.(int)$row['articleId']) ;
   		//listField ($description, '', 56) ;
//    	listField ($row["Fabric"]) ;
    	listField ($row["ColorNumber"]) ;
// 		listFieldIcon ('pen.gif', 'coledit', (int)$row['ColorId']) ;
    	listField ($row["Color"], '', 56) ;

        // Sizes
        dbQueryFree ($res) ;
        $query = sprintf ("SELECT count(*) as No FROM articlesize WHERE Active=1 AND articleId=%d", (int)$row['articleId']) ;
        $res = dbQuery ($query) ;
        $row1 = dbFetch ($res) ;
        listField ($row1["No"] ) ;
        dbQueryFree ($res) ;

        // Prices 

        $query = sprintf ("select group_concat(concat(tab.Name,' ',cast(tab.RetailPrice as char(7)),if(tab.CampaignPrice>0,concat('/',cast(tab.CampaignPrice as char(7))),'')) separator ', ') as Prices
                            from (SELECT c.Name, max(cmps.RetailPrice) as RetailPrice, max(cmps.CampaignPrice) as CampaignPrice
                                FROM (CollectionMemberPrice cmp, Currency c, CollectionMemberPriceSize cmps) 
                                WHERE cmp.active=1 AND cmp.CollectionMemberId=%d AND cmp.CurrencyId>0 AND c.Id=cmp.CurrencyId and cmps.CollectionMemberPriceId=cmp.id and cmps.active=1
                                group by cmp.id) tab", (int)$row['Id']) ;
        $res = dbQuery ($query) ;
        $row1 = dbFetch ($res) ;
        listField ($row1["Prices"], '', 56) ;
/*
		$description = tableGetFieldWhere('color_lang','Description','LanguageId=4 and ColorId='.(int)$row['ColorId']) ;
   		listField ($description, '', 56) ;
		$description = tableGetFieldWhere('color_lang','Description','LanguageId=5 and ColorId='.(int)$row['ColorId']) ;
   		listField ($description, '', 56) ;
		$description = tableGetFieldWhere('color_lang','Description','LanguageId=6 and ColorId='.(int)$row['ColorId']) ;
   		listField ($description, '', 56) ;
*/
//		listFieldIcon ('edit.gif', 'webgroupedit', (int)$row['Id']) ;
//		listField ($row["WebFitGroup"]) ;
//    	listField ($row["WebCampaign"]) ;
//  		listField ($row["ProgramName"]) ;
//    	listField ($row["SexName"]) ;
//    	listField ($row["LOT"]) ;

//    	listField ($row["Planned"]) ;
//    	listField ($row["Case"]) ;
        if ($ShowSeason)
    		listField ($row["SeasonName"]) ;
        if ($ShowCollection)
    		listField ($row["Collection"]) ;
		//if ($row["Focus"])
		//	listField ('Focus') ;
		//else
		//	listField ('') ;
        if ($row["Cancel"])
            listField ($row["CancelDescription"]?$row["CancelDescription"]:'Canceled', '', 56) ;
        else
            listField ('') ;

    	if ($row["notonwebshop"]) {
    			listField ('No', '', 56) ;
                listField (implode(' / ', getActiveSites($row['Id']))) ;
    		} else {
    			listField ('Yes - ' . $_row['Name']) ;
                listField (implode(' / ', getActiveSites($row['Id']))) ;
            }

            listField( checkPresale($row['Id']) );
        }

        

        listEnd () ;
        dbQueryFree ($Result) ;

    function checkPresale($cmId) { //janis
        $sql = sprintf("SELECT COUNT(vc.Id) AS Test FROM variantcode vc LEFT JOIN collectionmember cm ON cm.Articleid = vc.ArticleId AND cm.ArticleColorId = vc.ArticleColorId AND cm.Active = 1 WHERE cm.Id = %d AND vc.Presale = 1 AND vc.Active = 1", (int)$cmId);
        $result = dbQuery($sql);
        $row = dbFetch($result);
        dbQueryFree($result);
        $out = 'No';
        if ((int)$row['Test'] > 0) {
            $out = 'Yes';
        }
        return $out;
    }
    
    function CheckForTransferInProgress($colMemberId){
        $query = sprintf ("
            SELECT 
            COUNT(cj.Id) as count 
            FROM cron_jobs cj
            WHERE 
            cj.done=0 AND 
            cj.type='shopify' AND 
            cj.internalid=%d
            ",
            $colMemberId
        ) ;
        $res = dbQuery ($query) ;
        $return = dbFetch ($res) ;
        dbQueryFree ($res) ;

        if((int)$return['count'] > 0){
            return 'yellow';
        }

        return true;
    }

    function CheckForCron($colMemberId, $count){
        $query = sprintf ("
            SELECT 
            cj.id
            FROM cron_jobs cj
            WHERE 
            cj.done=0 AND 
            cj.type='shopify' AND 
            cj.internalid=%d
            ORDER BY Id DESC
            LIMIT 1
            ",
            $colMemberId
        ) ;
        $res = dbQuery ($query) ;
        $return = dbFetch ($res) ;
        dbQueryFree ($res) ;
        if((int)$return['id'] > 0){
            return 'yellow';
        }

        $query = sprintf ("
            SELECT 
            cj.id,
            cj.error
            FROM cron_jobs cj
            WHERE 
            cj.done=1 AND
            cj.type='shopify' AND 
            cj.internalid=%d
            ORDER BY Id DESC
            LIMIT 1
            ",
            $colMemberId
        ) ;
        $res = dbQuery ($query) ;
        $returnsecond = dbFetch ($res) ;
        dbQueryFree ($res) ;
        if((int)$returnsecond['id'] > 0){
            if((string)$returnsecond['error'] !== ''){
                return 'red';
            }
        }

        if($count == 0){
            return 'red';
        }

        return 'green';
    }

    function CheckForShopifyId($articleId, $articleColorId, $colMemberId){
        $query = sprintf ("
            SELECT 
            COUNT(v.Id) as count 
            FROM (variantcode v, shopifyvariant sv)
            WHERE 
            v.Active=1 AND 
            v.ArticleId=%d AND 
            v.ArticleColorId=%d AND
            sv.VariantCodeId=v.Id AND sv.ShopifyProductId != ''
            GROUP BY v.ArticleId, v.ArticleColorId, sv.ShopifyshopId LIMIT 1
            ", 
            $articleId, 
            $articleColorId
        ) ;
        $res = dbQuery ($query) ;
        $return = dbFetch ($res) ;
        dbQueryFree ($res) ;

        /*if((int)$return['count'] == 4){
           // return CheckForTransferInProgress($colMemberId);
        }*/
        if((int)$return['count'] > 0){
            if(CheckForTransferInProgress($colMemberId) !== true){
                return CheckForTransferInProgress($colMemberId);
            }
        }

        return CheckForCron($colMemberId, (int)$return['count']);
    }
    return 0 ;
?>
