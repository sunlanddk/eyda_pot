<?php 

require_once "lib/config.inc";
require_once "lib/db.inc";

require_once 'lib/item.inc' ; //?
require_once 'lib/table.inc' ; //?
// echo $_GET['id'];
// echo "<br>";
// echo $_GET['variant'];
// echo "<br>";

//$output = getTestQuery($_GET['variant']);
//echo json_encode($output);

listStart () ;
listRow () ;
listHead ('Transaction ID',5) ;
listHead ('Type',3) ;
listHead ('Date',10) ;
listHead ('Quantity',3) ;
listHead ('Value',3) ;
listHead ('From',10) ;
listHead ('To',10) ;
listHead ('Comment',10) ;


getTransactions($Id);
listEnd() ;


function getTransactions($variant) {
	$query = sprintf(
		"SELECT 
		t.`Type` AS 'Type', 
		t.`Comment` AS 'Comment',
		ti.`TransactionId`, 
		t.`CreateDate`, 
		ti.`Quantity`, 
		t.`FromStockId` AS 'From', 
		t.`ToStockId` AS 'To', 
		ti.`Value` AS 'Value',
		u.`FirstName`,
		u.`LastName` 
		FROM 
		`variantcode` AS vc, 
		`transactionitem` AS ti, 
		`transaction` AS t
		LEFT JOIN `user` u ON t.`CreateUserId` = u.`Id`
		WHERE 
		vc.`ArticleSizeId` = %d 
		AND vc.`Active` = 1 
		AND ti.`Active` = 1 
		AND t.`Active` = 1 
		AND ti.`VariantCodeId` = vc.`Id` 
		AND t.`Id` = ti.`TransactionId` 
		ORDER BY 
		ti.`CreateDate` 
		DESC 
		", 
		(int)$variant
	);

	$query = sprintf(
		"SELECT 
        t.`Type` AS 'Type', 
		t.`Comment` AS 'Comment',
		ti.`TransactionId`, 
		t.`CreateDate`, 
		ti.`Quantity`, 
		t.`FromStockId` AS 'From', 
		t.`ToStockId` AS 'To', 
		ti.`Value` AS 'Value',
		u.`FirstName`,
		u.`LastName`,
        IF(t.Type='Ship', ol.Id, 0) AS OrderLineId,
        IF(t.Type='Ship', ol.OrderId, 0) As OrderId,
        IF(t.Type='Receive', rl.Id, 0) AS RequisitionLineId,
        IF(t.Type='Receive', rl.RequisitionId, 0) AS RequisitionId
        FROM variantcode vc
        LEFT JOIN `transactionitem` ti ON ti.ArticleSizeId=vc.ArticleSizeId AND ti.Active=1
        LEFT JOIN `transaction` t ON t.`Id`=`TransactionId` AND t.Active=1
        LEFT JOIN `user` u ON t.`CreateUserId` = u.`Id`
        LEFT JOIN `orderline` ol ON ol.Active=1 AND ol.Id=ti.ObjectLineId AND ol.ArticleColorId=vc.ArticleColorId
        LEFT JOIN `requisitionline` rl ON rl.Active=1 AND rl.Id=ti.ObjectLineId AND rl.ArticleColorId=vc.ArticleColorId
        WHERE 
        vc.Id=%d
        AND IF(
			t.Type='Receive', 
            rl.Id IS NOT NULL, 
            IF(
				t.Type='Ship', 
                ol.Id IS NOT NULL, 
                IF(
					(t.Type = 'refund' OR t.Type='InvLoss' OR t.Type='InvLoss' ),
                    ti.VariantCodeId=vc.Id,
                    1=1
                )
			)
		)
        AND t.Id IS NOT NULL
        ORDER BY ti.CreateDate DESC
		",
		$variant
	);

	$result = dbQuery ($query);


	while ($row = dbFetch ($result)) {

		

        listRow ();
        listField($row['TransactionId']);
        listField($row['Type']);
        listField($row['CreateDate']);
        listField($row['Quantity']);
        listField($row['Value']);
        listField(getStockLabel($row['From']));
        listField(getStockLabel($row['To']));
        listField($row['Comment']);
    }
    dbQueryFree ($result);
}

function getStockLabel($stock_id){
	$query = sprintf("
		SELECT 
		
		IF (Type = 'Shipment', Name, Name) AS Name

		FROM 
		`stock` 
		WHERE 
		`Id` = %d", 
		(int)$stock_id
	);
	
	$result = dbQuery ($query);
	$row = dbFetch ($result);
	dbQueryFree ($result);
	return $row['Name'];
}


function getTestQuery($variant) {
	$query = sprintf("
		SELECT 
		ti.`TransactionId` 
		FROM 
		`variantcode` AS vc, 
		`transactionitem` AS ti 
		WHERE 
		vc.`VariantCode` = %d 
		AND vc.`Active` = 1 
		AND ti.`Active` = 1 
		AND ti.`VariantCodeId` = vc.`Id`", 
		(int)$variant
	);
	$output = [];

	$result = dbQuery ($query);

	while ($row = dbFetch ($result)) {
        $output[] = $row;
    }
    dbQueryFree ($result);
    return $output;
}




return 0;






?>