<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    global $User, $Navigation, $Record, $Size, $Id ;

    itemStart () ;
    itemSpace () ;
    itemField ('Article', $Record['ArticleDescription'].' ('.$Record['ArticleNumber'].')') ;
    itemField ('Color', $Record['ColorDescription'].' ('.$Record['ColorNumber'].')') ;
    itemSpace () ;
    itemEnd () ;

    // Form
//    formStart () ;
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;
    
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Season', formText ('SeasonName', $Record['SeasonName'], 100, 'width:100%;')) ;
    itemFieldRaw ('Core', formCheckbox ('Core', $Record['Core'])) ;
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Discontinued', formCheckbox ('Discontinued', $Record['Discontinued'])) ;
    itemSpace () ;
    itemFieldRaw ('Cancel', formCheckbox ('Cancel', $Record['Cancel'])) ;
    itemFieldRaw ('Cancel Reason', formText ('CancelDescription', $Record['CancelDescription'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('LOT', formDBSelect ('CollectionLOTId', $Record['CollectionLOTId'], 
                                            sprintf("SELECT Id, Concat(Name,' - ', Date) AS Value FROM Collectionlot WHERE SeasonId=%d AND Active=1 ORDER BY Date", $Record['SeasonId']), 'width:250px;')) ;  
    itemSpace () ;
//    itemFieldRaw ('Program', formDBSelect ('ProgramId', $Record['ProgramId'], 
//                                          sprintf("SELECT Id, Concat(Name,' - ', Date) AS Value FROM Collectionlot WHERE SeasonId=%d AND Active=1 ORDER BY Date", $Record['SeasonId']), 'width:250px;')) ;  
//  itemFieldRaw ('Program', formDBSelect ('ProgramId', $Record['ProgramId'], sprintf('Select 0 as Id, "-Select-" as Value Union SELECT Program.Id, Program.Name AS Value FROM Program WHERE SeasonId=%d AND Active=1 ORDER BY Value', $Record['SeasonId']), 'width:200px')) ; 
    itemSpace () ;
    itemFieldRaw ('Show On B2B', formCheckbox ('ShowOnB2B', $Record['ShowOnB2B'])) ;
    itemFieldRaw ('B2B Focus style', formCheckbox ('Focus', $Record['Focus'])) ;
    itemSpace () ;
    itemFieldRaw ('On Webshop', formCheckbox ('onwebshop', !$Record['notonwebshop'])) ;
    itemFieldRaw ('Frontpage webshop', formCheckbox ('WebFrontpage', $Record['WebFrontpage'])) ;
    itemSpace () ;

	itemFieldRaw ('Fit with group', formText ('WebFitGroup', $Record['WebFitGroup'], 100, 'width:250px;')) ;
//	itemFieldRaw ('Campaign group', formText ('WebCampaign', $Record['WebCampaign'], 100, 'width:100%;')) ;

    itemSpace () ;
    itemSpace () ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=10000000>\n") ;
	$_file =	$Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '.jpg' ;
	printf ("<tr><td><p>Main Photo</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;    
	itemFieldRaw ('IconAdjX', formText ('IconAdjX', $Record['IconAdjX'], 5, 'width:75px;') . 'Value=0 is horisontal centerpoint - 75 pixels (600x600)') ;
	itemFieldRaw ('IconAdjY', formText ('IconAdjY', $Record['IconAdjY'], 5, 'width:75px;') . 'Value=0 is vertical centerpoint - 30 pixels') ;
	if (file_exists($Config['photoLib'].$_file)) {
		itemFieldRaw ('Delete', formCheckbox ('DeleteMain', 0))  ;
	}

    itemSpace () ;
	$_file = $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_d1' . '.jpg' ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=10000000>\n") ;
	printf ("<tr><td><p>Detail Photo 1</p></td><td><input type=file name=Doc1 size=60></td></tr>\n") ;    
	if (file_exists($Config['photoLib'].$_file)) {
		itemFieldRaw ('Delete', formCheckbox ('DeleteD1', 0))  ;
	}

    itemSpace () ;
	$_file = $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_d2' . '.jpg' ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=10000000>\n") ;
	printf ("<tr><td><p>Detail Photo 2</p></td><td><input type=file name=Doc2 size=60></td></tr>\n") ;    
	if (file_exists($Config['photoLib'].$_file)) {
		itemFieldRaw ('Delete', formCheckbox ('DeleteD2', 0))  ;
	}

    itemSpace () ;
	$_file = $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_d3' . '.jpg' ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=10000000>\n") ;
	printf ("<tr><td><p>Detail Photo 2</p></td><td><input type=file name=Doc3 size=60></td></tr>\n") ;    
	if (file_exists($Config['photoLib'].$_file)) {
		itemFieldRaw ('Delete', formCheckbox ('DeleteD3', 0))  ;
	}

    itemSpace () ;
	$_file = $Record['ArticleNumber'] . '_' . $Record['ColorNumber'] . '_d4' . '.jpg' ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=10000000>\n") ;
	printf ("<tr><td><p>Detail Photo 4</p></td><td><input type=file name=Doc4 size=60></td></tr>\n") ;    
	if (file_exists($Config['photoLib'].$_file)) {
		itemFieldRaw ('Delete', formCheckbox ('DeleteD4', 0))  ;
	}
    itemSpace () ;
    itemSpace () ;
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
