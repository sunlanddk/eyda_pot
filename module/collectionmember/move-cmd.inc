<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (    
  	'SeasonId'		=> array ('check' => true)
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'SeasonId' :
			if (!$changed) return false ;
			break ;
	}

	if (!$changed) return false ;
	return true ;	
    }
    
	$old_seasonid = $Record['SeasonId'] ;
	$new_seasonid = $_POST['SeasonId'] ;
	
//return 'test: ' . $fields['SeasonId']['value']	. ' r ' . $Record['SeasonId'] . ' p ' . $_POST['SeasonId'] ;
     
    $res = saveFields ('Collection', $Id, $fields, true) ;
    if ($res) return $res ;

	// Also update orders and collection LOTs
    $query = sprintf ("UPDATE Collectionlot SET `SeasonId`=%d, `ModifyDate`='%s', `ModifyUserId`=%d WHERE SeasonId=%d", $new_seasonid, dbDateEncode(time()), $User["Id"], $old_seasonid) ;
    dbQuery ($query) ;


    return 0 ;    
?>
