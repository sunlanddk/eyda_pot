<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('Season', $Record['SeasonName']) ;
	    itemField ('Collection', $Record['Name']) ;
	    itemSpace () ;
	    itemField ('WARNING', 'all LOTs in season will be moved') ;
		$query='select * from `order` where seasonid=' . $Record['SeasonId'] ;
		$res = dbQuery ($query) ;
		if (dbNumRows($res) > 0) {
	    itemField ('WARNING', 'You have orders connected to this Season. They will not be moved') ;
		}
		dbQueryFree ($res) ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;
    itemSpace () ;
 	itemFieldRaw ('New Season', formDBSelect ('SeasonId', $Record['SeasonId'], 'SELECT Season.Id, Season.Name AS Value FROM Season WHERE Done=0 And OwnerId=' . $Record['OwnerId'] . ' And Active=1 ORDER BY Value', 'width:200px')) ; 
// 	itemFieldRaw ('LOT', formDBSelect ('SeasonId', $Record['BrandId'], 'SELECT Season.Id, Season.Name AS Value FROM Season WHERE Active=1 ORDER BY Value', 'width:200px')) ; 
    itemSpace () ;

    itemInfo ($Record) ;
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
