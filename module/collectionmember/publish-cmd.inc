<?php

    require_once 'lib/table.inc' ;

	// Insert code below for inserting records in cronjobs for collectionmember defined by $Id.

	$post = array(
		'type'				=> 'shopify',
		'internalid' 		=> (int)$Id,
		'cron_name' 		=> 'Update product',
		'cron_function' 	=> 'publishProduct',
		'cron_value' 		=> (int)$Id,
		'cron_scheduled_at'	=> dbDateEncode(time()),
	);
	tableWriteCron('cron_jobs', $post);

	$post = array(
		'Cancel' => 0,
		'notonwebshop' => 0
	);
	tableWrite('collectionmember', $post, $Id);

	return navigationCommandMark ('collmemberview', (int)$Id) ;

    return 0 ;
?>
