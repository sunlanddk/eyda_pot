<?php


    require_once 'lib/navigation.inc' ;
    require_once 'lib/fieldtype.inc' ;
    require_once 'lib/lookups.inc';

//	Global $User, $Record, $Navigation, $Line;

function printorderloadco ($_Id, $_record) {
	Global $Id, $User, $Record, $Navigation;
	$Id = $_Id;
	$Record = $_record;

	if ($User['Restricted']) {
		$LimitClause = sprintf(' AND User.Loginname IN ( %s ) ', $User['UserList']);
	} else {
		$LimitClause = '';
	}

    $StateField    = 'IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0, "Draft",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=1, "Proposal",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=1, "Confirmed",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%finished%%"), "Shipped",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%cancelled%%"),"Cancelled", "Undefined" )))))';

    $queryFields = '`Order`.*,
		    Currency.Symbol AS CurrencySymbol,
		    Company.Number AS CompanyNumber, Company.Name AS CompanyName, Company.Mail AS Mail,
		    Company.Surplus AS CompanySurplus,
		    CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS SalesUserName,
		    OrderDone.Name AS OrderDoneName,
		    Season.Name as SeasonName,
		    ' . $StateField . ' AS State' ;

    $queryTables = '`Order`
		    LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
		    LEFT JOIN Company ON Company.Id=Order.CompanyId
		    LEFT JOIN User ON User.Id=Order.SalesUserId
		    LEFT JOIN Season ON Season.Id=Order.SeasonId
		    LEFT JOIN OrderDone ON OrderDone.Id=Order.OrderDoneId' ;

	$companylimit = sprintf('`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) AND `Order`.Draft = 0 ', $User['Id']);
	$companylimit = sprintf('(`Order`.ToCompanyId in (select CompanyId From UserCompanies where UserId=%d) or `Order`.ToCompanyId = %d) AND `Order`.Draft = 0 ', $User['Id'], $User['CompanyId']);

	if ($User['Extern']) 
		$companylimit = sprintf('(`Order`.CompanyId = %d) AND `Order`.Draft = 0 ', $User['CompanyId']);

	global $Line;
	$_ready = (int) $Record['Ready'] ;
	// Query Order
	$query = sprintf ('SELECT %s FROM %s WHERE Order.Id=%d AND Order.Active=1 AND %s', $queryFields, $queryTables, $Id, $companylimit) ;
//return $query ;
	$query .= $LimitClause;
	$res = dbQuery ($query) ;
	$Record = dbFetch ($res) ;
	dbQueryFree ($res) ;
	 $Record['Ready'] = $_ready>0 ? $_ready : $Record['Ready'] ;

	// Variables
	$SizeCount = 0 ;

	// Get OrderLines
	$Line = array () ;
	$query = sprintf ('SELECT OrderLine.Id, min(OrderLine.No) as No, 
							 group_concat(OrderLine.No) as NoGroup,
							 count(OrderLine.Id) as NoGroupLines,
							`orderline`.`Description`,
							`orderline`.`InvoiceFooter`,
							`orderline`.`OrderId`,
							`orderline`.`CaseId`,
							`orderline`.`ArticleId`,
							`orderline`.`ArticleColorId`,
							 sum(`orderline`.`Quantity`) as Quantity,
							`orderline`.`Surplus`,
							 sum(`orderline`.`PriceSale`*`orderline`.`Quantity`) as PriceSaleSubTotal,
							`orderline`.`PriceCost`,
							`orderline`.`Discount`,
							`orderline`.`DeliveryDate`,
							`orderline`.`ArticleCertificateId`,
							`orderline`.`PrefDeliveryDate`,
							`orderline`.`RequestComment`,
							`orderline`.`CustArtRef`,
							`orderline`.`ProductionId`,
							`orderline`.`Done`,
							`orderline`.`DoneUserId`,
							`orderline`.`DoneDate`,
							`orderline`.`ConfDeliveryDate`,
							`orderline`.`LineFooter`,
							`orderline`.`AltColorName`,
								Case.CustomerReference, Case.ArticleCertificateId, ArticleColor.Id as ArticleColorId,
								Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantDimension, VariantSortation,
								Color.Description AS ColorDescription, Color.Number AS ColorNumber,
								ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
								Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals, (SELECT Number FROM Production WHERE Id = OrderLine.ProductionId) AS ProductionNo,
								CustomsPosition.Id AS CustomsPositionId, CustomsPosition.Description AS CustomsPositionDesc, CustomsPosition.Name as CustomsPos, Country.Name as WorkCountryName, Country.Name as WorkCountryId
		FROM OrderLine
		LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
		LEFT JOIN CustomsPosition ON Article.CustomsPositionId=CustomsPosition.Id and CustomsPosition.active=1
		LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
		LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
		LEFT JOIN Unit ON Unit.Id=Article.UnitId
		LEFT JOIN Country ON Article.WorkCountryId=Country.Id
		WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 
		GROUP BY OrderLine.ArticleId, OrderLine.ArticleColorId
		ORDER BY OrderLine.DeliveryDate, No', $Id) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
		$Line[(int)$row['Id']] = $row ;
	}
	dbQueryFree ($result) ;

	// Get size specific quantities for each Line with VariantSize/Dimension
	foreach ($Line as $i => $l) {
//			if (!$l['VariantSize'] or !$l['VariantDimension']) continue ;

		$Line[$i]['Size'] = array () ;
		if ($l['VariantSize']) {
			$query = sprintf ("SELECT ArticleSize.*, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity, vc.VariantCode as VariantCode, OrderLine.PriceSale, OrderLine.No as OrderLineNo
			FROM ArticleSize
			INNER JOIN OrderQuantity ON OrderQuantity.Active=1 AND OrderQuantity.ArticleSizeId=ArticleSize.Id
			INNER JOIN OrderLine On orderline.orderid=%d and OrderLine.Id=OrderQuantity.OrderLineId and OrderLine.articlecolorid=%d and orderline.active=1
			LEFT JOIN VariantCode vc ON vc.articleid=%d AND vc.articlecolorid=%d AND vc.articlesizeid=ArticleSize.Id AND vc.Active=1
			WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name
			", 
			$Id, (int)$l['ArticleColorId'],(int)$l['ArticleId'],(int)$l['ArticleColorId'], (int)$l['ArticleId']) ;
		} else if ($l['VariantDimension']) {
			$query = sprintf ("SELECT OrderQuantity.Dimension as Name, OrderQuantity.Dimension as Id, OrderQuantity.Id AS OrderQuantityId, OrderQuantity.Quantity
								FROM OrderQuantity
								WHERE OrderQuantity.OrderLineId=%d AND OrderQuantity.Active=1", $i) ;
		} else continue ;

		$result = dbQuery ($query) ;
		$n = 0 ;
		while ($row = dbFetch ($result)) {
			$Line[$i]['Size'][$row['Id']] = $row ;
			$n++ ;
		}
		dbQueryFree ($result) ;

		// Update max size count
		if ($n > $SizeCount) $SizeCount = $n ;
	}
    return 0 ;
}
?>
