<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/save.inc' ;

    function flag (&$Record, $field) {
	$s = ($Record[$field]) ? sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) : 'No' ;
	if ($field == 'Done' and $Record['Done'] and (int)$Record['OrderDoneId'] > 0) {
	    $s .= ', ' . htmlentities($Record['OrderDoneName']) ;
	}
	itemField ($field, $s) ;
    }
    printf ("<table class=item><tr><td>\n") ;
    itemStart () ;
	if ((int)$Record['Proposal']>0) {
		itemField ('Proposal', '(change to order by resetting proposal flag usig EDIT)') ;
		itemSpace () ;
	}
    itemHeader () ;
    itemField ('ConsolidatedId', $Record['Orders']) ;
    //itemField ('Consolidated', (int)$Record['ConsolidatedId']) ;
    if ($Record['PickOrderId']>0) {
	    if (!$User['Extern'] AND !$User['SalesRef'])
			itemFieldIcon ('PickOrder', $Record['PickOrderState'], 'cart.gif', 'picklist', $Record['PickOrderId'], '', 'component.gif', 'packlist') ;
	} else {
		if (!$User['Extern'] AND !$User['SalesRef'])
			$PickOrderId = tableGetFieldWhere('pickorder', 'Id', sprintf("ConsolidatedId=%d and Type='SalesOrder' and Active=1 and packed=1", (int)$Record['Id'])) ;
//return 'pickid ' . $PickOrderId . 'query ' . sprintf("referenceid=%d and Type='SalesOrder' and Active=1 and packed=1", (int)$Record['Id']) ;
		if ($PickOrderId>0) {
			itemFieldIcon ('PickOrder', 'Done', 'cart.gif', 'picklist', $PickOrderId, '', 'component.gif', 'packlist') ;
		}
    }
    if (!$User['Extern'])
		itemField ('Description', $Record['Description']) ;
    if ($Record['SeasonId']>0) {
		if ($User['Extern'] OR $User['SalesRef']) 
			itemField ('Season', tableGetField('season','Name',$Record['SeasonId'])) ;
		else
			itemFieldIcon ('Season', tableGetField('season','Name',$Record['SeasonId']), 'component.gif', 'seasonview', $Record['SeasonId']) ;
    }
	itemSpace () ;
    itemField ('State', $Record['State']) ;
	if (!$User['Extern'] AND !$User['SalesRef'])
		itemFieldIcon ('Owner', tableGetField ('Company', 'Company.Name', (int)$Record['ToCompanyId']), 'company.gif', 'companyview', $Record['ToCompanyId']) ;
	if ($Record['PurchaseOrderId']>0)
		itemFieldIcon ('From PO',$Record['PurchaseOrderId'] , 'requisition.gif', 'requisitionview', $Record['PurchaseOrderId']) ;
//	if ($Record['ToPurchaseOrderId']>0)
//		itemFieldIcon ('To PO',$Record['ToPurchaseOrderId'] , 'requisition.gif', 'requisitionview', $Record['ToPurchaseOrderId']) ;
    itemSpace () ;
    if($Record['OrderTypeId'] == 10){
        if (!$User['Extern']) 
    		flag ($Record, 'Ready') ;
        if (!$User['Extern']) 
    		flag ($Record, 'Done') ; 
    }
    else{
        // $query = 'SELECT * FROM `order` WHERE Active=1 AND ConsolidatedId='.$Id;
        // $allConsolidatedOrders = dbQuery($query);
        // while ($row = dbFetch($allConsolidatedOrders)) {
        //     if (!$User['Extern']){
        //         itemFieldIcon ('Order', $row['Id'], $Navigation['Icon'], 'orderviewcon', (int)$row['Id']) ;
        //         flag ($row, 'Ready') ;
        //         flag ($row, 'Done') ;        
        //         itemInfo ($row) ;
        //         itemSpace () ;
        //         itemSpace () ;
        //     }
        // }
    }
 	// itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;
    printf ("</td>\n") ;

    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
    
    listStart () ;
    listHeader ('Consolidated Orders included') ;
    $cosolidatedorderlines = $cosolidatedorders;
	listHeadIcon() ;
	listHead('OrderNumber') ;
	listHead('Season') ;
	listHead('State') ;
	listHead('Confimation pdf') ;
	listHead('Edit order') ;
    while ($row = dbFetch($cosolidatedorders)) {
        listRow () ;
        listFieldIcon ($Navigation['Icon'], 'orderviewcon', (int)$row['Id']);
        listfield ($row['Id']);
        listfield ($row['SeasonName']);
        listfield ($row['Ready']?'Confirmed':'Draft');
        listFieldIcon ('print.gif', 'printordercon', (int)$row['Id']);
        listFieldIcon ('edit.gif', 'soedit', (int)$row['Id']);
    }
    itemEnd () ;
    printf ("</td>\n") ;

    printf ("<tr><td>\n") ;

    itemStart () ;
    itemHeader ('Sales') ;
	if ($User['Extern']) 
		itemField('Customer', $Record['CompanyNumber']) ;
	else
		itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
    itemField ('Name', empty($Record['AltCompanyName'])?$Record['CompanyName']:$Record['AltCompanyName'].' ('.$Record['CompanyName'].')') ;    
    itemSpace () ;
    itemField ('Reference', $Record['Reference']) ;
    itemSpace () ;
    itemField ('Sales Ref', tableGetField ('User', 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', (int)$Record['SalesUserId'])) ;  
    itemSpace () ;
    itemEnd () ;

    printf ("</td>") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
    itemHeader ('Confirmation/Invoice') ;
    itemField ('Currency', tableGetField ('Currency', 'CONCAT(Description," (",Name,")")', (int)$Record['CurrencyId'])) ;  
    itemSpace () ;
    itemField ('Payment Term', tableGetField ('PaymentTerm', 'CONCAT(Description," (",Name,")")', (int)$Record['PaymentTermId'])) ;  
    itemSpace () ;
	if (!$User['Extern'] AND !$User['SalesRef'])
		itemField ('VariantCodes', $Record['VariantCodes']?'Yes':'No') ;
    itemFieldText ('Header', $Record['InvoiceHeader']) ;
    itemFieldText ('Footer', $Record['InvoiceFooter']) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
    
    itemStart () ;
    itemHeader ('Delivery') ;
    itemField ('Delivery Terms', tableGetField ('DeliveryTerm', 'CONCAT(Name," (",Description,")")', (int)$Record['DeliveryTermId'])) ;  
    itemSpace () ;
    itemField ('Carrier', tableGetField ('Carrier', 'Name', (int)$Record['CarrierId'])) ;  
    itemSpace () ;
    itemField ('Street', $Record['Address1']) ;
    itemField ('', $Record['Address2']) ;
    itemField ('ZIP', $Record['ZIP']) ;
    itemField ('City', $Record['City']) ;
    itemField ('Country',  tableGetField ('Country', 'CONCAT(Name," - ",Description)', (int)$Record['CountryId'])) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("</tr></table>\n") ;
    if(isset($Result) === true){
        listStart () ;
        listHeader ('Lines') ;
        listRow () ;
        listHead ('', 50) ;
        listHead ('Pos', 30) ;
        if($Record['OrderTypeId'] == 10){
            listHead ('Consolidated', 75) ;    
        }
//        listHead ('Case', 50) ;
        listHead ('Article', 75) ;
        listHead ('Colour', 90) ;
        listHead ('Description') ;
        listHead ('Delivery', 70) ;
        listHead ('Done', 70) ;
        listHead ('Quantity', 100, 'align=right') ;
        listHead ('Surplus', 70, 'align=right') ;
        listHead ('Discount', 70, 'align=right') ;
        listHead ('Price', 90, 'align=right') ;
        listHead ('Total', 90, 'align=right') ;
        listHead ('', 8) ;

        while ($row = dbFetch($Result)) {
        listRow () ;
        listStyleIcon ('orderlineview', $row) ;
        listField ((int)$row['No']) ;
        if($Record['OrderTypeId'] == 10){
            listField ($row['OrderId']) ;
        }
//        listField (((int)$row['CaseId'] > 0) ? (int)$row['CaseId'] : '') ;
        listField ($row['ArticleNumber']) ;

        $field = '' ;
        if ($row['VariantColor']) {
            $field .= '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
            if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
            $field .= 'display:inline;"></div>' ;
            $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorDescription']) ;     
        }
        listFieldRaw ($field) ;

        listField ($row['Description']) ;

        $t = dbDateDecode($row['DeliveryDate']) ;
        listField (($t > 0) ? date ('Y-m-d', $t) : '') ;
        $s = ($row['Done']) ? sprintf ('%s', date('Y-m-d', dbDateDecode($row['DoneDate']))) : 'No' ;
        listfield ($s);
        listField (number_format ((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
        listField ((int)$row['Surplus'] . ' %', 'align=right') ;
        listField ((int)$row['Discount'] . ' %', 'align=right') ;
        listField (number_format((float)$row['PriceSale'], 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
        $v = (float)$row['PriceSale'] * (float)$row['Quantity'] ;
        if ((int)$row['Discount'] > 0) $v -= $v * (int)$row['Discount'] / 100 ;
        $total += $v ;
          $qtytotal += (float)$row['Quantity'] ;
        listField (number_format($v, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
        }
        if (dbNumRows($Result) == 0) {
        listRow () ;
        listField () ;
        listField ('No OrderLines', 'colspan=2') ;
        } else {
        // Total
        listRow () ;
        listField ('', 'colspan=8 style="border-top: 1px solid #cdcabb;"') ;
        listField (number_format ((float)$qtytotal, 2, ',', '.'), 'align=right style="border-top: 1px solid #cdcabb;"') ;
        listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;
        listField (number_format ($total, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right style="border-top: 1px solid #cdcabb;"') ;
        }
        listEnd () ;
    }
    else{
        $query = sprintf ('SELECT `order`.*, season.id as SeasonId, season.Name as SeasonName
            FROM `order`, season
            WHERE `order`.ConsolidatedId=%d and `order`.active=1 and season.id=`order`.seasonid', $Id) ;
        $cosolidatedorders = dbQuery ($query) ;

        while ($rowCon = dbFetch($cosolidatedorders)) {
            // $query = 'SELECT * FROM `order` WHERE Active=1 AND ConsolidatedId='.$Id;
            // $allConsolidatedOrders = dbQuery($query);
            listStart () ;
            listHeader ('Lines For Order ' . $rowCon['Id']) ;
            if (!$User['Extern']){
                itemStart () ;
                itemHeader () ;
                itemFieldIcon ('Order', $rowCon['Id'], $Navigation['Icon'], 'orderviewcon', (int)$rowCon['Id']) ;
                itemField ('Season', $rowCon['SeasonName']) ;
				ItemFieldIcon ('Confirmation', 'PDF', 'print.gif', 'printordercon', (int)$rowCon['Id']);
                flag ($rowCon, 'Ready') ;
                flag ($rowCon, 'Done') ;        
                itemInfo ($rowCon) ;
                itemSpace () ;
                itemSpace () ;
                itemSpace () ;
            }
            listStart () ;
            // listHeader ('Lines For Order ' . $rowCon['Id']) ;
            // listRow () ;
            listHead ('', 50) ;
            listHead ('Pos', 30) ;
 //           listHead ('Case', 50) ;
            listHead ('Article', 75) ;
            listHead ('Colour', 90) ;
            listHead ('Description') ;
            listHead ('Delivery', 70) ;
            listHead ('Done', 70) ;
            listHead ('Quantity', 100, 'align=right') ;
			listHead ('Boxnumbers', 100, 'align=right') ;
            listHead ('Surplus', 70, 'align=right') ;
            listHead ('Discount', 70, 'align=right') ;
            listHead ('Price', 90, 'align=right') ;
            listHead ('Total', 90, 'align=right') ;
            listHead ('', 8) ;
            $query = sprintf ('SELECT OrderLine.*,
                Article.Number AS ArticleNumber, Article.VariantColor,
                Color.Description AS ColorDescription, Color.Number AS ColorNumber,
                ColorGroup.Id AS ColorGroupId, ColorGroup.ValueRed, ColorGroup.ValueGreen, ColorGroup.ValueBlue,
                Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals
                FROM OrderLine
                LEFT JOIN Article ON Article.Id=OrderLine.ArticleId
                LEFT JOIN ArticleColor ON ArticleColor.Id=OrderLine.ArticleColorId
                LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
                LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId
                LEFT JOIN Unit ON Unit.Id=Article.UnitId
                WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 ORDER BY OrderLine.No', $rowCon['Id']) ;
            $Ress = dbQuery ($query) ;
            $singletotal = 0;
            $singleqtytotal = 0;
            while ($row = dbFetch($Ress)) {
            	listRow () ;
            	// listFieldIcon ($Navigation['Icon'], 'orderlineviewcon', (int)$row['Id']);
                listStyleIcon ('orderlineviewcon', $row) ;
                // listStyleIconMark ('orderlineviewcon', $row) ;

            	listField ((int)$row['No']) ;
 //           	listField (((int)$row['CaseId'] > 0) ? (int)$row['CaseId'] : '') ;
            	listField ($row['ArticleNumber']) ;

            	$field = '' ;
            	if ($row['VariantColor']) {
            	    $field .= '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
            	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
            	    $field .= 'display:inline;"></div>' ;
            	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorDescription']) ;	    
            	}
            	listFieldRaw ($field) ;

            	listField ($row['Description']) ;

            	$t = dbDateDecode($row['DeliveryDate']) ;
            	listField (($t > 0) ? date ('Y-m-d', $t) : '') ;
            	$s = ($row['Done']) ? sprintf ('%s', date('Y-m-d', dbDateDecode($row['DoneDate']))) : 'No' ;
            	listfield ($s);
            	listField (number_format ((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
				$query = 'SELECT group_concat(cast(boxnumber as char)) as box from (select boxnumber FROM orderquantity oq where oq.quantity>0 and oq.active=1 and orderlineid=' . $row['Id'] . ' group by boxnumber) Tab';
				$res = dbQuery ($query) ;
				$_row = dbFetch ($res) ;
				dbQueryFree ($res) ;
				listField ($_row['box'], 'align=right') ;
            	listField ((int)$row['Surplus'] . ' %', 'align=right') ;
            	listField ((int)$row['Discount'] . ' %', 'align=right') ;
            	listField (number_format((float)$row['PriceSale'], 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
            	$v = (float)$row['PriceSale'] * (float)$row['Quantity'] ;
            	if ((int)$row['Discount'] > 0) $v -= $v * (int)$row['Discount'] / 100 ;
                $singletotal += $v;
            	$total += $v ;
                $singleqtytotal += (float)$row['Quantity'] ;
                $qtytotal += (float)$row['Quantity'] ;
            	listField (number_format($v, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
            }
            if (dbNumRows($Ress) == 0) {
        	listRow () ;
        	listField () ;
        	listField ('No OrderLines', 'colspan=2') ;
            } else {
        	// Total
            listRow () ;
            listField ('SubTotal', 'colspan=8 style="border-top: 1px solid #cdcabb;"') ;
            listField (number_format ((float)$singleqtytotal, 2, ',', '.'), 'align=right style="border-top: 1px solid #cdcabb;"') ;
            listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;
            listField (number_format ($singletotal, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right style="border-top: 1px solid #cdcabb;"') ;
        	listRow () ;
        	listField ('GrandTotal', 'colspan=8 style="border-top: 1px solid #cdcabb;"') ;
        	listField (number_format ((float)$qtytotal, 2, ',', '.'), 'align=right style="border-top: 1px solid #cdcabb;"') ;
        	listField ('', 'colspan=3 style="border-top: 1px solid #cdcabb;"') ;
        	listField (number_format ($total, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right style="border-top: 1px solid #cdcabb;"') ;
            }
        }
    }

    return 0 ;
?>
