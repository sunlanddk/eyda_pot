<?php
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/list.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }

    function flagOrders (&$Record, $field) {
		if ($Record['Ready']) {
			itemField ('Order '.$Record['Id'], sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record['ReadyDate'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['ReadyUserId']))) ;
		} else {
			itemFieldRaw ($field.' '.$Record['Id'], formCheckbox ($field.'['.$Record['Id'].']', $Record['Ready'])) ;
		}
    }

    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('ConsolidateId', $Record['Id']) ;
    itemSpace () ;
    itemHeader () ;
    $query = 'SELECT * FROM `order` o WHERE o.ConsolidatedId='.$Record['Id'].' AND o.Active=1';
    $res = dbQuery($query);
    while ($row = dbFetch ($res)) {
    	// flag ($Record['Ready'], 'Ready') ;
    	flagOrders ($row, 'Order') ;
    }
	itemSpace () ;
    itemSpace () ;
    itemSpace () ;
    itemSpace () ;
    itemEnd () ;

    formEnd () ;

 ?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
