<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    // Filter Bar
    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'company' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'companyview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
	case 'company-new' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'customerview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
    }
   
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 70) ;
    listHead ('Consolidated Id', 70) ;
    if (!$HideCompany) {
	listHead ('Company') ;
    }
    listHead ('State', 70) ;
    listHead ('PO', 40) ;
    listHead ('Season', 90) ;
    listHead ('Reference', 90) ;
    listHead ('Description') ;
    listHead ('Sales person', 160) ;
    listHead ('Created', 120) ;
    if (!$HideDone) {
	listHead ('Done', 75) ;
    }

    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'orderview', (int)$row['Id']) ;
	listField ((int)$row['Id']) ;
	if (!$HideCompany) {
	    listField (sprintf ('%s (%s)', $row['CompanyName'], $row['CompanyNumber'])) ;
	}
	listField ($row['State']) ;
	listField ($row['ToPurchaseOrderId']>0?$row['ToPurchaseOrderId']:'') ;
	listField ($row['SeasonName']) ;
	listField ($row['Reference']) ;
	listField ($row['Description']) ;
	listField ($row['SalesUserName']) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
	if (!$HideDone) {
	    listField (($row['Done']) ? date ("Y-m-d", dbDateDecode($row['DoneDate'])) : 'No') ;
	}	
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Orders', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
