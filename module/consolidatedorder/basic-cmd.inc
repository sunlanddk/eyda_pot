<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    

    $fields = array (
		'PartDelivery'					=> array ('type' => 'checkbox',				'check' => false),
		'PartDeliveryLine'				=> array ('type' => 'checkbox',				'check' => false),
		'ConsolidatedInvoice'			=> array ('type' => 'checkbox',				'check' => false),
    ) ;

    $query = 'SELECT * FROM `order` WHERE ConsolidatedId='.$Id;
    $res = dbQuery($query);
    //$orders = dbFetch($res);
    $PartDelivery = 0;
    $PartDeliveryLine = 0;


    if(isset($_POST['PartDelivery']) === true){
    	if($_POST['PartDelivery'] == 'on'){
    		$PartDelivery = 1;
    	}
    }
    if(isset($_POST['PartDeliveryLine']) === true){
    	if($_POST['PartDeliveryLine'] == 'on'){
    		$PartDeliveryLine = 1;
    	}
    }

    while ($orders = dbFetch($res)) {
    	$row['PartDelivery'] = $PartDelivery;
    	$row['PartDeliveryLine'] = $PartDeliveryLine;
    	tableWrite('Order', $row, $orders['Id']);
    }
    

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
			case 'Ready':
					
				return true ;

			case 'Done':	
				return true ;
		}
	}

	$res = saveFields ('ConsolidatedOrder', $Id, $fields, true) ;
    if ($res) return $res ;

    return 0 ;    
?>
