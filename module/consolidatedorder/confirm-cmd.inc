<?php

    require_once _LEGACY_LIB.'/lib/file.inc' ;
    require_once _LEGACY_LIB.'/lib/save.inc' ;
    require_once _LEGACY_LIB.'/lib/table.inc' ;
    require_once _LEGACY_LIB.'/lib/mail.inc';
    require_once _LEGACY_LIB.'/module/order/stockExportToWeb.php';

	global $_ready ;
	$_ready = ($_POST['Ready']=='on') ? 1 : 0 ;
//  return 'test ' .  $_POST['Ready'] . ' ' . $_ready; 

    $fields = array (
		'Ready'			=> array ('type' => 'checkbox',				'check' => true),
		'ReadyUserId'	=> array ('type' => 'set'),
		'ReadyDate'		=> array ('type' => 'set'),
    ) ;

	function generatePdfFile_org() {
		 global $Id, $Config, $Record, $_ready;

		 $Config['savePdfToFile'] = true;
		 $Record['Ready'] = $_ready ;
		 require_once 'printorder.inc';
		 $Config['savePdfToFile'] = false;

		 return fileName('order', (int)$Id);
	}
	
    function generatePdfFile() {
        global $User, $Id, $Config, $Record, $_ready;

        $cons_record = $Record ;
		$Navigation['Function'] = 'printorder';
		include _LEGACY_LIB.'/module/order/load.inc';

        $Config['savePdfToFile'] = true;
//		$Record['Ready'] = $_ready ;
        include _LEGACY_LIB.'/module/order/printorder.inc';
        $Config['savePdfToFile'] = false;

        $Record = $cons_record ;

		return fileName('order', (int)$Id);
      }

	
    
    function checkfield ($fieldname, $value, $changed) {
		global $User, $Record, $Id, $fields ;
		global $flagcount, $flaglist ;
		switch ($fieldname) {
			case 'Ready':
				// Ignore if not setting flag
				if (!$changed or !$value) return false ;
				
				if ($Record['Proposal']==1) return 'Setting Ready is not allowed for proposal' ;

				// Any Order Positions
				$query = sprintf ("SELECT OrderLine.*, Article.Number AS ArticleNumber, Article.VariantColor, Article.CustomsPositionId, Article.MaterialCountryId, Article.KnitCountryId, Article.WorkCountryId FROM OrderLine LEFT JOIN Article ON Article.Id=OrderLine.ArticleId WHERE OrderLine.OrderId=%d AND OrderLine.Active=1", $Id) ;
				$result = dbQuery ($query) ;
				$n = 0 ;
				while ($row = dbFetch ($result)) {
					// Validate OrderLine
					if ($row['VariantColor'] and (int)$row['ArticleColorId'] == 0) return sprintf ('line %d, article %s: no color assigned', (int)$row['No'], $row['ArticleNumber']) ;
					if ((int)$row['Quantity'] == 0) return sprintf ('line %d, article %s: zero Quantity', (int)$row['No'], $row['ArticleNumber']) ;
					$n++ ;    
				}
				dbQueryFree ($result) ;

				if ($n == 0) return 'the Order can not be set Ready when it has no OrderLines' ;

				// Get and save exchangerate
				$fields['ExchangeRate']['value'] = tableGetField ('Currency', 'Rate', (int)$Record['CurrencyId']) ;
				
				// Set tracking information
				$fields['ReadyUserId']['value'] = $User['Id'] ;
				$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
				
				return true ;
		}
		return true ;	
	}
	$_consolidatedId = $Id ;

   $_attachmentArray = array() ;
    while ($row = dbFetch($cosolidatedorders)) {
//	return 'her ' . $Id . ' - ' . $row['Id'] ;
		$Id = $row['Id'] ;
		if ($Config['Instance'] == 'Test') {
			$_test=1 ;
		} else {
			sendStockDataToWebshop($Id) ;
		}
		$res = saveFields ('Order', $Id, $fields, true) ;
		if ($res) return $res ;

		// Generate and email
		$_pdffile = generatePdfFile() ; //fileName('order', (int)$Id); 
//		$_pdffile = fileName('order', (int)$Id); 

		//preparing pdf attachment for the mail
	   $_attachmentArray[] = 
			array(
				'path'     => $_pdffile,
				'mimeType' => 'application/pdf',
				'name'     => sprintf('Order #%d.pdf', $Id)
			)
		;
	}

	$_param['body'] = nl2br($_POST['BodyTxt']) . '<br>' ;
		if ($Record['OrderTypeId']==10) {
			$_mark="weborderconf" ;
		} else {
			$_mark="b2borderconf" ;
		}
	if ($Config['Instance'] == 'Test')
		$mailres=sendmail($_mark, $Record['CompanyId'], NULL, $_param, NULL, false, $_POST['Mail'], false, $_attachmentArray, sprintf(' to %s Order #%s', $Record['CompanyName'], $Record['OrderIds'] . ' ('.$Record['Id'].')'));
	else {
		$mailres=sendmail($_mark, $Record['CompanyId'], NULL, $_param, NULL, false, $_POST['Mail'] . ', sales@eyda.dk', false, $_attachmentArray, sprintf(' to %s Order #%s', $Record['CompanyName'], $Record['OrderIds'] . ' ('.$Record['Id'].')'));
	}
//$test = 'hallo '.   $_POST['Mail'] ;
	foreach ( $_attachmentArray as $key => $value) {
		$_pdffile = $value['path'] ;
		if (is_file($_pdffile) AND !$_POST['Ready']=='on') unlink ($_pdffile) ;
	}
	
    if ($mailres) return 'Sending email failed with system error message:<br>' . $mailres ;
	
    return 0 ;	
?>
