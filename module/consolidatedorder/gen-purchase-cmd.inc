<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	global $User, $Navigation, $Record, $Size, $Id ;

	// Validate Sales Order 
/*
	if ($Record['Ready']==0) {
		if ($_POST['Ready'] != 'on')  return 'Cant generate Purchase Order Since SalesOrder not ready' ;

		// Set ready
		$UpdOrder = array (
			'ExchangeRate'	=> tableGetField ('Currency', 'Rate', (int)$Record['CurrencyId']),
			'Ready'			=> 1,
			'ReadyUserId'	=> $User['Id'] ,
			'ReadyDate'		=> dbDateEncode(time())
		) ;
		tableWrite ('Order',$UpdOrder, $Id) ;		
	}
*/	
	// Find what to generate purchase orderlines for.
	$first=1 ;
	$NextLineNo = 1 ;
	$ErrorTxt=0 ;
	$CurrentTime = time () -1  ;

	$query = sprintf(
		"SELECT o.id as OrderId, o.companyid as CompanyId,
				a.UnitId as ArticleUnitId, a.Comment as ArticleComment,a.variantsize as ArticleVarinatSize, a.variantdimension as ArticleVariantDimension, a.variantcolor as ArticleVariantColor,
				ol.id as OrderLineId, ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid, ol.articlecolorid,ol.pricesale as Price,ol.LineFooter as LineFooter,
				oq.articlesizeid, oq.dimension, if(a.variantsize=1,oq.quantity,ol.quantity) as quantity,
				c.description as color, az.name as size, az.id as ArticleSizeId
		FROM (`order` o, orderline ol, article a)
		LEFT JOIN orderquantity oq ON  oq.orderlineid=ol.id and oq.active=1
	    LEFT JOIN articlecolor ac ON ol.articlecolorid=ac.id
	    LEFT JOIN color c ON  ac.colorid=c.id
	    LEFT JOIN articlesize az ON oq.articlesizeid=az.id
		WHERE ol.orderid=o.id and o.active=1 and ol.active=1 and ol.done=0
	    and o.done=0 and o.id=%d and a.id=ol.articleid
	    ORDER BY ol.id"
	, $Id);
	$res = dbQuery ($query) ;
	$NoOrderLines = dbNumRows($res) ;
	if ($NoOrderLines == 0) return 'No order lines' ;
	
	// Get supplier information
	$query = sprintf ("SELECT * FROM Company WHERE Active=1 AND Id=%d", $_POST['SupplierCompanyId']) ;
	$cres = dbQuery ($query) ;
	$Supplier = dbFetch ($cres) ;
	dbQueryFree ($cres) ;

	// Create new Purchase Order
	$PurchaseOrder = array (
//		'Id' => 0,
		'CompanyId' => $Supplier['Id'],
		'Description' => sprintf('Generated from sales order %s', $Record['Id']) ,
		'Reference' => '' ,
		'PurchaseUserId' => $Supplier['PurchaseRefId'],
		'CurrencyId' => $Supplier['PurchaseCurrencyId'],
		'PaymentId' => $Supplier['PurchasePaymentTermId'],
		'DeliveryTermId' => $Supplier['PurchaseDeliveryTermId'],
		'CarrierId' => $Supplier['PurchaseCarrierId'],
		'DeliveryId' =>  $Supplier['Id'],
//		'RequisitionHeader' => $data->sheets[$k]['cells'][3][2],
//		'RequisitionFooter' => $data->sheets[$k]['cells'][4][2],
		'FromCompanyId' => $Record['ToCompanyId'] //$User['CompanyId']
	) ;
	$PurchaseOrderId = tableWrite ('requisition', $PurchaseOrder) ;
		
	$OrderLineId = 0 ;
	while ($row = dbFetch ($res)) {
		if ($OrderLineId <> $row['OrderLineId']) {
			// Create new Purchase Order Line
		    $OrderLineId = $row['OrderLineId'] ;
		    
		    // Calculation needed: GP, Currency rate, logistic cost etc.
			$query = sprintf('SELECT * FROM articleprice WHERE OwnerId=%d and SupplierId=%d and articleid=%d and active=1',
								$PurchaseOrder['FromCompanyId'], $PurchaseOrder['CompanyId'],$row['articleid'] ) ;
			$res_ap = dbQuery ($query) ;
			$ap_row = dbFetch ($res_ap) ;

			if ($ap_row['Id']>0) {
				$LogisticCost = (float)$ap_row['Logistic']/$ap_row['QuantityStart'] ; 
				$PriceCost = (float)$ap_row['Purchase']/$ap_row['QuantityStart'] ; 								
			} else {
				// make calculated purchase price automatic 
/*
				$query = sprintf('SELECT * FROM tradeconfig WHERE CustomerId=%d and SupplierId=%d and active=1',
									$PurchaseOrder['FromCompanyId'], $PurchaseOrder['CompanyId'] ) ;
				$res_tradeconfig = dbQuery ($query) ;
				$tc_row = dbFetch ($res_tradeconfig) ;
				if ($tc_row['id']>0) { 
					if ($tc_row['LogisticFixed']>0)
						$LogisticCost = (float)$tc_row['LogisticFixed'] ;
					else
						$LogisticCost = (float)$tc_row['LogisticPct']*$row['Price']/100 ; 
					$PriceCost = (float)(100-$tc_row['GpPct'])*($row['Price']-$LogisticCost)/100 ; 
				}
				dbQueryFree ($res_tradeconfig) ;
*/
				$LogisticCost = 0 ; 
				$PriceCost = 0 ; 								
			}
			dbQueryFree ($res_ap) ;

		    // 9* articles and others?
			unset ($PurchaseOrderLine) ;
			$PurchaseOrderLine = array (
//				'Id' => 0,
				'RequisitionId' => $PurchaseOrderId,
				'ArticleId' => $row['articleid'],
				'ArticleColorId' => $row['articlecolorid'],
				'No' => (int)$NextLineNo,
				'Description' => $row['ArticleDescription'],
				'ArticleCertificateId' => 0,
				'Surplus' => $Supplier['PurchaseSurplus'],
				'PurchasePriceConsumption' => (float)$PriceCost,
				'ActualPriceConsumption' => (float)$PriceCost,
				'LogisticCostConsumption' => (float)$LogisticCost,
				'PurchasePrice' => (float)$PriceCost,
				'ActualPrice' => (float)$PriceCost,
				'LogisticCost' => (float)$LogisticCost,
				'CaseId' => 0,
				'ProductionId' => 0,
				'RequestedDate' => $row[DeliveryDate],
				'ConfirmedDate' => $row[DeliveryDate],
//				'ExpectedDate' => '',
				'AvailableDate' => $row[DeliveryDate],
				'LineFooter' => $row[LineFooter] , //$data->sheets[$k]['cells'][$i][15],
				'ArticleComment' => $Article['Comment'],
				'Group' => 1,
				'ArticlePurchaseUnitId' => (int)tableGetFieldWhere('articlepurchaseunit','Id',sprintf('ArticleId=%d AND UnitId=%d',$row['articleid'], $row['ArticleUnitId'])),								
				'ArticlePURatio' => 1,								
				'ArticlePUName' => tableGetField('Unit','Name',$row['ArticleUnitId']),								
				'ArticlePUDecimals' => (int)tableGetField('Unit','Decimals',$row['ArticleUnitId'])				
			) ;

			$PurchaseOrderLineId = tableWrite ('requisitionline', $PurchaseOrderLine) ;

			$NextLineNo ++ ;
		}

		// Create new Purchase Order Line Quantity
		unset ($PurchaseOrderLineQty) ;
		$PurchaseOrderLineQty = array (
//			'Id' => 0,
			'RequesitionLineId' => $PurchaseOrderLineId,
			'ArticleSizeId' => (int)$row['ArticleSizeId'],
//				'Dimension' => NULL,
			'Quantity' => $row['quantity'],
		) ;
		if ($row['ArticleVariantSize']) {
			$PurchaseOrderLineQty['ArticleSizeId'] =  (int)$row['articlesizeid'];
		}
		if ($row['ArticleVariantDimension']) {
			$PurchaseOrderLineQty['Dimension'] =  (int)$row['dimension'];
		}
//		if ($row['ArticleVariantDimension']) $Error_txt = $Error_txt . 'Dimension not supported in sales orders' ;
		$PurchaseOrderLineQtyId = tableWrite ('requisitionlinequantity', $PurchaseOrderLineQty) ;

	}
 	dbQueryFree ($res) ;
 	
 	dbQuery (sprintf("UPDATE `Order` SET ToPurchaseOrderId=%d where Id=%d", (int)$PurchaseOrderId, $Id)) ;

	if ($ErrorTxt==0)
		return navigationCommandMark ('requisitionview', (int)$PurchaseOrderId) ;

	return $ErrorTxt ;

?>
