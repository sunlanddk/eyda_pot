<?php

    switch ($Navigation["Parameters"]) {
	case "new":
	    unset ($Record) ;
	    break ;
   }

    require_once "lib/html.inc" ;

    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Record['Id']) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td><p>Number</p></td><td><input type=text name=Number maxlength=10 value=\"%s\" style=\"width:95px\"></td></tr>\n", htmlentities($Record["Number"])) ;
    printf ("<tr><td><p>Name</p></td><td><input type=text name=Name size=50 value=\"%s\"></td></tr>\n", htmlentities($Record["Name"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Phone</p></td><td><input type=text name=PhoneMain size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["PhoneMain"])) ;
    printf ("<tr><td><p>Fax</p></td><td><input type=text name=PhoneFax size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["PhoneFax"])) ;
    printf ("<tr><td><p>Mail</p></td><td><input type=text name=Mail size=40 value=\"%s\"></td></tr>\n", htmlentities($Record["Mail"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Reg.Number</p></td><td><input type=text name=RegNumber size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["RegNumber"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Customer</p></td><td><input type=checkbox name=TypeCustomer %s></td></tr>\n", ($Record["TypeCustomer"])?"checked":"") ;
    printf ("<tr><td><p>Supplier</p></td><td><input type=checkbox name=TypeSupplier %s></td></tr>\n", ($Record["TypeSupplier"])?"checked":"") ;
    printf ("<tr><td><p>Agent</p></td><td><input type=checkbox name=TypeAgent %s></td></tr>\n", ($Record["TypeAgent"])?"checked":"") ;
    printf ("<tr><td><p>Carrier</p></td><td><input type=checkbox name=TypeCarrier %s></td></tr>\n", ($Record["TypeCarrier"])?"checked":"") ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Internal</p></td><td><input type=checkbox name=Internal %s></td></tr>\n", ($Record["Internal"])?"checked":"") ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Hide from list</p></td><td><input type=checkbox name=ListHidden %s></td></tr>\n", ($Record["ListHidden"])?"checked":"") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>CompanyFooter</td><td><textarea name=CompanyFooter style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['CompanyFooter']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    print htmlItemSpace() ;
    print htmlItemInfo($Record) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
