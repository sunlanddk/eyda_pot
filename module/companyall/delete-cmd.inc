<?php

    require_once 'lib/table.inc' ;
    
    $query = sprintf ("SELECT Id FROM `Order` WHERE Active=1 AND CompanyId=%d", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Can't be deleted when Customer Orders is assigned - Use Hide from list instead" ;
	
    $query = sprintf ("SELECT Id FROM `Requisition` WHERE Active=1 AND CompanyId=%d", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Can't be deleted when Purchase Orders is assigned - Use Hide from list instead" ;


    $query = sprintf ("SELECT Id FROM User WHERE Active=1 AND CompanyId=%d", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Can't be deleted due to associated users" ;

    if ($Config['proEnable']) {
	$query = sprintf ("SELECT Id FROM Project WHERE Active=1 AND CompanyId=%d", $Id) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	dbQueryFree ($result) ;
	if ($count > 0) return "Can't be deleted due to associated projects" ;
    }
    
    tableDelete ('Company', $Id) ;

    return 0
?>
