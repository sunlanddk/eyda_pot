<?php

    require 'lib/save.inc' ;

    $prefix = $Navigation['Parameters'] ;
    
    $fields = array (
	$prefix.'Address1'	=> array (),
	$prefix.'Address2'	=> array (),
	$prefix.'ZIP'		=> array (),
	$prefix.'City'		=> array (),
	$prefix.'CountryId'	=> array ('type' => 'integer')
    ) ;
  
    return saveFields ('Company', $Id, $fields) ;
?>
