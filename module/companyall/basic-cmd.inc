<?php

    require 'lib/save.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new':
	    unset ($Record) ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id ;
	switch ($fieldname) {
	    case 'Number':
		if (!$changed) return false ;
		
		// Check that number does not allready exist
		$query = sprintf ('SELECT Id FROM Company WHERE Active=1 AND Number="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Company allready existing' ;
		return true ;
	}
	return false ;
    }
     
    $fields = array (
	'Number'	=> array ('mandatory' => true,	'check' => true),
	'Name'		=> array ('mandatory' => true),
	'PhoneMain'	=> array (),
	'PhoneFax'	=> array (),
	'Mail'		=> array (),
	'RegNumber'	=> array (),
	'Internal'	=> array ('type' => 'checkbox'),
	'TypeCustomer'	=> array ('type' => 'checkbox'),
	'TypeCarrier'	=> array ('type' => 'checkbox'),
	'TypeSupplier'	=> array ('type' => 'checkbox'),
	'TypeAgent'	=> array ('type' => 'checkbox'),
	'ListHidden'	=> array ('type' => 'checkbox'),
	'Comment'		=> array (),
	'CompanyFooter'		=> array ()
    ) ;
  
    // Save record
    $res = saveFields ('Company', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new article
	    require_once 'lib/navigation.inc' ;
	    return navigationCommandMark ('companyview', $Record['Id']) ;
    }
    
    return 0 ;    

    
?>
