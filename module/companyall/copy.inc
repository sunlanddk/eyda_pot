<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

	Global $User ;


//	if (stripos($User['Title'],'multi')===false) return 'Operation not allowed with user permissions' ;


	$Record['Number']='' ;
	$Record['ToCompanyId']=1 ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Number', formText ('Number', $Record['Number'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Sales Ref', formDBSelect ('SalesUserId', (int)$Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.TypeSupplier=1 AND Company.Active=1 AND User.CompanyId=Company.id AND User.Active=1 AND User.Login=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Owner Company', formDBSelect ('ToCompanyId', (int)$Record['ToCompanyId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  
    itemSpace () ;
    printf ("<tr><td><p>View new record</p></td><td><input type=checkbox name=ChangeOwner %s> </td></tr>\n", (1)?"checked":"") ;

    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
