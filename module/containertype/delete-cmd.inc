<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM Container WHERE ContainerTypeId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the ContainerType has associated Containers") ;

    // Do delete
    tableDelete ('ContainerType', $Id) ;

    return 0
?>
