<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 175) ;
    listHead ('Description') ;
    listHead ('Tara (kg)', 80, 'align=right') ;
    listHead ('Volumen (m3)', 80, 'align=right') ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['TaraWeight'], 'align=right') ;
	listField ($row['Volume'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Container types', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
