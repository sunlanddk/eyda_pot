<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemField ($field, 'No') ;
	}
    }

    itemStart () ;
    itemHeader () ;
    itemField ('Name', $Record['Name']) ;
    itemField ('Description', $Record['Description']) ;

    itemSpace () ;
    itemFieldIcon ('Company', empty($Record['AltCompanyName'])?$Record['CompanyName']:$Record['AltCompanyName'].' ('.$Record['CompanyName'].')', 'company.gif', 'companyview', $Record['CompanyId']) ;

    itemSpace () ;
    itemField ('State', $Record['State']) ;

    itemSpace () ;
    itemField ('Departure', date ('Y-m-d', dbDateDecode($Record['DepartureDate']))) ;
	
	itemSpace () ;
    itemFieldIcon ('Owner', tableGetField ('Company', 'Company.Name', (int)$Record['FromCompanyId']), 'company.gif', 'companyview', $Record['FromCompanyId']) ;

    itemSpace () ;
    itemField ('DeliveryTerm', $Record['DeliveryTermName']) ;

    itemSpace () ;
    itemField ('Carrier', $Record['CarrierName']) ;
    
    $query = sprintf ('SELECT orderadresses.AltCompanyName, orderadresses.Address1, orderadresses.Address2, orderadresses.ZIP, orderadresses.City, country.Description as CountryName FROM (pickorder, orderadresses, country) WHERE country.Id=orderadresses.CountryId AND pickorder.ToId=%d AND pickorder.Active=1 AND orderadresses.OrderId=pickorder.ReferenceId AND orderadresses.Active=1 AND orderadresses.Type="shipping"', $Id) ;
    $res = dbQuery ($query) ;
    $Addresses = dbFetch ($res);
    itemSpace () ;
    itemField ('Delivery', $Record['AltCompanyName']) ;  
    itemField ('', $Record['Address1']) ;  
    itemField ('', $Record['Address2']) ;  
    itemField ('', $Record['ZIP'] . ' ' . $Record['City']) ;  
    itemField ('', $Record['CountryName']) ;  

    itemSpace () ;
    $query = sprintf ('SELECT * FROM webshipperlabel WHERE StockId=%d AND Active=1', $Id) ;
    $res = dbQuery ($query) ;
    $labels = false;
    while ($row = dbFetch ($res)) {
        itemFieldIcon (sprintf("%s", ($labels === false ? 'Labels' : '')), sprintf("%s label %d", $row['Type'], $row['LabelId']), 'print.gif', 'webshipperlabel', $row['Id'], null, null, null, true) ;
        $labels = true;
    }
    itemSpace () ;
    dbQueryFree ($res) ;
    

    itemSpace () ;
    flag ($Record, 'Ready') ;
    flag ($Record, 'Departed') ; 
    flag ($Record, 'Invoiced') ; 
    flag ($Record, 'Done') ; 

    itemInfo ($Record) ;
    itemEnd () ;

    return 0 ;
?>
