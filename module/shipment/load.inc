<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/fieldtype.inc' ;

    $StateField = 'IF(Stock.Done,"Done",IF(Stock.Departed,"Departed",IF(Stock.Invoiced,"Invoiced",IF(NOT Stock.Ready,"Defined","Ready"))))' ;

    $DelNoteField = 'IF(Stock.Ready=1,Stock.Id,"")' ;

    $companylimit = sprintf(' AND (Stock.FromCompanyId in (select CompanyId From UserCompanies where UserId=%d) or Stock.FromCompanyId=%d or Stock.FromCompanyId=0)',  $User['Id'], $User['CompanyId']) ;

    $queryFields =  'Stock.*, o.Reference as OReference,
		    CONCAT(Company.Name," (",Company.Number,")") AS CompanyName,
			Owner.Name as Owner,
		    If(Invoice.id is null,"No","Yes") AS InvoiceMade,
		    CONCAT(DeliveryTerm.Description," (",DeliveryTerm.Name,")") AS DeliveryTermName,
		    Carrier.Name AS CarrierName,
		    CONCAT(Country.Name," - ",Country.Description) AS CountryName, 
		    ' . $StateField . ' As State,' . $DelNoteField . ' As DelNote';
    
//    $queryTables = '(Stock, container, item)
    $queryTables = '(Stock)
		    left join Invoice ON Invoice.stockid=Stock.Id and Invoice.active=1
		    LEFT JOIN Company ON Company.Id=Stock.CompanyId
		    LEFT JOIN Company Owner ON Owner.Id=Stock.FromCompanyId
		    LEFT JOIN DeliveryTerm ON DeliveryTerm.Id=Stock.DeliveryTermId
		    LEFT JOIN Carrier ON Carrier.Id=Stock.CarrierId
		    LEFT JOIN Country ON Country.Id=Stock.CountryId
		    LEFT JOIN container c ON c.Id=(select MAX(Id) as Id FROM container WHERE container.StockId=Stock.Id LIMIT 1)
		    LEFT JOIN item i ON i.Id=(select MAX(Id) as Id FROM item WHERE item.ContainerId=c.Id LIMIT 1)
		    LEFT JOIN orderline ol ON ol.Id=(select MAX(Id) as Id FROM orderline WHERE orderline.Id=i.OrderLineId LIMIT 1)
		    LEFT JOIN `order` o ON o.Id=ol.OrderId' ;

    switch ($Navigation['Function']) {
	case 'departlist' :
	case 'departlist-cmd' :
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Shopify Order',		'db' => 'o.Reference'),
		array ('name' => 'Name',		'db' => 'Stock.Name'),
		array ('name' => 'Company',		'db' => 'Company.Name'),
		array ('name' => 'Owner',		'db' => 'Owner.Name'),
		array ('name' => 'Carrier',		'db' => 'Carrier.Name'),
		array ('name' => 'State',		'db' => $StateField),
		array ('name' => 'DelNote',		'db' => $DelNoteField),
		array ('name' => 'Departure',		'db' => 'DepartureDate',	'type' => FieldType::DATE)		
	    ) ;

	    $queryGroup = 'Stock.Id' ;
	    switch ($Navigation['Parameters']) {
		case 'transport' :
		    $queryClause = sprintf ('Stock.Type="shipment" AND Stock.BatchId=%d AND Stock.Active=1', $Id) ;
		    $queryGroup = 'Stock.Id' ;
		    break ;
		case 'company' :
		    // Specific company
		    $query = sprintf ('SELECT Company.Id, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $HideCompany = true ;

		    $queryClause = sprintf ('Stock.Type="shipment" AND Stock.CompanyId=%d AND Stock.Active=1', $Id) ;
		    break ;

		default :
		    $queryClause = 'Stock.Type="shipment" AND Stock.Done=0 AND Stock.Active=1' ;
//		    $queryClause = 'Stock.Type="shipment" AND (Stock.Done=0 or Stock.BatchId=10179) AND Stock.Active=1' ;
		    break ;
	    }
//	$queryClause = $queryClause . ' and Container.stockid=Stock.id and Container.active=1 and item.containerid=container.id and item.active=1' ;

//	    if ($User['CompanyId']==1169)
	 	    $queryClause = $queryClause . $companylimit ;

	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, '', $queryGroup) ;
	    if (is_string($Result)) return $Result ;
	    return listView ($Result, 'shipmentview') ;

	    // Query list
	    $query = sprintf ('SELECT %s FROM %s WHERE %s ORDER BY Stock.Name', $queryFields, $queryTables, $queryClause) ;
	    $Result = dbQuery ($query) ;
	    return 0 ;

	    
	case 'basic' :
	case 'basic-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;

		case 'neworder' :
		    // Create Shipment from Order
		    // Get Order
		    $query = sprintf ('SELECT `Order`.*, MIN(OrderLine.DeliveryDate) AS DeliveryDate, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM `Order` LEFT JOIN OrderLine ON OrderLine.OrderId=Order.Id AND OrderLine.Active=1 AND OrderLine.DeliveryDate>="%s" LEFT JOIN Company ON Company.Id=Order.CompanyId WHERE Order.Id=%d AND Order.Active=1 GROUP BY Order.Id', dbDateOnlyEncode(time()), $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    return 0 ;
	    }

	    // Fall through
	case 'view' :
	case 'revert' :
	case 'revert-cmd' :
	case 'delete-cmd' :
	case 'printnote' :
	    // Query Stock
	    $query = sprintf ('SELECT %s FROM %s WHERE Stock.Id=%d AND Stock.Type="shipment" AND Stock.Active=1', $queryFields, $queryTables, $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    $query = sprintf ('SELECT * FROM webshipperlabel WHERE StockId=%d AND Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    if(dbNumRows($Result) > 0){
	    	navigationPasive ('genwebshipperlabel') ;	
	    }
	    // Validate type
	    if ($Record['Type'] != 'shipment') return sprintf ('%s(%d) invalid type, value "%s"', __FILE__, __LINE__, $Record['Type']) ;

	    // Navigation
	    if (!$Record['Ready']) {
		navigationPasive ('invoice') ;
		navigationPasive ('orderclose') ;

	    }
	    navigationPasive ('printlabel') ;
	    
	    return 0 ;
	case 'printlabel' :
		$query = sprintf ('SELECT * FROM webshipperlabel WHERE Id=%d', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
		return 0;
	case 'webshipperlabel' :
		$query = sprintf ('SELECT * FROM Stock WHERE Id=%d', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
		return 0;
	case 'orderclose' :
	case 'orderclose-cmd' :
	    // Query Invoice
	    $query = sprintf ('SELECT %s FROM %s WHERE Stock.Id=%d AND Stock.Type="shipment" AND Stock.Active=1', $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		$query = sprintf ("SELECT ol.No as OrderLineNo, ol.orderid As OrderNo, ol.id as OrderLineId,
								Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
								sum(i.quantity) as ShipQty,
								(select sum(if(isnull(oq.quantity),ols.quantity,oq.quantity)) as Qty From orderline ols left join orderquantity oq on oq.orderlineid=ols.id and oq.active=1	where ols.id=i.orderlineid and ols.active=1 group by ols.id) as OQty,
								(SELECT sum(i.quantity) as ShipQty FROM (item i, container c, stock s) where i.orderlineid=ol.id and i.containerid=c.id and c.stockid=s.id and s.`type`='shipment' and i.active=1 and c.active=1 and s.active=1 group by i.orderlineid) as TotalShipQty
							FROM (item i, container c)
							LEFT JOIN orderline ol ON ol.id=i.orderlineid and ol.active=1
							LEFT JOIN Article a on a.id=i.articleid
							LEFT JOIN Unit ON Unit.Id=a.UnitId	
							WHERE c.stockid=%d and i.containerid=c.id and i.active=1
							GROUP BY ol.id", $Id) ;
		$Result = dbQuery ($query) ;
	    return 0 ;

    }

    return 0 ;
?>
