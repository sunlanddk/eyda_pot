<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    
    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Name', $Record['Name']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemEnd () ;
  
    formStart () ;
    itemStart () ;
    itemHeader() ;

    function flagRevert (&$Record, $field) {
	if ($Record[$field]) {
	    itemFieldRaw ($field, formCheckbox ($field, 1, 'margin-left:4px;') . date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])) . ', ' . tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    itemField ($field, 'no') ;
	}
    }
    flagRevert ($Record, 'Ready') ;
    flagRevert ($Record, 'Departed') ;
    flagRevert ($Record, 'Invoiced') ;

    itemInfo ($Record) ;
    itemEnd () ;
    
    formEnd () ;
   
    return 0 ;
?>
