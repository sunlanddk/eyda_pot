<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;
    require_once 'lib/table.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;
	if ($Record['Done']) return 'Shipment already done' ;

	if ($_POST['Done'] == 'on')  {
		// Set Flags
		$Shipment = array (	
			'Departed'		=>  1,
			'DepartedUserId'	=>  $User['Id'],
			'DepartedDate'	=>  dbDateEncode(time()),
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time())
		) ;
		tableWrite ('Stock', $Shipment, $Id) ;
	} 

    // Set Orderlines to done!
	if ($_POST['CompletelyDone'] == 'on')
	{
		// set �ll orders done
		$query = sprintf ('select orderid from item i, container c, orderline ol
										 where c.stockid=%d and i.containerid=c.id and i.orderlineid=ol.id and i.active=1 and ol.active=1
										 group by ol.orderid', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $row['orderid']) ;
			
			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $row['orderid'])) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;

			// Set state on WEbshop order if there is one.
			$OrderTypeId=tableGetField('`Order`', 'OrderTypeId', $row['orderid']) ;
			if ($OrderTypeId == 10) {
				// Set state in weborder.
				$WEBID=tableGetField('`Order`', 'Reference', $row['orderid']) ;
				dbReConnect('192.168.2.251', 'webshop') ;
				$query = sprintf ("UPDATE webshop.ws_order SET STATUS=5 Where WEBID='%s'", $WEBID) ;
				$res = dbQuery ($query) ;
				dbReConnect('localhost', 'devel') ;
			}		
		}
		dbQueryFree ($result) ;
		
		// set all lines done that are not already done
		$query = sprintf ('select * 
					from orderline 
					join
						(select ol.orderid from item i, container c, orderline ol
						where c.stockid=%d and i.containerid=c.id and i.orderlineid=ol.id and i.active=1 and ol.active=1 and ol.done=0
						group by ol.id) Orders
					where orderline.orderid=Orders.orderid and orderline.active=1 and orderline.done=0
					group by id', $Id) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Orderline = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
			) ;
			tableWrite ('Orderline', $Orderline, $row['Id']) ;
		}
		dbQueryFree ($query) ;
	} else {
	foreach ($_POST['OrderLineId'] as $ol_id => $flag) {
		if ($ol_id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;
		$Orderline = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
		) ;
		tableWrite ('Orderline', $Orderline, $ol_id) ;

		// Set Orderdone?
		// last line? set order done
		$OrderId=tableGetField('OrderLine', 'OrderId', $ol_id) ;
		$query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0', $OrderId) ;
		$res = dbQuery ($query) ;
		if (!($row = dbFetch ($res))) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $OrderId) ;

			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $OrderId)) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
				
			// Set state on WEbshop order if there is one.
			$OrderTypeId=tableGetField('`Order`', 'OrderTypeId', $OrderId) ;
			if ($OrderTypeId == 10) {
				// Set state in weborder.
				$WEBID=tableGetField('`Order`', 'Reference', $OrderId) ;
				dbReConnect('192.168.2.251', 'webshop') ;
				$query = sprintf ("UPDATE webshop.ws_order SET STATUS=5 Where WEBID='%s'", $WEBID) ;
				$res = dbQuery ($query) ;
//				dbReConnect('localhost', 'devel') ;
			}		
		}
	}
	}
	
    return 0 ;
?>
	
