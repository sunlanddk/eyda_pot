<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;


    switch ($Navigation['Parameters']) {
	case 'company' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'companyview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
    }
       // Filter Bar
    listFilterBar () ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Shopify Order', 80) ;
    listHead ('Name', 220) ;
    listHead ('Description') ;
    if (!$HideCompany) {
		listHead ('Company') ;
    }
    listHead ('Owner', 200) ;
    listHead ('DeliveryTerms', 90) ;
    listHead ('Carrier', 80) ;
    listHead ('State', 55) ;
    listHead ('Delnote', 45) ;
    listHead ('Invoice', 40) ;
    listHead ('Departure', 80) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'shipmentview', (int)$row['Id']) ;
	listField ($row['OReference']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	if (!$HideCompany) {
	    listField ($row['CompanyName']) ;
	}
	listField ($row['Owner']) ;
	listField ($row['DeliveryTermName']) ;
	listField ($row['CarrierName']) ;
	listField ($row['State']) ;
	listField ($row['DelNote']) ;
	listField ($row['InvoiceMade']) ;
	listField (date('Y-m-d', dbDateDecode ($row['DepartureDate']))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No active Shipments', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
