<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'lib/variant.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;
    
    $ShowRetailPrice = false ;
	switch ((int)$Record['FromCompanyId']) {
		case 1169:	// NT Japan
		case 787:	// GC
		case 1430:  // Drappa Dot
			$ShowRetailPrice = true ;
		case 2:		// Novotex
		case 3592:	// Lvivtex
		default:
		    break;
    }


    class PDF extends FPDF {
	
	function Header() {
	    global $Record, $Company, $MyCompany, $Page, $Orders, $FirstPage;

	    $this->SetAutoPageBreak(false) ; 

	    // Borders
	    $this->Line(15,97,205,97) ;
	    $this->Line(15,115,205,115) ;
	    $this->Line(15,269,205,269) ;
	    $this->Line(15,272,205,272) ;
	    $this->Line(15,282,205,282) ;
	    $this->Line(15,97,15,269) ;
	    $this->Line(15,272,15,282) ;
	    $this->Line(205,97,205,269) ;
	    $this->Line(205,272,205,282) ;

	    // My Company Header
//	    $this->Image ('image/novotex/A4Logo.jpg', 146, 22, 64) ;
//	    $this->Image ('image/novotex/A4Logo.jpg', 140, 13, 64) ;
	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['FromCompanyId']) ;
		if ((int)$Record['FromCompanyId']==1340) 
	    		$this->Image ($ImageString, 165, 13, 35 );
		else if ((int)$Record['FromCompanyId']==787) 
	    		$this->Image ($ImageString,  167, 10, 30 ) ;
		else if ((int)$Record['FromCompanyId']==1430) 
	    		$this->Image ($ImageString, 140, 13, 64 ) ;
//		else if ((int)$Record['FromCompanyId']==1430) 
//	    		$this->Image ($ImageString, 160, 13, 35 ) ;
		else if ((int)$Record['FromCompanyId']==5060) 
	    		$this->Image ($ImageString, 160, 13, 35 ) ;
		else if ((int)$Record['FromCompanyId']==516) 
	    		$this->Image ($ImageString, 160, 13, 35 ) ;
		else
	    		$this->Image ($ImageString, 140, 13, 64 ) ;
	    $this->SetFont('Arial','',10);
	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
//	    $this->Cell(49, 5, $MyCompany['NameExpanded'], 0, 1, 'R') ;
	    $this->SetFont('Arial','',8);
	    $this->Cell(49, 4, $CompanyMy['Name'], 0, 1, 'R') ;
	    $this->Cell(49, 4, $MyCompany['Address1'], 0, 1, 'R') ;
	    if ($MyCompany['Address2']) $this->Cell(48, 4, $MyCompany['Address2'], 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('%s-%s %s', $MyCompany['CountryName'], $MyCompany['ZIP'], $MyCompany['City']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Telephone %s', $MyCompany['PhoneMain']), 0, 1, 'R') ;
	    if ($MyCompany['PhoneFax']!='')
		    $this->Cell(49, 4, sprintf ('Fax %s', $MyCompany['PhoneFax']), 0, 1, 'R') ;
	    if ((int)$Record['FromCompanyId']==1091) {
	    	$this->Cell(49, 4, sprintf ('Email %s', $MyCompany['Mail']), 0, 1, 'R') ;
    		$this->Cell(49, 4, sprintf ('Reg. No %s', $MyCompany['RegNumber']), 0, 1, 'R') ;
	    } else {
    		$this->Cell(49, 4, sprintf ('Reg. No %s', $MyCompany['RegNumber']), 0, 1, 'R') ;
	    	$this->Cell(49, 4, sprintf ('Email %s', $MyCompany['Mail']), 0, 1, 'R') ;
	    }

	    // Delivery Address
	    $this->SetMargins(25,0,0) ;
	    $this->SetY (40) ;
	    $this->SetFont('Arial','B',9);
	    $this->Cell (30, 4, 'Delivery adress:', 0, 1) ;
	    $this->SetFont('Arial','',9);
	    $this->Cell(80, 4, empty($Record['AltCompanyName'])?$Company['Name']:$Record['AltCompanyName'].' ('.$Company['Name'].')', 0, 1) ;
	    if ($Record['Address1']) {
		    $this->Cell(80, 4, $Record['Address1'], 0, 1) ;
		    if ($Record['Address2']) $this->Cell(80, 4, $Record['Address2'], 0, 1) ;
		    $this->Cell(80, 4, sprintf ('%s %s', $Record['ZIP'], $Record['City']), 0, 1) ;
		    $this->Cell(80, 4, tableGetField('Country','Description',$Record['CountryId']), 0, 1) ;
	    } else {
			$this->Cell(80, 4, $Company['Address1'], 0, 1) ;
		    if ($Company['Address2']) $this->Cell(80, 4, $Company['Address2'], 0, 1) ;
		    $this->Cell(80, 4, sprintf ('%s %s', $Company['ZIP'], $Company['City']), 0, 1) ;
		    $this->Cell(80, 4, $Company['CountryDescription'], 0, 1) ;
	    }
	    // Text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(120,0,0) ;
	    $this->SetY (75) ;
	    $this->Cell (30, 8, 'Packing List' . (($Record['Ready']) ? ($Record['PartShip']) ? ' (Partly shipment)' : '' : ' (draft)'), 0, 1) ;
	    $this->SetFont('Arial','',9);
	    $this->Cell (20, 4, 'Number') ;
	    if ($Record['Ready']) $this->Cell(50, 4, $Record['Id']) ;
	    $this->Ln () ;
	    $this->Cell (20, 4, 'Date') ;
	    if ($Record['Ready']) $this->Cell(50, 4, date('Y.m.d', dbDateDecode($Record['DepartureDate']))) ;
	    
	    // Header
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,0,0) ;
	    $this->SetY (98) ;
	    $this->Cell(30, 4, 'Customer') ;
	    $this->Cell(74, 4, $Company['Number']) ;
	    $this->Cell(20, 4, 'Page') ;
	    $this->Cell(50, 4, $this->PageNo().' of {nb}', 0, 1) ;

	    $this->Cell(30, 4, 'Send By') ;
	    $this->Cell(74, 4, $Record['CarrierName']) ;
	    $this->Cell(20, 4, 'Sales Rep') ;
	    $this->Cell(50, 4, '', 0, 1) ;

	    $this->Cell(30, 4, 'Terms of Delivery') ;
	    $this->Cell(74, 4, $Record['DeliveryTermDescription']) ;
		if (substr($Record['Name'],0,6)=='Extern') {
		    $this->Cell(20, 4, 'Order') ;
			$this->Cell(50, 4, substr($Record['Name'],7), 0, 1) ;
		} else {
		    $this->Cell(20, 4, 'Reference') ;
			$this->Cell(50, 4, '', 0, 1) ;
	    }

	    $this->Cell(30, 4, 'Terms of Payment') ;
	    $this->Cell(74, 4, '') ;
	    $this->Cell(20, 4, 'Date Due') ;
	    $this->Cell(80, 4, '', 0, 1) ;

	    // Initialize for main page
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,118,6) ;
	    $this->SetY (118) ;
	    $this->SetAutoPageBreak(true, 30) ; 
	    
	    
	    // Sup page header
	    switch ($Page) {
		case 'item' :
    // Insert Order Invoice header?
		    if ($FirstPage) {
			$FirstPage = FALSE ;
			if ($Company['InvoiceHeader']) {
			foreach ($Orders as $order=>$invheader) {
			 	$this->Cell (26, 4, 'Order: '. $order, 0, 0, 'R') ;
	    			$this->MultiCell (186, 4, $invheader) ;
			}
			$this->Ln();
			}
		    }
		    // Item List
		    if (!strstr($Record['Description'], '@')) {
		    $this->Cell(16, 4, 'Container', 0, 0, 'R') ;
		    $this->Cell(13, 4, 'Order', 0, 0, 'R') ;
		    $this->Cell(22, 4, 'Article', 0, 0, 'R') ;
		    $this->Cell(3, 4, '') ;
		    $this->Cell(56, 4, 'Description') ;
		    $this->Cell(16, 4, 'Size', 0, 0, 'R') ;
		    $this->Cell(44, 4, 'Color', 0, 0, 'R') ;
		    $this->Cell(18, 4, 'Quantity', 0, 0, 'R') ;
		    $this->Ln () ;
		    $this->Ln (2) ;
		    }
		    break ;
	    }
		

	}

	function Footer () {
	    global $Record, $Total, $Page ;

	    // Sup page header
	    switch ($Page) {
		default :
		    $this->SetXY (16, 273) ;
		    $this->Cell(25, 4, 'Carried over') ;
		    break ;

		case 'end' :
		    $this->SetXY (16, 273) ;
		    $this->Cell(20, 4, 'Colli', 0, 0, 'R') ;
		    $this->Cell(25, 4, 'Gross Weight', 0, 0, 'R') ;
		    $this->Cell(25, 4, 'Net Weight', 0, 0, 'R') ;
		    $this->Cell(25, 4, 'Volume', 0, 0, 'R') ;
		    if ($Total['UnitName'] != '') $this->Cell(93, 4, 'Total Quantity', 0, 0, 'R') ;

		    $this->SetXY (16, 277) ;
		    $this->Cell(20, 4, (int)$Total['Collie'], 0, 0, 'R') ;
		    $this->Cell(25, 4, sprintf ('%0.1f Kg', (float)$Total['GrossWeight']), 0, 0, 'R') ;
		    $this->Cell(25, 4, sprintf ('%0.1f Kg', (float)$Total['NetWeight']), 0, 0, 'R') ;
		    $this->Cell(25, 4, sprintf ('%0.2f m3', (float)$Total['Volume']), 0, 0, 'R') ;
		    if ($Total['UnitName'] != '') $this->Cell(93, 4, number_format ($Total['Quantity'], (int)$Total['UnitDecimals'], ',', '.') . ' ' . $Total['UnitName'], 0, 0, 'R') ;
		    break ;
	    }
	    if (((int)$Record['FromCompanyId']==1340) or ((int)$Record['FromCompanyId']==787)) {
		    $this->SetFont('Arial','B',10);
		    $this->SetMargins(16,0,0) ;
		    $this->SetY (285) ;
		    $this->Cell (10, 4, 'No returned goods without prior agreement - if no agreement the goods are returned to you on your account', 0, 1) ;
	    }
	}

	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}
	
	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }
 
    // Use previously generated pdf-file ?
    if ($Record['Ready']) {
	$file = fileName ('packinglist', (int)$Record['Id']) ;
	if (is_file($file)) {
	    $pdfdoc = file_get_contents($file) ;
    	    httpNoCache ('pdf') ;
	    httpContent ('application/pdf', sprintf('PackingList%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
	    print ($pdfdoc) ; 
	    return 0 ;
	}
    }
    
    // Variables
    $FirstPage = TRUE ;
    $Total = array ('Quantity' => 0, 'Amount' => 0) ;
    $Page = 'item' ;

    // Get Full Company information
    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['CompanyId']) ;
    $result = dbQuery ($query) ;
    $Company = dbFetch ($result) ;
    dbQueryFree ($result) ;
   
    // Get Additional information for Record
    $Record['CountryDescription'] = tableGetField ('Country', 'Description', (int)$Record['CountryId']) ;
    $Record['CarrierName'] = tableGetField ('Carrier', 'Name', (int)$Record['CarrierId']) ;
    $Record['DeliveryTermDescription'] = tableGetField ('DeliveryTerm', 'Description', (int)$Record['DeliveryTermId']) ;

    // Get My Company information
//    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', parameterGet('CompanyMy')) ;
    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['FromCompanyId']) ;
    $result = dbQuery ($query) ;
    $MyCompany = dbFetch ($result) ;
    dbQueryFree ($result) ;
    // Insert tripple-space in name
    if ($MyCompany['Name'] != '') {
	$s = $MyCompany['Name']{0} ;
	$n = 1 ;
	while ($MyCompany['Name']{$n}) $s .= '  ' . $MyCompany['Name']{$n++} ;
	$MyCompany['NameExpanded'] = strtoupper($s) ;
    }
    
//if (!strstr($Record['Description'], '@')) {
    // Get Items for this Shipment for the Container list
    $query =   'SELECT
		Item.Id, Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity, 
		VariantCode.VariantUnit as VariantUnit, VariantCode.VariantSize As VariantSize, VariantCode.VariantModelRef as VariantModelRef,
		VariantCode.VariantColorDesc As VariantColorDesc, VariantCode.VariantColorCode As VariantColorCode, VariantCode.VariantDescription As VariantDescription,
		Container.Id AS ContainerId, Container.GrossWeight, Container.TaraWeight, Container.Volume,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
		Color.Number AS ColorNumber, Color.Description AS ColorDescription,
		ArticleSize.Name AS SizeName,
		OrderLine.Id AS OrderLineId, OrderLine.InvoiceFooter,
		Order.Id AS OrderId, Order.Reference AS OrderReference,
		Case.Id AS CaseId, Case.CustomerReference, Order.InvoiceHeader
		FROM Container
		INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
		INNER JOIN Article ON Article.Id=Item.ArticleId
		LEFT JOIN Unit ON Unit.Id=Article.UnitId
		LEFT JOIN ArticleSize ON ArticleSize.Id=Item.ArticleSizeId
		LEFT JOIN ArticleColor ON ArticleColor.Id=Item.ArticleColorId
		LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
		LEFT JOIN VariantCode On VariantCode.ArticleId=Item.ArticleId and VariantCode.ArticleColorId=Item.ArticleColorId and VariantCode.ArticleSizeid=Item.ArticleSizeId and VariantCode.Active=1
		LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId' ;
    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$Record['Id']) ;
    $query .= ' GROUP BY Container.Id, Order.Id, Article.Id, Article.VariantColor*Item.ArticleColorId, Article.VariantSize*Item.ArticleSizeId' ;
    $query .= ' ORDER BY Container.Id, Order.Id, Article.Number, Color.Number, ArticleSize.DisplayOrder' ;
//return $query ;
    $res = dbQuery ($query) ;
    $Item = array () ;
    $ContainerId = 0 ;
    $n = 0 ;
    while ($row = dbFetch ($res)) {
		// Correct if variant to ship is in pcs: Eg. more stock units per ship unit
		if ($row['VariantUnit']) {
//			if (($row['Quantity'] % $PcsPerVariantUnit[$row['VariantUnit']])>0)			
			$row['Quantity']=(int)($row['Quantity']/$PcsPerVariantUnit[$row['VariantUnit']]) ;
			$row['UnitName']=$row['VariantUnit'] ;
			$row['UnitDecimals']=0 ;
		}		
		if ($row['VariantDescription']) $row['ArticleDescription']=$row['VariantDescription'];
		if ($row['VariantModelRef']) {
			$VariantModelRef=explode(',',strval($row['VariantModelRef']));
			$row['ArticleDescription'].= ', ' . $VariantModelRef[0] ;
		}
		if ($row['VariantColorDesc']) $row['ColorDescription']=$row['VariantColorDesc'];
		if ($row['VariantColorCode']) $row['ColorNumber']=$row['VariantColorCode'];
//		if ($row['VariantSize']) $row['SizeName']=$row['VariantSize'];

		// Save item
        $Item[(int)$row['Id']] = $row ;
        $Orders[$row['OrderId']] = $row['InvoiceHeader'];
	
	// Totals
	if ($n == 0) {
	    $Total['UnitName'] = $row['UnitName'] ;
	    $Total['UnitDecimals'] = (int)$row['UnitDecimals'] ;
	} else {
	    if ($Total['UnitName'] != $row['UnitName']) $Total['UnitName'] = '' ;
	}
	$Total['Quantity'] += (float)$row['Quantity'] ;
	if ($ContainerId != (int)$row['ContainerId']) {
	    $Total['Collie'] += 1 ;
	    $Total['GrossWeight'] += $row['GrossWeight'] ;
	    $Total['NetWeight'] += $row['GrossWeight'] - $row['TaraWeight'] ;
	    $Total['Volume'] += $row['Volume'] ;
	    $ContainerId = (int)$row['ContainerId'] ;
	}

	$n++ ;
    }
    dbQueryFree ($res) ;
    if (count($Item) == 0) return 'no Items for this Shipment' ;
//}

    // Get Items for this Shipment for the Article list
    $query =   'SELECT
		Item.Id, Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity, 
		VariantCode.VariantUnit as VariantUnit, VariantCode.VariantSize As VariantSize, VariantCode.VariantModelRef as VariantModelRef,
		VariantCode.VariantColorDesc As VariantColorDesc, VariantCode.VariantColorCode As VariantColorCode, VariantCode.VariantDescription As VariantDescription,
		Container.Id AS ContainerId, Container.GrossWeight, Container.TaraWeight, Container.Volume,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Unit.Name AS UnitName, Unit.Decimals AS UnitDecimals,
		Color.Number AS ColorNumber, Color.Description AS ColorDescription,
		ArticleSize.Name AS SizeName,
		OrderLine.Id AS OrderLineId, OrderLine.InvoiceFooter, OrderLine.No AS OrderLineNo,
		Order.Id AS OrderId, Order.Reference AS OrderReference, Order.CurrencyId as CurrencyId, Order.SeasonId as SeasonId,
		Case.Id AS CaseId, Case.CustomerReference
		FROM Container
		INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
		INNER JOIN Article ON Article.Id=Item.ArticleId
		LEFT JOIN Unit ON Unit.Id=Article.UnitId
		LEFT JOIN ArticleSize ON ArticleSize.Id=Item.ArticleSizeId
		LEFT JOIN ArticleColor ON ArticleColor.Id=Item.ArticleColorId
		LEFT JOIN VariantCode On VariantCode.ArticleId=Item.ArticleId and VariantCode.ArticleColorId=Item.ArticleColorId and VariantCode.ArticleSizeid=Item.ArticleSizeId and VariantCode.Active=1
		LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
		LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
		LEFT JOIN `Case` ON Case.Id=OrderLine.CaseId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId' ;
    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$Record['Id']) ;
    $query .= ' GROUP BY OrderLine.Id, Article.Id, Article.VariantColor*Item.ArticleColorId, Article.VariantSize*Item.ArticleSizeId' ;
    $query .= ' ORDER BY Order.Id, OrderLine.No, Article.Number, Color.Number, ArticleSize.DisplayOrder' ;
    $res = dbQuery ($query) ;
    $Line = array () ;
    $n = 0 ;
    while ($row = dbFetch ($res)) {
		// Correct if variant to ship is in pcs: Eg. more stock units per ship unit
		if ($row['VariantUnit']) {
			$row['Quantity']=(int)($row['Quantity']/$PcsPerVariantUnit[$row['VariantUnit']]) ;
			$row['UnitName']=$row['VariantUnit'] ;
			$row['UnitDecimals']=0 ;
		}
		if ($row['VariantDescription']) $row['ArticleDescription']=$row['VariantDescription'];
		
		if ($row['VariantDescription']) $row['ArticleDescription']=$row['VariantDescription'];
		if ($row['VariantModelRef']) {
			$VariantModelRef=explode(',',strval($row['VariantModelRef']));
			$row['ArticleDescription'].= ', ' . $VariantModelRef[0] ;
		}
		
		if ($row['VariantColorDesc']) $row['ColorDescription']=$row['VariantColorDesc'];
		if ($row['VariantColorCode']) $row['ColorNumber']=$row['VariantColorCode'];
//		if ($row['VariantSize']) $row['SizeName']=$row['VariantSize'];
	// New Line ?
	if ($n == 0 or
	    (int)$Line[$n]['OrderLineId'] != (int)$row['OrderLineId'] or
	    (int)$Line[$n]['ArticleId'] != (int)$row['ArticleId'] or
	    ($row['VariantColor'] and ((int)$Line[$n]['ArticleColorId'] != (int)$row['ArticleColorId']))) {
	    // New line
	    $n++ ;
	    $Line[$n] = $row ;
	    $Line[$n]['Size'] = array () ;
	} else {
	    // Add quantity to existing line
	    $Line[$n]['Quantity'] += $row['Quantity'] ;
	}

	if ($row['VariantSize']) {
	    $Line[$n]['Size'][(int)$row['ArticleSizeId']] = array ('Name' => $row['SizeName'], 'Quantity' => $row['Quantity']) ;
	}
    }
    dbQueryFree ($res) ;
    
//require_once 'lib/log.inc' ;
//logPrintVar ($Item, 'Item') ;
    
    // Make PDF
    $pdf=new PDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->AddPage() ;


    // Loop Items
    if (!strstr($Record['Description'], '@'))
    foreach ($Item as $item) {
	$pdf->Cell (16, 4, (int)$item['ContainerId'], 0, 0, 'R') ;
	$pdf->Cell (13, 4, (int)$item['OrderId'], 0, 0, 'R') ;
	$pdf->Cell (22, 4, $item['ArticleNumber'], 0, 0, 'R') ;
	$pdf->Cell (3, 4, '') ;
	$pdf->Cell (56, 4, $pdf->TruncString ($item['ArticleDescription'], 52)) ;
	$pdf->Cell (16, 4, $item['SizeName'], 0, 0, 'R') ;
	$pdf->Cell (26, 4, $pdf->TruncString ($item['ColorDescription'], 24), 0, 0, 'R') ;
	$pdf->Cell (18, 4, $item['ColorNumber'], 0, 0, 'R') ;
	
	$pdf->Cell (18, 4, number_format((float)$item['Quantity'], (int)$item['UnitDecimals'], ',', '.'), 0, 0, 'R') ;
	$pdf->Ln () ;
    }

    $pdf->Ln (8) ;
    $Page = 'line' ;
    
    foreach ($Line as $line) {
	// Check if sufficent space is availage on page for next position
	if ((int)$line['OrderId'] == 0 and !$line['VariantColor'] and !$line['VariantSize']) $pdf->Ln(4) ;
	$pdf->RequireSpace (32) ;
	
	// Order
	if ((int)$line['OrderId'] > 0) {
	    $pdf->Cell(15, 4, 'Order') ;
	    $pdf->Cell(89, 4, sprintf ('%d.%d', (int)$line['OrderId'], (int)$line['OrderLineNo'])) ;
	    $pdf->Cell(60, 4, 'Reference: '. $line['OrderReference']) ;
	    $pdf->Ln () ;
	}
	
	// Article Number and Reference
	$pdf->Cell(15, 4, 'Article') ;
	$pdf->Cell(26, 4, $line['ArticleNumber']) ;
	$pdf->Cell(63, 4, $pdf->TruncString ($line['ArticleDescription'], 60)) ;
	if ((int)$line['CaseId'] > 0) {
	    $pdf->Cell(60, 4, 'Reference: '. $line['CustomerReference']) ;
	}
	$pdf->Ln () ;

	// Color
	if ($line['VariantColor']) {
	    $pdf->Cell(15, 4, 'Colour') ;
	    if ((int)$line['ArticleColorId'] > 0) {
		$pdf->Cell(26, 4, $line['ColorNumber']) ;
		$pdf->Cell(60, 4, $pdf->TruncString ($line['ColorDescription'], 60)) ;
	    } else {
		$pdf->Cell(10, 4, '-') ;
	    }
	    $pdf->Ln () ;
	}
	
	// Recommended sales price?
	if ($ShowRetailPrice) {
		$tmpquery = sprintf('select cmp.RetailPrice as RetailPrice from collectionmemberprice cmp, collectionmember cm, collection c
							where c.seasonid=%d and cm.collectionid=c.id and cmp.collectionmemberid=cm.id
							and cmp.currencyid=%d and cm.articleid=%d and cm.articlecolorid=%d
							and c.active=1 and cm.active=1 and cmp.active=1 ', $line['SeasonId'],$line['CurrencyId'],$line['ArticleId'],$line['ArticleColorId']) ; 
			$result = dbQuery ($tmpquery) ;
			$row = dbFetch ($result) ;
			
		if ($row['RetailPrice']>0) {
			$pdf->Cell(41, 4, 'Recommended Retail Price') ;
			$pdf->Cell(60, 4,  number_format ((float)$row['RetailPrice'], 2, ',', '.')) ;
			$pdf->Ln () ;
		}
		dbQueryFree ($result) ;
	}

	// Sizes
	if ($line['VariantSize']) {
	    $n = 0 ;
	    foreach ($line['Size'] as $size) {
		if (($n % 8) == 0) {
		    if ($n > 0) $pdf->Ln () ;
		    $pdf->Ln (2) ;
		    $pdf->RequireSpace (8) ;
		    $pdf->Cell(15, 4, 'Size') ;
		    $pdf->Ln () ;
		    $pdf->Cell(15, 4, 'Quantity') ;
		}

		$pdf->SetXY ($pdf->GetX(), $pdf->GetY()-4) ;		
		$pdf->Cell(16, 4, $size['Name']) ;
		$pdf->SetXY ($pdf->GetX()-16, $pdf->GetY()+4) ;		
		$pdf->Cell(16, 4, number_format ((float)$size['Quantity'], (int)$line['UnitDecimals'], ',', '.')) ;
		$n++ ;
	    }
	    $pdf->Ln () ;
	}

	// Quantity header
	$pdf->SetXY (182, $pdf->GetY()-8) ;
	$pdf->Cell(22, 4, 'Quantity', 0, 0, 'R') ;
	$pdf->Ln () ;

	// Quantity
	$pdf->SetX (182) ;
	$pdf->Cell(22, 4, number_format((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', '.') . ' ' . $line['UnitName'], 0, 0, 'R') ;
	$pdf->Ln () ;


	// OrderLine Invoice Footer
    if ($Company['OrderLineFooter']) {
		if ($line['InvoiceFooter']) {
	    	$pdf->Ln (2) ;
	    	$pdf->MultiCell (186, 4, $line['InvoiceFooter']) ;
		}    
    }

	$pdf->Ln (4) ;
	if ($Record['DeliveryComment']) {
    	$pdf->Ln (2) ;
		$pdf->MultiCell (250, 4, $Record['DeliveryComment']) ;
	}    
 	$pdf->Ln (4) ;
   }

    $Page = 'end' ;

    // Generate PDF document
    $pdfdoc = $pdf->Output('', 'S') ;

    // Download
    if (headers_sent()) return 'stop' ;
    httpNoCache ('pdf') ;
    httpContent ('application/pdf', sprintf('PackingList%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
    print ($pdfdoc) ; 

    // Save file
    if ($Record['Ready']) {
	$file = fileName ('packinglist', (int)$Record['Id'], true) ;
	$resource = fopen ($file, 'w') ;
	fwrite ($resource, $pdfdoc) ;
	fclose ($resource) ;
	chmod ($file, 0640) ;
    }

//    if ($Navigation['Parameters'] == 'pick') return navigationCommandMark ('pickorders', 0) ;

    return 0 ;
?>
