<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;


    switch ($Navigation['Parameters']) {
	case 'company' :
	    itemStart () ;
	    itemSpace () ;
	    itemFieldIcon ('Company', $Record['CompanyName'], 'company.gif', 'companyview', $Record['Id']) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;	    
    }
   
    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 150) ;
    listHead ('Description') ;
    if (!$HideCompany) {
	listHead ('Company') ;
    }
    listHead ('DeliveryTerms', 100) ;
    listHead ('Carrier', 100) ;
    listHead ('State', 80) ;
    listHead ('Departure', 80) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'shipmentview', (int)$row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	if (!$HideCompany) {
	    listField ($row['CompanyName']) ;
	}
	listField ($row['DeliveryTermName']) ;
	listField ($row['CarrierName']) ;
	listField ($row['State']) ;
	listField (date('Y-m-d', dbDateDecode ($row['DepartureDate']))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No active Shipments', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
