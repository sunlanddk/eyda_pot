<?php 
require_once('lib/http.inc');

$ch = curl_init($Record['LabelUrl'].$Record['LabelId']);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer 5f1ec30a4aee3be406c9160070bd5929667d19b7f5843fdc5cac67b1239b9a76', 'Content-Type: application/vnd.api+json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);    
$resultObj = json_decode($result, true);

$pdfdoc =  base64_decode($resultObj['data']['attributes']['base64']);
httpNoCache ('pdf') ;
httpContent ('application/pdf', sprintf('Order%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
print ($pdfdoc) ;

return 0;