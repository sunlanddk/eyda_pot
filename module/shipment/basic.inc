<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
	}
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    $Record['DepartureDate'] = dbDateEncode(time()) ;
	    $Record['FromCompanyId'] = $User['CompanyId'] ;
	    break ;

	case 'neworder' :
	    // Default
	    // Use fields from Order
	    $Record['Name'] = sprintf ('O%d', $Record['Id']) ;
	    $Record['FromCompanyId'] = $Record['ToCompanyId'] ;
	    $Record['DepartureDate'] = ($Record['DeliveryDate']) ? $Record['DeliveryDate'] : dbDateOnlyEncode(time()) ;

	    // Fall through
    
	default :
	    itemStart () ;
	    itemSpace () ;
	    itemField ('Company', $Record['CompanyName']) ;
	    itemSpace () ;
	    itemEnd () ;
	    break ;
    }

    // Form
    formStart () ;
    formCalendar () ;
    itemStart () ;
    itemHeader () ;


    switch ($Navigation['Parameters']) {
	case 'new' :
	    itemFieldRaw ('Company', formDBSelect ('CompanyId', (int)$Record['CompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCustomer=1 AND Active=1 ORDER BY Name', 'width:250px;')) ;  
	    itemSpace () ;
	    break ;
    }

    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 20)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%')) ;
    itemSpace () ;
    itemFieldRaw ('Departure', formDate ('DepartureDate', dbDateDecode($Record['DepartureDate']))) ;

    itemSpace () ;
    if (tableGetFieldWhere('Company', 'Id', sprintf('TypeSupplier=1 And Internal=1 AND Active=1 And Id=%d', $Record['FromCompanyId']))==0 and $Record['FromCompanyId']>0)
	 itemFieldRaw ('Owner Company', formText ('FromCompanyId', (int)$Record['FromCompanyId'], 8,'','disabled') . tableGetFieldWhere('Company', 'Name', sprintf ('Id=%d', (int)$Record['FromCompanyId']))) ;  
    else
	 itemFieldRaw ('Owner Company', formDBSelect ('FromCompanyId', (int)$Record['FromCompanyId'], sprintf('SELECT Company.Id, Company.Name AS Value FROM Company WHERE Company.TypeSupplier=1 AND Company.Internal=1 AND Company.Active=1 ORDER BY Value'), 'width:250px;')) ;  

    switch ($Navigation['Parameters']) {
	case 'new' :
	    break ;

	default :
	    itemSpace () ;
	    itemFieldRaw ('Delivery Terms', formDBSelect ('DeliveryTermId', (int)$Record['DeliveryTermId'], 'SELECT Id, CONCAT(Name," (",Description,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Name', 'width:250px;')) ;  
	    itemSpace () ;
	    itemFieldRaw ('Carrier', formDBSelect ('CarrierId', (int)$Record['CarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Name', 'width:250px;')) ;  
	    itemSpace () ;
	    /*itemFieldRaw ('Street', formText ('Address1', $Record['Address1'], 50)) ;
	    itemFieldRaw ('', formText ('Address2', $Record['Address2'], 50)) ;
	    itemFieldRaw ('ZIP', formText ('ZIP', $Record['ZIP'], 20)) ;
	    itemFieldRaw ('City', formText ('City', $Record['City'], 50)) ;
	    itemFieldRaw ('Country',  formDBSelect ('CountryId', (int)$Record['CountryId'], 'SELECT Id, Description AS Value FROM Country WHERE Active=1 ORDER BY Name', 'width:250px;')) ;*/

	    break ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	case 'neworder' :
	    break ;

	default :
	    itemSpace () ;
	    flag ($Record, 'Ready') ;
	    flag ($Record, 'Departed') ;
	    flag ($Record, 'Invoiced') ;
	    itemSpace () ;
	    itemFieldRaw ('DeliveryComm', formTextArea ('DeliveryComment', $Record['DeliveryComment'], 'width:100%;height:100px;')) ;
	    itemSpace () ;
	    itemInfo ($Record) ;
    }
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
