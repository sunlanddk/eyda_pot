<?php

    require_once 'lib/table.inc' ;

    // Verify references
    $query = sprintf ("SELECT Id FROM Container WHERE StockId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return ("the Shipment has associated Containers") ;

    // Do delete  
    tableDelete ('Stock', $Id) ;

    return 0
?>
