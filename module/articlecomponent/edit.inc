<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    unset ($Record['ComponentId']) ;
	    unset ($Record['Percent']) ;
	    $Id = -1 ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Component</td><td>%s</td></tr>\n", htmlDBSelect ('ComponentId style="width:200px"', $Record['ComponentId'], sprintf ('SELECT Component.Id, concat(Component.Name, " - ", Component.Description) AS Value FROM Component LEFT JOIN ArticleComponent ON ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1 AND ArticleComponent.ComponentId=Component.Id WHERE Component.Active=1 AND (Component.Id=%d OR ISNULL(ArticleComponent.Id)) ORDER BY Value', $Record['ArticleId'], $Record['ComponentId']))) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Percent</td><td><input type=text name=Percent maxlength=3 size=3' value=\"%d\"> %%</td></tr>\n", (int)$Record['Percent']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
