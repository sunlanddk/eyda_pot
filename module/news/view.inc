<?php
 
    require_once 'lib/save.inc' ;
    require_once 'lib/html.inc' ;
    
    printf ("<H2>%s</H2>\n", htmlentities($Record['Header'])) ;
    printf ("<p style='padding-right:8px;'>%s</p>\n", str_replace('  ', '&nbsp;&nbsp;', nl2br(htmlentities($Record['Text'])))) ;

    printf ("<br><HR width=100%% size=1 noshade>\n") ;

    printf ("<table class=item>\n") ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;

    // Update user last read mark
    if (dbDateDecode($User['LastNewsDate']) < dbDateDecode($Record['CreateDate'])) {
	$query = sprintf ("UPDATE User SET LastNewsDate='%s' WHERE Id=%d", $Record['CreateDate'], $User['Id']) ;
	dbQuery ($query) ;
    }

    return 0 ;
?>
