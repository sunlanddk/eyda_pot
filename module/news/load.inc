<?php

    require_once 'lib/navigation.inc' ;

    define ('LINES', 40) ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Get page offset
	    $offset = (int)$_GET["offset"] ;

	    // Last page ?
	    if ($offset == -1) {
		$query = sprintf ("SELECT COUNT(Id) AS Count FROM News WHERE Active=1") ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
	    	dbQueryFree ($result) ;

		$offset= $row['Count'] - LINES ;
		if ($offset < 0) $offset = 0 ;
	    }

	    // Query records to list
	    $query = sprintf ("SELECT News.*, CONCAT(User.FirstName,' ',User.LastName,' (',User.Loginname,')') AS UserName FROM News LEFT JOIN User ON User.Id=News.CreateUserId WHERE News.Active=1 ORDER BY News.CreateDate DESC LIMIT %d, %d", $offset, LINES+1) ;
	    $Result = dbQuery ($query) ;
	    $count=dbNumRows ($Result) ;
	    
	    if ($offset > 0) {
		if ($offset < LINES) $offset=LINES ;
		navigationSetParameter ("previous", "offset=".((int)(($offset-LINES)/LINES)*LINES)) ;
	    } else {
		navigationPasive ("first") ;
		navigationPasive ("previous") ;
	    }

	    if ($count > LINES) {
		navigationSetParameter ("next", sprintf ("offset=%d", ((int)(($offset+LINES)/LINES))*LINES)) ;
		navigationSetParameter ("last", "offset=-1") ;
	    } else {
		navigationPasive ("next") ;
		navigationPasive ("last") ;
	    }

	    return 0 ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    return 0 ;
    }
	    
    if ($Id <= 0) return 0 ;

    // Get record
    $query = sprintf ("SELECT * FROM News WHERE Id=%d AND Active=1", $Id) ;
    $Result = dbQuery ($query) ;
    $Record = dbFetch ($Result) ;
    dbQueryFree ($Result) ;

    return 0 ;
?>
