<?php 

//echo json_encode($_POST);
require_once 'lib/table.inc' ;

if (!(int)$_POST['Company'] > 0) {
	return 'Please select a company!';
}

if (!isset($_POST['CompanyCollectionMember'])) {
	return 'Select some collection members to add to the company collection!';
}

$selectedCollectionMembers = [];

foreach ($_POST['CompanyCollectionMember'] as $key => $value) {
	if ($value == 'on') {
		$selectedCollectionMembers[] = $key;
	}
}

processCollectionMembers($selectedCollectionMembers, $existingResult, $_POST['Company']);

function processCollectionMembers($selected, $existing, $company) {

	foreach ($selected as $sId) {
		if (in_array($sId, $existing)) {
			$keyToRemove = array_search($sId, $existing);
			unset($existing[$keyToRemove]);
			continue;
		} else {
			$rowToWrite = array(
				'CompanyId' => $company,
				'CollectionMemberId' => $sId
			);
			tableWrite('companycollectionmember', $rowToWrite);
		}
	}

	foreach ($existing as $eId) {
		tableDelete ('companycollectionmember', tableGetFieldWhere('companycollectionmember', 'Id', sprintf("CompanyId = %d AND CollectionMemberId = %d", (int)$company, (int)$eId ))) ;
	}
}

return 0;
?>