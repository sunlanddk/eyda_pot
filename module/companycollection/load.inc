<?php

switch ($Navigation['Function']) {
	case 'list' :
		$query = "SELECT ccm.CompanyId AS Id, c.Name FROM companycollectionmember ccm LEFT JOIN company c ON c.Id = ccm.CompanyId WHERE ccm.Active = 1 GROUP BY ccm.CompanyId";
		$Result = dbRead($query);
		return 0;

	case 'edit' :
	case 'save-cmd':
		$query = "
			SELECT cm.id as Id, a.Number, a.Description, c.Description AS 'Color' FROM collectionmember cm
			LEFT JOIN article a ON cm.ArticleId = a.Id AND a.Active = 1
			LEFT JOIN articlecolor ac ON cm.ArticleColorId = ac.Id AND ac.Active = 1
			LEFT JOIN color c ON ac.ColorId = c.Id AND c.Active = 1
			WHERE cm.CollectionId = 1 AND cm.Active = 1 ORDER BY a.Number, cm.Id";
		$Result = dbQuery($query);

		$existingQuery = sprintf("SELECT ccm.CollectionMemberId FROM companycollectionmember ccm WHERE ccm.CompanyId = %d AND ccm.Active = 1", $Id);
		$existingResult = [];
		$existingOutput = dbRead($existingQuery);
		foreach ($existingOutput as $key => $value) {
			$existingResult[] = $value['CollectionMemberId'];
		}
		return 0;
	case 'delete-cmd':
		$existingQuery = sprintf("SELECT ccm.Id FROM companycollectionmember ccm WHERE ccm.CompanyId = %d AND ccm.Active = 1", $Id);
		$existingResult = dbRead($existingQuery);
}

return 0;
?>