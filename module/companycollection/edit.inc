<?php

require_once 'lib/item.inc' ;
require_once 'lib/form.inc' ;
require_once 'lib/list.inc' ;


$new = false;

switch ($Navigation['Parameters']) {
	case 'new' :
		$new = true;
    break ;
}

formStart();
itemStart();
if ($new) {
	$companyQuery = sprintf(
		'SELECT c.Id, c.Name AS Value 
		FROM company c 
		LEFT JOIN companycollectionmember ccm ON c.Id = ccm.CompanyId AND ccm.Active = 1
		WHERE c.TypeCustomer = 1 AND c.Active = 1 AND ccm.Id IS NULL 
		GROUP BY c.Id 
		ORDER BY c.Id');
	itemFieldRaw('Select company', formDBSelect('Company', 0, $companyQuery) );
} else {
	itemField('Company', tableGetField('company', 'Name', $Id) );
	itemFieldRaw('', formHidden('Company', $Id) );
}

itemSpace();
itemEnd();

listStart();
listHeader();
listRow();
listHead('', 20);
listHead('Number', 50);
listHead('Description', 200);
listHead('Color');
while($row = dbFetch($Result)) {
	listRow();
	$checkQuery = sprintf("SELECT Id FROM companycollectionmember WHERE CollectionMemberId=%d AND CompanyId=%d AND Active = 1", $row['Id'], $Id);
	listFieldRaw(formCheckbox('CompanyCollectionMember[' . $row['Id']  .']', ( !empty(dbRead($checkQuery)) ? 1 : 0 ) ));
	listField($row['Number']);
	listField($row['Description']);
	listField($row['Color']);
}
listEnd();



formEnd();




return 0;
?>