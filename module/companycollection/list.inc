<?php

require_once 'lib/list.inc' ;
require_once 'lib/table.inc';


if (empty($Result)){
	echo "Press 'Create' to create a new company collection!";
	return 0;
}

listStart();
listRow();
listHead('', 25);
listHead('Name', 150);
listHead('Report');
foreach ($Result as $row) {
	listRow () ;
	listFieldIcon('edit.gif', 'editcc',  $row['Id']);
	listField($row['Name']) ;
	listFieldIcon('report.gif', 'reportcc',  $row['Id']);
}
listEnd();

return 0;
?>