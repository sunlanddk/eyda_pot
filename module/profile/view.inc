<?php
require_once('lib/config.inc');

   print_r('<div id="buttons-wrapper"  style="height: 50px;">');
    if($User["2FA"] == "") {
        print_r("<input type='submit' id='createButton' style='width:100px;' value='Create 2FA' onclick='redirectTo(\"create\")'></input>");
    } else {
        print_r("<input type='submit' id='resetButton' style='width:100px;' value='Reset 2FA' onclick='redirectTo(\"reset\")'></input>");
    }
    print_r('</div>');
    ?>
    <script>
        function loader () {
            var loader = document.createElement('div');
            loader.id = 'loader';
            var buttonsWrapper = document.getElementById("buttons-wrapper");
            buttonsWrapper.style ="opacity: 0.5;"
            buttonsWrapper.appendChild(loader);
        }
        function redirectTo(type) {
            <?php
                $cipher_algo = openssl_get_cipher_methods()[0];
                $encrypt_key = '2846dja5waY#%"/Ehbdiskisl';
                $iv = base64_decode("IlY9Ia3OB+6Z3Q53yR49\/w==");
                $loginname = $User["Loginname"];
                $userId = $User["Id"];
                $time = time();
                $token = $loginname . "-" . (string)$time;
                $token = openssl_encrypt($token, $cipher_algo, $encrypt_key, OPENSSL_RAW_DATA, $iv);
                $hashedToken = base64_encode($token);
            ?>
            if(type === "create") {
                window.open('<?php echo _LEGACY_URI."/login/create/" . $hashedToken;?>', '_blank').focus();
            } else if(type === "reset") {
                window.open('<?php echo _LEGACY_URI."/login/reset/" . $hashedToken;?>', '_blank').focus();
            }
            loader()
                setTimeout(function() {
                    location.reload();
                }, 5000);
        }
    </script>
    <style>
        #loader {
            position: absolute;
            left: 50%;
            top: 20%;
            z-index: 1;
            width: 60px;
            height: 60px;
            margin: -76px 0 0 -76px;
            border: 16px solid #f3f3f3;
            border-radius: 50%;
            border-top: 16px solid #919191;
            -webkit-animation: spin 2s linear infinite;
            animation: spin 2s linear infinite;
        }
        @-webkit-keyframes spin {
            0% { -webkit-transform: rotate(0deg); }
            100% { -webkit-transform: rotate(360deg); }
        }
        @keyframes spin {
            0% { transform: rotate(0deg); }
            100% { transform: rotate(360deg); }
        }
        .animate-bottom {
            position: relative;
            -webkit-animation-name: animatebottom;
            -webkit-animation-duration: 1s;
            animation-name: animatebottom;
            animation-duration: 1s
        }
        @-webkit-keyframes animatebottom {
            from { bottom:-100px; opacity:0 } 
            to { bottom:0px; opacity:1 }
        }
        @keyframes animatebottom { 
            from{ bottom:-100px; opacity:0 } 
            to{ bottom:0; opacity:1 }
        }
    </style>
<?php
    return 0 ;
?>
