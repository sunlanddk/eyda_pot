<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;


	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 50000000)
		    return 'file too big, max 50000 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if ($FileUpload) {
		// Update database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		$row_index=1 ;
		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
				$PickOrder = array (	
					'Type'			=>  "Extern",	// Pick order from external customer
					'Reference'			=>  0,			// Order number - C1
					'FromId'			=> 0,				// 
					'CompanyId'			=>  0,			// Shop Code	- C9
					'OwnerCompanyId'		=>  1091,			// Katvig
					'HandlerCompanyId'	=> 18,				// Lvivtex
					'Packed'			=>  0,
					'Instructions'		=>  "GLS labels to be applied on cartons",
					'DeliveryDate'		=>  "2009-07-15"			// Expected pick and pack complete - C8
				) ;

				$PickOrderLine = array (	
					'VariantCodeId'		=>  "",			//
					'VariantCode'		=>  "",			// EAN code - C2 
					'OrderedQuantity'	=>  0,				//		- C4		
					'PickedQuantity'	=>  0,			//
					'PackedQuantity'	=>  0,			//
					'PickOrderId'		=>	0,
					'VariantDescription'		=>	"",
					'VariantColor'		=>	"",
					'VariantSize'		=>	"",
					'Done'				=>  0
				) ;
	
	   			$index = 1;
   			
   				// Get and process all cells in a row
  				$cells = $row->getElementsByTagName( 'Cell' );
  				foreach($cells as $cell) { 
					$ind = $cell->getAttribute('ss:Index');
					if ($ind != null) $index = $ind;
  
					// PickOrder
					if ( $index == 1 ) $PickOrder['Reference'] = $cell->nodeValue;
					if ( $index == 9 ) {
						$PickOrder['CompanyId'] = tableGetFieldWhere('Company','Id',sprintf("Number='%s' and ToCompanyId=%d",$cell->nodeValue,$PickOrder['OwnerCompanyId']));
						if ($PickOrder['CompanyId'] == 0) {
							//return sprintf('Company %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
							// Create temporary shop recordif no record found
							$Company = array (	
								'ToCompanyId'		=>  1091,	// KATVIG is Owner
								'TypeCustomer'		=>  1,	// View delivery address
								'Number'			=>  0,	// Shop Code		- C1
								'Name'			=>  "Missing shop data", // Shop Name		- C2
								'Comment'			=>  "", // Contact			- C7
								'DeliveryAddress1'	=>  "", // Adress of shop	- C6
								'DeliveryZip'		=>  "", // Zip code of shop	- C4
								'DeliveryCity'		=>  "", // City of shop		- C5
								'DeliveryCountryId' =>  0	// Country of shop	- C3
							) ;
							$Company['Number'] = $cell->nodeValue;
							$PickOrder['CompanyId'] = tableWrite ('Company', $Company, 0) ;
						}
					}
//					if ( $index == 8 ) $PickOrder['DeliveryDate'] = $cell->nodeValue;
					if ( ($index == 10) and ((int)$cell->nodeValue==2)) {
						$PickOrder['DeliveryDate'] = "2009-07-16";
						$PickOrder['Instructions'] = "Handwritten ordernumber on cartons. (Palletized together with other handwritten orders)";
					}
					if ( ($index == 11) and ((int)$cell->nodeValue==2)) {
						if ($PickOrder['DeliveryDate'] == "2009-07-16") {
							$PickOrder['DeliveryDate'] = "2009-07-18";
						} else {
							$PickOrder['DeliveryDate'] = "2009-07-17";
						}
					}
	 	 
					//PickOrderLine
					if ( $index == 2 ) {
						$PickOrderLine['VariantCodeId'] = tableGetFieldWhere('VariantCode','Id',sprintf('VariantCode="%s"',$cell->nodeValue));
						if ($PickOrderLine['VariantCodeId'] == 0) return sprintf('Variant code %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
					   	$PickOrderLine['VariantCode'] = $cell->nodeValue;
					
					}
					if ( $index == 4 ) $PickOrderLine['OrderedQuantity'] = $cell->nodeValue;
  					$index += 1;
	  			}

				$PickOrderLine['VariantDescription'] = tableGetFieldWhere('VariantCode','VariantDescription',sprintf('Id=%d',$PickOrderLine['VariantCodeId']));
				$PickOrderLine['VariantColor'] = tableGetFieldWhere('VariantCode','VariantColorDesc',sprintf('Id=%d',$PickOrderLine['VariantCodeId']));
				$PickOrderLine['VariantSize'] = tableGetFieldWhere('VariantCode','VariantSize',sprintf('Id=%d',$PickOrderLine['VariantCodeId']));

				// Update or insert into PickOrder table.
				$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
				$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
				$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
				
				// Update or insert into PickOrderLine table.
				$PickOrderLine['PickOrderId'] = $PickOrderId ;
				$whereclause = sprintf ("VariantCodeId=%d and PickOrderId=%d", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
				$PickOrderLineId=tableGetFieldWhere ('PickOrderLine', 'Id', $whereclause ) ;
				tableWrite ('PickOrderLine', $PickOrderLine, $PickOrderLineId) ;

				$row_index++;
	  	    }			
	    }
    } else return 'No file uploaded' ;

    return 0 ;
?>
