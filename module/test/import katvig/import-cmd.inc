<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;


	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 500000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if (FileUpload) {
		// Udate database

		$Company = array (	
			'ToCompanyId'		=>  1091,	// KATVIG is Owner
			'TypeCustomer'		=>  1,	// View delivery address
			'Number'			=>  0,	// Shop Code		- C1
			'Name'				=>  "", // Shop Name		- C2
			'Comment'			=>  "", // Contact			- C7
			'DeliveryAddress1'	=>  "", // Adress of shop	- C6
			'DeliveryZip'		=>  "", // Zip code of shop	- C4
			'DeliveryCity'		=>  "", // City of shop		- C5
			'DeliveryCountryId' =>  ""	// Country of shop	- C3
		) ;

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
   			$index = 1;
   			
   			// Get and process all cells in a row
  			$cells = $row->getElementsByTagName( 'Cell' );
  			foreach($cells as $cell) { 
				$ind = $cell->getAttribute('ss:Index');
				if ($ind != null) $index = $ind;
  
				if ( $index == 1 ) $Company['Number'] = $cell->nodeValue;
				if ( $index == 2 ) $Company['Name'] = utf8_decode($cell->nodeValue);
				if ( $index == 3 ) $Company['DeliveryCountryId'] = (int)$cell->nodeValue;
//tableGetFieldWhere('Country','Id',sprintf('Name="%s"',$cell->nodeValue));
				if ( $index == 4 ) $Company['DeliveryZip'] = $cell->nodeValue;
				if ( $index == 5 ) $Company['DeliveryCity'] = utf8_decode($cell->nodeValue);
				if ( $index == 6 ) $Company['DeliveryAddress1'] = utf8_decode($cell->nodeValue);
				if ( $index == 7 ) $Company['Comment'] = utf8_decode($cell->nodeValue);
 	 
  				$index += 1;
	  	    }
			// Update or insert into table.
			$whereclause = sprintf ("Number='%s' and ToCompanyId=%d and Active=1", $Company['Number'], $Company['ToCompanyId']) ;
			$CompanyId=tableGetFieldWhere ('Company', 'Id', $whereclause ) ;
			tableWrite ('Company', $Company, $CompanyId) ;
	  	    }			
	    }
    }

    return 0 ;
?>
