<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 5000000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

	$ExtColor['186'] = 7609;
	$ExtColor['230'] = 7612;
	$ExtColor['231'] = 7610;
	$ExtColor['279'] = 7824;
	$ExtColor['233'] = 7611;
	$ExtColor['232'] = 7613;
	$ExtColor['280'] = 744;
	$ExtColor['234'] = 161;
	$ExtColor['236'] = 1907;
	$ExtColor['197'] = 1333;
	$ExtColor['235'] = 1424;
	$ExtColor['237'] = 1215;


    if (FileUpload) {
		// Udate database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		$row_index=1 ;
		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
				$VariantCode = array (	
					'VariantCode'		=>  "",			// EAN code - C2
					'VariantUnit'		=>  "",			// 			- C3
					'VariantmodelRef'	=>  "",			// STYLE NOS- C4
					'VariantDescription' => "",			//			- C5
					'VariantColorDesc'		=>  "",			//			- C6
					'VariantColorCode'	=>  "",			// 			- C7
					'VariantSize'		=>  "",			//SIZE CODE	- c8
					'ArticleId'			=>	0,
					'ArticleColorId'	=>	0,
					'ArticleSizeId'		=>	0,
					'Reference'			=>	0,			// Case
					'Type'				=>	"case"
				) ;
	   			$index = 1;
   			
   				// Get and process all cells in a row
  				$cells = $row->getElementsByTagName( 'Cell' );
  				foreach($cells as $cell) { 
					$ind = $cell->getAttribute('SS:Index');
					if ($ind != null) $index = $ind;
	  
					if ( $index == 2 ) $VariantCode['VariantCode'] = $cell->nodeValue;
					if ( $index == 3 ) $VariantCode['VariantUnit'] = $cell->nodeValue;
					if ( $index == 4 ) {
						$VariantCode['VariantmodelRef'] = $cell->nodeValue;
						$VariantCode['Reference'] = tableGetFieldWhere('`Case`','Id',sprintf("CustomerReference='%s' and active=1",$cell->nodeValue));
						if ($VariantCode['Reference'] == 0) continue ; //return sprintf('STYLE REF %s in line %d dosnt exist in case', $cell->nodeValue, $row_index) ;
						$VariantCode['ArticleId'] = tableGetField('`Case`','ArticleId',$VariantCode['Reference']);
					}
					if ( $index == 5 ) $VariantCode['VariantDescription'] = $cell->nodeValue;
					if ( $index == 6 ) $VariantCode['VariantColorDesc'] = $cell->nodeValue;
					if ( $index == 7 ) {
						$VariantCode['VariantColorCode'] = $cell->nodeValue;
						$VariantCode['ArticleColorId'] = $ExtColor[$cell->nodeValue] ;
						$query=sprintf("SELECT c.articleid as Id, c.id as Reference FROM `case` c, articlecolor ac 
								   WHERE c.articleid=ac.articleid and c.Customerreference='%s' 
									and c.active=1 and ac.active=1 and ac.colorid=%d", $VariantCode['VariantmodelRef'], $ExtColor[$cell->nodeValue]) ;
						$result = dbQuery ($query) ;
						$ArticleId = dbFetch ($result) ;
						dbQueryFree ($result) ;
						if ($ArticleId>0) {
							$VariantCode['ArticleId'] = $ArticleId['Id'] ;
							$VariantCode['Reference'] = $ArticleId['Reference'] ;
						}
						$VariantCode['ArticleColorId'] = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d and active=1",$VariantCode['ArticleId'],$ExtColor[$cell->nodeValue]));
						if ($VariantCode['ArticleColorId'] == 0) {
							if ($ExtColor[$cell->nodeValue]== 7824) 
								$VariantCode['ArticleColorId'] = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d and active=1",$VariantCode['ArticleId'],7825));
							if ($VariantCode['ArticleColorId'] == 0)
								return sprintf('ColorCode %s in line %d dosnt exist for article %d', $cell->nodeValue, $row_index,$VariantCode['ArticleId'] ) ;
						}
					}
					if ( $index == 8 ) {
						$VariantCode['VariantSize'] = str_replace('-','/',$cell->nodeValue);
						$VariantCode['ArticleSizeId'] = tableGetFieldWhere('ArticleSize','Id',sprintf("ArticleId=%d and name='%s' and active=1",$VariantCode['ArticleId'],$VariantCode['VariantSize'] ));
						if ($VariantCode['ArticleSizeId'] == 0) return sprintf('Size %s in line %d dosnt exist for article (%d)', $cell->nodeValue, $row_index, $VariantCode['ArticleId']) ;
					}
	 	 
  					$index += 1;
	  			}
				// Update or insert into variant table.
//				$whereclause = sprintf ("VariantCode='%s'", $VariantCode['VariantCode']) ;
//				$VariantCodeId=tableGetFieldWhere ('VariantCode', 'Id', $whereclause ) ;
				$VariantCodeId=0;
				tableWrite ('VariantCode', $VariantCode, $VariantCodeId) ;
				
				$row_index++;
	  	    }			
	    }
    }

    return 0 ;
?>
