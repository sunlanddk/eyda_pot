<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	$Error =0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 500000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if (FileUpload) {
		// Udate database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		$row_index=1 ;
		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
	   			$index = 1;
   			
   				// Get and process all cells in a row
  				$cells = $row->getElementsByTagName( 'Cell' );
  				foreach($cells as $cell) { 
					$ind = $cell->getAttribute('ss:Index');
					if ($ind != null) $index = $ind;
	  
					if ( $index == 1 ) $txt=$cell->nodeValue ;
					if ( $index == 2 ) { // Article number
						$ArticleId = tableGetFieldWhere('Article','Id',sprintf("Number='%s'",$cell->nodeValue));
						if ($ArticleId == 0) return sprintf('Article %s in line %d dosnt exist', $cell->nodeValue, $row_index) ;
					}
					if ( $index == 4 )  { // Color
						$Color = $cell->nodeValue;
						$ColorId = tableGetFieldWhere('Color','Id',sprintf("Number='%s'",$cell->nodeValue));
						if ($ColorId == 0) return sprintf('Color %s, in line %d dosnt exist', $cell->nodeValue, $row_index) ;
						$ArticleColorId = tableGetFieldWhere('ArticleColor','Id',sprintf("ArticleId=%d and ColorId=%d",$ArticleId,$ColorId));
						if ($ArticleColorId == 0) return sprintf('ArticleColor %s, in line %d dosnt exist', $cell->nodeValue, $row_index) ;
					}
					if ( $index == 5 ) $NoSizes = (int)$cell->nodeValue;
					if ( $index == 6 ) $Street = $cell->nodeValue ;
					if ( $index == 7 ) $Numbers = explode(',',strval($cell->nodeValue));
	 	 
  					$index += 1;
	  			}
	  			
	  			$query = sprintf("select vc.id as VariantCodeId, az.* from variantcode vc, articlesize az, productionquantity pq, production p 
							where vc.articlesizeid=az.id and vc.articleColorId=%d and az.active=1 and pq.active=1 and p.active=1
								and pq.articlecolorid=vc.articlecolorid and pq.articlesizeid=vc.articlesizeid and p.id=pq.productionid and p.number>99
 							order by az.displayorder", $ArticleColorId) ;
	
				$res = dbQuery ($query) ;
				$display_val = '' ;
				$size_index=0;	
				while ($sizerow = dbFetch ($res)) {
					if ($sizerow['VariantCodeId']) {
						if (tableGetFieldWhere('VariantCode','PickContainerId',sprintf("Id=%d",$sizerow['VariantCodeId']))>0) {
							$Error=$Error . sprintf('Dupplicate found for art %d color %d, size %d txt (%s) at line %d<br>', $ArticleId, $ArticleColorId, $sizerow['Id'], $txt, $row_index) ;
						} else {
							// Calculate Position
//							$displayorder=(int)$sizerow['DisplayOrder']-1;							
							$displayorder=$size_index ;
							$seqno=(int)($displayorder/4) ;
							if ($seqno > sizeof($Numbers)-1)
								$seqno= sizeof($Numbers)-1 ;
							$No=$Numbers[$seqno] ;
							$Floor = chr(ord('a')+($displayorder-$seqno*4)) ;

//							$display_val = $display_val . sprintf('%d %d %d<br>', $displayorder,$seqno,($displayorder-$seqno*4)) ;
//							$display_val = $display_val . $sizerow['Name'] . " " .  $sizerow['DisplayOrder'] . " " . $Street . $No . $Floor . '<br>';

							// Create new Container
							$Container = array (
								'StockId' => (int)9440, // LT picking 
								'ContainerTypeId' => 39,
								'Position' => $Street . sprintf('%02d',$No) . $Floor ,
								'TaraWeight' => 0,
								'GrossWeight' => 0,
								'Volume' => 0
							) ;							
							$Container['Id'] = tableWrite ('Container', $Container) ;
							$VariantCode['PickContainerId']=$Container['Id'] ;
							tableWrite ('VariantCode', $VariantCode, $sizerow['VariantCodeId']) ;
						} 
					} else $Error=$Error . sprintf('Variant not found for art %d color %d, size %d txt (%s) at line %d<br>', $ArticleId, $ArticleColorId, $sizerow['Id'], $txt, $row_index) ;
					$size_index++ ;
				}
// return $display_val ;
				$row_index++;
	  	    }			
	    }
    }

    return $Error ;
?>
