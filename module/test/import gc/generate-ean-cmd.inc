<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	if (strlen($Record['EksternVariantCode'])==0){
		$query = sprintf("SELECT max(variantcode) as VariantCode FROM variantcode@) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree($res) ;
	
		$BaseVariantCode = 0 + substr($row['VariantCode'],0,12) ;
	} else {
		$BaseVariantCode = $Record['EksternVariantCode'] ;		// 
	}

	query = "
		SELECT c.name as Collection, colm.caseid as `Case`, a.Id as ArticleId, ac.Id as ArticleColorId, az.Id as ArticleSizeId
		FROM (season s, collection c, collectionmember colm, article a)
		LEFT JOIN articlecolor ac on colm.ArticleColorId=ac.id and ac.active=1
		LEFT JOIN color co on co.id=ac.colorid and co.active=1
		LEFT JOIN articlesize az on az.articleid=a.id and az.active=1
		WHERE s.id=[SeasonId] and c.seasonid=s.id and colm.collectionid=c.id and colm.articleid=a.id
			and s.active=1 and c.active=1 and colm.active=1 and a.active=1 " ;

	$res = dbQuery ($query) ;
	
	$row_index=1 ;
	while ($row = dbFetch ($res)) {
		$VariantCode = array (	
			'VariantCode'		=>  "",			// EAN code
			'VariantUnit'		=>  "pcs",			// 			
			'VariantmodelRef'		=>  "",			
			'VariantDescription' 	=> "",			
			'VariantColorDesc'	=>  "",		
			'VariantColorCode'	=>  "",			
			'VariantSize'		=>  "",			
			'ArticleId'			=>	0,			// 
			'ArticleColorId'		=>	0,			// 
			'ArticleSizeId'		=>	0,			// 
			'Reference'			=>	0,			// None
			'Type'			=>	"case"
		) ;
 		
		$barcode = sprintf("%d", ($BaseVariantCode + $row_index)) ;
		$barcode.=GetCheckDigit($barcode); 			// Add control digit

		$VariantCode['VariantCode'] = $barcode;
		$VariantCode['ArticleId'] = (int)$row['ArticleId'];
		$VariantCode['ArticleColorId'] = (int)$row['ArticleColorId'];
		$VariantCode['ArticleSizeId'] = (int)$row['ArticleSizeId'];
	 			
		// Update or insert into VariantCode table.
		$whereclause = sprintf ("VariantCode='%s'", $VariantCode['VariantCode']) ;
		$VariantCodeId=tableGetFieldWhere ('VariantCode', 'Id', $whereclause ) ;
		tableWrite ('VariantCode', $VariantCode, $VariantCodeId) ;
				
		$row_index++;
    }
    dbQueryFree ($res) ;	
    return 0 ;
?>
