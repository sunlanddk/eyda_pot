<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	$query = "
SELECT c.name as Collection, colm.caseid as `Case`, a.number as Article, a.description as ArtDesc, co.description as Color, co.number as ColorNumber
FROM (season s, collection c, collectionmember colm, article a)
LEFT JOIN articlecolor ac on colm.ArticleColorId=ac.id and ac.active=1
LEFT JOIN color co on co.id=ac.colorid and co.active=1
LEFT JOIN articlesize az on az.articleid=a.id and az.active=1
WHERE s.id=[SeasonId] and c.seasonid=s.id and colm.collectionid=c.id and colm.articleid=a.id
	and s.active=1 and c.active=1 and colm.active=1 and a.active=1
	";

	$res = dbQuery ($query) ;
	
	while ($row = dbFetch ($res)) {
  		// Initialize
		$PickOrder = array (	
			'Type'				=>  "SalesOrder",	// Pick order from internal customer
			'Reference'			=>  0,			// Order number 
			'FromId'			=> 2059,		// GC ss09 Picking stock
			'CompanyId'			=>  0,			// Shop Code	
			'OwnerCompanyId'	=>  787,		// NW GC
			'HandlerCompanyId'	=>  2,		// Novotex
			'Packed'				=>  0,
			'DeliveryDate'		=>  ""			// Expected pick and pack complete
		) ;

		$PickOrderLine = array (	
			'VariantCodeId'		=>  "",			//
			'VariantCode'		=>  "",			// EAN code 
			'OrderedQuantity'	=>  0,			//			
			'PickedQuantity'	=>  0,			//
			'PackedQuantity'	=>  0,			//
			'PickOrderId'		=>	0,
			'VariantDescription'		=>	"",
			'VariantColor'		=>	"",
			'VariantSize'		=>	"",
			'Done'				=>  0
		) ;
	
		// Generate PickOrders
		$PickOrder['Reference'] = $row['OrderId'] ;
		$PickOrder['ReferenceId'] = $row['OrderId'] ;
		$PickOrder['CompanyId'] = $row['CompanyId'] ;
		$PickOrder['DeliveryDate'] = $row['DeliveryDate'] ;
		
		$PickOrderLine['VariantCode'] = $row['VariantCode'] ;
		$PickOrderLine['VariantCodeId'] = $row['VariantCodeId'] ;
		$PickOrderLine['OrderedQuantity'] = $row['quantity'] ;
		$PickOrderLine['VariantDescription'] = $row['ArticleDescription'] ;
		$PickOrderLine['VariantColor'] = $row['color'] ;
		$PickOrderLine['VariantSize'] = $row['size'] ;

		// Update or insert into PickOrder table.
		$whereclause = sprintf ("Reference='%s' and OwnerCompanyId=%d", $PickOrder['Reference'], $PickOrder['OwnerCompanyId']) ;
		$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', $whereclause ) ;
		$PickOrderId=tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;
		
		// Update or insert into PickOrderLine table.
		$PickOrderLine['PickOrderId'] = $PickOrderId ;
		$whereclause = sprintf ("VariantCodeId=%d and PickOrderId=%d", $PickOrderLine['VariantCodeId'], $PickOrderId) ;
		$PickOrderLineId=tableGetFieldWhere ('PickOrderLine', 'Id', $whereclause ) ;
		tableWrite ('PickOrderLine', $PickOrderLine, $PickOrderLineId) ;
    }
    dbQueryFree ($res) ;
    return 0 ;
?>
