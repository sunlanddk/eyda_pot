<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 800000)
		    return 'file too big, max 800 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if (FileUpload) {
		// Udate database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		$row_index=1 ;
		foreach ($rows as $row) {
		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
				$VariantCode = array (	
					'VariantCode'		=>  "",			// EAN code - C2
					'VariantUnit'		=>  "pcs",			// 			
					'VariantmodelRef'	=>  "",			
					'VariantDescription' => "",			
					'VariantColorDesc'		=>  "",		
					'VariantColorCode'	=>  "",			
					'VariantSize'		=>  "",			
					'ArticleId'			=>	0,			// C3
					'ArticleColorId'	=>	0,			// C4
					'ArticleSizeId'		=>	0,			// C5
					'Reference'			=>	0,			// None
					'Type'				=>	"case"
				) ;
	   			$index = 1;
   			
   				// Get and process all cells in a row
  				$cells = $row->getElementsByTagName( 'Cell' );
  				foreach($cells as $cell) { 
					$ind = $cell->getAttribute('Index');
					if ($ind != null) $index = $ind;
	  
					if ( $index == 2 ) $VariantCode['VariantCode'] = $cell->nodeValue;
					if ( $index == 3 ) $VariantCode['ArticleId'] = (int)$cell->nodeValue;
					if ( $index == 4 ) $VariantCode['ArticleColorId'] = (int)$cell->nodeValue;
					if ( $index == 5 ) $VariantCode['ArticleSizeId'] = (int)$cell->nodeValue;
	 	 
  					$index += 1;
	  			}
	  			
				// Update or insert into PickOrder table.
				$whereclause = sprintf ("VariantCode='%s'", $VariantCode['VariantCode']) ;
				$VariantCodeId=tableGetFieldWhere ('VariantCode', 'Id', $whereclause ) ;
				tableWrite ('VariantCode', $VariantCode, $VariantCodeId) ;
				
				$row_index++;
	  	    }			
	    }
    }

    return 0 ;
?>
