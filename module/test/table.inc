<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Get abailable tables
    $TableNames = array() ;
    $query = sprintf ('SHOW TABLES') ;
    $r = dbQuery ($query) ;
    while ($row = dbFetch ($r)) {
       	$TableNames[] = current($row) ;
    }
    dbQueryFree ($r) ;
    if (count($TableNames) == 0) return 'no Tables' ;

    $Table = $_GET['Table'] ;
    
    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemFieldRaw ('Table', formSelect ('Table', $Table, $TableNames, 'width:250px;', NULL, sprintf('onchange="appLoad(%d,0,\'Table=\'+document.appform.Table.options[document.appform.Table.selectedIndex].text);"', (int)$Navigation['Id']))) ; 
    itemSpace () ;
    itemEnd () ;
    formEnd () ;

    if ($Table == '') return 0 ;
 
    listStart () ;
    listRow () ;
    listHead ('Name', 150) ;
    listHead ('Type') ;
    $query = sprintf ('SHOW COLUMNS FROM `%s`', $Table) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {
	listRow () ;
	listField ($row['Field']) ;
	listField ($row['Type']) ;
    }
    dbQueryFree ($result) ;
    listEnd ;
    
    return 0 ;
?>
