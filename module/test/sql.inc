<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;

    // Get SQL query
    $query = stripslashes($_GET['query']) ;
    
    // Form
    printf ("<form method=GET name=appform>\n") ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Query</td><td><textarea name=query style='width:100%%;height:150px;'>%s</textarea></td></tr>\n", $query) ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Result</td><td class=itemfield style=\"padding-right:8px;\">") ;

    $query = stripslashes ($query) ;
    
    if ($query == '') {
	// No query
	printf ("No Query") ;
    } else {
	// Do query
 	$res = dbQuery ($query) ;
	if ($res === TRUE) {
	    printf ("Executed, %d rows affected", dbNumRows ($res)) ;
	} else {
	    printf ("%d rows returned", dbNumRows($res)) ;
	}
    }

    printf ("</td></tr>\n") ;
    print htmlItemSpace () ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
      
    if ($query == '' or $res === true) return 0 ;

    if (dbNumRows($res) > 0) {
	require_once 'lib/list.inc' ;
        listStart () ;
	$n = 0 ;
	while ($row = dbFetch($res)) {
	    if ($n == 0) {
		// Make header
	    	listRow () ;
		foreach ($row as $name => $value) {
		    listHead (htmlentities($name)) ;
		}
	    }
	    listRow () ;
	    foreach ($row as $value) {
		listField ($value) ;
	    }
	    $n++ ;
	}
	listEnd () ;
    }

    dbQueryFree ($res) ;

    return 0 ;
?>
