<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	
	function process_file($_handle) {
		$debug = 0; 
		$row = 1 ; 
		while (($data = fgetcsv($_handle, 10000, ";")) !== FALSE) {
			$_cols = (int)count($data) ;
			if ($row == 1) {
				$_headers = $data ;
//return 'her 1 ' . $_cols . ' - ' . $data[0] . ' - ' . $data[1];
			} else {
				foreach ($data as $key => $value) {
//return 'her ' . $_cols . ' - ' . $key . ' - ' . $value . ' - ' . $_headers[$key];
					$_record[$_headers[$key]] = mb_convert_encoding($value, 'ISO-8859-1', 'UTF-8') ;
					$_rows .=  $value . ',' ;
				}
				tableWrite($_POST['Table'], $_record) ;
				$_rows .= '<br>' ;
			}
			$row++ ;
		}	
		return ($row - 1) . ' DataRows from File succefully imported into ' . $_POST['Table'] . '<br>' . $_rows ;
	}

	$error_txt = 0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 500000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

		if ($FileUpload) {
			if (($handle = fopen($_FILES['Doc']['tmp_name'], "r")) !== FALSE) {
				$error_txt = process_file($handle) ;
				fclose($handle);
			} else {
				$error_txt = 'Cant open file ' ;
			}
		} else {
			$error_txt = 'No file specified ' . $_FILES['Doc']['tmp_name'] . ' size ' . $_FILES['Doc']['size'] . ' error ' . $_FILES['Doc']['error'];
		}

    return $error_txt ;
?>


