<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;



	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

    if (FileUpload) {
		// Udate database

		$dom = DOMDocument::load( $_FILES['Doc']['tmp_name'] );
		
		// Get and process all rows!
		$rows = $dom->getElementsByTagName( 'Row' );
		$first_row = true;
		foreach ($rows as $row) {
		$Company = array (	
			'ToCompanyId'	=>  1430,	// NW Drappa Dot is Owner
			'TypeCustomer'	=>  1,		// Customer
			'Number'		=>  0,		// Shop Code	- C1
			'Name'			=>  "",		// Shop Name	- C2
			'Address1'		=>  "", // Adress of shop	- C3
			'Zip'			=>  "", // Zip code of shop	- C6
			'City'			=>  "", // City of shop		- C7
			'CountryId'		=>  0,	// Country of shop	- C4
			'DeliveryTermId' =>  9,	// EXWorx
			'VATNumber'		=>  "", // VAT code		- C8
			'RegNumber'		=>  "", // DK reg nr	- C9
			'PhoneMain'		=>  "", // Phone		- C10
			'PaymentTermId' =>  0,	// Payment term	- C11
			'Mail'			=>  "", // Email adress - C12
			'CurrencyId'	=>  0,	// cURRENCY		- C13
			'InvoiceHeader'  =>  1,
			'Comment'			=>  "" // Contact	- C16
		) ;

		  	if ($first_row) {
		  		 $first_row = false;
		  	} else {
   			$index = 1;
   			
   			// Get and process all cells in a row
  			$cells = $row->getElementsByTagName( 'Cell' );
  			foreach($cells as $cell) { 
				$ind = $cell->getAttribute('ss:Index');
				if ($ind != null) { $index = (int)$ind; }
  
				if ( $index == 1 ) $Company['Number'] = (int)$cell->nodeValue + 600000;
				if ( $index == 2 ) $Company['Name'] = utf8_decode($cell->nodeValue);
				if ( $index == 3 ) $Company['Address1'] = utf8_decode($cell->nodeValue);
				if ( $index == 4 ) $Company['CountryId'] = tableGetFieldWhere('Country','Id',sprintf('Name="%s"',$cell->nodeValue));
				if ( $index == 6 ) $Company['Zip'] = $cell->nodeValue;
				if ( $index == 7 ) $Company['City'] = utf8_decode($cell->nodeValue);
				if ( $index == 8 ) $Company['VATNumber'] = $cell->nodeValue;
				if ( $index == 9 ) $Company['RegNumber'] = $cell->nodeValue;
				if ( $index == 10 ) $Company['PhoneMain'] = $cell->nodeValue;
				if ( $index == 11 ) $Company['PaymentTermId'] = (int)$cell->nodeValue;
				if ( $index == 12 ) $Company['Mail'] = $cell->nodeValue;
				if ( $index == 13 ) $Company['CurrencyId'] = tableGetFieldWhere('Currency','Id',sprintf('Name="%s"',$cell->nodeValue));
				if ( $index == 16 ) $Company['Comment'] = utf8_decode($cell->nodeValue);
 	 
  				$index += 1;
	  	    }
			// Update or insert into table.
			$whereclause = sprintf ("Number='%s' and ToCompanyId=%d and Active=1", $Company['Number'], $Company['ToCompanyId']) ;
			$CompanyId=tableGetFieldWhere ('Company', 'Id', $whereclause ) ;
			tableWrite ('Company', $Company, $CompanyId) ;
	  	    }			
	    }
    }

    return 0 ;
?>
