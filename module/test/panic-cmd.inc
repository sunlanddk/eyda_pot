<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;

	$query = "
SELECT o.id as OrderId, o.companyid as CompanyId, o.Instructions as Instructions,
				ol.Description as ArticleDescription, ol.deliverydate as DeliveryDate, ol.articleid as articleid, ol.articlecolorid as articlecolorid,
				oq.articlesizeid, oq.quantity as quantity,
				c.description as color, az.name as size
		FROM (`order` o, orderline ol, orderquantity oq, articlecolor ac, color c, articlesize az)
    left join variantcode vc on ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid and vc.active=1
		WHERE ol.orderid=o.id and oq.orderlineid=ol.id and o.active=1 and ol.active=1 and ol.done=0 and oq.active=1
		and o.done=0 and o.ready=1
		and ol.articlecolorid=ac.id and  ac.colorid=c.id and oq.articlesizeid=az.id
		and o.tocompanyid=787 and o.seasonid=1 and isnull(vc.id)
  group by ol.articleid, ol.articlecolorid
		ORDER BY o.id	";

	$res = dbQuery ($query) ;
	
	while ($row = dbFetch ($res)) {
			$CollectionMember['ArticleId']=(int)$row['articleid'] ;

			$CollectionMember['ArticleColorId'] = (int)$row['articlecolorid'] ;

			$query = sprintf ("select max(id) as Id from `case` c where c.articleid=%d group by c.articleid", $CollectionMember['ArticleId']) ;
			$result = dbQuery ($query) ;
			$caserow = dbFetch ($result) ;
	    		dbQueryFree ($result) ;   	
			$CollectionMember['CaseId']=(int)$caserow['Id'] ;

			$CollectionMember['CollectionId']=11 ;
			tableWrite ('CollectionMember', $CollectionMember) ;
	} 

    return 0 ;    
?>
