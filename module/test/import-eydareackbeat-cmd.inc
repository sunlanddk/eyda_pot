<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'lib/variant.inc' ;
	require_once 'lib/parameter.inc' ;
	
	function process_file($_handle) {
		$_varbasecode = ParameterGet('BaseSKU787') ;
		$debug = 0; 
		$row = 1 ; 
		while (($data = fgetcsv($_handle, 10000, ";")) !== FALSE) {
			$_cols = (int)count($data) ;
			if ($row == 1) {
				$_headers = $data ; //return 'her 1 ' . $_cols . ' - ' . $data[0] . ' - ' . $data[1];
			} else {
				foreach ($data as $key => $value) {
					//return 'her ' . $_cols . ' - ' . $key . ' - ' . $value . ' - ' . $_headers[$key];
					$_record[$row][$_headers[$key]] = mb_convert_encoding($value, 'ISO-8859-1', 'UTF-8') ;
					$_rows .=  $value . ',' ;
				}
				//tableWrite($_POST['Table'], $_record) ;
				$_rows .= '<br>' ;
			}
			$row++ ;
		}
		foreach ($_record as $key => $_row) {
			$_name = explode('-', $_row['Navn']) ;

			$_articledescription = trim($_name[0]) ;
			$_article[$_articledescription]['Description'] = $_articledescription ;
			$_article[$_articledescription]['Description']['DK'] =  $_row['Beskrivelse'] ; //	LangId 4
//			$_article[$_articledescription]['Number'] = $_row['Nummer'] ;

			$_variants = explode('/', $_name[1]) ;

			$_colordescription = trim($_variants[0]) ;
			$_article[$_articledescription]['Color'][$_colordescription]['Description'] = $_colordescription ;

			$_sizedescription = trim($_variants[1]) ;
			$_article[$_articledescription]['Sizes'][$_sizedescription]['Description'] = $_sizedescription ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['Weight'] = $_row['Vægt'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['Reference'] = $_row['Nummer'] ;
			
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['RetailPrice'][1] = $_row['Salgspris'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['PurchasePrice'][1] = $_row['Kostpris'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['RetailPrice'][2] = $_row['Salgspris (EUR)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['PurchasePrice'][2] = $_row['kostpris (EUR)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['RetailPrice'][3] = $_row['Salgspris (GBP)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['PurchasePrice'][3] = $_row['kostpris (GBP)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['RetailPrice'][4] = $_row['Salgspris (NOK)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['PurchasePrice'][4] = $_row['kostpris (NOK)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['RetailPrice'][5] = $_row['Salgspris (SEK)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['PurchasePrice'][5] = $_row['kostpris (SEK)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['RetailPrice'][10] = $_row['Salgspris (USD)'] ;
			$_article[$_articledescription]['Color'][$_colordescription]['Sizes'][$_sizedescription]['PurchasePrice'][10] = $_row['kostpris (USD)'] ;
			
		}
		$_no = (int)tableGetFieldwhere('article','Max(Number)','active=1 and articletypeid=1') ;
		$_colno = (int)tableGetFieldwhere('color','Max(Number)','active=1') ;
		$_no = 1000 ;
		$_colno = 100 ;
		foreach ($_article as $_artdesc => $_artrow) {
			$_articleid = (int)tableGetFieldWhere('article','Id','active=1 and Description="'.$_artdesc.'"');
			if ($_articleid==0) {
				$_no++ ;
				$Article = array (
					'Number' 			=>	$_no,
					'Description' 		=>	$_artrow['Description'],
					'ArticleTypeId' 	=>	1, //(int)$ArticleType['Id'],
					'UnitId' 			=>	3, // Pcs
					'VariantColor' 		=>	1,
					'VariantSize' 		=>	1,
					'VariantSortation' 	=>	1,
					'VariantCertificate'=>  0,
					'FabricArticleId' 	=>	(int)0,
				) ;
				$_articleid = tableWrite('article', $Article) ;
			}
			
			// Sizes
			$_displayorder=0 ;
			foreach ($_artrow['Sizes'] as $_sizedesc => $_sizerow) {
				$_displayorder++ ;
				$ArticleSize = array (
					'ArticleId' =>	$_articleid,
					'Name' 		=>	$_sizedesc,
					'DisplayOrder' => $_displayorder
				) ;
				$_articlesizeid=tableWrite('articlesize', $ArticleSize) ;
				$_article[$_artdesc]['Sizes'][$_sizedesc]['ArticleSizeId'] = $_articlesizeid ;
			}
			
			// Colors, variants and collectionmembers
			foreach ($_artrow['Color'] as $_coldesc => $_colrow) {
				// Color
				$_colorid = (int)tableGetFieldWhere('color','Id','active=1 and Description="'.$_coldesc.'"');
				if ($_colorid==0) {
					$_colno++ ;
					$Color = array (
						'Number' 			=>	$_colno,
						'Description' 		=>	$_colrow['Description']
					) ;
					$_colorid=tableWrite('color', $Color) ;
				}
				$ArticleColor = array (
					'ArticleId' 	=>	$_articleid,
					'ColorId' 		=>	$_colorid
				) ;
				$_artcolorid=tableWrite('articlecolor', $ArticleColor) ;
				
				$CollectionMember = array (
					'CollectionId' 	 =>	1,
					'CollectionLOTId'=>	1,
					'ArticleId' 	 =>	$_articleid,
					'ArticleColorId' =>	$_artcolorid
				) ;
				$_collectionmemberid=tableWrite('collectionmember', $CollectionMember) ;
				
				$_currencyids = array(0,1,2,3,4,10) ;
				foreach ($_currencyids as $_currencyid) {
					$CollectionMemberPrice = array (
						'CollectionMemberId'=>	$_collectionmemberid,
						'CurrencyId' 		=>	$_currencyid,
						'RetailPrice' 	 	=>	0
					) ;
					$_collectionmemberpriceid=tableWrite('collectionmemberprice', $CollectionMemberPrice) ;
					foreach ($_artrow['Sizes'] as $_sizedesc => $_sizerow) {
						$_sizecurrencyids = array(1,2,3,4,10) ;
						foreach ($_sizecurrencyids as $_currencyid) {
							$CollectionMemberPriceSize = array (
								'CollectionMemberPriceId'=>	$_collectionmemberpriceid,
								'ArticleSizeId'			=>	$_article[$_artdesc]['Sizes'][$_sizedesc]['ArticleSizeId'],
								'RetailPrice' 	 		=>	(float)str_replace(',','.',$_article[$_artdesc]['Color'][$_coldesc]['Sizes'][$_sizedesc]['RetailPrice'][$_currencyid]),
								'PurchasePrice' 	 	=>	(float)str_replace(',','.',$_article[$_artdesc]['Color'][$_coldesc]['Sizes'][$_sizedesc]['PurchasePrice'][$_currencyid]),
							) ;
							$_collectionmemberpricesize=tableWrite('collectionmemberpricesize', $CollectionMemberPriceSize) ;
						}
					}
				}

				foreach ($_artrow['Sizes'] as $_sizedesc => $_sizerow) {
					$_variantcode = GetNextVariantCode($_varbasecode) ;
					$Variant = array (
						'ArticleId' 		=>	$_articleid,
						'ArticleColorId'	=>	$_artcolorid,
						'ArticleSizeId' 	=>	$_article[$_artdesc]['Sizes'][$_sizedesc]['ArticleSizeId'],
						'VariantCode'		=>  $_variantcode,
						'Reference' 		=>	$_article[$_artdesc]['Color'][$_coldesc]['Sizes'][$_sizedesc]['Reference']
					) ;
					$_variantcodeid=tableWrite('variantcode', $Variant) ;
				}
			}	
		}
		return ($row - 1) . ' DataRows from File succefully imported into ' . $_POST['Table'] . '<br>' . $_rows ;
	}

	$error_txt = 0 ;
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return ' upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 500000)
		    return 'file too big, max 500 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}

		if ($FileUpload) {
			if (($handle = fopen($_FILES['Doc']['tmp_name'], "r")) !== FALSE) {
				$error_txt = process_file($handle) ;
				fclose($handle);
			} else {
				$error_txt = 'Cant open file ' ;
			}
		} else {
			$error_txt = 'No file specified ' . $_FILES['Doc']['tmp_name'] . ' size ' . $_FILES['Doc']['size'] . ' error ' . $_FILES['Doc']['error'];
		}

    return $error_txt ;
?>


