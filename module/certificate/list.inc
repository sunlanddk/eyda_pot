<?php

    require_once 'lib/list.inc' ;

    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Name', 120) ;
    listHead ('Description') ;
    listHead ('Supplier') ;
    listHead ('Holder') ;
    listHead ('Type', 120) ;
    listHead ('Valid Until', 90, 'align=right') ;
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'editcert', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['SupplierName']) ;
	listField ($row['HolderName']) ;
	listField ($row['CertificateTypeName']) ;
	listField (date ("Y-m-d", dbDateDecode($row['ValidUntil'])), 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Certificates', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
