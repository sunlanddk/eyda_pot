<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/form.inc' ;
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    unset ($Record) ;
	    $Record['ValidUntil'] = dbDateEncode(time()+6*30*24*60*60) ;
	    break ;
    }
 
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Name</td><td><input type=text name=Name maxlength=20 size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%;' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Type</td><td>%s</td></tr>\n", htmlDBSelect ('CertificateTypeId style="width:150px;"', $Record['CertificateTypeId'], 'SELECT Id, Name AS Value FROM CertificateType WHERE Active=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Supplier</td><td>%s</td></tr>\n", htmlDBSelect ('SupplierCompanyId style="width:200px;"', $Record['SupplierCompanyId'], 'SELECT Id, CONCAT(Company.Name," (",Company.Number,")") AS Value FROM Company WHERE TypeSupplier=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Holder</td><td>%s</td></tr>\n", htmlDBSelect ('HolderCompanyId style="width:200px;"', $Record['HolderCompanyId'], 'SELECT Id, CONCAT(Company.Name," (",Company.Number,")") AS Value FROM Company WHERE TypeSupplier=1 AND Active=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
//    printf ("<tr><td class=itemfield>Valid Until</td><td><input type=text id=IdValidUntil name=ValidUntil size=10 maxlength=10 value=\"%s\" style='width:68px' readonly=1><img src='image/date.gif' id=IdValidUntilButton style='vertical-align:bottom;margin-left:0px;cursor: pointer;'></td></tr>\n", date("Y-m-d",dbDateDecode($Record['ValidUntil']))) ;
    printf ("<tr><td class=itemfield>Valid Until</td><td>%s</td></tr>\n", formDate ('ValidUntil', dbDateDecode($Record['ValidUntil']), null, 'tR')) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;
    
    return 0 ;
?>
