<?php
// TJ 2021-04-24
//die("load") ;
    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
		case 'list' :
		    // List field specification
		    $fields = array (
				NULL, // index 0 reserved
				array ('name' 	=> 'Name',			'db' => 'ClusterType.Name'),
				array ('name' 	=> 'DisplayOrder',  'db' => 'ClusterType.`DisplayOrder`', 'nofilter' => true),
				array ('name' 	=> 'ClusterGroup',  'db' => 'ClusterGroup.`Name`'),
		    ) ;
		    $queryFields = 'ClusterType.*, ClusterGroup.Name as ClusterGroupName' ;
		    $queryTables = 'ClusterType LEFT JOIN ClusterGroup ON ClusterGroup.Id=ClusterType.ClusterGroupId' ;
		    $queryClause = 'ClusterType.Active=1' ;	    

		    require_once 'lib/list.inc' ;
		    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
		    if (is_string($Result)) return $Result ;
		    //die("22 load") ;
		    return listView ($Result, 'clusterview') ;
		    
		case 'view' :
		    // Get ClusterType
		   	$query = sprintf ("SELECT ClusterType.*, ClusterGroup.Name as ClusterGroupName 
		   						FROM ClusterType 
		   						LEFT JOIN ClusterGroup ON ClusterGroup.Id=ClusterType.ClusterGroupId
		   						WHERE ClusterType.Active=1 AND ClusterType.Id=%d", $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		 	break ;

		    // Get Cluster Value
		   	$query = sprintf ("SELECT Cluster.*, ClusterType.Name as ClusterTypeName 
		   						FROM Cluster 
		   						LEFT JOIN ClusterType ON ClusterType.Id=Cluster.TypeId
		   						WHERE Cluster.Active=1 AND Cluster.Id=%d", $Id) ;
		    //die("47 query=".$query );
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    break ;

		default:
			break ;
    }
    
    return 0 ;
    
?>
