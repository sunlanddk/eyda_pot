<?php
// TJ 2021-04-23
//die("view cluster") ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ; // Insert by Ken

    itemStart() ;
    
    itemField ('Name', $Record['Name']) ;
    itemField ('DisplayOrder', $Record['DisplayOrder']) ;
    itemField ('ClusterGroup', $Record['ClusterGroupName']) ; // Insert by Ken
    itemSpace() ;
	itemInfo($Record) ;
	itemSpace() ;
    itemEnd () ;

    // List	
    listStart() ;
    listHeader("Cluster Values") ;
    listRow () ;
    listHeadIcon () ;
    listHead("Name",220) ;
    listHead("Description") ;
	listHead ('', 800) ;
	
    $query = sprintf ("SELECT * FROM Cluster WHERE Active=1 AND TypeId=%d", $Id) ;
    $res = dbQuery ($query) ;
	
    while ($row = dbFetch ($res)) {
        listRow() ;         //new line
        listFieldIcon ($Navigation['Icon'], 'clusterval', $row['Id']) ;
        listField($row['Name']) ;
        listField($row['Description']) ;
    }
    dbQueryFree ($res) ;

    listEnd();
    return 0 ;
?>
