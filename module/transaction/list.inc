<?php
// TJ 2021-04-24
//die("list") ;
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Name', 120) ;
    listHead ('DisplayOrder', 120) ;
    listHead ('ClusterGroup') ;
    listHead ('', 800) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
    	listFieldIcon ($Navigation['Icon'], 'clusterview', $row['Id']) ;
		listField ($row['Name']) ;
    	listField ($row['DisplayOrder']) ;
        listField ($row['ClusterGroupName']) ;
    }
    if (dbNumRows($Result) == 0) {
    	listRow () ;
    	listField () ;
    	listField ('Cluster not found', 'colspan=3') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
    return 0 ;
?>
