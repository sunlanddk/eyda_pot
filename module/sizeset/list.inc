<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 135) ;
    listHead ('Description') ;
    listHead ('Sizes', 60, 'align=right') ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'sizesetview', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Description']) ;
	listField ($row['Sizes'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No SizeSets', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
