<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Name</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name') ;
    listHead ('DisplayOrder', 100, 'align=right') ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'sizeedit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['DisplayOrder'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Sizes', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
