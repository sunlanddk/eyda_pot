<?php

    require_once 'lib/log.inc' ;
    require_once 'lib/table.inc' ;
    
    $Line = array () ;
    $Operation = array () ;
    $OperationUsed = array () ;
    $ErrorText = 'while processing the operations, the following errors has been found:<br><br>' ;
    $ErrorNote = '' ;
    $ProductionMinutes = 0 ;
    $SequenseChange = false ;
    $Changed = false ;

    // Read and verify all posted data
    for ($n = 1 ; $n <= 99 ; $n++) {
	// Is No of sewing operation valid
	$value = (int)$_POST['No'][$n] ;
	if ($n != $value) return sprintf ('%s(%d) invalid No for row of operation, value %d, expected %d', __FILE__, __LINE__, $value, $n) ;
	
	// Get and lookup Operation Number
	$value = trim(stripslashes($_POST['Operation'][$n])) ;
	if (trim($value) == '') continue ;
	$query = sprintf ('SELECT MachineGroup.Id AS MachineGroupId, Operation.Id AS OperationId FROM MachineGroup, Operation WHERE MachineGroup.Active=1 AND Operation.Active=1 AND Operation.MachineGroupId=MachineGroup.Id AND CONCAT(MachineGroup.Number, Operation.Number)="%s"', addslashes($value)) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if (!$row) $ErrorNote .= sprintf ("Line %d: Operation %s not found<br>", $n, $value) ;

	// Has Operation allerady been used in Style
	if ($OperationLine[(int)$row['OperationId']]) {
	    $ErrorNote .= sprintf ("Line %d: Operation %s has allready been used in line %d<br>", $n, $value, $OperationLine[(int)$row['OperationId']]) ;
	    continue ;
	}

	// Save Operation information
	$OperationLine[(int)$row['OperationId']] = $n ;
	$Line[$n]['OperationId'] = (int)$row['OperationId'] ;
	$Line[$n]['MachineGroupId'] = (int)$row['MachineGroupId'] ;
	
	// Get Description
	$value = trim(stripslashes($_POST['Description'][$n])) ;
	$Line[$n]['Description'] = $value ;

	// Get Bundele Text
	$value = trim(stripslashes($_POST['BundleText'][$n])) ;
	$Line[$n]['BundleText'] = $value ;
	
	// Get ProductionMinutes
	$value = str_replace(',','.',trim(stripslashes($_POST['Minutes'][$n]))) ;
	$value = (float)$value ;
	if ($value < 0.05 or $value > 99) $ErrorNote .= sprintf ("Line %d: Invalid ProductionMinutes<br>", $n) ;
	$Line[$n]['ProductionMinutes'] = $value ;
	$ProductionMinutes += $value ;
    }

    // Validate against all ProductionOrders using the Style and not beeing done
    $query = sprintf ("SELECT Production.Number, Production.CaseId, ProductionMinutes FROM Production INNER JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId AND ProductionLocation.TypeSample=0 WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Production.ProductionMinutes < %s", (int)$Record['Id'], $ProductionMinutes) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {		    
	$ErrorNote .= sprintf ("ProductionMinutes for %d/%d has been exceded by %01.2f minutes<br>", (int)$row['CaseId'], (int)$row['Number'], $ProductionMinutes-$row['ProductionMinutes']) ;
    }
    dbQueryFree ($result) ;

    // Validate if operations has consecurative Numbers
    foreach ($Line as $n => $line) {
	if ($n <= 1) continue ;
	if (!isset($Line[$n-1])) {
    	    $ErrorNote .= sprintf ('Operations must have consecutive No (no gabs), n %d<br>', $n) ;
	    break ;
	}
    }

    // Read existing Operations and check if sequense has been changed
    $n = 1 ;
    while ($row = dbFetch ($Result)) {
	$Operation[$n] = $row ;
	if ((int)$row['OperationId'] != $Line[$n]['OperationId']) $SequenseChange = true ;
	$n++ ;
    }
    dbQueryFree ($Result) ;

    // Changing Operation Line No is only allowed when no bundles has been made for ProductionOrders not Done
    if ($SequenseChange) {
	$query = sprintf ("SELECT Production.Number, Production.CaseId, COUNT(Bundle.Id) FROM Production, Bundle WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Bundle.ProductionId=Production.Id AND Bundle.Active=1 GROUP BY Production.Id", $Record['Id']) ;
	$result = dbQuery ($query) ;
	$s = '' ;
	while ($row = dbFetch ($result)) {
	    if ($s != '') $s .= ', ' ;
	    $s .= $row['CaseId'] . '/' . $row['Number'] ;
	}
	dbQueryFree ($result) ;
	if ($s != '') $ErrorNote .= sprintf ('Sequense of operations can not be changed because bundles has been made for ProductionOrders (%s)<br>', $s) ;
    }

//logPrintVar ($SequenseChange, 'SequenseChange') ;
//logPrintVar ($Line, 'Line') ;
//logPrintVar ($Operation, 'Operation') ;
//return sprintf ('%s(%d) STOP', __FILE__, __LINE__) ;    

    // Errors so fare ?
    if ($ErrorNote != '') return $ErrorText . $ErrorNote ;

    // Opdate the records made
    foreach ($Line as $n => $line) {
	if (isset($Operation[$n])) {
	    // Update existing record
	    $operation = &$Operation[$n] ;
	    if ($n != (int)$operation['No']) $line['No'] = $n ;
	    if ($line['OperationId'] == (int)$operation['OperationId']) unset ($line['OperationId']) ;
	    if ($line['MachineGroupId'] == (int)$operation['MachineGroupId']) unset ($line['MachineGroupId']) ;
	    if ($line['Description'] == $operation['Description']) unset ($line['Description']) ;	    
	    if ($line['BundleText'] == $operation['BundleText']) unset ($line['BundleText']) ;
	    if ($line['ProductionMinutes'] == (float)$operation['ProductionMinutes']) unset ($line['ProductionMinutes']) ;

	    if (count($line) > 0) {
		tableWrite ('StyleOperation', $line, (int)$operation['Id']) ;
		$Changed = true ;
//logPrintVar ($line, 'Operation Update') ;
	    }	
	    unset ($Operation[$n]) ;
	    
	} else {
	    // Create new operation
	    $line['No'] = $n ;
	    $line['StyleId'] = (int)$Record['Id'] ;
	    tableWrite ('StyleOperation', $line) ;
	    $Changed = true ;
//logPrintVar ($line, 'Operation Create') ;
	}
    }

    // Delete operations left over
    foreach ($Operation as $operation) {
	tableDelete ('StyleOperation', (int)$operation['Id']) ;
	$Changed = true ;
//printf ("Operation Delete, No %d, Id %d<br>\n", (int)$operation['No'], (int)$operation['Id']) ;
    }

    if ($Changed) tableTouch ('Style', (int)$Record['Id']) ;
    
    return 0 ;
?>
