<?php

    require_once "lib/table.inc" ;
    require_once "lib/html.inc" ;
    require_once "lib/navigation.inc" ;

    // Orderline header
    printf ("<table class=item>\n") ;
    print htmlItemSpace () ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield width=80>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Style Version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    print htmlItemSpace () ;
    printf ("</table>\n") ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=25><p class=listhead>No</p></td>") ;
    printf ("<td class=listhead width=60><p class=listhead>Operation</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=80><p class=listhead>BundleText</p></td>") ;
    printf ("<td class=listhead width=80 align=right><p class=listhead>Minutes</p></td>") ;
    printf ("<td class=listhead></td>") ;
    printf ("</tr>\n") ;
    $sum = 0 ;
    while ($row = dbFetch($Result)) {
        printf ("<tr>") ;
//	printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/'.$Navigation['Icon'], navigationOnClickLink ('edit', $row["Id"])) ;
	printf ("<td class=list><img class=list src='%s' style='cursor:default;'></td>", './image/toolbar/'.$Navigation['Icon']) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['No'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['MachineGroupNumber'].$row['OperationNumber'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['BundleText'])) ;
	printf ("<td align=right><p class=list>%s</p></td>", htmlentities($row['ProductionMinutes'])) ;
	printf ("</tr>\n") ;
	$sum += $row['ProductionMinutes'] ;
    }
    if (dbNumRows($Result) == 0) {
        printf ("<tr><td></td><td colspan=2><p class=list>No operations</p></td></tr>\n") ;
    } else {
	// Total
	printf ("<tr>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>Total</p></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("</tr>\n") ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
