<?php

    require 'lib/save.inc' ;

    $fields = array (
	'No'			=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
	'Description'		=> array (),
	'Operation'		=> array ('mandatory' => true,				'check' => true),
	'BundleText'		=> array (),
	'ProductionMinutes'	=> array ('mandatory' => true,	'type' => 'decimal',	'check' => true,	'format' => '4.2'),
	'MachineGroupId'	=> array (			'type' => 'set'),
	'OperationId'		=> array (			'type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $OperationCount, $Id, $Record, $fields ;
	switch ($fieldname) {
	    case 'No':
		if (!$changed) return false ;

		// New operations may always be inserted at the end
		if ($Id == -1 and $value == ($OperationCount+1)) return true ;
		
		// Validate Operation Line No
		if ($value <= 0 or $value > $OperationCount) return "Invalid operation line No." ;

		// Changing Operation Line No is only allowed when no bundles has been made for ProductionOrders not Done
	        $query = sprintf ("SELECT CONCAT(Case.Id,'/',Production.Number) AS Number, COUNT(Bundle.Id) FROM Production, Bundle LEFT JOIN `Case` ON Case.Id=Production.CaseId WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Bundle.ProductionId=Production.Id AND Bundle.Active=1 GROUP BY Production.Id", $Record['StyleId']) ;
	        $result = dbQuery ($query) ;
		$s = '' ;
	        while ($row = dbFetch ($result)) {
		    if ($s != '') $s .= ', ' ;
		    $s .= $row['Number'] ;
		}
		dbQueryFree ($result) ;
		if ($s != '') return sprintf ("Sequense of operations can not be changed because bundles has been made for ProductionOrders (%s) using the Style", htmlentities($s)) ;
		
		return true ;

	    case 'Operation':
		// Locate entry
		$query = sprintf ("SELECT MachineGroup.Id AS MachineGroupId, Operation.Id AS OperationId FROM MachineGroup, Operation WHERE MachineGroup.Active=1 AND Operation.Active=1 AND Operation.MachineGroupId=MachineGroup.Id AND CONCAT(MachineGroup.Number, Operation.Number)=%s", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if (!$row) return "Operation not found" ;

		// Ensure that operation not allready has been used within the Style
		$query = sprintf ("SELECT * FROM StyleOperation WHERE StyleId=%d AND OperationId=%d AND Active=1 AND Id<>%d", $Record['StyleId'], $row['OperationId'], $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return "Operation is allready used in Style" ;

		// Set referances
		$fields['MachineGroupId']['value'] = $row['MachineGroupId'] ;
		$fields['OperationId']['value'] = $row['OperationId'] ;

		// ... and the ignor the operation field
		return false ;

	    case 'ProductionMinutes':
		if (!$changed) return false ;

		// Validate value
		if ($value < 0.05) return sprintf ("the ProductionMinutes (%01.2f) must be grater than 0.05", $value) ;

		// Get total time for Style
	        $query = sprintf ("SELECT SUM(ProductionMinutes) AS ProductionMinutes FROM StyleOperation WHERE StyleId=%d AND Active=1 AND Id<>%d ORDER BY No", $Record['StyleId'], $Id) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
	        dbQueryFree ($result) ;
		$ProductionMinutes = $row['ProductionMinutes'] ;
		$ProductionMinutes += $value ;
//printf ("minutes: others %01.2f, this %01.2f, total %01.2f<br>\n", $row['ProductionMinutes'], $value, $ProductionMinutes) ; 		
		
		// Validate against all ProductionOrders using the Style and not beeing done
	        $query = sprintf ("SELECT CONCAT(Case.Id,'/',Production.Number) AS Number, ProductionMinutes FROM Production INNER JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId AND ProductionLocation.TypeSample=0 LEFT JOIN `Case` ON Case.Id=Production.CaseId WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0", $Record['StyleId']) ;
	        $result = dbQuery ($query) ;
	        while ($row = dbFetch ($result)) {
//printf ("minutes: validating against %s, %01.2f<br>\n", $row['Number'], $row['ProductionMinutes']) ;		    
		    if ($row['ProductionMinutes'] < $ProductionMinutes) {
			return sprintf ("Maximun ProductionMinutes of %01.2f for ProductionOrder %s has been exceded by %01.2f minutes", $row['ProductionMinutes'], htmlentities($row['Number']), $ProductionMinutes-$row['ProductionMinutes']) ;
		    }
		}
		dbQueryFree ($result) ;
		
		return true ;
	}
	return false ;
    }
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $Record['StyleId'] = $Id ;
	    $fields['StyleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Id = -1 ;
	    break ;
    }
     
    // Save Record
    $res = saveFields ('StyleOperation', $Id, $fields, true) ;
    if ($res) return $res ;

    // Renummerate other operations
    $query = sprintf ("SELECT Id, No FROM StyleOperation WHERE StyleId=%d AND Active=1 AND Id<>%d ORDER BY No", $Record['StyleId'], $Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i == $Record['No']) $i += 1 ;       
	if ($row['No'] != $i) {
	    $query = sprintf ("UPDATE StyleOperation SET No=%d WHERE Id=%d", $i, $row['Id']) ;
	    dbQuery ($query) ;
	}
	$i += 1 ;
    }
    dbQueryFree ($result) ;

    return 0 ;
?>
