<?php

    require_once 'lib/table.inc' ;

    // Deleting Operation is only allowed when no bundles has been made for ProductionOrders not Done
    $query = sprintf ("SELECT CONCAT(Case.Id,'/',Production.Number) AS Number, COUNT(Bundle.Id) FROM Production, Bundle LEFT JOIN `Case` ON Case.Id=Production.CaseId WHERE Production.StyleId=%d AND Production.Active=1 AND Production.Done=0 AND Bundle.ProductionId=Production.Id AND Bundle.Active=1 GROUP BY Production.Id", $Record['StyleId']) ;
    $result = dbQuery ($query) ;
    $s = '' ;
    while ($row = dbFetch ($result)) {
	if ($s != '') $s .= ', ' ;
	    $s .= $row['Number'] ;
	}
    dbQueryFree ($result) ;
    if ($s != '') return sprintf ("Operation can not be deleted because bundles has been made for ProductionOrders (%s) using the Style", htmlentities($s)) ;

    // Do the deleting
    tableDelete ('StyleOperation', $Id) ;

    // Renummerate other operations
    $query = sprintf ("SELECT Id, No FROM StyleOperation WHERE StyleId=%d AND Active=1 ORDER BY No", $Record['StyleId']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($row['No'] != $i) {
	    $query = sprintf ("UPDATE StyleOperation SET No=%d WHERE Id=%d", $i, $row['Id']) ;
	    dbQuery ($query) ;
	}
	$i += 1 ;
    }
    dbQueryFree ($result) ;

    return 0
?>
