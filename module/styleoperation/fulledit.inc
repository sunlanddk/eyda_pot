<?php

//    require_once "lib/table.inc" ;
//    require_once "lib/html.inc" ;
//    require_once "lib/navigation.inc" ;

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Generate list of operations
    $query = 'SELECT Operation.*, MachineGroup.Number AS MachineGroupNumber FROM Operation LEFT JOIN MachineGroup ON MachineGroup.Id=Operation.MachineGroupId WHERE Operation.Active=1 ORDER BY MachineGroup.Number, Operation.Number' ;
    $result = dbQuery ($query) ;
    printf ("<script type='text/javascript'>\nvar opList =\n[") ;
    $n = 0 ;
    while ($row = dbFetch($result)) {
	if ($n > 0) printf (",") ;
	printf ("\n\t['%s', '%s', '%s', '%s']", addslashes($row['MachineGroupNumber'].$row['Number']), addslashes($row['Description']), addslashes($row['BundleText']), number_format($row['ProductionMinutes'], 2, ',', '')) ;
	$n++ ;
    }
    dbQueryFree ($result) ;
    printf ("\n] ;\n</script>\n") ;
    
    // Orderline header
    itemStart () ;
    itemSpace () ;
    itemField ('Article', $Record['Number']) ;
    itemField ('Description', $Record['Description']) ;
    itemField ('Style Version', (int)$Record['Version']) ;
    itemSpace () ;
    itemEnd () ;

//    printf ("<textarea name=DebugField ID='DebugField' style='width:100%%;height:100px;'></textarea><br><br>\n") ;

    formStart () ;
    listStart () ;    
    listRow () ;
    listHead ('No', 30, 'align=right') ;
    listHead ('Operation', 60, 'align=right') ;
    listHead ('Description', 310) ;
    listHead ('BundleText', 90) ;
    listHead ('Minutes', 60, 'align=right') ;
    listHead () ;

    $sum = 0 ;
    for ($n = 1 ; $n <= 99 ; $n++) {
	$row = dbFetch($Result) ;
	
	listRow () ;
	listFieldRaw (formText (sprintf ('No[%d]', $n), $n, 2, 'width:20px;text-align:right;')) ;
	listFieldRaw (formText (sprintf ('Operation[%d]', $n), $row['MachineGroupNumber'].$row['OperationNumber'], 5, 'width:50px;text-align:right;')) ;
	listFieldRaw (formText (sprintf ('Description[%d]', $n), $row['Description'], 100, 'width:300px;')) ;
	listFieldRaw (formText (sprintf ('BundleText[%d]', $n), $row['BundleText'], 10, 'width:80px;')) ;
	listFieldRaw (formText (sprintf ('Minutes[%d]', $n), ((int)$row['Id'] > 0) ? number_format ($row['ProductionMinutes'], 2, ',', '') : '', 7, 'width:50px;text-align:right;')) ;
	$sum += $row['ProductionMinutes'] ;
    }
    
    if (false) {
	// Total
	listRow () ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>Total</p></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
    }

    listEnd () ;
    formEnd () ;

    dbQueryFree ($Result) ;
?>

<script type='text/javascript'>

var opElement = null ;
var opRows = new Array(99) ;

function opRowInsert() {
    var n ;
    var j ;
    var e = opElement ;
    if (!e) return ;
    var row = e.opRow ;

    for (n = opRows.length - 1 ; n > row ; n--) {
	from = opRows[n-1] ;
	to = opRows[n] ;
	for (j = 1 ; j < to.length ; j++) to[j].value = from[j].value ;	
    }
    
    to = opRows[row] ;
    for (j = 1 ; j < to.length ; j++) to[j].value = '' ;	
    
    opRows[row][1].focus() ;
}

function opRowDelete() {
    var n ;
    var j ;
    var e = opElement ;
    if (!e) return ;
    var row = e.opRow ;

    for (n = row ; n < opRows.length - 1 ; n++) {
	from = opRows[n+1] ;
	to = opRows[n] ;
	for (j = 1 ; j < to.length ; j++) to[j].value = from[j].value ;	
    }
    
    to = opRows[opRows.length - 1] ;
    for (j = 1 ; j < to.length ; j++) to[j].value = '' ;	
    
    opRows[row][1].focus() ;
}

function opChangeNo () {
    var j ;
    var n ;
    var e = event.srcElement ;
    var row = e.opRow ;
    var no = parseInt(e.value) ;

//  DebugField.value += 'opChangeNo ' ;
    
    // Validate number
    if (isNaN(no) || no.toString() != e.value || no < 0 || no > 99) {
	window.confirm('Invalid digits in No') ;
	
	event.returnValue=false ;
	opRows[row][0].value = row + 1 ;
	opRows[row][0].select() ;
	return ;
    }
    
    // Adjust no for zero offset
    no-- ;

    // Any changes ?
    if (no == row) return ;

    // Save the row to move
    var save = new Array () ;
    var from = opRows[row] ;
    for (j = 1 ; j < from.length ; j++) save.push(from[j].value) ;
    from[0].value = row + 1 ; 

    if (no < row) {
	// Move row upward - means rows in between has to be moved down
	for (n = row ; n > no ; n--) {
	    var from = opRows[n-1] ;
	    var to = opRows[n] ;
	    for (j = 1 ; j < to.length ; j++) to[j].value = from[j].value ;	
	}
    } else {
	// Move row downward - means that rows in between has to be moded up
	for (n = row ; n < no ; n++) {
	    var from = opRows[n+1] ;
	    var to = opRows[n] ;
	    for (j = 1 ; j < to.length ; j++) to[j].value = from[j].value ;	
	}
    }

    // Restore row to move in new position
    var to = opRows[no] ;
    to[0].value = no + 1 ;
    for (j = 1 ; j < from.length ; j++) to[j].value = save.shift() ;

    // Focus to moved field
    to[1].focus() ;
    to[1].select() ;
}

function opChangeOperation () {
    var n ;
    var e = event.srcElement ;
    var row = e.opRow ;
    var fields = opRows[row] ;

//  DebugField.value += 'opChangeOperation ' ;
      
    // Find item in list
    for (n = 0 ; n < opList.length ; n++) {
	var item = opList[n] ;
	if (item[0] == e.value) break ;
	item = null ;
    }
    
    if (!item) {
	// Not found
	fields[2].value = 'Not found ' ;
	fields[3].value = '' ;
	fields[4].value = '' ;
	return ;
    }

    // Show default values
    fields[2].value = item[1] ;
    fields[3].value = item[2] ;
    fields[4].value = item[3] ;
}

function opOnBlur () {
//  DebugField.value += 'opOnBlur ' ;
    opElement = event.srcElement ;
    return ;
}

function opOnFocus () {
//  DebugField.value += 'opFocus ' ;  
    opElement = event.srcElement ;
    opElement.select () ;
    return ;
}

function opInitialize () {
//  DebugField.value += 'opInitialize ' ;

    var col = document.appform.elements ;
    var row = -1 ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'text') continue ;

	var pos = e.name.indexOf('[') ;
	if (pos == -1) continue ;
	
	switch (e.name.substr(0,pos)) {
	    case 'No' :
		row++ ;
		opRows[row] = new Array () ;
		e.attachEvent ('onchange', opChangeNo) ;
		break ;

	    case 'Operation' :
		e.attachEvent ('onchange', opChangeOperation) ;
		break ;
	}
	
	if (row >= 0) {
	    opRows[row].push(e) ;	
	    e.opRow = row ;	
	    e.attachEvent ('onblur', opOnBlur) ;
	    e.attachEvent ('onfocus', opOnFocus) ;
	}
    }
}

opInitialize () ;

</script>

<?php
    return 0 ;
?>
