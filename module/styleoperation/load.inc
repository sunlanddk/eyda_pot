<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/select.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	case 'fulledit' :
	case 'fulledit-cmd' :
	case 'paste-cmd' :
	    // Get Style (parent)
	    $query = sprintf ("SELECT Style.*, Article.Number, Article.Description FROM Style LEFT JOIN Article ON Article.Id=Style.ArticleId WHERE Style.Id=%d AND Style.Active=1", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Query operations to list
	    $query = sprintf ("SELECT StyleOperation.*, Operation.Number AS OperationNumber, MachineGroup.Number AS MachineGroupNumber FROM StyleOperation LEFT JOIN Operation ON StyleOperation.OperationId=Operation.Id LEFT JOIN MachineGroup ON Operation.MachineGroupId=MachineGroup.Id WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No", $Id) ;
	    $Result = dbQuery ($query) ;

	    // Paste
	    if (selectGetType() != 'style' or selectGetId() == (int)$Record['Id']) navigationPasive ('paste') ;
	    return 0 ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $query = sprintf ("SELECT * FROM Style WHERE Active=1 AND Id=%d", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

    	    // Get number of existion operations in dessin
	    $query = sprintf ("SELECT count(*) AS OperationCount FROM StyleOperation WHERE Active=1 AND StyleId=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    $OperationCount = $row['OperationCount'] ;
	    return 0 ;
    }
	    
    if ($Id <= 0) return 0 ;

    // Get record
    $query = sprintf ("SELECT StyleOperation.*, Operation.Number AS OperationNumber, MachineGroup.Number AS MachineGroupNumber FROM StyleOperation LEFT JOIN Operation ON StyleOperation.OperationId=Operation.Id LEFT JOIN MachineGroup ON Operation.MachineGroupId=MachineGroup.Id WHERE StyleOperation.Active=1 AND StyleOperation.Id=%d", $Id) ;
    $Result = dbQuery ($query) ;
    $Record = dbFetch ($Result) ;
    dbQueryFree ($Result) ;
    
    // Get number of existion operations in dessin
    $query = sprintf ("SELECT count(*) AS OperationCount FROM StyleOperation WHERE Active=1 AND StyleId=%d", $Record['StyleId']) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $OperationCount = $row['OperationCount'] ;

    return 0 ;
?>
