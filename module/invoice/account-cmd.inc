<?php
	require_once _LEGACY_LIB.'/lib/table.inc' ;
	require_once _LEGACY_LIB.'/lib/file.inc' ;
	
	global $User, $Navigation, $Record ;

	$debug = 0 ;

	// Configuration parameters
	$ftpParam['host'] 	= 'vion.dk';
	$ftpParam['usr'] 	= 'vion';
	$ftpParam['pwd'] 	= 'mslhjzuby';
	if ($debug == 1) {
		$fileinfo['ftpPath']  	= '/eyda_test/';
	} else {
		$fileinfo['ftpPath']  	= '/eyda/';
	}
	$fileinfo['localePath'] = $Config['fileStore'] .'/export/';
	 

    // Intiate new transaction
	if ($debug == 1) {
		$accno = 1186 ; //$dbInsertId;
	} else {
		$query = sprintf("INSERT INTO AccountingNo SET AccountingDate=now(), CompanyId=%d",787);
		dbQuery($query);
		$accno = $dbInsertId;
	}
	
	// Mark all new invoices
    $query = sprintf ("UPDATE Invoice SET AccountingNo=%s WHERE AccountingNo=0 AND Ready=1 AND Active=1 AND FromCompanyId=%d", $accno, 787) ;
	dbQuery ($query) ;
	if ($Navigation['Parameters'] == 'Manuel') {
		printf("%d invoices assigned to accounting number %d for %s.<br><br>", $dbRowsAffected, $accno, tableGetField (Company, Name, 787));
	}

	// Mark all new Customers.
    $query = sprintf ("UPDATE Company SET AccountingNo=%s WHERE AccountingNo=0 AND TypeCustomer=1 AND Active=1 AND ToCompanyId=%d", $accno, 787) ;
	dbQuery ($query) ;
	if ($Navigation['Parameters'] == 'Manuel') {
		printf("%d new debitors assigned to accounting number %d for %s.<br><br>", $dbRowsAffected, $accno, tableGetField (Company, Name, 787));
	}
	
	// Get data for Debitors
	$query = sprintf("SELECT  Company.Number as CompanyNumber, Company.Name as CompanyName,
							Company.Address1 as CompanyAddress1, Company.Address2 as CompanyAddress2, Company.Zip as CompanyZip,Company.City as CompanyCity,
							Country.IsoCode as CountryName,
							(IF(Country.IsoCode='DK',1,IF(Country.CountryGroup='eu',2,3))) AS `Zone`,
							Company.VATNumber as CompanyVATNumber,
							Currency.Name as CurrencyCode
							FROM (`Company`) 
							LEFT JOIN `Country` ON Country.Id=Company.CountryId 
							LEFT JOIN `Currency` ON Currency.Id=Company.CurrencyId 
						WHERE Company.AccountingNo = %d AND Company.TypeCustomer=1 and Company.Active=1
					", $accno) ;
	$result = dbQuery ($query) ;
	$n=0 ;
	$debitorFile 	=  'deb_' . $accno .  '.csv' ;
	$local_file = $fileinfo['localePath'] . $debitorFile; 
	$file = fopen($local_file,"w");
	while ($row = dbFetch($result)) {
		if ($n==0) { // Header rows
			$csv_data = "\r\n\r\n\r\n\r\n" ;
			$_keys = array_keys($row) ;
			$csv_data .= array2csv($_keys) ;
			$csv_data .= "\r\n" ;
			fwrite($file,$csv_data);
		}
		$csv_data = array2csv($row);
		$csv_data .= "\r\n" ;
		fwrite($file,$csv_data);
		$n++ ;
	}
	dbQueryFree ($result) ;
	fclose($file) ;

	// Get data for invoice and goods consumption
	$query = sprintf("SELECT * FROM (
						SELECT
							Invoice.Number AS `InvoiceNumber`, 
							Company.Number AS `CompanyNumber`, 
							Company.Name AS `CompanyName`, 
							PaymentTerm.Name AS `PaymentTermName`, 
							if((Convert(Invoice.InvoiceDate, char) > '0001-01-01' and  Invoice.InvoiceDate is not null), Invoice.InvoiceDate, '') AS `InvoiceDate`, 
							if((Convert(Invoice.DueBaseDate, char) > '0001-01-01'  and Invoice.DueBaseDate is not null), Invoice.DueBaseDate, '') AS `DueBase`, 
							if((Convert(Invoice.DueDate, char) > '0001-01-01' and Invoice.DueDate is not null), Invoice.DueDate, '') AS `InvoiceDueDate`, 
							if((Convert(Invoice.DueDate, char) > '0001-01-01' and Invoice.DueDate is not null), ADDDATE(Invoice.DueBaseDate, PaymentTerm.DiscountDays), '') AS `DiscountDue`, 
							PaymentTerm.DiscountPct AS `DiscountPct`, 
							Currency.Name AS `CurrencyName`, 
							SUM(InvoiceLine.PriceSale*InvoiceLine.Quantity*(100-InvoiceLine.Discount)/100)*IF(Invoice.Credit,-1,1) AS `InvoiceTotal`, 
							SUM(InvoiceLine.PriceSale*InvoiceLine.Quantity*(100-InvoiceLine.Discount)/100)*IF(Invoice.Credit,-1,1)*Country.VATPercentage/100 AS `InvoiceVAT`,
							SUM(((InvoiceLine.PriceSale*InvoiceLine.Quantity*(100-InvoiceLine.Discount)/100)*IF(Invoice.Credit,-1,1)*Country.VATPercentage/100)+(InvoiceLine.PriceSale*InvoiceLine.Quantity*(100-InvoiceLine.Discount)/100)*IF(Invoice.Credit,-1,1)) AS `InvoiceTotal2`, 
							(SELECT Account FROM AccountMap m WHERE m.LeadDigit=ArticleType.LeadDigit AND m.CountryGrp=IF(Country.IsoCode='DK',1,IF(Country.CountryGroup='eu',2,3))) AS `Account`
						FROM ( `Invoice`, `InvoiceLine`, `Article`, `ArticleType`  ) 
							  LEFT JOIN `Company` ON Company.Id=Invoice.CompanyId 
							  LEFT JOIN `PaymentTerm` ON PaymentTerm.Id=Invoice.PaymentTermId 
							  LEFT JOIN `Currency` ON Currency.Id=Invoice.CurrencyId 
							  LEFT JOIN `Country` ON Country.Id=IF(Company.InvoiceCountryId>0,Company.InvoiceCountryId,Company.CountryId) 
						WHERE (Invoice.AccountingNo = %d) 	  
								AND Invoice.Ready=1 and Invoice.Active=1
								AND InvoiceLine.InvoiceId=Invoice.Id
								AND InvoiceLine.Active=1 
								AND InvoiceLine.ArticleId=Article.Id 
							AND Article.ArticleTypeId=ArticleType.Id 
						GROUP BY Invoice.Id, Account
						UNION
						SELECT 
							Invoice.Number AS `InvoiceNumber`, 
							'02030' AS `CompanyNumber`, 
							Concat('VF fakt. ', Invoice.Number) AS `CompanyName`, 
							'' AS `PaymentTermName`, 
							if((Convert(Invoice.InvoiceDate, char) > '0001-01-01' and  Invoice.InvoiceDate is not null), Invoice.InvoiceDate, '') AS `InvoiceDate`, 
							'' AS `DueBase`, 
							'' AS `InvoiceDueDate`, 
							'' AS `DiscountDue`, 
							0 AS `DiscountPct`, 
							'DKK' AS `CurrencyName`, 
							SUM(InvoiceLine.PriceCost*InvoiceLine.Quantity*IF(Invoice.Credit,-1,1)) AS `InvoiceTotal`, 
							0 AS `InvoiceVAT`,
							SUM(InvoiceLine.PriceCost*InvoiceLine.Quantity*IF(Invoice.Credit,-1,1)) AS `InvoiceTotal2`, 
							'14500' AS `Account`
						  FROM ( `Invoice`, `InvoiceLine`, `Article`, `ArticleType`  ) 
							  LEFT JOIN `Company` ON Company.Id=Invoice.CompanyId 
							  LEFT JOIN `PaymentTerm` ON PaymentTerm.Id=Invoice.PaymentTermId 
							  LEFT JOIN `Currency` ON Currency.Id=Invoice.CurrencyId 
							  LEFT JOIN `Country` ON Country.Id=IF(Company.InvoiceCountryId>0,Company.InvoiceCountryId,Company.CountryId) 
						  WHERE (Invoice.AccountingNo = %d) 	  
								AND Invoice.Ready=1 and Invoice.Active=1
								AND InvoiceLine.InvoiceId=Invoice.Id
								AND InvoiceLine.Active=1 
								AND InvoiceLine.ArticleId=Article.Id 
							AND Article.ArticleTypeId=ArticleType.Id 
						GROUP BY Invoice.Id, Account) tab
					  ORDER BY InvoiceNumber, `Account`", 
					$accno, $accno) ;
	$result = dbQuery ($query) ;
	$n=0 ;
	$invoiceFile 	=  'inv_' . $accno .  '.csv' ;
	$local_file = $fileinfo['localePath'] . $invoiceFile; 
	$file = fopen($local_file,"w");
	while ($row = dbFetch($result)) {
		if ($n==0) { // Header rows
			$csv_data = "\r\n\r\n\r\n\r\n" ;
			$_keys = array_keys($row) ;
			$csv_data .= array2csv($_keys) ;
			$csv_data .= "\r\n" ;
			fwrite($file,$csv_data);
		}
		$row['InvoiceTotal'] = number_format($row['InvoiceTotal'] ,2,',','.') ;
		$row['InvoiceTotal2'] = number_format($row['InvoiceTotal2'] ,2,',','.') ;
		$row['InvoiceVAT'] = number_format($row['InvoiceVAT'],2,',','.') ;
		$csv_data = array2csv($row);
		$csv_data .= "\r\n" ;
		fwrite($file,$csv_data);
		$n++ ;
	}
	dbQueryFree ($result) ;
	fclose($file) ;

	if ($Navigation['Parameters'] == 'Manuel') {
		$_msg = printf("Export lists now available from Reports. Invoice Entries: " . $n . "<br><br>") ;
	} else {
		$_msg = "No new entries" ;
	}
	
	if ($Config['Instance'] == 'Test' and $debug == 0) return 0 ; // dont transfer file if test instance - unless debugging to test folder.

	// Transfer to FTP
	$fileinfo['fileName'] = $debitorFile ;
	ftpUpload($ftpParam, $fileinfo) ;
	
	$fileinfo['fileName'] = $invoiceFile ;
	ftpUpload($ftpParam, $fileinfo) ;
	
    return $_msg ;
?>
