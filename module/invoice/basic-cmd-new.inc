<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;

    $fields = array (
  	'CompanyId'		=> array ('type' => 'set'),
	'FromCompanyId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'Credit'		=> array ('type' => 'checkbox'),

	'Description'		=> array (						'check' => true),
	'Reference'		=> array (						'check' => true),

	'SalesUserId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'CurrencyId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'PaymentTermId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
//	'DeliveryTermId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
//	'CarrierId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

//	'Address1'		=> array (						'check' => true),
//	'Address2'		=> array (						'check' => true),
//	'ZIP'			=> array (						'check' => true),
//	'City'			=> array (						'check' => true),
//	'CountryId'		=> array ('type' => 'integer',				'check' => true),
	
	'InvoiceHeader'		=> array (						'check' => true),
	'InvoiceFooter'		=> array (						'check' => true),
	'CompanyFooter'		=> array (						'check' => true),

	'Ready'			=> array ('type' => 'checkbox',				'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate'		=> array ('type' => 'set'),
	'Done'			=> array ('type' => 'checkbox',				'check' => true),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set'),

	'InvoiceDate'		=> array ('type' => 'date',	'mandatory' => true,	'check' => true),
	'DueBaseDate'		=> array ('type' => 'date',	'mandatory' => true,	'check' => true),
	
	'Number'		=> array ('type' => 'set'),
	'DueDate'		=> array ('type' => 'set'),
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Ready':
		// Ignore if not setting flag
		if (!$changed or !$value) {
		    unset ($fields['DueBaseDate']) ;
		    unset ($fields['InvoiceDate']) ;
		    return false ;
		}

		// Any Invoice Lines
		$query = sprintf ("SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1", $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count == 0) return 'the Invoice can not be set Ready when it has no InvoiceLines' ;

		// Has colors been assigned to all Articles


		
		// Validate Quantity

		// Get last used number
	if (($Record['FromCompanyId'] == 2) or ($Record['FromCompanyId'] == 787)) {
		$query = 'SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND (FromCompanyID=2 or FromCompanyID=787)' ;
	} else {
		$query = sprintf('SELECT MAX(Number) AS Number FROM Invoice WHERE Ready=1 AND Active=1 AND FromCompanyID=%d', $Record['FromCompanyId']) ;
	}
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$fields['Number']['value'] = (int)$row['Number'] + 1 ;
		
		// Set tracking information
		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;

		return true ;

	    case 'Done':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Ready']) return 'the Invoice can not be Done before it has been set Ready' ;	

		// Set tracking information
		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'InvoiceDate' :
		// Get InvoiceDate
		$t = dbDateDecode($value) ;
		
		// Validate
		if ($t < time()-30*24*60*60 or $t > time()+30*24*60*60) return "Invoice Date must be with +/- one month from today" ;
		return true ;

	    case 'DueBaseDate' :
		// Get DueBaseDate
		$t = dbDateDecode($value) ;
		
		// Validate
		if ($t < dbDateDecode($Record['InvoiceDate'])) return "DueBase can not be before Invoice date" ;
		if ($t > dbDateDecode($Record['InvoiceDate'])+365*24*60*60) return "DueBase must be within one year from Invoice date" ;
		
		// calculate DueDate
		if (!$Record['Credit']) {
		    // Get PaymentTerms and 
		    $query = sprintf ('SELECT * FROM PaymentTerm WHERE Id=%d', (int)$Record['PaymentTermId']) ;
		    $result = dbQuery ($query) ;
		    $row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    $t = getdate ($t) ;
		    if ($row['RunningMonth']) {
			$t['mday'] = 1 ;
			$t['mon'] += 1 ;
		    }
		    $t['mon'] += (int)$row['PayMonths'] ;
		    $t['year'] += ($t['mon'] - 1) / 12 ;		
		    $t['mon'] = ($t['mon'] - 1) % 12 + 1 ;
		    $t = mktime (0, 0, 0, $t['mon'], $t['mday'], $t['year']) ;
		    $t += 60*60*24*(int)$row['PayDays'] ;
		    $fields['DueDate']['value'] = date ('Y-m-d', $t) ;
		}
		
		return true ;
	}

	if (!$changed) return false ;
	if ($Record['Ready']) return sprintf ('the Invoice can not be modified when Ready, field %s', $fieldname) ;
	return true ;	
    }
    
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($fields['Ready']) ;
	    unset ($fields['Done']) ;
	    unset ($fields['DueBaseDate']) ;
	    unset ($fields['InvoiceDate']) ;
	    $fields['CompanyId']['value'] = (int)$Record['Id'] ;
	    $Id = -1 ;
	    break ;

	default :
	    unset ($fields['Credit']) ;
	    if ($Record['Done']) return 'the Invoice can not be modified when Done' ;
	    break ;
    }
     
    $res = saveFields ('Invoice', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View
	    return navigationCommandMark ('invoiceview', (int)$Record['Id']) ;
    }

    return 0 ;    
?>
