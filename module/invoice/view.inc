<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;

    function flag (&$Record, $field) {
	if ($Record[$field]) {
	    itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
	} else {
	    itemField ($field, 'No') ;
	}
    }

    itemStart () ;
    itemHeader () ;
    itemField ('Number', ($Record['Ready']) ? (int)$Record['Number'] : 'Draft') ;
    itemField ('Date', ($Record['Ready']) ? date('Y-m-d', dbDateDecode($Record['InvoiceDate'])) : '-') ;
    itemSpace () ;
    itemField ('Decsription', $Record['Description']) ;
    itemSpace () ;
    itemField ('Type', $Record['Type']) ;
    itemSpace () ;
    itemField ('State', $Record['State']) ;
	if (!$User['Extern']) 
		itemFieldIcon ('Owner', tableGetField ('Company', 'Company.Name', (int)$Record['FromCompanyId']), 'company.gif', 'companyview', $Record['FromCompanyId']) ;
    itemSpace () ;
    flag ($Record, 'Ready') ;
    flag ($Record, 'Done') ; 
    itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;

    printf ("<table class=item><tr><td>\n") ;

    itemStart () ;
    itemHeader ('Sales') ;
	if ($User['Extern']) 
		itemField('Customer', $Record['CompanyNumber']) ;
	else
		itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
    itemField ('Name', $Record['CompanyName']) ;    
    itemSpace () ;
    itemField ('Reference', $Record['Reference']) ;
    itemSpace () ;
    itemField ('Sales Ref', tableGetField ('User', 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")', (int)$Record['SalesUserId'])) ;  
    itemSpace () ;
    itemEnd () ;

    printf ("</td>") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
    itemHeader ('Invoice') ;
    itemField ('Currency', tableGetField ('Currency', 'CONCAT(Description," (",Name,")")', (int)$Record['CurrencyId'])) ;  
    itemSpace () ;
    itemField ('Payment Term', tableGetField ('PaymentTerm', 'CONCAT(Description," (",Name,")")', (int)$Record['PaymentTermId'])) ;  
    itemField ('Date Base', ($Record['DueBaseDate']>'2001-01-01') ? date('Y-m-d', dbDateDecode($Record['DueBaseDate'])) : '') ;
    itemField ('Date Due', ($Record['DueDate']>'2001-01-01') ? date('Y-m-d', dbDateDecode($Record['DueDate'])) : '') ;
    itemSpace () ;
    itemFieldText ('InvoiceHeader', $Record['InvoiceHeader']) ;
    itemFieldText ('InvoiceFooter', $Record['InvoiceFooter']) ;
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
    
    itemStart () ;
    itemHeader ('Delivery') ;
    if ((int)$Record['StockId'] > 0) {
	if ($User['Extern']) 
		itemField('Shipment', $Record['StockName']) ;
	else
		itemFieldIcon ('Shipment', $Record['StockName'], 'shipment.gif', 'shipmentview', (int)$Record['StockId']) ;
	itemField ('Description', $Record['StockDescription']) ;    
	itemSpace () ;
	itemField ('Delivery Terms', tableGetField ('DeliveryTerm', 'CONCAT(Name," (",Description,")")', (int)$Record['DeliveryTermId'])) ;  
	itemSpace () ;
	itemField ('Carrier', tableGetField ('Carrier', 'Name', (int)$Record['CarrierId'])) ;  
	itemSpace () ;
	itemField ('Street', $Record['Address1']) ;
	itemField ('', $Record['Address2']) ;
	itemField ('ZIP', $Record['ZIP']) ;
	itemField ('City', $Record['City']) ;
	itemField ('Country',  tableGetField ('Country', 'CONCAT(Name," - ",Description)', (int)$Record['CountryId'])) ;
    } else {
	itemField ('No Shipment', '') ;
    }
    itemSpace () ;
    itemEnd () ;

    printf ("</td>\n") ;
    printf ("</tr></table>\n") ;

    listStart () ;
    listHeader ('Lines') ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Pos', 30) ;
    listHead ('Order', 50) ;
    listHead ('Article', 75) ;
    listHead ('Colour', 140) ;
    listHead ('Description') ;
    listHead ('Sizes') ;
    listHead ('Quantity', 80, 'align=right') ;
    listHead ('Discount', 70, 'align=right') ;
    listHead ('Price', 90, 'align=right') ;
    listHead ('Total', 90, 'align=right') ;
    listHead ('', 8) ;

    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ('invoiceline.gif', 'invoicelineview', (int)$row['Id']) ;
	listField ((int)$row['No']) ;
	listField (((int)$row['OrderLineId'] > 0) ? sprintf('%d,%d', (int)$row['OrderId'], (int)$row['OrderLineNo']) : '') ;
	listField ($row['ArticleNumber']) ;

	$field = '' ;
	if ($row['VariantColor']) {
	    $field .= '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorDescription']) ;	    
	}
	listFieldRaw ($field) ;

	listField ($row['Description']) ;

		$query = 'SELECT group_concat(az.name) AS Sizes FROM invoicequantity oq, articlesize az where oq.quantity>0 and oq.active=1 and oq.invoicelineid=' . $row['Id'] . ' and az.id=oq.articlesizeid group by oq.invoicelineid';
	    $res = dbQuery ($query) ;
	    $_row = dbFetch ($res) ;
	    dbQueryFree ($res) ;
		listField ($_row['Sizes']) ;

	listField (number_format((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
	listField ((int)$row['Discount'] . ' %', 'align=right') ;
	listField (number_format((float)$row['PriceSale'], 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
	$v = (float)$row['PriceSale'] * (float)$row['Quantity'] ;
	if ((int)$row['Discount'] > 0) $v -= $v * (int)$row['Discount'] / 100 ;
	$total += $v ;
	listField (number_format($v, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No InvoiceLines', 'colspan=5') ;
    } else {
	// Total
	listRow () ;
	listField ('', 'colspan=10 style="border-top: 1px solid #cdcabb;"') ;
	listField (number_format($total, 2, ',', '.') . ' ' . $Record['CurrencySymbol'], 'align=right style="border-top: 1px solid #cdcabb;"') ;
    }
    listEnd () ;

    return 0 ;
?>
