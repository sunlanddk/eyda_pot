<?php
// The character set we should use for this language. If you are unsure, use ISO-8859-1
$charset = "ISO-8859-1";

// General Texts
$Txt['Amount']			= 'Sum ';
$Txt['Quantity']		= 'Antal';
$Txt['Price']			= 'Pris';

// Texts for invoice lines
$Txt['Order']			= 'Ordre';
$Txt['Reference']		= 'Reference: ';
$Txt['SMS']			= 'SMS: ';
$Txt['Pos']				= 'Pos';
$Txt['Article']			= 'Artikel';
$Txt['Certificate']		= 'Certifikat';
$Txt['Origin']			= 'Oprindelse';
$Txt['Comp']			= 'Komp.';
$Txt['Colour']			= 'Farve';
$Txt['Size']			= 'Str.';
$Txt['Discount']		= 'Rabat';

// VAT
$Txt['Customer']		= 'Kunde';
$Txt['VATNo']			= 'Moms No.: ';
$Txt['VAT']				= 'Moms ';

// Texts for custom lines
$Txt['Customsinformation']= 'Told information';
$Txt['CustomPos']		= 'Pos';
$Txt['CustomAmount']	= 'Sum';

// Texts for body footer
$Txt['Colli']			= 'Colli';
$Txt['GrossWeight']		= 'Brutto v�gt';
$Txt['NetWeight']		= 'Netto v�gt';
$Txt['Volume']			= 'Volume';
$Txt['TotalQuantity']	= 'Total Antal';

$Txt['PrePay'] = 'FORUD BETALT' ;

?>