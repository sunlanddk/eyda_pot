<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
	
//	$Record['SetReady'] = 1 ;
    if ($Record['PreInvoiceId'] > 0) {
	   //Update shipment invoice flag
		if ($Record['Departed']) {
			$Stock = array (
			'Invoiced' => (int)1,
			'InvoicedUserId' => $User['Id'],
			'InvoicedDate' => dbDateEncode(time()),
			'Done' => (int)1,
			'DoneUserId' => $User['Id'],
			'DoneDate' => dbDateEncode(time())
			) ;
		} else {
			$Stock = array (
			'Invoiced' => (int)1,
			'InvoicedUserId' => $User['Id'],
			'InvoicedDate' => dbDateEncode(time())
			) ;
			
		}
		tableWrite ('Stock', $Stock, $Record['Id']) ;

		// set áll orders done
		$query = sprintf ('select orderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid', (int)$Record['PreInvoiceId']) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Order = array (	
				'Done'			=>  1,
				'DoneUserId'	=>  $User['Id'],
				'DoneDate'		=>  dbDateEncode(time()),
				'OrderDoneID'	=>  5
			) ;
			tableWrite ('Order', $Order, $row['orderid']) ;
			
			$OrderId = $row['orderid'] ;
			
			// Finish (set Packed) pickorder if there is one
			$PickOrderId=tableGetFieldWhere ('PickOrder', 'Id', sprintf('ReferenceId=%d and Active=1', $row['orderid'])) ;
			$PickOrder = array (	
				'Packed'			=>  1
			) ;
			if ($PickOrderId>0)
				tableWrite ('PickOrder', $PickOrder, $PickOrderId) ;

		}
		dbQueryFree ($result) ;
		
		// set all lines done that are not already done
		$query = sprintf ('select orderline.* from orderline join
										(select ol.orderid as OLorderid from invoiceline il, orderline ol
										 where il.invoiceid=%d and il.orderlineid=ol.id and il.active=1 and ol.active=1 and ol.done=0
										 group by ol.orderid) Orders
							where orderline.orderid=Orders.OLorderid and orderline.active=1', (int)$Record['PreInvoiceId']) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch($result)) {
			$Orderline = array (	
					'Done'			=>  1,
					'DoneUserId'	=>  $User['Id'],
					'DoneDate'		=>  dbDateEncode(time()),
			) ;
			tableWrite ('Orderline', $Orderline, $row['Id']) ;
		}
		
		//return 'test ' . $row['OrderId'] . ' M ' .  $row['orderid'] . ' M '. $OrderId . ' M '. $OrderTypeId . ' Test OT:' . $OrderTypeID . ' ShopNo ' . $ShopNo . ' WEBID ' . $WEBID . ' query ' . $query ;
		
		dbQueryFree ($result) ;

		return navigationCommandMark ('orderstatus', (int)$Record['PreInvoiceOrderId']) ;
	    return navigationCommandMark ('invoiceview', (int)$Record['PreInvoiceId']) ;
	}
    // Validate Shipment
    if (!$Record['Ready']) return 'Shipment is not Ready' ;
    if ($Record['Type'] != 'shipment') 'selected Stock must be a shipment' ;

    // Check if Invoices allready has been made for this Shipment
    $query = sprintf ('SELECT Id FROM Invoice WHERE StockId=%d AND Active=1', (int)$Record['Id']) ;
    $res = dbQuery ($query) ;
    $count = dbNumRows ($res) ;
    dbQueryFree ($res) ;
    if ($count > 0) return 'Invoice(s) has allready been generated for this Shipment' ;
    
    // Get Company information
    $query = sprintf ('SELECT * FROM Company WHERE Id=%d', (int)$Record['CompanyId']) ;
    $res = dbQuery ($query) ;
    $Company = dbFetch ($res) ;
    dbQueryFree ($res) ;
//logPrintVar ($Company, 'Company') ;
   
    // Valiables
    $Part = 0 ;
    $OrderId = 0 ;
    $Invoice = array () ;
    $InvoiceLine = array () ;
    $InvoiceQuantity = array () ;
    $Quantity = (float)0 ;   

	//TMP: No invoices if prices is 0!
    $query = sprintf('
		select sum(PriceSale) as PriceSale 
		from
			(select sum(PriceSale) as PriceSale
			    from container
				INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
				LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
 				WHERE Container.StockId=%d AND Container.Active=1
			union
			SELECT sum(PriceSale) as PriceSale
				FROM `Order`
			    JOIN (select ol.orderid as orderid from item i, container c, orderline ol where i.containerid=c.id and c.stockid=%d and i.orderlineid=ol.id group by ol.orderid) Orders ON `Order`.Id=Orders.OrderId
				JOIN Orderline On `Order`.Id=OrderLine.OrderId
				JOIN Article ON Article.Id=Orderline.ArticleId
			WHERE article.articletypeid=6 and orderline.done=0) Prices
		',  (int)$Record['Id'], (int)$Record['Id']) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    if ($row['PriceSale'] > 0) 
	$Price_to_be_invoiced = 1 ;
    else
	$Price_to_be_invoiced = 1 ;
//	return 'No invoice generated since Price is 0' ;


    // Generate Query for Item Quantities in Shipment
/*
    $query =   'SELECT
		Order.Id AS OrderId, Order.CurrencyId, Order.PaymentTermId, Order.SalesUserId,  
		OrderLine.Id AS OrderLineId, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity
		FROM Container
		INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
		INNER JOIN Article ON Article.Id=Item.ArticleId
		LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId' ;
    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$Record['Id']) ;
    $query .= ' GROUP BY OrderLine.Id, Item.ArticleId, Item.ArticleColorId*Article.VariantColor, Item.ArticleSizeId*Article.VariantSize, Item.ArticleCertificateId*Article.VariantCertificate
		ORDER BY OrderLine.OrderId, OrderLine.No, Article.Number' ;
*/
    $query =   '(SELECT
		Order.Id AS OrderId, Order.CurrencyId, Order.PaymentTermId, Order.SalesUserId, Order.Email, 
		OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity, SUM(Item.Price*Item.Quantity) AS ItemCost
		FROM Container
		INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
		INNER JOIN Article ON Article.Id=Item.ArticleId
		LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId' ;
    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$Record['Id']) ;
    $query .= ' GROUP BY OrderLine.Id, Item.ArticleId, Item.ArticleColorId*Article.VariantColor, Item.ArticleSizeId*Article.VariantSize, Item.ArticleCertificateId*Article.VariantCertificate' ;

    $query .=   sprintf ( ') union (SELECT
		`Order`.Id AS OrderId, `Order`.CurrencyId, `Order`.PaymentTermId, `Order`.SalesUserId, Order.Email,
		OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Orderline.ArticleColorId as ArticleColorId, OrderQuantity.ArticleSizeId as ArticleSizeId, 0 as ArticleCertificateId, OrderLine.Quantity AS Quantity, SUM((OrderLine.Quantity*OrderLine.PriceSale)*Currency.Rate/100) AS ItemCost
	FROM `Order`
    JOIN (select ol.orderid as orderid from item i, container c, orderline ol where i.containerid=c.id and c.stockid=%d and i.orderlineid=ol.id group by ol.orderid) Orders ON `Order`.Id=Orders.OrderId
	JOIN Orderline On `Order`.Id=OrderLine.OrderId
	LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=Orderline.Id
	JOIN Article ON Article.Id=Orderline.ArticleId
	JOIN Currency ON Currency.Id=Order.CurrencyId
    WHERE article.articletypeid=6 and orderline.done=0 and orderline.active=1 and OrderLine.Quantity>0
     GROUP BY order.id, Orderline.id) ORDER BY OrderId, OrderLineNo, ArticleNumber', (int)$Record['Id']) ;

    $res = dbQuery ($query) ;
    while ($row = dbFetch ($res)) {
	// Check if Order associated the Item
	if ((int)$row['OrderId'] == 0) {
	    // No Order
	    // Fill in default information from Company
	    $row['CurrencyId'] = $Company['CurrencyId'] ;
	    $row['PaymentTermId'] = $Company['PaymentTermId'] ;
	    $row['SalesUserId'] = $Company['SalesUserId'] ;
	    $row['CompanyFooter'] = $Company['CompanyFooter'] ;
	    $row['Description'] = $row['ArticleDescription'] ;
	}

//logPrintVar ($row, 'row') ;

	// New Invoice ?
	if ((int)$Invoice['Id'] == 0 or
	    ($Company['InvoiceSplit'] and ((int)$row['OrderId'] != $OrderId)) or
	    $Invoice['CurrencyId'] != (int)$row['CurrencyId'] or
	    $Invoice['PaymentTermId'] != (int)$row['PaymentTermId'] or
	    $Invoice['SalesUserId'] != (int)$row['SalesUserId']				) {
		
	    // Generate the Invoice
	    $Invoice = array (
		'Active'		=> 1,
		'CompanyId'		=> (int)$Company['Id'],
		'FromCompanyId'		=> (int)$Record['FromCompanyId'],
		'StockId'		=> (int)$Record['Id'],
		'Credit'		=> 0,
		'Description'		=> sprintf ('Generated from Shipment, order %d - part %d', (int)$row['OrderId'], ++$Part),
		'Email'				=> $row['Email'],
		'CurrencyId'		=> (int)$row['CurrencyId'],
		'PaymentTermId'		=> (int)$row['PaymentTermId'],
		'SalesUserId'		=> (int)$row['SalesUserId'],
		'CompanyFooter'		=> sprintf ("%s", $Company['CompanyFooter']),
	    ) ;

	    $Invoice['Id'] = tableWrite ('Invoice', $Invoice) ;
//$Invoice['Id'] = 9999 ;
//logPrintVar ($Invoice, 'Invoice') ;
	    
	    $No = 0 ;
	    $OrderId = (int)$row['OrderId'] ;
	}

	// New Invoice Line ?
	if ($InvoiceLine['Id'] == 0 or
	    $InvoiceLine['InvoiceId'] != $Invoice['Id'] or
	    $InvoiceLine['OrderLineId'] != (int)$row['OrderLineId'] or
	    $InvoiceLine['ArticleId'] != (int)$row['ArticleId'] or
	    !$row['VariantSize'] or
	    ($row['VariantColor'] and ($InvoiceLine['ArticleColorId'] != (int)$row['ArticleColorId'])) or
	    ($row['VariantCertificate'] and ($InvoiceLine['ArticleCertificateId'] != (int)$row['ArticleCertificateId']))  ) {
	    // Update Invoice with correct Quantity
	    if ($InvoiceLine['Id'] > 0 and $Quantity > $InvoiceLine['Quantity']) {
			$_item =  (float)$ItemCost / $Quantity ;
			$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f, PriceCost=%.04f WHERE Id=%d', $Quantity, $_item, $InvoiceLine['Id']) ;
			dbQuery ($query) ;
	    }
		
	    // Generate the InvoiceLine
	    $InvoiceLine = array (
			'Active'		=> 1,
			'InvoiceId'		=> $Invoice['Id'],
			'OrderLineId'		=> (int)$row['OrderLineId'],
			'No'			=> ++$No,
			'Description'		=> $row['Description'],
			'InvoiceFooter'		=> $row['InvoiceFooter'],
			'CustArtRef'		=> $row['CustArtRef'],
			'ArticleId'		=> (int)$row['ArticleId'],
			'Quantity'		=> $row['Quantity'],
			'Discount'		=> (int)$row['Discount'],
			'PriceCost'		=> (float)$row['ItemCost']/(float)$row['Quantity'],
			'PriceSale'		=> (float)$row['PriceSale']
	    ) ;
	    if ($row['VariantColor']) $InvoiceLine['ArticleColorId'] = (int)$row['ArticleColorId'] ;
	    if ($row['VariantCertificate']) $InvoiceLine['ArticleCertificateId'] = (int)$row['ArticleCertificateId'] ;

	    $InvoiceLine['Id'] = tableWrite ('InvoiceLine', $InvoiceLine) ;
//$InvoiceLine['Id'] = 9999 ;
//logPrintVar ($InvoiceLine, 'InvoiceLine') ;

	    $Quantity = (float)0 ;   
	    $ItemCost = (float)0 ;   
	}


	// Size Variants
	if ($row['VariantSize']) {
	    $InvoiceQuantity = array (
		'Active'		=> 1,
		'InvoiceLineId'		=> $InvoiceLine['Id'],
		'ArticleSizeId'		=> (int)$row['ArticleSizeId'],
		'Quantity'		=> $row['Quantity']
	    ) ;
		
	    tableWrite ('InvoiceQuantity', $InvoiceQuantity) ;
//logPrintVar ($InvoiceQuantity, 'InvoiceQuantity') ;

	    $Quantity += (float)$row['Quantity'] ;
	    $ItemCost += (float)$row['ItemCost'] ;
	}
	
    }
    dbQueryFree ($res) ;

    // Update Invoice with correct Quantity
    if ($InvoiceLine['Id'] > 0 and $Quantity > (float)$InvoiceLine['Quantity']) {
	$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f WHERE Id=%d', $Quantity, $InvoiceLine['Id']) ;
	dbQuery ($query) ;
//printf ("%s<br>\n", $query) ;
    }
		
   //Update shipment invoice flag
	if ($Record['Departed']) {
		$Stock = array (
	    'Invoiced' => (int)1,
	    'InvoicedUserId' => $User['Id'],
	    'InvoicedDate' => dbDateEncode(time()),
	    'Done' => (int)1,
	    'DoneUserId' => $User['Id'],
	    'DoneDate' => dbDateEncode(time())
	    ) ;
	} else {
		$Stock = array (
	    'Invoiced' => (int)1,
	    'InvoicedUserId' => $User['Id'],
	    'InvoicedDate' => dbDateEncode(time())
	    ) ;
	}
	tableWrite ('Stock', $Stock, $Record['Id']) ;
	
	if(isset($Record['GoToShipment'])){
		$GoToShipment = true;
	}
	else{
		$GoToShipment = false;
	}

	if($GoToShipment === true){
		$Record['GoToShipment'] = 1;
	}

	if ($Record['SetReady']) {
		$Id = (int)$Invoice['Id'] ;
		$_POST['CompletelyDone'] = 'on' ;
		$_POST['Ready'] = 'on' ;
		$_POST['Mail'] = isset($Invoice['Email'])?$Invoice['Email']:'kc@passon-solutions.com' ; //$Record['Mail'] ;
		$_POST['OrderTypeId'] = $Record['OrderTypeId'];

		if ($Record['OrderTypeId']==10) {
			$_POST['BodyTxt'] =	 '' ;
		}
		$Navigation['Function'] = 'confirm-cmd' ; 
		
		require_once 'module/invoice/load.inc' ;
		if($GoToShipment === true){
			$Record['GoToShipment'] = 1;
		}
		require_once 'module/invoice/confirm-cmd.inc' ;
	}
	if($GoToShipment === true){
		// return navigationCommandMark ('shipmentview', (int)$Record['GoToShipment']) ;
	}
	else{
	    switch ($Part) {
		case 0 :
		    return 'no Invoices generated' ;

		case 1 :
		    return navigationCommandMark ('invoiceview', (int)$Invoice['Id']) ;

		default :
		    return navigationCommandMark ('companyinvoices', (int)$Company['Id']) ;
	    }
	}
