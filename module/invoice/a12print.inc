<?php

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;

    require_once 'lib/http.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    class PDF extends FPDF {

    }
 
    // Any CustomsPositions and forms ?
    if (count($Form) == 0) return "no Customs Forms" ;

    // Validation
    if ((int)$Record['StockId'] <= 0) return 'no Shipment for this Invoice' ;

    // Get Additional information for Record
    $Record['CurrencyName'] = tableGetField ('Currency', 'Name', (int)$Record['CurrencyId']) ;
    $Record['TotalAmount'] = 0 ;
    
    // Get My Company information
    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', parameterGet('CompanyMy')) ;
    $result = dbQuery ($query) ;
    $CompanyMy = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$CompanyMy['Id'] <= 0) return 'CompanyMy not found' ;

    // Get Full Company information
    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.EFTAMember FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['CompanyId']) ;
    $result = dbQuery ($query) ;
    $Company = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Company['Id'] <= 0) return 'Company not found' ;

    // Get Shipment
    $query = sprintf ('SELECT Stock.*, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.EFTAMember, Carrier.Name AS CarrierName, Carrier.TransportBorder, Carrier.TransportLocal, Carrier.CustomsLocation, DeliveryTerm.Name AS DeliveryTermName, DeliveryTerm.Description AS DeliveryTermDescription, COUNT(Container.Id) AS Count, SUM(Container.GrossWeight) AS GrossWeight, SUM(Container.TaraWeight) AS TaraWeight, SUM(Container.Volume) AS Volume FROM Stock LEFT JOIN Country ON Country.Id=Stock.CountryId LEFT JOIN Carrier ON Carrier.Id=Stock.CarrierId LEFT JOIN DeliveryTerm ON DeliveryTerm.Id=Stock.DeliveryTermId LEFT JOIN Container ON Container.StockId=Stock.Id AND Container.Active=1 WHERE Stock.Id=%d GROUP BY Stock.Id', (int)$Record['StockId']) ;
    $result = dbQuery ($query) ;
    $Shipment = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Shipment['Id'] <= 0) return 'Shipment not found' ;

    // Loop Positions to find number of different CustomsPositions
    $CustomsPositionsCount = 0 ;
    $FormCount = 0 ;
    $i = -1 ;
    foreach ($Form as $Country) {
	foreach ($Country as $row) {
	    if ($i != (int)$row['CustomsPositionId']) {
	        $CustomsPositionsCount++ ;
	        $i = (int)$row['CustomsPositionId'] ;
	    }
	    $FormCount += (count($row['Form']) <= 0) ? 1 : count($row['Form']) ; 
	    $Record['TotalAmount'] += (float)$row['Price'] ;
	}
    }
        
    // Make PDF
    $pdf=new PDF('P', 'mm', 'A4') ;
    $pdf->SetFont('Courier','',12);
    $pdf->SetAutoPageBreak(false) ; 

    $n = 1 ;
    foreach ($Form as $Country) {
	foreach ($Country as $row) {
	    reset ($row['Form']) ;
	    while (true) {
	    	$form = current ($row['Form']) ;   
	
		$pdf->AddPage() ;

		// Field 1
		$pdf->setXY (112,17) ;
		$pdf->Cell(10, 4, ($Shipment['EFTAMember']) ? 'EU' : 'EX') ;
		$pdf->setXY (122,17) ;
		$pdf->Cell(10, 4, '1') ;

		// Field 2 - Export�r
		$pdf->setXY (64,13) ;
		$pdf->Cell(40, 4, $CompanyMy['VATNumber']) ;
		$pdf->SetMargins(25,0,0) ;
		$pdf->SetY (18) ;
		$pdf->Cell(80, 4, $CompanyMy['Name'], 0, 1) ;
		$pdf->Cell(80, 4, $CompanyMy['Address1'], 0, 1) ;
		if ($CompanyMy['Address2']) $pdf->Cell(80, 4, $CompanyMy['Address2'], 0, 1) ;
		$pdf->Cell(80, 4, sprintf ('%s %s', $CompanyMy['ZIP'], $CompanyMy['City']), 0, 1) ;
		$pdf->Cell(80, 4, $CompanyMy['CountryDescription'], 0, 1) ;

		// Field 3 - Formularer
		$pdf->setXY (112,26) ;
		$pdf->Cell(10, 4, sprintf ('%2d', $n)) ;
		$pdf->setXY (119.5,26) ;
		$pdf->Cell(10, 4, sprintf ('%2d', $FormCount)) ;

		// Field 5 - Vareposter
		$pdf->setXY (112,34) ;
		$pdf->Cell(15, 4, sprintf ('%5d', $FormCount)) ;  // $CustomsPositionsCount

		// Field 6 - Kolli i alt
		$pdf->setXY (132,34) ;
		$pdf->Cell(15, 4, sprintf ('%5d', (int)$Shipment['Count'])) ;

		// Field 8 - Modtager
		$pdf->SetMargins(25,0,0) ;
		$pdf->SetY (43) ;
		$pdf->Cell(80, 4, $Company['Name'], 0, 1) ;
		$pdf->Cell(80, 4, $Shipment['Address1'], 0, 1) ;
		if ($Shipment['Address2']) $pdf->Cell(80, 4, $Shipment['Address2'], 0, 1) ;
		$pdf->Cell(80, 4, sprintf ('%s %s', $Shipment['ZIP'], $Shipment['City']), 0, 1) ;
		$pdf->Cell(80, 4, $Shipment['CountryDescription'], 0, 1) ;

		// Field 14 - Representant
		$pdf->setXY (25,72) ;
		$pdf->Cell(80, 4, $Shipment['CarrierName']) ;

		// Field 17 - Bestemmelsesland
		$pdf->setXY (183,67) ;
		$pdf->Cell(20, 4, $Shipment['CountryName']) ;
		$pdf->setXY (160,76) ;
		$pdf->Cell(40, 4, substr($Shipment['CountryDescription'], 0, 16)) ;

		// Field 19 - Ctr.
		$pdf->setXY (106,85) ;
		$pdf->Cell(20, 4, '0') ;
		
		// Field 20 - Leveringsbetingelser
		$pdf->setXY (112,85) ;
		$pdf->Cell(10, 4, $Shipment['DeliveryTermName']) ;
		$pdf->setXY (123,85) ;
		$pdf->Cell(72, 4, $Shipment['DeliveryTermDescription']) ;

		// Field 22 - Fakturaens m�ntsort og bel�b
		$pdf->setXY (112,93) ;
		$pdf->Cell(10, 4, $Record['CurrencyName']) ;
		$pdf->setXY (123,93) ;
		$pdf->Cell(33, 4, number_format ((float)$Record['TotalAmount'], 2, ',', '.'), 0, 0, 'R') ;

		// Field 25 - Transportm�de ved gr�nsen
		$pdf->setXY (21,102) ;
		$pdf->Cell(10, 4, $Shipment['TransportBorder']) ;

		// Field 26 - Indenlandsk transportm�de
		$pdf->setXY (44,102) ;
		$pdf->Cell(10, 4, $Shipment['TransportLocal']) ;
		
		// Field 28 - Financielle oplysninger og bankdata
		$pdf->setXY (112,102) ;
		$pdf->Cell(90, 4, sprintf ('Invoice %d', (int)$Record['Number'])) ;
		$pdf->setXY (112,106) ;
		$pdf->Cell(90, 4, sprintf ('Date %s', date ('Y.m.d', dbDateDecode($Record['InvoiceDate'])))) ;

		// Field 29 - Udgangs - Indgangs toldsted
		$pdf->setXY (44,110) ;
		$pdf->Cell(10, 4, $Shipment['CustomsLocation']) ;

		// Field 31 - Kolli og varebeskrivelse
		$pdf->setXY (25,125) ;
		$pdf->Cell(90, 4, sprintf ('%d Kolli mrk. adr.', (int)$Shipment['Count'])) ;
		$pdf->setXY (25,129) ;
		$pdf->Cell(90, 4, $row['CustomsPositionDescription']) ;

		// Field 32 - Varepost nr.
		$pdf->setXY (122,119) ;
		$pdf->Cell(10, 4, sprintf ('%2d', $n)) ;

		// Field 33 - Varekode
		$pdf->setXY (135,119) ;
		$pdf->Cell(10, 4, $row['CustomsPositionName']) ;

		// Field 34 - Oprind. I. kode
		$pdf->setXY (137,127) ;
		$pdf->Cell(20, 4, $row['CountryName']) ;

		// Field 35 - Bruttomasse (kg)
		$pdf->setXY (162,127) ;
		$pdf->Cell(40, 4, sprintf ('%8d',  ((int)$form['Field35'] == 0) ? (int)$Shipment['GrossWeight'] : (int)$form['Field35'])) ;

		// Field 37 - Procedure
		$pdf->setXY (135,135) ;
		$pdf->Cell(10, 4, ($form['Field37'] == '') ? '1000 1' : $form['Field37']) ;

		// Field 38 - Bruttomasse (kg)
		$pdf->setXY (162,135) ;
		$pdf->Cell (40, 4, sprintf ('%8d', ((int)$form['Field38'] == 0) ? (float)$Shipment['GrossWeight'] - (float)$Shipment['TaraWeight'] : (int)$form['Field38'])) ;

		// Field 41 - Supplerende enheder
		$pdf->setXY (136,152) ;
		$pdf->Cell(21, 4, number_format (((float)$form['Field41'] == 0) ? (float)$row['Quantity'] : (float)$form['Field41'], (int)$row['UnitDecimals'], ',', ''), 0, 0, 'R') ;

		// Field 44 - Supplerende oplysninger
		$pdf->setXY (25,154) ;
		$pdf->MultiCell(105, 4, ($form['Field44'] == '') ? sprintf ('Ref.nr. %d', (int)$Record['Id']) : $form['Field44']) ;
		
		// Field 46 - Statisk v�rdi
		$pdf->setXY (166,169) ;
		$pdf->Cell(35, 4, number_format (($form['Field46'] == '') ? (float)$Record['TotalAmount']*(float)$Record['CurrRate']/100 : (float)$form['Field46'], 0, ',', '.'), 0, 0, 'R') ;

		// Field 49 - Identificering af oplag
		$pdf->setXY (158,178) ;
		$pdf->Cell(40, 4, $form['Field49']) ;

		// Field 54 - Sted og Dato
		$pdf->setXY (138,261) ;
		$pdf->Cell(65, 4, $CompanyMy['City'] . ' den ' . date ('Y.m.d', dbDateDecode($Record['ReadyDate']))) ;
		$pdf->setXY (138,270) ;
		$pdf->Cell(65, 4, $CompanyMy['Name']) ;
		$pdf->setXY (138,274) ;
		$pdf->Cell(65, 4, tableGetField('User', 'CONCAT(FirstName," ",LastName)', (int)$Record['ReadyUserId'])) ;

		if (!next($row['Form'])) break ; 

		$n++ ;
	    }
	}
    }
    
    // HTTP Header
    if (headers_sent()) return 'stop' ;
    httpNoCache ('pdf') ;

    // Generate PDF
    $pdf->Output() ;

    return 0 ;
?>
