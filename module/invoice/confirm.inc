<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    
    function flag (&$Record, $field, $name='') {
		if ($name=='') $name=$field ;
		if ($Record[$field]) {
			itemField ($name, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($name, formCheckbox ($field, $Record[$field])) ;
		}
    }
	
	// in cmd: nl2br($_POST['Body']) . '<br>'
	//'Please confirm the attached order confirmation from FUB to your sales agent.
	
	if ($Record['OrderTypeId']==10) { // B2C Webshop order
		if ($Record['Credit']== 1) {
			$BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="webcreditnotecreated"') ;
		} else {
			$BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="webinvoicecreated"') ;
		}
	} else { // B2B order
		if ($Record['Credit']== 1) {
			$BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="b2bcreditnotecreated"') ;
		} else {
			$BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="b2binvoicecreated"') ;
		}
	}

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Invoice', (int)$Record['Number']) ;
    itemField ('Customer', $Record['CompanyName']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemEnd () ;
  
    formStart () ;
    itemStart () ;
    itemHeader() ;
	
	if ($Record['Ready'])
			itemField ('Confirmed', sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record['ReadyDate'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['ReadyUserId']))) ;
	itemFieldRaw ($Record['Ready']?'':'Confirm', formCheckbox ('Ready', 1, '', $Record['Ready']?'Hidden':'')) ;
    itemSpace () ;
	$_mail = (isset($Record['Email']) and $Record['Email']!='')?$Record['Email']:$Record['Mail'] ;

    itemFieldRaw ('Email', formText ('Mail', $_mail, 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('BodyTxt', formtextArea ('BodyTxt', $BodyTxt, 'width:100%;height:200px;')) ;
    itemSpace () ;
    itemFieldRaw ('Part Delivery', formCheckbox ('CompletelyDone', $Record['PartDelivery'])) ;

    itemInfo ($Record) ;
    itemEnd () ;
    
    formEnd () ;
   
    return 0 ;
?>
