<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
    
    // Validate state of Invoice
    if (!$Record['Done']) return 'Invoice is not Done' ;
    if (!$Record['Ready']) return 'Invoice is not Ready' ;
    if ($Record['Credit']) return 'this is allready a Credit Note' ;

    // Generate the Incoice
    $Invoice = array (
		'Active'		=> 1,
		'CompanyId'		=> (int)$Record['CompanyId'],
		'FromCompanyId'		=> (int)$Record['FromCompanyId'],
		'Credit'		=> 1,
		'Description'		=> sprintf ('Credit Note for Invoice %d', (int)$Record['Number']),
		'CurrencyId'		=> (int)$Record['CurrencyId'],
		'SalesUserId'		=> (int)$Record['SalesUserId'],
		'InvoiceHeader'		=> sprintf ('Credit Note for Invoice %d', (int)$Record['Number']),
		'Email'		=> $Record['Email'],
		'InvoiceFooter'		=> $Record['InvoiceFooter']
    ) ;
    if ($Record['InvoiceHeader'] != '') $Invoice['InvoiceHeader'] .= "\n\n" . $Record['InvoiceHeader'] ;
    $Invoice['Id'] = tableWrite ('Invoice', $Invoice) ;
//logPrintVar ($Invoice, 'Invoice') ;

    // Loop all the Invoice Lines
//    foreach ($Line as $line) {
	foreach ($_POST['LineQuantity'] as $il_id => $IQty) {
		// Initialize the InvoiceLine
		$line = $Line[$il_id] ;
		unset ($InvoiceLine) ;
		$InvoiceLine = array (
			'Active'		=> 1,
			'InvoiceId'		=> $Invoice['Id'],
			'No'		    => (int)$line['No'],
			'Description'	=> $line['Description'],
			'InvoiceFooter'	=> $line['InvoiceFooter'],
			'ArticleId'		=> (int)$line['ArticleId'],
			'Quantity'		=> 0 , // $IQty > 0 ? $IQTY : 0 ; $line['Quantity'],
			'Discount'		=> (int)$line['Discount'],
			'PriceSale'		=> $line['PriceSale'],
			'PriceCost'		=> $line['PriceCost'],
			'OrderLineId'       => $line['OrderLineId']
		) ;
		if ($line['VariantColor']) $InvoiceLine['ArticleColorId'] = (int)$line['ArticleColorId'] ;
		if ($line['VariantCertificate']) $InvoiceLine['ArticleCertificateId'] = (int)$line['ArticleCertificateId'] ;
//if ($il_id == 62591) return 'her ' . $line['VariantSize'] ;
		// Set quantities if any
		if ($line['VariantSize']) {
			$InvoiceLine['Quantity'] = 0 ;
			foreach ($line['Size'] as $size) {
				if ((float)$_POST['SizeQuantity'][$il_id][$size['Id']] > 0) {
					$InvoiceLine['Quantity'] += (float)$_POST['SizeQuantity'][$il_id][$size['Id']] ;
				}
			}
			if ($InvoiceLine['Quantity']>0) {
				 $SomeQty = 1 ;				
			} else {
				continue ;
			}
		} else {
			if ($IQty > 0) {
				$InvoiceLine['Quantity'] = $IQty ;
			} else {
			    continue ;
			}
		}
//if ($il_id == 62591) return 'her ' . $line['VariantSize'] . " QTY " . $InvoiceLine['Quantity'] . " IQTY " . $IQty;
		
		$id = tableWrite ('InvoiceLine', $InvoiceLine) ;
		//logPrintVar ($InvoiceLine, 'InvoiceLine') ;

		// Generate sizes in invoicequantity table
		if ($line['VariantSize']) {
			$InvoiceLine['Quantity'] = 0 ;
			foreach ($line['Size'] as $size) {
				if ((float)$_POST['SizeQuantity'][$il_id][$size['Id']] > 0) {
					$InvoiceQuantity = array (
						'Active'	=> 1,
						'InvoiceLineId'	=> $id,
						'ArticleSizeId'	=> (int)$size['Id'],
						'Quantity'	=> (float)$_POST['SizeQuantity'][$il_id][$size['Id']]
					) ;
					tableWrite ('InvoiceQuantity', $InvoiceQuantity) ;
				}
			}
		} 
    }

    // View
    return navigationCommandMark ('invoiceview', (int)$Invoice['Id']) ;  
?>
