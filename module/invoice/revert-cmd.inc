<?php

    require_once 'lib/file.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    
    $fields = array (
	'Done'			=> array ('type' => 'checkbox',	'check' => true),
	'Ready'			=> array ('type' => 'checkbox',	'check' => true),
    ) ;

    $flaglist = '' ;
    $flagcount = 0 ;
    
    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	global $flagcount, $flaglist ;
	switch ($fieldname) {
	    case 'Ready' :
		if (!$changed or $value) return false ;
		if ($Record['Done']) return 'Ready can not be reverted when still Done' ;
		break ;
	}
	
	if (!$changed or $value) return false ;
	$flaglist .= sprintf ("        %s\n", $fieldname) ;
	$flagcount++ ;
	return true ;
    }
    
    $res = saveFields ('Invoice', $Id, $fields, true) ;
    if ($res) return $res ;
    
    // Remove stored pdf-document
    if (!$fields['Ready']['value'] and $fields['Ready']['db']) {
	$file = fileName ('invoice', (int)$Record['Id']) ;
	if (is_file($file)) unlink ($file) ;
    }
    
    // Generate news
    if ($flagcount > 0) {
	unset ($row) ;
	$row['Header'] = sprintf ("Invoice %d (%s) reverted", $Record['Number'], $Record['Description']) ;
	$row['Text'] = sprintf ("The flag%s:\n\n%s\nhas been reverted by %s (%s) at %s", ($flagcount > 1) ? 's' : '', $flaglist, $User['FullName'], $User['Loginname'], date('Y-m-d H:i:s', dbDateDecode($Record['ModifyDate']))) ;
	tableWrite ('News', $row) ;
    }

    return 0 ;
	
?>

