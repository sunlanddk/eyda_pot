<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
    
    // Validate state of Invoice
    if (!$Record['Done']) return 'Invoice is not Done' ;
    if (!$Record['Ready']) return 'Invoice is not Ready' ;
    if ($Record['Credit']) return 'this is allready a Credit Note' ;

	// Reset StockId to enable new Invoice from same shipment.
	$Invoice['StockId'] = 0 ;
	tableWrite ('Invoice', $Invoice, $Record['Id']) ;
	
	
    // Generate the Incoice
    $Invoice = array (
		'Active'			=> 1,
		'CompanyId'			=> (int)$Record['CompanyId'],
		'FromCompanyId'		=> (int)$Record['FromCompanyId'],
		'Credit'			=> 1,
		'Description'		=> sprintf ('Credit Note for Invoice %d', (int)$Record['Number']),
		'CurrencyId'		=> (int)$Record['CurrencyId'],
		'SalesUserId'		=> (int)$Record['SalesUserId'],
		'InvoiceHeader'		=> sprintf ('Credit Note for Invoice %d', (int)$Record['Number']),
		'Email'		=> $Record['Email'],
		'InvoiceFooter'		=> $Record['InvoiceFooter']
    ) ;
    if ($Record['InvoiceHeader'] != '') $Invoice['InvoiceHeader'] .= "\n\n" . $Record['InvoiceHeader'] ;
    $Invoice['Id'] = tableWrite ('Invoice', $Invoice) ;
//logPrintVar ($Invoice, 'Invoice') ;

    // Loop all the Invoice Lines
    foreach ($Line as $line) {
		// Generate the InvoiceLine
		$InvoiceLine = array (
			'Active'		=> 1,
			'InvoiceId'		=> $Invoice['Id'],
			'No'			=> (int)$line['No'],
			'Description'	=> $line['Description'],
			'InvoiceFooter'	=> $line['InvoiceFooter'],
			'ArticleId'		=> (int)$line['ArticleId'],
			'Quantity'		=> $line['Quantity'],
			'Discount'		=> (int)$line['Discount'],
			'PriceSale'		=> $line['PriceSale'],
			'PriceCost'		=> $line['PriceCost'],
			'OrderLineId'   => $line['OrderLineId']
		) ;
		if ($line['VariantColor']) $InvoiceLine['ArticleColorId'] = (int)$line['ArticleColorId'] ;
		if ($line['VariantCertificate']) $InvoiceLine['ArticleCertificateId'] = (int)$line['ArticleCertificateId'] ;
		$id = tableWrite ('InvoiceLine', $InvoiceLine) ;
	//logPrintVar ($InvoiceLine, 'InvoiceLine') ;

		// Generate sizes
		if ($line['VariantSize']) {
			foreach ($line['Size'] as $size) {
			if ((float)$size['Quantity'] > 0) {
				$InvoiceQuantity = array (
				'Active'	=> 1,
				'InvoiceLineId'	=> $id,
				'ArticleSizeId'	=> (int)$size['Id'],
				'Quantity'	=> $size['Quantity']
				) ;
			
				tableWrite ('InvoiceQuantity', $InvoiceQuantity) ;
	//logPrintVar ($InvoiceQuantity, 'InvoiceQuantity') ;
			}
			}
		}
    }

    // View
    return navigationCommandMark ('invoiceview', (int)$Invoice['Id']) ;  
?>
