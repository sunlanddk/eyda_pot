<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;

    $fields = array (
	'No'			=> array ('type' => 'integer',				'check' => true),

	'Description'		=> array (),

	'ArticleColorId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'PriceSale'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.4'),
	'Discount'		=> array ('type' => 'integer',	'mandatory' => true),
	
	'Quantity'		=> array ('type' => 'decimal',				'check' => true),
	
	'CustArtRef'		=> array (),
	'InvoiceFooter'		=> array ()
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Size, $Id, $fields ;
	switch ($fieldname) {
	    case 'No' :
		if (!$changed) return false ;

		// Validate number
		$query = sprintf ('SELECT Id FROM InvoiceLine WHERE InvoiceId=%d AND Active=1 AND Id<>%d', (int)$Record['InvoiceId'], (int)$Record['Id']) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($value <= 0 or $value > ($count+1)) return sprintf ('invalid InvoiceLine Position number, max %d', $count+1) ;

		return true ;

	    case 'ArticleColorId' :
		if (!$changed) return false ;

		// Validate number
		$query = sprintf ('SELECT * FROM ArticleColor WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] <= 0) return sprintf ('%s(%d) not found, id %d', __FILE__, __LINE__, $value) ;
		if ((int)$row['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) ArticleColor mismatch, id %d', __FILE__, __LINE__) ;

		return true ;
		
	    case 'Quantity' :
		if (!$changed) return false ;
		
		// Validate
		if ($value <= 0) return 'invalud Quantity' ;

		return true ;
	}
	
	return false ;	
    }
    
    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    // Validate state of Invoice
    if ($Record['InvoiceDone']) return 'the InvoiceLine can not be modified when Invoice is Done' ;
    if ($Record['InvoiceReady']) return 'the InvoiceLine can not be modified when Invoice is Ready' ;
 
    if (!$Record['VariantColor']) {
	unset ($fields['ArticleColorId']) ;
    }
    
if (false) {
    if ($Record['VariantSize']) {
	// Get and validate Quantities by Sizes
	$Quantity = 0 ;
	foreach ($Size as $i => $s) {
	    $value_string = trim($_POST['Quantity'][(int)$s['Id']]) ;
	    if ($value_string != '0') $value_string = ltrim($value_string, '0') ;
	    $value = (int)$value_string ;
	    if ($value_string != (string)$value) return sprintf ("invalid quantiry for %s", $s['Name']) ;
	    $Size[$i]['Value'] = $value ;
	    $Quantity += $value ;
	}

	// Set total quantity for invoiceline
	$fields['Quantity'] = array ('type' => 'set', 'value' => $Quantity) ;
    }
}

    if ($Record['VariantSize']) {
	// Generat verify format
	$format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", 9-(int)$Record['UnitDecimals'], 9-(int)$Record['UnitDecimals'], (int)$Record['UnitDecimals']) ;

	$Quantity = 0 ;
	foreach ($Size as $i => $s) {
	    $value_string = trim($_POST['Quantity'][(int)$s['Id']]) ;
	    if ($value_string == '') $value_string = '0' ;

	    // verify format
	    if (!ereg($format,$value_string)) return sprintf ("invalid quantity for %s", $s['Name']) ;
	    
	    // Substitute , -> .
	    $value_string = str_replace (',', '.', $value_string) ;
	    
	    $Size[$i]['Value'] = $value_string ;
	    $Quantity += (float)$value_string ;
	}

	// Set total quantity for invoiceline
	$fields['Quantity'] = array ('type' => 'set', 'value' => number_format ($Quantity, (int)$Record['UnitDecimals'], '.', '')) ;
    }
        
    // Process and save InvoiceLine
    $res = saveFields ('InvoiceLine', $Id, $fields, true) ;
    if ($res) return $res ;

if (false) {
    if ($Record['VariantSize']) {
	// Update Quantities by Sizes
	foreach ($Size as $i => $s) {
	    if ((int)$s['Quantity'] != (int)$s['Value']) {
		if ((int)$s['InvoiceQuantityId'] > 0) {
		    // Update existing record
		    $fields = array (
			'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
		    ) ;
		    $res = saveFields ('InvoiceQuantity', (int)$s['InvoiceQuantityId'], $fields) ;
		} else {
		    // Create new record
		    $fields = array (
			'InvoiceLineId'		=> array ('type' => 'set',	'value' => (int)$Record['Id']),
			'ArticleSizeId'		=> array ('type' => 'set',	'value' => (int)$s['Id']),
			'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
		    ) ;
		    $res = saveFields ('InvoiceQuantity', -1, $fields) ;
		}
		if ($res) return $res ;
	    }			    
	}
    }
}

    if ($Record['VariantSize']) {
	// Update Quantities by Sizes
	foreach ($Size as $i => $s) {
	    if ((float)$s['Quantity'] != (float)$s['Value']) {
		if ((int)$s['InvoiceQuantityId'] > 0) {
		    if ((float)$s['Value'] > 0) {
			// Update existing record
			$fields = array (
			    'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
			) ;
			$res = saveFields ('InvoiceQuantity', (int)$s['InvoiceQuantityId'], $fields) ;
		    } else {
			// Delete record
			tableDelete ('InvoiceQuantity', (int)$s['InvoiceQuantityId']) ;
		    }
		} else {
		    // Create new record
		    $fields = array (
			'InvoiceLineId'		=> array ('type' => 'set',	'value' => (int)$Record['Id']),
			'ArticleSizeId'		=> array ('type' => 'set',	'value' => (int)$s['Id']),
			'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
		    ) ;
		    $res = saveFields ('InvoiceQuantity', -1, $fields) ;
		}
		if ($res) return $res ;
	    }			    
	}
    }

    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM InvoiceLine WHERE InvoiceId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['InvoiceId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i == (int)$Record['No']) $i += 1 ;       
	if ($i != (int)$row['No']) {
	    $query = sprintf ("UPDATE InvoiceLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;
    
    return 0 ;    
?>
