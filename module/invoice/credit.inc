<?php
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;   
/*    
    itemStart() ;
    itemSpace () ;
    itemFieldRaw ('Credit Complete Invoice', formCheckbox ('CompleteCredit', 0)) ;
    itemSpace () ;
    itemEnd() ;
*/
    // List
    listClear () ;
    listStart () ;
    listRow () ;
 
    listHead ('Pos', 15) ;
    listHead ('Article', 35)  ;
    listHead ('Description', 80)  ;
    listHead ('Color', 25) ;
    listHead ('Description', 50)  ;
    listHead ('Size', 35) ;
    listHead ('Credit Qty', 50) ;
    listHead ('Invoice Qty', 35, 'align=right') ;
    listHead ('Unit', 15) ;
    listHead ('Return items', 35, 'align=right') ;

    // Loop all the Invoice Lines
    foreach ($Line as $line) {
   	listRow () ;
    	listField ($line['No']) ;
    	listField ($line['ArticleNumber']) ;
    	listField ($line['ArticleDescription']) ;
    	listField ($line['VariantColor']?$line['ColorNumber']:'') ;
    	listField ($line['VariantColor']?$line['ColorDescription']:'') ;

    	if ($line['VariantSize']) {
        $first = 1 ;
  	    foreach ($line['Size'] as $size) {
         if ($first) {
            $first = 0 ;
            listField ('') ;
            listField ('') ;
            ListFieldRaw (formHidden(sprintf('LineQuantity[%d]', (int)$line['Id']), $line['Quantity'], 8,'','')) ;
            listField (sprintf('%s',$line['UnitName'])) ;
          }
         	listRow () ;
          listField ('') ;
          listField ('') ;
          listField ('') ;
          listField ('') ;
          listField ('') ;
          listField ($size['Name']) ;
          if ((float)$size['Quantity'] > 0) {
      	    // listFieldRaw (formText (sprintf('SizeQuantity[%d]', (int)$size['Id']), number_format ((float)$size['Quantity'], (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:70px;margin-right:0;'), 'align=right') ;
      	    listFieldRaw (formText (sprintf('SizeQuantity[%d][%d]', (int)$line['Id'], (int)$size['Id']), number_format (0, (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:70px;margin-right:0;'), 'align=right') ;
          } else {
            listField ('') ;
          }
          listField (number_format((float)$size['Quantity'], (int)$size['UnitDecimals'], ',', '.'), 'align=right') ;
          if ($first) {
            $first = 0 ;
            listField (sprintf('%s',$line['UnitName'])) ;
          } else {
            listField ('') ;
          }
          if ((float)$size['Quantity'] > 0) {
//            listFieldRaw (formCheckbox (sprintf('Item[%d]', (int)$row['Id']), 0, 'margin:0;padding:0;'), 'align=right') ;
            listField ('') ;
          } else {
            listField ('') ;
          }

        }
      } else {
        listField ('---') ;
//   	    listFieldRaw (formText (sprintf('LineQuantity[%d]', (int)$line['Id']), number_format ((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:60px;margin-right:0;'), 'align=right') ;
   	    listFieldRaw (formText (sprintf('LineQuantity[%d]', (int)$line['Id']), number_format (0, (int)$line['UnitDecimals'], ',', ''), 7, 'text-align:right;width:70px;margin-right:0;'), 'align=right') ;
        listField (number_format((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', '.'), 'align=right') ;
        listField (sprintf('%s',$line['UnitName'])) ;
        if ((float)$size['Quantity'] > 0) {
          listFieldRaw (formCheckbox (sprintf('Item[%d]', (int)$row['Id']), 0, 'margin:0;padding:0;'), 'align=right') ;
        } else {
          listField ('') ;
        }
      }
    }
    if (0) {
	    listRow () ;
	    listField () ;
	    listField ('No Invoicelines', 'colspan=4') ;
    }
    listEnd () ;
    formEnd () ;
    return 0 ;
?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
