<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'InvoiceId'		=> array ('type' => 'set',	'value' => (int)$Record['InvoiceId']),
	'CustomsPositionId'	=> array ('type' => 'set',	'value' => (int)$Record['CustomsPositionId']),
	'CountryId'		=> array ('type' => 'set',	'value' => (int)$Record['CountryId']),

	'Field35'		=> array ('type' => 'integer'),
	'Field37'		=> array (),
	'Field38'		=> array ('type' => 'integer'),
	'Field41'		=> array ('type' => 'integer'),
	'Field44'		=> array (),
	'Field46'		=> array ('type' => 'decimal',	'format' => '12.2'),
    	'Field49'		=> array ()
    ) ;

    if ($Record['InvoiceDone']) return 'the Customs Forms can not be modified when Invoice is Done' ;
     
    return saveFields ('CustomsFormA12', -1, $fields) ;
?>
