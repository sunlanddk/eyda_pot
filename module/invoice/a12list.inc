<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Number', ($Record['Ready']) ? (int)$Record['Number'] : 'Draft') ;
    itemField ('Description', $Record['Description']) ;     
    itemField ('Company', sprintf ('%s (%s)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemSpace () ;
    itemEnd () ;

//require_once 'lib/log.inc' ;
//logPrintVar ($Pos, 'CustomsPositions') ;
//return 0 ;
    
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Position', 70) ;
    listHead ('Description') ;
    listHead ('Work Country', 90) ;
    listHead ('Quantity', 85, 'align=right') ;
    listHead ('', 20) ;
    listHead ('Procedure', 75) ;
    listHead ('Oplag', 75) ;
    listHead ('Info', 75) ;
    listHead ('Gross (Kg)', 75, 'align=right') ;
    listHead ('Net (Kg)', 75, 'align=right') ;
    listHead ('Quantity', 85, 'align=right') ;
    listHead ('Value (Kr)', 75, 'align=right') ;
    listHead ('', 8) ;

    foreach ($Form as $Country) {
	foreach ($Country as $row) {
	
	    listRow () ;
	    listFieldIcon ($Navigation['Icon'], 'a12edit', (int)$row['Form'][0]['Id'], sprintf('i=%d&p=%d&c=%d', (int)$Record['Id'], (int)$row['CustomsPositionId'], (int)$row['CountryId'])) ;
	    listField ($row['CustomsPositionName']) ;
	    listField ($row['CustomsPositionDescription']) ;
	    listField (sprintf ('%s (%s)', $row['CountryDescription'], $row['CountryName'])) ;
	    listField (number_format((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;

	    foreach ($row['Form'] as $i => $form) {

		if ($i > 0) {
		    listRow () ;
		    listFieldIcon ($Navigation['Icon'], 'a12edit', (int)$form['Id']) ;
		    listField ('', 'colspan=4') ;
		}

		listField () ;
		listField ($form['Field37']) ;
		listField ($form['Field49']) ;
		listField ($form['Field44']) ;
		listField ((int)$form['Field35'], 'align=right') ;
		listField ((int)$form['Field38'], 'align=right') ;
		listField (number_format ((float)$form['Field41'], (int)$row['UnitDecimals'], ',', '.') . ' ' . $row['UnitName'], 'align=right') ;
		listField ((int)$form['Field46'], 'align=right') ;
	    }
	}
    }
    if (count($Form) == 0) {
	listRow () ;
	listField () ;
	listField ('No Customs Positions', 'colspan=3') ;
    }
    listEnd () ;

    return 0 ;
?>
