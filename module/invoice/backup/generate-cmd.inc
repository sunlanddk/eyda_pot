<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
    
    // Validate Shipment
    if (!$Record['Ready']) return 'Shipment is not Ready' ;
    if ($Record['Type'] != 'shipment') 'selected Stock must be a shipment' ;

    // Check if Invoices allready has been made for this Shipment
    $query = sprintf ('SELECT Id FROM Invoice WHERE StockId=%d AND Active=1', (int)$Record['Id']) ;
    $res = dbQuery ($query) ;
    $count = dbNumRows ($res) ;
    dbQueryFree ($res) ;
    if ($count > 0) return 'Invoice(s) has allready been generated for this Shipment' ;
    
    // Get Company information
    $query = sprintf ('SELECT * FROM Company WHERE Id=%d', (int)$Record['CompanyId']) ;
    $res = dbQuery ($query) ;
    $Company = dbFetch ($res) ;
    dbQueryFree ($res) ;
//logPrintVar ($Company, 'Company') ;
   
    // Valiables
    $Part = 0 ;
    $OrderId = 0 ;
    $Invoice = array () ;
    $InvoiceLine = array () ;
    $InvoiceQuantity = array () ;
    $Quantity = (float)0 ;   

	//TMP: No invoices if prices is 0!
    $query = sprintf('
		select sum(PriceSale) as PriceSale 
		from
			(select sum(PriceSale) as PriceSale
			    from container
				INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
				LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
 				WHERE Container.StockId=%d AND Container.Active=1
			union
			SELECT sum(PriceSale) as PriceSale
				FROM `Order`
			    JOIN (select ol.orderid as orderid from item i, container c, orderline ol where i.containerid=c.id and c.stockid=%d and i.orderlineid=ol.id group by ol.orderid) Orders ON `Order`.Id=Orders.OrderId
				JOIN Orderline On `Order`.Id=OrderLine.OrderId
				JOIN Article ON Article.Id=Orderline.ArticleId
			WHERE article.articletypeid=6 and orderline.done=0) Prices
		',  (int)$Record['Id'], (int)$Record['Id']) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    if ($row['PriceSale'] > 0) 
	$Price_to_be_invoiced = 1 ;
    else
	return 'No invoice generated since Price is 0' ;


    // Generate Query for Item Quantities in Shipment
/*
    $query =   'SELECT
		Order.Id AS OrderId, Order.CurrencyId, Order.PaymentTermId, Order.SalesUserId,  
		OrderLine.Id AS OrderLineId, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity
		FROM Container
		INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
		INNER JOIN Article ON Article.Id=Item.ArticleId
		LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId' ;
    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$Record['Id']) ;
    $query .= ' GROUP BY OrderLine.Id, Item.ArticleId, Item.ArticleColorId*Article.VariantColor, Item.ArticleSizeId*Article.VariantSize, Item.ArticleCertificateId*Article.VariantCertificate
		ORDER BY OrderLine.OrderId, OrderLine.No, Article.Number' ;
*/
    $query =   '(SELECT
		Order.Id AS OrderId, Order.CurrencyId, Order.PaymentTermId, Order.SalesUserId,  
		OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		Item.ArticleColorId, Item.ArticleSizeId, Item.ArticleCertificateId, SUM(Item.Quantity) AS Quantity
		FROM Container
		INNER JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
		INNER JOIN Article ON Article.Id=Item.ArticleId
		LEFT JOIN OrderLine ON OrderLine.Id=Item.OrderLineId
		LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId' ;
    $query .= sprintf (' WHERE Container.StockId=%d AND Container.Active=1', (int)$Record['Id']) ;
    $query .= ' GROUP BY OrderLine.Id, Item.ArticleId, Item.ArticleColorId*Article.VariantColor, Item.ArticleSizeId*Article.VariantSize, Item.ArticleCertificateId*Article.VariantCertificate' ;

    $query .=   sprintf ( ') union (SELECT
		`Order`.Id AS OrderId, `Order`.CurrencyId, `Order`.PaymentTermId, `Order`.SalesUserId,
		OrderLine.Id AS OrderLineId, `OrderLine`.`No` AS OrderLineNo, OrderLine.PriceSale, OrderLine.Discount, OrderLine.Description, OrderLine.InvoiceFooter, OrderLine.CustArtRef,
		Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.VariantColor, Article.VariantSize, Article.VariantSortation, Article.VariantCertificate,
		0 as ArticleColorId, 0 as ArticleSizeId, 0 as ArticleCertificateId, OrderLine.Quantity AS Quantity
	FROM `Order`
    JOIN (select ol.orderid as orderid from item i, container c, orderline ol where i.containerid=c.id and c.stockid=%d and i.orderlineid=ol.id group by ol.orderid) Orders ON `Order`.Id=Orders.OrderId
	JOIN Orderline On `Order`.Id=OrderLine.OrderId
	JOIN Article ON Article.Id=Orderline.ArticleId
    WHERE article.articletypeid=6 and orderline.done=0 and orderline.active=1
     GROUP BY order.id, Orderline.id) ORDER BY OrderId, OrderLineNo, ArticleNumber', (int)$Record['Id']) ;

    $res = dbQuery ($query) ;
    while ($row = dbFetch ($res)) {
	// Check if Order associated the Item
	if ((int)$row['OrderId'] == 0) {
	    // No Order
	    // Fill in default information from Company
	    $row['CurrencyId'] = $Company['CurrencyId'] ;
	    $row['PaymentTermId'] = $Company['PaymentTermId'] ;
	    $row['SalesUserId'] = $Company['SalesUserId'] ;
	    $row['CompanyFooter'] = $Company['CompanyFooter'] ;
	    $row['Description'] = $row['ArticleDescription'] ;
	}

//logPrintVar ($row, 'row') ;

	// New Invoice ?
	if ((int)$Invoice['Id'] == 0 or
	    ($Company['InvoiceSplit'] and ((int)$row['OrderId'] != $OrderId)) or
	    $Invoice['CurrencyId'] != (int)$row['CurrencyId'] or
	    $Invoice['PaymentTermId'] != (int)$row['PaymentTermId'] or
	    $Invoice['SalesUserId'] != (int)$row['SalesUserId']				) {
		
	    // Generate the Invoice
	    $Invoice = array (
		'Active'		=> 1,
		'CompanyId'		=> (int)$Company['Id'],
		'FromCompanyId'		=> (int)$Record['FromCompanyId'],
		'StockId'		=> (int)$Record['Id'],
		'Credit'		=> 0,
		'Description'		=> sprintf ('Generated from Shipment, part %d', ++$Part),
		'CurrencyId'		=> (int)$row['CurrencyId'],
		'PaymentTermId'		=> (int)$row['PaymentTermId'],
		'SalesUserId'		=> (int)$row['SalesUserId'],
		'CompanyFooter'		=> sprintf ("%s", $Company['CompanyFooter']),
	    ) ;

	    $Invoice['Id'] = tableWrite ('Invoice', $Invoice) ;
//$Invoice['Id'] = 9999 ;
//logPrintVar ($Invoice, 'Invoice') ;
	    
	    $No = 0 ;
	    $OrderId = (int)$row['OrderId'] ;
	}

	// New Invoice Line ?
	if ($InvoiceLine['Id'] == 0 or
	    $InvoiceLine['InvoiceId'] != $Invoice['Id'] or
	    $InvoiceLine['OrderLineId'] != (int)$row['OrderLineId'] or
	    $InvoiceLine['ArticleId'] != (int)$row['ArticleId'] or
	    !$row['VariantSize'] or
	    ($row['VariantColor'] and ($InvoiceLine['ArticleColorId'] != (int)$row['ArticleColorId'])) or
	    ($row['VariantCertificate'] and ($InvoiceLine['ArticleCertificateId'] != (int)$row['ArticleCertificateId']))  ) {

	    // Update Invoice with correct Quantity
	    if ($InvoiceLine['Id'] > 0 and $Quantity > $InvoiceLine['Quantity']) {
		$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f WHERE Id=%d', $Quantity, $InvoiceLine['Id']) ;
		dbQuery ($query) ;
//printf ("%s<br>\n", $query) ;
	    }
		
	    // Generate the InvoiceLine
	    $InvoiceLine = array (
		'Active'		=> 1,
		'InvoiceId'		=> $Invoice['Id'],
		'OrderLineId'		=> (int)$row['OrderLineId'],
		'No'			=> ++$No,
		'Description'		=> $row['Description'],
		'InvoiceFooter'		=> $row['InvoiceFooter'],
		'CustArtRef'		=> $row['CustArtRef'],
		'ArticleId'		=> (int)$row['ArticleId'],
		'Quantity'		=> $row['Quantity'],
		'Discount'		=> (int)$row['Discount'],
		'PriceSale'		=> (float)$row['PriceSale']
	    ) ;
	    if ($row['VariantColor']) $InvoiceLine['ArticleColorId'] = (int)$row['ArticleColorId'] ;
	    if ($row['VariantCertificate']) $InvoiceLine['ArticleCertificateId'] = (int)$row['ArticleCertificateId'] ;

	    $InvoiceLine['Id'] = tableWrite ('InvoiceLine', $InvoiceLine) ;
//$InvoiceLine['Id'] = 9999 ;
//logPrintVar ($InvoiceLine, 'InvoiceLine') ;

	    $Quantity = (float)0 ;   
	}

	// Size Variants
	if ($row['VariantSize']) {
	    $InvoiceQuantity = array (
		'Active'		=> 1,
		'InvoiceLineId'		=> $InvoiceLine['Id'],
		'ArticleSizeId'		=> (int)$row['ArticleSizeId'],
		'Quantity'		=> $row['Quantity']
	    ) ;
		
	    tableWrite ('InvoiceQuantity', $InvoiceQuantity) ;
//logPrintVar ($InvoiceQuantity, 'InvoiceQuantity') ;

	    $Quantity += (float)$row['Quantity'] ;
	}
	
    }
    dbQueryFree ($res) ;

    // Update Invoice with correct Quantity
    if ($InvoiceLine['Id'] > 0 and $Quantity > (float)$InvoiceLine['Quantity']) {
	$query = sprintf ('UPDATE InvoiceLine SET Quantity=%.03f WHERE Id=%d', $Quantity, $InvoiceLine['Id']) ;
	dbQuery ($query) ;
//printf ("%s<br>\n", $query) ;
    }
		
   //Update shipment invoice flag
	if ($Record['Departed']) {
		$Stock = array (
	    'Invoiced' => (int)1,
	    'InvoicedUserId' => $User['Id'],
	    'InvoicedDate' => dbDateEncode(time()),
	    'Done' => (int)1,
	    'DoneUserId' => $User['Id'],
	    'DoneDate' => dbDateEncode(time())
	    ) ;
	} else {
		$Stock = array (
	    'Invoiced' => (int)1,
	    'InvoicedUserId' => $User['Id'],
	    'InvoicedDate' => dbDateEncode(time())
	    ) ;
	}
	tableWrite ('Stock', $Stock, $Record['Id']) ;

    switch ($Part) {
	case 0 :
	    return 'no Invoices generated' ;

	case 1 :
	    return navigationCommandMark ('invoiceview', (int)$Invoice['Id']) ;

	default :
	    return navigationCommandMark ('companyinvoices', (int)$Company['Id']) ;
    }
?>
