<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemHeader () ;
    	if ($User['Extern']) 
		itemField('Customer', $Record['CompanyNumber']) ;
	else
		itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
    itemField ('Name', $Record['CompanyName']) ;     
 
    if ((int)$Record['OrderLineId'] > 0) {
	itemSpace () ;
	itemFieldIcon ('Order', (int)$Record['OrderId'], 'order.png', 'orderview', (int)$Record['OrderId']) ;
	itemField ('Line', (int)$Record['OrderLineNo']) ;
    }	

    itemSpace () ;
	if ($User['Extern']) 
    itemField('Article', $Record['ArticleNumber']) ;     
	else
    itemFieldIcon ('Article', $Record['ArticleNumber'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;     
    itemField ('Description', $Record['ArticleDescription']) ;     
    if ((int)$Record['CaseId'] > 0) {
	itemSpace () ;
	itemFieldIcon ('Case', (int)$Record['CaseId'], 'case.gif', 'caseview', (int)$Record['CaseId']) ;     
    }
    
    itemSpace () ;
    itemField ('Pos', (int)$Record['No']) ;
    itemField ('Description', $Record['Description']) ;

    if ($Record['VariantColor']) {
	$field = '' ;
	if ((int)$Color['Id'] > 0) {
	    $field .= sprintf ('<p class=list style="display:inline;">%s (%s)</p>', $Color['ColorNumber'], $Color['ColorDescription']) ;
	    if ((int)$Color['ColorGroupId'] > 0) {
		$field .= sprintf ('<div style="width:80px;height:15px;margin-left:8px;margin-top:1px;background:#%02x%02x%02x;display:inline;"></div>', $Color['ValueRed'], $Color['ValueGreen'], $Color['ValueBlue']) ;
	    }
	}
	itemSpace () ;
	itemFieldRaw ('Colour', $field) ;
    }
    itemSpace () ;

    if ($Record['VariantSize']) {
	$field = '<table><tr>' ;
	foreach ($Size as $i => $s) $field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : ' width=60') . '>' . htmlentities($s['Name']) . '</td>' ;
	$field .= '<td width=60></td>' ;
	$field .= '</tr>' ;
	$field .= '<tr>' ;
	foreach ($Size as $i => $s) $field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : '') . '>' . number_format ((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
	$field .= '<td style="padding-left=8px">' . htmlentities($Record['UnitName']) . '</td>' ;
	$field .= '</tr></table>' ;	
	itemFieldRaw ('Quantity', $field) ; 
	itemSpace () ;
	itemField ('Quantity Total', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ;
    } else {
	itemField ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ;
    }

    itemSpace () ;
    itemField ('Price', number_format ((float)$Record['PriceSale'], 2, ',', '.') . ' ' . $Record['CurrencySymbol']) ;
    itemField ('Discount', (int)$Record['Discount'] . ' %') ;

    itemSpace () ;
	if (!$User['Extern']) 
		itemFieldText ('CustArtRef', $Record['CustArtRef']) ;
    if (!$User['Extern']) 
		itemFieldText ('LineFooter', $Record['InvoiceFooter']) ;

	if (!$User['Extern']) 
		itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
