<?php

    require_once "lib/list.inc" ;

    // Article header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Supply to', 120) ;
    listHead (sprintf ('Quantity (%s)', $Record['UnitName']), 100, 'align=right') ;
//    listHead ('', 80, 'align=right') ;
    listHead ('Purchase Price', 120, 'align=right') ;
    listHead ('Logstic Cost', 120) ;
    listHead ('Supplier', 120) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'articlepriceedit', $row['Id']) ;
	listField ($row['OwnerName']) ;
	listField (number_format ((float)$row['QuantityStart'], (int)$Record['UnitDecimals'], ',', '.'), 'align=right') ;
//	listField (number_format ((float)$row['QuantityEnd'], (int)$Record['UnitDecimals'], ',', '.'), 'align=right') ;
	listField ($row['Purchase'], 'align=right') ;
	listField ($row['Logistic'], 'align=right') ;
	listField ($row['SupplierName']) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row['ModifyDate']))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Prices', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
