<?php

    require_once 'lib/save.inc' ;
    
    $fields = array (
	'QuantityStart'		=> array ('type' => 'decimal',	'mandatory' => true,	'check' => true),
	'Purchase'		=> array ('mandatory' => true,	'type' => 'decimal',	'format' => '9.4'),
	'Logistic'		=> array ('mandatory' => true,	'type' => 'decimal',	'format' => '9.4'),
	'SupplierId'		=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
	'OwnerId'		=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
	'Comment'		=> array ()
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $fields ;
	switch ($fieldname) {
	    case 'QuantityStart':
			if (!$changed) return false ;

			// Validate quantity
			if ($value <= 0) return 'invalid Quantity ' .  $value ;
					  
			return true ;
	    case 'SupplierId':
			if (!$changed) return false ;

			// Validate 
			if ($value <= 0) return 'Please select a supplier' ;
					  
			return true ;
		case 'OwnerId':
			if (!$changed) return false ;

			// Validate quantity
			if ($value <= 0) return 'Please select who to supply to' ;

			if (tableGetFieldWhere('articleprice','Id',sprintf('OwnerId=%d and articleid=%d and active=1',(int)$value,$Id)) > 0)
				return 'Supplier for Owner already set' ; 
					  
			return true ;
	}
	return false ;
    }

    // Update field list
    $fields['QuantityStart']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['ArticleId'] = array ('type' => 'set', 'value' => $Id) ;

	    unset ($Record) ;
	    $Record['ArticleId'] = $Id ;

	    $Id = -1 ;
	    break ;
    }

    $rus = saveFields ('ArticlePrice', $Id, $fields, true) ;
    if ($rus) return $rus ;
 
	return 0 ;

	// Auto update other prices according tradeconfig
	$query = sprintf('SELECT * FROM tradeconfig WHERE SupplierId=%d and active=1', $_POST['OwnerId'] ) ;
	$res_tradeconfig = dbQuery ($query) ;
	$tc_row = dbFetch ($res_tradeconfig) ;
	if ($tc_row['id']>0) { 
		$ArticlePriceId = (int)tableGetFieldWhere('ArticlePrice','Id',sprintf('OwnerId=%d and articleid=%d and active=1', $_POST['OwnerId'], $Record['ArticleId'])) ;
		if ($tc_row['LogisticFixed']>0)
			$ArticlePrice['Logistic'] = (float)$tc_row['LogisticFixed'] ;
		else
			$ArticlePrice['Logistic'] = (float)$tc_row['LogisticPct']*$_POST['Purchase']/100 ; 
		$ArticlePrice['Purchase'] = (float)(100-$tc_row['GpPct'])*($_POST['Purchase']-$LogisticCost)/100 ; 
		$ArticlePrice['SupplierId'] = $tc_row['SupplierId'] ;
		$ArticlePrice['OwnerId'] = $tc_row['CustomerId'] ;
		tableWrite('ArticlePrice', $ArticlePrice, $ArticlePriceId) ;
	}
	dbQueryFree ($res_tradeconfig) ;
 
	return 0 ;
?>
