<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    
    // Outher table
    printf ("<table class=item><tr>\n<td width=400>\n") ;
    
    // Header
    itemStart () ;
    itemHeader('Basic info') ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    if (!$Record['ArticleTypeProduct']) {
	itemSpace () ;
	printf ("<tr><td class=itemlabel>Unit</td><td class=itemfield>%s</td></tr>\n", tableGetField ('Unit', 'Name', (int)$Record['UnitId'])) ;
    }
    itemSpace () ;
    itemFieldIcon ('Supplier', tableGetField ('Company', 'CONCAT(Name," (",Number,")")', (int)$Record['SupplierCompanyId']), 'company.gif', 'companyview', (int)$Record['SupplierCompanyId']) ;
    if (!$Record['ArticleTypeProduct']) {
	printf ("<tr><td class=itemlabel>Reference</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['SupplierNumber'])) ;
	if ($Record['ArticleTypeFabric']) {
	    print htmlItemSpace() ;
	    printf ("<tr><td class=itemlabel>Print Number</td><td class=itemfield>%s</td></tr>\n", $Record['PrintNumber']) ;
	}
    }
//	print htmlItemSpace() ;
//	printf ("<tr><td class=itemlabel>Landed cost</td><td class=itemfield>%0.2f %s</td></tr>\n", $Record['PriceStock'], tableGetField('Currency','Symbol',1)) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Customs Pos</td><td class=itemfield>%s</td></tr>\n", tableGetField ('CustomsPosition', 'Name', (int)$Record['CustomsPositionId'])) ;
    print htmlItemSpace() ;
//  printf ("<tr><td class=itemlabel>Material</td><td class=itemfield>%s</td></tr>\n", ((int)$Record['MaterialCountryId'] > 0) ? tableGetField ('Country', 'CONCAT(Description," (",Name,")")', (int)$Record['MaterialCountryId']) : 'none') ;
//  printf ("<tr><td class=itemlabel>Knit</td><td class=itemfield>%s</td></tr>\n", tableGetField ('Country', 'CONCAT(Description," (",Name,")")', (int)$Record['KnitCountryId'])) ;
    printf ("<tr><td class=itemlabel>Work</td><td class=itemfield>%s</td></tr>\n", tableGetField ('Country', 'CONCAT(Description," (",Name,")")', (int)$Record['WorkCountryId'])) ;
	if ($Record['ArticleTypeFabric']) {
		// Composition
		$s = '' ;
		$query = sprintf ('SELECT ArticleComponent.Percent, Component.Name FROM ArticleComponent INNER JOIN Component ON Component.Id=ArticleComponent.ComponentId AND Component.Active=1 WHERE ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1', (int)$Record['Id']) ;
		$res = dbQuery ($query) ;
		while ($row = dbFetch ($res)) {
		    if ($s != '') $s .= ', ' ;
		    $s .= sprintf ('%s %d%%', $row['Name'], (int)$row['Percent']) ;
		}
		dbQueryFree ($res) ;
		itemSpace () ;
		itemField ('Composition', $s) ;
	}
	if ($Record['ArticleTypeProduct']) {
		// Fabrics
/*		
		$query = sprintf ('SELECT Article.* FROM Article WHERE Article.Id=%d AND Article.Active=1', (int)$Record['FabricArticleId']) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree ($res) ;
		itemSpace () ;
		itemFieldIcon ('Fabric', $row['Number'], 'article.gif', 'articleview', (int)$row['Id']) ;
		itemField ('Description', $row['Description']) ;
*/	
		// Composition
		$s = '' ;
		$query = sprintf ('SELECT ArticleComponent.Percent, Component.Name FROM ArticleComponent INNER JOIN Component ON Component.Id=ArticleComponent.ComponentId AND Component.Active=1 WHERE ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1', (int)$Record['Id']) ;
		$res = dbQuery ($query) ;
		while ($row = dbFetch ($res)) {
		    if ($s != '') $s .= ', ' ;
		    $s .= sprintf ('%s %d%%', $row['Name'], (int)$row['Percent']) ;
		}
		dbQueryFree ($res) ;
		itemSpace () ;
		itemField ('Composition', $s) ;
	
    }

    itemEnd () ;
	printf ("<br><br>") ;
//     printf ("<br></td><td width=500 style=\"border-left: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader('Clusters') ;
	listRow () ;
	listHead ('Group', 60) ;
	listHead ('Name', 120) ;
	listHead ('Value', 200) ;
	$_query = sprintf('select ct.Name as ClusterTypeName, ct.id as ClusterTypeId, c.name as ClusterValue, cg.Name as ClusterGroupName, ca.Name as CaName
						from (clustertype ct) 
						left join clusterarticle ca ON ca.typeid=ct.id and ca.articleid=%d and ca.active=1
						left join cluster c ON c.id=ca.clusterid and c.active=1
						left join clustergroup cg ON cg.id=ct.clustergroupid
						where  ct.active=1 order by cg.displayorder, ct.displayorder', $Id);
	$_result = dbQuery($_query) ;
	while ($_row=dbFetch($_result)) {
		listRow () ;
		listField ($_row['ClusterGroupName']) ;
		listField ($_row['ClusterTypeName']) ;
		listField (((int)$_row['ClusterTypeId'] === 11 ? $_row['CaName'] : $_row['ClusterValue'])) ;
	}
	dbQueryFree($_result) ;
	listEnd() ;

    printf ("<br></td><td style=\"border-left: 10px solid #FFFFFF;\">\n") ;
	$query = sprintf ("SELECT Color.Number FROM ArticleColor INNER JOIN Color ON Color.Id=ArticleColor.ColorId AND Color.Active=1 WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 ORDER BY Color.Number", (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	$_fileexists = 0 ;
	while ($row = dbFetch ($res)) {
		$file =	'image/thumbnails/' . $Record['Number'] . '_' . $row['Number'] . '.jpg' ;
		if (is_file($file)) {
		// Sketch exists
			printf ('<img width=250px src="' . $file .'">') ;
			$_fileexists = 1 ;
			break ;
		} 
	}
	// No Sketch
	if($_fileexists==0) {
		printf ('<p style="padding-top:4px;">No photos</p>') ;
    }
	dbQueryFree ($res) ;
    printf ("</td></tr></table>\n") ;

    // Outher table, 1st field
    printf ("<table class=item><tr>") ;
    
    if ($Record['ArticleTypeProduct']) {
	// Colors
	$query = sprintf ("SELECT ArticleColor.*, Color.Number, Color.Number as ColorNumber, Article.Number as ArticleNumber, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen 
						FROM (ArticleColor, Article) 
						INNER JOIN Color ON Color.Id=ArticleColor.ColorId AND Color.Active=1 
						LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId AND ColorGroup.Active=1 
						WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Article.Id=ArticleColor.ArticleId ORDER BY Color.Number", (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Colours') ;
	listRow () ;
	listHeadIcon (50) ;
	listHeadIcon () ;
	listHead ('Number', 75) ;
	listHead ('Colour', 40) ;
	listHead ('', 6) ;
	listHeadIcon () ;
	listHead ('Description',200) ;
	listHead ('') ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
		$row['Id']=tableGetFieldWhere('collectionmember','Id','ArticleId='.$row['ArticleId'].' AND ArticleColorId=' . $row['Id']) ;
		listStyleIcon ('ArtMemberView', $row) ;
	    listFieldIcon ('pen.gif', 'cmedit', $row['Id']) ;
	    listField ($row['Number']) ;
	    if ($row['ColorGroupId'] > 0) {
		listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	    } else {
		listField ('') ;
	    }
	    listField ('') ;
	    listFieldIcon ('pen.gif', 'colorlang', $row['ColorId']) ;
	    listField ($row['Description']) ;
	    listField ('') ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Colors', 'colspan=3') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;

	// Sizes
	$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder', (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Sizes') ;
	listRow () ;
	listHead ('Name') ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
	    listField ($row['Name']) ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField ('No Sizes') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;

	// articlecategorygroups.
/*
	printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listRow () ;
	listHead ('CategoryGroups', 200) ;
	listEnd() ;
	listStart () ;
	listRow () ;
	listHead ('Name', 50) ;
	listHead ('ArticleGroup', 50) ;
	listHead ('', 10) ;
	listHead ('', 10) ;

	$query = sprintf ('Select aacg.id as Id, acg.Name as Name, ag.Name as GroupName 
						From (articlearticlecategorygroup aacg, articlecategorygroup acg, articlegroup ag) 
						Where aacg.ArticleId=%d AND aacg.Active=1 AND acg.id=aacg.articlecategorygroupid and ag.id=acg.articlegroupid Order By acg.DisplayOrder', $Id) ; 
	$res = dbQuery ($query) ;
	while ($row = dbFetch ($res)) {
		listRow () ;
		listField ($row['Name']) ;
		listField ($row['GroupName']) ;
		listFieldIcon ('edit.gif', 'editaacg', (int)$row['Id']) ;
		listFieldIcon ('delete.gif', 'deleteaacg', (int)$row['Id']) ;
	}
	listRow () ;
	listFieldIcon ('add.gif', 'addaacg', (int)$Id) ;
	listEnd() ;
	printf ("</td>\n") ;
*/	
	// Certificats
	if ($Record['VariantCertificate']) {
	    $query = sprintf ("SELECT ArticleCertificate.Id, Certificate.Name AS CertificateName, CertificateType.Name AS CertificateTypeName FROM ArticleCertificate INNER JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 INNER JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId AND CertificateType.Active=1 WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Certificate.Name", (int)$Record['Id']) ;
	    $res = dbQuery ($query) ;
	    printf ("<td width= 180 style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    listStart () ;
	    listHeader ('Certificates') ;
	    listRow () ;
	    listHead ('Name', 90) ;
	    listHead ('Type', 90) ;
	    while ($row = dbFetch ($res)) {
		listRow () ;
		listField ($row['CertificateName']) ;
		listField ($row['CertificateTypeName']) ;
	    }
	    if (dbNumRows($res) == 0) {
		listRow () ;
		listField ('No Certificates', 'colspan=2') ;
	    }
	    listEnd () ;
	    printf ("<br></td>") ;
	    dbQueryFree ($res) ;
	}
	
    } else if ($Record['ArticleTypeFabric']) {
    	
	// Fabric view
    printf ("<td width=50%% style=\"border-bottom: 1px solid #cdcabb;\">\n") ;
	itemStart () ;
	itemHeader ('General') ;
	itemFieldText ('Comment', $Record['Comment']) ;
	itemInfo ($Record) ;
	itemEnd () ;
	printf ("<br></td>") ;
    
	if ($Record['ArticleTypeTest']) {
	    // Outher table, 2nd field
	    printf ("<td width=25%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    itemStart () ;
	    itemHeader('Tests General', 'Name', 'Value') ;
	    itemField ('Width Usable', $Record['WidthUsable'] . ' cm') ;
	    itemField ('Width Full', $Record['WidthFull'] . ' cm') ;
	    itemSpace () ;
	    itemField ('Weight', $Record['M2Weight'] . ' g/m2') ;
	    itemField ('', (int)($Record['M2Weight']*$Record['WidthFull']/100) . ' g/mtr') ;
	    if ($Record['M2Weight']!=0 and $Record['WidthFull']!=0) 
	    	 itemField ('', ((int)(1000*(1000/($Record['M2Weight']*$Record['WidthFull']/100))))/1000 . ' mtr/kg (Excl. prod. shrinkage)') ;
	    itemSpace () ;
	    itemField ('Shrink Wash', $Record['ShrinkageWashLength'] . ' % length') ;
	    itemField ('', $Record['ShrinkageWashWidth'] . ' % width') ;
	    itemField ('Shrink Work', $Record['ShrinkageWorkLength'] . ' % length') ;
	    itemField ('', $Record['ShrinkageWorkWidth'] . ' % width') ;
	    itemSpace () ;
	    itemField ('Rubbing Dry', $Record['RubbingDry']) ;
	    itemField ('Rubbing Wet', $Record['RubbingWet']) ;
	    itemSpace () ;
	    itemField ('Washinginstr.', $Record['WashingInstruction']) ;
	    itemEnd () ;
	    printf ("<br></td>") ;
	    
	    // Outher table, 2nd field
	    printf ("<td width=25%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    itemStart () ;
	    itemHeader ('Tests Colourfastness', 'Type', 'Value') ;
	    foreach (array('Water','Wash','Light','Perspiration','Chlor','Sea','Durability') as $name) {
		itemField ($name, $Record['ColorFastness'.$name]) ;
	    }
	    itemEnd () ;
	    printf ("<br></td>") ;
	}
    } else {
   	// Generel view.
    printf ("<td style=\"border-bottom: 1px solid #cdcabb;\">\n") ;
	itemStart () ;
	itemHeader ('General') ;
	itemFieldText ('Comment', $Record['Comment']) ;
	itemInfo ($Record) ;
	itemEnd () ;
	printf ("<br></td>") ;
	// Colors
	if ($Record['VariantColor']) {
	$query = sprintf ("SELECT ArticleColor.*, Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM ArticleColor INNER JOIN Color ON Color.Id=ArticleColor.ColorId AND Color.Active=1 LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId AND ColorGroup.Active=1 WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 ORDER BY Color.Number", (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td width=300 style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Colours') ;
	listRow () ;
	listHeadIcon () ;
	listHead ('Number', 75) ;
	listHead ('Colour', 40) ;
	listHead ('', 6) ;
	listHead ('Description1') ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
	    listFieldIcon ('color.gif', 'color', $row['Id']) ;
	    listField ($row['Number']) ;
	    if ($row['ColorGroupId'] > 0) {
		listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	    } else {
		listField ('') ;
	    }
	    listField ('') ;
	    listField ($row['Description']) ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Colors', 'colspan=3') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;
	}

	// Sizes
	if ($Record['VariantSize']) {
	$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder', (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td width=100 style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Sizes') ;
	listRow () ;
	listHead ('Name') ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
	    listField ($row['Name']) ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField ('No Sizes') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;
	}

	// Certificats
	if ($Record['VariantCertificate']) {
	    $query = sprintf ("SELECT ArticleCertificate.Id, Certificate.Name AS CertificateName, CertificateType.Name AS CertificateTypeName FROM ArticleCertificate INNER JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 INNER JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId AND CertificateType.Active=1 WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Certificate.Name", (int)$Record['Id']) ;
	    $res = dbQuery ($query) ;
	    printf ("<td width= 180 style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    listStart () ;
	    listHeader ('Certificates') ;
	    listRow () ;
	    listHead ('Name', 90) ;
	    listHead ('Type', 90) ;
	    while ($row = dbFetch ($res)) {
		listRow () ;
		listField ($row['CertificateName']) ;
		listField ($row['CertificateTypeName']) ;
	    }
	    if (dbNumRows($res) == 0) {
		listRow () ;
		listField ('No Certificates', 'colspan=2') ;
	    }
	    listEnd () ;
	    printf ("<br></td>") ;
	    dbQueryFree ($res) ;
	}
    }
    printf ("</tr></table>\n") ;

    // Footer
    if ($Record['ArticleTypeProduct']) {
	itemStart () ;
	itemHeader ('General') ;
	itemFieldText ('Comment', $Record['Comment']) ;
	itemInfo ($Record) ;
	itemEnd () ;
    }
    return 0 ;
?>
