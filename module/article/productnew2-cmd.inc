<?php

    define ('DEBUG', 0) ;

    require_once 'lib/string.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/log.inc' ;
	require_once 'lib/variant.inc' ;
	require_once 'lib/parameter.inc' ;

 	$_varbasecode = ParameterGet('BaseSKU787') ;
   // Remove Colors not selected
    foreach ($Color as $i => $row) {
		if (!strCheckBox($_POST['Color'][$i])) unset ($Color[$i]) ;
    }

    // Remove Colors not selected
    foreach ($Components as $i => $row) {
		if (!strCheckBox($_POST['Components'][$i])) unset ($Components[$i]) ;
    }

    // Colors
    if (count($Color) > 0) {
		// Copy Colors
		if ((int)$Record['FabricArticleId']>0) {
			// Make list of ArticleColorId
			$s = '' ;
			foreach ($Color as $i => $row) {
				if ($s != '') $s .= ',' ;
				$s .= $i ;
			}
			$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.Id IN (%s)', $s) ;
			$result = dbQuery ($query) ;
			while ($row = dbFetch ($result)) {
				$row['ArticleId'] = (int)$Record['ArticleId'] ;
				$row['Id'] = tableWrite ('ArticleColor', $row) ;
			}
			dbQueryFree ($result) ;
		} else {
			foreach ($Color as $i => $row) {
				$art_color['ArticleId'] = (int)$Record['ArticleId'] ;
				$art_color['ColorId'] = (int)$i ;
				$row['Id'] = tableWrite ('ArticleColor', $art_color) ;
				
				// Create Collectionmember?
				If ($_POST['CollectionId']>0) {
					$query = sprintf("SELECT cl.Id as Id, cl.Name AS Value FROM Collectionlot cl, collection c WHERE c.Id=%d and cl.SeasonId=c.SeasonId AND cl.Active=1 ORDER BY Date", (int)$_POST['CollectionId']) ;
					$clres = dbQuery ($query) ;
					$clrow = dbFetch($clres) ;
					dbQueryFree ($clres) ;


					$CollectionMember['CollectionId'] = (int)$_POST['CollectionId'] ;
					$CollectionMember['ArticleId'] = (int)$Record['ArticleId'] ;
					$CollectionMember['ArticleColorId'] = (int)$row['Id'];
					$CollectionMember['CollectionLOTId'] = (int)$clrow['Id']; //$_POST['CollectionLOTId'];
					$_colmid = tableWrite ('CollectionMember', $CollectionMember) ;

					// always make price record with currencyid=0
					$CollectionMemberPrice['CollectionMemberId'] = (int)$_colmid ;
					$CollectionMemberPrice['WholesalePrice'] = (float)0;
					$CollectionMemberPrice['RetailPrice'] = (float)0 ;
					$CollectionMemberPrice['CampaignPrice'] = (float)0;
					$CollectionMemberPrice['CurrencyId'] = (int)0;
					$CollectionMemberPrice['CollectionPriceGroupId'] = (int)0;
					$ColletionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice) ;
				}
				// Create EAN codes.
				$_query = sprintf ('SELECT * FROM articlesize WHERE Active=1 and articleid=%d', (int)$Record['ArticleId']) ;
				$_result = dbQuery ($_query) ;
				while ($sizerow = dbFetch ($_result)) {
					$_variantcode = GetNextVariantCode($_varbasecode) ;
					$Variant = array (
						'ArticleId' 		=>	(int)$Record['ArticleId'],
						'ArticleColorId'	=>	(int)$row['Id'],
						'ArticleSizeId' 	=>	(int)$sizerow['Id'],
						'VariantCode'		=>  $_variantcode
					) ;
					$_variantcodeid=tableWrite('variantcode', $Variant) ;
				}
				dbQueryFree ($_result) ;
				
			}
		}
	}
    
	foreach ($Components as $i => $row) {
			$art_Component['ArticleId'] 	= (int)$Record['ArticleId'] ;
			$art_Component['ComponentId'] 	= (int)$i ;
			$art_Component['Percent'] 		= (int)$_POST['Percent'][$i] ;
			$row['Id'] = tableWrite ('ArticleComponent', $art_Component) ;

	}
	
//	navigationCommandMark ('createstyleversion', (int)$Record['Id']) ;
    // View new Case
//    return navigationCommandMark ('articleview', (int)$Record['Id']) ;
    return navigationCommandMark ('collmemberlist', 1, '&Article='.$Record['Number']) ;
?>
