<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

 
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;
//    printf ("<tr><td class=itemfield>Delivery Comment</td><td><textarea name=DeliveryComment style='width:100%%;height:300px;'>%s</textarea></td></tr>\n", $Record['DeliveryComment']) ;
//    itemSpace () ;
    itemFieldRaw ('DK Webshop', formCheckbox ('OnWebshop[4]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=4 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>DK Name</td><td><input type=text name=Name[4] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=4 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>DK Desc</td><td><textarea name=Desc[4] style='width:100%%;height:300px;' id='daWeb'>%s</textarea></td></tr>\n", base64_decode(tableGetFieldWhere('article_lang','WebDescription','LanguageId=4 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemFieldRaw ('EN Webshop', formCheckbox ('OnWebshop[6]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=6 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>EN Name</td><td><input type=text name=Name[6] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=6 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>EN Desc</td><td><textarea name=Desc[6] style='width:100%%;height:300px;' id='enWeb'>%s</textarea></td></tr>\n", base64_decode(tableGetFieldWhere('article_lang','WebDescription','LanguageId=6 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemFieldRaw ('SE Webshop', formCheckbox ('OnWebshop[8]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=8 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>SE Name</td><td><input type=text name=Name[8] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=8 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>SE Desc</td><td><textarea name=Desc[8] style='width:100%%;height:300px;' id='deWeb'>%s</textarea></td></tr>\n", base64_decode(tableGetFieldWhere('article_lang','WebDescription','LanguageId=8 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemFieldRaw ('NO Webshop', formCheckbox ('OnWebshop[7]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=7 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>NO Name</td><td><input type=text name=Name[7] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=7 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>NO Desc</td><td><textarea name=Desc[7] style='width:100%%;height:300px;' id='deWeb'>%s</textarea></td></tr>\n", base64_decode(tableGetFieldWhere('article_lang','WebDescription','LanguageId=7 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemFieldRaw ('NL Webshop', formCheckbox ('OnWebshop[9]', tableGetFieldWhere('article_lang','OnWebshop','LanguageId=9 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>NL Name</td><td><input type=text name=Name[9] maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities(tableGetFieldWhere('article_lang','Description','LanguageId=9 and ArticleId='. $Id))) ;
    printf ("<tr><td class=itemfield>NL Desc</td><td><textarea name=Desc[9] style='width:100%%;height:300px;' id='deWeb'>%s</textarea></td></tr>\n", base64_decode(tableGetFieldWhere('article_lang','WebDescription','LanguageId=9 and ArticleId='. $Id))) ;
    itemSpace () ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    // g0fn2kp435fzid6x5pb9icvsodd6jl817mpj5cidwlqx2ny1
    ?>
    <script src='https://cdn.tiny.cloud/1/g0fn2kp435fzid6x5pb9icvsodd6jl817mpj5cidwlqx2ny1/tinymce/5/tinymce.min.js' referrerpolicy="origin">

    </script>
    <script>
        // tinymce.init({
        //     selector: '#daWeb',
        //     plugins: 'link',
        //     // menubar: 'insert',
        //     // toolbar: 'link'
        // });

        // tinymce.init({
        //     selector: '#enWeb',
        //     plugins: 'link',

        // });

        // tinymce.init({
        //     selector: '#deWeb',
        //     plugins: 'link',
        // });
      </script>

    <?php 

    return 0 ;
?>
