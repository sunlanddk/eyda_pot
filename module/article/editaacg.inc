<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
	require_once 'lib/navigation.inc' ;

 	if ($Navigation['Parameters'] == 'new') {
		$new = 1 ;
	} else {
	    $query = sprintf ("SELECT * FROM ArticleArticleCategoryGroup WHERE Id=%d AND Active=1", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

		$new = 0 ;
	}
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemSpace () ;

	itemFieldRaw ('CategoryGroup', formDBSelect ('ArticleCategoryGroupId', (int)$Record['ArticleCategoryGroupId'], sprintf('SELECT Id, ArticleCategoryGroup.Name AS Value FROM ArticleCategoryGroup ORDER BY Value'), 'width:250px;')) ;  
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
