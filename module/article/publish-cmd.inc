<?php
	require_once 'lib/table.inc' ;

	$query = sprintf ("
		SELECT Id
		FROM collectionmember
		WHERE Active=1 AND Cancel=0 AND notonwebshop=0 AND ArticleId=%d
		", 
		$Id
	);
    
    $res = dbQuery ($query) ;

    while ($row = dbFetch($res)) {
      	$post = array(
			'type'				=> 'shopify',
			'internalid' 		=> (int)$row['Id'],
			'cron_name' 		=> 'Update product',
			'cron_function' 	=> 'publishProduct',
			'cron_value' 		=> (int)$row['Id'],
			'cron_scheduled_at'	=> dbDateEncode(time()),
		);
		tableWriteCron('cron_jobs', $post);
    }
    dbQueryFree ($res) ;


	return navigationCommandMark ('articleview', (int)$Id) ;
    return 0 ;
?>
