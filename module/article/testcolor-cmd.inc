<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'ColorFastnessWater'		=> array (),
	'ColorFastnessWash'		=> array (),
	'ColorFastnessLight'		=> array (),
	'ColorFastnessPerspiration'	=> array (),
	'ColorFastnessChlor'		=> array (),
	'ColorFastnessSea'		=> array (),
	'ColorFastnessDurability'	=> array ()
    ) ;

    switch ($Navigation['Parameters']) {
	case 'Item' :
	    if ((int)$Record['ContainerId'] == 0 or $Record['StockType'] == 'shipment') return sprintf ('%s(%d) stock locked, id %d', __FILE__, __LINE__, (int)$Record['StockId']) ;
	    break ;
    }

    // Save record
    return saveFields ($Navigation['Parameters'], $Id, $fields) ;
?>
