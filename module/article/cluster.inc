<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/db.inc' ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
	$_query = 'select clustertype.*, clustergroup.name as ClusterGroupName
                from clustertype 
                left join clustergroup ON clustergroup.id=clustertype.clustergroupid
                where clustertype.active=1 order by clustergroup.displayorder, clustertype.displayorder';
	$_result = dbQuery($_query) ;
    $_clusterGroupName = NULL ;
	while ($_row=dbFetch($_result)) {
        if ($_clusterGroupName<>$_row['ClusterGroupName']) {
            if (is_null($_clusterGroupName)) { // Start New Groupn
                itemEnd() ;
                echo '<table><tr><td>'.$_row['ClusterGroupName'].'</td>.<td>' ;
                itemStart () ;
                itemHeader () ;
            }
            if (is_null($_row['ClusterGroupName'])) { // End of Group
                itemEnd() ;
                echo '</td></tr></table>' ;
                itemStart () ;
                itemHeader () ;
            }
            $_clusterGroupName = $_row['ClusterGroupName'] ;
        }
        switch ($_row['InputType']) {
            case 'select':
                $_clusterId = (int)tableGetFieldWhere('clusterarticle','ClusterId','TypeId=' . $_row['Id'] . ' and ArticleId='. $Id) ;
                itemFieldRaw ($_row['Name'], formDBSelect (ClusterType.'['.$_row['Id'].']', $_clusterId, 
                            sprintf("SELECT Id, Name AS Value FROM cluster WHERE TypeId=%d ORDER BY Value", $_row['Id']), 'width:250px;')) ;  
                break ;
            case 'selectmultiple': // not completely implemented.
                $_clusterId = (int)tableGetFieldWhere('clusterarticle','ClusterId','TypeId=' . $_row['Id'] . ' and ArticleId='. $Id) ;
                itemFieldRaw ($_row['Name'], formDBSelect (ClusterType.'['.$_row['Id'].']', $_clusterId, 
                            sprintf("SELECT Id, Name AS Value FROM cluster WHERE TypeId=%d ORDER BY Value", $_row['Id']), 'width:250px;','','multiple', 5)) ;  
                break ;
            case 'text': // not completely implemented.
                $_clusterId = (int)tableGetFieldWhere('clusterarticle','ClusterId','TypeId=' . $_row['Id'] . ' and ArticleId='. $Id) ;
                $_text = tableGetFieldWhere('clusterarticle', 'Name', 'TypeId=11 AND ClusterId=33 AND Active=1 AND ArticleId='.$Id);
                itemFieldRaw ($_row['Name'], formText (ClusterType.'['.$_row['Id'].']', $_text, 255, 'width:250px !important')) ;  
                break ;
            default:
                echo 'Hallo ' . $_row['InputType'] ;
                break ;
        }
	}
	dbQueryFree($_result) ;

    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
