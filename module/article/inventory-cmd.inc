<?php 
require_once('lib/table.inc');
require_once('lib/inventory.inc');

$updated = [];
foreach ($_POST['variant'] as $VariantCodeId => $sku) {
	if($sku == ''){
		continue;
	}

	if( ((int)$sku - (int)$_POST['varianthidden'][$VariantCodeId] ) !== 0 ){
		$changed = ((int)$sku - (int)$_POST['varianthidden'][$VariantCodeId]);
		array_push($updated, array('variantcodeid' => $VariantCodeId, 'changed' => $changed));
	}
}

if(count($updated) > 0){
	$return = updateStockCountByVariant($updated, $_POST['comment']);
}

return 0;
