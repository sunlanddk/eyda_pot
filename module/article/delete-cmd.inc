<?php

    require_once 'lib/table.inc' ;

    // Ensure Article not used in collectionmember
    $query = sprintf ("SELECT Season.Name as SeasonName, Collection.Name as CollectionName 
						FROM `collectionmember`, Collection, Season 
						WHERE ArticleId=%d AND Collection.Id=CollectionMember.CollectionId AND Season.Id=Collection.SeasonId AND collectionmember.Active=1 and Collection.Active=1 AND Season.Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    if ($count > 0) {
		$row = dbFetch ($result) ;
		return "Article used in Collection: " . $row['CollectionName'] . ' Season: ' . $row['SeasonName'] ;
	}
    dbQueryFree ($result) ;

    // Ensure Article not used in Stock
    $query = sprintf ("SELECT Id FROM Item WHERE ArticleId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Article used for Items on Stock" ;
    		
    // Loop trough Styles for Article
    $query = sprintf ("SELECT Id FROM Style WHERE ArticleId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch ($result)) {
	tableDelete ('Style', $row['Id']) ;
	tableDeleteWhere ('StyleOperation', sprintf ("StyleId=%d", $row['Id'])) ;
	tableDeleteWhere ('StylePDM', sprintf ("StyleId=%d", $row['Id'])) ;
	tableDeleteWhere ('StyleLog', sprintf ("StyleId=%d", $row['Id'])) ;
    }
    dbQueryFree ($result) ;
    
    tableDelete ('Article', $Id) ;

    
    return 0
?>
