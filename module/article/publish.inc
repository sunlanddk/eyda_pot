<script type="text/javascript" >
	function CheckAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			e.checked = true ;
		}
	}
</script>

<?php


    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;
	
	
//	die('her') ;

    formStart () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Selected', 30) ;
    listHead ('Article', 45)  ;
    listHead ('Description', 120) ;
    listHead ('', 95) ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldRaw (formCheckbox (sprintf('PrintFlag[%d]', (int)$row['Id']), 0, '')) ;
    	listField ($row["Number"]) ;
    	listField ($row["Description"]) ;
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Articles', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    return 0 ;
?>
