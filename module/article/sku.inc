<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    global $User, $Navigation, $Record, $Result2, $Size, $Id ;

    itemStart();
    itemField(Article, $Record['Number']);
    itemField(Description, $Record['Description']);
    itemEnd() ;
    echo'<br>'; 

    formStart();
    $query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder
						FROM ArticleSize
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id) ;
	$result = dbQuery ($query) ;
	$Sizes = array() ;
	while ($row = dbFetch ($result)) {
		$Sizes[$row['Id']] = $row ;
	}
    dbQueryFree ($result) ;

    $query = sprintf ("SELECT ArticleColor.Id, Color.Description
						FROM ArticleColor
						LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
						WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1
						ORDER BY Color.Description", $Id) ;
	$result = dbQuery ($query) ;
	$Colors = array() ;
	while ($row = dbFetch ($result)) {
		$Colors[$row['Id']] = $row ;
	}
    dbQueryFree ($result) ;
    ?>

    <table class="item">
    	<thead>
    		<tr>
    			<th></th>
    			<?php 
    				foreach ($Sizes as $key => $value) {
    					echo '<th style="text-align:center;">'.$value['Name'].'</th>';
    				}
    			?>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		foreach ($Colors as $ckey => $Color) {
    			echo '<tr>';
    				echo '<td>'.$Color['Description'].'</td>';
    				foreach ($Sizes as $key => $size) {
    					$variantcodeid = tableGetFieldWhere('variantcode', 'Id', sprintf("Active=1 AND ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d", $Id, $Color['Id'], $size['Id']));
    					$sku = tableGetFieldWhere('variantcode', 'SKU', sprintf("Active=1 AND ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d",$Id, $Color['Id'], $size['Id']));
    					echo '<td style="text-align:left;"><input name="variant['.$variantcodeid.']" value="'.$sku.'"></td>';
    				}
    			echo '</tr>';
    		}
    		?>

    	</tbody>
    </table>

    <style>
    	input{
    		margin: 0;
    		padding: 0;
    		margin-bottom: 5px;
    	}
    </style>


    <?php 
    
    formEnd () ;

    return 0 ;
?>
