<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;

	
	if ($Navigation['Parameters'] == 'new') {
		// Create record
		$_record['ArticleId'] = (int)$Id;
		$_record['ArticleCategoryGroupId'] = (int)$_POST['ArticleCategoryGroupId'];
		
		tableWrite('ArticleArticleCategoryGroup',$_record) ;
		return navigationCommandMark ('articleview', (int)$Id) ;
	} else {
		// Edit record
		$ArticleId=tableGetField('ArticleArticleCategoryGroup','ArticleId',(int)$Id) ;
		$_record['ArticleCategoryGroupId'] = (int)$_POST['ArticleCategoryGroupId'];
		
		tableWrite('ArticleArticleCategoryGroup',$_record, $Id) ;
		return navigationCommandMark ('articleview', (int)$ArticleId) ;
	}
 
 return 0 ;    

?>
