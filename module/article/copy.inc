<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    
    // Form
    formStart () ;
    itemStart () ;
    itemSpace () ;
    itemField ('Article', sprintf ('%s, %s', $Record['Number'], $Record['Description'])) ;
    itemSpace () ;
    itemHeader () ;
    itemFieldRaw ('Number', formText ('Number', '', 10)) ;
    itemSpace () ;
    itemFieldRaw ('Details', formCheckbox ('Details')) ;
    itemFieldRaw ('Prices', formCheckbox ('Price')) ;
    if ($Record['VariantColor'])itemFieldRaw ('Colour', formCheckbox ('Color')) ;
    if ($Record['VariantSize']) itemFieldRaw ('Sizes', formCheckbox ('Size')) ;
    if ($Record['VariantCertificate']) itemFieldRaw ('Certificates', formCheckbox ('Certificate')) ;
    itemFieldRaw ('Composition', formCheckbox ('Composition')) ;
    if ($Record['ArticleTypeProduct']) itemFieldRaw ('Style', formCheckbox ('Style')) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
