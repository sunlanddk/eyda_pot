<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/select.inc' ;
    require_once 'module/style/include.inc' ;
	
	if ($User['Restricted']) {
		$LimitClause = sprintf(' AND Company.Number IN ( %s ) ', $User['CompanyList']);
	} else {
		$LimitClause = '';
	}

    $ArticleTypeFields = 'ArticleType.Id AS ArticleTypeId,
ArticleType.Product AS ArticleTypeProduct,
ArticleType.Material AS ArticleTypeMaterial,
ArticleType.Fabric AS ArticleTypeFabric,
ArticleType.Test AS ArticleTypeTest,
ArticleType.VariantColor AS ArticleTypeVariantColor,
ArticleType.VariantDimension AS ArticleTypeVariantDimension,
ArticleType.VariantSize AS ArticleTypeVariantSize,
ArticleType.VariantSortation AS ArticleTypeVariantSortation,
ArticleType.VariantCertificate AS ArticleTypeVariantCertificate' ;

    switch ($Navigation['Function']) {
	case 'list' :
	case 'publish' :
	    // List field specification
	    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Article',		'db' => 'Article.Number',		'direct' => 'Article.Active=1'),
			array ('name' => 'Description',		'db' => 'Article.Description'),
			array ('name' => 'Reference',		'db' => 'Article.SupplierNumber'),
			array ('name' => 'Supplier',		'db' => 'Company.Number'),
			array ('name' => 'Name',		'db' => 'Company.Name'),
			array ('name' => 'Print',		'db' => 'Article.PrintNumber')
	    ) ;
	    $queryFields = 'Article.*, Company.Number AS SupplierCompanyNumber, Company.Name AS SupplierCompanyName' ;
	    $queryTables = 'Article LEFT JOIN Company ON Company.Id=Article.SupplierCompanyId' ;
	    $queryClause = 'Article.Active=1' ;	 
		$queryClause .= $LimitClause;   
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'articleview') ;
	    return $res ;

	case 'basic' :
	case 'basic-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through
		
	case 'detail' :
	case 'detail-cmd' :
	case 'testgeneric' :
	case 'testgeneric-cmd' :
	case 'testcolor' :
	case 'testcolor-cmd' :
	case 'delete-cmd' :
	case 'select-cmd' :
	case 'copy' :
	case 'sku-cmd' :
	case 'weight-cmd' :
	case 'sku' :
	case 'weight' :
	case 'copy-cmd' :
	case 'sku':
	case 'sku-cmd':
	case 'weight':
	case 'weight-cmd':
	case 'view' :
    	    global $styleStateField ;		// For direct view only

	    // Get Article
	    $query = sprintf ("SELECT Article.*, %s FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Id=%d AND Article.Active=1", $ArticleTypeFields, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    if ($Record['ArticleTypeProduct']) {
		// Has Production
		
		// Get Style information
		$query = sprintf ('SELECT MAX(Version) as Version FROM Style WHERE ArticleId=%d AND Active=1', $Id) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree ($res) ;
		if ($row['Version'] > 0) {
		    $query = sprintf ('SELECT Style.Id AS StyleId, Style.Version AS StyleVersion, %s AS StyleState FROM Style WHERE Style.ArticleId=%d AND Style.Version=%d AND Style.Active=1', $styleStateField, $Id, $row['Version']) ;
		    $res = dbQuery ($query) ;
		    $Record = array_merge ($Record, dbFetch ($res)) ;
		    dbQueryFree ($res) ;
		}

		// Navigation
		navigationEnable ('production') ;
		navigationEnable ('stylelist') ;
	    }

	    if ($Record['VariantSize']) {
		// Has Size Variant
		navigationEnable ('sizes') ;
	    }

	    if ($Record['VariantColor']) {
		// Has Size Variant
		navigationEnable ('colors') ;
	    }
		
	    if ($Record['VariantCertificate']) {
		// Has Size Variant
		navigationEnable ('certificates') ;
	    }

	    if ($Record['ArticleTypeTest']) {
		// Enable test data entry
		navigationEnable ('test') ;
	    }
	    break ;	

	case 'sketch' :
	    // Get Article
	    $query = sprintf ("SELECT Article.* FROM Article WHERE Article.Id=%d AND Article.Active=1", $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    break ;

	case 'editaacg' :
	case 'editaacg-cmd' :
	case 'deleteaacg-cmd' :
	    // Get Article
	    break ;
	case 'productnew' :
	case 'productnew-cmd' :
	    break ;

	case 'productnew2' :
	case 'productnew2-cmd' :
	    // Get Article
	    $query = sprintf ("SELECT Article.*, %s, Article.Id AS ArticleId FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Id=%d AND Article.Active=1", $ArticleTypeFields, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Get Colors
		if ((int)$Record['FabricArticleId']>0)
			$query = sprintf ("SELECT ArticleColor.*, Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM ArticleColor INNER JOIN Color ON Color.Id=ArticleColor.ColorId AND Color.Active=1 LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId AND ColorGroup.Active=1 WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 ORDER BY Color.Number", (int)$Record['FabricArticleId']) ;
		else
			$query = "SELECT Color.Id as Id, Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen 
					FROM Color 
					LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId AND ColorGroup.Active=1 
					WHERE Color.Active=1 ORDER BY Color.Description" ;
	    $result = dbQuery ($query) ;
	    $Color = array () ;
	    while ($row = dbFetch ($result)) {
			$Color[(int)$row['Id']] = $row ;
	    }
	    dbQueryFree ($result) ;

	    // Get Certificates
	    $query = sprintf ('SELECT CertificateType.Id FROM ArticleCertificate INNER JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 INNER JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId AND CertificateType.Active=1 WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 GROUP BY CertificateType.Id', (int)$Record['FabricArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Certificate = array () ;
	    while ($row = dbFetch ($result)) {
		// Locate Certificates for the new supplier
		$query = sprintf ('SELECT Certificate.*, CertificateType.Name AS CertificateTypeName FROM Certificate INNER JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId WHERE Certificate.CertificateTypeId=%d AND Certificate.SupplierCompanyId=%d AND Certificate.Active=1 AND Certificate.ValidUntil>"%s"', (int)$row['Id'], (int)$Record['SupplierCompanyId'], dbDateOnlyEncode(time())) ;
		$result2 = dbQuery ($query) ;
		while ($row = dbFetch ($result2)) {
		    $Certificate[(int)$row['Id']] = $row ;
		}
		dbQueryFree ($result2) ;		
	    }
	    dbQueryFree ($result) ;

	    // Get Components
	    $Components = array () ;
		$query = sprintf ('SELECT component.* FROM Component Where active=1') ;
		$result2 = dbQuery ($query) ;
		while ($row = dbFetch ($result2)) {
		    $Components[(int)$row['Id']] = $row ;
		}
		dbQueryFree ($result2) ;		


	    break ;
    }
 
    return 0 ;
?>
