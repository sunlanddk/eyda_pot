<?php 
require_once('lib/table.inc');

foreach ($_POST['variant'] as $VariantCodeId => $sku) {
	if($sku == ''){
		continue;
	}
	$query = sprintf ("SELECT Id, VariantCode FROM `VariantCode` v WHERE v.Id != %d AND v.Active=1 AND v.SKU='%s' LIMIT 1", $VariantCodeId, $sku) ;
	$result = dbQuery ($query) ;
	$count = dbNumRows ($result) ;
	$row = dbFetch ($result);
	dbQueryFree ($result) ;
	if ($count > 0) return "Sku (".$sku.") Already set on another variant. ( ".$row['VariantCode']." )" ;

	$fields = array(
		'SKU' => $sku
	);

	tableWrite ('variantcode', $fields, $VariantCodeId) ;
}


return 0;
