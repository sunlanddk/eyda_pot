<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    global $User, $Navigation, $Record, $Result2, $Size, $Id ;

    formStart();

    itemStart();
    itemField('Article', tableGetField('article','Number', $Id));
    itemField('Description', tableGetField('article','Description', $Id));
    itemField('Stock', tableGetField('stock','Name', 14321));
    itemSpace();
    itemFieldRaw('Comment', '<input type="text" name="comment" value="" maxlength="100">');
    itemEnd() ;
    echo'<br>'; 

    
    $query = sprintf ("SELECT ArticleSize.Id, ArticleSize.Name, ArticleSize.DisplayOrder
						FROM ArticleSize
						WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1
						ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Id) ;
	$result = dbQuery ($query) ;
	$Sizes = array() ;
	while ($row = dbFetch ($result)) {
		$Sizes[$row['Id']] = $row ;
	}
    dbQueryFree ($result) ;

    $query = sprintf ("SELECT ArticleColor.Id, Color.Description
						FROM ArticleColor
						LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
						WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1
						ORDER BY Color.Description", $Id) ;
	$result = dbQuery ($query) ;
	$Colors = array() ;
	while ($row = dbFetch ($result)) {
		$Colors[$row['Id']] = $row ;
	}
    dbQueryFree ($result) ;
    ?>

    <table class="item">
    	<thead>
    		<tr>
    			<th></th>
    			<?php 
    				foreach ($Sizes as $key => $value) {
    					echo '<th style="text-align:left;">'.$value['Name'].'</th>';
    				}
    			?>
    		</tr>
    	</thead>
    	<tbody>
    		<?php
    		foreach ($Colors as $ckey => $Color) {
    			echo '<tr>';
    				echo '<td>'.$Color['Description'].'</td>';
    				foreach ($Sizes as $key => $size) {
    					$variantcodeid = tableGetFieldWhere('variantcode', 'Id', sprintf("Active=1 AND ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d", $Id, $Color['Id'], $size['Id']));
    					$pickContainerId = (int)tableGetField('variantcode', 'PickContainerId', $variantcodeid);
    					if($pickContainerId > 0){
    						$query = sprintf ("SELECT SUM(i.Quantity) as Qty
													FROM (item i, container c) 
													WHERE
													i.Active=1 AND i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.ContainerId=c.Id AND 
													c.Active=1 AND c.Id NOT IN(%d) AND c.StockId=14321", (int)$Id, (int)$Color['Id'], (int)$size['Id'], (int)$pickContainerId) ;	
							$result = dbQuery ($query) ;
							$StockTotal = dbFetch ($result);
	    					dbQueryFree ($result) ;

	    					$query = sprintf ("SELECT SUM(i.Quantity) as Qty, c.Position
												FROM (item i, container c) 
												WHERE
												i.Active=1 AND i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.ContainerId=%d AND i.ContainerId=c.Id AND 
												c.Active=1 AND c.StockId=14321", (int)$Id, (int)$Color['Id'], (int)$size['Id'], (int)$pickContainerId) ;
	    					$result = dbQuery ($query) ;
							$Stock = dbFetch ($result);
    						dbQueryFree ($result) ;

	    				}
	    				else{
	    					$query = sprintf ("SELECT SUM(i.Quantity) as Qty 
												FROM (item i, container c) 
												WHERE
												i.Active=1 AND i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.ContainerId=c.Id AND 
												c.Active=1 AND c.StockId=14321", (int)$Id, (int)$Color['Id'], (int)$size['Id']) ;	
	    					$result = dbQuery ($query) ;
							$Stock = dbFetch ($result);
    						dbQueryFree ($result) ;
    						$Stock['Position'] = '';
	    					$StockTotal['Qty'] = $Stock['Qty'];
	    					$StockTotal['Qty'] = 0;
	    				}

						
						echo '<td style="text-align:left; padding-bottom: 10px;"><div style="width: 100%; height:12px;">'.$Stock['Position'].'</div><div style="width: 100%;"><input type="hidden" name="varianthidden['.$variantcodeid.']" value="'.(int)$Stock['Qty'].'"><input name="variant['.$variantcodeid.']" value="'.(int)$Stock['Qty'].'"></div><div style="width: 100%;">'.((int)$StockTotal['Qty'] > 0 ? '(' .(int)$StockTotal['Qty']. ')' :  '') .' </div></td>';

    					//echo '<td style="text-align:left;">('.(int)$Stock["Qty"].') '.((int)$StockTotal['Qty'] != (int)$Stock['Qty'] ? '(' .(int)$StockTotal['Qty']. ')' : '(0)' ) .' <input type="hidden" name="varianthidden['.$variantcodeid.']" value="'.(int)$Stock['Qty'].'"><input name="variant['.$variantcodeid.']" value="'.(int)$Stock['Qty'].'"></td>';

    				}
    			echo '</tr>';
    		}
    		?>

    	</tbody>
    </table>

    <style>
    	input{
    		margin: 0;
    		padding: 0;
    		margin-bottom: 5px;
    		width: 45%;
    	}
    </style>


    <?php 
    
    formEnd () ;

    return 0 ;
?>
