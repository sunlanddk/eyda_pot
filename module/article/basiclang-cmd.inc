<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'module/article/include.inc' ;

    $fields = array (
		'DeliveryComment'		=> array (),
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $Id, $fields, $Record, $ArticleTypeFields ;
		$Record = array() ;
		return false ;
    }

    switch ($Navigation['Parameters']) {
		case 'new' :
			unset ($Record) ;
			$Id = -1 ;
			break ;
    }
     
    // Save record
    $res = saveFields ('Article', $Id, $fields, true) ;
    if ($res) return $res ;

	
	// save localized texts.
	foreach ($_POST['Name'] as $key => $value) {
		$_article_lang_id = tableGetFieldWhere('article_lang','Id','LanguageId='. $key .' and ArticleId='. $Id) ;
		if ($_article_lang_id > 0) {
			$_query = sprintf('Update article_lang set Description="%s", WebDescription="%s", OnWebshop="%s" where id=%d', $value, base64_encode($_POST['Desc'][$key]), ($_POST['OnWebshop'][$key]=='on')?1:0, $_article_lang_id) ;
		} else {
			$_query = sprintf('Insert into article_lang set ArticleId=%d, Description="%s", WebDescription="%s", OnWebshop="%s", LanguageId=%d', $Id, $value, base64_encode($_POST['Desc'][$key]), $_POST['OnWebshop'][$key], $key) ;
		}
		dbQuery($_query) ;
	}
	
	
    switch ($Navigation['Parameters']) {
	case 'new' :
	    if ($Record['ArticleTypeProduct']) {
			// Create default version 1 style
			require_once 'lib/table.inc' ;
			$Style = array ('ArticleId' => (int)$Record['Id'], 'Version' => 1, 'Active' => 1) ;
			tableWrite ('Style', $Style) ;
	    }
	    
	    // View new article
	    return navigationCommandMark ('articleview', (int)$Record['Id']) ;
    }
    
    return 0 ;    
?>
