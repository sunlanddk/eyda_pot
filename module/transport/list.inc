<?php

    require_once 'lib/list.inc' ;

     // Filter Bar
    listFilterBar () ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 150) ;
    listHead ('State', 80) ;
    listHead ('Description') ;
    listHead ('Owner', 80) ;
    listHead ('Departure', 80) ;
    listHead ('Arrival', 80) ;
    listHead ('Stock', 80) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'transportview', (int)$row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['State']) ;
	listField ($row['Description']) ;
	listField ($row['Owner']) ;
	listField (date('Y-m-d', dbDateDecode ($row['DepartureDate']))) ;
	listField (date('Y-m-d', dbDateDecode ($row['ArrivalDate']))) ;
	listField (date('Y-m-d', dbDateDecode ($row['StockDate']))) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Transportations', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
