<?php

    $StateField = 'IF(Stock.Done,"Done",IF(NOT Stock.Ready,"Defined",IF(NOT Stock.Departed,"Ready",IF(NOT Stock.Arrived,"Departed",IF(NOT Stock.Verified,"Arrived","Verified"))))) AS State' ;
    $queryFields = 'Stock.*, Company.Name as Owner, ' . $StateField ;

    switch ($Navigation['Function']) {
	case 'list' :
	    switch ($Navigation['Parameters']) {
		case 'all' :
		    // List field specification
		    $fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Departure',		'db' => 'DepartureDate', 'desc' => true),		
			array ('name' => 'Name',		'db' => 'Stock.Name'),
			array ('name' => 'Description',	'db' => 'Stock.Description'),
			array ('name' => 'State',		'db' => $StateField),
			array ('name' => 'Arrival',		'db' => 'ArrivalDate'),		
			array ('name' => 'Stock',		'db' => 'StockDate')		
		    ) ;

		    $queryTables = '(Stock) LEFT JOIN Company ON Company.Id=Stock.FromCompanyId' ;
		    $queryClause = sprintf ('Stock.Type="transport" AND Stock.Active=1 
				AND (Stock.FromCompanyId in (select CompanyId From UserCompanies where UserId=%d) or Stock.FromCompanyId=%d or Stock.FromCompanyId=0)',  $User['Id'], $User['CompanyId']) ;
		    require_once 'lib/list.inc' ;
		    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
		    if (is_string($Result)) return $Result ;
		    return listView ($Result, 'transallview') ;

		    // Query list
		    $query = sprintf ('SELECT %s FROM %s WHERE %s ORDER BY Stock.Name', $queryFields, $queryTables, $queryClause) ;
		    $Result = dbQuery ($query) ;

		    break ;
		default :
		    // Query list - List without Filter
		    $query = sprintf ('SELECT %s FROM (Stock) LEFT JOIN Company ON Company.Id=Stock.FromCompanyId WHERE Stock.Type="transport" AND Stock.Done=0 AND Stock.Active=1 
				AND (Stock.FromCompanyId in (select CompanyId From UserCompanies where UserId=%d) or Stock.FromCompanyId=%d or Stock.FromCompanyId=0)
				ORDER BY Stock.Name', $queryFields,$User['Id'], $User['CompanyId'] ) ;
		    $Result = dbQuery ($query) ;
//		    $queryClause = 'Stock.Type="shipment" AND Stock.Done=0 AND Stock.Active=1' ;
		    break ;
	    }

	    return 0 ;
	    
	case 'basic' :
	case 'basic-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'view' :
	    // Query Stock
	    $query = sprintf ('SELECT %s, COUNT(Container.Id) AS ContainerCount, Company.Name as Owner 
						FROM Stock 
						LEFT JOIN Container ON Container.StockId=Stock.Id AND Container.Active=1 
						LEFT JOIN Company ON Company.Id=Stock.FromCompanyId
						WHERE Stock.Id=%d AND Stock.Type="transport" AND Stock.Active=1 GROUP BY Stock.Id', $queryFields, $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Validate type
	    if ($Record['Type'] != 'transport') return sprintf ('%s(%d) invalid type, value "%s"', __FILE__, __LINE__, $Record['Type']) ;

	    return 0 ;

	case 'revert' :
	case 'revert-cmd' :
	case 'delete-cmd' :
	    // Query Stock
	    $query = sprintf ('SELECT %s, Company.Name as Owner 
						FROM Stock 
						LEFT JOIN Company ON Company.Id=Stock.FromCompanyId
						WHERE Stock.Id=%d AND Stock.Type="transport" AND Stock.Active=1', $queryFields, $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Validate type
	    if ($Record['Type'] != 'transport') return sprintf ('%s(%d) invalid type, value "%s"', __FILE__, __LINE__, $Record['Type']) ;

	    return 0 ;
    }

    return 0 ;
?>
