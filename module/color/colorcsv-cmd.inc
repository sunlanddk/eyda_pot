<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;

	function array_2_csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {
				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
	
	
	global $User, $Navigation, $Record, $Size, $Id ;

	$ErrorTxt = 0 ;

    // header
    // header('Content-Length: '.$size); 
    header ('Content-Type: text/plain') ;
    header ('Content-Disposition: attachment; filename=colorlist.csv') ;
    header ('Last-Modified: '.date('D, j M Y G:i:s T')) ;

	$query = 'SELECT Number, Description
						FROM (color)
						WHERE active=1' ;
	$result = dbQuery ($query) ;

//	$file = fopen("test.txt","w");
	$n = 0 ;
	while ($row = dbFetch ($result)) {
		$csv_data = array_2_csv($row);
		$csv_data .= "\r\n" ;
//		echo $n . ":;; <pre>";
//		fwrite($file,$csv_data.PHP_EOL);
		print_r($csv_data);
//		echo '</pre>'   ; 
		$n++ ;  
	}
	dbQueryFree ($result) ;
//	fclose($file);
	
	return 0 ; // navigationCommandMark ('colorlist', (int)$Record['Id']) ;

?>
