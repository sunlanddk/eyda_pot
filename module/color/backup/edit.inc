<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Number</td><td><input type=text name=Number maxlength=10 size=10' value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td>%s</td></tr>\n", htmlDBSelect ("ColorGroupId style='width:150px'", $Record['ColorGroupId'], 'SELECT Id, Name AS Value FROM ColorGroup WHERE Active=1 ORDER BY Name')) ;  
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
