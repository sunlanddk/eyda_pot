<?php

    require_once 'lib/save.inc' ;
    require_once 'module/color/include.inc' ;

    $AutoFill = false ;

    $fields = array (
	'Number'		=> array ('mandatory' => true,		'check' => true),
	'Description'		=> array (),
	'ColorGroupId'		=> array ('mandatory' => true, 		'type' => 'integer') 
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }
     
    function checkfield ($fieldname, $value) {
	global $Id, $fields, $AutoFill ;
	switch ($fieldname) {
	    case 'Number':
		$AutoFill = colorNumberFill ($value) ;
		if (is_string($AutoFill)) return $AutoFill ;

		// Check that Number does not allready exist
		$query = sprintf ('SELECT Id FROM Color WHERE Active=1 AND Number="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Color allready existing' ;

		// Save Color number
		$fields[$fieldname]['value'] = $value ;

		return true ;
	}
	return false ;
    }

    $res = saveFields ('Color', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new article
		break;	
	    if ($AutoFill) return navigationCommandMark ('coloredit', (int)$Record['Id']) ;
    }
    
    return 0 ;
?>
