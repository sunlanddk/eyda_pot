<?php

    require_once 'lib/navigation.inc' ;
    
    switch ($Navigation['Function']) {
	case 'list' :
	    // List colors
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Color.Number'),
		array ('name' => 'Description',		'db' => 'Color.Description'),
		array ('name' => 'Group',		'db' => 'ColorGroup.Name')
	    ) ;
	    $queryFields = 'Color.*, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen' ;
	    $queryTables = 'Color LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId' ;
	    $queryClause = 'Color.Active=1' ;
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
	    if (is_string($Result)) return $Result ;
	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	case 'delete-cmd' :
	    $query = sprintf ("SELECT Color.*, 
							(select Description From Color_Lang where ColorId=Color.Id  and LanguageId=4) as DescDK, 
							(select Description From Color_Lang where ColorId=Color.Id  and LanguageId=5) as DescDE,						
							(select Description From Color_Lang where ColorId=Color.Id  and LanguageId=6) as DescUK, 
							(select LongDescription From Color_Lang where ColorId=Color.Id  and LanguageId=4) as LongDescDK, 
							(select LongDescription From Color_Lang where ColorId=Color.Id  and LanguageId=5) as LongDescDE,						
							(select LongDescription From Color_Lang where ColorId=Color.Id  and LanguageId=6) as LongDescUK 
							FROM Color WHERE Color.Id=%d AND Color.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
