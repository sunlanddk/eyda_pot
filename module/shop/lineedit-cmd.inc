<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/uts.inc' ;

    $fields = array (
	'No'			=> array ('type' => 'integer',				'check' => true),

	'Description'		=> array (),

	'ArticleColorId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'PriceCost'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
	'PriceSale'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '12.2'),
	'Discount'		=> array ('type' => 'integer',	'mandatory' => true),
	
	'DeliveryDate'		=> array ('type' => 'date',				'check' => true),
	'Quantity'		=> array ('type' => 'decimal',				'check' => true),
	'Surplus'		=> array ('type' => 'integer'),
	
	'InvoiceFooter'		=> array ()
   ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Size, $Id, $fields ;
	switch ($fieldname) {
	    case 'No' :
		if (!$changed) return false ;

		// Validate number
		$query = sprintf ('SELECT Id FROM OrderLine WHERE OrderId=%d AND Active=1 AND Id<>%d', (int)$Record['OrderId'], (int)$Record['Id']) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($value <= 0 or $value > ($count+1)) return sprintf ('invalid OrderLine Position number, max %d', $count+1) ;

		return true ;

	    case 'ArticleColorId' :
		if (!$changed) return false ;

		// Validate number
		$query = sprintf ('SELECT * FROM ArticleColor WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] <= 0) return sprintf ('%s(%d) not found, id %d', __FILE__, __LINE__, $value) ;
		if ((int)$row['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) ArticleColor mismatch, id %d', __FILE__, __LINE__) ;

		return true ;
		
	    case 'DeliveryDate' :
		if (!$changed) return false ;

		// Validate
		$t = dbDateDecode ($value) ;
		if ($t < utsDateOnly(time())) return 'Delivery Date can not be in the past' ;

		return true ;
		
	    case 'Quantity' :
		if (!$changed) return false ;
		
		// Validate
		if ($value <= 0) return 'invalid Quantity' ;

		return true ;
	}
	
	return false ;	
    }
    
    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    // Validate state of Order
    if ($Record['OrderDone']) return 'the OrderLine can not be modified when Order is Done' ;
    if ($Record['OrderReady']) return 'the OrderLine can not be modified when Order is Ready' ;
 
    if (!$Record['VariantColor']) {
	unset ($fields['ArticleColorId']) ;
    }
    
    if ($Record['VariantSize']) {
	// Generat verify format
	$format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", 9-(int)$Record['UnitDecimals'], 9-(int)$Record['UnitDecimals'], (int)$Record['UnitDecimals']) ;

	$Quantity = 0 ;
	foreach ($Size as $i => $s) {
	    $value_string = trim($_POST['Quantity'][(int)$s['Id']]) ;
	    if ($value_string == '') $value_string = '0' ;

	    // verify format
	    if (!ereg($format,$value_string)) return sprintf ("invalid quantity for %s", $s['Name']) ;
	    
	    // Substitute , -> .
	    $value_string = str_replace (',', '.', $value_string) ;
	    
	    $Size[$i]['Value'] = $value_string ;
	    $Quantity += (float)$value_string ;
	}

	// Set total quantity for orderline
	$fields['Quantity'] = array ('type' => 'set', 'value' => number_format ($Quantity, (int)$Record['UnitDecimals'], '.', '')) ;
    }
        
    // Process and save OrderLine
    $res = saveFields ('OrderLine', $Id, $fields, true) ;
    if ($res) return $res ;

    if ($Record['VariantSize']) {
	// Update Quantities by Sizes
	foreach ($Size as $i => $s) {
	    if ((float)$s['Quantity'] != (float)$s['Value']) {
		if ((int)$s['OrderQuantityId'] > 0) {
		    if ((float)$s['Value'] > 0) {
			// Update existing record
			$fields = array (
			    'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
			) ;
			$res = saveFields ('OrderQuantity', (int)$s['OrderQuantityId'], $fields) ;
		    } else {
			// Delete record
			tableDelete ('OrderQuantity', (int)$s['OrderQuantityId']) ;
		    }
		} else {
		    // Create new record
		    $fields = array (
			'OrderLineId'		=> array ('type' => 'set',	'value' => (int)$Record['Id']),
			'ArticleSizeId'		=> array ('type' => 'set',	'value' => (int)$s['Id']),
			'Quantity'		=> array ('type' => 'set',	'value' => $s['Value']) 
		    ) ;
		    $res = saveFields ('OrderQuantity', -1, $fields) ;
		}
		if ($res) return $res ;
	    }			    
	}
    }
    
    // Renumber other entries
    $query = sprintf ('SELECT Id, No FROM OrderLine WHERE OrderId=%d AND Active=1 AND Id<>%d ORDER BY No', (int)$Record['OrderId'], (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i == (int)$Record['No']) $i += 1 ;       
	if ($i != (int)$row['No']) {
	    $query = sprintf ("UPDATE OrderLine SET No=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;
    
    return 0 ;    
?>
