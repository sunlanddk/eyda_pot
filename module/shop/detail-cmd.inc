<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'CustomsPositionId'	=> array ('type' => 'integer'),
	'MaterialCountryId'	=> array ('type' => 'integer'),
	'KnitCountryId'		=> array ('type' => 'integer'),
	'WorkCountryId'		=> array ('type' => 'integer'),
	'VariantColor'		=> array ('type' => 'checkbox'),
	'VariantDimension'	=> array ('type' => 'checkbox'),
	'VariantSize'		=> array ('type' => 'checkbox'),
	'VariantSortation'	=> array ('type' => 'checkbox'),
	'VariantCertificate'	=> array ('type' => 'checkbox'),
	'PriceDimensionAdjust'	=> array ('type' => 'checkbox'),
	'PrintNumber'		=> array (),
	'FabricArticleNumber'	=> array ('check' => true),
	'FabricArticleId'	=> array ('type' => 'set')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $fields, $Record, $ArticleTypeFields ;
	switch ($fieldname) {
	    case 'FabricArticleNumber':
		if ($value == '') {
		    // No Fabric
		    $fields['FabricArticleId']['value'] = 0 ;
		    return false ;
		}
		
		// Validate articlenumber
		if (strlen($value) != 10) return 'invalid Article number for Fabric' ;

		// Ensure that Article number not allready are used
		$query = sprintf ('SELECT Article.Id, %s FROM Article LEFT JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Number="%s" AND Article.Active=1', $ArticleTypeFields, addslashes($value)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] <= 0) return 'Article for Fabric not found' ;
		if (!$row['ArticleTypeFabric']) return 'Article for Fabric has wrong type' ;
		
		// Set Id for fabrics
		$fields['FabricArticleId']['value'] = (int)$row['Id'] ;
		return false ;
	    
	}
    }
    
    // Select fields to process
    if (!$Record['ArticleTypeProduct']) unset ($fields['FabricArticleNumber']) ;

    // Check if document upload was succesfull
    if (!$Record['ArticleTypeProduct']) {
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return 'sketch upload failed, file not uploaded' ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return 'invalid sketch size' ;
		if ($_FILES['Doc']['size'] > 50000)
		    return 'sketch too big, max 50 kB' ;

		// Set tracking information
		$fields['SketchUserId'] = array('type' => 'set', 'value' => $User['Id']) ;
		$fields['SketchDate'] = array('type' => 'set', 'value' => dbDateEncode(time())) ;
		$fields['SketchType'] = array('type' => 'set', 'value' => $_FILES['Doc']['type']) ;

		// Set flag for Sketch upload	
		$SketchUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$SkecthUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	}
    }

    // Save record
    $res = saveFields ('Article', $Id, $fields) ;
    if ($res) return $res ;

    // Check if sketch to be archieved
    if (!$Record['ArticleTypeProduct'] and $SketchUpload) {
	// Archive sketch
	require_once 'lib/file.inc' ;
	$file = fileName ('article', $Id, true) ;
	if (!move_uploaded_file ($_FILES['Doc']['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive document', __FILE__, __LINE__) ;
	chmod ($file, 0640) ;
    }

    return 0 ;
?>
