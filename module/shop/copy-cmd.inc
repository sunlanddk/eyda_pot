<?php

    define ('DEBUG', 0) ;

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/article/include.inc' ;

    $fields = array (
	'Number'		=> array (),
	'Details'		=> array ('type' => 'checkbox'),
	'Price'			=> array ('type' => 'checkbox'),
	'Color'			=> array ('type' => 'checkbox'),
	'Size'			=> array ('type' => 'checkbox'),
	'Certificate'		=> array ('type' => 'checkbox'),
	'Composition'		=> array ('type' => 'checkbox'),
	'Style'			=> array ('type' => 'checkbox'),
    ) ;

    // Save fields
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
        
    // Partitial specified Article number ?
    $ArticleNumber = $fields['Number']['value'] ;
    $res = articleNumberFill ($ArticleNumber) ;
    if (is_string ($res)) return $res ;

    // Ensure that new Article number not allready are used
    $query = sprintf ('SELECT Id FROM Article WHERE Number="%s" AND Active=1', addslashes($ArticleNumber)) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Article number allready in use" ;

    // Get Article type
    $query = sprintf ('SELECT ArticleType.Id, %s FROM ArticleType WHERE LeadDigit="%s" AND Active=1', $ArticleTypeFields, addslashes(substr($ArticleNumber,0,1))) ;
    $result = dbQuery ($query) ;
    $ArticleType = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$ArticleType['Id'] <= 0) return sprintf ('article type not known, digit "%s"', substr($ArticleNumber,0,1)) ;
    if ((int)$ArticleType['Id'] != (int)$Record['ArticleTypeId']) return 'the new Article must have the same type' ;
  
    // Get article to copy
    $query = sprintf ('SELECT Article.* FROM Article WHERE Id=%d AND Active=1', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $Article = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$ArticleType['Id'] <= 0) return sprintf ('article type not known, digit "%s"', substr($ArticleNumber,0,1)) ;

    // Modify Article
    $Article['Number'] = $ArticleNumber ;
    if ($ArticleType['ArticleTypeProduct']) {
	$Article['SupplierNumber'] = $ArticleNumber ;
    }

    // Details
    if (!$fields['Details']['value']) {
	unset ($Article['CustomsPositionId']) ;
	unset ($Article['WorkCountryId']) ;
	unset ($Article['MaterialCountryId']) ;
	unset ($Article['KnitCountryId']) ;
	unset ($Article['FabricArticleId']) ;
	unset ($Article['PrintNumber']) ;
    }    
    $Article['Id'] = tableWrite ('Article', $Article) ;  
    if (DEBUG) logPrintVar ($Article, 'Article') ;

    // Prices
    if ($fields['Price']['value']) {
	// Lookup Prices to Copy
	$query = sprintf ('SELECT ArticlePrice.* FROM ArticlePrice WHERE ArticlePrice.ArticleId=%d AND ArticlePrice.Active=1', (int)$Record['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = $Article['Id'] ;
	    $row['Id'] = tableWrite ('ArticlePrice', $row) ;
	    if (DEBUG) logPrintVar ($row, 'ArticlePrice') ;
	}
	dbQueryFree ($result) ;
    }
    
    // Colors
    if ($Record['VariantColor'] and $fields['Color']['value']) {
	// Copy Colors
	$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1', (int)$Record['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = $Article['Id'] ;
	    $row['Id'] = tableWrite ('ArticleColor', $row) ;
	    if (DEBUG) logPrintVar ($row, 'ArticleColor') ;
	}
	dbQueryFree ($result) ;
    }

    // Sizes
    if ($Record['VariantSize'] and $fields['Size']['value']) {
	// Copy Sizes
	$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1', (int)$Record['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = $Article['Id'] ;
	    $row['Id'] = tableWrite ('ArticleSize', $row) ;
	    if (DEBUG) logPrintVar ($row, 'ArticleSize') ;
	}
	dbQueryFree ($result) ;
    }

    // Certificates
    if ($Record['VariantCertificate'] and $fields['Certificate']['value']) {
	// Copy Certificates
	$query = sprintf ('SELECT ArticleCertificate.* FROM ArticleCertificate WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1', (int)$Record['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = $Article['Id'] ;
	    $row['Id'] = tableWrite ('ArticleCertificate', $row) ;
	    if (DEBUG) logPrintVar ($row, 'ArticleCertificate') ;
	}
	dbQueryFree ($result) ;
    }

    // Copy Composition
    if ($fields['Composition']['value']) {
	$query = sprintf ('SELECT ArticleComponent.* FROM ArticleComponent WHERE ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1', (int)$Record['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = $Article['Id'] ;
	    $row['Id'] = tableWrite ('ArticleComponent', $row) ;
	    if (DEBUG) logPrintVar ($row, 'ArticleComponent') ;
	}
	dbQueryFree ($result) ;
    }

    if ($Record['ArticleTypeProduct']) {
	// Style
      	if ($fields['Style']['value']) {
	    // Get Style
	    $query = sprintf ('SELECT Style.* FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 ORDER BY Style.Approved DESC, Style.Ready DESC, Style.Version DESC LIMIT 1', (int)$Record['Id']) ;
	    $result = dbQuery ($query) ;
	    $StyleTemplate = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if ((int)$StyleTemplate['Id'] > 0) {
		// Get PDM record
		$query = sprintf ("SELECT StylePDM.* FROM StylePDM WHERE StylePDM.StyleId=%d AND StylePDM.Active=1 ORDER BY StylePDM.Version DESC LIMIT 1", (int)$StyleTemplate['Id']) ;
		$res = dbQuery ($query) ;
		$StylePDMTemplate = dbFetch ($res) ;
		dbQueryFree ($res) ;	    

		if (DEBUG) printf ("Style Id %d, Version %d, Approved %d, Ready %d, PDM Version %d<br>\n", (int)$StyleTemplate['Id'], (int)$StyleTemplate['Version'], $StyleTemplate['Approved'], $StyleTemplate['Ready'], (int)$StylePDMTemplate['Version']) ;

		// Create new Style
		$Style = $StyleTemplate ;
		$Style['ArticleId'] = $Article['Id'] ;
		$Style['Version'] = 1 ;
		$Style['PatternMade'] = 0 ;
		$Style['Ready'] = 0 ;
		$Style['Approved'] = 0 ;
		$Style['Id'] = tableWrite ('Style', $Style) ;
		if (DEBUG) logPrintVar ($Style, 'Style') ;

		// Generate Log
		$s = sprintf ("The Style has been copied from Article %s\nStyle version %d", $Record['Number'], $StyleTemplate['Version']) ;
		if ((int)$StylePDMTemplate['Id'] > 0) $s .= sprintf ("\nPDM Version %d", $StylePDMTemplate['Version']) ;
		if ($StyleTemplate['Ready']) $s .= sprintf ("\nApproved by constructor, %s at %s", tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$StyleTemplate['ReadyUserId']), date('Y.m.d H:i:s', dbDateDecode($StyleTemplate['ReadyDate']))  ) ;
		if ($StyleTemplate['Approved']) $s .= sprintf ("\nApproved by sales, %s at %s", tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$StyleTemplate['ApprovedUserId']), date('Y.m.d H:i:s', dbDateDecode($StyleTemplate['ApprovedDate']))) ;
		$StyleLog = array (
		    'StyleId' => $Style['Id'],
		    'Header' => 'Created',
		    'Text' => $s
		) ;
		$StyleLog['Id'] = tableWrite ('StyleLog', $StyleLog) ;
		if (DEBUG) logPrintVar ($StyleLog, 'StyleLog') ;

		// Copy sketch
		if (is_file(fileName('sketch', (int)$StyleTemplate['Id']))) {
		    copy (fileName('sketch',(int)$StyleTemplate['Id']), fileName('sketch',$Style['Id'], true)) ;
		    chmod (fileName('sketch',$Style['Id']), 0640) ;
		}

		// Copy all operations
		$query = sprintf ('SELECT * FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No', (int)$StyleTemplate['Id']) ;
		$result = dbQuery ($query) ;
		while ($row = dbFetch ($result)) {
		    $row['StyleId'] = (int)$Style['Id'] ;
		    $row['Id'] = tableWrite ('StyleOperation', $row) ;
		    if (DEBUG) logPrintVar ($row, 'StyleOperation') ;
		}
		dbQueryFree ($result) ;

		if ((int)$StylePDMTemplate['Id'] > 0) {
		    // Create new PDM record
		    $StylePDM = $StylePDMTemplate ;
		    $StylePDM['Version'] = 1 ;
		    $StylePDM['StyleId'] = $Style['Id'] ;
		    $StylePDM['StyleLogId'] = $StyleLog['Id'] ;
		    $StylePDM['Id'] = tableWrite ('StylePDM', $StylePDM) ;
		    if (DEBUG) logPrintVar ($StylePDM, 'StylePDM') ;

		    // Copy PDM Scheed
		    copy (fileName('pdm',(int)$StylePDMTemplate['Id']), fileName('pdm',(int)$StylePDM['Id'], true)) ;
		    chmod (fileName('pdm',(int)$StylePDM['Id']), 0640) ;
		}
	    }
	}
    } else {
        if ($fields['Details']['value']) {
	   // Copy Article Sketch
	   if (is_file(fileName('article', (int)$Record['Id']))) {
		copy (fileName('article',(int)$Record['Id']), fileName('article',$Article['Id'], true)) ;
		chmod (fileName('article',$Article['Id']), 0640) ;
	    }
	}
    }
   
    // View new Case
    return navigationCommandMark ('articleview', (int)$Article['Id']) ;
?>
