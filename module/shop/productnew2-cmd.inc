<?php

    define ('DEBUG', 0) ;

    require_once 'lib/string.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/log.inc' ;

    // Initialize
    $CertificateId = 0 ;
    
    // Remove Colors not selected
    foreach ($Color as $i => $row) {
	if (!strCheckBox($_POST['Color'][$i])) unset ($Color[$i]) ;
    }

    // Check if any Certificate selected for the Case
    foreach ($Certificate as $i => $row) {
	if (strCheckBox($_POST['CertificateCase'][$i])) {
	    if ($CertificateId > 0) return 'more than one Certificate selected for Case' ;
	    if (!strCheckBox($_POST['Certificate'][$i])) return 'Certificate selected for Case must Also be selected for Article' ;
	    $CertificateId = $i ;
	}
    }

    // Remove Certificates not selected
    foreach ($Certificate as $i => $row) {
	if (!strCheckBox($_POST['Certificate'][$i])) unset ($Certificate[$i]) ;
    }
    
    // Automatic selection of Certificate for Case
    if ($CertificateId == 0 and count($Certificate) == 1) {
	$row = reset($Certificate) ;
	$CertificateId = (int)$row['Id'] ;
    }

    // Check that Certificate has been selected
    if ($CertificateId == 0 and count($Certificate) > 1) return 'please select Certificate for Case' ;

    // Colors
    if (count($Color) > 0) {
	// Make list of ArticleColorId
	$s = '' ;
	foreach ($Color as $i => $row) {
	    if ($s != '') $s .= ',' ;
	    $s .= $i ;
	}
	
	// Copy Colors
	$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.Id IN (%s)', $s) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = (int)$Record['ArticleId'] ;
	    $row['Id'] = tableWrite ('ArticleColor', $row) ;
    	}
	dbQueryFree ($result) ;
    }
    
    // Certificates
    if (count($Certificate) > 0) {
	// Update Article to have Certificates
	$Article = array (
	    'VariantCertificate' => 1
	) ;
	$Article['Id'] = tableWrite ('Article', $Article, (int)$Record['ArticleId']) ;  

	foreach ($Certificate as $i => $row) {
    	    $ArticleCertificate = array (
		'ArticleId'		=> (int)$Record['ArticleId'],
		'CertificateId'		=> $i
	    ) ;
	    $Certificate[$i]['ArticleCertificateId'] = tableWrite ('ArticleCertificate', $ArticleCertificate) ;
	}

	if ($CertificateId > 0) {
	    // Set selected Certificate for Case
	    $Case = array (
		'ArticleCertificateId' =>	$Certificate[$CertificateId]['ArticleCertificateId']
	    ) ;    
	    $Case['Id'] = tableWrite ('Case', $Case, (int)$Record['Id']) ;
	}
    }
    
    // View new Case
    return navigationCommandMark ('caseview', (int)$Record['Id']) ;
?>
