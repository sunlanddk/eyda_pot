<?php

    require_once 'lib/list.inc' ;
	



    // Filter Bar
    listFilterBar () ;
    
    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Article', 80) ;
    listHead ('Description') ;
    listHead ('Reference', 90) ;
    listHead ('Supplier', 90) ;
    listHead ('Name') ;
    listHead ('Print', 90) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'addarticle', $row['Id']) ;
	listField ($row['Number']) ;
	listField ($row['Description']) ;
	listField ($row['SupplierNumber']) ;
	listField ($row['SupplierCompanyNumber']) ;
	listField ($row['SupplierCompanyName']) ;
	listField ($row['PrintNumber']) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Articles', 'colspan=2') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
