<?php

    define ('DEBUG', 0) ;

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/article/include.inc' ;

    $fields = array (
	'Number'		=> array (			'mandatory' => true),
	'Description'		=> array (),
	'SupplierCompanyId'	=> array ('type' => 'integer',	'mandatory' => true),
	'CustomerCompanyId'	=> array ('type' => 'integer',	'mandatory' => true),
	'CustomerReference'	=> array (),
	'CaseDescription'	=> array (),
	'SizeSetId'		=> array ('type' => 'integer'),
	'FabricNumber'		=> array (),
	'StyleNumber'		=> array (),
	'Comment'		=> array ()
    ) ;

    // Initialize
    $Case = array () ;
    $Article = array () ;
    
    // Save fields
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
    
    // Partitial specified Article number ?
    $ArticleNumber = $fields['Number']['value'] ;
    $res = articleNumberFill ($ArticleNumber) ;
    if (is_string ($res)) return $res ;

    // Ensure that Article number not allready are used
    $query = sprintf ('SELECT Id FROM Article WHERE Number="%s" AND Active=1 AND Id<>%d', addslashes($ArticleNumber), $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "Article number allready in use" ;

    // Get Article type
    $query = sprintf ('SELECT ArticleType.Id, %s FROM ArticleType WHERE LeadDigit="%s" AND Active=1', $ArticleTypeFields, addslashes(substr($ArticleNumber,0,1))) ;
    $result = dbQuery ($query) ;
    $ArticleType = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$ArticleType['Id'] <= 0) return sprintf ('article type not known, digit "%s"', substr($ArticleNumber,0,1)) ;

    // Validate supplier Company
    if ($fields['SupplierCompanyId']['value'] <= 0) return 'please select supplier Company' ;
    $query = sprintf ('SELECT Company.* FROM Company WHERE Company.Id=%d AND Company.Active=1', $fields['SupplierCompanyId']['value']) ;
    $result = dbQuery ($query) ;
    $SupplierCompany = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$SupplierCompany['Id'] != $fields['SupplierCompanyId']['value']) return 'supplier Company not found' ;
    
    // Validate customer Company
    if ($fields['CustomerCompanyId']['value'] <= 0) return 'please select customer Company' ;
    $query = sprintf ('SELECT Company.* FROM Company WHERE Company.Id=%d AND Company.Active=1', $fields['CustomerCompanyId']['value']) ;
    $result = dbQuery ($query) ;
    $CustomerCompany = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$CustomerCompany['Id'] != $fields['CustomerCompanyId']['value']) return 'customer Company not found' ;

    // Validate SizeSet
    if ($fields['SizeSetId']['value'] > 0) {
	$query = sprintf ('SELECT SizeSet.* FROM SizeSet WHERE SizeSet.Id=%d AND SizeSet.Active=1', $fields['SizeSetId']['value']) ;
	$result = dbQuery ($query) ;
	$SizeSet = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$SizeSet['Id'] != $fields['SizeSetId']['value']) return 'SizeSet not found' ;
    } else {
	$SizeSet = array () ;
    }

    // Get Fabric Article
    if ($fields['FabricNumber']['value'] != '') {
	$query = sprintf ('SELECT Article.*, %s FROM Article INNER JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Number="%s" AND Article.Active=1', $ArticleTypeFields, addslashes($fields['FabricNumber']['value'])) ;
	$result = dbQuery ($query) ;
	$FabricArticle = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$FabricArticle['Id'] <= 0) return 'Article for Fabric not found' ;
	if (!$FabricArticle['ArticleTypeFabric']) return 'Article for Fabric has wrong type' ;
    } else {
	$FabricArticle = array () ;
    }

    // Get Style templete Article
    if ($fields['StyleNumber']['value'] != '') {
	$query = sprintf ('SELECT Article.*, %s FROM Article INNER JOIN ArticleType ON ArticleType.Id=Article.ArticleTypeId WHERE Article.Number="%s" AND Article.Active=1', $ArticleTypeFields, addslashes($fields['StyleNumber']['value'])) ;
	$result = dbQuery ($query) ;
	$StyleArticle = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$StyleArticle['Id'] <= 0) return 'Article for Style-template not found' ;
	if (!$StyleArticle['ArticleTypeProduct']) return 'Article for Style-template has wrong type' ;
    } else {
	$StyleArticle = array () ;
    }

    // Get unit for Pieces
    $query = sprintf ('SELECT Unit.* FROM Unit WHERE Unit.Id=%d AND Unit.Active=1', parameterGet ('UnitPieces')) ;
    $result = dbQuery ($query) ;
    $Unit = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Unit['Id'] <= 0) return 'Unit not found, check Parameter "UnitPieces"' ;
    
    // Create Article
    $Article = array (
	'Number' =>		$ArticleNumber,
	'Description' =>	$fields['Description']['value'],
	'ArticleTypeId' =>	(int)$ArticleType['Id'],
	'UnitId' =>		(int)$Unit['Id'],
	'VariantColor' =>	1,
	'VariantSize' =>	1,
	'VariantSortation' =>	1,
	'VariantCertificate' => 0,
	'FabricArticleId' =>	(int)$FabricArticle['Id'],
	'MaterialCountryId' =>	(int)$FabricArticle['MaterialCountryId'],
	'WorkCountryId' =>	(int)$FabricArticle['WorkCountryId'],
	'KnitCountryId' =>	(int)$FabricArticle['KnitCountryId'],
	'CustomsPositionId' =>	(int)$StyleArticle['CustomsPositionId'],
    	'SupplierCompanyId' =>	(int)$SupplierCompany['Id'],
	'SupplierNumber' =>	$ArticleNumber,
	'Comment' =>		$fields['Comment']['value']
    ) ;
    $Article['Id'] = tableWrite ('Article', $Article) ;  
    if (DEBUG) logPrintVar ($Article, 'Article') ;

    // Generate Sizes
    if ((int)$SizeSet['Id'] > 0) {
	// Lookup Sizes in SizeSet
	$query = sprintf ('SELECT SizeValue.* FROM SizeValue WHERE SizeValue.SizeSetId=%d AND SizeValue.Active=1 ORDER BY SizeValue.DisplayOrder', (int)$SizeSet['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $ArticleSize = array (
		'Name'		=> $row['Name'],
		'DisplayOrder'	=> (int)$row['DisplayOrder'],
		'ArticleId'	=> $Article['Id']
	    ) ;
	    $ArticleSize['Id'] = tableWrite ('ArticleSize', $ArticleSize) ;
	    if (DEBUG) logPrintVar ($ArticleSize, 'ArticleSize') ;
	}
	dbQueryFree ($result) ;
    }

    // Collect information from Fabric
    if ((int)$FabricArticle['Id'] > 0) {
	// Copy Components
	$query = sprintf ('SELECT ArticleComponent.* FROM ArticleComponent WHERE ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1', (int)$FabricArticle['Id']) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $row['ArticleId'] = $Article['Id'] ;
	    $row['Id'] = tableWrite ('ArticleComponent', $row) ;
	    if (DEBUG) logPrintVar ($row, 'ArticleComponent') ;
	}
	dbQueryFree ($result) ;
    }

    // Collect information from Style-template Article
    if ((int)$StyleArticle['Id'] > 0) {
	// Get Style
	$query = sprintf ('SELECT Style.* FROM Style WHERE Style.ArticleId=%d AND Style.Active=1 ORDER BY Style.Approved DESC, Style.Ready DESC, Style.Version DESC LIMIT 1', (int)$StyleArticle['Id']) ;
	$result = dbQuery ($query) ;
	$StyleTemplate = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$StyleTemplate['Id'] > 0) {
	    // Get PDM record
	    $query = sprintf ("SELECT StylePDM.* FROM StylePDM WHERE StylePDM.StyleId=%d AND StylePDM.Active=1 ORDER BY StylePDM.Version DESC LIMIT 1", (int)$StyleTemplate['Id']) ;
	    $res = dbQuery ($query) ;
	    $StylePDMTemplate = dbFetch ($res) ;
	    dbQueryFree ($res) ;	    

	    if (DEBUG) printf ("Style Id %d, Version %d, Approved %d, Ready %d, PDM Version %d<br>\n", (int)$StyleTemplate['Id'], (int)$StyleTemplate['Version'], $StyleTemplate['Approved'], $StyleTemplate['Ready'], (int)$StylePDMTemplate['Version']) ;

	    // Create new Style
	    $Style = $StyleTemplate ;
	    $Style['ArticleId'] = $Article['Id'] ;
	    $Style['Version'] = 1 ;
	    $Style['PatternMade'] = 0 ;
	    $Style['Ready'] = 0 ;
	    $Style['Approved'] = 0 ;
	    $Style['Id'] = tableWrite ('Style', $Style) ;
	    if (DEBUG) logPrintVar ($Style, 'Style') ;

	    // Generate Log
	    $s = sprintf ("The Style has been created from Article %s\nStyle version %d", $StyleArticle['Number'], $StyleTemplate['Version']) ;
	    if ((int)$StylePDMTemplate['Id'] > 0) $s .= sprintf ("\nPDM Version %d", $StylePDMTemplate['Version']) ;
	    if ($StyleTemplate['Ready']) $s .= sprintf ("\nApproved by constructor, %s at %s", tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$StyleTemplate['ReadyUserId']), date('Y.m.d H:i:s', dbDateDecode($StyleTemplate['ReadyDate']))  ) ;
	    if ($StyleTemplate['Approved']) $s .= sprintf ("\nApproved by sales, %s at %s", tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$StyleTemplate['ApprovedUserId']), date('Y.m.d H:i:s', dbDateDecode($StyleTemplate['ApprovedDate']))) ;
	    $StyleLog = array (
		'StyleId' => $Style['Id'],
		'Header' => 'Created',
		'Text' => $s
	    ) ;
	    $StyleLog['Id'] = tableWrite ('StyleLog', $StyleLog) ;
	    if (DEBUG) logPrintVar ($StyleLog, 'StyleLog') ;

	    // Copy sketch
	    if (is_file(fileName('sketch', (int)$StyleTemplate['Id']))) {
		copy (fileName('sketch',(int)$StyleTemplate['Id']), fileName('sketch',$Style['Id'], true)) ;
		chmod (fileName('sketch',$Style['Id']), 0640) ;
	    }

	    // Copy all operations
	    $query = sprintf ('SELECT * FROM StyleOperation WHERE StyleOperation.StyleId=%d AND StyleOperation.Active=1 ORDER BY StyleOperation.No', (int)$StyleTemplate['Id']) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
		$row['StyleId'] = (int)$Style['Id'] ;
		$row['Id'] = tableWrite ('StyleOperation', $row) ;
		if (DEBUG) logPrintVar ($row, 'StyleOperation') ;
	    }
	    dbQueryFree ($result) ;

	    if ((int)$StylePDMTemplate['Id'] > 0) {
		// Create new PDM record
		$StylePDM = $StylePDMTemplate ;
		$StylePDM['Version'] = 1 ;
		$StylePDM['StyleId'] = $Style['Id'] ;
		$StylePDM['StyleLogId'] = $StyleLog['Id'] ;
		$StylePDM['Id'] = tableWrite ('StylePDM', $StylePDM) ;
		if (DEBUG) logPrintVar ($StylePDM, 'StylePDM') ;

		// Copy PDM Scheed
		copy (fileName('pdm',(int)$StylePDMTemplate['Id']), fileName('pdm',(int)$StylePDM['Id'], true)) ;
		chmod (fileName('pdm',(int)$StylePDM['Id']), 0640) ;
	    }
	}
    } else {
	// No style template specified
	// Create empty style
	$Style = array (
	    'ArticleId'			=> $Article['Id'],
	    'Version'			=> 1 
	) ;
	$Style['Id'] = tableWrite ('Style', $Style) ;
    }
   
    // Create Case
    $Case = array (
	'ArticleId' => 		$Article['Id'],
	'CompanyId' =>		(int)$CustomerCompany['Id'],
	'CustomerReference' =>	$fields['CustomerReference']['value'],
	'Description' =>	$fields['CaseDescription']['value']
    ) ;    
    $Case['Id'] = tableWrite ('Case', $Case) ;
    if (DEBUG) logPrintVar ($Case, 'Case') ;

    if ((int)$FabricArticle['Id'] > 0) {
	// Step 2
	return navigationCommandMark ('productnew2', (int)$Case['Id']) ;
    } else {
	// View new Case
	return navigationCommandMark ('caseview', (int)$Case['Id']) ;
    }
?>
