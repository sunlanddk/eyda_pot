<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
	
	$_POST['ArticleNumber'] = 42; //hack

    // Validate
    if ($Record['Done']) return 'the OrderLine can not be added when Order is Done' ;
    if ($Record['Ready']) return 'the OrderLine can not be added when Order is Ready' ;
 
    // Find next position
    $query = sprintf ('SELECT MAX(No) AS No FROM OrderLine WHERE OrderId=%d AND Active=1', (int)$Record['Id']) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $No = (int)$row['No'] ;

    // Default DeliveryDate
    if ($No == 0) {
	// No existing OrderLine to default to
	unset ($row) ;
	$row['DeliveryDate'] = dbDateEncode (time() + 60*60*24*60) ;
    } else {
	// Default to DeliveryDate in last OrderLine
	$query = sprintf ('SELECT * FROM OrderLine WHERE OrderId=%d AND No=%d AND Active=1', (int)$Record['Id'], $No) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
    }

    $fields = array (
	'CaseId'		=> array ('type' => 'integer',		'check' => true),
	'ArticleNumber'		=> array (				'check' => true),
	'ArticleId'		=> array ('type' => 'set'),
	'Description'		=> array ('type' => 'set'),
	'DeliveryDate'		=> array ('type' => 'set',		'value' => $row['DeliveryDate']),
	'Surplus'		=> array ('type' => 'set',		'value' => $row['Surplus']),
	'OrderId'		=> array ('type' => 'set',		'value' => (int)$Record['Id']),
	'No'			=> array ('type' => 'set',		'value' => $No + 1)
    ) ;
 
   function checkfield ($fieldname, $value, $changed) {
	global $Id, $fields, $Record, $ArticleTypeFields ;
	switch ($fieldname) {
	    case 'CaseId' :
		// Validate
		if ($value <= 0) return false ;

		// Get Case
		$query = sprintf ('SELECT `Case`.*, Article.Description AS ArticleDescription FROM `Case` LEFT JOIN Article ON Article.Id=Case.ArticleId WHERE Case.Id=%d AND Case.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] == 0) return 'Case not found' ;

		// Validate company
		if ((int)$Record['CompanyId'] != (int)$row['CompanyId']) return sprintf ('%s(%d) invalid company id', __FILE__, __LINE__) ; 

		// Save ArticleNumber
		$fields['ArticleId']['value'] = (int)$row['ArticleId'] ;
		$fields['Description']['value'] = $row['ArticleDescription'] ;

		return true ;		
		
	    case 'ArticleNumber':
		// Validate
		if ($fields['CaseId']['value'] > 0) {
		    if ($value != '') return 'please only enter either Case or Article' ;
		    return false ;
		}
		if ($value == '') return 'please enter either Case or Article' ;
		
		// Get Article
		$query = sprintf ('SELECT Id, Description FROM Article WHERE Id=%s AND Active=1', $Id) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] == 0) return 'Article not found' ;

		// Save Article number
		$fields['ArticleId']['value'] = (int)$row['Id'] ;
		$fields['Description']['value'] = $row['Description'] ;

		// ArticleNumber are not to be used
		return false ;
	}
	return false ;
    } 

    // Read fields
    $res = saveFields ('OrderLine', -1, $fields) ;
    if ($res) return $res ;

    return navigationCommandMark ('artorderlineedit', dbInsertedId ()) ;

?>
