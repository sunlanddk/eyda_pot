<?php

    function articleNumberFill (&$value) {
	$len = strlen($value) ;
	
	// Partitial specified number
	switch ($len) {
	    case 4 :
		// Type and Category specified
		$query = sprintf ('SELECT MAX(Number) as Number, COUNT(*) AS Count FROM Article WHERE Number>="%s000000" AND Number<="%s999900" AND Active=1', addslashes($value), addslashes($value)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Count'] > 0) {
		    $n = (int)substr($row['Number'],4,4) ;
		    if ($n >= 9999) return 'no free Article numbers within specified category' ;
		    $n++ ;			    
		} else {
		    $n = 0 ;
		}
		$value = sprintf ('%s%04d00', $value, $n) ;
		return 1 ;

	    case 8 :
		// Type, Category and index specified
		$query = sprintf ('SELECT MAX(Number) as Number, COUNT(*) AS Count FROM Article WHERE Number>="%s00" AND Number<="%s99" AND Active=1', addslashes($value), addslashes($value)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Count'] > 0) {
		    $n = (int)substr($row['Number'],8,2) ;
		    if ($n >= 99) return 'no free Article numbers within specified index' ;
		    $n++ ;			    
		} else {
		    $n = 0 ;
		}
		$value = sprintf ('%s%02d', $value, $n) ;
		return 1 ;

	    case 10 :
		// Fully specified
		return 0 ;

	    default :
		// Invalid number of difits in article
		return 'invalid number of digits in article' ;
	}
    }
    
?>
