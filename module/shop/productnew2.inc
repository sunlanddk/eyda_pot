<?php

    require_once 'lib/parameter.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    define ('DEBUG', false) ;
    
    // Page header
    itemStart () ;
    itemSpace () ;
    itemField ('Case', (int)$Record['Id']) ;
    itemField ('Article', $Record['Number'] . ', ' . $Record['Description']) ;
    itemSpace () ;
    itemEnd () ;

    formStart () ;

    // Colors
    listStart () ;
    listHeader ('Colours') ;
    listRow () ;
    listHead ('', 30) ;
    listHead ('Number', 75) ;
    listHead ('Colour', 40) ;
    listHead ('', 6) ;
    listHead ('Description') ;
    foreach ($Color as $i => $row) {
	listRow () ;
	listFieldRaw (formCheckBox(sprintf('Color[%d]', $i), 0)) ;
	listField ($row['Number']) ;
	if ($row['ColorGroupId'] > 0) {
	    listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	} else {
	    listField ('') ;
	}
	listField ('') ;
	listField ($row['Description']) ;
    }
    if (count($Color) == 0) {
	listRow () ;
	listField () ;
	listField ('No Colors', 'colspan=3') ;
    }
    listEnd () ;
    printf ("<br>\n") ;

    // Certificates    
    listStart () ;
    listHeader ('Certificates') ;
    listRow () ;
    listHead ('', 30) ;
    listHead ('Name', 121) ;
    listHead ('Type', 121) ;
    listHead ('Case') ;
    foreach ($Certificate as $i => $row) {
	listRow () ;
	listFieldRaw (formCheckBox(sprintf('Certificate[%d]', $i), 0)) ;
	listField ($row['Name']) ;
	listField ($row['CertificateTypeName']) ;
	listFieldRaw (formCheckBox(sprintf('CertificateCase[%d]', $i), 0)) ;
    }
    if (count($Certificate) == 0) {
	listRow () ;
	listField () ;
	listField ('No Certificates', 'colspan=2') ;
    }
    listEnd () ;
    printf ("<br>\n") ;

    formEnd () ;

    return 0 ;
?>
