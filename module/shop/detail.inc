<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;

    function htmlFlag ($name, $condition='') {
	global $Record ;
	if ($condition == '') $condition = $name ;
	if (!$Record['ArticleType'.$condition]) return ;
	printf ("<tr><td class=itemlabel>%s</td><td><input type=checkbox name=%s%s></td></tr>\n", str_replace('Variant','',$name), $name, ($Record[$name])?' checked':'') ;
    }

    // Form
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
//    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Customs Pos</td><td>%s</td></tr>\n", htmlDBSelect ("CustomsPositionId style='width:300px'", $Record['CustomsPositionId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM CustomsPosition WHERE Active=1 ORDER BY Description')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Material</td><td>%s</td></tr>\n", htmlDBSelect ("MaterialCountryId style='width:200px'", $Record['MaterialCountryId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Country WHERE Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Knit</td><td>%s</td></tr>\n", htmlDBSelect ("KnitCountryId style='width:200px'", $Record['KnitCountryId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Country WHERE Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Work</td><td>%s</td></tr>\n", htmlDBSelect ("WorkCountryId style='width:200px'", $Record['WorkCountryId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Country WHERE Active=1 ORDER BY Name')) ;  
    print htmlItemSpace() ;
    htmlFlag ('VariantColor') ;
    htmlFlag ('VariantDimension') ;
    htmlFlag ('VariantSize') ;
    htmlFlag ('VariantSortation') ;
    htmlFlag ('VariantCertificate') ;
    htmlFlag ('PriceDimensionAdjust', 'VariantDimension') ;
    if ($Record['ArticleTypeFabric']) {
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Print Number</td><td><input type=text name=PrintNumber maxlength=20' size=20 value=\"%s\"></td></tr>\n", htmlentities($Record['PrintNumber'])) ;
    }
    if ($Record['ArticleTypeProduct']) {
	itemSpace () ;
	itemFieldRaw ('Fabric', formText ('FabricArticleNumber', tableGetField('Article', 'Number', (int)$Record['FabricArticleId']), 10)) ;
    } else {
    	itemSpace () ;
	printf ("<input type=hidden name=MAX_FILE_SIZE value=2000000>\n") ;
	printf ("<tr><td><p>Sketch</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;    
    }
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
