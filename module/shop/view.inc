<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    

    // Outher table
    printf ("<table class=item><tr>\n<td width=280>\n") ;
    
    // Header
    itemStart () ;
    itemHeader('Basic info') ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    if (!$Record['ArticleTypeProduct']) {
	itemSpace () ;
	printf ("<tr><td class=itemlabel>Unit</td><td class=itemfield>%s</td></tr>\n", tableGetField ('Unit', 'Name', (int)$Record['UnitId'])) ;
    }
    itemSpace () ;
    itemFieldIcon ('Supplier', tableGetField ('Company', 'CONCAT(Name," (",Number,")")', (int)$Record['SupplierCompanyId']), 'company.gif', 'companyview', (int)$Record['SupplierCompanyId']) ;
    if (!$Record['ArticleTypeProduct']) {
	printf ("<tr><td class=itemlabel>Reference</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['SupplierNumber'])) ;
	if ($Record['ArticleTypeFabric']) {
	    print htmlItemSpace() ;
	    printf ("<tr><td class=itemlabel>Print Number</td><td class=itemfield>%s</td></tr>\n", $Record['PrintNumber']) ;
	}
	print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Price</td><td class=itemfield>%0.2f Kr.</td></tr>\n", $Record['PriceStock']) ;
    }
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Customs Pos</td><td class=itemfield>%s</td></tr>\n", tableGetField ('CustomsPosition', 'Name', (int)$Record['CustomsPositionId'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Material</td><td class=itemfield>%s</td></tr>\n", ((int)$Record['MaterialCountryId'] > 0) ? tableGetField ('Country', 'CONCAT(Description," (",Name,")")', (int)$Record['MaterialCountryId']) : 'none') ;
    printf ("<tr><td class=itemlabel>Knit</td><td class=itemfield>%s</td></tr>\n", tableGetField ('Country', 'CONCAT(Description," (",Name,")")', (int)$Record['KnitCountryId'])) ;
    printf ("<tr><td class=itemlabel>Work</td><td class=itemfield>%s</td></tr>\n", tableGetField ('Country', 'CONCAT(Description," (",Name,")")', (int)$Record['WorkCountryId'])) ;
    if ($Record['ArticleTypeProduct']) {
	// Fabrics
	$query = sprintf ('SELECT Article.* FROM Article WHERE Article.Id=%d AND Article.Active=1', (int)$Record['FabricArticleId']) ;
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	itemSpace () ;
	itemFieldIcon ('Fabric', $row['Number'], 'article.gif', 'articleview', (int)$row['Id']) ;
	itemField ('Description', $row['Description']) ;

	// Composition
	$s = '' ;
	$query = sprintf ('SELECT ArticleComponent.Percent, Component.Name FROM ArticleComponent INNER JOIN Component ON Component.Id=ArticleComponent.ComponentId AND Component.Active=1 WHERE ArticleComponent.ArticleId=%d AND ArticleComponent.Active=1', (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	while ($row = dbFetch ($res)) {
	    if ($s != '') $s .= ', ' ;
	    $s .= sprintf ('%s %d%%', $row['Name'], (int)$row['Percent']) ;
	}
	dbQueryFree ($res) ;
	itemSpace () ;
	itemField ('Composition', $s) ;

	// Approved style
	$query = sprintf ('SELECT Style.* FROM Style WHERE Style.ArticleId=%d AND Style.Approved=1 AND Style.Active=1', (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	itemSpace () ;
	if ((int)$row['Id'] > 0) {
	    itemFieldIcon ('Sales Style', $row['Version'], 'style.gif', 'styleview', (int)$row['Id']) ;
	    $StyleId = (int)$row['Id'] ;
	} else {
	    itemField ('Sales Style', 'none') ;
	}

	// Latest style
	itemSpace () ;
	if ((int)$Record['StyleId'] > 0) {
	    itemFieldIcon ('Latest Style', $Record['StyleVersion'], 'style.gif', 'styleview', (int)$Record['StyleId']) ;
	    itemField ('State', $Record['StyleState']) ;
	    if ($StyleId <= 0) $StyleId = (int)$Record['StyleId'] ;
	} else {
	    itemField ('Latest Style', 'none') ;
	}
    }
    itemEnd () ;

    printf ("<br></td><td style=\"border-left: 1px solid #cdcabb;\">\n") ;
        
    // Sketch
    if ($Record['ArticleTypeProduct']) {
	$file = fileName ('sketch', $StyleId) ;
    } else {
	$file = fileName ('article', $Record['Id']) ;
    }
    if (is_file($file)) {
	// Sketch exists
	if ($Record['ArticleTypeProduct']) {
	    // Sketch from Style
	    printf ("<img src=index.php?nid=%d&id=%d>\n", navigationMark('sketch'), $StyleId) ;
	} else {
	    // Sketch from Article
	    printf ("<img src=index.php?nid=%d&id=%d>\n", navigationMark('articlesketch'), $Record['Id']) ;
	}	    
    } else {
	// No Sketch
	printf ('<p style="padding-top:4px;">No sketch</p>') ;
    }
    printf ("</td></tr></table>\n") ;

    // Outher table, 1st field
    printf ("<table class=item><tr>") ;
    
    if ($Record['ArticleTypeProduct']) {
	// Cases
	$query = sprintf ('SELECT Case.Id, Case.CustomerReference, Case.ArticleCertificateId, Company.Number AS CustomerNumber, Company.Name AS CustomerName, CertificateType.Name AS CertificateTypeName
	FROM `Case`
	LEFT JOIN Company ON Company.Id=Case.CompanyId AND Company.Active=1
	LEFT JOIN ArticleCertificate ON ArticleCertificate.Id=Case.ArticleCertificateId
	LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId
	LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId
	WHERE Case.ArticleId=%d AND Case.Done=0 AND Case.Active=1 ORDER BY Case.Id DESC', (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Cases') ;
	listRow () ;
	listHeadIcon () ;
	listHead ('Number', 50) ;
	listHead ('Customer') ;
	listHead ('Reference',90) ;
	listHead ('Certificate', 90) ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
	    listFieldIcon ('case.gif', 'case', $row['Id']) ;
	    listField ($row['Id']) ;
	    listField ($row['CustomerNumber'] . ', ' . $row['CustomerName']) ;
	    listField ($row['CustomerReference']) ;
	    listField (((int)$row['ArticleCertificateId'] <= 0) ? 'None' : $row['CertificateTypeName']) ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Cases', 'colspan=2') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;

	// Colors
	$query = sprintf ("SELECT ArticleColor.*, Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM ArticleColor INNER JOIN Color ON Color.Id=ArticleColor.ColorId AND Color.Active=1 LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId AND ColorGroup.Active=1 WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 ORDER BY Color.Number", (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Colours') ;
	listRow () ;
	listHeadIcon () ;
	listHead ('Number', 75) ;
	listHead ('Colour', 40) ;
	listHead ('', 6) ;
	listHead ('Description') ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
	    listFieldIcon ('color.gif', 'color', $row['Id']) ;
	    listField ($row['Number']) ;
	    if ($row['ColorGroupId'] > 0) {
		listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	    } else {
		listField ('') ;
	    }
	    listField ('') ;
	    listField ($row['Description']) ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Colors', 'colspan=3') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;

	// Sizes
	$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder', (int)$Record['Id']) ;
	$res = dbQuery ($query) ;
	printf ("<td width=100 style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	listStart () ;
	listHeader ('Sizes') ;
	listRow () ;
	listHead ('Name') ;
	while ($row = dbFetch ($res)) {
	    listRow () ;
	    listField ($row['Name']) ;
	}
	if (dbNumRows($res) == 0) {
	    listRow () ;
	    listField ('No Sizes') ;
	}
	listEnd () ;
	printf ("<br></td>") ;
	dbQueryFree ($res) ;

	// Certificats
	if ($Record['VariantCertificate']) {
	    $query = sprintf ("SELECT ArticleCertificate.Id, Certificate.Name AS CertificateName, CertificateType.Name AS CertificateTypeName FROM ArticleCertificate INNER JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 INNER JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId AND CertificateType.Active=1 WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Certificate.Name", (int)$Record['Id']) ;
	    $res = dbQuery ($query) ;
	    printf ("<td width= 180 style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    listStart () ;
	    listHeader ('Certificates') ;
	    listRow () ;
	    listHead ('Name', 90) ;
	    listHead ('Type', 90) ;
	    while ($row = dbFetch ($res)) {
		listRow () ;
		listField ($row['CertificateName']) ;
		listField ($row['CertificateTypeName']) ;
	    }
	    if (dbNumRows($res) == 0) {
		listRow () ;
		listField ('No Certificates', 'colspan=2') ;
	    }
	    listEnd () ;
	    printf ("<br></td>") ;
	    dbQueryFree ($res) ;
	}
	
    } else {
	// Generel
        printf ("<td width=50%% style=\"border-bottom: 1px solid #cdcabb;\">\n") ;
	itemStart () ;
	itemHeader ('General') ;
	itemFieldText ('Comment', $Record['Comment']) ;
	itemInfo ($Record) ;
	itemEnd () ;
	printf ("<br></td>") ;
    
	if ($Record['ArticleTypeTest']) {
	    // Outher table, 2nd field
	    printf ("<td width=25%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    itemStart () ;
	    itemHeader('Tests General', 'Name', 'Value') ;
	    itemField ('Width Usable', $Record['WidthUsable'] . ' cm') ;
	    itemField ('Width Full', $Record['WidthFull'] . ' cm') ;
	    itemSpace () ;
	    itemField ('Weight', $Record['M2Weight'] . ' g/m2') ;
	    itemField ('', (int)($Record['M2Weight']*$Record['WidthFull']/100) . ' g/mtl') ;
	    itemSpace () ;
	    itemField ('Shrink Wash', $Record['ShrinkageWashLength'] . ' % length') ;
	    itemField ('', $Record['ShrinkageWashWidth'] . ' % width') ;
	    itemField ('Shrink Work', $Record['ShrinkageWorkLength'] . ' % length') ;
	    itemField ('', $Record['ShrinkageWorkWidth'] . ' % width') ;
	    itemSpace () ;
	    itemField ('Rubbing Dry', $Record['RubbingDry']) ;
	    itemField ('Rubbing Wet', $Record['RubbingWet']) ;
	    itemSpace () ;
	    itemField ('Washinginstr.', $Record['WashingInstruction']) ;
	    itemEnd () ;
	    printf ("<br></td>") ;
	    
	    // Outher table, 2nd field
	    printf ("<td width=25%% style=\"border-left: 1px solid #cdcabb;border-bottom: 1px solid #cdcabb;\">\n") ;
	    itemStart () ;
	    itemHeader ('Tests Colourfastness', 'Type', 'Value') ;
	    foreach (array('Water','Wash','Light','Perspiration','Chlor','Sea','Durability') as $name) {
		itemField ($name, $Record['ColorFastness'.$name]) ;
	    }
	    itemEnd () ;
	    printf ("<br></td>") ;
	}
    }
    printf ("</tr></table>\n") ;

    // Footer
    if ($Record['ArticleTypeProduct']) {
	itemStart () ;
	itemHeader ('General') ;
	itemFieldText ('Comment', $Record['Comment']) ;
	itemInfo ($Record) ;
	itemEnd () ;
    }
    return 0 ;
?>
