<?php

    require_once 'lib/html.inc' ;

    function htmlFlag (&$Record, $field, $name=NULL) {
	if (is_null($name)) $name = $field ;
	$s = sprintf ("<tr><td class=itemfield>%s</td>", $name) ;
	if ($Record[$field]) {
	    $s .= sprintf ("<td class=itemfield>%s, %s</td>", date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    $s .= sprintf ("<td><input type=checkbox name=%s%s></td>", $field, ($Record[$field])?' checked':'') ;
	}
	$s .= "</tr>\n" ;
	return $s ;
    }
    
    printf ("<br><table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</td></tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Style version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    printf ("</table><br>\n") ;
  
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    break ;
    }
    
    // Form
    printf ("<form method=POST name=appform enctype='multipart/form-data'>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>SplitOperation</td><td><input type=text name=SplitOperationNo size=2 maxlength=2 value=%d></td></tr>\n", $Record['SplitOperationNo']) ;
    print htmlItemSpace() ;
    printf ("<input type=hidden name=MAX_FILE_SIZE value=2000000>\n") ;
    printf ("<tr><td><p>Sketch</p></td><td><input type=file name=Doc size=60></td></tr>\n") ;    

    switch ($Navigation['Parameters']) {
	case 'new' :
	    break ;

	default ;
	    print htmlItemSpace() ;
	    print htmlFlag ($Record, 'PatternMade', 'Pattern made') ;
	    print htmlFlag ($Record, 'Ready', 'Cons approve') ;
    }

    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
