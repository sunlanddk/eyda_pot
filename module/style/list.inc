<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;

    // Style header
    printf ("<table class=item>\n") ;
    print htmlItemSpace () ;
    itemFieldIcon ('Article', $Record['Number'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace () ;
    printf ("</table>\n") ;

    // Log list
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHeadIcon () ;
    listHead ('Version', 50) ;
    listHead ('State', 70) ;
    listHead ('Created', 110) ;
    listHead () ;
    listHead ('Constructor', 110) ;
    listHead () ;
    listHead ('Approved', 110) ;
    listHead () ;
    listHead ('', 8) ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'styleview', $row['Id']) ;
	listFieldIcon ('pdm.gif', 'pdm', $row['Id']) ;
	listField ((int)($row['Version'])) ;
	listField ($row['State']) ;
	listField (date ("Y-m-d H:i:s", dbDateDecode($row['CreateDate']))) ;
	listField ($row['CreateUserName']) ;
	if ($row['Ready']) {
	    listField (date ("Y-m-d H:i:s", dbDateDecode($row['ReadyDate']))) ;
	    listField ($row['ReadyUserName']) ;
	} else {
	    listField ('', 'colspan=2') ;
	}
	if ($row['Approved']) {
	    listField (date ("Y-m-d H:i:s", dbDateDecode($row['ApprovedDate']))) ;
	    listField ($row['ApprovedUserName']) ;
	} else {
	    listField ('', 'colspan=2') ;
	}	
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Styles', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;


    return 0 ;
?>
