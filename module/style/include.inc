<?php

    $styleStateField = 'IF(NOT Style.Ready,IF(Style.PatternMade,"Pattern","Defined"), IF(Style.Approved,"Sale","Constructor"))' ;

	function isCurrentMaterial($styleid) {
		$query = sprintf('SELECT COUNT(*) as cnt FROM StylePageVersion p 
						INNER JOIN StyleMaterialsVersion m ON p.PageId = m.Id AND p.StyleId = %d 
						WHERE p.Current=1 
						AND p.PageTypeId = 4', $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
	}


	function isCurrentMaterialClosed($styleid) {
		$query = sprintf('SELECT COUNT(*) as cnt FROM StylePageVersion p 
						INNER JOIN StyleMaterialsVersion m ON p.PageId = m.Id AND p.StyleId = %d 
						WHERE p.Current=1 
						AND p.PageTypeId = 4
						AND m.ConstructionDone = 1 
						AND m.FabricConsumptionDone = 1 
						AND m.ThreadConsumptionDone = 1', $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
	}

	function isCurrentSketchesClosed($styleid) {
		$query = sprintf('SELECT COUNT(*) as cnt FROM StylePageVersion p 
						INNER JOIN StyleSketchVersion s ON p.PageId = s.Id 	AND p.StyleId = %d
						LEFT JOIN StyleSketch ss ON ss.StyleSketchVersionId = s.Id
						WHERE p.Current=1 AND p.PageTypeId = 2
						AND (
							ss.Id IS NULL 
							OR 
							ss.ConstructionDone = 0
							OR 
							ss.TechnologyDone = 0
						)', $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return !$row['cnt'];
	}

	function isCurrentOperationsClosed($styleid) {
		$query = sprintf('SELECT COUNT(*) as cnt FROM StylePageVersion p 
						INNER JOIN StyleOperationsVersion o ON p.PageId = o.Id 	AND p.StyleId = %d
						WHERE p.Current=1 
						AND p.PageTypeId = 3
						AND o.Done = 1', $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
		
	}

	function isCurrentDesignClosed($styleid) {
		$query = sprintf('SELECT COUNT(*) as cnt FROM StylePageVersion p 
						INNER JOIN StyleDesignVersion d ON p.PageId = d.Id 	AND p.StyleId = %d
						WHERE p.Current=1 
						AND p.PageTypeId = 1
						AND d.Done = 1 ', $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
		
	}

	function isStyleAllocated($styleid) {
		$query = sprintf("SELECT COUNT(*) as cnt FROM Style s 
						WHERE s.ActualDate > '2000-01-01' 
						AND s.Id = %d", $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
	}

	function isStyleReadyForSample($styleid) {
		$query = sprintf('SELECT COUNT(*) as cnt FROM Style s 
						WHERE s.ReadyForSample = 1 
						AND s.Id = %d', $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
	}

	function isStyleCanceled($styleid) {
		$query = sprintf("SELECT COUNT(*) as cnt FROM Style s 
						WHERE s.Canceled > 0 
						AND s.Id = %d", $styleid);
		$result = dbQuery ($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		return $row['cnt'];		
	}


	function StyleState_NewStyles ($styleid) {
 	    if (isStyleCanceled($styleid)) {
			return 'Canceled' ;
	    }

  	    if (isCurrentMaterialClosed($styleid) &&
			isCurrentSketchesClosed($styleid) &&
			isCurrentOperationsClosed($styleid) &&
			isCurrentDesignClosed($styleid) ) {
			return 'ReadyP for production' ;
		}

  	    if (isStyleAllocated($styleid)) {
	  	    if (isStyleReadyForSample($styleid)) {
				return 'ReadyS for sample' ;
			}
			return 'Allocated' ;
		}
		return 'Request' ;
	}


?>
