<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/item.inc' ;

    // Outher table
    printf ("<table class=item><tr>\n") ;
    
    // Header
    printf ("<td width=280 style=\"border-bottom: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
    itemHeader('Basic info') ;
    itemFieldIcon ('Article', $Record['Number'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;
//    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Style Version</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Version']) ;
    print htmlItemSpace() ;
    itemField ('State', $Record['State']) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>PDM Version</td><td class=itemfield>%s</td></tr>\n", ($Record['PDMVersion'] > 0) ? (int)$Record['PDMVersion'] : 'None') ;
    if ($Record['PDMVersion'] > 0) {
	printf ("<tr><td class=itemlabel>Author</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['PDMCreateUser'])) ;
	printf ("<tr><td class=itemlabel>Released</td><td class=itemfield>%s</td></tr>\n", date('Y-m-d H:i:s', dbDateDecode($Record['PDMCreateDate']))) ;
	if ($Record['TaskId'] > 0) {
	    printf ("<tr><td class=itemlabel>Locked</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['TaskUserId']))) ;
	    printf ("<tr><td class=itemlabel>&nbsp;&nbsp;&nbsp;&nbsp;at</td><td class=itemfield>%s</td></tr>\n", date('Y-m-d H:i:s', dbDateDecode($Record['TaskCreateDate']))) ;
	} else {
	    printf ("<tr><td class=itemlabel>Locked</td><td class=itemfield>No</td></tr>\n") ;
	}
    }
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Operations</td><td class=itemfield>%d</td></tr>\n", $Record['OperationCount']) ;
    printf ("<tr><td class=itemlabel>Time</td><td class=itemfield>%0.2f minutes</td></tr>\n", $Record['OperationProductionMinutes']) ;
    printf ("<tr><td class=itemlabel>Split</td><td class=itemfield>%s</td></tr>\n", ($Record['SplitOperationNo'] > 0) ? (int)$Record['SplitOperationNo'] : 'no') ;
    print htmlItemSpace() ;
    itemEnd () ;
    printf ("</td>") ;
        
    // Sketch
    printf ("<td 0 style=\"border-bottom: 1px solid #cdcabb;border-left: 1px solid #cdcabb;\">\n") ;
    $file = fileName ('sketch', $Record['Id']) ;
    if (is_file($file)) {
	// Sketch exists
	printf ("<img src=index.php?nid=%d&id=%d>\n", navigationLink('sketch'), $Record['Id']) ;
    } else {
	// No Sketch
	printf ('<p style="padding-top:4px;">No sketch</p>') ;
    }
    printf ("</td>") ;
    
    // Outher table END
    printf ("</tr></table>\n") ;

    // Footer
    itemStart () ;
    itemHeader ('General') ;
    function htmlFlag (&$row, $field, $name=NULL) {
	if (is_null($name)) $name = $field ;
	printf ("<tr><td class=itemlabel>%s</td><td class=itemfield>%s</td></tr>", $name, ($row[$field]) ? date("Y-m-d H:i:s", dbDateDecode($row[$field.'Date'])) . ', ' . tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $row[$field.'UserId']) :'no') ;
    }
    htmlFlag ($Record, 'PatternMade', 'Pattern made') ;
    htmlFlag ($Record, 'Ready', 'Cons approve') ;
    htmlFlag ($Record, 'Approved', 'Sale approve') ;
    itemInfo($Record) ;
    itemEnd () ;

    return 0 ;
?>
