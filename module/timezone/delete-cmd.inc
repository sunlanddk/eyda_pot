<?php

    $query = sprintf ("SELECT * FROM TimeZone WHERE Id=%d", $Id) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if (!$row) return "Record does not exist" ;   
   
    require_once "lib/table.inc" ;
    tableDelete ("TimeZone", $Id) ;

    return 0
?>
