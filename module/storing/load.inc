<?php
    require_once  "module/sales/shop.inc" ;

    switch ($Navigation['Function']) {
		case 'view_layout' :
		case 'store' :
		case 'store-cmd' :
			if (isset($_GET['ToStock'])) $Id=$_GET['ToStock'] ;
		    // Get record
		    $query = sprintf ("SELECT * FROM Stock WHERE Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;

		case 'receiveupload' :
		case 'receiveupload-cmd' :
			$query = sprintf ("SELECT r.*, s.Name as SupplierName, Season.FromStockId
								FROM (Requisition r, Company s) 
								LEFT JOIN Season ON Season.Id=r.SeasonId
								WHERE r.Id=%d and s.id=r.CompanyId", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			break ;

		case 'receivebyline' :
		case 'receivebyline-cmd' :
			global $Record, $Lines, $max_qtys ;
			require_once 'lib/parameter.inc' ;

			$query = sprintf ("SELECT r.*, s.Name as SupplierName, Season.FromStockId, Currency.Rate as ActualPriceCurrencyRate,  currency.Symbol as CurrencySymbol
								FROM (Requisition r, Company s) 
								LEFT JOIN Currency ON Currency.Id=r.CurrencyId
								LEFT JOIN Season ON Season.Id=r.SeasonId
								WHERE r.Id=%d and s.id=r.CompanyId", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
		
			$reqcontainer_id = ParameterGet('ReqContainerId');
			$tquery = sprintf ("SELECT min(rl.No) as LineNo, group_concat(rl.No order by rl.No) as LineNos, rl.ActualPrice, rl.LogisticCost,
									group_concat(rl.Id) as RequisitionLineId, rl.ActualPriceConsumption, rl.LogisticCostConsumption, r.CurrencyId, 
									a.Id as ArticleId, a.Number as ArticleNumber, a.Description as ArticleName, a.CostCurrencyId, 
									a.VariantSize as VariantSize, ac.id as ArticleColorId, c.Description as ColorName, c.Number as ColorNumber,
									Currency.Rate as ActualPriceCurrencyRate,  currency.Symbol as CurrencySymbol, CostCurrency.Rate as LogisticCurrencyRate,  CostCurrency.Symbol as LogisticCurrencySymbol
						FROM (requisition r, requisitionline rl, article a)
						LEFT JOIN articlecolor ac ON a.variantcolor=1 and ac.id=rl.articlecolorid
						LEFT JOIN color c ON c.id=ac.colorid
						LEFT JOIN Currency ON Currency.Id=r.CurrencyId
						LEFT JOIN Currency CostCurrency ON CostCurrency.Id=a.CostCurrencyId
						WHERE r.id=%d and rl.requisitionid=r.id and rl.active=1 and a.id=rl.articleid
							  and r.done=0 and r.ready=1 and (isnull(rl.done) or rl.done=0)
                        GROUP BY a.id, ac.id 
						OrDER BY LineNo", (int)$Record['Id']) ; //die($tquery);
			$result = dbQuery ($tquery) ;
			
			$Lines = array() ;
			$max_qtys=0;
			while ($row = dbFetch ($result)) {
				$Lines[$row['LineNo']] = $row ;

				$qtyquery = sprintf ("SELECT rq.Id as RequisitionQuantityId, rq.Quantity as Quantity, rq.QuantityReceived as QuantityReceived, rq.QuantityPrepacked as QuantityPrepacked, rq.Dimension as Dimension, 
											az.id as ArticleSizeId, az.DisplayOrder as DisplayOrder, if(isnull(az.name), rq.Dimension, az.name) as Name,
											sum(i.Quantity) as ItemQuantity
									FROM (requisitionlinequantity rq)
									LEFT JOIN articlesize az ON az.id=rq.articlesizeid
									LEFT JOIN item i ON i.requisitionlineid=rq.requesitionlineid and i.articlesizeid=rq.articlesizeid and i.Active=1  and i.ContainerId=%d 
									WHERE rq.requesitionlineid in (%s) and rq.active=1 
									GROUP BY rq.Id ORDER BY az.DisplayOrder", (int)$reqcontainer_id, $row['RequisitionLineId']) ;
				$qtyresult = dbQuery ($qtyquery) ;

				$qtys=0;
				while ($qtyrow = dbFetch ($qtyresult)) {
					$Lines[$row['LineNo']]['SizeDim'][$qtyrow['DisplayOrder']] = $qtyrow ;
					$qtys++;
				}
				dbQueryFree ($qtyresult) ;
				if ($qtys>$max_qtys) $max_qtys = $qtys ; 
			}
			dbQueryFree ($result) ;

			break ;

		case 'unpackbyline' :
		case 'unpackbyline-cmd' :
			global $Record, $Lines, $max_qtys ;
			require_once 'lib/parameter.inc' ;
			$reqcontainer_id = ParameterGet('ReqContainerId');

			$query = sprintf ("SELECT o.*, c.Name as CustomerName, Season.FromStockId, Season.PickStockId, sum(oq.quantityreceived) as QuantityReceived
								FROM (`Order` o, Company c, orderline ol, orderquantity oq) 
								LEFT JOIN Season ON Season.Id=o.SeasonId
								WHERE o.Id=%d and c.id=o.CompanyId and ol.orderid=o.id and ol.active=1 and oq.orderlineid=ol.id and oq.active=1
								GROUP BY o.id", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
		
			$tquery = sprintf ("SELECT min(ol.No) as LineNo, group_concat(ol.No order by ol.No) as LineNos, 
									group_concat(ol.Id) as OrderLineId, ol.PriceSale, o.CurrencyId,
									a.Id as ArticleId, a.Number as ArticleNumber, a.Description as ArticleName,
									a.VariantSize as VariantSize, ac.id as ArticleColorId, c.Description as ColorName, c.Number as ColorNumber,
									Currency.Rate as ActualPriceCurrencyRate
						FROM ( `order` o, orderline ol, article a)
						LEFT JOIN articlecolor ac ON a.variantcolor=1 and ac.id=ol.articlecolorid
						LEFT JOIN color c ON c.id=ac.colorid
						LEFT JOIN Currency ON Currency.Id=o.CurrencyId
						WHERE o.id=%d and ol.orderid=o.id and ol.active=1 and a.id=ol.articleid
							  and o.done=0  and (isnull(ol.done) or ol.done=0)
                        GROUP BY a.id, ac.id 
						OrDER BY LineNo", (int)$Record['Id']) ;
			$result = dbQuery ($tquery) ;
			
			$Lines = array() ;
			$max_qtys=0;
			while ($row = dbFetch ($result)) {
				$Lines[$row['LineNo']] = $row ;
				// Presale?
				$_query = sprintf('select cl.Presale, cl.ShowOnly 
										from (collectionlot cl, collectionmember cm, collection c)
										where c.seasonid=%d and cm.collectionid=c.id and cm.articleid=%d and cm.articlecolorid=%d and cm.active=1 and c.active=1 and cl.id=cm.collectionlotid', 
										$Record['SeasonId'], $row['ArticleId'], $row['ArticleColorId']) ;
				$_result = dbQuery($_query) ;
				$_row = dbFetch($_result) ;
				dbQueryFree ($_result) ;
				
				// Quantity per size
				$qtyquery = sprintf ("SELECT oq.Id as OrderQuantityId, sum(oq.Quantity) as Quantity, sum(oq.QuantityReceived) as QuantityReceived,
											az.id as ArticleSizeId, az.DisplayOrder as DisplayOrder, if(isnull(az.name), oq.Dimension, az.name) as Name
									FROM (orderquantity oq)
									LEFT JOIN articlesize az ON az.id=oq.articlesizeid
									WHERE oq.orderlineid in (%s) and oq.active=1 
									GROUP BY oq.Id", $row['OrderLineId']) ;
				$qtyresult = dbQuery ($qtyquery) ;

				$qtys=0;
				while ($qtyrow = dbFetch ($qtyresult)) {
					$Lines[$row['LineNo']]['SizeDim'][$qtyrow['DisplayOrder']] = $qtyrow ;
					$qtys++;
					
					// Availabile qty
/*					$qnty = -1;
					if(!$_row['Presale']) {
						$qnty = Shop::get_article_qnty_from_pickstock((int)$Record['SeasonId'], (int)$row['ArticleId'], $row['ArticleColorId'], $qtyrow['ArticleSizeId']);
						$qnty_reqcon = Shop::get_article_qnty_from_reqcontainer($reqcontainer_id, (int)$row['ArticleId'], $row['ArticleColorId'], $qtyrow['ArticleSizeId']);
						$qnty_in_order = Shop::get_article_qnty_in_orders((int)$Record['SeasonId'], (int)$row['ArticleId'], $row['ArticleColorId'], $qtyrow['ArticleSizeId'], 787);

						$qnty = $qnty + $qnty_reqcon - $qnty_in_order;

						if($_row['ShowOnly']) $qnty = 0 ;
						if($qnty < 0) $qnty = 0;
					}
					$Lines[$row['LineNo']]['SizeDim'][$qtyrow['DisplayOrder']]['QuantityAvailable'] = $qnty ;
*/
				}
				dbQueryFree ($qtyresult) ;
				if ($qtys>$max_qtys) $max_qtys = $qtys ; 
			}
			dbQueryFree ($result) ;

			break ;
		default:
			return 'Error' ;
	}

    return 0 ;
?>
