<?php

    switch ($Navigation['Function']) {
		case 'view_layout' :
		case 'store' :
		case 'store-cmd' :
			if (isset($_GET['ToStock'])) $Id=$_GET['ToStock'] ;
		    // Get record
		    $query = sprintf ("SELECT * FROM Stock WHERE Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;

		case 'receiveupload' :
		case 'receiveupload-cmd' :
			$query = sprintf ("SELECT r.*, s.Name as SupplierName, Season.FromStockId
								FROM (Requisition r, Company s) 
								LEFT JOIN Season ON Season.Id=r.SeasonId
								WHERE r.Id=%d and s.id=r.CompanyId", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			break ;

		case 'receivebyline' :
		case 'receivebyline-cmd' :
			global $Record, $Lines, $max_qtys ;
			require_once 'lib/parameter.inc' ;

			$query = sprintf ("SELECT r.*, s.Name as SupplierName, Season.FromStockId
								FROM (Requisition r, Company s) 
								LEFT JOIN Season ON Season.Id=r.SeasonId
								WHERE r.Id=%d and s.id=r.CompanyId", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
		
			$reqcontainer_id = ParameterGet('ReqContainerId');
			$tquery = sprintf ("SELECT min(rl.No) as LineNo, group_concat(rl.No order by rl.No) as LineNos, 
									group_concat(rl.Id) as RequisitionLineId, rl.ActualPriceConsumption, rl.LogisticCostConsumption, r.CurrencyId, 
									a.Id as ArticleId, a.Number as ArticleNumber, a.Description as ArticleName, a.CostCurrencyId, 
									a.VariantSize as VariantSize, ac.id as ArticleColorId, c.Description as ColorName, c.Number as ColorNumber,
									Currency.Rate as ActualPriceCurrencyRate, CostCurrency.Rate as LogisticCurrencyRate
						FROM (requisition r, requisitionline rl, article a)
						LEFT JOIN articlecolor ac ON a.variantcolor=1 and ac.id=rl.articlecolorid
						LEFT JOIN color c ON c.id=ac.colorid
						LEFT JOIN Currency ON Currency.Id=r.CurrencyId
						LEFT JOIN Currency CostCurrency ON CostCurrency.Id=a.CostCurrencyId
						WHERE r.id=%d and rl.requisitionid=r.id and rl.active=1 and a.id=rl.articleid
							  and r.done=0 and r.ready=1 and (isnull(rl.done) or rl.done=0)
                        GROUP BY a.id, ac.id 
						OrDER BY LineNo", (int)$Record['Id']) ;
			$result = dbQuery ($tquery) ;
			
			$Lines = array() ;
			$max_qtys=0;
			while ($row = dbFetch ($result)) {
				$Lines[$row['LineNo']] = $row ;

				$qtyquery = sprintf ("SELECT rq.Id as RequisitionQuantityId, rq.Quantity as Quantity, rq.QuantityReceived as QuantityReceived, rq.Dimension as Dimension, 
											az.id as ArticleSizeId, az.DisplayOrder as DisplayOrder, if(isnull(az.name), rq.Dimension, az.name) as Name,
											sum(i.Quantity) as ItemQuantity
									FROM (requisitionlinequantity rq)
									LEFT JOIN articlesize az ON az.id=rq.articlesizeid
									LEFT JOIN item i ON i.requisitionlineid=rq.requesitionlineid and i.articlesizeid=rq.articlesizeid and i.Active=1  and i.ContainerId=%d 
									WHERE rq.requesitionlineid in (%s) and rq.active=1 
									GROUP BY rq.Id", (int)$reqcontainer_id, $row['RequisitionLineId']) ;
				$qtyresult = dbQuery ($qtyquery) ;

				$qtys=0;
				while ($qtyrow = dbFetch ($qtyresult)) {
					$Lines[$row['LineNo']]['SizeDim'][$qtyrow['DisplayOrder']] = $qtyrow ;
					$qtys++;
				}
				dbQueryFree ($qtyresult) ;
				if ($qtys>$max_qtys) $max_qtys = $qtys ; 
			}
			dbQueryFree ($result) ;

			break ;

		default:
			return 'Error' ;
	}

    return 0 ;
?>
