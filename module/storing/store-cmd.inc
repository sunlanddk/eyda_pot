<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout
 
    // Get Stock to move from
    if ($_POST['FromStockId'] == 0) return 'please select Stock to move from' ;
    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $_POST['FromStockId']) ;
    $result = dbQuery ($query) ;
    $FromStock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$FromStock['Id'] <= 0) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $_POST['FromStockId']) ;
    if ($FromStock['Type'] != 'fixed') return sprintf ('%s(%d) invalid Stock, type "%s"', __FILE__, __LINE__, $_POST['FromStockId']) ;

	// Get StockName to pick from
	$PickStockName = tableGetField ('Stock', 'Name', $Id) ;

	// Check EAN code
	if (strlen($_POST['EAN']) != 13 ) {
//		return sprintf('SKU code length is 13 - only %d digits entered', strlen($_POST['EAN'])) ;
		return 'No SKU code entered before position was scanned' ;
	} 
	// Process scanned position
	$ScannedPos =str_ireplace("+","", $_POST['Position']) ;
	$ScannedPos =str_ireplace("-","", $ScannedPos) ;
	switch (strlen($ScannedPos)) {
		case 3:
			$ScannedPos = sprintf ('%s%03d%s', substr($ScannedPos, 0, 1),  (int)substr($ScannedPos, 1, 1), substr($ScannedPos, 2, 1)) ;
			break ;
		case 4:
			$ScannedPos = sprintf ('%s%03d%s', substr($ScannedPos, 0, 1),  (int)substr($ScannedPos, 1, 2), substr($ScannedPos, 3, 1)) ;
			break ;
		case 5:
			break;
		default:
			return 'Wrong length of scanned position ' . $ScannedPos ;
	}
	
	// Get variant information.
    $query = sprintf ('SELECT 
					 VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, 
					 VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, VariantCode.ArticleSizeId as ArticleSizeId, 
					 VariantCode.VariantCode AS VariantCode,
					 Container.`Position` AS `Position`, Container.Id as ContainerId, Stock.Name as StockName, Stock.Id as StockId,
					 if (VariantCode.VariantDescription="", Article.Description, VariantCode.VariantDescription) as VariantDescription,
					 if (VariantCode.VariantColorDesc="", Color.Description,VariantCode.VariantColorDesc) as VariantColor,
					 if (VariantCode.VariantColorCode="", Color.Number,VariantCode.VariantColorCode) as VariantNumber,
					 if (VariantCode.VariantSize="", ArticleSize.Name,VariantCode.VariantSize) as VariantSize, 
					 Article.Number as ArticleNumber, Article.VariantSize as SizeRange
					FROM VariantCode
						   LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
						   LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
						   LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
						   LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
						   LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
           				 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1                      
					WHERE VariantCode.variantcode="%s" AND VariantCode.Active=1', $_POST['EAN']) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	
	if ($_POST['SizeRange'] != 'on') $row['SizeRange'] = 0 ;			// Only assign this one size
	
	// Check if ok to assign and store
    if (!$row['Id']) 											// Check if EAN code exits
	    return sprintf('Last scanned SKU %s not found', $_POST['EAN']) ;
    if ($row['StockId'] >0 and $row['StockId']!= $Id)			// Check if it is the correct stock if already assigned
		return sprintf ('SKU is ASSIGNED TO different stock (%s)', $row['StockName']) ;
	if ($row['ContainerId'] > 0) {								// Check if position is defined and is correct if already assigned 
		if ($row['Position']) {		
			if (!stristr($ScannedPos, $row['Position']))
				return sprintf ('SKU %s is already assigned to %s! Cant be stored at %s', $_POST['EAN'], $row['Position'], $ScannedPos) ;
		} else {
			return sprintf('SKU %s assigned to Container with no Position', $_POST['EAN'])  ;
		}
		$AssignedPickContainerId = $row['ContainerId'] ;

	// Everything ok but no position prev assigned, so assign container and position to all variants of whole sizeset.
    } else {
		// Check if posistion is assigned to another EAN
		$query = sprintf ('SELECT Container.Id, VariantCode.VariantCode FROM Container 
							LEFT JOIN VariantCode ON VariantCode.PickContainerId=Container.Id AND VariantCode.Active=1
							WHERE Container.StockId=%d and Container.Active=1 and Container.`Position`="%s"', 
							$Id, $ScannedPos) ; // HUSK case insensitive
		$result = dbQuery ($query) ;
		if (dbNumRows($result) > 1)
			return sprintf('More than one container with the position (%s) - Please clean before storing', $ScannedPos) ;
		$PickContainer = dbFetch ($result) ;
		dbQueryFree ($result) ; 
		if ($PickContainer['Id']>0 and $PickContainer['VariantCode'] and $PickContainer['VariantCode']!=$_POST['EAN'])
			return sprintf('Position cant be used already assigned to different SKU %s',$PickContainer['VariantCode']) ;

		// Assign PickContainers
		if ($row['SizeRange']) {
		    if ($_POST['OnStockOnly'] == 'on')
			  $query = sprintf ("SELECT VariantCode.Id as VariantCodeId, ArticleSize.Id as ArticleSizeId,  PickContainer.Id as PickContainerId
								FROM (ArticleSize, VariantCode, Item, Container, Stock )
						            LEFT JOIN Container PickContainer ON VariantCode.PickContainerId=PickContainer.Id and PickContainer.Active=1
								WHERE
								VariantCode.ArticleColorId=%d AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.Active=1
								AND ArticleSize.ArticleId=%d AND ArticleSize.Active=1
								AND Item.ArticleId=%d AND Item.ArticleColorId=%d AND Item.ArticleSizeId=ArticleSize.Id AND Item.Active=1
								AND Item.ContainerId=Container.Id AND Container.Active=1
								AND Container.StockId=Stock.id and Stock.`Type`='fixed'
								Group by ArticleSize.Id
								ORDER BY ArticleSize.DisplayOrder", $row['ArticleColorId'], $row['ArticleId'], $row['ArticleId'], $row['ArticleColorId']) ;
		    else
			  $query = sprintf ("SELECT VariantCode.Id as VariantCodeId, ArticleSize.Id as ArticleSizeId,  PickContainer.Id as PickContainerId
								FROM (ArticleSize, VariantCode)
					            LEFT JOIN Container PickContainer ON VariantCode.PickContainerId=PickContainer.Id and PickContainer.Active=1
								WHERE
								VariantCode.ArticleColorId=%d AND VariantCode.ArticleSizeId=ArticleSize.Id AND VariantCode.Active=1
								AND ArticleSize.ArticleId=%d AND ArticleSize.Active=1
								Group by ArticleSize.Id
								ORDER BY ArticleSize.DisplayOrder", $row['ArticleColorId'], $row['ArticleId']) ;
			$res = dbQuery ($query) ;
			
			$AssignPos = $ScannedPos ;
			$AssignCurrentPos= '';
			while ($ArtSize = dbFetch($res)) {
				if ($ArtSize['PickContainerId']>0) continue;			// Variant already assigned
				
				// Find or Create Container for this position
				$query = sprintf ('SELECT Container.Id, VariantCode.ArticleSizeId, VariantCode.VariantCode FROM Container 
									LEFT JOIN VariantCode ON VariantCode.PickContainerId=Container.Id AND VariantCode.Active=1
									WHERE Container.StockId=%d and Container.Active=1 and Container.`Position`="%s"', 
									$Id, $AssignPos) ; // HUSK case insensitive
				$result = dbQuery ($query) ;
				if (dbNumRows($result) > 1)
					return sprintf('More than one container at the position (%s) or more Variants in same container - Please clean before storing', $AssignPos) ;
				$PickContainer = dbFetch ($result) ;
				dbQueryFree ($result) ; 
				if ($PickContainer['Id']>0) {							// Use existing Container on Position
					if ($PickContainer['ArticleSizeId'] and $PickContainer['ArticleSizeId']!=$ArtSize['ArticleSizeId'])
						return sprintf('Position (%s) cant be used, since already assigned to different SKU %s',$AssignPos,$PickContainer['VariantCode']) ;
					$PickContainerId = $PickContainer['Id'] ;
				} else {												// Make new Container on position if none present
					$NewContainer = array (
						'StockId' => (int)$Id,
						'ContainerTypeId' => 39,
						'Position' => $AssignPos ,
						'TaraWeight' => 0,
						'GrossWeight' => 0,
						'Volume' => 0
					) ;
					$PickContainerId = tableWrite ('Container', $NewContainer) ;
				}	
				if ($row['ArticleSizeId'] == $ArtSize['ArticleSizeId']) { // Assigning for scanned SKU?
					$AssignCurrentPos=$AssignPos;
					$AssignedPickContainerId = $PickContainerId ;
				}
			    // Assign Pick Container.
			    $VariantCode = array (
					'PickContainerId' => $PickContainerId
			    ) ;
				tablewrite ('VariantCode', $VariantCode, $ArtSize['VariantCodeId']) ;
				
				// Next Position. CALC
				$Street = substr($AssignPos, 0, 1) ;
				$StreetNo = (int)substr($AssignPos, 1, 3) ;
				$Floor = substr($AssignPos, 4, 1) ;
				$NoFloors = ($StreetNo/2 > (int)($StreetNo/2)) ? 'OddNoFloors' : 'EvenNoFloors' ;
				$FirstFloor =(ord($Floor) < 'a') ? 'A' : 'a' ;
				
				if (ord(strtolower($Floor)) + 1 - ord('a') > $PickLayout[$PickStockName][$Street][$NoFloors] - 1) {
					$Floor = $FirstFloor ;
					if ($StreetNo + 1 > $PickLayout[$PickStockName][$Street]['NoStreetNo']) {
					    $Street = chr(ord($Street)+1);
						$StreetNo = 1 ;
					} else
						$StreetNo++;
				} else
					$Floor = chr(ord($Floor)+1); // Next Floor
				
				$AssignPos = sprintf ('%s%03d%s', $Street,  $StreetNo, $Floor) ;
			}
			dbQueryFree ($res) ;
			if ($AssignCurrentPos == '')
				return 'SKU ' . $_POST['EAN'] . ' not assigned since no items in this size exists (not produced or purchased).' ;
			if ($AssignCurrentPos != $ScannedPos)  
				return 'The SKU wasnt the smallest unassigned size - Please try again at position ' . $AssignCurrentPos;
		} else {
			if ($PickContainer['Id']>0) {							// PickContainer Exist unassigned on this Prosition
				$PickContainerId = $PickContainer['Id'] ;
				$AssignedPickContainerId = $PickContainer['Id'] ;
			} else {												// Make new Container on position if none present
				$NewContainer = array (
					'StockId' => (int)$Id,
					'ContainerTypeId' => 39,
					'Position' => $ScannedPos ,
					'TaraWeight' => 0,
					'GrossWeight' => 0,
					'Volume' => 0
				) ;
				$AssignedPickContainerId = tableWrite ('Container', $NewContainer) ;
			}	
		    // Assign Pick Container.
		    $VariantCode = array (
				'PickContainerId' => $AssignedPickContainerId
		    ) ;
			tablewrite ('VariantCode', $VariantCode, $row['Id']) ;
		}
    }

    // Move relevant items to variants pickcontainer
	// Create ItemOperation referance
    $ItemOperation = array (
		'Description' => ''
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

	// Lookup Items
	$query = sprintf ('SELECT Item.* FROM (Item, Container)
						WHERE Item.ContainerId=Container.Id AND Container.StockId=%d AND
						Item.ArticleId=%d AND Item.ArticleColorId=%d AND Item.ArticleSizeId=%d And Item.Active=1 And Item.Sortation<2',
						$FromStock['Id'], (int)$row['ArticleId'],(int)$row['ArticleColorId'],(int)$row['ArticleSizeId']) ;
	$res = dbQuery ($query) ;

	// Update the Items to the new location
    if ($_POST['Quantity'] > 0) {
	$QtyToMove = (int)$_POST['Quantity'] ;
	$MovedQty = 0 ;
    while ($OrgItem = dbFetch($res)) {
		if ($MovedQty >= $QtyToMove) { // When storing and making Inventory at the same time then consume remaing rest of qty after storing
			if ($_POST['Inventory'] != 'on') return 'UPS' ;

			$ConsumeItem['ContainerId'] = 0 ;
			$ConsumeItem['BatchNumber'] = 'StoreInv' ;
			tableWrite ('Item', $ConsumeItem, $OrgItem['Id']) ;
			continue ;
		}

		$Item = array (
			'ItemOperationId' => $ItemOperation['Id'],
			'PreviousContainerId' => (int)$OrgItem['ContainerId'],
			'OrderLineid' => 0,
			'ContainerId' => (int)$AssignedPickContainerId
		) ;
		
		// If more qty than to be stored then split item
		if (($MovedQty+(int)$OrgItem['Quantity']) > $QtyToMove) {
			// QTY to be moved and stored
			$Item['Quantity'] = $QtyToMove - $MovedQty ;
			$MovingQty = $Item['Quantity'] ;
			
			// new item to ramin or being consumed for loss.
			$OrgItem['Quantity'] = $OrgItem['Quantity'] - ($QtyToMove - $MovedQty) ;
			$OrgItem['ParentId'] = $OrgItem['Id'] ;
			if ($_POST['Inventory'] == 'on') {
				$OrgItem['ContainerId'] = 0 ;
				$OrgItem['BatchNumber'] = 'InvLoss' ;
			}
			tableWrite ('Item', $OrgItem) ;
		} else
			$MovingQty = $OrgItem['Quantity'] ;
		
		tableWrite ('Item', $Item, $OrgItem['Id']) ;
		
		if ($Id <> $FromStock['Id']) // Dont delete if assigned PickContainer - moving from/to same stock!!!
			containerDeleteEmpty ((int)$Item['PreviousContainerId']) ; 
		
		$MovedQty += (int)$MovingQty ;

		if ($MovedQty == $QtyToMove) {
			if ($_POST['Inventory'] != 'on') break ;
		}
	}
	if ($MovedQty < $QtyToMove) { // Storing surplus: New item with surplus qty
		$OrgItem['ParentId'] = $OrgItem['Id'] ;
		unset ($OrgItem['Id']) ;
		$OrgItem['BatchNumber'] = 'InvSurplus' ;
		$OrgItem['ContainerId'] = (int)$AssignedPickContainerId ;
		$OrgItem['ItemOperationId'] = $ItemOperation['Id'] ;
		$OrgItem['Quantity'] = ($QtyToMove - $MovedQty) ;
		tableWrite ('Item', $OrgItem) ;
/*
	    $SurplusItem = array (
			'ArticleId' => (int)$row['ArticleId'],
			'ArticleColorId' => (int)$row['ArticleColorId'],
			'ArticleSizeId' => (int)$row['ArticleSizeId'],
			'ContainerId' => (int)$AssignedPickContainerId,
			'BatchNumber' => 'InvSurplus' ,
			'Sortation' => 1,
			'ItemOperationId' => $ItemOperation['Id'] ,
			'Quantity' => ($QtyToMove - $MovedQty)
		) ;
		tableWrite ('Item', $SurplusItem) ;
*/
		}
	dbQueryFree ($res) ;
    }
    $ItemOperation['Description']= substr(sprintf('Storing %s: %s(%s),%s,%s', $row['VariantCode'], $row['VariantDescription'], $row['ArticleNumber'], $row['VariantColor'], $row['VariantSize']),0,99) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation, $ItemOperation['Id']) ;

    // View itemOperations
    $Params = sprintf('&ToStock=%d&FromStock=%d',$Id,$FromStock['Id']) ;
    if  (isset($_GET['CollMemberId'])) $Params .= sprintf('&CollMemberId=%d',(int)$_GET['CollMemberId']) ;
    return navigationCommandMark ('itemlistopstoring', $ItemOperation['Id'], $Params) ;
//    return navigationCommandMark ('itemlistopstoring', $ItemOperation['Id']) ;

    return 0 ; //navigationCommandMark ('store', StockId) ;

?>
