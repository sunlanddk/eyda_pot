<script type='text/javascript'>
  function Update (nav, id) {
  if (window.event.keyCode != 13)  return ;

  appFocus ('Code') ;
  appLoaded () ;

  if (document.appform.FromStockId.value > 0) {
  if (document.appform.Code.value.length == 13) {             // EAN code
  document.appform.EAN.value = document.appform.Code.value;
  appLoad (nav,id,'StockId='+document.appform.FromStockId.value+'&Ean='+document.appform.EAN.value);
  } else if (document.appform.Code.value.length > 2) {        // Positions kode
  document.appform.Position.value = document.appform.Code.value;
  appSubmit(2047,id,null);
  } else {
  alert ('Not a valid code') ;
  }
  } else {
  alert ('select Stock to store from before scanning') ;
  }
  }
</script>

<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/navigation.inc' ;

   if (isset($_GET['CollMemberReturnId']))
				return navigationCommandMark ('collmemberview', $_GET['CollMemberReturnId'],'',1) ;

    if (isset($_GET['Ean'])) {
		  // Lookup variant
		  $query = sprintf ('SELECT 
                         VariantCode.Id AS Id, Container.`Position` AS `Position`, Container.Id AS ContainerId, 
                         VariantCode.ArticleId as ArticleId, VariantCode.ArticleColorId as ArticleColorId, VariantCode.ArticleSizeId as ArticleSizeId,
                         VariantCode.VariantCode AS VariantCode,
                         Container.`Position` AS `Position`, Stock.Name as StockName,  Stock.Id as StockId,
                         if (VariantCode.VariantDescription="", Article.Description, VariantCode.VariantDescription) as VariantDescription,
                         if (VariantCode.VariantColorDesc="", Color.Description,VariantCode.VariantColorDesc) as VariantColor,
                         if (VariantCode.VariantColorCode="", Color.Number,VariantCode.VariantColorCode) as VariantNumber,
                         if (VariantCode.VariantSize="", ArticleSize.Name,VariantCode.VariantSize) as VariantSize, 
                         Article.Number as ArticleNumber
                        FROM VariantCode
			                   LEFT JOIN Article ON VariantCode.ArticleId=Article.Id and Article.Active=1 
			                   LEFT JOIN ArticleSize ON VariantCode.ArticleSizeId=ArticleSize.Id and ArticleSize.Active=1 
			                   LEFT JOIN ArticleColor ON VariantCode.ArticleColorId=ArticleColor.Id and ArticleColor.Active=1 
			                   LEFT JOIN Color ON ArticleColor.ColorId=Color.Id and Color.Active=1 
			                   LEFT JOIN Container ON Container.Id=VariantCode.PickContainerId and Container.Active=1 
                   			 LEFT JOIN Stock ON Container.StockId=Stock.Id and Stock.Active=1                      
                        WHERE VariantCode.variantcode="%s" AND VariantCode.Active=1', $_GET['Ean']) ;
		  $result = dbQuery ($query) ;
		  $row = dbFetch ($result) ;
		  dbQueryFree ($result) ;
    }
    
    $ValidSKU = 0 ;
    // Form
    formStart () ;
    itemStart () ;
    
    itemHeader () ;
    itemField ('To Stock', $Record['Name']) ;

  // Get parameters
  if (isset($_GET['StockId']) and (int)$_GET['StockId']>0) {
		// Case specified	
		$value = (int)$_GET['StockId'] ;
	
		// Lookup production
		$query = sprintf ('SELECT * FROM `Stock` WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$FromStock = dbFetch ($result) ;
		dbQueryFree ($result) ;
		
		if ($FromStock['Id']>0)
		  	itemField ('From Stock', $FromStock['Name']) ;
		else
		  	itemField ('From Stock', 'Didnt find id ' . $FromStock['Id'] . ' ' . $value) ;
    		itemFieldRaw ('', formHidden('FromStockId', $FromStock['Id'], 8,'','')) ;
	} else {
		  $query = sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1 ORDER BY Value') ;
	    itemFieldRaw ('From Stock', formDBSelect ('FromStockId', 0, $query, 'width:250px;', NULL, 
	                    sprintf('onchange="appLoad(%d,%d,\'StockId=\'+document.appform.FromStockId.options[document.appform.FromStockId.selectedIndex].value);"', (int)$Navigation['Id'], $Id)
	    )) ;
    }
	  itemSpace () ;

    // Lookup ItemQty
      $query = sprintf ('SELECT sum(Item.Quantity) as Qty FROM (Item, Container)
						WHERE Item.ContainerId=Container.Id AND Container.StockId=%d AND
						Item.ArticleId=%d AND Item.ArticleColorId=%d AND Item.ArticleSizeId=%d And Item.Active=1 And Item.Sortation<2',
						$FromStock['Id'], (int)$row['ArticleId'],(int)$row['ArticleColorId'],(int)$row['ArticleSizeId']) ;
	    $res = dbQuery ($query) ;
      $ItemQty = dbFetch($res) ;
	    dbQueryFree ($res) ;
 		itemFieldRaw ('Assign ranges', formCheckbox ('SizeRange', $Record['RangeAssign'], '', '') . htmlentities('  Assign "size range" from smallest size - or assign one size at a time anywhere')) ;
 		itemFieldRaw ('Only on stock', formCheckbox ('OnStockOnly', $Record['OnStockOnly'], '', 'DISABLED') . htmlentities('  Assign only sizes with quantity on stock or "in request" when assigning ranges')) ;
 		itemFieldRaw ('Do inventory', formCheckbox ('Inventory', $Record['InventoryMode'], '', 'DISABLED') . '   Missing quantity is consumed as loss in the "do inventory mode" - otherwise qty is left on "from stock"') ;
	  itemSpace () ;
	  itemSpace () ;
    itemFieldRaw ('Quantity', formText ('Quantity', number_format($ItemQty['Qty'],0), 5, 'text-align:right;')) ;
	  itemSpace () ;
    itemFieldRaw ('SKU or Position', formText ('Code', '', 13, '', sprintf('onkeypress="Update(%d,%d);"',(int)$Navigation['Id'], $Id))) ;
	  itemSpace () ;

    itemHeader () ;
    itemSpace () ;

    // Handle and show EAN code     ex 5704302160671
    if (isset($_GET['Ean'])) {
		// Lookup variant
		if ($row['Id'] > 0) {
			if ($ItemQty['Qty'] > 0) {
				if ($row['StockId'] >0 and $row['StockId']!= $Id)
				  $Pos = sprintf (' is ASSIGNED TO %s different stock (%s)', $row['Position'], $row['StockName']) ;          
				if ($row['Position']) {
				  $Pos = sprintf (' to be stored at %s', $row['Position']) ;
				} else {
				  $Pos = 'is going to be assigned to position when scanned' ;
				}
				itemField ('Variant', sprintf('%s: %s(%s),%s,%s', $row['VariantCode'], $row['VariantDescription'], $row['ArticleNumber'], $row['VariantColor'], $row['VariantSize'])) ;
				itemFieldRaw ('', formHidden('EAN', $_GET['Ean'], 13,'','')) ; // Save EAN in form.
				$ValidSKU = 1 ;
			} else {
				itemField ('Variant', 'No Quantity found for SKU scanned') ;
				itemFieldRaw ('', formHidden('EAN', '', 13,'','')) ; // Save EAN in form.
			}
		} else {
			itemField ('Variant', sprintf('Var: %s not found', $row['VariantCode'])) ;      
			itemFieldRaw ('', formHidden('EAN', '', 13,'','')) ; // Save EAN in form.
		}
	} else {
	    itemField ('Variant', 'No SKU scanned') ;
   		itemFieldRaw ('', formHidden('EAN', '', 13,'','')) ; // Save EAN in form.
    }
    
    // Save Position in form.
	  itemSpace () ;
// 		itemFieldRaw ('', formHidden('Position', '', 13,'','')) ; 
    itemFieldRaw ('To position', formText ('Position', $Pos, 60)) ;
	  itemSpace () ;
	  itemSpace () ;
    itemEnd () ;
    formEnd () ;

    if ($ValidSKU) {
      // Lookup Items
      $query = sprintf ('SELECT Container.Id, sum(Item.Quantity) as Qty FROM (Item, Container)
						WHERE Item.ContainerId=Container.Id AND Container.StockId=%d AND
						Item.ArticleId=%d AND Item.ArticleColorId=%d AND Item.ArticleSizeId=%d And Item.Active=1 And Item.Sortation<2
            GROUP BY Container.Id ORDER BY Container.Id',
						$FromStock['Id'], (int)$row['ArticleId'],(int)$row['ArticleColorId'],(int)$row['ArticleSizeId']) ;
	    $res = dbQuery ($query) ;
      listStart () ;
      listRow () ;
      listHeadIcon () ;
      listHead ('Container', 95)  ;
      listHead ('Quantity', 45) ;
      listHead ('Position', 95) ;
      while ($ItemList = dbFetch($res)) {
    	  listRow () ;
    	  listFieldIcon ('container.gif', 'view', (int)$ItemList['Id']) ;
    	  listField ($ItemList["Id"]) ;
    	  listField (number_format($ItemList["Qty"],0)) ;
    	  listField ($ItemList["Position"]) ;
  	  }
      listEnd () ;
	    dbQueryFree ($res) ;
    }
    return 0 ;
?>
