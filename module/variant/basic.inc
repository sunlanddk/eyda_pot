<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
		case 'new' :
			unset ($Record) ;

			// Default values
			break ;
	}
     
    // Form
	formStart() ;
    itemStart () ;
    itemSpace () ;
    itemFieldRaw ('VariantCode', formText ('VariantCode', $Record['VariantCode'], 20)) ;
    itemFieldRaw ('Description', formText ('VariantDescription', $Record['VariantDescription'], 80)) ;
//    itemFieldRaw ('Weight', formText ('Weight', $Record['Weight'], 20) . ' gram') ;
//    itemFieldRaw ('SSCC Qty', formText ('Quantity', $Record['Quantity'], 20) . ' (default GTIN quntity on a pallet with no sscc label)') ;
    itemSpace () ;
	$_query = 'SELECT Id, CONCAT(Description," (",Number,")") AS Value FROM Article WHERE Active=1  Union select 0 as Id, " --- New ---" as Value ORDER BY Value' ;
//	itemFieldRaw ('Article', formDBSelect ('ArticleId', (int)$Record['ArticleId'], $_query, 'width:200px;')) ;  
//    itemSpace () ;
    itemFieldRaw ('GtinPickPos', formText ('GtinPickPos', $Record['GtinPickPos'], 20,'','') . ' blank out field to remove Gtin pick position assignment' ) ;
    itemFieldRaw ('QtyOnPickPos', formText ('QtyOnPickPos', (int)$Record['ItemQuantity'], 20,'',''))  ;
    print (htmlItemInfo ($Record)) ;
    itemEnd () ;

	formEnd() ;

    return 0 ;
?>
