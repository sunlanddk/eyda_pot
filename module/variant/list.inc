<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

	$_showQtyOnStock = 0;
	
	if ($Navigation['Parameters']=='outbound') {
		itemStart () ;
		itemSpace () ;
		$_query = 'select * from pickorder where current=1 and active=1 And OwnerCompanyId='.$User['CustomerCompanyId'].' AND createuserid='.$User['Id'] ;
		$_res = dbQuery($_query) ;
		$_row = dbFetch($_res) ;
		if ($_row['Id']>0) {
			$_neworderSet = 1;
			itemField ('Add to Basket:','(select products to add in below list)') ;
			itemField ('Order Number',$_row['Id']) ;
			itemField ('Reference',$_row['Reference']) ;
			itemSpace () ;
			itemEnd () ;
			dbQueryFree($_res) ;

			listStart () ;
			listRow () ;
			listHead ('Variant', 95)  ;
			listHead ('Description',250) ;
			listHead ('SSCC Quantity',85) ;
			listHead ('GTIN Quantity',83) ;
			listHead ('') ;

			$_query = "select PickOrderLine.*, variantcode.VariantDescription, PickOrder.Type 
						from (PickOrder, PickOrderLine, variantcode) 
						where variantcode.id=pickorderline.variantcodeid and PickOrderLine.Done=0 and PickOrderLine.Active=1 and PickOrderLine.PickOrderId=PickOrder.Id and PickOrder.ConsolidatedPickOrderId=". (int)$_row['ConsolidatedPickOrderId'] ;
//			printf($_query) ;
			$_res = dbQuery($_query) ;
			while ($row = dbFetch($_res)) {
				$_ordered[$row["VariantCode"]][$row["Type"]] = $row ;
			}
			foreach ($_ordered as $_line) {
				listRow () ;
				listField (((int)$_line["SSCC"]["OrderedQuantity"])>0?$_line["SSCC"]["VariantCode"]:$_line["GTIN"]["VariantCode"]) ;
				listField (((int)$_line["SSCC"]["OrderedQuantity"])>0?$_line["SSCC"]["VariantDescription"]:$_line["GTIN"]["VariantDescription"]) ;
				listField ((int)$_line["SSCC"]["OrderedQuantity"], 'align="right"') ;
				listField ((int)$_line["GTIN"]["OrderedQuantity"], 'align="right"') ;
				listField ('') ;
			}
			dbQueryFree($_res) ;
			listEnd();
			
		} else {
			$_neworderSet = 0;
			itemField ('Create Order:','Please press "New Basket" in the toolbar to create a new outbound basket for pick order, before adding products/variants') ;
			itemSpace () ;
			itemEnd () ;
			dbQueryFree($_res) ;
		}
	}

   // Filter Bar
    listFilterBar () ;

    // List
    listStart () ;
    listRow () ;
	if ($Preadvice) {
		listHead ('VariantCode', 95)  ;
	    listHead ('VariantDescription', 250) ;
	    listHead ('SSCC in basket', 88) ;
		listHeadIcon () ;
	    listHead ('GTIN in basket', 85) ;
		listHeadIcon () ;
	} else {
		listHeadIcon () ;
		listHead ('VariantCode', 95)  ;
		listHead ('VariantDescription') ;
//		listHeadIcon () ;
	}
    listHead ('Color', 150) ;
    listHead ('Size', 150) ;
    listHead ('ArticleNumber', 150) ;
//	listHead ('ArticleDescription') ;
	if ($_showQtyOnStock) {
	    listHead ('SSCCs on Stock', 100) ;
	    listHead ('SSCCs Inbound', 100) ;
	    listHead ('SSCCs In picking', 100) ;
	    listHead ('SSCCs Available') ;
	}
    listHead ('PickPosition', 95) ;
    listHead ('Stock', 95) ;
    listHead ('GTIN Pick Qty', 95) ;
//	listHeadIcon () ;//    listHead ('Container', 95) ;

    while ($row = dbFetch($Result)) {
    	listRow () ;
		if ($Preadvice) {
			listField ($row["VariantCode"]) ;
			listField ($row["VariantDescription"]) ;
			listField ((int)$_ordered[$row["VariantCode"]]["SSCC"]["OrderedQuantity"], 'align="right"') ;
			if ($Navigation['Parameters']=='inbound') {
				listFieldIcon ('add.gif', 'addinbound', $row['Id']) ;
			} else if ($Navigation['Parameters']=='outbound') {
				if ($_neworderSet) {
					if (((int)$_ordered[$row["VariantCode"]]["SSCC"]["OrderedQuantity"])>0) {
						listFieldIcon ('pen.gif', 'addoutbound', $row['Id']) ;
					} else {
						listFieldIcon ('add.gif', 'addoutbound', $row['Id']) ;
					}
				} else {
					listfield('') ;
				}
			} 
			listField ((int)$_ordered[$row["VariantCode"]]["GTIN"]["OrderedQuantity"], 'align="right"') ;
			if ($Navigation['Parameters']=='inbound') {
				listFieldIcon ('add.gif', 'addinbound', $row['Id']) ;
			} else if ($Navigation['Parameters']=='outbound') {
				if ($_neworderSet) {
					if (((int)$_ordered[$row["VariantCode"]]["GTIN"]["OrderedQuantity"])>0) {
						listFieldIcon ('pen.gif', 'addoutbGtin', $row['Id']) ;
					} else {
						listFieldIcon ('add.gif', 'addoutbGtin', $row['Id']) ;
					}
				} else {
					listfield('') ;
				}
			} 
		} else {
			listFieldIcon ('edit.gif', 'variantedit', $row['Id']) ;
			listField ($row["VariantCode"]) ;
			listField ($row["VariantDescription"]) ;
//			listFieldIcon ('edit.gif', 'articleedit', $row['ArticleId']) ;
		}
    	listField ($row["ColorDescription"]) ;
    	listField ($row["ArticleSize"]) ;
    	listField ($row["ArticleNumber"]) ;
//    	listField ($row["ArticleDescription"]) ;
		if ($_showQtyOnStock) {
			$_query = sprintf('select count(*) as QtyOnSTock from (item i, container c, stock s)
						inner join stocklayout sl ON sl.stockid=c.stockid and sl.Position=c.Position and sl.type="sscc"
						where i.active=1 and i.articleid=%d and i.articlecolorid=%d and i.articlesizeid=%d
						and c.id=i.containerid and s.id=c.stockid and s.Type="fixed" and c.active=1', $row['ArticleId'], $row['ArticleColorId'], $row['ArticleSizeId']) ;
			$_res = dbQuery($_query) ;
			$_row = dbFetch ($_res) ;
			listField ((int)$_row["QtyOnSTock"]) ;
			$_available = (int)$_row["QtyOnSTock"] ;
			dbQueryFree($_res) ;

			$_query = 'select count(*) as QtyOnArrival from (item i, container c, stock s) where i.active=1 and i.variantcodeid='. $row['Id'].' and c.id=i.containerid and s.id=c.stockid and s.Type="inbound" and c.active=1' ;
			$_res = dbQuery($_query) ;
			$_row = dbFetch ($_res) ;
			listField ((int)$_row["QtyOnArrival"]) ;
			$_available += (int)$_row["QtyOnArrival"] ;
			dbQueryFree($_res) ;

			$_query = 'select sum(OrderedQuantity-if(PackedQuantity>0,PackedQuantity,0)) as OtherOrderQuantity from (pickorder p, pickorderline pl) 
						where pl.pickorderid=p.id and p.active=1 and p.packed=0 and p.current=0 and p.type="SSCC" and pl.active=1 and pl.done=0 and pl.variantcodeid='. $row['Id'] ;
//die($_query) ;
			$_res = dbQuery($_query) ;
			$_row = dbFetch ($_res) ;
			listField ((int)$_row["OtherOrderQuantity"]) ;
			dbQueryFree($_res) ;
			$_available -= (int)$_row["OtherOrderQuantity"] ;

			listField ((int)$_available) ;
		}
		
    	listField ($row["Position"]) ;
    	listField ($row["StockName"]) ;
		if ($row["ContainerId"]>0) {
			$_query = sprintf('select sum(i.quantity) as QtyOnSTock 
								from (item i, container c) 
								where i.active=1 and i.articleid=%d and i.articlecolorid=%d and i.articlesizeid=%d
								and c.id=i.containerid and c.active=1 and c.id=%d', $row['ArticleId'], $row['ArticleColorId'], $row['ArticleSizeId'],$row["ContainerId"]) ; //die($_query) ;
			$_res = dbQuery($_query) ;
			$_row = dbFetch ($_res) ;
			dbQueryFree($_res) ;
		} else {
			$_row["QtyOnSTock"] = 0 ;	
		}
		listField ((int)$_row["QtyOnSTock"]) ;
//		listFieldIcon ('container.gif', 'varcontainerview', $row['ContainerId']) ; //    	listField ($row["ContainerId"]) ;
	}    	
    listEnd () ;
    dbQueryFree ($Result) ;
   
    return 0 ;
?>
