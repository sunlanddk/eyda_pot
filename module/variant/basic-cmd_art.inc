<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'module/article/include.inc' ;

    $fields = array (
	'Number'		=> array ('mandatory' => true,	'check' => true),
	'ArticleTypeId'		=> array ('type' => 'set'),
	'Description'		=> array (),
	'UnitId'		=> array ('mandatory' => true,	'type' => 'integer'),
	'Doubles'		=> array ('type' => 'checkbox',	'check' => true),
	'SupplierCompanyId'	=> array ('mandatory' => true,	'type' => 'integer',	'check' => true),
	'SupplierNumber'	=> array ('check' => true),
	'PriceStock'		=> array ('mandatory' => true,	'type' => 'decimal',	'format' => '6.2'),
	'Comment'		=> array (),
	'Public'		=> array ('type' => 'checkbox'),
	'SalesPrice'		=> array (	'type' => 'decimal',	'format' => '6.2'),
	'DeliveryComment'		=> array (),
	'VariantColor'		=> array ('type' => 'set'),
	'VariantSize'		=> array ('type' => 'set'),
	'VariantSortation'	=> array ('type' => 'set'),
	'VariantDimension'	=> array ('type' => 'set'),
	'VariantCertificate'	=> array ('type' => 'set'),
	'ListHidden'	=> array ('type' => 'checkbox')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $Id, $fields, $Record, $ArticleTypeFields ;
		$Record = array() ;
		switch ($fieldname) {
			case 'Number':
				if (!$changed) return false ;

				// Check that Article not used in live Cases
				$query = sprintf ("SELECT Id FROM `Case` WHERE Case.ArticleId=%d AND Case.Active=1 AND Case.Done=0", $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Article number can not be changed when Article are used in live Cases" ;
				
				// Partitial specified number ?
				$res = articleNumberFill ($value) ;
				if (is_string ($res)) return $res ;
				
				// Get Article type
				$query = sprintf ("SELECT ArticleType.Id AS ArticleTypeId, %s FROM ArticleType WHERE LeadDigit=\"%s\" AND Active=1", $ArticleTypeFields, addslashes(substr($value,0,1))) ;
				$result = dbQuery ($query) ;
				$Record = array_merge ($Record, dbFetch ($result)) ;
				dbQueryFree ($result) ;
				if ($Record['ArticleTypeId'] <= 0) return sprintf ('article type not known, digit "%s"', substr($value,0,1)) ;
				$fields['ArticleTypeId']['value'] = $Record['ArticleTypeId'] ;

				// Ensure that Article number not allready are used
				$query = sprintf ("SELECT Id FROM Article WHERE Number=\"%s\" AND Active=1 AND Id<>%d", addslashes($value), $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Article number allready in use" ;

				// Ensure Article not used in Cases
				$query = sprintf ("SELECT Id FROM `Case` WHERE ArticleId=%d AND Active=1 AND Done=0", $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Article used in Cases" ;

				// Ensure Article not used in Stock
				$query = sprintf ("SELECT Id FROM Item WHERE ArticleId=%d AND Active=1", $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Article used for Items on Stock" ;

				// Save Article number
				$fields[$fieldname]['value'] = $value ;

				// Set default values for new Article
				if ($Id <= 0) {
					if ($Record['ArticleTypeProduct']) {
					$fields['VariantColor']['value'] = 1 ;
					$fields['VariantSize']['value'] = 1 ;
					$fields['VariantSortation']['value'] = 1 ;
					}
					if ($Record['ArticleTypeFabric'] or substr($value,0,1) == '4') {
					$fields['VariantColor']['value'] = 1 ;
					}
				}
				return true ;

				case 'SupplierCompanyId' :
				if (!$changed) return false ;
				
				// Ensure Article has no Certificates, since they depend on the Supplier
				$query = sprintf ("SELECT Id FROM ArticleCertificate WHERE ArticleId=%d AND Active=1", $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Supplier can not be changed when Certificates has been assigned to the Article" ;

				return true ;

			case 'SupplierNumber' :
				if ($Id <= 0 and $value == '' and $Record['ArticleTypeProduct']) {
					// Default to Article.Number for new Items
					$value = $Record['Number'] ;
					$fields[$fieldname]['value'] = $value ;
				}

				// Check for doublets
				if ($value != '' and !$fields['Doubles']['value']) {
					$query = sprintf ('SELECT COUNT(Id) AS Count FROM Article WHERE Article.Id<>%d AND Article.SupplierCompanyId=%d AND Article.SupplierNumber="%s" AND Article.Active=1', (int)$Id, (int)$Record['SupplierCompanyId'], addslashes($value)) ;
					$res = dbQuery ($query) ;
					$row = dbFetch ($res) ;
					dbQueryFree ($res) ;
					if ((int)$row['Count'] > 0) return 'the Supplier Reference has allready been used - ' . $query ;
				}
				return true ;	

			case 'Doubles' :
				return false ;
		}
		return false ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $Id = -1 ;
	    break ;
    }
     
    // Save record
    $res = saveFields ('Article', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    if ($Record['ArticleTypeProduct']) {
			// Create default version 1 style
			require_once 'lib/table.inc' ;
			$Style = array ('ArticleId' => (int)$Record['Id'], 'Version' => 1, 'Active' => 1) ;
			tableWrite ('Style', $Style) ;
			$articlepurchaseunit = array ('ArticleId' => (int)$Record['Id'], 'UnitId' => (int)$Record['UnitId'], 'Ratio' => 1) ;
			tableWrite ('articlepurchaseunit', $articlepurchaseunit) ;
	    }
	    
	    // View new article
	    return navigationCommandMark ('articleview', (int)$Record['Id']) ;
    }
    
    return 0 ;    
?>
