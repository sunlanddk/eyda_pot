<?php
   
    require_once "lib/navigation.inc" ;
    require_once "lib/object.inc" ;
    require_once "lib/perm.inc" ;
    
    // Approvements
    $query = sprintf ("SELECT Approval.Id, Approval.Name, Approval.ReadyDate, Approval.Ready, Approval.EndDate, Project.Name AS ProjectName, CONCAT(User.FirstName, ' ', User.LastName) AS UserName FROM Object, ObjectType LEFT JOIN Object AS Approval ON Object.ParentId=Approval.Id LEFT JOIN Project ON Object.ProjectId=Project.Id LEFT JOIN User ON Approval.UserId=User.Id WHERE Object.Done=0 AND Object.Active=1 AND Object.UserId=%d AND Object.ObjectTypeId=ObjectType.Id AND ObjectType.Mark='approveuser' ORDER BY Approval.EndDate", $User["Id"]) ;
    $result = dbQuery ($query) ;        
    printf ("<H3>Project approvements</H3>\n") ;
    printf ("<table class=list>\n<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=70><p class=listhead>Due</p></td>") ;
    printf ("<td class=listhead width=90><p class=listhead>Project</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Name</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Responsible</p></td>") ;
    printf ("<td class=listhead width=120><p class=listhead>Ready</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($result)) {
	$icon = "image/object/approve" ;
	$icon .= (time() > (dbDateDecode($row["EndDate"])+60*60*24)) ? "_over" : "_open" ;
	$icon .= ".gif" ;
	printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", $icon, navigationOnClickMark ("phase", $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d", dbDateDecode($row["EndDate"]))) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["ProjectName"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Name"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["UserName"])) ;
	printf ("<td><p class=list>%s</p></td>", ($row["Ready"]) ? date ("Y-m-d H:i:s", dbDateDecode($row["ReadyDate"])) : "No") ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($result) == 0) {
	printf ("<tr><td></td><td colspan=3><p>Nothing to approve</p></td></tr>") ;
    }
    printf ("</table>\n") ;
    dbQueryFree ($result) ;

    // My activities
    print "<H3>Activites</H3>\n" ;
    $query = sprintf ("SELECT Object.Id, Object.EndDate, Object.CreateDate, Object.Name, Object.External, Object.Done, %s, Category.Name AS Category, Project.Name AS ProjectName FROM Object, ObjectType LEFT JOIN Category ON Object.CategoryId=Category.Id LEFT JOIN Project ON Object.ProjectId=Project.Id WHERE Object.ProjectId=%d AND Object.UserId=%d AND Object.Active=1 AND Object.Done=0 AND ObjectType.OverdueList=1 AND Object.ObjectTypeId=ObjectType.Id %s ORDER BY Object.EndDate", $ObjectTypeFields, $Project["Id"], $User['Id'], permClauseExternal()) ;
    $result = dbQuery ($query) ;
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=70><p class=listhead>Due</p></td>") ;
    printf ("<td class=listhead width=90><p class=listhead>Project</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Header</p></td>") ;
    printf ("<td class=listhead width=100><p class=listhead>Created</p></td>") ;
    printf ("<td class=listhead width=90><p class=listhead>Category</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($result)) {
	printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", objIcon($row), navigationOnClickMark ($row["NavigationMark"], $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d", dbDateDecode($row["EndDate"]))) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["ProjectName"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Name"])) ;
	printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i", dbDateDecode($row["CreateDate"]))) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Category"])) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($result) == 0) {
        printf ("<tr><td></td><td colspan=3><p class=list>No overdue activities</p></td></tr>\n") ;
    }
    printf ("</table>\n") ;
    dbQueryFree ($result) ;
   
    
    return 0 ;
?>
