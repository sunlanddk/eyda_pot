<?php

    require_once 'lib/html.inc' ;

    // Get Line No
    $Line = (int)$_GET['line'] ;

    // Line existing
    $ColorId = $Record[sprintf('Line%02dColorId', $Line)] ;
    if (!isset($ColorId)) return sprintf ('%s(%s) invalid line, id %d, no %d', $Id, $Line) ;
    if ($ColorId > 0) {
	// Get record
	$query = sprintf ("SELECT Color.Number, Color.Description, ColorGroup.Id AS ColorGroupId, ColorGroup.Name AS ColorGroupName, ColorGroup.ValueRed, ColorGroup.ValueBlue, ColorGroup.ValueGreen FROM Color LEFT JOIN ColorGroup ON ColorGroup.Id=Color.ColorGroupId WHERE Color.Id=%d", $ColorId) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	$ColorNumber = $row['Number'] ;
    }

    // Article header
    printf ("<br><table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s, %s</tr>\n", htmlentities($Record['ArticleNumber']), htmlentities($Record['ArticleDescription'])) ;
    printf ("<tr><td class=itemlabel>Colour</td><td class=itemfield>%s, %s</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Line</td><td class=itemfield>%d</tr>\n", $Line) ;
    printf ("</table>\n<br>\n") ;
  
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=line value=%d>\n", $Line) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Colour</td><td><input type=text name=Number maxlength=9 size=9' value=\"%s\"></td></tr>\n", $ColorNumber) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=40 size=40' value=\"%s\"></td></tr>\n", htmlentities($Record[sprintf ('Line%02dDescription', $Line)])) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
