<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'module/color/include.inc' ;
    require_once 'lib/uts.inc' ;

    $ColorCreated = false ;
    
    $fields = array (
//	'Number'		=> array ('mandatory' => true,	'check' => true),
	'ColorId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Reference'		=> array (),
	'Comment'		=> array (),
	'PriceAdjustPercentage'	=> array ('type' => 'decimal',	'format' => '4.1'),
	'HeavyMetals'		=> array ('type' => 'checkbox'),
//	'ColorId'		=> array ('type' => 'set'),
		'Archived'			=> array ('type' => 'checkbox','check' => true),
		'ArchivedUserId'	=> array ('type' => 'set'),
		'ArchivedDate'		=> array ('type' => 'set'),
	'OrderedDate'		=> array ('type' => 'date',				'check' => true),
	'ReceivedDate'		=> array ('type' => 'date',				'check' => true),
	'RejectedDate'		=> array ('type' => 'date',				'check' => true),
	'ApprovedDate'		=> array ('type' => 'date',				'check' => true),
	'CustomerCompanyId'	=> array ('type' => 'integer'),
	'SalesUserId'		=> array ('type' => 'integer'),
	'SexId'		=> array ('type' => 'integer'),
		'ReceivedUserId'	=> array ('type' => 'set'),
		'ReceivedUserDate'	=> array ('type' => 'set'),
		'OrderedUserId'		=> array ('type' => 'set'),
		'OrderedUserDate'	=> array ('type' => 'set'),
		'ApprovedUserId'	=> array ('type' => 'set'),
		'ApprovedUserDate'	=> array ('type' => 'set'),
		'ReceiveCompanyId'	=> array ('type' => 'integer'),
		'LabdyeComment'		=> array (),
		'OrderedQty'		=> array ('type' => 'decimal',	'format' => '4.2',				'check' => true),
		'ReceivedQty'		=> array ('type' => 'decimal',	'format' => '4.2',				'check' => true)
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $fields['ArticleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['ArticleId'] = $Id ;
	    $Id = -1 ;
	    break ;
    }
    $ReceivedDateChanged = false ;
    $OrderedDateChanged = false ;
	$ReceivedQty = 0 ;
   
    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields, $ColorCreated, $ReceivedDateChanged, $ReceivedQty ;
	switch ($fieldname) {
	    case 'OrderedQty':
		if (!$changed) return false ;
		return true ;

	    case 'ReceivedQty':
		if (!$changed) return false ;
		$ReceivedQty = $value ;
		return true ;
		
	    case 'OrderedDate':
		if (!$changed) return false ;
		if ($_POST['OrderedDate']<'2001-01-02' and $Record['OrderedDate']<'2001-01-02')  return false ;
		
		$OrderedDateChanged = true ;
     	      $fields['Archived']['type'] = 'set' ;
     	      $fields['Archived']['value'] = 0 ;

		// Set tracking information
		$fields['OrderedUserId']['value'] =  $User['Id'] ;
		$fields['OrderedUserDate']['value'] = dbDateEncode(time()) ;
		return true ;
		
	    case 'Archived':
		if ($OrderedDateChanged) {
	   		return true ;
		}
		if (!$changed) return false ;
		if (!isset($_POST['Archived']))  return false ;
		
		// Set tracking information
		$fields['ArchivedUserId']['value'] =  $User['Id'] ;
		$fields['ArchivedDate']['value'] = dbDateEncode(time()) ;
		return true ;
		
	    case 'ApprovedDate':
		if (!$changed) return false ;
		if ($_POST['ApprovedDate']<'2001-01-02' and $Record['ApprovedDate']<'2001-01-02')  return false ;
		// Set tracking information
		$fields['ApprovedUserId']['value'] =  $User['Id'] ;
		$fields['ApprovedUserDate']['value'] = dbDateEncode(time()) ;
		return true ;
		
		case 'ReceivedDate':
		if (!$changed) return false ;
		if ($_POST['ReceivedDate']<'2001-01-02' and $Record['ReceivedDate']<'2001-01-02')  return false ;
		if ($fields['ReceivedDate']['value']) {
 	    	$ReceivedDateChanged = true ;
	   		$_POST['RejectedDate'] = '' ;
		}
		$fields['ReceivedUserId']['value'] =  $User['Id'] ;
		$fields['ReceivedUserDate']['value'] = dbDateEncode(time()) ;
 	    return true ;
			
	    case 'RejectedDate':
		if ($ReceivedDateChanged) {
	   		$_POST['RejectedDate'] = '' ;
	   		return true ;
		}
	    if (!$changed) return false ;
		return true ;
	
	    case 'ColorId':
		if (!$changed) return false ;
	
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ArticleColor WHERE ArticleId=%d AND Active=1 AND ColorId=%d AND Id<>%d', $Record['ArticleId'], $value, $Id) ; //die($query) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'Color allready existing' ;

		return true ;

	    case 'Number':
		if (!$changed) return false ;

		// Fill out color number
		$r = colorNumberFill ($value) ;
		if (is_string($r)) return $r ;

		// Locate the color number
		$query = sprintf ('SELECT Id FROM Color WHERE Number="%s" AND Active=1', addslashes($value)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$ColorId = (int)$row['Id'] ;
		if ($ColorId <= 0) {
		    // Color not found
		    // Create it 
		    $row = array ('Number' => $value) ;
		    $ColorId = tableWrite ('Color', $row) ;

		    $ColorCreated = true ;
		    
		} else {
		    // Color found
		    // Check that name does not allready exist
		    $query = sprintf ('SELECT Id FROM ArticleColor WHERE ArticleId=%d AND Active=1 AND ColorId=%d AND Id<>%d', $Record['ArticleId'], $ColorId, $Id) ;
		    $result = dbQuery ($query) ;
		    $count = dbNumRows ($result) ;
		    dbQueryFree ($result) ;
		    if ($count > 0) return 'Color allready existing' ;
		}
		
		// Set new color
		$fields['ColorId']['value'] = $ColorId ;
		return false ;
	}
	return false ;
    }

    $fields['OrderedQty']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;
    $fields['ReceivedQty']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;

    $r = saveFields ('ArticleColor', $Id, $fields, true) ;
    if ($r) return $r ;
 
	// Create Collectionmember?
	If ($Navigation['Parameters']=='new' and $_POST['CollectionId']>0) {
		$query = sprintf("SELECT cl.Id as Id, cl.Name AS Value FROM Collectionlot cl, collection c WHERE c.Id=%d and cl.SeasonId=c.SeasonId AND cl.Active=1 ORDER BY Date", (int)$_POST['CollectionId']) ;
		$clres = dbQuery ($query) ;
		$clrow = dbFetch($clres) ;
		dbQueryFree ($clres) ;


		$CollectionMember['CollectionId'] = (int)$_POST['CollectionId'] ;
		$CollectionMember['ArticleId'] = (int)$Record['ArticleId'] ;
		$CollectionMember['ArticleColorId'] = (int)(int)$Record['Id'];
		$CollectionMember['CollectionLOTId'] = (int)$clrow['Id']; //$_POST['CollectionLOTId'];
		$_colmid = tableWrite ('CollectionMember', $CollectionMember) ;

		// always make price record with currencyid=0
		$CollectionMemberPrice['CollectionMemberId'] = (int)$_colmid ;
		$CollectionMemberPrice['WholesalePrice'] = (float)0;
		$CollectionMemberPrice['RetailPrice'] = (float)0 ;
		$CollectionMemberPrice['CampaignPrice'] = (float)0;
		$CollectionMemberPrice['CurrencyId'] = (int)0;
		$CollectionMemberPrice['CollectionPriceGroupId'] = (int)0;
		$ColletionMemberPriceId=tableWrite ('CollectionMemberPrice', $CollectionMemberPrice) ;
	}
	// Insert variantcode
	if ($Navigation['Parameters']=='new') {
		require_once 'lib/parameter.inc' ;
		require_once 'lib/variant.inc' ;

		$_varbasecode = ParameterGet('BaseSKU787') ;
		$_query = sprintf ('SELECT * FROM articlesize WHERE Active=1 and articleid=%d', (int)$Record['ArticleId']) ;
		$_result = dbQuery ($_query) ;
		while ($sizerow = dbFetch ($_result)) {
			$_variantcode = GetNextVariantCode($_varbasecode) ;
			$Variant = array (
				'ArticleId' 		=>	(int)$Record['ArticleId'],
				'ArticleColorId'	=>	(int)$Record['Id'],
				'ArticleSizeId' 	=>	(int)$sizerow['Id'],
				'VariantCode'		=>  $_variantcode
			) ;
			$_variantcodeid=tableWrite('variantcode', $Variant) ;
		}
		dbQueryFree ($_result) ;
	}

	if ($ReceivedQty > 0) {
 	    $Item = array (
			'ArticleId' => (int)$Record['ArticleId'],
			'ArticleColorId' => (int)$Record['Id'],
			'ContainerId' => (int)parameterGet('FabricDevelContainer'),
			'OwnerId' => (int)$User['CompanyId'],
			'PurchasedForId' => (int)$fields['CustomerCompanyId']['value'] ,
			'BatchNumber' => 'DevelSampl' ,
			'Sortation' => 0,
			'Quantity' =>  $fields['ReceivedQty']['value']
		) ;
		$ItemId=tableWrite ('Item', $Item) ;

    }
    if ($ColorCreated) {
		// View color created
		return navigationCommandMark ('coloredit', $fields['ColorId']['value']) ;
    }

    return 0 ;
?>
