<?php

    require_once 'lib/table.inc' ;

    // Ensure ArticleColor not used in ProductionOrder
    $query = sprintf ('SELECT Id FROM ProductionQuantity WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used in ProductionOrder' ;
 
    // Ensure ArticleColor not used in Order
    $query = sprintf ('SELECT Id FROM OrderLine WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used in Order' ;

    // Ensure ArticleColor not used in Invoice
    $query = sprintf ('SELECT Id FROM InvoiceLine WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used in Invoice' ;

    // Ensure ArticleColor not used for Items
    $query = sprintf ('SELECT Id FROM Item WHERE ArticleColorId=%d AND Active=1', $Record['Id']) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return 'Color used for Items' ;
    		   
    tableDelete ('ArticleColor', $Id) ;

    return 0
?>
