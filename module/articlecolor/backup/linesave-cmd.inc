<?php

    require_once 'lib/save.inc' ;

    // Get Line No
    $Line = (int)$_POST['line'] ;

    // Line existing
    $ColorId = $Record[sprintf('Line%02dColorId', $Line)] ;
    if (!isset($ColorId)) return sprintf ('%s(%s) invalid line, id %d, no %d', $Id, $Line) ;

    // Locate the color number
    $ColorNumber = trim(stripslashes($_POST['Number'])) ;
    $query = sprintf ('SELECT Id FROM Color WHERE Number="%s" AND Active=1', addslashes($ColorNumber)) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $ColorId = (int)$row['Id'] ;
    if ($ColorId <= 0) return sprintf ('Color not found, number "%s"', $ColorNumber) ;
    
    $fields = array (
	sprintf ('Line%02dColorId', $Line)	=> array ('type' => 'set', 'value' => $ColorId),
	sprintf ('Line%02dDescription', $Line)	=> array ('type' => 'set', 'value' => stripslashes($_POST['Description']))
    ) ;

    return saveFields ('ArticleColor', $Id, $fields) ;
?>
