<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;

    class PDF extends FPDF {
	
	function Header() {
	    global $Record, $Company, $CompanyMy, $Now ;

	    $this->SetAutoPageBreak(false) ; 

	    // Borders
//	    $this->Line(15,30,205,30) ;
//	    $this->Line(15,44,205,44) ;
//	    $this->Line(15,49,205,49) ;
//	    $this->Line(15,280,205,280) ;

//	    $this->Line(15,30,15,280) ;
//	    $this->Line(205,30,205,280) ;

//	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['FromCompanyId']) ;
	    $ImageString = 'image/logo/2.jpg';
	    $this->Image ($ImageString, 140, 13, 64) ;
	    $this->SetFont('Arial','',10);
	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);

	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
	    $this->SetFont('Arial','',8);
	    $this->Cell(49, 4, $CompanyMy['Name'], 0, 1, 'R') ;
	    $this->Cell(49, 4, $CompanyMy['Address1'], 0, 1, 'R') ;
	    if ($CompanyMy['Address2']) $this->Cell(48, 4, $CompanyMy['Address2'], 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('%s-%s %s', $CompanyMy['CountryName'], $CompanyMy['ZIP'], $CompanyMy['City']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Telephone %s', $CompanyMy['PhoneMain']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Fax %s', $CompanyMy['PhoneFax']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Reg. No %s', $CompanyMy['RegNumber']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Email %s', $CompanyMy['Mail']), 0, 1, 'R') ;

	    // Address
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(25,0,0) ;
	    $this->SetY (40) ;
	    $this->Cell(80, 4, $Company['Name'], 0, 1) ;
	    $this->Cell(80, 4, $Company['Address1'], 0, 1) ;
	    if ($Company['Address2']) $this->Cell(80, 4, $Company['Address2'], 0, 1) ;
	    $this->Cell(80, 4, sprintf ('%s %s', $Company['ZIP'], $Company['City']), 0, 1) ;
	    $this->Cell(80, 4, $Company['CountryDescription'], 0, 1) ;

	    // Text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(160,0,0) ;
	    $this->SetY (85) ;
	    
	    $this->SetFont('Arial','',9);
	    $this->Cell(16, 4, 'Date') ;
	    $this->Cell(30, 4, date('Y.m.d', ($Record['OrderedDate']) ? dbDateDecode($Record['OrderedDate']) : $Now)) ;
	    
	    // Initialize for main page
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(25,100,6) ;
	    $this->SetY (100) ;
	    $this->SetAutoPageBreak(true, 30) ; 
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

	}
	
	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }
 
    // Variables
    $LastPage = false ;

    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)2) ;
    $result = dbQuery ($query) ;
    $CompanyMy = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$CompanyMy['Id'] <= 0) return 'CompanyMy not found' ;

    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)18) ;
    $result = dbQuery ($query) ;
    $CompanyTo = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$CompanyMy['Id'] <= 0) return 'CompanyTo not found' ;

    // Insert tripple-space in name
    if ($CompanyMy['Name'] != '') {
	$s = $CompanyMy['Name']{0} ;
	$n = 1 ;
	while ($CompanyMy['Name']{$n}) $s .= '  ' . $CompanyMy['Name']{$n++} ;
		$CompanyMy['NameExpanded'] = strtoupper($s) ;
    }
    // Get Full Company information
	$query = sprintf ('SELECT Company.*, Country.Id as CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId LEFT JOIN PaymentInformation ON PaymentInformation.CompanyId=Company.Id WHERE Company.Id=%d', (int)$Record['CompanyId']) ;
    $result = dbQuery ($query) ;
    $Company = dbFetch ($result) ;
    dbQueryFree ($result) ;


    // Make PDF
    $pdf=new PDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->SetAutoPageBreak(true, 15) ; 
    $pdf->AddPage() ;
	
    $pdf->SetFont('Arial','BU',9);
	$pdf->Cell(180, 4, 'Regarding: Lab dye request') ;
	$pdf->Ln () ;
	$pdf->Ln () ;
    $pdf->SetFont('Arial','',9);
    if ($Record['RejectedDate']>'0001-01-01')
		$pdf->Cell(180, 4, 'Lab dyes has been rejected on:') ;
    else
		$pdf->Cell(180, 4, 'Please make lab dyes on:') ;
	$pdf->Ln () ;
	$pdf->Ln () ;
	$pdf->Cell(180, 4, sprintf('   %s', $Record['ArticleDescription'])) ;
	$pdf->Ln () ;
	$pdf->Cell(180, 4, sprintf('   Our ref:  %s',$Record['ArticleNumber'])) ;
//	if ($Record['SupplierNumber'])
		$pdf->Ln () ;
		$pdf->Cell(180, 4, sprintf('   Your ref: %s', $Record['SupplierNumber'])) ;
	$pdf->Ln () ;
	$pdf->Ln () ;
	$pdf->Cell(180, 4, 'in the color:') ;
	$pdf->Ln () ;
	$pdf->Ln () ;
	$pdf->Cell(180, 4, sprintf('   %s (%09d) based on %s', $Record['Description'],$Record['Number'],$Record['Reference'])) ;
	$pdf->Ln () ;
	$pdf->Ln () ;
	if ($Record['LabdyeComment']) {
	    if ($Record['RejectedDate']>'0001-01-01') {
			$pdf->Cell(40, 4, 'Please make new lab dyes and note the following:') ;
			$pdf->Ln () ;
		}
		$pdf->MultiCell(60, 4, $Record['LabdyeComment']) ;
		$pdf->Ln () ;
		$pdf->Ln () ;
	} else {
	    if ($Record['RejectedDate']>'0001-01-01') {
			$pdf->Cell(39, 4, 'Please make new lab dyes.') ;
			$pdf->Ln () ;
			$pdf->Ln () ;
		}
	}
	$pdf->Cell(180, 4, 'Please return this document together with the lab dyes to:') ;
	$pdf->Ln () ;
    $pdf->SetMargins(29,0,0) ;
	$pdf->Ln () ;
    $pdf->Cell(49, 4, 'Att: Ivanka Bardyn', 0, 1, 'L') ;
    $pdf->Cell(49, 4, $CompanyTo['Name'], 0, 1, 'L') ;
    $pdf->Cell(49, 4, $CompanyTo['Address1'], 0, 1, 'L') ;
    if ($CompanyTo['Address2']) $pdf->Cell(48, 4, $CompanyTo['Address2'], 0, 1, 'L') ;
    $pdf->Cell(49, 4, sprintf ('%s-%s %s', $CompanyTo['CountryName'], $CompanyTo['ZIP'], $CompanyTo['City']), 0, 1, 'L') ;
	$pdf->Ln () ;
    $pdf->Cell(49, 4, sprintf ('Telephone %s', $CompanyTo['PhoneMain']), 0, 1, 'L') ;
    $pdf->Cell(49, 4, sprintf ('Fax %s', $CompanyTo['PhoneFax']), 0, 1, 'L') ;
//	    $pdf->Cell(49, 4, sprintf ('Reg. No %s', $CompanyTo['RegNumber']), 0, 1, 'L') ;
//	    $pdf->Cell(49, 4, sprintf ('Email %s', $CompanyTo['Mail']), 0, 1, 'L') ;
    $pdf->SetMargins(25,0,0) ;
     
	$pdf->Ln () ;
	$pdf->Ln () ;
	$pdf->Ln () ;
	$pdf->Ln () ;
    $pdf->SetFont('Arial','B',9);
	$pdf->Cell(180, 4, 'Best regards') ;
	$pdf->Ln () ;
	$pdf->Ln () ;
    $pdf->Cell(49, 4, $CompanyMy['Name'], 0, 1, 'L') ;
    // Last Page generated
    $LastPage = True ;
    
    // Generate PDF document
    $pdfdoc = $pdf->Output('', 'S') ;

    // Download
    if (headers_sent()) return 'stop' ;
    httpNoCache ('pdf') ;
    httpContent ('application/pdf', sprintf('Labdye%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
    print ($pdfdoc) ; 

	dbQueryFree ($Result) ;
	
    return 0 ;
?>
