ALTER TABLE `devel`.`ArticleColor` ADD COLUMN `OrderedDate` DATE NULL ;
ALTER TABLE `devel`.`ArticleColor` ADD COLUMN `ReceivedDate` DATE NULL ;
ALTER TABLE `devel`.`ArticleColor` ADD COLUMN `RejectedDate` DATE NULL ;
ALTER TABLE `devel`.`ArticleColor` ADD COLUMN `ApprovedDate` DATE NULL ;
ALTER TABLE `devel`.`ArticleColor` ADD COLUMN `CustomerCompanyId` INTEGER NOT NULL DEFAULT 0 ;
ALTER TABLE `devel`.`ArticleColor` ADD COLUMN `SalesUserId` INTEGER NOT NULL DEFAULT 0 ;
