<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    function flag (&$Record, $field) {
		if ($Record[$field]) {
			itemField ($field, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($field, formCheckbox ($field, $Record[$field])) ;
		}
    }

 	if (isset($_GET['CollectionId'])) {
		$collection_id = (int)$_GET['CollectionId'] ;
	} else {
		$collection_id = 1 ;
	}
   switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    $ArticleNumber = $Record['Number'];
	    unset ($Record) ;
	    $Record['PriceAdjustPercentage'] = 0 ;
	    $Record['ArticleId'] = $Id ;
	    $Record['ArticleNumber'] = $ArticleNumber ;
	    break ;
    }
    $MyCompanyId = parameterGet ('CompanyMy') ;
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
//    printf ("<tr><td class=itemlabel>Colour</td><td>%s", htmlentities($Record['Number'])) ;
//    itemField ('Colour', sprintf ('%s, %s', $Record['Number'], $Record['Description'])) ;
//    printf ("<tr><td class=itemlabel>Colour</td><td><input type=text name=Number maxlength=9 size=9' value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
	printf ("<tr><td class=itemlabel>Colour:</td><td>%s</td></tr>\n", htmlDBSelect ("ColorId style='width:250px'", $Record['ColorId'], 'SELECT Id, Concat(Description," (",Number,")") AS Value FROM Color where active=1 order by Description')) ;  
    print htmlItemSpace() ;
	printf ("<tr><td class=itemlabel>Sex:</td><td>%s</td></tr>\n", htmlDBSelect ("SexId style='width:250px'", $Record['SexId'],
					'SELECT 0 as Id, "-- None --" AS Value UNION SELECT Id, Name AS Value FROM Sex')) ;  
    itemSpace () ;

	$query = 'SELECT Collection.Id AS Id,CONCAT(Season.Name, ", ", Collection.Name) AS Value FROM Season, collection WHERE collection.seasonid=season.id AND season.Active=1 AND collection.Active=1 ORDER BY Value' ;
	$reload = '' ; //sprintf('onchange="appLoad(%d,%d,\'CollectionId=\'+document.appform.CollectionId.options[document.appform.CollectionId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
	itemFieldRaw ('Add to Collection', formDBSelect ('CollectionId', $collection_id, $query, 'width:250px;', NULL, $reload)) ; 
/* Comment back in if more than one LOT for a season	
	if ($collection_id > 0) {
		$query = sprintf("SELECT cl.Id as Id, cl.Name AS Value FROM Collectionlot cl, collection c WHERE c.Id=%d and cl.SeasonId=c.SeasonId AND cl.Active=1 ORDER BY Date", $collection_id) ;
		$res = dbQuery ($query) ;
		$row = dbFetch($res) ;
		itemFieldRaw ('LOT', formDBSelect ('CollectionLOTId', (int)$row['Id'], $query, 'width:100px;')) ; 
		dbQueryFree ($res) ;
	}
*/

/*	
    itemFieldRaw ('Add to Collection', 
				formDBSelect ('CollectionId', 0, 'SELECT Collection.Id AS Id,CONCAT(Season.Name, ", ", Collection.Name) AS Value FROM Season, collection WHERE collection.seasonid=season.id AND season.Active=1 AND collection.Active=1 ORDER BY Value', 'width:280px;')
				) ;
*/				
    itemSpace () ;
    itemField ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Supplier', sprintf ('%s (%s)', $Record['SupplierCompanyName'], $Record['SupplierCompanyNumber'])) ;
    itemField ('Reference', $Record['SupplierNumber']) ;
 /*
	if ($Record['PrintNumber']!='')
     itemField ('Print number', $Record['PrintNumber']) ;
    itemSpace () ;

	printf ("<tr><td class=itemlabel>Customer</td><td>%s</td></tr>\n", htmlDBSelect ("CustomerCompanyId style='width:250px'", $Record['CustomerCompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCustomer=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Sales Ref</td><td>%s</td></tr>\n", htmlDBSelect ('SalesUserId style="width:250px"', $Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=%d AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 ORDER BY Value',$MyCompanyId))) ;  
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Lab dye dates:</td></tr>\n") ;  

    $usertxt = $Record['OrderedUserId']>0?'    (by "' . tableGetField('User','Loginname',$Record['OrderedUserId']) . '" at ' . $Record['OrderedUserDate'] . ')' :'' ;

    itemFieldRaw ('Ordered', formDate ('OrderedDate', $Record['OrderedDate']>'2001-01-02'?dbDateDecode($Record['OrderedDate']):'') . $usertxt ) ;
    itemFieldRaw ('Qty Ordered', formText ('OrderedQty', number_format($Record['OrderedQty'], (int)$Record['UnitDecimals'], ',', ''), 8, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 

    $usertxt = $Record['ReceivedUserId']>0?'    (by "' . tableGetField('User','Loginname',$Record['ReceivedUserId']) . '" at ' . $Record['ReceivedUserDate'] . ')' :'' ;
    itemFieldRaw ('Received', formDate ('ReceivedDate',  $Record['ReceivedDate']>'2001-01-02'?dbDateDecode($Record['ReceivedDate']):'') .  $usertxt) ;
    itemFieldRaw ('Qty Received', formText ('ReceivedQty', number_format($Record['ReceivedQty'], (int)$Record['UnitDecimals'], ',', ''), 8, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 

    itemFieldRaw ('Rejected', formDate ('RejectedDate', $Record['RejectedDate']>'2001-01-02'?dbDateDecode($Record['RejectedDate']):'')) ;

    $usertxt = $Record['ApprovedUserId']>0?'    (by "' . tableGetField('User','Loginname',$Record['ApprovedUserId']) . '" at ' . $Record['ApprovedUserDate'] . ')' :'' ;
    itemFieldRaw ('Approved', formDate ('ApprovedDate',  $Record['ApprovedDate']>'2001-01-02'?dbDateDecode($Record['ApprovedDate']):'') .  $usertxt) ;
    itemSpace () ;
    flag ($Record, 'Archived') ;
    print htmlItemSpace() ;

    printf ("<tr><td class=itemlabel>Supplier Ref</td><td><input type=text name=Reference maxlength=20 size=20' value=\"%s\"></td></tr>\n", htmlentities($Record['Reference'])) ;
    itemSpace () ;
    printf ("<tr><td class=itemlabel>Price Adjust</td><td><input type=text name=PriceAdjustPercentage maxlength=6 size=6' value=\"%s\"> %%</td></tr>\n", $Record['PriceAdjustPercentage']) ;
    printf ("<tr><td class=itemlabel>No Heavy Metals</td><td><input type=checkbox name=HeavyMetals %s></td></tr>\n", ($Record['HeavyMetals'])?'checked':'') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:140px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
	printf ("<tr><td class=itemlabel>Send back to</td><td>%s</td></tr>\n", htmlDBSelect ("ReceiveCompanyId style='width:250px'", $Record['ReceiveCompanyId']>0?$Record['ReceiveCompanyId']:18, 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE Internal=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemfield>Labdye request text</td><td><textarea name=LabdyeComment style='width:100%%;height:140px;'>%s</textarea></td></tr>\n", $Record['LabdyeComment']) ;
*/
    print (htmlItemInfo ($Record)) ;

    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
