<?php

    require_once "lib/list.inc" ;

    // Article header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    // Filter Bar
    listFilterBar () ;

    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Number', 80) ;
    listHead ('Colour', 60) ;
    listHead ('', 10) ;
    listHead ('Description') ;
/*
    listHead ('Reference') ;
    listHead ('Lab dye State', 80) ;
    listHead ('Group', 100) ;
    listHead ('HeavyMetals', 80) ;
    listHead ('PriceAdjust', 80, 'align=right') ; 
*/
    listHead ('', 8) ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'articlecolorview', $row['Id']) ;
	listField ($row['Number']) ;
	if ($row['ColorGroupId'] > 0) {
	    listField ('', sprintf ('bgcolor="#%02X%02X%02X"', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'])) ;
	} else {
	    listField ('') ;
	}
	listField ('') ;
	listField ($row['Description']) ;
	listField ($row['Reference']) ;
/*
	if ($row['ApprovedDate']) {
		listField ('Approved') ;
	} elseif ($row['RejectedDate']) {
		listField ('Rejected') ;
	} elseif ($row['ReceivedDate']) {
		listField ('Received') ;
	} elseif ($row['OrderedDate']) {
		listField ('Ordered') ;
	} else listField (' ') ;

	listField (($row['ColorGroupId'] > 0) ? $row['ColorGroupName'] : '') ;
	listField (($row['HeavyMetals']) ? 'no' : 'yes') ;

	listField ($row['PriceAdjustPercentage'] .' %', 'align=right') ;
*/
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Colors', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
