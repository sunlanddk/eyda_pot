<?php

    require_once 'lib/html.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemfield>Number</td><td><input type=text name=Number maxlength=3 style='width:30px' value=\"%s\"></td></tr>\n", htmlentities($Record['Number'])) ;
    printf ("<tr><td class=itemfield>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>BundleText</td><td><input type=text name=BundleText maxlength=10 style='width:80px' value=\"%s\"></td></tr>\n", htmlentities($Record['BundleText'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Production</td><td><input type=text name=ProductionMinutes maxlength=10 style='width:80px' value=\"%s\">minutes</td></tr>\n", htmlentities($Record['ProductionMinutes'])) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ; 
    printf ("</form>\n") ;

    return 0 ;
?>
