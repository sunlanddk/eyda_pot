<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Get parent Machinegroup
	    if ($Id <= 0) return 0 ;
	    $query = sprintf ("SELECT * FROM MachineGroup WHERE Id=%d AND Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    
	    // Get Operations to list
	    $query = sprintf ("SELECT Operation.* FROM Operation WHERE Operation.MachineGroupId=%d AND Operation.Active=1 ORDER BY Operation.Number", $Id) ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $query = sprintf ("SELECT * FROM MachineGroup WHERE Id=%d AND Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
 	    return 0 ;
    }

    // Get record
    if ($Id <= 0) return 0 ;
    $query = sprintf ("SELECT * FROM Operation WHERE Id=%d AND Active=1", $Id) ;
    $Result = dbQuery ($query) ;
    $Record = dbFetch ($Result) ;
    dbQueryFree ($Result) ;
    return 0 ;
?>
