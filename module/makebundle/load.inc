<?php

    if ($Id <= 0) return 0 ;

    switch ($Navigation['Function']) {
	case 'select':
	    // Get Production
	    $queryFields = 'Production.*, CONCAT(Case.Id,"/",Production.Number) AS Number, Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription';
	    $queryTables = 'Production LEFT JOIN `Case` ON Case.Id=Production.CaseId LEFT JOIN Article ON Article.Id=Case.ArticleId' ;
	    $query = sprintf ('SELECT %s FROM %s WHERE Production.Id=%d AND Production.Active=1', $queryFields, $queryTables, $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Get colours
	    $query = sprintf ("SELECT ArticleColor.Id, CONCAT(Color.Number,' (',Color.Description,')') AS Number FROM ArticleColor, Color WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Color.Number", $Record['ArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Color = array() ;
	    while ($row = dbFetch ($result)) {
		$Color[(int)$row['Id']] = $row['Number'] ;
	    }
	    dbQueryFree ($result) ;

	    // Get sizes
	    $query = sprintf ("SELECT * FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder, ArticleSize.Name", $Record['ArticleId']) ;
	    $result = dbQuery ($query) ;
	    $Size = array() ;
	    while ($row = dbFetch ($result)) {
		$Size[(int)$row['Id']] = $row['Name'] ;
	    }
	    dbQueryFree ($result) ;
    
	    // Get counts
	    $query = sprintf ("SELECT ProductionQuantity.*, SUM(Bundle.Quantity) AS BundleQuantity FROM ProductionQuantity LEFT JOIN Bundle ON ProductionQuantity.Id=Bundle.ProductionQuantityId AND Bundle.Active=1 WHERE ProductionQuantity.ProductionId=%d AND ProductionQuantity.Active=1 GROUP BY ProductionQuantity.Id", $Record['Id']) ;
	    $result = dbQuery ($query) ;
	    unset ($Quantity) ;
	    while ($row = dbFetch ($result)) {
		$ArticleColorId = (int)$row['ArticleColorId'] ;
		$ArticleSizeId = (int)$row['ArticleSizeId'] ;
		if (!isset($Color[$ArticleColorId])) continue ;
		if (!isset($Size[$ArticleSizeId])) continue ;
		if (isset($Quantity[$ArticleColorId][$ArticleSizeId])) continue ;
		$Quantity[$ArticleColorId][$ArticleSizeId] = $row ;
	    }
	    dbQueryFree ($result) ;

	    return 0 ;

	default:
	    $query = sprintf ('SELECT ProductionQuantity.*, CONCAT(Case.Id,"/",Production.Number) AS Number, Article.Id AS ArticleId, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Production.Id AS ProductionId, Production.StyleId, Production.Description, Production.Approved, Production.Cut, Production.Done, ArticleSize.Name AS SizeName, CONCAT(Color.Description," (",Color.Number,")") AS ColorNumber, SUM(Bundle.Quantity) AS BundleQuantity 
	    FROM (ProductionQuantity, Production)
	    LEFT JOIN `Case` ON Case.Id=Production.CaseId
	    LEFT JOIN Article ON Article.Id=Case.ArticleId
	    LEFT JOIN ArticleSize ON ArticleSize.Id=ProductionQuantity.ArticleSizeId
	    LEFT JOIN ArticleColor ON ArticleColor.Id=ProductionQuantity.ArticleColorId
	    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
	    LEFT JOIN Bundle ON Bundle.ProductionQuantityId=ProductionQuantity.Id AND Bundle.Active=1
	    WHERE ProductionQuantity.Id=%d AND ProductionQuantity.Active=1 AND Production.Id=ProductionQuantity.ProductionId GROUP BY ProductionQuantity.Id', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;	    
    }
    
    return 0 ;
?>
