<?php

function label ($id, $quantity, $no=0, $operation=0, $minutes=0) {
    $output .= sprintf ("T54.5,8,0,596,4,q93;%2d\n", $quantity) ;
    $output .= sprintf ("B62,1.7,0,e,8,0.38;[U:CODEC]%d%08d%02d%d\n", ($no == 0) ? 2 : 3, $id, $no, 0) ;
    $output .= sprintf ("T62.5,12,0,596,2.8,q93;%08d", $id) ;
    if ($no > 0) $output .= sprintf (" %02d %05s %03.2f", $no, $operation, $minutes) ;
    $output .= "\n" ;
    return $output ;
}

function PrintLabel ($id, $queue=NULL) {

    // Check printer
    if (is_null($queue)) return ;
    
    // Get Bundle information
    $query = sprintf ("SELECT Bundle.Id, Bundle.Quantity, ArticleSize.Name AS ArticleSizeName, Color.Description AS ColorDescription, Production.Number AS ProductionNumber, Production.CaseId, Production.StyleId, Production.SourceLocationId, Production.SourceFromNo, Production.SourceToNo, Production.BundleText, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Style.Version AS StyleVersion, Style.SplitOperationNo, Company.Name AS CompanyName 
    FROM Bundle
    LEFT JOIN Production ON Production.Id=Bundle.ProductionId
    LEFT JOIN `Case` ON Case.Id=Production.CaseId
    LEFT JOIN Company ON Company.Id=Case.CompanyId 
    LEFT JOIN ProductionQuantity ON ProductionQuantity.Id=Bundle.ProductionQuantityId
    LEFT JOIN ArticleSize ON ArticleSize.Id=ProductionQuantity.ArticleSizeId
    LEFT JOIN ArticleColor ON ArticleColor.id=ProductionQuantity.ArticleColorId
    LEFT JOIN Color ON Color.Id=ArticleColor.ColorId
    LEFT JOIN Style ON Style.Id=Production.StyleId
    LEFT JOIN Article ON Article.Id=Style.ArticleId
    WHERE Bundle.Id=%d AND Bundle.Active=1", $id) ;
    $result = dbQuery ($query) ;
    $Record = dbFetch ($result) ;
    dbQueryFree ($result) ;

    // Get operation information
    $Operation = array () ;
    $query = sprintf ("SELECT currentstyleoperation.No, currentstyleoperation.BundleText, currentstyleoperation.ProductionMinutes, CONCAT(MachineGroup.Number,Operation.Number) AS Number FROM (currentstyleoperation, Operation, MachineGroup) WHERE currentstyleoperation.StyleId=%d AND currentstyleoperation.Active=1 AND currentstyleoperation.OperationId=Operation.Id AND currentstyleoperation.MachineGroupId=MachineGroup.Id ORDER BY currentstyleoperation.No", $Record['StyleId']) ;
    $result = dbQuery ($query) ;
    $TotalProductionMinutes = 0 ;
    while ($row = dbFetch ($result)) {
	if (strlen($row['Number']) == 4) $row['Number'] .= ' ' ;
	$Operation[(int)$row['No']] = $row ;
    }
    dbQueryFree ($result) ;
    
    // Initialization
    $output = "" ;
    $output .= "mm\nzO\n" ;
    $jobheader = "J\nH50,0,D\nSl1;0.0,0.9,13.0,16.0,103.0\n" ;
    $No = 1 ;
    while ($No <= count($Operation)) {
 	// Operation sequense labels
	for ($n = 0 ; $No <= count($Operation) and !($No == $Record['SplitOperationNo'] and $n != 0) ; $No++) {    
	    $operation = &$Operation[$No] ;

	    // External operation 
	    if ($Record['SourceLocationId'] > 0 and $Record['SourceFromNo'] <= $No and $Record['SourceToNo'] >= $No) continue ;

	    if ($n == 0) {
		// 1. Header label
		$output .= $jobheader ;
		$output .= sprintf ("T1.5,4,0,596,4,q93;BUNDLE  %08d\n", $Record['Id']) ;
		$output .= sprintf ("T1.5,8,0,596,4,q93;ORDER   %s\n", $Record['CaseId'].'/'.$Record['ProductionNumber']) ;
		$output .= sprintf ("T1.5,12,0,596,4,q93;STYLE   %s\n", $Record['ArticleNumber'].'-'.$Record['StyleVersion']) ;
		$output .= sprintf ("T55,10,0,596,10,q93;%08d\n", $Record['Id']) ;
		$output .= "A 1\n" ;

		// 2. Header label
		$output .= $jobheader ;
		$output .= sprintf ("T1.5,4,0,596,4,q93;CUST    %-12.12s\n", $Record['CompanyName']) ;
		$output .= sprintf ("T1.5,8,0,596,4,q93;DESCR   %-12.12s\n", $Record['ArticleDescription']) ;
		$output .= sprintf ("T1.5,12,0,596,4,q93;QTY.    %d\n", $Record['Quantity']) ;
		$output .= label ($Record['Id'], $Record['Quantity']) ;
		$output .= "A 1\n" ;

		// 3. Header label
		$output .= $jobheader ;
		$output .= sprintf ("T1.5,4,0,596,4,q93;SIZE    %-12.12s\n", $Record['ArticleSizeName']) ;
		$output .= sprintf ("T1.5,8,0,596,4,q93;COLOR   %-12.12s\n", $Record['ColorDescription']) ;
//		$output .= sprintf ("T1.5,12,0,596,4,q93;QUALITY %-12.12s\n", $Record['Quality']) ;
		$output .= label ($Record['Id'], $Record['Quantity']) ;
		$output .= "A 1\n" ;

		// 4. Header label
		$output .= $jobheader ;
		$s = trim($Record['BundleText']) ;
		for ($n = 0 ; $n < 3 ; $n++) {
		    $p = strrpos(substr($s,0,20),' ') ;
		    if ($p > 0) {
			$s1 = substr($s,0,$p) ;
			$s = trim(substr($s,$p+1)) ;
		    } else {
			$s1 = substr($s,0,20) ;
			$s = trim(substr($s,20)) ;
		    }
		    $output .= sprintf ("T1.5,%d,0,596,4,q93;%s\n", 4+4*$n, trim($s1)) ;
		}
		$output .= label ($Record['Id'], $Record['Quantity']) ;
	    }
	    
	    // Operation Label
	    $output .= "A 1\n" ;
	    $output .= $jobheader ;
	    $output .= sprintf ("T1.5,4,0,596,4,q93;%02d %05s %-11.11s\n", $operation['No'], $operation['Number'], substr($operation['BundleText'],0,12)) ;
	    $output .= sprintf ("T1.5,8,0,596,4,q93;         %-6s % 4d\n", $operation['ProductionMinutes'], $Record['Quantity']) ;
	    $output .= label ($Record['Id'], $Record['Quantity'], $operation['No'], $operation['Number'], $operation['ProductionMinutes']*$Record['Quantity']) ;

	    $n++ ;
    	}

	// Final print command with cut request
	if ($n > 0) $output .= "C 1,2.5\nA 1\n" ;
    }
    
    if (false) {
	printf ("StyleId %d<br>\n", $Record['StyleId']) ;
	printf ("Labelprinter output:<br>%s<br>END<br>\n", nl2br($output)) ;
	return 0 ;
    }
    
    require_once "lib/spool.inc" ;
    return Spool ($queue, $output) ;
}
?>
