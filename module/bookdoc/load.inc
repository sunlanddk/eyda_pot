<?php

    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Parameters']) {
	case 'newdoc' :
	    // Get record
	    if ($Id <= 0) break ;
	    
	    $query = sprintf ("SELECT BookLevel.*, Book.Name AS BookName, Book.Description AS BookDescription FROM BookLevel, Book WHERE BookLevel.Id=%d AND BookLevel.Active=1 AND Book.Id=BookLevel.BookId", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    switch ($Navigation['Function']) {
	case 'view' :
	    // Any Id supplied ?
	    if ($Id <= 0) {
		// Get name
		if ($Navigation['Parameters']) {
		    $name = $Navigation['Parameters'] ;
		} else {
		    $name = stripslashes($_GET['name']) ;
		    if (!$name) return 0 ;
		}
		
		// Lookup name of document
		$query = sprintf ('SELECT DocVersion.Id FROM DocVersion, DocType, Doc LEFT JOIN BookLevel ON BookLevel.Id=Doc.ParentId WHERE DocType.Mark="book" AND DocType.Active=1 AND Doc.DocTypeId=DocType.Id AND Doc.Active=1 AND DocVersion.DocId=Doc.Id AND DocVersion.Active=1 AND CONCAT(BookLevel.Name,BookLevel.Delimiter,Doc.Name)="%s" ORDER BY DocVersion.Released DESC, DocVersion.Version DESC', addslashes($name)) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id = $row['Id'] ;
		if ($Id <= 0) return sprintf ('documet not fount, name "%s"', $name) ;
	    }

	    // Fall through

	case 'approve-cmd' :
	    // Load Task for approvers
	    $query = sprintf ('SELECT Task.*, TaskType.Mark, TaskType.Name AS TaskTypeName, TaskType.DisplayOrder, User.Loginname, CONCAT(User.FirstName," ",User.LastName) AS FullName FROM TaskType, Task LEFT JOIN User ON User.Id=Task.UserId WHERE Task.RefId=%d AND Task.Active=1 AND TaskType.Category="book" AND TaskType.Active=1 AND Task.TaskTypeId=TaskType.Id ORDER BY TaskType.DisplayOrder', $Id) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		if (!$row['Done']) {
		    $row['FullName'] = '-' ;
		    if ($row['UserId'] == $User['Id']) navigationEnable ('approve') ;
		}
		$Approve[(int)$row['DisplayOrder']] = $row ;
	    }
 	    dbQueryFree ($result) ;
	    
	    // Fall through
	    
	case 'mimepart' :
	case 'download' :
	case 'veredit' :
	case 'versave-cmd' :
	case 'verdelete-cmd' :
	case 'comment' :
   	    // Query document to view (Id for DocVersion)
	    $query = sprintf ("SELECT DocVersion.*, Doc.Name, Doc.Description, BookLevel.Level, BookLevel.Name AS BookLevelName, BookLevel.Delimiter AS BookLevelDelimiter, BookLevel.Description AS BookLevelDescription, Book.Name AS BookName, Book.Description AS BookDescription, Book.Footer AS BookFooter FROM DocVersion, Doc, BookLevel, Book WHERE DocVersion.Id=%d AND DocVersion.Active=1 AND Doc.Id=DocVersion.DocId AND Doc.Active=1 AND BookLevel.Id=Doc.ParentId AND BookLevel.Active=1 AND Book.Id=BookLevel.BookId AND Book.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
 	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Adjust Toolbar
	    if ($Record['Released']) navigationPasive('delete') ;
	    break ;

	case 'docdelete-cmd' :
	case 'save-cmd' :
	case 'edit' :
   	    // Query document to edit (Id for DocVersion)
	    $query = sprintf ("SELECT Doc.*, DocVersion.Id, DocVersion.DocId FROM DocVersion, Doc WHERE DocVersion.Id=%d AND Doc.Id=DocVersion.DocId AND Doc.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
 	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    break ;

	case 'versionlist' :
   	    // Get Doc.Id (Id for DocVersion)
	    $query = sprintf ("SELECT DocId FROM DocVersion WHERE DocVersion.Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
 	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    $DocId = $row['DocId'] ;
	    if ($DocId <= 0) return sprintf ('%s(%d) document version not found, id %d', __FILE__, __LINE__, $Id) ;

	    // Query Header information
	    $query = sprintf ("SELECT %d AS Id, Doc.Name, Doc.Description, BookLevel.Name AS BookLevelName, BookLevel.Delimiter AS BookLevelDelimiter, BookLevel.Description AS BookLevelDescription, BookLevel.Level, Book.Name AS BookName, Book.Description AS BookDescription FROM Doc, BookLevel, Book WHERE Doc.Id=%d AND Doc.Active=1 AND BookLevel.Id=Doc.ParentId AND BookLevel.Active=1 AND Book.Id=BookLevel.BookId", $Id, $DocId) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    // Query list of Versions
	    $query = sprintf ("SELECT DocVersion.Id, DocVersion.Version, DocVersion.Released, DocVersion.CreateDate, User.Loginname FROM DocVersion LEFT JOIN User ON User.Id=DocVersion.CreateUserId WHERE DocVersion.DocId=%d AND DocVersion.Active=1 ORDER BY DocVersion.Version DESC", $DocId) ;
	    $Result = dbQuery ($query) ;
	    break ;	    

	case 'approverlist' :
   	    // Query document to view (Id for DocVersion)
	    $query = sprintf ("SELECT DocVersion.*, Doc.Name, Doc.Description, BookLevel.Level, BookLevel.Name AS BookLevelName, BookLevel.Delimiter AS BookLevelDelimiter, BookLevel.Description AS BookLevelDescription, Book.Name AS BookName, Book.Description AS BookDescription FROM DocVersion, Doc, BookLevel, Book WHERE DocVersion.Id=%d AND DocVersion.Active=1 AND Doc.Id=DocVersion.DocId AND Doc.Active=1 AND BookLevel.Id=Doc.ParentId AND BookLevel.Active=1 AND Book.Id=BookLevel.BookId AND Book.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
 	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Query list of Approvers
	    $query = sprintf ('SELECT Task.*, TaskType.Name AS TaskTypeName, User.Loginname, CONCAT(User.FirstName," ",User.LastName) AS Fullname FROM TaskType, Task LEFT JOIN User ON User.Id=Task.UserId WHERE TaskType.Category="book" AND TaskType.Active=1 AND Task.TaskTypeId=TaskType.Id AND Task.RefId=%d AND Task.Active=1 ORDER BY TaskType.DisplayOrder', $Id) ;
	    $Result = dbQuery ($query) ;
	    break ;

	case 'approveedit' :
	case 'approvesave-cmd' :
	    // Query Approval task
	    $query = sprintf ('SELECT Task.*, TaskType.Name AS TaskTypeName FROM TaskType, Task WHERE Task.Id=%d AND Task.Active=1 AND TaskType.Category="book" AND TaskType.Active=1 AND Task.TaskTypeId=TaskType.Id', $Id) ;
	    $Result = dbQuery ($query) ;
 	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Adjust Toolbar
	    if ($Record['Done']) navigationPasive('save') ;
	    break ;
    }
   
    return 0 ;
?>
