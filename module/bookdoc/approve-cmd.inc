<?php

    require_once 'lib/save.inc' ;

    // Verify that document not allready has been approved and released
    if ($Record['Released']) return 'document has allready been approved' ; 
  
    $allapproved = true ;
    foreach ($Approve as $i => $task) {
	// Has task allready been approved
	if ($task['Done']) continue ;
	
	// Approval requested by current user ?
	if ($task['UserId'] != $User['Id']) {
	    $allapproved = false ;
	    continue ;
	}

	// Do approval
	$fields = array (
	    'Done'		=> array ('type' => 'set',	'value' => 1),
	    'DoneUserId'	=> array ('type' => 'set',	'value' => $User['Id']),
	    'DoneDate'		=> array ('type' => 'set',	'value' => dbDateEncode(time()))
	) ;
        $res = saveFields ('Task', $task['Id'], $fields) ; 
	if ($res) return $res ;
    }

    // Release document if all approved
    if ($allapproved) {
	// Release document
	$fields = array (
	    'Released'		=> array ('type' => 'set',	'value' => 1)
	) ;
        $res = saveFields ('DocVersion', $Id, $fields) ; 
	if ($res) return $res ;
    }
    
    return 0 ; 
?>
