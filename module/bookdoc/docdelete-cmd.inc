<?php

    // Id referes to a DocVersion assiciated the document !!!

    // Check if the document has released versions
    $query = sprintf ("SELECT Id FROM DocVersion WHERE Active=1 AND Released=1 AND DocId=%d", $Record["DocId"]) ;
    $result = dbQuery ($query) ;
    $n = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($n > 0) return "this document can't be deleted because it is has been released" ;   
   
    // Delete
    require_once "lib/table.inc" ;
    tableDelete ("Doc", $Record['DocId']) ;
    tableDeleteWhere ('DocVersion', 'DocId='.$Record['DocId']) ;

    return 0
?>
