<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/file.inc' ;

    // Get document type
    $DocMark = 'book' ;
    $query = sprintf ('SELECT * FROM DocType WHERE DocType.Mark="%s" AND DocType.Active=1', $DocMark) ;
    $result = dbQuery ($query) ;
    $DocType = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if (!$DocType) return sprintf ('%s(%d), document type not found, mark "%s"', __FILE__, __LINE__, $DocMark) ;

    switch ($Navigation['Parameters']) {
	case 'newdoc':
	case 'newversion':
	    // Check if document upload was succesfull
	    switch ($_FILES['Doc']['error']) {
		case 0: break ;
		case 4: return 'Please specify file' ;
		default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	    }
	    if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		return 'document upload failed, file not uploaded' ;
	    if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		return 'invalid document size' ;
	    if ($_FILES['Doc']['type'] != 'message/rfc822')
		return sprintf ('not a Mime file, id %d, type %s', $Id, $_FILES['Doc']['type']) ;
    }

    // Document record
    $docfields = array (
	'Name'		=> array ('mandatory' => true),
	'Description'	=> array (),
	'Comment'	=> array (),
	'KeyWords'	=> array ()
    ) ;

    // Version record
    $verfields = array (
	'Comment'	=> array (),
	'Size'		=> array ('type' => 'set',	'value' => $_FILES['Doc']['size']),	
	'Filename'	=> array ('type' => 'set',	'value' => $_FILES['Doc']['name']),
	'Type'		=> array ('type' => 'set',	'value' => $_FILES['Doc']['type'])	

    ) ;

    switch ($Navigation['Parameters']) {
        case 'newdoc':
	    // The doc record must be created
	    // Validate the versions fields
	    if (($res = saveFields (NULL, -1, $verfields)) !== 0) return $res ;

	    // Save document record
	    $docfields['ParentId'] = array ('type' => 'set', 'value' => $Record['Id']) ;
	    $docfields['DocTypeId'] = array ('type' => 'set', 'value' => $DocType['Id']) ;
	    if (($res = saveFields ('Doc', -1, $docfields)) !== 0) return $res ;

	    // Save version record
	    $verfields['DocId'] = array ('type' => 'set', 'value' => dbInsertedId ()) ;
	    $verfields['Version'] = array ('type' => 'set', 'value' => 1) ;
	    if (($res = saveFields ('DocVersion', -1, $verfields)) !== 0) return $res ;
	    break ;

	case 'newversion':
	    // Save new version

	    // Find highest version number
	    $query = sprintf ('SELECT MAX(DocVersion.Version) AS Version FROM DocVersion WHERE DocVersion.DocId=%d AND DocVersion.Active=1', $Record['DocId']) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if (!$row) return sprintf ('%s(%d), no versions found, Doc.Id %d', __FILE__, __LINE__, $Record['DocId']) ;
	    $version = ((int)$row['Version']) + 1 ;

	    $verfields['DocId'] = array ('type' => 'set', 'value' => $Record['DocId']) ;
	    $verfields['Version'] = array ('type' => 'set', 'value' => $version) ;
	    if (($res = saveFields ('DocVersion', -1, $verfields)) !== 0) return $res ;
	    break ;

	default :
	    // Id is for the version record
	    // Get the DocId from the record
	    return saveFields ('Doc', $Record['DocId'], $docfields) ;
    }

    switch ($Navigation['Parameters']) {
        case 'newdoc':
	case 'newversion':
	    //Archive document
	    $Id = dbInsertedId () ;
	    $file = fileName ('doc', $Id, true) ;
	    if (!move_uploaded_file ($_FILES['Doc']['tmp_name'], $file)) return sprintf ('%s(%d) failed to archive document', __FILE__, __LINE__) ;
	    chmod ($file, 0640) ;

	    // Generate Tasks for approvals
	    $query = sprintf ('SELECT TaskType.Id, TaskType.DisplayOrder From TaskType Where TaskType.Active=1 AND TaskType.Category="book"') ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch($result)) {
		$task = array (
		    'TaskTypeId' 	=> $row['Id'],
		    'RefId'		=> $Id,
		    'EndDate'		=> dbDateOnlyEncode(time()+$row['DisplayOrder']*60*60*24) 
		) ;
		tableWrite ('Task', $task) ;	
	    }
	    dbQueryFree ($result) ;
 	    
	    break ;
    }

    return 0 ;   
?>
