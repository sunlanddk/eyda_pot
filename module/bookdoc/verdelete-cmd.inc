<?php

    // Check if it is the only version for this document
    $query = sprintf ("SELECT Id FROM DocVersion WHERE Active=1 AND DocId=%d", $Record["DocId"]) ;
    $result = dbQuery ($query) ;
    $n = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($n <= 1) return "this version can't be deleted because it is the only version of this document" ;   
   
    // Check if document has been approved
    if ($Record['Released']) return "this version can't be delete because it has been released" ;

    // Delete
    require_once "lib/table.inc" ;
    tableDelete ("DocVersion", $Id) ;

    return 0
?>
