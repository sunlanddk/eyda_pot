<?php

    require_once 'PEAR/Mail/mimeDecode.php' ;
    require_once 'lib/file.inc' ;
    
    // Check for type of file
    if ($Record['Type'] != 'message/rfc822') return sprintf ('%s(%d) not a Mime file, id %d, type %s', __FILE__, __LINE__, $Id, $Record['Type']) ;
    
    // Chack if part number supplied
    if (!isset($_GET['part'])) return sprintf ('%s(%d) no part supplied, id %d', __FILE__, __LINE__, $Id) ;
    $partno = (int)$_GET['part'] ;
    
    // Make filename
    $file = fileName ('doc', $Id) ;
    $mime = file_get_contents($file) ;

    // Decode mime structure
    $params['include_bodies'] = true ;
    $params['decode_bodies']  = true ;
    $params['decode_headers'] = true ;
    $decoder = new Mail_mimeDecode($mime) ;
    $structure = $decoder->decode($params) ;    

    // Validate that part exists
    if (!isset($structure->parts[$partno])) return sprintf ('%s(%d) part does not exist, id %d, part %d', __FILE__, __LINE__, $Id, $partno) ;

    // Debug
    if (false) {
	printf ("id: %d<br>\n", $Id) ;
	printf ("partno: %d<br>\n", $partno) ;
	printf ("file: %s (%s)<br>\n", htmlentities($structure->parts[$partno]->headers['content-location']), htmlentities($file)) ;
	printf ("size: %d<br>\n", strlen($structure->parts[$partno]->body)) ;
	printf ("content-type: %s<br>\n", htmlentities($structure->parts[$partno]->headers['content-type'])) ;
	return 0 ;
    }

    // Download Mime part
    header ('Content-Type: '.$structure->parts[$partno]->headers['content-type']) ;
    header ('Last-Modified: '.date("D, j M Y G:i:s T", dbDateDecode($Record['CreateDate'])));
    print ($structure->parts[$partno]->body) ;

    return 0 ;
?>
