<?php

define ('DEBUG', 0) ;

if (DEBUG) require_once 'lib/log.inc' ;
    
// Compute date intervals
$FromDate = $Parameter['Week']['Value'] ;
$ToDate = $FromDate + 7*24*60*60 ;
if (DEBUG) printf ("From %s to %s<br><br>\n", date('Y-m-d', $FromDate), date('Y-m-d', $ToDate)) ;

// Get WorkGroups
$query = sprintf ("SELECT * FROM WorkGroup WHERE Active=1 AND Sewing=1 ORDER BY Number") ;
$result = dbQuery ($query) ;
$WorkGroup = array () ;
while ($row = dbFetch($result)) {
  $id = (int)$row['Id'] ;
  $WorkGroup[$id] = $row ;
}
dbQueryFree ($result) ;
if (count($WorkGroup) == 0) return sprintf ("No WorkGroups defined") ;

// Add additional WorkGroup for work outsourced
$WorkGroup[0] = array ('Number' => 'Outsourced') ;

// Get Capacities
$query = sprintf ("SELECT WorkGroupId AS Id, Count(Id) AS Days, SUM(Operators*ProductionMinutes) AS OperatorMinutes FROM Capacity WHERE ActualDate>='%s' AND ActualDate<'%s' AND Active=1 GROUP BY WorkGroupId", date('Y-m-d', (time() > $FromDate) ? time() : $FromDate), date('Y-m-d',$ToDate)) ;
$result = dbQuery ($query) ;
while ($row = dbFetch($result)) {
  $id = (int)$row['Id'] ;
  if (!isset($WorkGroup[$id])) continue ;
  $WorkGroup[$id]['OperatorMinutes'] = (int)($row['OperatorMinutes'] * $WorkGroup[$id]['Efficiency'] / 100) ;
  $WorkGroup[$id]['Days'] = (int)$row['Days'] ;
}
dbQueryFree ($result) ;
if (DEBUG) logPrintVar ($WorkGroup, 'WorkGroup') ;

// Get Productions
$clause = sprintf ("Production.Done=0 AND Production.ProductionStartDate < '%s' 
					AND Production.Packed=0 AND Production.Active=1" 
					, date('Y-m-d',$ToDate)) ;
if (time() < $FromDate) $clause .= sprintf(" AND Production.ProductionEndDate>='%s'", date('Y-m-d', $FromDate)) ;
if ($Parameter['Requests']['Value']) { 
  $clause .= ' AND (Production.Allocated=1 OR Production.Fabrics=1)' ;
} else {
  $clause .= ' AND Production.Allocated=1' ;
}

$clause .= sprintf (' AND Production.ProductionLocationId=%d', $Parameter['Location']['Value']) ;
$query = sprintf ("SELECT Production.* , Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Style.Version AS StyleVersion, CONCAT(Company.Name,' (',Company.Number,')') AS CompanyName, 
IF(Production.SubDeliveryDate < '%s''','Yes','No') AS Late FROM Production LEFT JOIN Style ON Style.Id=Production.StyleId LEFT JOIN Article ON Article.Id=Style.ArticleId LEFT JOIN `Case` ON Case.Id=Production.CaseId LEFT JOIN Company ON Company.Id=Case.CompanyId WHERE %s ORDER BY Production.CaseId",date('Y-m-d', $FromDate), $clause) ;
if (DEBUG) printf ("Production query: %s<br>\n", $query) ;
$result = dbQuery ($query) ;
$Production = array () ;

while ($row = dbFetch($result)) {

  $id = (int)$row['Id'] ;
  $row['Work'] = array() ;
  $Production[$id] = $row ;
}

dbQueryFree ($result) ;
if (count($Production) == 0) return sprintf ("No ProductionOrders") ;



// Get real bundle quantity for Productions cut
foreach ($Production as $id => $row) {
  if (!$row['Cut']) continue ;
  $Production[$id]['Quantity'] = 0 ;
	$query = sprintf ("SELECT Bundle.ProductionId AS Id, SUM(Bundle.Quantity) AS Quantity 
					FROM Bundle 
					WHERE Bundle.ProductionId = %d AND Bundle.Active=1 GROUP BY Bundle.ProductionId", $id) ;
	$result = dbQuery ($query) ;
  while ($row = dbFetch($result)) {
    $id = (int)$row['Id'] ;
    $Production[$id]['Quantity'] = (int)$row['Quantity'] ;
  }
  dbQueryFree ($result) ;	
}


// Get Work Done

foreach ($Production as $id => $row) {
$query = sprintf ("SELECT BundleWork.ProductionId AS Id, MachineGroup.WorkGroupId, SUM(BundleWork.Quantity*currentstyleoperation.ProductionMinutes) AS ProductionMinutes FROM BundleWork, currentstyleoperation, MachineGroup WHERE BundleWork.ProductionId = %d AND BundleWork.Active=1 AND currentstyleoperation.Id=BundleWork.StyleOperationId AND MachineGroup.Id=currentstyleoperation.MachineGroupId GROUP BY BundleWork.ProductionId, MachineGroup.WorkGroupId", $id) ;
$result = dbQuery ($query) ;
while ($row = dbFetch($result)) {
  $Production[(int)$row['Id']]['Work'][(int)$row['WorkGroupId']]['ProductionMinutes'] = $row['ProductionMinutes'] ;
}
dbQueryFree ($result) ;
}
//getoutsourced work done
foreach ($Production as $id=>$prod) {
  
  $query = sprintf("select sum(so.ProductionMinutes * b.Quantity) as DoneTime
from Production p 
inner join currentstyleoperation so on p.StyleId = so.StyleId
inner join Bundle b on b.ProductionId = p.Id and b.Active=1
inner join OutSourcing os on os.BundleId = b.Id
	and so.No <= os.OperationToNo and so.No >= os.OperationFromNo
where p.Id = %d and os.InDate is not null ", $id );

  $tab = dbRead($query);
  $Production[$id]['Work'][0]['ProductionMinutes'] += $tab[0]['DoneTime'];
}


//get work to be done pr group
foreach ($Production as $id=>$prod) {
 	$tab = dbRead(sprintf("SELECT COUNT(*) as Cnt FROM ProductionQuantity WHERE ProductionId = %d", $id));
	
	if ($tab[0]['Cnt'] == 0) {
	  // 1st case - No size breakdown - full quantity of order
	$query = sprintf("select 
	mg.WorkGroupId,
	SUM(IF(osp.Id is null, p.Quantity *so.ProductionMinutes, 0)) AS intime,
	SUM(IF(osp.Id is not null, p.Quantity*so.ProductionMinutes , 0)) AS outtime
from Production p
	
	inner join currentstyleoperation so ON so.StyleId = p.StyleId
	inner join MachineGroup mg ON so.MachineGroupId = mg.Id
	left join OutSourcingPlan osp ON
		osp.ProductionId = p.Id AND
		osp.OperationFromNo <= so.No AND osp.OperationToNo >= so.No
where p.Id = %d
GROUP BY mg.WorkGroupId",$id);
  }elseif($prod['Cut']) {
    $query = sprintf("select
	mg.WorkGroupId,
	SUM(IF(os.Id is null, b.Quantity *so.ProductionMinutes, 0)) AS intime,
	SUM(IF(os.Id is not null, b.Quantity*so.ProductionMinutes , 0)) AS outtime
from Production p
	
	inner join currentstyleoperation so ON so.StyleId = p.StyleId
	inner join MachineGroup mg ON so.MachineGroupId = mg.Id
	inner join Bundle b ON b.ProductionId=p.Id AND b.Active = 1
	left join OutSourcing os ON os.BundleId = b.Id
		 and os.OperationFromNo <= so.No and os.OperationToNo >= so.No
where p.Id = %d
group by mg.WorkGroupId", $id);
  } else {
    $query = sprintf("select 
	mg.WorkGroupId,
	SUM(IF(osp.Id is null, pq.Quantity *so.ProductionMinutes, 0)) AS intime,
	SUM(IF(osp.Id is not null, pq.Quantity*so.ProductionMinutes , 0)) AS outtime
from Production p
	
	inner join currentstyleoperation so ON so.StyleId = p.StyleId
	inner join MachineGroup mg ON so.MachineGroupId = mg.Id
	inner join ProductionQuantity pq ON pq.ProductionId = p.Id
	left join OutSourcingPlan osp ON
		osp.ProductionId = p.Id AND
		(osp.ArticleColorId = pq.ArticleColorId OR osp.ArticleColorId IS NULL) AND
		(osp.ArticleSizeId = pq.ArticleSizeId OR osp.ArticleSizeId IS NULL) AND
		osp.OperationFromNo <= so.No AND osp.OperationToNo >= so.No
where p.Id = %d
GROUP BY mg.WorkGroupId
", $id);
  }
//print $query . "\n";

  $res = dbQuery($query);
 
  while($row = dbFetch($res)) {
    $Production[$id]['TotalWork'][(int)$row['WorkGroupId']] = $row['intime'];
    $Production[$id]['TotalWork'][0] += $row['outtime'];
  }
  dbQueryFree($res);
}
//print_r($Production); 
//return 0;

//get production factors

$query = sprintf("select p.Id, count(*) AS TotalDays, SUM(if(Actualdate < '%s' and Actualdate >='%s',1,0)) as WeekDays 
from Production p inner join (SELECT DISTINCT ActualDate FROM Capacity) c on c.ActualDate >= p.ProductionStartDate
AND c.ActualDate <= p.ProductionEndDate
where p.Done=0 and p.Packed=0 and p.Active=1 and c.ActualDate >= now()
group by p.Id", date('Y-m-d',$ToDate), date('Y-m-d',$FromDate));

$res=dbQuery($query);
$ProductionFactors = array();

while($row = dbFetch($res)) {
  if(!isset($Production[$row['Id']])) continue;
  $ProductionFactors[$row['Id']] = $row;
}
dbQueryFree($res);

//print_r($ProductionFactors); return 0;

require_once 'Spreadsheet/Excel/Writer.php';

// Initialization
define ('HEADLINE', 4) ;
define ('HEADLINECOLOR', 22) ;

// Create workbook
$book = new Spreadsheet_Excel_Writer () ;

// Create a worksheet
$sheet =& $book->addWorksheet ('Minute Plan') ;
$sheet->setPortrait () ;
$sheet->setMargins (0.2) ;
$sheet->hideGridlines () ;
$sheet->setHeader ('', 0) ;
$sheet->setFooter ('', 0) ;
//$sheet->centerHorizontally () ;
$sheet->centerVertically () ;
$sheet->fitToPages (5, 1) ;
$sheet->freezePanes (array(6,5)) ;

// Header
$format =& $book->addFormat () ;
$format->setSize (22) ;
$format->setBold () ;
$sheet->writeString (0, 0, 'Minute Plan', $format) ;
$sheet->writeString (1, 0, sprintf (" %s: %s, Week %d, from %s to %s%s", $Parameter['Location']['Description'], reportParameterString ($Parameter['Location']), date('W',$FromDate), date ('Y-m-d', $FromDate), date ('Y-m-d', $ToDate), ($Parameter['Requests']['Value']) ? ', including requested ProductionOrders' : '')) ;
$sheet->writeString (2, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;

// Table Headline formats
$format =& $book->addFormat () ;
$format->setFgColor (HEADLINECOLOR) ;

$formatBorder =& $book->addFormat () ;
$formatBorder->setFgColor (HEADLINECOLOR) ;
$formatBorder->setLeft (1) ;

$formatRight =& $book->addFormat () ;
$formatRight->setFgColor (HEADLINECOLOR) ;
$formatRight->setAlign ('right') ;

$formatRightBorder =& $book->addFormat () ;
$formatRightBorder->setFgColor (HEADLINECOLOR) ;
$formatRightBorder->setAlign ('right') ;
$formatRightBorder->setLeft (1) ;

$formatDec3 =& $book->addFormat () ;
$formatDec3->setFgColor (HEADLINECOLOR) ;
$formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

// Table Headline 1
$collumn = 0 ;
$sheet->setColumn($collumn, $collumn, 30) ;
$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
$sheet->setColumn($collumn, $collumn, 10) ;
$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
$sheet->setColumn($collumn, $collumn, 13) ;
$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
$sheet->setColumn($collumn, $collumn, 8) ;
$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
$sheet->setColumn($collumn, $collumn, 5) ;
$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
foreach ($WorkGroup as $row) {
  $sheet->setColumn($collumn, $collumn, 8) ;
  $sheet->writeString (HEADLINE, $collumn++, $row['Number'], $formatBorder) ;
  $sheet->setColumn($collumn, $collumn, 8) ;
  $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
  $sheet->setColumn($collumn, $collumn, 8) ;
  $sheet->writeNumber (HEADLINE, $collumn++, $row['OperatorMinutes'], $formatDec3) ;
}

// Table Headline 2
$collumn = 0 ;
$sheet->writeString (HEADLINE+1, $collumn++, 'Customer', $format) ;
$sheet->writeString (HEADLINE+1, $collumn++, 'Production', $format) ;
$sheet->writeString (HEADLINE+1, $collumn++, 'Style', $format) ;
$sheet->writeString (HEADLINE+1, $collumn++, 'Quantity', $formatRight) ;
$sheet->writeString (HEADLINE+1, $collumn++, 'Late', $formatRight) ;
for ($n = 0 ; $n < count($WorkGroup) ; $n++) {
  //    foreach ($WorkGroup as $row) {
  $sheet->writeString (HEADLINE+1, $collumn++, 'Time', $formatRightBorder) ;
  $sheet->writeString (HEADLINE+1, $collumn++, 'Total', $formatRight) ;
  $sheet->writeString (HEADLINE+1, $collumn++, 'Rest', $formatRight) ;
}

// Table Data formats
$formatDec3 =& $book->addFormat () ;
$formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

$formatDec4 =& $book->addFormat () ;
$formatDec4->setNumFormat (4) ;		// Format: decimal #,##0.00

$formatDec4Border =& $book->addFormat () ;
$formatDec4Border->setNumFormat (4) ;	// Format: decimal #,##0.00
$formatDec4Border->setLeft (1) ;

$formatBorder =& $book->addFormat () ;
$formatBorder->setLeft (1) ;

$formatRight =& $book->addFormat () ;
$formatRight->setAlign ('right') ;

// Table Data
$line = HEADLINE + 2 ;
foreach ($Production  as $row) {
  $collumn = 0 ;
  $pid = $row['Id']; 
  if($ProductionFactors[$pid]['TotalDays']) {
    $productionfactor = (float)($ProductionFactors[$pid]['WeekDays']/$ProductionFactors[$pid]['TotalDays']); 
  } else { 
    $productionfactor = 1;
  } 
  $sheet->writeString ($line, $collumn++, $row['CompanyName']) ;
  $sheet->writeString ($line, $collumn++, $row['CaseId'].'/'.$row['Number']) ;
  $sheet->writeString ($line, $collumn++, $row['ArticleNumber'].'-'.$row['StyleVersion']) ;
  $sheet->writeNumber ($line, $collumn++, $row['Quantity'], $formatDec3) ;
  $sheet->writeString ($line, $collumn++, $row['Late'], $formatRight) ;

  foreach ($WorkGroup as $WorkGroupId => $w) {
    $ProductionMinutes = $row['TotalWork'][$WorkGroupId] / $row['Quantity'] ;
    if ($ProductionMinutes > 0) {
      $sheet->writeNumber ($line, $collumn++, $ProductionMinutes, $formatDec4Border) ;

      $v = (int)($row['TotalWork'][$WorkGroupId]) *$productionfactor ;
      $WorkGroup[$WorkGroupId]['TotalTime'] += $v ;
      $sheet->writeNumber ($line, $collumn++, $v, $formatDec3) ;

      $v -= (int)($row['Work'][$WorkGroupId]['ProductionMinutes']) * $productionfactor ;
      if ($v < 0) $v = 0 ;
      $WorkGroup[$WorkGroupId]['TotalRest'] += $v ;
      $sheet->writeNumber ($line, $collumn++, $v, $formatDec3) ;
    } else {
      $sheet->writeBlank ($line, $collumn++, $formatBorder) ;
      $collumn++ ;
      $collumn++ ;
    }
  }
  $line++ ;
}

// Total formats
$format =& $book->addFormat () ;
$format->setTop (1) ;

$formatBorder =& $book->addFormat () ;
$formatBorder->setTop (1) ;
$formatBorder->setLeft (1) ;

$formatDec3 =& $book->addFormat () ;
$formatDec3->setTop (1) ;
$formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

// Total
$collumn = 0 ;
$sheet->writeString ($line, $collumn++, 'Total', $format) ;
$sheet->writeBlank ($line, $collumn++, $format) ;
$sheet->writeBlank ($line, $collumn++, $format) ;
$sheet->writeBlank ($line, $collumn++, $format) ;
$sheet->writeBlank ($line, $collumn++, $format) ;
foreach ($WorkGroup as $row) {
  $sheet->writeBlank ($line, $collumn++, $formatBorder) ;
  $sheet->writeNumber ($line, $collumn, $row['TotalTime'], $formatDec3) ;
  $collumn++ ;
  $sheet->writeNumber ($line, $collumn, $row['TotalRest'], $formatDec3) ;
  $collumn++ ;
}

if (headers_sent()) {
  printf ("<br>Header allready send, stopping<br>\n") ;
  return 0 ;	
}

// Let's send the file
$book->send ('plan.xls') ;
$book->close();
    
return 0 ;
?>
