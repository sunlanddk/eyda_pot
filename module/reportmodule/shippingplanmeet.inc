<?php
// proWide
// Hvass Innovation ApS
// Niels Kristensen
//
// File:    ./module/prodstat/sewing-xls.inc
// Version: 1.0

    // Get Productions
    $clause = sprintf ('Production.Done=0 AND Production.Packed=0  AND Production.Active=1 AND Production.ProductionLocationId=%d', $Parameter['Location']['Value']) ;
    if (!$Parameter['Defined']['Value']) {
    	if ($Parameter['Requests']['Value']) { 
			$clause .= ' AND Production.Fabrics=1' ;
    	} else {
			$clause .= ' AND Production.Allocated=1' ;
    	}
    }
    $query = sprintf ("SELECT Production.*, Season.Name as SeasonName, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Style.Version AS StyleVersion, CONCAT(Company.Name,' (',Company.Number,')') AS CompanyName 
FROM Production 
LEFT JOIN Season ON Season.Id=Production.SeasonId 
LEFT JOIN Style ON Style.Id=Production.StyleId 
LEFT JOIN Article ON Article.Id=Style.ArticleId 
LEFT JOIN `Case` ON Case.Id=Production.CaseId 
LEFT JOIN Company ON Company.Id=Case.CompanyId 
WHERE %s ORDER BY Production.SubDeliveryDate", $clause) ;
    $result = dbQuery ($query) ;
    $Production = array () ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	$Production[$id] = $row ;
    }
    dbQueryFree ($result) ;

    // Get Quantity from Bundles
    $query = sprintf ("SELECT Production.Id, SUM(Bundle.Quantity) AS BundleQuantity FROM Production LEFT JOIN Bundle ON Bundle.ProductionId=Production.Id AND Bundle.Active=1 WHERE %s GROUP BY Production.Id", $clause) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	if (!isset($Production[$id])) continue ; 
	$Production[$id]['BundleQuantity'] = (int)$row['BundleQuantity'] ;
    }
    dbQueryFree ($result) ;

    // Get ProductionMinutes
    $query = sprintf ("SELECT Production.Id, 
				MAX(Production.ProductionMinutes) as ProdProductionMinutesTotal,
				SUM(currentstyleoperation_open.ProductionMinutes) AS ProductionMinutesTotal,
    				SUM(currentstyleoperation_open.ProductionMinutes*WorkGroup.Sewing) AS ProductionMinutesSewing,
    				SUM(IF(Production.SourceLocationId > 0 AND Production.SourceFromNo <= currentstyleoperation_open.No AND Production.SourceToNo >= currentstyleoperation_open.No AND WorkGroup.Sewing = 1, currentstyleoperation_open.ProductionMinutes, 0)) AS ProductionMinutesSourced
    			    FROM Production 
			    LEFT JOIN currentstyleoperation_open ON currentstyleoperation_open.StyleId=Production.StyleId AND currentstyleoperation_open.Active=1 
			    LEFT JOIN MachineGroup ON MachineGroup.Id=currentstyleoperation_open.MachineGroupId 
			    LEFT JOIN WorkGroup ON WorkGroup.Id=MachineGroup.WorkGroupId 
                      WHERE %s GROUP BY Production.Id", $clause) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	if (!isset($Production[$id])) continue ; 
	$Production[$id]['ProductionMinutesTotal'] = (float)$row['ProductionMinutesTotal']>0?(float)$row['ProductionMinutesTotal']:(float)$row['ProdProductionMinutesTotal'] ;
	$Production[$id]['ProductionMinutesSewing'] = (float)$row['ProductionMinutesSewing'] ;
	$Production[$id]['ProductionMinutesSourced'] = (float)$row['ProductionMinutesSourced'] ;
   }
    dbQueryFree ($result) ;
   //Get planned outsourced Minutes
foreach( $Production as $id =>$prod) {
  if(!$prod['Cut']) {
    $query = sprintf("SELECT SUM(o.ProductionMinutes * q.Quantity *w.Sewing) AS OutMinutes
from Production p INNER JOIN  currentstyleoperation_open o ON p.StyleId=o.StyleID AND o.Active=1
INNER JOIN OutSourcingPlan s ON s.ProductionId = p.Id 
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
INNER JOIN ProductionQuantity q ON q.ProductionId=p.Id
	AND (q.ArticleColorId = s.ArticleColorId OR s.ArticleColorId is null)
	AND (q.ArticleSizeId = s.ArticleSizeId OR s.ArticleSizeId is null)
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d", $id);
  } else {
    $query = sprintf("
SELECT coalesce(SUM(o.ProductionMinutes * b.Quantity * w.Sewing), 0) AS OutMinutes
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id AND b.Active=1
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN currentstyleoperation_open o ON o.StyleId = p.StyleId AND o.Active=1
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d", $id);
  }
  $tab = dbRead($query);
  
  $Production[$id]['ProductionMinutesSourced'] = $tab[0]['OutMinutes'];
}

//Get insourced Minutes
foreach( $Production as $id =>$prod) {
 
    $query = sprintf("
SELECT coalesce(SUM(o.ProductionMinutes * b.Quantity * w.Sewing), 0) AS OutMinutes
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id AND b.Active=1
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN currentstyleoperation_open o ON o.StyleId = p.StyleId AND o.Active=1
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE s.InDate IS NOT NULL AND 
p.Id=%d;
", $id);
  
  $tab = dbRead($query);
  
  $Production[$id]['DoneOut'] = $tab[0]['OutMinutes'];
}

//get done minutes
foreach ($Production as $id=>$prod) {
    $query = sprintf("
SELECT coalesce(SUM(o.ProductionMinutes * bw.Quantity * w.Sewing), 0) AS Minutes
FROM BundleWork bw INNER JOIN Bundle b ON bw.BundleId=b.Id AND b.Active=1
INNER JOIN Production p ON bw.ProductionId = p.Id
INNER JOIN currentstyleoperation_open o ON o.Active=1
	AND bw.StyleOperationId = o.Id
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d;
", $id);
  
  $tab = dbRead($query);
  
  $Production[$id]['DoneIn'] = $tab[0]['Minutes'];
}


    // logPrintVar ($Production, 'Production') ;

    // Any samples
    if (count($Production) == 0) return "No ProductionOrders" ;

    require_once 'Spreadsheet/Excel/Writer.php';

    // Initialization
    define ('HEADLINE', 4) ;
    define ('HEADLINECOLOR', 22) ;

    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;
    $book->send ('plan.xls') ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ('Shipping Plan for meeting') ;
    $sheet->setLandscape () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    $sheet->fitToPages (1, 100) ;
    $sheet->freezePanes (array(4,4)) ;
    
    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString (0, 0, 'Shipping plan for meeting', $format) ;
    //$sheet->writeString (1, 0, sprintf (" %s: %s", $Parameter['Location']['Description'], reportParameterString ($Parameter['Location']))) ;
    $sheet->writeString (1, 0, sprintf (" Location %s, %s requested ProductionOrders, %s defined ProductionOrders", reportParameterString ($Parameter['Location']), ($Parameter['Defined']['Value']||$Parameter['Requests']['Value']) ? 'Including' : 'Excluding',($Parameter['Defined']['Value']) ? 'Including' : 'Excluding')) ;
    $sheet->writeString (2, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;

    // Table Headline
    $collumn = 0 ;
    $format =& $book->addFormat () ;
    $format->setFgColor (HEADLINECOLOR) ;
    $format->setBold () ;

    $formatright =& $book->addFormat () ;
    $formatright->setFgColor (HEADLINECOLOR) ;
    $formatright->setAlign ('right') ;
    $formatrighthead =& $book->addFormat () ;
    $formatrighthead->setFgColor (HEADLINECOLOR) ;
    $formatrighthead->setAlign ('right') ;
    $formatrighthead->setAlign ('right') ;
    $formatrighthead->setBold () ;

    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Note', $format) ;
    $sheet->setColumn($collumn, $collumn, 8) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Production', $format) ;
    $sheet->setColumn($collumn, $collumn, 11) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Style', $format) ;
    $sheet->setColumn($collumn, $collumn, 6) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Qty', $formatrighthead) ;
    $sheet->setColumn($collumn, $collumn, 13) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Customer', $format) ;
    $sheet->setColumn($collumn, $collumn, 13) ;
    $sheet->writeString (HEADLINE, $collumn++, 'SeasonName', $format) ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Description', $format) ;
    $sheet->setColumn($collumn, $collumn, 8) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Material', $formatrighthead) ;
    $sheet->setColumn($collumn, $collumn, 8) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Start', $formatrighthead) ;
//    $sheet->setColumn($collumn, $collumn, 10) ;
//    $sheet->writeString (HEADLINE, $collumn++, 'End', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 8) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Shipment', $formatrighthead) ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Missing', $format) ;
    $sheet->setColumn($collumn, $collumn, 7) ;
 //   $sheet->writeString (HEADLINE, $collumn++, 'Sew', $formatright) ;
 //   $sheet->setColumn($collumn, $collumn, 10) ;
 //   $sheet->writeString (HEADLINE, $collumn++, 'Total Sew', $formatright) ;
 //   $sheet->setColumn($collumn, $collumn, 10) ;
 //   $sheet->writeString (HEADLINE, $collumn++, 'Out', $formatright) ;
 //   $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Tot. Out', $formatrighthead) ;
    $sheet->setColumn($collumn, $collumn, 7) ;
 //   $sheet->writeString (HEADLINE, $collumn++, 'All', $formatright) ;
 //   $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Tot. All', $formatrighthead) ;

    $sheet->setColumn($collumn, $collumn, 7) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Rem. LT', $formatrighthead) ;
//    $sheet->setColumn($collumn, $collumn, 10) ;
//    $sheet->writeString (HEADLINE, $collumn++, 'Remain Out', $formatright) ;
//    $sheet->setColumn($collumn, $collumn, 10) ;
//    $sheet->writeString (HEADLINE, $collumn++, 'Remain All', $formatright) ;

    // Table Data
    $line = HEADLINE + 1 ;
    $formatdec3 =& $book->addFormat () ;
    $formatdec3->setNumFormat (3) ;		// Format: decimal #,##0
    $formatdec3->setTop (1) ;
    $formatdec3->setleft (1) ;
    $formatdec3->setright (1) ;
    $formatdec4 =& $book->addFormat () ;
    $formatdec4->setNumFormat (4) ;		// Format: decimal #,##0.00
    $formatdec4->setTop (1) ;
    $formatdec4->setleft (1) ;
    $formatdec4->setright (1) ;

    $formatright =& $book->addFormat () ;
    $formatright->setAlign ('right') ;
    $formatright->setTop (1) ;
    $formatright->setleft (1) ;
    $formatright->setright (1) ;

    $formattop =& $book->addFormat () ;
    $formattop->setAlign ('left') ;
    $formattop->setAlign ('top') ;
    $formattop->setTextWrap () ;
    $formattop->setTop (1) ;
    $formattop->setleft (1) ;
    $formattop->setright (1) ;

    $formatdef =& $book->addFormat () ;
    $formatdef->setAlign ('left') ;
    $formatdef->setTop (1) ;
    $formatdef->setleft (1) ;
    $formatdef->setright (1) ;


    $formatTotal =& $book->addFormat () ;
    $formatTotal->setTop (1) ;
    $formatTotal->setBold (1) ;
    $formatTotalDec3 =& $book->addFormat () ;
    $formatTotalDec3->setTop (1) ;
    $formatTotalDec3->setBold (1) ;
    $formatTotalDec3->setNumFormat (3) ;	// Format: decimal #,##0

    $row = reset ($Production) ;
    $lastweek = (int)date('W', dbDateDecode($row['SubDeliveryDate'])) ;    
    $total = array() ;
    while ($row) {
	$season = dbRead(sprintf('select s.name as Name, s.id as Id from (orderline ol, `order` o)
									Left join season s on s.id=o.seasonid
									where ol.productionid=%d and ol.orderid=o.id', $row['Id']));
	$collumn = 0 ;
	$Quantity = (int)(($row['Cut']) ? $row['BundleQuantity'] : $row['Quantity']) ;
	$sheet->writeString ($line, $collumn++, $row['PlanText'], $formattop) ;
	$sheet->writeString ($line, $collumn++, $row['CaseId'].'/'.$row['Number'], $formatdef) ;
	$sheet->writeString ($line, $collumn++, $row['ArticleNumber'].'-'.$row['StyleVersion'], $formatdef) ;
	$sheet->writeNumber ($line, $collumn++, $Quantity, $formatdec3) ;
	$sheet->writeString ($line, $collumn++, $row['CompanyName'], $formatdef) ;
	$sheet->writeString ($line, $collumn++, $season[0]['Name'], $formatdef) ;
	$sheet->writeString ($line, $collumn++, $row['ArticleDescription'], $formatdef) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['MaterialDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionStartDate'])), $formatright) ;
//	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionEndDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['SubDeliveryDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, $row['MissingText'], $formattop) ;

//	$sheet->writeNumber ($line, $collumn++, $row['ProductionMinutesSewing'], $formatdec4) ;
	$v = (int)($row['ProductionMinutesSewing'] * $Quantity - $row['ProductionMinutesSourced']) ;
	$total['TotalSew'] += $v ;
//	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;

//	$sheet->writeNumber ($line, $collumn++, $row['ProductionMinutesSourced'], $formatdec4) ;
	$v = (int)($row['ProductionMinutesSourced']) ;
	$total['TotalSourced'] += $v ;
	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;


//	$sheet->writeNumber ($line, $collumn++, $row['ProductionMinutesTotal'], $formatdec4) ;
	$v = (int)($row['ProductionMinutesTotal'] * $Quantity) ;
	$total['TotalAll'] += $v ;
	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;


	$v = (int)($row['ProductionMinutesSewing'] * $Quantity - $row['ProductionMinutesSourced'] -$row['DoneIn']) ;
	$total['RemainSew'] += $v ;
	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;


	$v = (int)($row['ProductionMinutesSourced'] - $row['DoneOut']) ;
	$total['RemainSourced'] += $v ;
//	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;



	$v = (int)($row['ProductionMinutesTotal'] * $Quantity -$row['DoneIn'] - $row['DoneOut']) ;
	$total['RemainAll'] += $v ;
//	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;

	$total['QuantityTotal'] += $Quantity ;

	$line++ ;

	$row = next ($Production) ;
	$thisweek = (int)date('W', dbDateDecode($row['SubDeliveryDate'])) ;    
	
	if (!$row or $thisweek != $lastweek) {
	    $collumn = 0 ;

	    $sheet->writeString ($line, $collumn++, sprintf ("Total, week %d", $lastweek), $formatTotal) ;
	    for ($n = 0 ; $n < 2 ; $n++) $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['QuantityTotal'], $formatTotalDec3) ;
	    for ($n = 3 ; $n < 9 ; $n++) $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
//	    $sheet->writeNumber ($line, $collumn++, $total['TotalSew'], $formatTotalDec3) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['TotalSourced'], $formatTotalDec3) ;
//	    $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['TotalAll'], $formatTotalDec3) ;
	    $sheet->writeNumber ($line, $collumn++, $total['RemainSew'], $formatTotalDec3) ;
//	    $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
//	    $sheet->writeNumber ($line, $collumn++, $total['RemainSourced'], $formatTotalDec3) ;
//	    $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
//	    $sheet->writeNumber ($line, $collumn++, $total['RemainAll'], $formatTotalDec3) ;

	    $line += 2 ;
	    $lastweek = $thisweek ;
	    $total = array() ;
	}
    }

    // Let's send the file
    $book->close();
    
    return 0 ;
?>
