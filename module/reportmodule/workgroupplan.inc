<?php

    // Any WorkGroup Selected ?
    $WorkGroupId = $Parameter['WorkGroup']['Value'] ;
    if ($WorkGroupId <= 0) return "No WorkGroup selected" ;
  
    // Get WorkGroup
    $query = sprintf ("SELECT * FROM WorkGroup WHERE Id=%d", $WorkGroupId) ;
    $result = dbQuery ($query) ;
    $WorkGroup = dbFetch($result) ;
    dbQueryFree ($result) ;
    if ($WorkGroup['Id'] != $WorkGroupId) return sprintf ("%s(%d) WorkGroup not found, Id %d", __FILE__, __LINE__, $WorkGroupId) ;
    
    // Get Productions
    $clause = sprintf ('Production.Done=0 AND Production.Packed=0 AND Production.Allocated=1 AND Production.Active=1 AND Production.ProductionLocationId=%d', $Parameter['Location']['Value']) ;
    $query = sprintf ("SELECT Production.*, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Style.Version AS StyleVersion, CONCAT(Company.Name,' (',Company.Number,')') AS CompanyName FROM Production LEFT JOIN Style ON Style.Id=Production.StyleId LEFT JOIN Article ON Article.Id=Style.ArticleId LEFT JOIN `Case` ON Case.Id=Production.CaseId LEFT JOIN Company ON Company.Id=Case.CompanyId WHERE %s ORDER BY Production.ProductionEndDate", $clause) ;
    $result = dbQuery ($query) ;
    $Production = array () ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	$Production[$id] = $row ;
    }
    dbQueryFree ($result) ;

    // Get Quantity from Bundles
    $query = sprintf ("SELECT Production.Id, SUM(Bundle.Quantity) AS BundleQuantity FROM Production LEFT JOIN Bundle ON Bundle.ProductionId=Production.Id AND Bundle.Active=1 WHERE %s GROUP BY Production.Id", $clause) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	if (!isset($Production[$id])) continue ; 
	$Production[$id]['BundleQuantity'] = (int)$row['BundleQuantity'] ;
    }
    dbQueryFree ($result) ;
    
    // Get Operations
    $query = sprintf ("SELECT Production.Id AS ProductionId, CONCAT(MachineGroup.Number,Operation.Number) AS OperationNumber, currentstyleoperation.Description AS StyleOperationDescription, currentstyleoperation.ProductionMinutes AS StyleOperationProductionMinutes FROM (Production, currentstyleoperation, MachineGroup) LEFT JOIN Operation ON Operation.Id=currentstyleoperation.OperationId WHERE %s AND MachineGroup.WorkGroupId=%d AND MachineGroup.Active=1 AND currentstyleoperation.StyleId=Production.StyleId AND currentstyleoperation.MachineGroupId=MachineGroup.Id AND currentstyleoperation.Active=1 AND (Production.SourceLocationId=0 or currentstyleoperation.No<Production.SourceFromNo or currentstyleoperation.No>Production.SourceToNo) ORDER BY Production.ProductionEndDate, currentstyleoperation.No", $clause, $WorkGroupId) ;
    $result = dbQuery ($query) ;
    $Operation = array () ;
    while ($row = dbFetch($result)) {
	$Operation[] = array_merge ($row, $Production[(int)$row['ProductionId']]) ;
    }
    dbQueryFree ($result) ;

    // logPrintVar ($Production, 'Production') ;
    // logPrintVar ($Operation, 'Operation') ;

    // Any samples
    if (count($Production) == 0) return "No Operations" ;

    require_once 'Spreadsheet/Excel/Writer.php';

    // Initialization
    define ('HEADLINE', 4) ;
    define ('HEADLINECOLOR', 22) ;

    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;
    $book->send ('plan.xls') ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ('WorkGroup Plan') ;
    $sheet->setLandscape () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    $sheet->fitToPages (1, 100) ;
    $sheet->freezePanes (array(5,4)) ;

    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString (0, 0, 'WorkGroup Plan', $format) ;
    $sheet->writeString (1, 0, sprintf (" %s: %s, Workgroup: %s - %s", $Parameter['Location']['Description'], reportParameterString ($Parameter['Location']), $WorkGroup['Number'], $WorkGroup['Description'])) ;
    $sheet->writeString (2, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;

    // Table Headline
    $collumn = 0 ;
    $format =& $book->addFormat () ;
    $format->setFgColor (HEADLINECOLOR) ;
    $formatright =& $book->addFormat () ;
    $formatright->setFgColor (HEADLINECOLOR) ;
    $formatright->setAlign ('right') ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Note', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Production', $format) ;
    $sheet->setColumn($collumn, $collumn, 13) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Style', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Quantity', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 30) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Customer', $format) ;
    $sheet->setColumn($collumn, $collumn, 30) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Description', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Material', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Start', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'End', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Shipment', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Missing', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Operation', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Description', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Time', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Total Time', $formatright) ;

    // Table Data
    $line = HEADLINE + 1 ;
 
    $formatdec3 =& $book->addFormat () ;
    $formatdec3->setNumFormat (3) ;		// Format: decimal #,##0
    $formatdec4 =& $book->addFormat () ;
    $formatdec4->setNumFormat (4) ;		// Format: decimal #,##0.00
    $formatright =& $book->addFormat () ;
    $formatright->setAlign ('right') ;

    $formatTotal =& $book->addFormat () ;
    $formatTotal->setTop (1) ;
    $formatTotalDec3 =& $book->addFormat () ;
    $formatTotalDec3->setTop (1) ;
    $formatTotalDec3->setNumFormat (3) ;	// Format: decimal #,##0

    $row = reset ($Operation) ;
    $lastweek = (int)date('W', dbDateDecode($row['ProductionEndDate'])) ;    
    $total = array() ;
    while ($row) {
	$collumn = 0 ;
	$Quantity = (int)(($row['Cut']) ? $row['BundleQuantity'] : $row['Quantity']) ;
	$sheet->writeString ($line, $collumn++, $row['PlanText']) ;
	$sheet->writeString ($line, $collumn++, $row['CaseId'].'/'.$row['Number']) ;
	$sheet->writeString ($line, $collumn++, $row['ArticleNumber'].'-'.$row['StyleVersion']) ;
	$sheet->writeNumber ($line, $collumn++, $Quantity, $formatdec3) ;
	$sheet->writeString ($line, $collumn++, $row['CompanyName']) ;
	$sheet->writeString ($line, $collumn++, $row['StyleDescription']) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['MaterialDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionStartDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionEndDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['SubDeliveryDate'])), $formatright) ;
	$sheet->writeString ($line, $collumn++, $row['MissingText']) ;
	$sheet->writeString ($line, $collumn++, $row['OperationNumber'], $formatright) ;
	$sheet->writeString ($line, $collumn++, $row['StyleOperationDescription']) ;
	$sheet->writeNumber ($line, $collumn++, $row['StyleOperationProductionMinutes'], $formatdec4) ;
	$v = (int)($row['StyleOperationProductionMinutes'] * $Quantity) ;
	$total['TotalTime'] += $v ;
	$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;
	$line++ ;

	$row = next ($Operation) ;
	$thisweek = (int)date('W', dbDateDecode($row['ProductionEndDate'])) ;    
	
	if (!$row or $thisweek != $lastweek) {
	    $collumn = 0 ;

	    $sheet->writeString ($line, $collumn++, sprintf ("Total, week %d", $lastweek), $formatTotal) ;
	    for ($n = 0 ; $n < 13 ; $n++) $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['TotalTime'], $formatTotalDec3) ;

	    $line += 2 ;
	    $lastweek = $thisweek ;
	    $total = array() ;
	}
    }

    // Let's send the file
    $book->close();
    
    return 0 ;
?>
