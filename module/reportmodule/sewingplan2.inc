<?php

    // Get Productions
    $clause = sprintf ('Production.Done=0 AND Production.Packed=0 AND Production.Allocated=1 
    					AND Production.Active=1 AND Production.ProductionLocationId=%d', $Parameter['Location']['Value']) ;
    $query = sprintf ("SELECT Production.*, 
    						Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, 
    						Style.Version AS StyleVersion, 
    						CONCAT(Company.Name,' (',Company.Number,')') AS CompanyName 
    					FROM Production LEFT JOIN Style ON Style.Id=Production.StyleId 
    						LEFT JOIN Article ON Article.Id=Style.ArticleId 
    						LEFT JOIN `Case` ON Case.Id=Production.CaseId 
    						LEFT JOIN Company ON Company.Id=Case.CompanyId 
    					WHERE %s 
    					ORDER BY Production.ProductionEndDate", $clause) ;
    $result = dbQuery ($query) ;
    $Production = array () ;
    while ($row = dbFetch($result)) {
		$id = (int)$row['Id'] ;
		$Production[$id] = $row ;
		$Production[$id]['TotalDayCount'] = 0 ;
    }
    dbQueryFree ($result) ;
//$testlog = fopen('/tmp/testlog','w');
//fwrite($testlog, print_r($Production, true);
//fclose($testlog);

    
// Get production weeks
    $query = sprintf ("SELECT Production.Id, COUNT(c.ActualDate) AS DayCount, YEARWEEK(c.ActualDate) AS WeekNo 
    					FROM Production INNER JOIN (SELECT DISTINCT ActualDate FROM Capacity) c 
    						ON COALESCE(Production.ProductionStartDate_Act,
Production.ProductionStartDate) <= c.ActualDate AND COALESCE(Production.ProductionStartDate_Act,
Production.ProductionEndDate) >=c.ActualDate 
    						AND YEARWEEK(c.ActualDate) >= YEARWEEK(now())  
    					WHERE %s 
    					GROUP BY Production.SubDeliveryDate, YEARWEEK(c.ActualDate), Production.Id
ORDER BY Production.SubDeliveryDate", $clause) ;
    $result = dbQuery ($query) ;
    $ProductionWeek = array () ;
    while ($row = dbFetch($result)) {
		$id = (int)$row['Id'] ;
		$ProductionWeek[$row['WeekNo']][$id] = $row['DayCount'] ;
		$Production[$id]['TotalDayCount'] += $row['DayCount'] ;
    }
    dbQueryFree ($result) ;

    //$testlog = fopen('/tmp/testlog','w');
//fwrite($testlog, print_r($Production, true);
//fclose($testlog);

    // Get Quantity from Bundles
    $query = sprintf ("SELECT Production.Id, SUM(Bundle.Quantity) AS BundleQuantity FROM Production LEFT JOIN Bundle ON Bundle.ProductionId=Production.Id AND Bundle.Active=1 WHERE %s GROUP BY Production.Id", $clause) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	if (!isset($Production[$id])) continue ; 
	$Production[$id]['BundleQuantity'] = (int)$row['BundleQuantity'] ;
    }
    dbQueryFree ($result) ;

    // Get ProductionMinutes
    $query = sprintf ("SELECT Production.Id, SUM(StyleOperation.ProductionMinutes) AS ProductionMinutesTotal,
    SUM(StyleOperation.ProductionMinutes*WorkGroup.Sewing) AS ProductionMinutesSewing,
    SUM(IF(Production.SourceLocationId > 0 AND Production.SourceFromNo <= StyleOperation.No AND Production.SourceToNo >= StyleOperation.No AND WorkGroup.Sewing = 1, StyleOperation.ProductionMinutes, 0)) AS ProductionMinutesSourced
    FROM Production LEFT JOIN StyleOperation ON StyleOperation.StyleId=Production.StyleId AND StyleOperation.Active=1 LEFT JOIN MachineGroup ON MachineGroup.Id=StyleOperation.MachineGroupId LEFT JOIN WorkGroup ON WorkGroup.Id=MachineGroup.WorkGroupId WHERE %s GROUP BY Production.Id", $clause) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	if (!isset($Production[$id])) continue ; 
	$Production[$id]['ProductionMinutesTotal'] = (float)$row['ProductionMinutesTotal'] ;
	$Production[$id]['ProductionMinutesSewing'] = (float)$row['ProductionMinutesSewing'] ;
	$Production[$id]['ProductionMinutesSourced'] = (float)$row['ProductionMinutesSourced'] ;
   }
    dbQueryFree ($result) ;
    
//Get planned outsourced Minutes
foreach( $Production as $id =>$prod) {
  if(!$prod['Cut']) {
    $query = sprintf("SELECT SUM(o.ProductionMinutes * q.Quantity *w.Sewing) AS OutMinutes
from Production p INNER JOIN  StyleOperation o ON p.StyleId=o.StyleID AND o.Active=1
INNER JOIN OutSourcingPlan s ON s.ProductionId = p.Id 
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
INNER JOIN ProductionQuantity q ON q.ProductionId=p.Id
	AND (q.ArticleColorId = s.ArticleColorId OR s.ArticleColorId is null)
	AND (q.ArticleSizeId = s.ArticleSizeId OR s.ArticleSizeId is null)
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d", $id);
  } else {
    $query = sprintf("
SELECT coalesce(SUM(o.ProductionMinutes * b.Quantity * w.Sewing), 0) AS OutMinutes
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN StyleOperation o ON o.StyleId = p.StyleId AND o.Active=1
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d AND s.InDate is null;
", $id);
  }
  $tab = dbRead($query);
  
  $Production[$id]['ProductionMinutesSourced'] = $tab[0]['OutMinutes'];
}

//Get insourced Minutes
foreach( $Production as $id =>$prod) {
 
    $query = sprintf("
SELECT coalesce(SUM(o.ProductionMinutes * b.Quantity * w.Sewing), 0) AS OutMinutes
FROM OutSourcing s INNER JOIN Bundle b ON s.BundleId=b.Id
INNER JOIN Production p ON s.ProductionId = p.Id
INNER JOIN StyleOperation o ON o.StyleId = p.StyleId AND o.Active=1
	AND s.OperationFromNo <= o.No AND s.OperationToNo >= o.No
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d AND YEARWEEK(s.InDate) < YEARWEEK(now());
", $id);
  
  $tab = dbRead($query);
  
  $Production[$id]['DoneOut'] = $tab[0]['OutMinutes'];
}

//get done minutes
foreach ($Production as $id=>$prod) {
    $query = sprintf("
SELECT coalesce(SUM(o.ProductionMinutes * bw.Quantity * w.Sewing), 0) AS Minutes
FROM BundleWork bw INNER JOIN Bundle b ON bw.BundleId=b.Id
INNER JOIN Production p ON bw.ProductionId = p.Id
INNER JOIN StyleOperation o ON o.Active=1
	AND bw.StyleOperationId = o.Id
LEFT JOIN MachineGroup m ON m.Id = o.MachineGroupId
LEFT JOIN WorkGroup w   ON m.WorkGroupId = w.Id
WHERE p.Id=%d AND YEARWEEK(bw.CreateDate) < YEARWEEK(now());
", $id);
  
  $tab = dbRead($query);
  
  $Production[$id]['DoneIn'] = $tab[0]['Minutes'];
}
    // logPrintVar ($Production, 'Production') ;

    // Any samples
    if (count($Production) == 0) return "No ProductionOrders" ;

    require_once 'Spreadsheet/Excel/Writer.php';

    // Initialization
    define ('HEADLINE', 4) ;
    define ('HEADLINECOLOR', 22) ;

    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;
    $book->send ('plan.xls') ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ('Sewing Plan') ;
    $sheet->setLandscape () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    $sheet->fitToPages (1, 100) ;
    $sheet->freezePanes (array(4,4)) ;
    
    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString (0, 0, 'Sewing Plan', $format) ;
    $sheet->writeString (1, 0, sprintf (" %s: %s", $Parameter['Location']['Description'], reportParameterString ($Parameter['Location']))) ;
    $sheet->writeString (2, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;

    // Table Headline
    $collumn = 0 ;
    $format =& $book->addFormat () ;
    $format->setFgColor (HEADLINECOLOR) ;
    $formatright =& $book->addFormat () ;
    $formatright->setFgColor (HEADLINECOLOR) ;
    $formatright->setAlign ('right') ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Note', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Production', $format) ;
    $sheet->setColumn($collumn, $collumn, 13) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Style', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Quantity', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 30) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Customer', $format) ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Description', $format) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Material Baseline', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Material Actual', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Start Baseline', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Start Actual', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'End Baseline', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'End Actual', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Shipment Baseline', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 15) ;
$sheet->writeString (HEADLINE, $collumn++, 'Shipment Actual', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 35) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Missing', $format) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
//$sheet->writeString (HEADLINE, $collumn++, 'Sew', $formatright) ;
//  $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Total Sew', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
//  $sheet->writeString (HEADLINE, $collumn++, 'Out', $formatright) ;
//  $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Total Out', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 10) ;
//  $sheet->writeString (HEADLINE, $collumn++, 'All', $formatright) ;
//  $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Total All', $formatright) ;

    // Table Data
    $line = HEADLINE + 1 ;
    $formatdec3 =& $book->addFormat () ;
    $formatdec3->setNumFormat (3) ;		// Format: decimal #,##0
    $formatdec4 =& $book->addFormat () ;
    $formatdec4->setNumFormat (4) ;		// Format: decimal #,##0.00
    $formatright =& $book->addFormat () ;
    $formatright->setAlign ('right') ;

    $formatTotal =& $book->addFormat () ;
    $formatTotal->setTop (1) ;
    $formatTotalDec3 =& $book->addFormat () ;
    $formatTotalDec3->setTop (1) ;
    $formatTotalDec3->setNumFormat (3) ;	// Format: decimal #,##0

    
    foreach ($ProductionWeek as $week => $production) {
	    $total = array() ;
	    foreach ($production as $pid => $daycount) {
	    	$row = $Production[$pid] ;
			$collumn = 0 ;
			$Quantity = (int)(($row['Cut']) ? $row['BundleQuantity'] : $row['Quantity']) ;
			$sheet->writeString ($line, $collumn++, $row['PlanText']) ;
			$sheet->writeString ($line, $collumn++, $row['CaseId'].'/'.$row['Number']) ;
			$sheet->writeString ($line, $collumn++, $row['ArticleNumber'].'-'.$row['StyleVersion']) ;
			$sheet->writeNumber ($line, $collumn++, ($daycount*$Quantity/$row['TotalDayCount']), $formatdec3) ;
			$sheet->writeString ($line, $collumn++, $row['CompanyName']) ;
			$sheet->writeString ($line, $collumn++, $row['ArticleDescription']) ;
			$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['MaterialDate'])), $formatright) ;
			if ($row['MaterialDate_Act']) {
$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['MaterialDate_Act'])), $formatright) ;
			} else {
			  $sheet->writeString($line,$collumn++,'');
			}

			$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionStartDate'])), $formatright) ;
if ($row['ProductionStartDate_Act']) {
$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionStartDate_Act'])), $formatright) ;
			} else {
			  $sheet->writeString($line,$collumn++,'');
			}

			$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionEndDate'])), $formatright) ;
if ($row['ProductionEndDate_Act']) {
$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['ProductionEndDate_Act'])), $formatright) ;
			} else {
			  $sheet->writeString($line,$collumn++,'');
			}

			$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['SubDeliveryDate'])), $formatright) ;
if ($row['SubDeliveryDate_Act']) {
$sheet->writeString ($line, $collumn++, date('m-d (W)', dbDateDecode($row['SubDeliveryDate_Act'])), $formatright) ;
			} else {
			  $sheet->writeString($line,$collumn++,'');
			}

			$sheet->writeString ($line, $collumn++, $row['MissingText']) ;
		
			//$sheet->writeNumber ($line, $collumn++, $row['ProductionMinutesSewing'], $formatdec4) ;
			$v = (int)($daycount*(($row['ProductionMinutesSewing']* $Quantity  
					       - $row['DoneIn'] - $row['ProductionMinutesSourced'] )/$row['TotalDayCount'])*$row['Efficiency']/100) ;
			$total['TotalSew'] += $v ;
			$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;
		
			//$sheet->writeNumber ($line, $collumn++, $row['ProductionMinutesSourced'], $formatdec4) ;
			$v = (int)(($daycount*($row['ProductionMinutesSourced'] -$row['DoneOut'])/$row['TotalDayCount'])*$row['Efficiency']/100) ;
			$total['TotalSourced'] += $v ;
			$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;
		
		
			//$sheet->writeNumber ($line, $collumn++, $row['ProductionMinutesTotal'], $formatdec4) ;
			$v = (int)(($daycount*(($row['ProductionMinutesTotal'] * $Quantity)-$row['DoneIn']-$row['DoneOut'])/$row['TotalDayCount'])*$row['Efficiency']/100) ;
			$total['TotalAll'] += $v ;
			$sheet->writeNumber ($line, $collumn++, $v, $formatdec3) ;
			$line++ ;
	    }
	    $collumn = 0 ;

	    $sheet->writeString ($line, $collumn++, sprintf ("Total, week %d", $week), $formatTotal) ;
	    for ($n = 0 ; $n < 14 ; $n++) $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['TotalSew'], $formatTotalDec3) ;
	    //$sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['TotalSourced'], $formatTotalDec3) ;
	    //$sheet->writeBlank ($line, $collumn++, $formatTotal) ;
	    $sheet->writeNumber ($line, $collumn++, $total['TotalAll'], $formatTotalDec3) ;

	    $line += 2 ;
	    $total = array() ;
    }
    // Let's send the file
    $book->close();
    
    return 0 ;
?>
