<?php

    require_once 'lib/log.inc' ;
    require_once 'lib/table.inc' ;

    $ProductionLocationId = $Parameter['Location']['Value'] ;
    
    // Get Productions
    $clause = sprintf ('Production.Packed=1 AND Production.Active=1 AND Production.PackedDate>="%s" AND Production.PackedDate<"%s" AND Production.ProductionLocationId=%d', dbDateEncode($Parameter['From']['Value']), dbDateEncode($Parameter['To']['Value']), $ProductionLocationId) ;
    $query = sprintf ("SELECT Production.*, Production.Quantity AS ProductionQuantity, SUM(Bundle.Quantity) AS Quantity, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Style.Version AS StyleVersion, CONCAT(Company.Name,' (',Company.Number,')') AS CompanyName FROM Production LEFT JOIN Style ON Style.Id=Production.StyleId LEFT JOIN Article ON Article.Id=Style.ArticleId LEFT JOIN `Case` ON Case.Id=Production.CaseId LEFT JOIN Company ON Company.Id=Case.CompanyId LEFT JOIN Bundle ON Bundle.ProductionId=Production.Id AND Bundle.Active=1 WHERE %s GROUP BY Production.Id ORDER BY Production.PackedDate", $clause) ;

    $result = dbQuery ($query) ;
    $Production = array () ;
    $InStyle = '' ;
    while ($row = dbFetch($result)) {
	$Production[(int)$row['Id']] = $row ;

	// Make list of Styles used
	if ($InStyle != '') $InStyle .= ',' ;
	$InStyle .= $row['StyleId'] ;
    }
    dbQueryFree ($result) ;

    // Any Production orders
    if (count($Production) == 0) return "No ProductionOrders" ;

    // Get Operations
    $query = sprintf ("SELECT StyleId, No, ProductionMinutes FROM currentstyleoperation WHERE StyleId IN (%s) AND Active=1", $InStyle) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$Style[(int)$row['StyleId']][(int)$row['No']] = $row['ProductionMinutes'] ;
    }
    dbQueryFree ($result) ;

    // Calculate times
    $quantity = 0 ;
    foreach ($Production as $i => $row) {
	$SourceLocationId = (int)$row['SourceLocationId'] ;
	$SourceFromNo = (int)$row['SourceFromNo'] ;
	$SourceToNo = (int)$row['SourceToNo'] ;
	$ProductionMinutes = 0 ;
	$SourceMinutes = 0 ;
	if (is_array($Style[(int)$row['StyleId']])) { 
	    foreach ($Style[(int)$row['StyleId']] as $no => $time) {
		if ($SourceLocationId == 0 or $no < $SourceFromNo or $no > $SourceToNo) {
		    $ProductionMinutes += $time ;
		} else {
		    $SourceMinutes += $time ;
		}
	    }
	}
	$Production[$i]['RealProductionMinutes'] = $ProductionMinutes + $SourceMinutes ;
	
	$Production[$i]['RealTotalProductionMinutes'] = ($ProductionMinutes + $SourceMinutes) * $row['Quantity'] ;
	
	$Production[$i]['ProductionLocation'][$ProductionLocationId] = $ProductionMinutes * $row['Quantity'] ;
	$ProductionLocation[$ProductionLocationId]['RealTotalProductionMinutes'] += $ProductionMinutes * $row['Quantity'] ;
	
	if ($SourceLocationId > 0) {
	    $Production[$i]['ProductionLocation'][$SourceLocationId] = $SourceMinutes * $row['Quantity'] ;
	    $ProductionLocation[$SourceLocationId]['RealTotalProductionMinutes'] += $SourceMinutes * $row['Quantity'] ;
	}

	$quantity += $row['Quantity'] ;
    }

    // Get names for ProductionLocations
    foreach ($ProductionLocation as $i => $row) {
	$ProductionLocation[$i]['Name'] = tableGetField ('ProductionLocation', 'Name', $i) ;
    }

//  logPrintVar ($Style, 'Style') ;
//  logPrintVar ($Production, 'Production') ;
//  logPrintVar ($ProductionLocation, 'ProductionLocation') ;
//  return 0 ;

    require_once 'Spreadsheet/Excel/Writer.php';

    // Initialization
    define ('HEADLINE', 4) ;
    define ('HEADLINECOLOR', 22) ;

    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;
    $book->send ('plan.xls') ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ('Production Finished') ;
    $sheet->setLandscape () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    $sheet->fitToPages (1, 100) ;
    $sheet->freezePanes (array(5,0)) ;

    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString (0, 0, 'Finished Production Statistics', $format) ;
    $sheet->writeString (1, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;
    $sheet->writeString (2, 0, sprintf (" %s: %s, From %s until %s", $Parameter['Location']['Description'], reportParameterString ($Parameter['Location']), date('Y-m-d', $Parameter['From']['Value']), date('Y-m-d', $Parameter['To']['Value']))) ;

    // Table Headline
    $collumn = 0 ;
    $format =& $book->addFormat () ;
    $format->setFgColor (HEADLINECOLOR) ;
    $formatright =& $book->addFormat () ;
    $formatright->setFgColor (HEADLINECOLOR) ;
    $formatright->setAlign ('right') ;
    $sheet->setColumn($collumn, $collumn, 10) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Production', $format) ;
    $sheet->setColumn($collumn, $collumn, 13) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Style', $format) ;
    $sheet->setColumn($collumn, $collumn, 30) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Customer', $format) ;
    $sheet->setColumn($collumn, $collumn, 30) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Description', $format) ;
    $sheet->setColumn($collumn, $collumn, 12) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Quantity', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 12) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Packed', $formatright) ;
    $sheet->setColumn($collumn, $collumn, 12) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Minutes', $formatright) ;
    foreach ($ProductionLocation as $row) {
	$sheet->setColumn($collumn, $collumn, 12) ;
	$sheet->writeString (HEADLINE, $collumn++, $row['Name'], $formatright) ;
    }
    $sheet->setColumn($collumn, $collumn, 12) ;
    $sheet->writeString (HEADLINE, $collumn++, 'Total', $formatright) ;

    // Table Data
    $line = HEADLINE + 1 ;
 
    $formatdec3 =& $book->addFormat () ;
    $formatdec3->setNumFormat (3) ;		// Format: decimal #,##0
    $formatdec4 =& $book->addFormat () ;
    $formatdec4->setNumFormat (4) ;		// Format: decimal #,##0.00
    $formatright =& $book->addFormat () ;
    $formatright->setAlign ('right') ;

    $formatTotal =& $book->addFormat () ;
    $formatTotal->setTop (1) ;
    $formatTotalDec3 =& $book->addFormat () ;
    $formatTotalDec3->setTop (1) ;
    $formatTotalDec3->setNumFormat (3) ;	// Format: decimal #,##0

    foreach ($Production as $row) {
	$collumn = 0 ;
	$sheet->writeString ($line, $collumn++, $row['CaseId'].'/'.$row['Number']) ;
	$sheet->writeString ($line, $collumn++, $row['ArticleNumber'].'-'.$row['StyleVersion']) ;
	$sheet->writeString ($line, $collumn++, $row['CompanyName']) ;
	$sheet->writeString ($line, $collumn++, $row['ArticleDescription']) ;
	$sheet->writeNumber ($line, $collumn++, $row['Quantity'], $formatdec3) ;
	$sheet->writeString ($line, $collumn++, date('Y-m-d', dbDateDecode($row['PackedDate'])), $formatright) ;
	$sheet->writeNumber ($line, $collumn++, $row['RealProductionMinutes'], $formatdec4) ;
	foreach ($ProductionLocation as $i => $prod) {
	    $sheet->writeNumber ($line, $collumn++, $row['ProductionLocation'][$i], $formatdec3) ;
	}
	$sheet->writeNumber ($line, $collumn++, $row['RealTotalProductionMinutes'], $formatdec3) ;
	$line++ ;
    }
	    
    // Total
    $total = 0 ;
    $collumn = 0 ;
    $sheet->writeString ($line, $collumn++, "Total", $formatTotal) ;
    for ($n = 0 ; $n < 3 ; $n++) $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
    $sheet->writeNumber ($line, $collumn++, $quantity, $formatTotalDec3) ;
    for ($n = 0 ; $n < 2 ; $n++) $sheet->writeBlank ($line, $collumn++, $formatTotal) ;
    foreach ($ProductionLocation as $i => $prod) {
	$sheet->writeNumber ($line, $collumn++, $prod['RealTotalProductionMinutes'], $formatTotalDec3) ;
	$total += $prod['RealTotalProductionMinutes'] ;
    }
    $sheet->writeNumber ($line, $collumn++, $total, $formatTotalDec3) ;
    $line++ ;

    // Let's send the file
    $book->close();
    
    return 0 ;
?>
