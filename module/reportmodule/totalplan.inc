<?php
// proWide
// Hvass Innovation ApS
// Niels Kristensen
//
// File:    ./module/prodstat/total-xls.inc
// Version: 1.0

    define ('DEBUG', 0) ;

    require_once 'lib/table.inc' ;
if (DEBUG) require_once 'lib/log.inc' ;

    // Calculat first date
    $FromDate = time () ;
//  $FromDate -= ((((int)date('w',$FromDate))+6)%7)*60*60*24 ;
if (DEBUG) printf ("From %s, week %d, day %d<br>\n", date('Y-m-d', $FromDate), date('W',$FromDate), date('w',$FromDate)) ; 
    
    // Get WorkGroups
    $query = sprintf ("SELECT * FROM WorkGroup WHERE Active=1 AND Sewing=1 ORDER BY Number") ;
if (DEBUG) printf ("<br>WorkGroup query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    $WorkGroup = array () ;
    while ($row = dbFetch($result)) {
	$WorkGroupId = (int)$row['Id'] ;
	$row['MachineGroup'] = array () ;
	$WorkGroup[$WorkGroupId] = $row ;
    }
    dbQueryFree ($result) ;
    if (count($WorkGroup) == 0) return sprintf ("No WorkGroups defined") ;

    // Get MachineGroups
    $query = sprintf ("SELECT * FROM MachineGroup WHERE Active=1 ORDER BY Number") ;
if (DEBUG) printf ("MachineGroup query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$WorkGroupId = (int)$row['WorkGroupId'] ;
	if (!isset($WorkGroup[$WorkGroupId])) continue ;
	$WorkGroup[$WorkGroupId]['MachineGroup'][(int)$row['Id']] = $row ;
    }
    dbQueryFree ($result) ;
    if (count($WorkGroup) == 0) return sprintf ("No WorkGroups defined") ;
if (DEBUG) logPrintVar ($WorkGroup, 'WorkGroup') ;
	    
    // Get Productions
    $clause = sprintf ('Production.Done=0 AND Production.Packed=0 AND Production.Active=1 AND Production.ProductionLocationId=%d', $Parameter['Location']['Value']) ;
//  if (!$Parameter['Requests']['Value']) $clause .= sprintf(" AND Production.Allocated=1") ;
	if (!$Parameter['Defined']['Value']) {
    	if ($Parameter['Requests']['Value']) { 
			$clause .= ' AND Production.Fabrics=1' ;
    	} else {
			$clause .= ' AND Production.Allocated=1' ;
    	}
    }
    $clause .= sprintf (" AND COALESCE(Production.ProductionEndDate)<'%s'", date('Y-m-d', $FromDate+60*60*24*30*6)) ;
    $query = sprintf ("SELECT Production.*, SUM(Bundle.Quantity) AS BundleQuantity FROM Production LEFT JOIN Bundle ON Bundle.ProductionId=Production.Id AND Bundle.Active=1 WHERE %s GROUP BY Production.Id ORDER BY Production.ProductionEndDate", $clause) ;
if (DEBUG) printf ("<br>Production query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    $Production = array () ;
    while ($row = dbFetch($result)) {
	$id = (int)$row['Id'] ;
	unset ($row['Id']) ;
//	$t = dbDateDecode($row['ProductionEndDate_Act']?$row['ProductionEndDate_Act']:$row['ProductionEndDate']) ;
	$t = dbDateDecode($row['ProductionEndDate']) ;
	if ($t < $FromDate) $t = $FromDate ;
	$WeekNo = (int)date('W', $t) ;
	$row['WeekNo'] = $WeekNo ;
	$row['Work'] = array () ;
	$Production[$id] = $row ;
    }
    dbQueryFree ($result) ;
    if (count($Production) == 0) return sprintf ("No ProductionOrders") ;	    

    // Get Work done for ProductionOrders
    $group = '' ;
    $Style = array() ;
    foreach ($Production as $id => $row) {
	if ($group != '') $group .= ',' ;
	$group .= $id ;		
    }
    $query = sprintf ("SELECT BundleWork.ProductionId, BundleWork.StyleOperationId, SUM(BundleWork.Quantity) AS Quantity FROM BundleWork WHERE BundleWork.ProductionId IN (%s) AND BundleWork.Active=1 GROUP BY BundleWork.ProductionId, BundleWork.StyleOperationId", $group) ;
if (DEBUG) printf ("<br>Production Work query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$ProductionId = (int)$row['ProductionId'] ;
	$StyleOperationId = (int)$row['StyleOperationId'] ;
	$Production[$ProductionId]['Work'][$StyleOperationId]['Quantity'] = (int)$row['Quantity'] ;
    }
    dbQueryFree ($result) ;
if (DEBUG) logPrintVar ($Production, 'Production') ;

    // Get Styles in use
    $group = '' ;
    $Style = array() ;
    foreach ($Production as $row) {
	$id = (int)$row['StyleId'] ;
	if (isset($Style[$id])) continue ;
	if ($group != '') $group .= ',' ;
	$group .= $id ;		
	$Style[$id] = array('StyleOperation' => array(), 'MachineGroup' => array()) ;
    }
    $query = sprintf ("SELECT currentstyleoperation.* FROM currentstyleoperation WHERE currentstyleoperation.StyleId IN (%s) AND currentstyleoperation.Active=1", $group) ;
if (DEBUG) printf ("<br>Style query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
	$Id = (int)$row['Id'] ;
	$StyleId = (int)$row['StyleId'] ;
	$MachineGroupId = (int)$row['MachineGroupId'] ;
	$ProductionMinutes = (float)$row['ProductionMinutes'] ;
	$Style[$StyleId]['StyleOperation'][$Id] = array ('ProductionMinutes' => $ProductionMinutes, 'MachineGroupId' => $MachineGroupId, 'No' => (int)$row['No']) ;
	$Style[$StyleId]['MachineGroup'][$MachineGroupId]['ProductionMinutes'] += $ProductionMinutes ;
    }
    dbQueryFree ($result) ;
if (DEBUG) logPrintVar ($Style, 'Style') ;

    // Calculate ProductionMinutes for each Week/MachineGroup based on the Production/Style
    $Week = array () ;
    $ProductionLocation = array () ;
    foreach ($Production as $prodid => $orderline) {
	$StyleId = (int)$orderline['StyleId'] ;
	$WeekNo = (int)$orderline['WeekNo'] ;
	$Quantity = ($orderline['Cut']) ? (int)$orderline['BundleQuantity'] : (int)$orderline['Quantity'] ;
	//$SourceLocationId = (int)$orderline['SourceLocationId'] ;
	//$SourceFromNo = (int)$orderline['SourceFromNo'] ;
	//$SourceToNo = (int)$orderline['SourceToNo'] ;
	//if ($SourceLocationId > 0) $ProductionLocation[$SourceLocationId]['Used'] = true ;

	// Total time for ProductionOrder
	foreach ($Style[$StyleId]['StyleOperation'] as $operation) {
	    $ProductionMinutes = $operation['ProductionMinutes'] * $Quantity ;
	    /* if ($SourceLocationId > 0 and $operation['No'] >= $SourceFromNo and $operation['No'] <= $SourceToNo) {
		if ($orderline['SourceIn'] == 0) {
		    $Week[$WeekNo]['SourceLocation'][$SourceLocationId]['ProductionMinutes'] += $ProductionMinutes ;
		    $Week[$WeekNo]['SourceLocation']['ProductionMinutes'] += $ProductionMinutes ;
		}
	    } else {*/
		$Week[$WeekNo]['MachineGroup'][(int)$operation['MachineGroupId']]['ProductionMinutes'] += $ProductionMinutes ;
	}
	
	// Get the Planned Outsourced quantities and productionLocations for this production Order
	
	$tab = dbRead(sprintf("SELECT COUNT(*) as Cnt FROM ProductionQuantity WHERE ProductionId = %d", $prodid));
	if ($tab[0]['Cnt'] == 0) {
	  // 1st case - No size breakdown - full quantity of order
	$query = sprintf("SELECT so.Id, osp.LocationId, p.Quantity 
						FROM OutSourcingPlan osp INNER JOIN Production p
						ON osp.ProductionId = p.Id
						INNER JOIN currentstyleoperation so ON p.StyleId = so.StyleId
						AND so.No >= osp.OperationFromNo AND so.No <= osp.OperationToNo
						WHERE p.Id = %d",$prodid);
	} elseif (!$orderline['Cut']) { 
	// 2nd case - size breakdown, no bundles - orderquantities
	$query = sprintf("SELECT so.Id, osp.LocationId, SUM(pq.Quantity) as Quantity
						FROM OutSourcingPlan osp INNER JOIN ProductionQuantity pq
						ON osp.ProductionId = pq.ProductionId
						INNER JOIN Production p ON p.Id = pq.ProductionId
						INNER JOIN currentstyleoperation so ON p.StyleId = so.StyleId
						AND so.No >= osp.OperationFromNo AND so.No <= osp.OperationToNo
						AND (pq.ArticleColorId = osp.ArticleColorId OR osp.ArticleColorId is null)
						AND (pq.ArticleSizeId = osp.ArticleSizeId OR osp.ArticleSizeId is null)
						WHERE p.Id = %d
						GROUP BY so.Id, osp.LocationId",$prodid);
	} else {
	// 3rd case - bundles - actual bundled quantities
	$query = sprintf("SELECT so.Id, os.LocationId, SUM(b.Quantity) as Quantity
					FROM currentstyleoperation so INNER JOIN Production p
					ON p.StyleId = so.StyleId
					INNER JOIN OutSourcing os ON p.Id = os.ProductionId
					AND so.No >= os.OperationFromNo AND so.No <= os.OperationToNo
					INNER JOIN Bundle b ON os.BundleId = b.Id
						WHERE os.ProductionId = %d
						GROUP BY so.Id, os.LocationId",$prodid);
	}
	$res = dbQuery($query);
	while($row = dbFetch($res)) {
	  		$operation =$Style[$StyleId]['StyleOperation'][$row['Id']];
	  		$ProductionMinutes = $row['Quantity']*$operation['ProductionMinutes'];
	 	 	$Week[$WeekNo]['SourceLocation'][$row['LocationId']]['ProductionMinutes'] += $ProductionMinutes ;
		    $Week[$WeekNo]['SourceLocation']['ProductionMinutes'] += $ProductionMinutes ;
		    $Week[$WeekNo]['MachineGroup'][(int)$operation['MachineGroupId']]['ProductionMinutes'] -= $ProductionMinutes ;
		    $ProductionLocation[$row['LocationId']]['Used'] = true ;
	}  	

	dbQueryFree($res) ;
	//Get done quantities outsourced for this Production Order
	$query = sprintf("SELECT so.Id, os.LocationId, SUM(b.Quantity) as Quantity
					FROM currentstyleoperation so INNER JOIN Production p
					ON p.StyleId = so.StyleId
					INNER JOIN OutSourcing os ON p.Id = os.ProductionId
					AND so.No >= os.OperationFromNo AND so.No <= os.OperationToNo
					INNER JOIN Bundle b ON os.BundleId = b.Id
						WHERE os.ProductionId = %d AND os.InDate IS NOT NULL
						GROUP BY so.Id, os.LocationId",$prodid);
	$res = dbQuery($query);
	while($row = dbFetch($res)) {
	  		$operation =$Style[$StyleId]['StyleOperation'][$row['Id']];
	  		$ProductionMinutes = $row['Quantity']*$operation['ProductionMinutes'];
	 	 	$Week[$WeekNo]['SourceLocation'][$row['LocationId']]['ProductionMinutes'] -= $ProductionMinutes ;
		    $Week[$WeekNo]['SourceLocation']['ProductionMinutes'] -= $ProductionMinutes ;
	} 

	dbQueryFree($res) ;
	// Reduce for work done
	foreach ($orderline['Work'] as $StyleOperationId => $styleoperation) {
	    $Quantity = $styleoperation['Quantity'] ;
	    $ProductionMinutes = $Style[$StyleId]['StyleOperation'][$StyleOperationId]['ProductionMinutes'] ;
	    $MachineGroupId = $Style[$StyleId]['StyleOperation'][$StyleOperationId]['MachineGroupId'] ;
	    $Week[$WeekNo]['MachineGroup'][$MachineGroupId]['ProductionMinutes'] -= $ProductionMinutes * $Quantity ;		    
if (DEBUG) printf ("Reducing: Week %d, MachineGroup %d, ProductionMinutes %01.2f, Quantity %d<br>\n", $WeekNo, $MachineGroupId, $ProductionMinutes, $Quantity) ;		    
			}
	    }

    // Get names for Production Locations
    foreach ($ProductionLocation as $i => $row) {
	$ProductionLocation[$i]['Name'] = tableGetField ('ProductionLocation', 'Name', $i) ;
    }
     
    // Get Capacities
    $query = sprintf ("SELECT DATE_FORMAT(ActualDate,'%%v') AS WeekNo, WorkGroupId, Count(Id) AS PlanDays, 
    SUM(Operators*ProductionMinutes) AS OperatorMinutes 
    FROM Capacity 
    WHERE ActualDate>='%s' AND ActualDate<'%s' AND Active=1 GROUP BY WeekNo, WorkGroupId", date('Y-m-d', $FromDate), date('Y-m-d', $FromDate+60*60*24*30*6)) ;
if (DEBUG) printf ("<br>Capacity query: %s<br>\n", $query) ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
		$WeekNo = (int)$row['WeekNo'] ;
		if (!isset($Week[$WeekNo])) continue ;
		$WorkGroupId = (int)$row['WorkGroupId'] ;
		if (!isset($WorkGroup[$WorkGroupId])) continue ;
		$row['OperatorMinutes'] = (int)($row['OperatorMinutes'] * $WorkGroup[$WorkGroupId]['Efficiency'] / 100) ;
		$Week[$WeekNo]['WorkGroup'][$WorkGroupId] = $row ;
		if ($Week[$WeekNo]['PlanDays'] < (int)$row['PlanDays']) $Week[$WeekNo]['PlanDays'] = (int)$row['PlanDays'] ;
    }
    dbQueryFree ($result) ;

    // Get Budget Goals
    $query = sprintf("
	    SELECT yearweek(d.ActualDate) as ywk, week(d.actualdate,3)as weekno, sum(v.BudgetGoal) AS SumBudgetGoal, sum(v.BudgetGoalExt) AS SumBudgetGoalExt 
	    FROM 
	    (SELECT Capacity.ActualDate, SUM(Capacity.Operators*Capacity.ProductionMinutes) AS ProductionMinutes 
	    		FROM Capacity WHERE Capacity.Active=1 
	    		GROUP BY Capacity.ActualDate) d 
	    left join
	    (SELECT b.Id, b.FromDate, min(g.FromDate) AS ToDate, b.BudgetGoal, b.BudgetGoalExt 
	    		FROM BudgetGoal b inner join BudgetGoal g ON b.FromDate<g.FromDate 
	    		WHERE b.Active=1 And g.Active=1 
	    		GROUP BY b.Id order by b.FromDate) v 
	    ON d.ActualDate>=v.FromDate and d.ActualDate<v.ToDate 
	    WHERE yearweek(d.ActualDate) >= yearweek(now()) 
	    GROUP BY ywk, weekno") ;
    $result = dbQuery ($query) ;
    while ($row = dbFetch($result)) {
		$weekno = $row['weekno']  ;
		$Week[$weekno]['BudgetGoal'] = (int)$row['SumBudgetGoal'] ;
		$Week[$weekno]['BudgetGoalExt'] = (int)$row['SumBudgetGoalExt'] ;
    }
    dbQueryFree ($result) ;
//		$Week[27]['BudgetGoal'] = (int)1001 ;

    // Calculate totals
    foreach ($Week as $WeekNo => $week) {
	$PlanDays = $week['PlanDays'] ;

	foreach ($WorkGroup as $WorkGroupId => $workgroup) {

	    foreach ($workgroup['MachineGroup'] as $MachineGroupId => $machinegroup) {
		// ProductionMinutes for WorkGroup
		$ProductionMinutes = (int)$week['MachineGroup'][$MachineGroupId]['ProductionMinutes'] ;
		if ($ProductionMinutes > 0) {
		    $Week[$WeekNo]['WorkGroup'][$WorkGroupId]['ProductionMinutes'] += $ProductionMinutes ;

		    // Machine Minutes
		    $MachineMinutes = $machinegroup['ProductionMinutes'] * $machinegroup['Machines'] * $PlanDays * $workgroup['Efficiency'] / 100 ;
		    $Week[$WeekNo]['MachineGroup'][$MachineGroupId]['MachineMinutes'] = $MachineMinutes ;

		    // Days
		    if ($MachineMinutes > 0) {
			$Days = $ProductionMinutes / $MachineMinutes * $PlanDays ;
			$Week[$WeekNo]['MachineGroup'][$MachineGroupId]['Days'] = $Days ;
		    }
		}

		// Days for WorkGroup
		$ProductionMinutes = $Week[$WeekNo]['WorkGroup'][$WorkGroupId]['ProductionMinutes'] ;
		$OperatorMinutes = $Week[$WeekNo]['WorkGroup'][$WorkGroupId]['OperatorMinutes'] ;
		if ($OperatorMinutes > 0 and $PlanDays > 0) {
		    $Days = $ProductionMinutes / $OperatorMinutes * $PlanDays ;
		    $Week[$WeekNo]['WorkGroup'][$WorkGroupId]['Days'] = $Days ;
		}	
	    }

	    // ProductionMinutes for Week
	    $Week[$WeekNo]['ProductionMinutes'] += $Week[$WeekNo]['WorkGroup'][$WorkGroupId]['ProductionMinutes'] ;

	    // OperatorMinutes for Week
	    $Week[$WeekNo]['OperatorMinutes'] += $Week[$WeekNo]['WorkGroup'][$WorkGroupId]['OperatorMinutes'] ;
	}

	// Days for Week
	$ProductionMinutes = $Week[$WeekNo]['ProductionMinutes'] ;
	$OperatorMinutes = $Week[$WeekNo]['OperatorMinutes'] ;
	$PlanDays = $Week[$WeekNo]['PlanDays'] ;
	if ($OperatorMinutes > 0 and $PlanDays > 0) {
	    $Days = $ProductionMinutes / $OperatorMinutes * $PlanDays ;
	    $Week[$WeekNo]['Days'] = $Days ;
	}	
    }	    
    if (DEBUG) logPrintVar ($Week, 'Week') ;

    require_once 'Spreadsheet/Excel/Writer.php';

    // Initialization
    define ('HEADLINE', 4) ;
    define ('HEADLINECOLOR', 22) ;
    define ('RESOURCE', 1) ;
    $total = array() ;

    // Create workbook
    $book = new Spreadsheet_Excel_Writer () ;

    // Create a worksheet
    $sheet =& $book->addWorksheet ('Total Plan') ;
    $sheet->setLandscape () ;
    $sheet->setMargins (0.2) ;
    $sheet->hideGridlines () ;
    $sheet->setHeader ('', 0) ;
    $sheet->setFooter ('', 0) ;
    $sheet->centerHorizontally () ;
    $sheet->centerVertically () ;
    $sheet->fitToPages (100, 1) ;
    $sheet->freezePanes (array(6,1)) ;

    // Header
    $format =& $book->addFormat () ;
    $format->setSize (22) ;
    $format->setBold () ;
    $sheet->writeString (0, 0, 'Total Plan', $format) ;
    $sheet->writeString (1, 0, sprintf (" ProductionLocation %s, %s requested ProductionOrders, %s defined ProductionOrders", reportParameterString ($Parameter['Location']), ($Parameter['Defined']['Value']||$Parameter['Requests']['Value']) ? 'Including' : 'Excluding',($Parameter['Defined']['Value']) ? 'Including' : 'Excluding')) ;
    $sheet->writeString (2, 0, sprintf (" Generated at %s by %s (%s)", date ('Y-m-d H:i:s'), $User['FullName'], $User['Loginname'])) ;

    // Table Headline formats
    $format =& $book->addFormat () ;
    $format->setFgColor (HEADLINECOLOR) ;

    $formatBorder =& $book->addFormat () ;
    $formatBorder->setFgColor (HEADLINECOLOR) ;
    $formatBorder->setLeft (1) ;

    $formatRight =& $book->addFormat () ;
    $formatRight->setFgColor (HEADLINECOLOR) ;
    $formatRight->setAlign ('right') ;

    $formatRightBorder =& $book->addFormat () ;
    $formatRightBorder->setFgColor (HEADLINECOLOR) ;
    $formatRightBorder->setAlign ('right') ;
    $formatRightBorder->setLeft (1) ;

    // Table Headline 1
    $collumn = 0 ;
    $sheet->setColumn($collumn, $collumn, 40) ;
    $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    foreach ($Week as $WeekNo => $row) {
        $sheet->setColumn($collumn, $collumn, 8) ;
	$sheet->writeString (HEADLINE, $collumn++, sprintf ("Week %d", $WeekNo), $formatBorder) ;
        if (RESOURCE) $sheet->setColumn($collumn, $collumn, 8) ;
	if (RESOURCE) $sheet->writeBlank (HEADLINE, $collumn++, $format) ;
        $sheet->setColumn($collumn, $collumn, 8) ;
	$sheet->writeBlank (HEADLINE, $collumn++, $format) ;
    }

    // Table Headline 2
    $collumn = 0 ;
    $sheet->writeString (HEADLINE+1, $collumn++, 'Group', $format) ;
    foreach ($Week as $WeekNo => $row) {
	$sheet->writeString (HEADLINE+1, $collumn++, 'Time', $formatRightBorder) ;
	if (RESOURCE) $sheet->writeString (HEADLINE+1, $collumn++, 'Resource', $formatRight) ;
	$sheet->writeString (HEADLINE+1, $collumn++, 'Days', $formatRight) ;
    }

    // Table Data formats
    $formatTop =& $book->addFormat () ;
    $formatTop->setTop (1) ;

    $formatLeft =& $book->addFormat () ;
    $formatLeft->setLeft (1) ;

    $formatTopLeft =& $book->addFormat () ;
    $formatTopLeft->setTop (1) ;
    $formatTopLeft->setLeft (1) ;
    
    $formatDec3 =& $book->addFormat () ;
    $formatDec3->setNumFormat (3) ;		// Format: decimal #,##0

    $formatDec3Left =& $book->addFormat () ;
    $formatDec3Left->setNumFormat (3) ;		// Format: decimal #,##0
    $formatDec3Left->setLeft (1) ;

    $formatDec3Top =& $book->addFormat () ;
    $formatDec3Top->setNumFormat (3) ;		// Format: decimal #,##0
    $formatDec3Top->setTop (1) ;

    $formatDec3TopLeft =& $book->addFormat () ;
    $formatDec3TopLeft->setNumFormat (3) ;	// Format: decimal #,##0
    $formatDec3TopLeft->setTop (1) ;
    $formatDec3TopLeft->setLeft (1) ;
       
    $formatDec4 =& $book->addFormat () ;
    $formatDec4->setNumFormat (4) ;		// Format: decimal #,##0.00

    $formatDec4Top =& $book->addFormat () ;
    $formatDec4Top->setNumFormat (4) ;		// Format: decimal #,##0.00
    $formatDec4Top->setTop (1) ;

    $formatRight =& $book->addFormat () ;
    $formatRight->setAlign ('right') ;

    // Table Data
    $line = HEADLINE + 2 ;

    // Total internal
    $collumn = 0 ;
    $sheet->writeString ($line, $collumn++, 'Total Internal', $formatTop) ;
    foreach ($Week as $WeekNo => $week) {
		$sheet->writeNumber ($line, $collumn++, $week['ProductionMinutes'], $formatDec3TopLeft) ;
		if (RESOURCE) $sheet->writeNumber ($line, $collumn++, $week['OperatorMinutes'], $formatDec3Top) ;
		$v = $week['Days'] ;
		if ($v == 0) {
		    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
		} else {
		    $sheet->writeNumber ($line, $collumn++, $v, $formatDec4Top) ;
		}
    }
    $line++ ;

    // Plan
    $collumn = 0 ;
    $sheet->writeString ($line, $collumn++, 'Budget Goal/Plandays') ;
    foreach ($Week as $WeekNo => $week) {
		$sheet->writeNumber ($line, $collumn++, $week['BudgetGoal'], $formatDec3) ;
		if (RESOURCE) $collumn++ ;		    
		$sheet->writeNumber ($line, $collumn++, $week['PlanDays'], $formatDec3) ;
    }
    $line++ ;

	// Total outsourcing
	$collumn = 0 ;
	$sheet->writeString ($line, $collumn++, 'Total Outsourcing', $formatTop) ;
	foreach ($Week as $WeekNo => $week) {
	    $v = $week['SourceLocation']['ProductionMinutes'] ;
	    $Week[$WeekNo]['ProductionMinutes'] += $v ;
	    $sheet->writeNumber ($line, $collumn++, $v, $formatDec3TopLeft) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	}
	$line++ ;

	// Total
	$collumn = 0 ;
	$sheet->writeString ($line, $collumn++, 'Grande Total', $formatTop) ;
	foreach ($Week as $WeekNo => $week) {
	    $sheet->writeNumber ($line, $collumn++, $week['ProductionMinutes'], $formatDec3TopLeft) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	}
	$line++ ;

	// Spacing
	$collumn = 0 ;
    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	foreach ($Week as $WeekNo => $week) {
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	    $sheet->writeBlank ($line, $collumn++, $formatTop) ;
	}
	$line++ ;

    foreach ($WorkGroup as $WorkGroupId => $workgroup) {

	foreach ($workgroup['MachineGroup'] as $MachineGroupId => $machinegroup) {
	    $collumn = 0 ;
	    $sheet->writeString ($line, $collumn++, $machinegroup['Description']) ;
 	    foreach ($Week as $WeekNo => $week) {
		$v = (int)$week['MachineGroup'][$MachineGroupId]['ProductionMinutes'] ;
		if ($v == 0) {
		    $sheet->writeBlank ($line, $collumn++, $formatLeft) ;
		    if (RESOURCE) $collumn++ ;		    
		    $collumn++ ;		    
		} else {
		    $sheet->writeNumber ($line, $collumn++, $v, $formatDec3Left) ;
		    if (RESOURCE) $sheet->writeNumber ($line, $collumn++, $week['MachineGroup'][$MachineGroupId]['MachineMinutes'], $formatDec3) ;
		    $v = $week['MachineGroup'][$MachineGroupId]['Days'] ;
		    if ($v == 0) {
			$collumn++ ;
		    } else {
			$sheet->writeNumber ($line, $collumn++, $v, $formatDec4) ;
		    }
		}
	    }
	    $line++ ;

	} 

	// WorkGroup sum
	$collumn = 0 ;
	$sheet->writeString ($line, $collumn++, $workgroup['Description'], $formatTop) ;
	foreach ($Week as $WeekNo => $week) {
	    $sheet->writeNumber ($line, $collumn++, $week['WorkGroup'][$WorkGroupId]['ProductionMinutes'], $formatDec3TopLeft) ;
	    if (RESOURCE) $sheet->writeNumber ($line, $collumn++, $week['WorkGroup'][$WorkGroupId]['OperatorMinutes'], $formatDec3Top) ;
	    $v = $week['WorkGroup'][$WorkGroupId]['Days'] ;
	    if ($v == 0) {
		$sheet->writeBlank ($line, $collumn++, $formatTop) ;
	    } else {
		$sheet->writeNumber ($line, $collumn++, $v, $formatDec4Top) ;
	    }
	}
	$line++ ;

	// Spacing
	$collumn = 0 ;
	$collumn++ ;
	foreach ($Week as $WeekNo => $week) {
	    $sheet->writeBlank ($line, $collumn++, $formatLeft) ;
	    if (RESOURCE) $collumn++ ;
	    $collumn++ ;
	}
	$line++ ;
    }

    if (count($ProductionLocation) == 0) {
	// No external porduction locations
	$line += 2 ;
	$collumn = 0 ;
	$sheet->writeString ($line, $collumn++, 'No external Production Locations') ;

    } else {
	// List external production locations
	// Spacing
	$collumn = 0 ;
	$collumn++ ;
	foreach ($Week as $WeekNo => $week) {
	    $sheet->writeBlank ($line, $collumn++, $formatLeft) ;
	    if (RESOURCE) $collumn++ ;
	    $collumn++ ;
	}
	$line++ ;

	foreach ($ProductionLocation as $i => $row) {
	    $collumn = 0 ;
	    $sheet->writeString ($line, $collumn++, $row['Name']) ;
 
	    foreach ($Week as $WeekNo => $week) {
		$sheet->writeNumber ($line, $collumn++, $week['SourceLocation'][$i]['ProductionMinutes'], $formatDec3Left) ;
		$collumn += 2 ;
    	    }

	    $line++ ;
	}
    }

    if (headers_sent()) {
	printf ("<br>Header allready send, stopping<br>\n") ;
	return 0 ;	
    }

    // Let's send the file
    $book->send ('plan.xls') ;
    $book->close();
    
    return 0 ;
?>
