<?php


    require_once 'lib/table.inc' ;

    if ($BundleWorkCount > 0) return "This Bundle can not be deleted because work has been registered" ;

		// First get allready created items
		$query = sprintf ("SELECT * FROM Item WHERE ArticleColorId=%d AND ArticleSizeId=%d AND ContainerId=165413 AND FromProductionId=%d AND Active=1", 
		                     $Record['ArticleColorId'], $Record['ArticleSizeId'], $Record['ProductionId']) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;

		if ($row['Id'] > 0) {
			$BundleQuantity = tableGetField ('Bundle', 'Quantity', $Id) ;
			if (($row['Quantity']-$BundleQuantity)>0) { 
				$query = sprintf ("UPDATE Item SET Quantity=%d WHERE Id=%d", ($row['Quantity']-$BundleQuantity), $row['Id']) ;
			} else {
				tableDelete('Item', $row['Id']) ;
			}
			$result = dbQuery ($query) ;
//			dbQueryFree ($result) ;
		} else {
			return 'Bundles hasnt items in production' ;
		}
    tableDelete ('Bundle', $Id) ;
    tableDeleteWhere ('BundleQuantity', sprintf ("BundleId=%d", $Id)) ; 


    return 0 ;
?>
