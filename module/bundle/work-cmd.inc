<?php

    require 'lib/save.inc' ;

    $fields = array (
	'Quantity'		=> array ('type' => 'integer',	'mandatory' => true, 	'check' => true),
	'UsedMinutes'		=> array ('type' => 'decimal',	'mandatory' => true,	'format' => '6.2')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Record, $Id, $fields ;
	switch ($fieldname) {
	    case 'Quantity':
		if (!$changed) return false ;

		// Validate quantity for bundle
		$query = sprintf ("SELECT SUM(BundleWork.Quantity) AS Quantity FROM BundleWork WHERE BundleWork.BundleId=%d AND BundleWork.StyleOperationId=%d AND BundleWork.Active=1 AND BundleWork.Id<>%d", $Record['BundleId'], $Record['StyleOperationId'], $Record['Id']) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if (($value + $row['Quantity']) > $Record['BundleQuantity']) return sprintf ("The quantity in registrated operations (%d) exceeds the qunatity of the bundle (%d)", $value + $row['Quantity'], $Record['BundleQuantity']) ;

		return true ;
	}
	return false ;
    }

    return saveFields ('BundleWork', $Id, $fields) ;
?>
