<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/dropmenu.inc' ;
require_once 'lib/table.inc';

    // Form (print command)
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;
    printf ("</form>\n") ;

    // Orderline header
    printf ("<br><table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Production</td><td width=100 class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ArticleNumber']), htmlentities($Record['ArticleDescription'])) ;
    printf ("</table>\n") ;

    // Bundle header
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Bundle</td><td class=itemfield>%08d</td></tr>\n", $Record['Id']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Colour</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ColorNumber'])) ;
    printf ("<tr><td class=itemlabel>Size</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['SizeName'])) ;
    printf ("<tr><td class=itemlabel>Quantity</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Quantity'])) ;
    print htmlItemInfo($Record) ;
    printf ("</table>\n") ;
  
    // Operations
    printf ("<br><p><b>Operations</b></p>\n") ;
    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=30><p class=listhead>No</p></td>") ;
    printf ("<td class=listhead width=60><p class=listhead>Operation</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead width=50 align=right><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead width=60 align=right><p class=listhead>Piece</p></td>") ;
    printf ("<td class=listhead width=60 align=right><p class=listhead>Time</p></td>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td width=10 class=listhead></td>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead><p class=listhead>Operator</p></td>") ;
    printf ("<td class=listhead width=100><p class=listhead>Date</p></td>") ;
    printf ("<td class=listhead width=50 align=right><p class=listhead>Quantity</p></td>") ;
    printf ("<td class=listhead width=50 align=right><p class=listhead>Time</p></td>") ;
    printf ("<td class=listhead width=50 align=right><p class=listhead>Used</p></td>") ;
    printf ("<td width=8 class=listhead></td>") ;
    printf ("</tr>\n") ;
    $sum = array() ;
    foreach ($Operation as $row) {
	// Calculate quantify for work done
	$WorkQuantity = 0 ;
	foreach ($row['work'] as $work) {
	    $WorkQuantity += $work['Quantity'] ;
	}
	
	// Output operation
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/operation.gif', navigationOnClickLink ('adjustquantity', $Record['Id']*100+$row['No'])) ;
	printf ("<td><p class=list>%d</p></td>", $row['No']) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Number'])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row['Description'])) ;
	printf ("<td align=right><p class=list>%d</p></td>", $row['Quantity']) ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $row['ProductionMinutes']) ;
	$sum['Piece'] += $row['ProductionMinutes'] ;
	printf ("<td align=right><p class=list>%01.2f</p></td>", $row['ProductionMinutes']*$row['Quantity']) ;
	$sum['Bundle'] += $row['ProductionMinutes']*$row['Quantity'] ;

	if ($row['Location']){
	    // Outsourced operation
	    printf ("<td class=list>%s</td>", ($row['InDate']) ? "<img class=list src='./image/toolbar/ok.gif' style='cursor:default;'>" : '') ;
	    printf ("<td></td>") ;
	    printf ("<td class=list><img class=list src='%s' style='cursor:default;'></td>", './image/toolbar/production.gif') ;
	    printf ("<td><p class=list>%s</p></td>", htmlentities(tableGetField('ProductionLocation','Name',$row['Location']))) ;
	    if ($row['InDate']) {
		printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i", dbDateDecode($row['InDate']))) ;
		printf ("<td align=right><p class=list>%d</p></td>", $row['Quantity']) ;
		printf ("<td align=right><p class=list>%01.2f</p></td>", $row['ProductionMinutes']*$row['Quantity']) ;
		$sum['WorkMinutes'] += $row['ProductionMinutes']*$row['Quantity'] ;
	    }

	} else {
    	    // Output work done by operator
	    printf ("<td class=list>%s</td>", ($WorkQuantity >= ((int)$row['Quantity'])) ? "<img class=list src='./image/toolbar/ok.gif' style='cursor:default;'>" : '') ;
	    $n = 0 ;
	    foreach ($row['work'] as $work) {
		if ($n > 0) {
		    printf ("</tr>\n") ;
		    printf ("<tr><td colspan=8></td>") ;
		}
		printf ("<td></td>") ;
		printf ("<td class=list><img class=list src='%s' %s></td>", './image/toolbar/user.gif', navigationOnClickLink ('adjustwork', $work['Id'])) ;
		printf ("<td><p class=list>%s</p></td>", htmlentities($work['UserName'])) ;
		printf ("<td><p class=list>%s</p></td>", date ("Y-m-d H:i", dbDateDecode($work['StartTime']))) ;
		printf ("<td align=right><p class=list>%d</p></td>", $work['Quantity']) ;
		printf ("<td align=right><p class=list>%01.2f</p></td>", $row['ProductionMinutes']*$work['Quantity']) ;
		$sum['WorkMinutes'] += $row['ProductionMinutes']*$work['Quantity'] ;
		printf ("<td align=right><p class=list>%01.2f</p></td>", $work['UsedMinutes']) ;
		$sum['Used'] += $work['UsedMinutes'] ;
		$n++ ;
	    }
	}
	printf ("</tr>\n") ;
    }
    if (count($Operation) == 0) {
        printf ("<tr>%s<td colspan=2><p class=list>No operations</p></td></tr>\n", dropmenuTDList()) ;
    } else {
	// Total
	printf ("<tr>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'><p class=list>Total</p></td>") ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Piece']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Bundle']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;' colspan=6></td>") ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['WorkMinutes']) ;
	printf ("<td align=right style='border-top: 1px solid #cdcabb;'><p class=list>%01.2f</p></td>", $sum['Used']) ;
	printf ("<td style='border-top: 1px solid #cdcabb;'></td>") ;
	printf ("</tr>\n") ;
       }
    printf ("</table>\n") ;

    return 0 ;
?>
