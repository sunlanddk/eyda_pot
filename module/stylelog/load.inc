<?php

    require_once 'lib/navigation.inc' ;

    define ('LINES', 40) ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Get Parent Record
	    $query = sprintf ("SELECT Style.*, Article.Number, Article.Description FROM Style LEFT JOIN Article ON Article.Id=Style.ArticleId WHERE Style.Id=%d AND Style.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    // Query records to list
	    $query = sprintf ("SELECT StyleLog.*, CONCAT(User.FirstName,' ',User.LastName,' (',User.Loginname,')') AS UserName FROM StyleLog LEFT JOIN User ON User.Id=StyleLog.CreateUserId WHERE StyleLog.StyleId=%d AND StyleLog.Active=1 ORDER BY StyleLog.Id DESC", $Id) ;
	    $Result = dbQuery ($query) ;

	    // Navigation
	    if ($Record['Ready']) {
		navigationPasive ('new') ;
	    }
	    return 0 ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Get Parent Record
	    $query = sprintf ("SELECT * FROM Style WHERE Id=%d AND Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }
	    
    if ($Id <= 0) return 0 ;

    // Get record
    $query = sprintf ("SELECT StyleLog.*, Style.Ready FROM StyleLog LEFT JOIN Style ON Style.Id=StyleLog.StyleId WHERE StyleLog.Id=%d AND StyleLog.Active=1", $Id) ;
    $Result = dbQuery ($query) ;
    $Record = dbFetch ($Result) ;
    dbQueryFree ($Result) ;

    // Validate time
    if ($Record['Ready'] or ((time() - dbDateDecode($Record['CreateDate'])) > 60*60*4)) {
	navigationPasive ('edit') ;
    }
    
    return 0 ;
?>
