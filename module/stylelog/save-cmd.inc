<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'Header'		=> array ('mandatory' => true),
	'Text'			=> array ()
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;
	    $fields['StyleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['StyleId'] = $Id ;
	    $Id = -1 ;
	    break ;

	default :
	    // Validate time
	    if ((time() - dbDateDecode($Record['CreateDate'])) > 60*60*4) return 'logs older than 4 hours can not be modified' ;
	    break ;	
    }
    
    if ($Record['Ready']) return 'no modification of Logs for Ready Style' ;
    return saveFields ('StyleLog', $Id, $fields) ;
     
    return 0 ;    
?>
