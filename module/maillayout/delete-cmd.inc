<?php

    if (!($Id > 0)) return "No Id supplied" ;
    if ($Record["Id"] != $Id) return "Record does not exist" ;

    require_once "lib/table.inc" ;

    tableDelete ("MailLayout", $Id) ;

    return 0
?>
