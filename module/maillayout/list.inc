<?php

    require_once "lib/navigation.inc" ;

    $query = sprintf ("SELECT MailLayout.* FROM MailLayout WHERE MailLayout.Active=1 ORDER BY MailLayout.Mark") ;
    $result = dbQuery ($query) ;

    printf ("<table class=list>\n") ;
    printf ("<tr>") ;
    printf ("<td width=23 class=listhead></td>") ;
    printf ("<td class=listhead width=350><p class=listhead>Description</p></td>") ;
    printf ("<td class=listhead><p class=listhead>Mark</p></td>") ;
    printf ("</tr>\n") ;
    while ($row = dbFetch($result)) {
        printf ("<tr>") ;
	printf ("<td class=list><img class=list src='%s' %s></td>", "image/toolbar/maillayout.gif", navigationOnClickLink ("edit", $row["Id"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Description"])) ;
	printf ("<td><p class=list>%s</p></td>", htmlentities($row["Mark"])) ;
	printf ("</tr>\n") ;
    }
    if (dbNumRows($result) == 0) {
        printf ("<tr><td></tr><td colspan=2><p class=list>No Layouts</p></td></tr>\n") ;
    }
    printf ("</table><br>\n") ;

    dbQueryFree ($result) ;

    return 0 ;
?>
