const url = weburl;
const appstore = new Vue({
  el: "#appRefund",
  data: {
    orderShopifyNumber: '',
    orderTrackingNumber: '',
    shopifyorder: '',
    view: {
      findOrder: true,
      scanItems: false,
      chooseFrom: false,
      chooseReason: false,
    },
    order: [],
    orderid: 0,
    line_items: [],
    reasons: [],
    scanned: [],
    reason: {
      id: 0,
      restock: 0,
      name: ''
    },
    manuelerror: '',
    chooseFrom: [],
    message: '',
    done: '',
    donesecond: '',
    url: url,
    preloader: false
  },
  beforeMount() {
    const vm = this;
    window.addEventListener("keyup", function (e) {
      if (e.keyCode == 8 || e.keyCode == 46) {
        vm.orderShopifyNumber = vm.orderShopifyNumber.substring(0, vm.orderShopifyNumber.length - 1);
      }
    });
    window.addEventListener("keypress", function (e) {
      vm.message = '';
      vm.done = '';
      vm.donesecond = '';
      var key = e.which;
      if (key == 13) {
        if (vm.orderShopifyNumber != '' && vm.view.findOrder == true) {
          vm.findOrder(vm.orderShopifyNumber);
          vm.orderShopifyNumber = '';
          return;
        }

        if (vm.orderShopifyNumber.substring(0, 6) == '99send') {
          // create refund record
          vm.CreateRefund();
          vm.orderShopifyNumber = '';
          return;
        }
        if (vm.orderShopifyNumber.substring(0, 6) == '99SEND') {
          // create refund record
          vm.CreateRefund();
          vm.orderShopifyNumber = '';
          return;
        }

        if (vm.orderShopifyNumber.substring(0, 6) == '99REAS' && vm.view.scanItems == true) {
          vm.setReason(parseInt(vm.orderShopifyNumber.substring(6, 8)));
          vm.orderShopifyNumber = '';
          return;
        }
        if (vm.orderShopifyNumber.substring(0, 6) == '99reas' && vm.view.scanItems == true) {
          vm.setReason(parseInt(vm.orderShopifyNumber.substring(6, 8)));
          vm.orderShopifyNumber = '';
          return;
        }
        if (vm.reason.id == 0) {
          vm.message = 'Please scan a reason before scanning variants.';
          vm.orderShopifyNumber = '';
          return;
        }

        if (vm.orderShopifyNumber != '' && vm.view.scanItems == true) {
          vm.checkVariant(vm.orderShopifyNumber);
          vm.orderShopifyNumber = '';
          return;
        }

        vm.orderShopifyNumber = '';

      } else {
        vm.orderShopifyNumber += String.fromCharCode(key);
      }
    });
  },
  methods: {
    reload() {
      location.reload();
    },
    removeitem: function (index) {
      this.items.splice(index, 1);
    },
    setReason(reasonId) {
      const self = this;
      self.reasons.forEach(function (e, i) {
        if (parseInt(e.Id) == parseInt(reasonId)) {
          self.reason.id = reasonId;
          self.reason.restock = e.Restock;
          self.reason.name = e.Name;
        }
      });

    },
    reset() {
      const vm = this;
      vm.view.findOrder = true;
      vm.order = [];
      vm.orderid = 0;
      vm.line_items = [];
      vm.preloader = false;
      vm.reasons = [];
      vm.scanned = [];
      vm.choosefrom = [];
      vm.shopifyorder = '';
      vm.orderShopifyNumber = '';
      vm.reason = {
        id: 0,
        restock: 0,
        name: ''
      };
      vm.view.scanItems = false;
      vm.view.choosefrom = false;
      vm.view.chooseReason = false;
    },
    CreateRefund() {
      const self = this;
      self.preloader = true;
      let data = {line_items: self.scanned, orderid: self.orderid, userid: $('.userid').val(), ws: $('.ws').val()};
      let path = window.location.pathname;
      if (self.scanned.length == 0) {
        self.preloader = false;
        self.message = 'Please scan items before creating.';
        return;
      }

      // self.postData('http://eyda.pot.dk/s4/public/refund/create/passon/refund', data, self.CreateRefundResponse);
      self.postData(weburlNew + 'refund/create/passon/refund', data, self.CreateRefundResponse);
    },
    CreateRefundResponse(response) {
      const self = this;
      self.done = 'Refund created.';
      self.reset();
    },
    noKeyboard() {
      const self = this;
      let tempArray = JSON.parse(JSON.stringify(self.line_items));
      if (self.reason.id == 0) {
        self.message = 'Please scan a reason before scanning variants.';
        self.orderShopifyNumber = '';
        return;
      }

      self.view.chooseFrom = true;
      self.manuelerror = '';
      self.manuelclose = true;
      self.chooseFrom = JSON.parse(JSON.stringify(tempArray));


    },
    closeChoosen() {
      const self = this;
      self.view.chooseFrom = false;
      self.chooseFrom = [];
    },
    checkVariant(variantcode) {
      const self = this;
      let tempArray = [];
      let oqId = 0;
      self.line_items.forEach(function (e, i) {
        if (e.VariantCode == variantcode) {
          if (parseInt(e.Quantity) >= (parseInt(e.RefundQuantity) + 1)) {
            // tempArray.push(JSON.parse(JSON.stringify(e)));
            tempArray.push(e);
            oqId = e.OrderQuantityId;
          }
        }
      });

      if (tempArray.length == 0) {
        self.message = 'Variantcode not found on this order. Or all quantity have already been returned on this variantcode.';
        return;
      }

      if (tempArray.length > 1) {
        self.view.chooseFrom = true;
        self.manuelerror = '';
        self.manuelclose = false;
        self.chooseFrom = tempArray;
        return;
      }

      // let alradyAdded = false;
      // self.scanned.forEach(function(e,i){
      // 	if(e.OrderQuantityId == oqId){
      // 		e.Scanned = (parseInt(e.Scanned) + 1);
      // 		alradyAdded = true;
      // 	}
      // });

      self.line_items.forEach(function (e, i) {
        if (e.VariantCode == variantcode) {
          if (parseInt(e.Quantity) >= (parseInt(e.RefundQuantity) + 1)) {
            e.RefundQuantity = (parseInt(e.RefundQuantity) + 1);
            e.Scanned++;
            element = JSON.parse(JSON.stringify(e));
            element.ReasonName = self.reason.name;
            element.ReasonId = self.reason.id;
            element.Restock = self.reason.restock;
            element.Scanned = 1;
            self.scanned.push(element);
          }
        }
      });


    },
    setLineActive(index) {
      const self = this;
      self.chooseFrom.forEach(function (e, i) {
        if (i == index) {
          e.active = true;
        } else {
          e.active = false;
        }

      });
    },
    addChoosen() {
      const self = this;
      let found = false;
      let oqId = 0;
      let alradyAdded = false;
      let element;
      let notenough = false;

      self.chooseFrom.forEach(function (e, i) {
        if (e.active == true) {
          if (parseInt(e.Quantity) >= (parseInt(e.RefundQuantity) + 1)) {
            e.Scanned++;
            element = JSON.parse(JSON.stringify(e));
            element.ReasonId = self.reason.id;
            element.Restock = self.reason.restock;
            element.ReasonName = self.reason.name;
            element.Scanned = 1;
            self.scanned.push(element);
            oqId = e.OrderQuantityId;
            found = true;
          } else {
            notenough = true;
          }
        }
      });

      if (notenough == true) {
        self.manuelerror = 'All quantity on line have already been scanned.';
        return;
      }

      if (found == false) {
        return;
      }

      // self.scanned.forEach(function(e,i){
      // 	if(e.OrderQuantityId == element.OrderQuantityId){
      // 		e.Scanned = (parseInt(e.Scanned) + 1);
      // 		alradyAdded = true;
      // 	}
      // });

      // if(alradyAdded == false){
      // 	self.scanned.push(element);
      // }

      self.line_items.forEach(function (e, i) {
        if (e.OrderQuantityId == oqId) {
          e.RefundQuantity = (parseInt(e.RefundQuantity) + 1);
        }
      });

      self.view.chooseFrom = false;
      self.chooseFrom = [];
    },
    findOrderResponse(response) {
      const self = this;
      self.orderid = response.orderId;
      self.order = response.order;
      self.line_items = response.line_items;
      self.reasons = response.reasons;
      self.shopifyorder = response.shopifyorder;
      self.scanned = [];
      self.choosefrom = [];
      self.preloader = false;
      self.view.findOrder = false;
      self.view.scanItems = true;
      self.view.choosefrom = false;
      self.view.chooseReason = false;
      self.reason = {
        id: 0,
        name: ''
      };
    },
    findOrder(orderShopifyNumber) {
      const self = this;
      let path = window.location.pathname;
      self.message = '';
      self.done = '';
      self.donesecond = '';
      self.preloader = true;
      self.getData(weburlNew + 'refund/find/order/' + encodeURIComponent(orderShopifyNumber), self.findOrderResponse);
      // self.getData('http://eyda.pot.dk/s4/public/refund/find/order/'+orderShopifyNumber, self.findOrderResponse);
    },
    postData(url, data, callback) {
      const vm = this;
      $.ajax({
        url: url,
        type: "POST",
        headers: {
          'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
          'X-Header-Userid': $('.userid').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        data: JSON.stringify(data),
        success: function (data, textStatus, jqXHR) {

          if (data.status == 'error') {
            vm.message = data.response;
            vm.preloader = false;
          } else {
            callback(data.response);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(errorThrown, textStatus, jqXHR);
          vm.message = 'An error occured, try by scanning again. Error message: ' + textStatus;
          console.log('Error: ');
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });


    },
    getData(url, callback) {
      const vm = this;
      $.ajax({
        url: url,
        type: "GET",
        headers: {
          'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
          'X-Header-Userid': $('.userid').val(),
          'Accept': 'application/json',
          'Content-Type': 'application/json'
        },
        success: function (data, textStatus, jqXHR) {

          if (data.status == 'error') {
            vm.message = data.response;
            vm.preloader = false;
          } else {
            callback(data.response);
          }
        },
        error: function (jqXHR, textStatus, errorThrown) {
          console.log(jqXHR);
          console.log(textStatus);
          console.log(errorThrown);
        }
      });


    }

  }
});



