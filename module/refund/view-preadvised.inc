<?php

require_once 'lib/table.inc';
require_once 'lib/item.inc';

function flag(&$Record, $field)
{
    if ($Record[$field]) {
        itemField(
            $field,
            sprintf(
                '%s, %s',
                date('Y-m-d H:i:s', dbDateDecode($Record[$field . 'Date'])),
                tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field . 'UserId'])
            )
        );
    } else {
        itemField($field, 'No');
    }
}

$vat = tableGetField('country', 'VATPercentage', (int)$Record['CountryId']);

itemStart();
    itemHeader();
    itemFieldIcon('Order', $Record['OrderId'], 'order.gif', 'orderview', $Record['OrderId']);
    itemField('Shopify Order', $Record['Reference']);
    itemFieldRaw('Tracking Code','&nbsp;&nbsp;&nbsp;<a href="https://gls-group.eu/DK/da/find-pakke/?match='.$Record['trackingCode'].'">' . $Record['trackingCode'].'</a>');
    itemField('Comment', $Record['Comment']);

    itemSpace();

    itemField('Name', $Record['AltCompanyName']);
    itemField('Email', $Record['Email']);
    itemField('Phone', $Record['Phone']);

    itemSpace();

    itemField('Address', $Record['AltCompanyName']);
    itemField('', $Record['Address1']);
    itemField('', $Record['Address2']);
    itemField('', $Record['ZIP'] . ' ' . $Record['City']);

    itemSpace();
    itemStart();
    itemSpace();
    itemSpace();

    itemInfo($Record);

    itemSpace();
    itemSpace();
itemEnd();

$query = sprintf(
    "
    SELECT 
        orderline.No,
        orderline.PriceSale,
        article.Description,
        variantcode.VariantCode,
        returnline.Quantity AS rtrnlnQuantity,
        returnline.Comment AS rtrnlnComment,
        refundreason2.Name AS CustomerRefundReasonName,
        clr.Description AS ColorName,
        artclsz.Name AS SizeName
    FROM `returnline`
        INNER JOIN orderline ON orderline.Id = returnline.orderLineId AND orderline.Active = 1
        INNER JOIN orderquantity ON orderquantity.OrderLineId = orderline.Id AND orderquantity.Active = 1
        INNER JOIN article ON article.Id = orderline.ArticleId AND article.Active = 1
        INNER JOIN variantcode ON variantcode.ArticleId = orderline.ArticleId
                              AND variantcode.ArticleColorId = orderline.ArticleColorId
                              AND variantcode.ArticleSizeId = orderquantity.ArticleSizeId
                              AND variantcode.Active = 1
        LEFT JOIN articlecolor artclclr ON artclclr.Id = orderline.ArticleColorId AND artclclr.Active = 1
        LEFT JOIN color clr ON clr.Id = artclclr.ColorId AND clr.Active = 1
        LEFT JOIN articlesize artclsz ON artclsz.Id = orderquantity.ArticleSizeId AND artclsz.Active = 1
        LEFT JOIN refundreason refundreason2 ON refundreason2.id = returnline.refundReasonId
                                            AND refundreason2.Active = 1
    WHERE returnline.ReturnId = %d
      AND returnline.Active = 1
    ",
    $Id
);
$result = dbQuery($query);

listStart();
    listHeader('Lines');
    listRow();
    listHead('Orderline', 30);
    listHead('Description', 200);
    listHead('Color', 120);
    listHead('Size', 60);
    listHead('VariantCode', 90);
    listHead('Customer Reason', 120);
    listHead('Customer Comment');
    listHead('Price', 40, 'align=center');
    listHead('Quantity', 50, 'align=center');
    listHead('',8);

    $totalQty = 0;
    $totalPrice = 0;

    while ($row = dbFetch($result)) {
        listRow();
        listField((int)$row['No']);
        listField($row['Description']);
        listField($row['ColorName']);
        listField($row['SizeName']);
        listField($row['VariantCode']);
        listField($row['CustomerRefundReasonName']);
        listField($row['rtrnlnComment']); // Customer Comment
        listField(($row['PriceSale'] * (1 + ($vat / 100))), 'align=center');
        listField(isset($row['rtrnlnQuantity']) ? $row['rtrnlnQuantity'] : 0, 'align=center'); // Customer Quantity

        $totalQty += (int)$row['rtrnlnQuantity'];
        $totalPrice += (float)($row['PriceSale'] * (1 + ($vat / 100)));
    }

    if (dbNumRows($result) == 0) {
        if ($Record['Type'] !== 'amount') {
            listRow();
            listField();
            listField('No OrderLines', 'colspan=2');
        } else {
            listRow();
            listField(1);
            listField('Specific amount returned');
            listField('');
            listField('');
            listField('');
            listField($Record['Note']);
            listField($Record['Note']);
            listField($Record['Amount'], 'align=right');
            listField(1, 'align=right');
        }
    } else {
        listRow();
        listField('Total');
        listField();
        listField();
        listField();
        listField();
        listField();
        listField();
        listField($totalPrice, 'align=center');
        listField($totalQty, 'align=center');
        listField('');
    }
listEnd();

function checkTransforStatus($row)
{
    if ((int)$row['cronId'] === 0) {
        return 'No status';
    }

    if ((int)$row['cronDone'] === 0) {
        return 'Awaiting export';
    }

    if ((string)$row['cronError'] !== '') {
        return 'Error in export';
    }

    return 'Exported';
}

return 0;
