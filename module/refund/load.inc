<?php

require_once 'lib/fieldtype.inc';
require_once 'lib/navigation.inc';
require_once 'lib/table.inc';
require_once 'lib/language.inc';

function checkTransforStatusLoad($row)
{
    if ((int)$row['cronId'] === 0) {
        return false;
    }

    if ((int)$row['cronDone'] === 0) {
        return true;
    }

    if ((string)$row['cronError'] !== '') {
        return false;
    }
    return false;
}

switch ($Navigation['Function']) {
    case 'reason-delete' :
        tableDelete('refundreason', $Id);
        navigationCommandMark('reasonlist');
        return 0;
    case 'reason-cmd' :
    case 'reason' :
        if ($Navigation['Parameters'] === 'new') {
            return 0;
        }

        $columns = [
            'name' => 'Name',
            'description' => 'Description'
        ];
        $languageColumns = [];
        foreach (getSupportedLanguages() as $languageId => $languageKey) {
            $languageColumns[] = sprintf(
                "(SELECT %s FROM refundreason_lang WHERE refundreason_id = refundreason.Id AND language_id = %d) AS %s%s",
                $columns['name'],
                $languageId,
                $columns['name'],
                $languageKey
            );
            $languageColumns[] = sprintf(
                "(SELECT %s FROM refundreason_lang WHERE refundreason_id = refundreason.Id AND language_id = %d) AS %s%s",
                $columns['description'],
                $languageId,
                $columns['description'],
                $languageKey
            );
        }
        $languageColumnsString = implode(", ", $languageColumns);

        $query = sprintf(
            "
			SELECT 
				refundreason.*,
				%s
			FROM refundreason
			WHERE refundreason.Id = %d",
            $languageColumnsString,
            $Id
        );

        $result = dbQuery($query);
        $Record = dbFetch($result);
        dbQueryFree($result);

        return 0;
    case 'refundamount' :
        // Query colors to list
        $query = sprintf('SELECT * FROM `order` WHERE Id=%d', $Id);
        $result = dbQuery($query);
        $Record = dbFetch($result);
        dbQueryFree($result);
        return 0;

    case 'giftcard-cmd' :
    case 'giftcard' :
    case 'refund-cmd' :
        // Query colors to list
        $query = sprintf('SELECT * FROM refund WHERE Id=%d', $Id);
        $result = dbQuery($query);
        $Record = dbFetch($result);
        dbQueryFree($result);
        return 0;

    case 'norefund-cmd' :
        $post = array(
            'Done' => 1,
            'DoneUserId' => $User['Id'],
            'DoneDate' => dbDateEncode(time()),
            'Type' => 'norefund'
        );
        tablewrite('refund', $post, $Id);
        navigationCommandMark('refundview', $Id);
        return 0;
    case 'delete-cmd' :
        tableDelete('Refund', $Id);
        navigationCommandMark('refundlist');
        return 0;

    case 'edit' :
    case 'edit-cmd' :
    case 'view' :
        $query = sprintf('SELECT r.*, o.Comment FROM (refund r, `order` o) WHERE  o.Id=r.OrderId and r.Id=%d AND r.Active=1', $Id);
        $result = dbQuery($query);
        $Record = dbFetch($result);
        dbQueryFree($result);

        $query = sprintf('SELECT id as cronId, done as cronDone, error as cronError FROM `cron_jobs` WHERE internalid=%d AND type="refund" ORDER BY id DESC LIMIT 1', $Id);
        $result = dbQuery($query);
        $cron = dbFetch($result);
        dbQueryFree($result);

        if ((int)$Record['Done'] === 1 || checkTransforStatusLoad($cron) === true) {
            navigationPasive('refundcmd');
            navigationPasive('refunddelete');
            navigationPasive('norefund');
            navigationPasive('refundgiftcardview');
            navigationPasive('refundedit');
        }

        $query = sprintf(
            "
        SELECT rl.*
        FROM (refundline rl) 
        WHERE rl.RefundId=%d AND rl.Active=1 AND rl.Refund=1",
            $Id
        );
        $lineres = dbQuery($query);
        if ((int)dbNumRows($lineres) === 0) {
            navigationPasive('refundcmd');
        }

        return 0;
    case 'reasonlist':
        // List field specification
        $fields = array(
            null, // index 0 reserved
            array('name' => 'Name', 'db' => 'r.Name'),
        );

        require_once 'lib/list.inc';
        $queryFields = 'r.Id, r.Name, r.Description, r.Restock, r.type';

        $queryTables = '(refundreason r) ';

        $queryClause = sprintf("r.Active=1");

        $Result = listLoad($fields, $queryFields, $queryTables, $queryClause);
        if (is_string($Result)) {
            return $Result;
        }

        $query =
            sprintf("SELECT r.Id, r.Name, r.Description, r.Restock, r.type FROM (refundreason r) WHERE r.Active=1");
        $Result2 = dbQuery($query);

        return listView($Result2, 'packlist');
    case 'listdone' :
        // List field specification
        $fields = array(
            null, // index 0 reserved
            array('name' => 'Order', 'db' => 'r.OrderId'),
            array('name' => 'ShopifyOrder', 'db' => 'o.Reference'),
            array('name' => 'Customer', 'db' => 'o.AltCompanyName'),
            array('name' => 'Comment', 'db' => 'o.Comment'),
            array(
                'name' => 'Type',
                'db' => 'r.Type',
                'type' => FieldType::SELECT,
                'options' => array('' => '- select -', 'full' => 'Full Refund', 'partial' => 'Partial Refund', 'norefund' => 'No Refund', 'giftcard' => 'Giftcard')
            ),
            array('name' => 'Date', 'db' => 'r.CreateDate')
        );

//if(po.Packed>0,"Done",if(po.ToId>0,if(s.ready>0,"Part shipped","Packing"),if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")))) as State,
        require_once 'lib/list.inc';

        $queryFields = 'r.Id, r.OrderId as OrderId, o.Reference as ShopifyOrder, o.Comment as Comment, o.AltCompanyName as Customer, r.Done, r.CreateDate, r.DoneDate, r.Type';

        $queryTables = '(refund r, `order` o) ';

        $queryClause = sprintf("r.Active=1 AND o.Id=r.OrderId AND o.Active=1 AND r.Done=1");

        $Result = listLoad($fields, $queryFields, $queryTables, $queryClause);
        if (is_string($Result)) {
            return $Result;
        }

        $query =
            sprintf(
                "SELECT r.Id, r.OrderId as OrderId, o.Reference as ShopifyOrder, o.AltCompanyName as Customer, r.Done, r.CreateDate, r.Type FROM (refund r, `order` o) WHERE r.Active=1 AND o.Id=r.OrderId AND o.Active=1  AND r.Done=1 ORDER BY r.CreateDate"
            );
        $Result2 = dbQuery($query);

        return listView($Result2, 'packlist');
    case 'list-preadvised' :
        // List field specification
        $fields = array(
            null, // index 0 reserved
            array('name' => 'ShopifyOrder', 'db' => 'o.Reference'),
            array('name' => 'Traking number', 'db' => 'rtrn.trackingCode'),
            array('name' => 'Customer', 'db' => 'o.AltCompanyName'),
//            array('name' => 'Comment', 'db' => 'o.Comment'),
            array('name' => 'Return Date', 'db' => 'rtrn.CreateDate'),
            array('name' => 'Id', 'db' => 'rtrn.Id', 'nofilter' => true),
            array('name' => 'Order Id', 'db' => 'o.Id')
        );

        require_once 'lib/list.inc';

        $queryFields = 'o.Id as OrderId, o.Reference as ShopifyOrder, o.email AS OrderEmail, rtrn.id as Id, rtrn.emailSendDate as ReturnEmailSendDate, rtrn.trackingCode,
                        rtrn.email as ReturnEmail, rtrn.emailLog as ReturnEmailLog, o.Comment as Comment, o.AltCompanyName as Customer, rtrn.CreateDate';

        $queryTables = "(`return` rtrn, `order` o) ";

        $queryClause = "o.Id=rtrn.orderId AND rtrn.active AND o.Active=1 AND rtrn.refundId IS NULL";

        $Result = listLoad($fields, $queryFields, $queryTables, $queryClause);
        if (is_string($Result)) {
            return $Result;
        }

        $query =
            sprintf(
                "SELECT r.Id, r.OrderId as OrderId, o.Reference as ShopifyOrder, o.AltCompanyName as Customer, r.Done, r.CreateDate, r.Type FROM (refund r, `order` o) WHERE r.Active=1 AND o.Id=r.OrderId AND o.Active=1  AND r.Done=0 ORDER BY r.CreateDate"
            );
        $Result2 = dbQuery($query);

        return listView($Result2, 'packlist');
    case 'batch-list-cmd' :
    case 'batch-list' :
    case 'list' :
        $consistencyField = '(
            SELECT 
                CASE
                    WHEN MAX(rtrn.id) IS NULL THEN "empty"
                    WHEN COUNT(DISTINCT rtrnln.id) > COUNT(DISTINCT rl.id) THEN "more"
                    WHEN COUNT(DISTINCT rtrnln.id) < COUNT(DISTINCT rl.id) THEN "less"
                    ELSE "equal"
                END AS comparison_result
            FROM refundline rl
            LEFT JOIN orderquantity oq ON oq.Id = rl.OrderQuantityId AND oq.Active = 1
            LEFT JOIN `return` rtrn ON rtrn.refundId = rl.refundid AND rtrn.active = 1
            LEFT JOIN returnline rtrnln ON rtrnln.returnId = rtrn.id AND rtrnln.orderLineId = oq.orderlineid AND rtrnln.active = 1
            WHERE rl.Active = 1 AND rl.refundid = r.id
            GROUP BY rl.refundid
        )';

        // List field specification
        $fields = array(
            null, // index 0 reserved
            array('name' => 'Id', 'db' => 'r.Id', 'nofilter' => true),
            array('name' => 'Order', 'db' => 'r.OrderId'),
            array('name' => 'ShopifyOrder', 'db' => 'o.Reference'),
            array('name' => 'Customer', 'db' => 'o.AltCompanyName'),
            array('name' => 'Comment', 'db' => 'o.Comment'),
            array(
                'name' => 'Type',
                'db' => 'r.Type',
                'type' => FieldType::SELECT,
                'options' => array('' => '- select -', 'full' => 'Full Refund', 'partial' => 'Partial Refund', 'norefund' => 'No Refund', 'giftcard' => 'Giftcard')
            ),
            array('name' => 'Date', 'db' => 'r.CreateDate'),
            [
                'name' => 'Consistency',
                'db' => $consistencyField,
                'type' => FieldType::SELECT,
                'options' => [
                    '' => '- select -',
                    'empty' => 'No preadvice for received quantity',
                    'more' => 'More preadvise than received quantity',
                    'less' => 'More received quantity than preadvise',
                    'equal' => 'Similar preadvise and received quantity'
                ]
            ]
        );

//if(po.Packed>0,"Done",if(po.ToId>0,if(s.ready>0,"Part shipped","Packing"),if (po.printed,"Printed",if(po.PickUserId=0,"","Assigned")))) as State,
        require_once 'lib/list.inc';
        $queryFields = "r.Id,
            r.OrderId AS OrderId,
            o.Reference AS ShopifyOrder,
            o.Comment AS Comment,
            o.AltCompanyName AS Customer,
            r.Done,
            r.CreateDate,
            r.Type,
            cj.id AS cronId,
            cj.done AS cronDone,
            cj.error AS cronError,
            $consistencyField AS Consistency
        ";

        $queryTables = sprintf("(refund r, `order` o) LEFT JOIN cron_jobs cj ON cj.id=(select MAX(c.id) as id from cron_jobs c WHERE c.type='refund' AND c.internalid=r.id)");

        $queryClause = sprintf("r.Active=1 AND o.Id=r.OrderId AND o.Active=1 AND r.Done=0");

        $Result = listLoad($fields, $queryFields, $queryTables, $queryClause);
        if (is_string($Result)) {
            return $Result;
        }

        $query =
            sprintf(
                "SELECT r.Id, r.OrderId as OrderId, o.Reference as ShopifyOrder, o.AltCompanyName as Customer, r.Done, r.CreateDate, r.Type FROM (refund r, `order` o) WHERE r.Active=1 AND o.Id=r.OrderId AND o.Active=1  AND r.Done=0 ORDER BY r.CreateDate"
            );
        $Result2 = dbQuery($query);

        return listView($Result2, 'packlist');
    case 'preadvised-email' :
    case 'view-preadvised' :
        $query = sprintf(
            "SELECT
                    rtrn.*,
                    ordr.id AS OrderId,
                    ordr.reference as Reference,
                    ordr.email AS OrderEmail,
                    ordr.Comment AS Comment,
                    ordr.AltCompanyName AS AltCompanyName,
                    ordr.Phone AS Phone,
                    ordr.Address1 AS Address1,
                    ordr.Address2 AS Address2,
                    ordr.ZIP AS ZIP,
                    ordr.City AS City,
                    ordr.CountryId AS CountryId
                FROM `return` rtrn
                INNER JOIN `order` ordr ON ordr.Id = rtrn.orderId
                WHERE rtrn.id=%d AND rtrn.active = 1",
            (int)$Id
        );
        $result = dbQuery($query);
        $Record = dbFetch($result);
        dbQueryFree($result);

        return 0;
    default:
        break;
}

return 0;
