
<input class="userid" type="hidden" value="<?php echo $User['Id']; ?>">
<table class="item">
	<tbody>
		<tr><td class="itemlabel">Order</td><td class="itemfield"><?php echo $Id; ?></td></tr>
		<tr><td class="itemlabel">Shopify Order</td><td class="itemfield"><?php echo $Record['Reference']; ?></td></tr>
		<tr><td class="itemspace"></td></tr>
	</tbody>
</table>

<table class="item">
	<tbody>
		<tr><td class="itemlabel">Notify Customer</td><td>
			<input class="notify" type="checkbox" checked ></td></tr>
		<tr><td class="itemspace"></td></tr>
		<tr><td class="itemlabel">Note</td><td>
			<input class="shopifynote" type="text" value="" maxlength="100" size="100" style="width:100%;"></td></tr>
		<tr><td class="itemspace"></td></tr>
		<tr><td class="itemlabel">Amount</td><td>
			<input class="amount" type="number" value="0.00" maxlength="100" size="100" style="width:100%;"></td></tr>
		<tr><td class="itemspace"></td></tr>
	</tbody>
</table>

<center class="loaderContainer" style="display:none;"><div class="loader"></div></center>

<div class="returnText">
	
</div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js?v=4"></script>

<script>
	

	function refundOrder(){
		if($('.note').val() == '' || $('.shopifynote').val() == '' || ($('.amount').val() == '' || $('.amount').val() == '0.00')){
			alert('Note, shopify note and amount needs to be enteret.');
			return;
		}

		let path = window.location.pathname;
		let postdata = {shopname: "<?php echo tableGetField('shopifyshops', 'Name', $Record['WebShopNo']) ?>", orderid: "<?php echo $Id ?>", shopifyorderid: "<?php echo $Record['ShopifyId'] ?>", note: $('.shopifynote').val(), amount: $('.amount').val(), notify: $('.notify')[0].checked};
		// console.log(JSON.stringify(postdata));
		// return;
		$.ajax({
             url: weburlNew+'refund/shopify/specific/amount',
            type: 'POST',
            data: JSON.stringify(postdata),
            host: 'passon.dk',
            headers: {
                'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
                'Access-Control-Allow-Origin': '*',
                'X-Header-Userid': $('.userid').val(),
            },
            success: function (result) {
            	window.opener.location.reload(true);
				window.close();
            	return;
            },
            error: function (error) {
                $('.loaderContainer').hide();
				$('.returnText').text(JSON.stringify(error));
            }
        });
	}

	function refundOrderResponse(response){
		$('.loaderContainer').hide();
		$('.returnText').text(response.response);
		window.close();
	}


	$(document).ready(function(){
		// refundOrder();
	});

	

</script>

<style>
	.loader {
		  border: 10px solid #f3f3f3; /* Light grey */
		  border-top: 10px solid #3498db; /* Blue */
		  border-radius: 50%;
		  width: 60px;
		  height: 60px;
		  animation: spin 2s linear infinite;
		}

		@keyframes spin {
		  0% { transform: rotate(0deg); }
		  100% { transform: rotate(360deg); }
		}
</style>

<?php 


return 0;