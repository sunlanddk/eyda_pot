<?php
    // require_once '/Users/alex/Documents/Sunland/Webdevelopment/PassOn/Original/eyda/lib/config.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

    $_wsid = (int)TableGetField('login','WorkstationId',$Login['Id']) ;
    if ($_wsid>0) {
        $_wsname = TableGetField('Workstation','Name',$_wsid) ;
        printf('<div id="workStationMsg" style="color:black;font-size:24px">&nbsp;&nbsp; Labels printed on: ' . $_wsname . '</div>') ;
    } else {
        printf('<div id="workStationMsg" style="color:black;font-size:24px">&nbsp;&nbsp;Select Workstation to print labels</div>') ;
        $_wsname = 'none' ;
    }
    ?>
    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
    <!-- <script src="http://eyda.pot.dk/eyda/lib/vue.js"></script> -->

    <div id="appRefund">
        <input class="userid" type="hidden" value="<?php echo $User['Id']; ?>">
        <input class="ws" type="hidden" value="<?php echo $_wsname; ?>">
        <!-- <input class="userid" type="hidden" value="214"> -->
        <p class="hiddenkeyboard">{{str}}</p>
        <center>
            <h3 style="color: red">{{message}}</h3>
            <h3 style="color: green">{{done}}</h3>
            <h3 style="color: green">{{donesecond}}</h3>
        </center>
        <center>
            <label for="shopifyOrder"><span>Single item return</span>
                <input name="shopifyOrder" type="text" placeholder="" v-model="str" readonly>
            </label>
            <div class="button" @click="returnItem(str)">return</div>
        </center>
        <div v-if="preloader" class="preloader"><div class="rotator"></div></div>
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js?v=3"></script>
<script src="<?php echo _LEGACY_URI; ?>/module/refund/simpleRefund.js?v=9"></script>

<!-- <script src="http://eyda.pot.dk/eyda/js/jquery/jquery-1.9.1.min.js?v=2"></script>
<script src="http://eyda.pot.dk/eyda/lib/config.js?v=2"></script>
<script src="http://eyda.pot.dk/eyda/module/refund/newRefund.js?v=1"></script> -->
<style>
    #appRefund{
        position: relative;
        margin-top: 50px;
    }
    #appRefund table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 2px;
        margin: 5px 0 0 0;
        /*float: left;*/
        display: inline-block;
        cursor: pointer;
    }
    .form{
        text-align: center;
    }
    h3{
        font-size: 15px;
    }
    h2{
        font-size: 18px;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .preloader{
        position: fixed;
        z-index: 99999999;
        left: 0;
        top: 0;
        background: rgba(255, 255, 255, 0.7);
        width: 100%;
        height: 100%;
    }
    .preloader .rotator{
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border: 5px solid #3b9ac4;
        border-radius: 50%;
        border-top: 5px solid #01205a;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
    select{
        padding: 10px 10px;
        border: solid 1px black;
        background: grey;
        color:  white;
        margin: 10px 0;
        border-radius: 10px;
    }
    label span{

    }
    input{
        font-size: 15px;
        text-align: center;
        margin: 5px 0 0 0;
        display: block;
        padding:  10px;
    }
    .chooseFrom{
        position: fixed;
        top: 0;
        left: 0;
        width: 100%;
        height: 100%;
        background: rgba(0,0,0,0.7);
    }
    .chooseFrom .container{
        width: 500px;
        height: auto;
        margin: auto;
        margin-top: 100px;
        background: white;
        border-radius: 2px;
        padding: 15px;
        max-height: calc(100vh - 100px - 50px);
        overflow: scroll;
    }
    .container table{
        width: 100%;
        border-collapse: collapse;
    }
    table th{
        text-align: left;
        border-bottom: solid black 2px;
        padding:  10px;
    }
    td{
        border-color: white;
        padding:  10px;
    }
    tr{
        cursor: pointer;
    }
    tr.active{
        background: #00ff98 !important;
    }
    table tr:nth-child(even){
        background: lightgrey;
    }
    center .container{
        max-width: 750px;
        text-align: left;
    }
    center table{
        text-align: left;
    }
</style>
<?php

    return 0 ;

?>
