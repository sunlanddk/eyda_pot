<?php

require_once 'lib/table.inc';
require_once 'lib/item.inc';
require_once 'lib/form.inc';
require_once 'lib/language.inc';

// Form
formStart();
    formCalendar();
    itemStart();
        itemHeader();

        if ((int)$Record['Restock'] === 1) {
            itemFieldRaw('Restock', '<select name="Restock"><option value="1" selected>Yes</option><option value="0">No</option></select>');
        } else {
            itemFieldRaw('Restock', '<select name="Restock"><option value="1">Yes</option><option selected value="0">No</option></select>');
        }

        $types = ['customer', 'warehouse'];
        $type = $Record['type'];
        $typeOptions = '';
        foreach ($types as $t) {
            if ($t === $type) {
                $typeOptions .= '<option selected value="' . $t . '">' . ucfirst($t) . '</option>';
            } else {
                $typeOptions .= '<option value="' . $t . '">' . ucfirst($t) . '</option>';
            }
        }
        itemFieldRaw('Type', '<select name="type">' . $typeOptions . '</select>');

        itemSpace();
        itemFieldRaw('Name', formText("Name[0]", $Record['Name'], 100));
        itemFieldRaw('Description', formText("Description[0]", $Record['Description'], 100));
        itemSpace();

        if (!empty($Record)) {
            foreach (getSupportedLanguages() as $languageId => $languageKey) {
                $value = $Record['Name' . $languageKey];
                itemFieldRaw($languageKey . ' name', formText("Name[$languageId]", $value, 100));

                $value = $Record['Description' . $languageKey];
                itemFieldRaw($languageKey . ' Description', formText("Description[$languageId]", $value, 100));

                itemSpace();
            }
        }

        itemSpace();
    itemEnd();
formEnd();

return 0;
