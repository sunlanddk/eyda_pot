<?php

require_once 'lib/save.inc';
require_once 'lib/navigation.inc';

$fields = array(
    'Name' => array('mandatory' => true),
    'Description' => array(),
    'Restock' => array('type' => 'integer', 'mandatory' => true),
    'type' => array('mandatory' => true, 'values' => ['customer', 'warehouse']),
);

function checkfield($fieldname, $value, $changed)
{
    return true;
}

if ($Navigation['Parameters'] === 'new') {
    unset ($Record);
    $Id = -1;

    $_POST['Name'] = !empty($_POST['Name'][0]) ? $_POST['Name'][0] : array_shift($_POST['Name']);
    $_POST['Description'] = !empty($_POST['Description'][0]) ? $_POST['Description'][0] : array_shift($_POST['Description']);
}

$result = saveFields('refundreason', $Id, $fields, true);
if (($Id === -1) && $result) {
    return $result;
}

foreach ($_POST['Name'] as $key => $value) {
    if ($key === 0) {
        continue;
    }

    $refundreasonLangId = tableGetFieldWhere('refundreason_lang', 'id', 'language_id=' . $key . ' and refundreason_id=' . $Id);
    if ($refundreasonLangId > 0) {
        $_query = sprintf('Update refundreason_lang set name="%s" where id=%d', $value, $refundreasonLangId);
    } else {
        $_query = sprintf('Insert into refundreason_lang set refundreason_id=%d, name="%s", language_id=%d', $Id, $value, $key);
    }
    dbQuery($_query);
}

foreach ($_POST['Description'] as $key => $value) {
    if ($key === 0) {
        continue;
    }

    $refundreasonLangId = tableGetFieldWhere('refundreason_lang', 'id', 'language_id=' . $key . ' and refundreason_id=' . $Id);
    if ($refundreasonLangId > 0) {
        $_query = sprintf('Update refundreason_lang set description="%s" where id=%d', $value, $refundreasonLangId);
    } else {
        $_query = sprintf('Insert into refundreason_lang set refundreason_id=%d, description="%s", language_id=%d', $Id, $value, $key);
    }
    dbQuery($_query);
}

$_POST['Name'] = array_shift($_POST['Name']);
$_POST['Description'] = array_shift($_POST['Description']);
$result = saveFields('refundreason', $Id, $fields, true);

return 0;
