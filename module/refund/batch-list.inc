<script type="text/javascript" >
	function CheckAll () {
		var col = document.appform.elements ;
		var n ;
		let count = 0;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
//			if (!e.name.indexOf('UpdateSetFlag')) continue ;
			e.checked = true ;
			count ++;
		}
		$('.totalselected span').text(count);
	}
	function CheckAllOnWebshop () {
		var col = document.appform.elements ;
		var n ;
		let count = 0;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			if (!e.name.indexOf('UpdateSetFlag')) continue ;
			e.checked = true ;
			count ++;
		}
		$('.totalselected span').text(count);
	}
	function ClearAll () {
		var col = document.appform.elements ;
		var n ;
		for (n = 0 ; n < col.length ; n++) {
			var e = col[n] ;
			if (e.type != 'checkbox') continue ;
			e.checked = false ;
		}
		$('.totalselected span').text(0);
	}
	function checkCount(){
	console.log('asdasd');
		$('input').on('change', function(){
			$('.totalselected span').text($('input:checked').length);
		});
	}
</script>
<div class="totalselected">Total selected ( <span>0</span> )</div>
<?php
    require_once 'module/style/include.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/form.inc' ;
	
    formStart () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Select', 20) ;
    listHead ('Id', 40) ;
	listHead ('Order', 40) ;
	listHead ('Shopify Order', 80) ;
	listHead ('Customer', 200) ;
	listHead ('Comment', 250) ;
	listHead ('Value', 80,'align="right"') ;
//	listHead ('Type', 30) ;
	listHead ('Date', 140) ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
    	if(checkTransforStatus($row) === true){
			listRow () ;
			listFieldRaw (formCheckbox (sprintf('UpdateSetFlag[%d]', (int)$row['Id']), 0, '')) ;
	    	listField ($row['Id']) ;
			listField ($row['OrderId']) ;
			listField ($row['ShopifyOrder']) ;
			listField ($row['Customer']) ;
			listField ($row['Comment']) ;
		   $query = sprintf ("
		        SELECT sum(rl.Quantity) as Quantity, sum(1.25*ol.PriceSale) as PriceSale
		        FROM (refundline rl, orderquantity oq, orderline ol) 
		        WHERE rl.RefundId=%d AND rl.Active=1 AND oq.Id=rl.OrderQuantityId AND ol.Id=oq.OrderLineId ", $row['Id']) ;
		   $result = dbQuery ($query) ;
		   $_row = dbFetch($result) ;
		   listField(number_format($_row['PriceSale'],2),'align="right"') ;
//			listField (getTypeRefund($row['Type'])) ;
			listField (date('Y-m-d H:i:s', dbDateDecode ($row['CreateDate']))) ;
		}
    }
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No refunds', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;
    formEnd () ;

    function getTypeRefund($type){
		switch ($type) {
			case 'norefund':
				return 'No Refund';
				break;
			case 'giftcard':
				return 'Giftcard';
				break;
			case 'full':
				return 'Full refund';
				break;
			case 'partial':
				return 'Partial refund';
				break;
			default:
				// code...
				break;
		}
	}

	function checkTransforStatus($row){
		if((int)$row['cronId'] === 0){
			return true;
		}

		if((int)$row['cronDone'] === 0){
			return false;
		}

		if((string)$row['cronError'] !== ''){
			return true;			
		}

		return true;
	}

	?>

	<script>

	 $('document').ready(function(){
	 	checkCount();
	 });
	</script>

	<?php 

    return 0 ;
?>
