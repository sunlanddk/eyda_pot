<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;


       // Filter Bar
	listFilterBar () ;

	listStart () ;
		listRow () ;
		listHead ('', 20) ;
		listHead ('Id', 40) ;
		listHead ('Order', 40) ;
		listHead ('Shopify Order', 80) ;
		listHead ('Customer', 200) ;
		listHead ('Comment', 250) ;
		listHead ('Type', 120) ;
		listHead ('Value', 80,'align="right"') ;
		listHead ('Create Date', 140) ;
		listHead ('Refund Date', 140) ;
		listHead ('') ;
		while ($row = dbFetch($Result)) {
			listRow () ;
			listFieldIcon ($Navigation['Icon'], 'refundview', (int)$row['Id']) ;
			listField ($row['Id']) ;
			listField ($row['OrderId']) ;
			listField ($row['ShopifyOrder']) ;
			listField ($row['Customer']) ;
			listField ($row['Comment']) ;
			listField (getTypeRefund($row['Type'])) ;
		   $query = sprintf ("
		        SELECT sum(rl.Quantity) as Quantity, sum(1.25*ol.PriceSale) as PriceSale
		        FROM (refundline rl, orderquantity oq, orderline ol) 
		        WHERE rl.RefundId=%d AND rl.Active=1 AND oq.Id=rl.OrderQuantityId AND ol.Id=oq.OrderLineId ", $row['Id']) ;
		   $result = dbQuery ($query) ;
		   $_row = dbFetch($result) ;
		   listField(number_format(($row['Type'] == 'amount' ? $row['Amount'] : $_row['PriceSale']),2),'align="right"') ;
			listField (date('Y-m-d H:i:s', dbDateDecode ($row['CreateDate']))) ;
			listField (date('Y-m-d H:i:s', dbDateDecode ($row['DoneDate']))) ;
			listField () ;
		}
		if (dbNumRows($Result) == 0) {
			listRow () ;
			listField () ;
			listField ('No active Refunds', 'colspan=2') ;
		}
	listEnd () ;

	dbQueryFree ($Result) ;


	function getTypeRefund($type){
		switch ($type) {
			case 'norefund':
				return 'No Refund';
				break;
			case 'giftcard':
				return 'Giftcard';
				break;
			case 'full':
				return 'Full refund';
				break;
			case 'partial':
				return 'Partial refund';
				break;
			case 'amount':
				return 'Specific amount';
				break;
			default:
				// code...
				break;
		}
	}

	return 0 ;
?>
