<div class="returnText" style="color:red"></div>

<?php

require_once 'lib/item.inc';
require_once 'lib/form.inc';
require_once 'lib/table.inc';

// in cmd: nl2br($_POST['Body']) . '<br>'
//'Please confirm the attached order confirmation from Eyda to your sales agent.
$BodyTxt = tableGetFieldWhere('maillayout', 'Body', 'Mark="preadvisedresendemail"');

// Header
itemStart();
itemSpace();
itemField('ShopifyOrder', (int)$Record['Reference']);
itemField('Order Id', (int)$Record['OrderId']);
itemField('Return Id', (int)$Id);
//itemField('Customer', $Record['CompanyName']);
//itemField('Description', $Record['Description']);
itemSpace();
itemEnd();

formStart();
itemStart();
itemHeader();

//if ($Record['Ready']) {
//    itemField(
//        'Confirmed',
//        sprintf(
//            '%s, %s',
//            date('Y-m-d H:i:s', dbDateDecode($Record['ReadyDate'])),
//            tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['ReadyUserId'])
//        )
//    );
//}
//itemFieldRaw(
//    $Record['Ready'] ? '' : 'Confirm',
//    formCheckbox(
//        'Ready',
//        $Record['Ready'] ? 1 : $_ready,
//        '',
//        $Record['Ready'] ? 'Hidden' : sprintf('onchange="appLoad(%d,%d,\'ready=\'+%d);"', (int)$Navigation['Id'], $Id, $_ready)
//    )
//);
itemSpace();

itemFieldRaw('Order Email', formText('OrderMail', $Record['OrderEmail'], 100, 'width:100%;', 'disabled'));
itemSpace();
itemFieldRaw('Return Email', formText('ReturnMail', !empty($Record['email']) ? $Record['email'] : $Record['OrderEmail'], 100, 'width:100%;'));

itemSpace();
itemFieldRaw('BodyText', $BodyTxt);

itemInfo($Record);
itemEnd();

formEnd();
?>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js?v=3"></script>
<script type='text/javascript'>
    function resendEmail() {
      const requestData = {
        returnId: "<?php echo $Id ?>",
        returnEmail: $("input[name=ReturnMail]").val()
      };

      $.ajax({
        url: `${weburlNew}api/return/label/send-email`,
        type: 'POST',
        data: JSON.stringify(requestData),
        host: window.location.hostname,
        headers: {
          'Access-Control-Allow-Origin': '*',
          'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
          'X-Header-Userid': $('.userid').val(),
        },
        success: function (response) {
          window.opener.location.reload(true);
          window.close();
        },
        error: function (error) {
          const errors = error.responseText ? JSON.parse(error.responseText) : {};
          $('.returnText').text(errors.errors[0].message);
        }
      });
    }
</script>

<?php
return 0;
