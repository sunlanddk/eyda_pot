<?php

require_once 'lib/table.inc';
require_once 'lib/item.inc';


// Refund ID
$refundId = (int)$Id;
// Order ID
$orderId = (int)$Record['OrderId'];

$query = sprintf(
    '
            SELECT 
                o.*,
                rtrn.fundsType AS returnFundsType
            FROM `order` o
            LEFT JOIN `return` rtrn ON rtrn.orderId = o.Id AND rtrn.refundId = %d AND rtrn.active = 1
            WHERE o.Id=%d AND o.Active = 1
        ',
    $refundId,
    $orderId
);
$result = dbQuery($query);
$order = dbFetch($result);
dbQueryFree($result);

$query = sprintf('SELECT id as cronId, done as cronDone, error as cronError FROM `cron_jobs` WHERE internalid=%d AND type="refund" ORDER BY id DESC LIMIT 1', $refundId);
$result = dbQuery($query);
$cron = dbFetch($result);
dbQueryFree($result);

function flag(&$Record, $field)
{
    if ($Record[$field]) {
        itemField(
            $field,
            sprintf(
                '%s, %s',
                date('Y-m-d H:i:s', dbDateDecode($Record[$field . 'Date'])),
                tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field . 'UserId'])
            )
        );
    } else {
        itemField($field, 'No');
    }
}

$vat = tableGetField('country', 'VATPercentage', (int)$order['CountryId']);

itemStart();
itemHeader();
itemFieldIcon('Order', $order['Id'], 'order.gif', 'orderview', $order['Id']);
itemField('Shopify Order', $order['Reference']);
itemField('Comment', $order['Comment']);

if ((int)$Record['Done'] === 1) {
    if (in_array($Record['Type'], array('full', 'partial')) === true) {
        itemSpace();
        itemField('Type', ucfirst($Record['Type']) . ' Refund');
        itemSpace();
        itemField('Shopify Refund Id', $Record['ShopifyRefundId']);
    } else {
        itemSpace();
        if ($Record['Type'] == 'norefund') {
            itemField('Type', 'No refund');
        }
        if ($Record['Type'] == 'amount') {
            itemField('Type', 'Specific amount');
        } else {
            itemField('Type', 'Giftcard');
        }
    }
} else {
    itemSpace();
    itemField('Type', ucfirst($Record['Type']) . ' Refund');
    itemField('Transfer', checkTransforStatus($cron));
    if ((string)$cron['cronError'] !== '') {
        itemField('Message', $cron['cronError']);
    }
}

itemSpace();

itemField('Name', $order['AltCompanyName']);
itemField('Email', $order['Email']);
itemField('Phone', $order['Phone']);

itemSpace();

itemField('Address', $order['AltCompanyName']);
itemField('', $order['Address1']);
itemField('', $order['Address2']);
itemField('', $order['ZIP'] . ' ' . $order['City']);

itemSpace();
itemStart();
itemSpace();
itemSpace();

if (isset($order['returnFundsType'])) {
    itemField(
        'Return Funds Type',
        ucfirst(str_replace('_', ' ', ucwords($order['returnFundsType'], '_')))
    );
    itemSpace();
}

flag($Record, 'Ready');
flag($Record, 'Done');

itemInfo($Record);

itemSpace();
itemSpace();
itemEnd();

$query = sprintf(
    "
            SELECT 
                rl.Quantity,
                ol.No,
                rr.Name AS RefundReasonName,
                a.Description,
                v.VariantCode,
                ol.PriceSale,
                rl.Refund,
                clr.Description AS ColorName,
                artclsz.Name AS SizeName,
                rtrnln.quantity AS rtrnlnQuantity,
                rtrnln.comment AS rtrnlnComment,
                rfndrsn.Name AS rfndrsnRefundReasonName
            FROM refundline rl
                JOIN orderquantity oq ON oq.Id = rl.OrderQuantityId AND oq.Active = 1
                JOIN orderline ol ON ol.Id = oq.OrderLineId AND ol.Active = 1
                JOIN refundreason rr ON rr.Id = rl.ReasonId AND rr.Active = 1
                JOIN article a ON a.Id = ol.ArticleId AND a.Active = 1
                JOIN variantcode v ON v.ArticleId = ol.ArticleId
                                  AND v.ArticleColorId = ol.ArticleColorId
                                  AND v.ArticleSizeId = oq.ArticleSizeId
                                  AND v.Active = 1
                LEFT JOIN articlecolor artclclr ON artclclr.Id = ol.ArticleColorId AND artclclr.Active = 1
                LEFT JOIN color clr ON clr.Id = artclclr.ColorId AND clr.Active = 1
                LEFT JOIN articlesize artclsz ON artclsz.Id = oq.ArticleSizeId AND artclsz.Active = 1
                LEFT JOIN `return` rtrn ON rtrn.refundId = %d
                                       AND rtrn.orderId = ol.orderId
                                       AND rtrn.active = 1
                LEFT JOIN `returnline` rtrnln ON rtrnln.returnId = rtrn.id
                                             AND rtrnln.orderLineId = ol.id
                                             AND rtrnln.active = 1
                LEFT JOIN `refundreason` rfndrsn ON rfndrsn.id = rtrnln.refundReasonId
                                                AND rfndrsn.Active = 1
            WHERE rl.RefundId = %d
              AND rl.Active = 1
          ",
    $refundId,
    $refundId
);
$resultRefundLines = dbQuery($query);

listStart();
    listHeader('Lines');
    listRow();
    listHead('Orderline', 30);
    listHead('Description', 75);
    listHead('Color', 50);
    listHead('Size', 30);
    listHead('VariantCode', 90);
    listHead('Warehouse Reason', 100);
    listHead('Customer Reason', 100);
    listHead('Refund', 30);
    listHead('Total refund value', 40, 'align=center');
    listHead('Price', 40, 'align=center');
    listHead('Warehouse Quantity', 50, 'align=center');
    listHead('Customer Quantity', 50, 'align=center');
    listHead('Customer Comment', 100);
    listHead('', 8);

    $warehouseQty = 0;
    $customerQty = 0;
    $totalPrice = 0;
    $totalRefundPrice = 0;
    $refundLinesVariantCodes = [];

    while ($row = dbFetch($resultRefundLines)) {
        $refundPrice = $row['PriceSale'] * (1 + ($vat / 100));

        listRow();
        listField((int)$row['No']);
        listField($row['Description']);
        listField($row['ColorName']);
        listField($row['SizeName']);
        listField($row['VariantCode']);
        listField($row['RefundReasonName']);
        listField($row['rfndrsnRefundReasonName']);
        listField(((int)$row['Refund'] === 1 ? 'Yes' : 'No'));
        if (((int)$row['Refund'] === 1) && ((int)$row['Quantity'] === 0) && ((int)$row['rtrnlnQuantity'] > 0)) {
            listField($row['Refund'] * $refundPrice * (int)$row['rtrnlnQuantity'], 'align=center');
            $totalRefundPrice += $row['Refund'] * $refundPrice * (int)$row['rtrnlnQuantity'];
        } else {
            listField($row['Refund'] * $refundPrice * (int)$row['Quantity'], 'align=center');
            $totalRefundPrice += $row['Refund'] * $refundPrice * (int)$row['Quantity'];
        }
        listField(($row['PriceSale'] * (1 + ($vat / 100))), 'align=center');
        listField($row['Quantity'], 'align=center');
        listField(isset($row['rtrnlnQuantity']) ? $row['rtrnlnQuantity'] : 0, 'align=center');
        listField($row['rtrnlnComment']);

        $warehouseQty += (int)$row['Quantity'];
        $totalPrice += (float)($row['PriceSale'] * (1 + ($vat / 100)));
        $refundLinesVariantCodes[] = $row['VariantCode'];
    }

    $query = sprintf(
        "
        SELECT 
            orderline.No,
            orderline.PriceSale,
            article.Description,
            variantcode.VariantCode,
            clr.Description AS ColorName,
            artclsz.Name AS SizeName,
            returnline.Quantity AS rtrnlnQuantity,
            returnline.Comment AS rtrnlnComment,
            refundreason2.Name AS CustomerRefundReasonName
        FROM `returnline`
            INNER JOIN `return` rtrn ON rtrn.Id = returnline.returnId AND rtrn.active = 1
            INNER JOIN orderline ON orderline.Id = returnline.orderLineId AND orderline.Active = 1
            INNER JOIN orderquantity ON orderquantity.OrderLineId = orderline.Id AND orderquantity.Active = 1
            INNER JOIN article ON article.Id = orderline.ArticleId AND article.Active = 1
            INNER JOIN variantcode ON variantcode.ArticleId = orderline.ArticleId
                                  AND variantcode.ArticleColorId = orderline.ArticleColorId
                                  AND variantcode.ArticleSizeId = orderquantity.ArticleSizeId
                                  AND variantcode.Active = 1
            LEFT JOIN articlecolor artclclr ON artclclr.Id = orderline.ArticleColorId AND artclclr.Active = 1
            LEFT JOIN color clr ON clr.Id = artclclr.ColorId AND clr.Active = 1
            LEFT JOIN articlesize artclsz ON artclsz.Id = orderquantity.ArticleSizeId AND artclsz.Active = 1
            LEFT JOIN refundreason refundreason2 ON refundreason2.id = returnline.refundReasonId
                                                AND refundreason2.Active = 1
        WHERE rtrn.refundId = %d
          AND rtrn.orderId = %d
          AND returnline.active = 1
        ",
        $refundId,
        $orderId
    );
    $resultReturnLines = dbQuery($query);

    $expectedTotalPrice = $totalPrice;

    while ($row = dbFetch($resultReturnLines)) {
        $customerQty += (int)$row['rtrnlnQuantity'];

        if (in_array($row['VariantCode'], $refundLinesVariantCodes) === true) {
            continue;
        }

        listRow('#FA8072');
        listField((int)$row['No']);
        listField($row['Description']);
        listField($row['ColorName']);
        listField($row['SizeName']);
        listField($row['VariantCode']);
        listField('');
        listField($row['CustomerRefundReasonName']);
        listField('');
        listField(0, 'align=center');
        listField(($row['PriceSale'] * (1 + ($vat / 100))), 'align=center');
        listField(0, 'align=center');
        listField(isset($row['rtrnlnQuantity']) ? $row['rtrnlnQuantity'] : 0, 'align=center'); // Customer Quantity
        listField($row['rtrnlnComment']); // Customer Comment

        $expectedTotalPrice += (float)($row['PriceSale'] * (1 + ($vat / 100)));
    }

    dbQueryFree($resultReturnLines);

    if (dbNumRows($resultRefundLines) === 0) {
        if ($Record['Type'] !== 'amount') {
            listRow();
            listField();
            listField('No OrderLines', 'colspan=2');
        } else {
            listRow();
            listField(1);
            listField('Specific amount returned');
            listField('');
            listField('');
            listField('');
            listField('');
            listField($Record['Note']);
            listField($Record['Note']);
            listField('Yes');
            listField($Record['Amount'], 'align=right');
            listField(1, 'align=right');
            listField('');
            listField('');
        }
    } else {
        listRow();
        listField('Total');
        listField();
        listField();
        listField();
        listField();
        listField();
        listField();
        listField();
        listField($totalRefundPrice, 'align=center');
        listField();
        listField($warehouseQty, 'align=center');
        listField($customerQty, 'align=center');
        listField('');
    }

    dbQueryFree($resultRefundLines);
listEnd();

function checkTransforStatus($row)
{
    if ((int)$row['cronId'] === 0) {
        return 'No status';
    }

    if ((int)$row['cronDone'] === 0) {
        return 'Awaiting export';
    }

    if ((string)$row['cronError'] !== '') {
        return 'Error in export';
    }

    return 'Exported';
}

return 0;
