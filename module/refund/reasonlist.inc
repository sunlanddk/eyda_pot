<?php

require_once 'lib/list.inc';
require_once 'lib/item.inc';

// Filter Bar
listFilterBar();

listStart();
	listRow();
	listHead('', 5);
	listHead('Id', 5);
	listHead('Name', 10);
	listHead('Description', 30);
	listHead('Restock', 5);
	listHead('Type', 10);
	while ($row = dbFetch($Result)) {
		listRow();
		listFieldIcon($Navigation['Icon'], 'reasonview', (int)$row['Id']);
		listField($row['Id']);
		listField($row['Name']);
		listField($row['Description']);
		listField(((int)$row['Restock'] === 1 ? 'Yes' : 'No'));
		listField(ucfirst($row['type']));
	}
	if (dbNumRows($Result) == 0) {
		listRow();
		listField();
		listField('No active Reasons', 'colspan=2');
	}
listEnd();

dbQueryFree($Result);

return 0;
