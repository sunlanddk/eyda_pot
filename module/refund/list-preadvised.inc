<?php

require_once 'lib/list.inc';
require_once 'lib/item.inc';

itemStart();
itemField('# preadviced', tableGetFieldWhere('`return`','count(Id)','refundId IS NULL and active=1')) ;
itemEnd();
// Filter Bar
listFilterBar();

listStart();
    listRow();
    listHead('', 20);
    listHead('ShopifyOrder', 80);
    listHead('Customer', 200);
    listHead('Tracking #', 100);
    listHead('Comment', 250);
    listHead('Value', 80, 'align="right"');
    //		listHead ('Type', 30) ;
    listHead('Return Date', 130);
    listHead('Email Send Date', 130);
    listHead('Error Message');
    listHead('Resend Email',80);
    listHead('Email',250);
    listHead('');
    listHead('Return Id', 75);
    listHead('Order Id', 70);
    listHead('', 20);

    while ($row = dbFetch($Result)) {
        listRow();

        if (!empty($row['ReturnEmailLog'])) {
            listRow('#FA8072');
        }
        listFieldIcon($Navigation['Icon'], 'returnview', (int)$row['Id']);
        listField($row['ShopifyOrder']);
        listField($row['Customer']);
        listFieldRaw('<a href="https://gls-group.eu/DK/da/find-pakke/?match='.$row['trackingCode'].'">' . $row['trackingCode'].'</a>');
        listField($row['Comment']);
        $query = sprintf(
            "SELECT 
                        SUM(rtrnln.Quantity) as Quantity, 
                        SUM(1.25 * ordrln.PriceSale) as PriceSale
                    FROM `return` rtrn
                        INNER JOIN returnline rtrnln ON rtrnln.returnId = rtrn.Id
                        INNER JOIN orderline ordrln ON ordrln.Id = rtrnln.OrderLineId
                    WHERE rtrn.id = %d
                      AND rtrn.Active = 1
                      AND ordrln.Active = 1
                      AND rtrnln.Active = 1",
            $row['Id']
        );
        $result = dbQuery($query);
        $_row = dbFetch($result);
        listField(number_format($_row['PriceSale'], 2), 'align="right"');
        listField(date('Y-m-d H:i:s', dbDateDecode($row['CreateDate'])));
        listField($row['ReturnEmailSendDate'] ? date('Y-m-d H:i:s', dbDateDecode($row['ReturnEmailSendDate'])) : null);
        listField($row['ReturnEmailLog']);
        listFieldIcon('email.gif', 'preadvresendemail', (int)$row['Id']);
        listField(!empty($row['ReturnEmail']) ? $row['ReturnEmail'] : $row['OrderEmail']);
        listField();
        listField($row['Id']);
        listField($row['OrderId']);
        listFieldIcon('delete.gif', 'deletereturn', (int)$row['Id']);
    }
    if (dbNumRows($Result) === 0) {
        listRow();
        listField();
        listField('No active Refunds', 'colspan=2');
    }
listEnd();

dbQueryFree($Result);

return 0;
