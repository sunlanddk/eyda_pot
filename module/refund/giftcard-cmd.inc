<?php 

	$query = sprintf ('SELECT o.* FROM `order` o WHERE o.Id=%d', $Record['OrderId']) ;
    $result = dbQuery ($query) ;
    $order = dbFetch ($result) ;
    dbQueryFree ($result) ;
    $url = '';
	switch ((int)$order['WebShopNo']) {
		case 1:
			$url = SHOPIFY_URL_DA;
			break;
		case 2:
			$url = SHOPIFY_URL_EU;
			break;
		case 3:
			$url = SHOPIFY_URL_NO;
			break;
		case 4:
			$url = SHOPIFY_URL_SE;
			break;
		case 5:
			$url = SHOPIFY_URL_AMBASSADOR;
			break;
		case 6:
			$url = SHOPIFY_URL_B2B;
			break;
		
		default:
			$url = SHOPIFY_URL_DA;
			break;
	}

	$newCustomer = array(
		"customer" => array(
        	"email" 			=> (string)$_POST['Email'],
        	"send_email_invite"	=> false,
        	"verified_email"	=> true
    	)
	);

	$ch = curl_init($url.'customers.json');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($newCustomer));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $result = curl_exec($ch);
    $result = json_decode($result, true);

    $customerId = false;

    if(isset($result['customer']['id']) === true) $customerId = $result['customer']['id'];

    if($customerId === false){
		$ch = curl_init($url.'customers/search.json?query=email:'.(string)$_POST['Email']);
	    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
	    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

	    $result = curl_exec($ch);
	    $result = json_decode($result, true);

	    if(isset($result['customers'][0]['id']) === true){
	    	if($result['customers'][0]['email'] === (string)$_POST['Email']){
	    		$customerId = $result['customers'][0]['id'];
	    	}
	    }
    }

    if($customerId === false) return "Customer can't be created and can't be found.";

    $giftcard = array(
	    "gift_card" => array(
    	    "initial_value" => (float)$_POST['Amount'],
	        "customer_id" 	=> (string)$customerId,
	    )
    );

    $ch = curl_init($url.'gift_cards.json');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($giftcard));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

    $result = curl_exec($ch);
    $resultObj = json_decode($result, true);

    if(isset($resultObj['gift_card']['id']) !== true){
    	return 'Error when creating giftcard: '.$result;
    }

	$post = array(
		'Done' => 1,
		'DoneUserId' => $User['Id'],
		'DoneDate' => dbDateEncode(time()),
		'Type' => 'giftcard'
	);
	tablewrite('refund', $post, $Id);

	$post = array(
		'Comment' => $order['Comment'].sprintf(' Refunded as giftcard, DKK %s, %s', (float)$_POST['Amount'], (string)$_POST['Email']),
	);
	tablewrite('order', $post, (int)$order['Id']);

	navigationCommandMark('refundview', $Id);
?>