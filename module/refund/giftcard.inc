<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    $query = sprintf ('SELECT o.* FROM `order` o WHERE o.Id=%d', $Record['OrderId']) ;
    $result = dbQuery ($query) ;
    $order = dbFetch ($result) ;
    dbQueryFree ($result) ;

    $vat = tableGetField('country', 'VATPercentage', (int)$order['CountryId']);
    $Currency = tableGetField('currency', 'Symbol', (int)$order['CurrencyId']);

    $query = sprintf ('SELECT rl.*, SUM(ol.PriceSale * rl.Quantity) as Sum FROM `refundline` rl LEFT JOIN orderquantity oq ON oq.Id=rl.OrderQuantityId LEFT JOIN orderline ol ON ol.Id=oq.OrderLineId WHERE rl.RefundId=%d AND rl.Active=1', $Id) ;
    $result = dbQuery ($query) ;
    $lineSum = dbFetch ($result) ;
    dbQueryFree ($result) ;



    formStart();
    itemStart () ;
    itemSpace () ;
    itemField ('Order', $order['Id']) ;
    itemField ('Shopify Order', $order['Reference']) ;
    itemField ('Comment', $order['Comment']) ;
    itemSpace () ;
    itemFieldRaw ('Email', formText('Email', '')) ;
    itemSpace () ;
    itemField ('Estimate Amount', (float)($lineSum['Sum']*(1+($vat/100))).' '.$Currency )  ;
    itemFieldRaw ('Amount', formNumber('Amount', (float)($lineSum['Sum']*(1+($vat/100))) ) ) ;
    itemSpace () ;
    itemEnd () ;
    formEnd();

    return 0 ;
?>
