<?php

require_once('lib/table.inc');

$full = true;

foreach ($_POST['RefundLine'] as $key => $value) {
    if ($key === 0) {
        $post = [
            'CreateDate' => date('Y-m-d H:i:s'),
            'CreateUserId' => 1,
            'ModifyDate' => date('Y-m-d H:i:s'),
            'ModifyUserId' => 1,
            'RefundId' => (int)$Id,
            'OrderQuantityId' => (int)$value,
            'ReasonId' => 1,
            'Quantity' => 0,
            'Refund' => 1,
        ];
        tableWrite('refundline', $post);

        continue;
    }

    if (isset($_POST['Refund'][$key]) === true) {
        $post = ['Refund' => 1];
    } else {
        $full = false;
        $post = ['Refund' => 0];
    }

    tableWrite('refundline', $post, (int)$key);
}

if ($full === false) {
    $post = ['Type' => 'partial'];
} else {
    $post = ['Type' => 'full'];
}
tableWrite('refund', $post, $Id);

return 0;
