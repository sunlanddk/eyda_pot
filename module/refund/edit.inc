<?php

require_once 'lib/table.inc';
require_once 'lib/item.inc';
require_once 'lib/form.inc';

$query = sprintf('SELECT o.* FROM `order` o WHERE o.Id=%d', $Record['OrderId']);
$result = dbQuery($query);
$order = dbFetch($result);
dbQueryFree($result);


itemStart();
    itemSpace();
    itemField('Order', $order['Id']);
    itemField('Shopify Order', $order['Reference']);
    itemSpace();
itemEnd();

// Form
printf("<form method=POST name=appform enctype='multipart/form-data'>\n");
printf("<input type=hidden name=id value=%d>\n", $Id);
printf("<input type=hidden name=nid>\n");

?>
<table class="list">
    <tbody>
    <tr class="list">
        <td class="listhead" align="left"><p class="listhead">VariantCode</p></td>
        <td class="listhead" align="left"><p class="listhead">Name</p></td>
        <td class="listhead" align="center"><p class="listhead">Color</p></td>
        <td class="listhead" align="center"><p class="listhead">Size</p></td>
        <td class="listhead" align="left"><p class="listhead">Warehouse Reason</p></td>
        <td class="listhead" align="left"><p class="listhead">Customer Reason</p></td>
        <td class="listhead" align="center"><p class="listhead">Warehouse Quantity</p></td>
        <td class="listhead" align="center"><p class="listhead">Customer Quantity</p></td>
        <td class="listhead" align="left"><p class="listhead">Customer Comment</p></td>
        <td class="listhead" align="right"><p class="listhead">Refund</p></td>
    </tr>
    <?php
    $query = sprintf(
        "
                SELECT 
                    rl.*,
                    ol.Description,
                    v.VariantCode,
                    rr.Name AS Reason,
                    clr.Description AS ColorName,
                    artclsz.Name AS SizeName,
                    rtrnln.quantity AS rtrnlnQuantity,
                    rtrnln.comment AS rtrnlnComment,
                    rfndrsn.Name AS rfndrsnRefundReasonName
                FROM refundline rl
                    JOIN orderquantity oq ON oq.Id = rl.OrderQuantityId
                    JOIN orderline ol ON ol.Id = oq.OrderLineId
                    JOIN refundreason rr ON rr.Id = rl.ReasonId
                    JOIN article a ON a.Id = ol.ArticleId
                    JOIN variantcode v ON v.ArticleId = ol.ArticleId
                                      AND v.ArticleColorId = ol.ArticleColorId
                                      AND v.ArticleSizeId = oq.ArticleSizeId
                                      AND v.Active = 1
                    LEFT JOIN articlecolor artclclr ON artclclr.Id = ol.ArticleColorId AND artclclr.Active = 1
                    LEFT JOIN color clr ON clr.Id = artclclr.ColorId AND clr.Active = 1
                    LEFT JOIN articlesize artclsz ON artclsz.Id = oq.ArticleSizeId AND artclsz.Active = 1
                    LEFT JOIN `return` rtrn ON rtrn.refundId = %d
                                           AND rtrn.orderId = ol.orderId
                                           AND rtrn.active = 1
                    LEFT JOIN `returnline` rtrnln ON rtrnln.returnId = rtrn.id
                                                 AND rtrnln.orderLineId = ol.id
                                                 AND rtrnln.active = 1
                    LEFT JOIN `refundreason` rfndrsn ON rfndrsn.id = rtrnln.refundReasonId
                                                    AND rfndrsn.Active = 1
                WHERE rl.RefundId = %d
                  AND rl.Active = 1
              ",
        $Id,
        $Id
    );
    $res = dbQuery($query);

    $count = 0;
    $refundLinesVariantCodes = [];
    while ($row = dbFetch($res)) {
        $count++;

        listRow();
        listField($row['VariantCode']);
        listField($row['Description']);
        listField($row['ColorName'], 'align=center');
        listField($row['SizeName'], 'align=center');
        listField($row['Reason']);
        listField($row['rfndrsnRefundReasonName']);
        listField($row['Quantity'], 'align=center');
        listField(isset($row['rtrnlnQuantity']) ? $row['rtrnlnQuantity'] : 0, 'align=center'); // Customer Quantity
        listField($row['rtrnlnComment']); // Customer Comment
        echo '<td class="list" align="right"><input type="hidden" name="RefundLine[' . $row['Id'] . ']" value="' . $row['Id'] . '" />' . formCheckbox(
                'Refund[' . $row['Id'] . ']',
                $row['Refund']
            ) . '</td>';

        $refundLinesVariantCodes[] = $row['VariantCode'];
    }
    dbQueryFree($res);

    $query = sprintf(
        "
        SELECT 
            orderline.No,
            orderline.PriceSale,
            article.Description,
            variantcode.VariantCode,
            clr.Description AS ColorName,
            artclsz.Name AS SizeName,
            returnline.Quantity AS rtrnlnQuantity,
            returnline.Comment AS rtrnlnComment,
            refundreason2.Name AS CustomerRefundReasonName,
            orderquantity.Id AS OrderQuantityId
        FROM `returnline`
            INNER JOIN `return` rtrn ON rtrn.Id = returnline.returnId AND rtrn.active = 1
            INNER JOIN orderline ON orderline.Id = returnline.orderLineId AND orderline.Active = 1
            INNER JOIN orderquantity ON orderquantity.OrderLineId = orderline.Id AND orderquantity.Active = 1
            INNER JOIN article ON article.Id = orderline.ArticleId AND article.Active = 1
            INNER JOIN variantcode ON variantcode.ArticleId = orderline.ArticleId
                                  AND variantcode.ArticleColorId = orderline.ArticleColorId
                                  AND variantcode.ArticleSizeId = orderquantity.ArticleSizeId
                                  AND variantcode.Active = 1
            LEFT JOIN articlecolor artclclr ON artclclr.Id = orderline.ArticleColorId AND artclclr.Active = 1
            LEFT JOIN color clr ON clr.Id = artclclr.ColorId AND clr.Active = 1
            LEFT JOIN articlesize artclsz ON artclsz.Id = orderquantity.ArticleSizeId AND artclsz.Active = 1
            LEFT JOIN refundreason refundreason2 ON refundreason2.id = returnline.refundReasonId
                                                AND refundreason2.Active = 1
        WHERE rtrn.refundId = %d
          AND rtrn.orderId = %d
          AND returnline.active = 1
        ",
        $Id,
        $order['Id']
    );
    $resultReturnLines = dbQuery($query);

    while ($row = dbFetch($resultReturnLines)) {
        if (in_array($row['VariantCode'], $refundLinesVariantCodes) === true) {
            continue;
        }

        listRow('#FA8072');
        listField($row['VariantCode']);
        listField($row['Description']);
        listField($row['ColorName'], 'align=center');
        listField($row['SizeName'], 'align=center');
        listField('');
        listField($row['CustomerRefundReasonName']);
        listField(0, 'align=center');
        listField(isset($row['rtrnlnQuantity']) ? $row['rtrnlnQuantity'] : 0, 'align=center'); // Customer Quantity
        listField($row['rtrnlnComment']); // Customer Comment
        echo '<td class="list" align="right"><input type="hidden" name="RefundLine[0]" value="' . $row['OrderQuantityId'] . '" />' . formCheckbox(
                'Refund[0__' . $row['OrderQuantityId'] . ']',
                $row['Refund']
            ) . '</td>';
    }
    dbQueryFree($resultReturnLines);

    ?>
    </tbody>
</table>
<?php

itemEnd();
formEnd();

return 0;
