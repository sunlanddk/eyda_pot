
const url = weburl; 
const appstore = new Vue({
	el: "#appRefund",
	data: {
		str: '',
		sku: '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		preloader: false
	},
	beforeMount(){
		var vm = this;
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99send'){
	        		// create refund record
	        		vm.sku = vm.str;
	        		vm.simpleRefund();
	        		vm.str = '';
	        		return;
	        	}
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		// create refund record
	        		vm.sku = vm.str;
	        		vm.simpleRefund();
	        		vm.str = '';
	        		return;
	        	}

	        	vm.sku = vm.str;
	        	vm.simpleRefund();
	        	vm.str = '';

	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		reload(){
			location.reload();
		},
	    reset(){
	    	var vm = this;
			vm.preloader = false;
	    	vm.str = '';
	    	vm.sku = '';
	    },
	    simpleRefund(){
	    	let self = this;
	    	self.preloader = true;
	    	let data = {sku: self.sku, userid: $('.userid').val(), ws: $('.ws').val()};
	    	let path = window.location.pathname;
	    	if(self.sku.length == 0){
	    		self.preloader = false;
	    		self.message = 'Please scan items before creating.';
	    		return;
	    	}

			self.postData(weburlNew+'refund/simpleitem', data, self.CreateRefundResponse);
	    },
	    CreateRefundResponse(response){
	    	let self = this;
	    	self.done = 'Return created.';
	    	self.reset();
	    	self.preloader = false;
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    headers: {
                	'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
                	'X-Header-Userid': $('.userid').val(),
                	'Accept': 'application/json',
					'Content-Type': 'application/json'
            	},
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {

			    	if(data.status == 'error'){
			    		vm.message = data.response;
			    		vm.preloader = false;
			    	}
			    	else{
			    		callback(data.response);
			    	}
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	console.log(errorThrown, textStatus, jqXHR);
			    	vm.message = 'An error occured, try by scanning again. Error message: ' +textStatus;
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
	    	let vm  = this;
			$.ajax({
			    url : url,
			    type: "GET",
			    headers: {
                	'X-Header-Auth': 'ajdkaur783i8rhfak1o182hr',
                	'X-Header-Userid': $('.userid').val(),
                	'Accept': 'application/json',
					'Content-Type': 'application/json'
            	},
			    success: function(data, textStatus, jqXHR)
			    {

			    	if(data.status == 'error'){
			    		vm.message = data.response;
			    		vm.preloader = false;
			    	}
			    	else{
			    		callback(data.response);
			    	}
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});



