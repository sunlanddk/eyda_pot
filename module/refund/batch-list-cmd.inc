<?php 
require_once('lib/table.inc');


if(isset($_POST['UpdateSetFlag']) === true){
	foreach ($_POST['UpdateSetFlag'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;

		switch ($Navigation['Parameters']) {
			case 'refund':
				$post = array(
					'type'				=> 'refund',
					'internalid' 		=> $id,
					'cron_name' 		=> 'Create refund',
					'cron_function' 	=> 'refundInShopifyCron',
					'cron_value' 		=> $id.','.$User['Id'],
					'cron_scheduled_at'	=> dbDateEncode(time()),
				);
				if((int)tableGetFieldWhere('cron_jobs','id', sprintf("internalid=%d AND type='refund' AND done=0",(int)$id)) === 0){
					tableWriteCron('cron_jobs', $post);
				}
				break ;

			case 'norefund':
				$post = array(
					'Done' => 1,
					'DoneUserId' => $User['Id'],
					'DoneDate'	 => dbDateEncode(time()),
					'Type'		 => 'norefund'
				);
				tablewrite('refund', $post, $id);
				break ;

			case 'giftcard':
				$post = array(
					'Done' => 1,
					'DoneUserId' => $User['Id'],
					'DoneDate' => dbDateEncode(time()),
					'Type' => 'giftcard'
				);
				tablewrite('refund', $post, $id);
				break ;

			default:
				return 'Unexpected command. Please contact support' ;
		}
	}
} else {
	return 'No refunds selected.';
}

return 0;