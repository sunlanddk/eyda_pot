<?php

    require 'lib/config.inc' ;
    require 'lib/save.inc' ;

	function CreateColorIcon($r,$g,$b) {
		Global $Config ;
		$_img =  $Config['photoLib'] . $r . '_' . $g . '_' . $b . '.png' ;
	 	unlink($_img) ;

		$w = 40 ; $h = 40 ;
		
		// Create the image
		$im = imagecreatetruecolor((int)$w, (int)$h);

		// Create color
		$_color = imagecolorallocate($im, $r, $g, $b);
		imagefilledrectangle($im, 0, 0, (int)$w-1, (int)$h-1, $_color);


		// Using imagepng() results in clearer text compared with imagejpeg()
	 	imagepng($im, $_img);

		imagedestroy($im);
		
		return $_img ;
	}
	
    $fields = array (
		'Name'			=> array ('mandatory' => true,	'check' => true),
		'Description'		=> array (),
		'ValueRed'		=> array ('type' => 'integer', 	'check' => true),
		'ValueGreen'		=> array ('type' => 'integer', 	'check' => true),
		'ValueBlue'		=> array ('type' => 'integer',	'check' => true)
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value) {
		global $Id ;
		switch ($fieldname) {
			case 'Name':
				// Check that name does not allready exist
				$query = sprintf ('SELECT Id FROM ColorGroup WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return 'ColorGroup allready existing' ;
				return true ;

			case 'ValueRed' :
			case 'ValueGreen' :
			case 'ValueBlue' :
				if ($value < 0 or $value > 255) return sprintf ('%s must be in the interval from 0 to 255', $fieldname) ;
				return true ;
		}
		return false ;
    }

	CreateColorIcon((int)$_POST['ValueRed'],(int)$_POST['ValueGreen'],(int)$_POST['ValueBlue']) ;
	
    return saveFields ('ColorGroup', $Id, $fields) ;
?>
