<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT Carrier.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName FROM Carrier LEFT JOIN Company ON Company.Id=Carrier.CompanyId WHERE Carrier.Active=1 ORDER BY Carrier.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query DeliveryTerm
	    $query = sprintf ('SELECT Carrier.* FROM Carrier WHERE Carrier.Id=%d AND Carrier.Active=1', $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;
	    return 0 ;
    }

    return 0 ;
?>
