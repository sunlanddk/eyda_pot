<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Name', formText ('Name', $Record['Name'], 20)) ;
    itemFieldRaw ('Description', formText ('Description', $Record['Description'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('Company', formDBSelect ('CompanyId', (int)$Record['CompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCarrier=1 AND Active=1 ORDER BY Name', 'width:250px;')) ;  
    itemSpace () ;
    itemFieldRaw ('Trans. Border', formText ('TransportBorder', $Record['TransportBorder'], 2)) ;
    itemFieldRaw ('Trans. Local', formText ('TransportLocal', $Record['TransportLocal'], 2)) ;
    itemFieldRaw ('Customs Loc.', formText ('CustomsLocation', $Record['CustomsLocation'], 4)) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
