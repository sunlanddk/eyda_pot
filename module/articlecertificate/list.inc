<?php

    require_once "lib/list.inc" ;

    // Article header
    printf ("<br>") ;
    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Article</td><td class=itemfield>%s (%s)</tr>\n", htmlentities($Record['Number']), htmlentities($Record['Description'])) ;
    printf ("<tr><td class=itemlabel>Supplier</td><td class=itemfield>%s</tr>\n", htmlentities($Record['SupplierCompanyName'])) ;
    printf ("</table>\n") ;
    printf ("<br>") ;

    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Certificate', 120) ;
    listHead ('Type') ;
    listHead ('Holder') ;
    listHead ('Valid Until') ;
    listHead ('Description') ;
    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'editartcert', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['CertificateTypeName']) ;
	listField ($row['HolderCompanyName']) ;
	listField (date ('Y.m.d', dbDateDecode($row['ValidUntil']))) ;
	listField ($row['Description']) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Certificates', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
