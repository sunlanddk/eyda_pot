<?php

    require_once 'lib/save.inc' ;

    $fields = array (
	'CertificateId'		=> array ('mandatory' => true,	'check' => true),
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $fields['ArticleId'] = array ('type' => 'set', 'value' => $Id) ;
	    $Record['ArticleId'] = $Id ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $fields ;
	switch ($fieldname) {
	    case 'CertificateId':
		if (!$changed) return false ;

		// Validate Id
		if ($value <= 0) return 'please select Certificate' ;
		
		// Validate the certificate
		$query = sprintf ('SELECT * FROM Certificate WHERE Id=%d AND Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ((int)$row['Id'] != $value) return sprintf ('%s(%d) component not found, id %d', __FILE__, __LINE__, $value) ;
		if ((int)$row['SupplierCompanyId'] != $Record['SupplierCompanyId']) return sprintf ('%s(%d) invalid supplier company, id %d', __FILE__, __LINE__, $row['SupplierCompanyId']) ;
		
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM ArticleCertificate WHERE ArticleId=%d AND Active=1 AND CertificateId=%d AND Id<>%d', $Record['ArticleId'], $value, $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return sprintf ('%s(%d) certificate allready existing', __FILE__, __LINE__) ;

		return true ;
	}
	return false ;
    }
     
    return saveFields ('ArticleCertificate', $Id, $fields) ;
?>
