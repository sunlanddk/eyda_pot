<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // Default values
	    unset ($Record['CertificateId']) ;
	    $Id = -1 ;
	    break ;
    }
     
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Certificate', formDBSelect ('CertificateId', $Record['CertificateId'], sprintf ('SELECT Certificate.Id, CONCAT(Certificate.Name," (",CertificateType.Name,", ",IF(ISNULL(Company.Id),"-",Company.Name),", ",DATE_FORMAT(Certificate.ValidUntil, "%%Y.%%m.%%d"),")") AS Value FROM Certificate LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId LEFT JOIN Company ON Company.Id=Certificate.HolderCompanyId LEFT JOIN ArticleCertificate ON ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 AND ArticleCertificate.CertificateId=Certificate.Id WHERE Certificate.SupplierCompanyId=%d AND Certificate.Active=1 AND (Certificate.Id=%d OR ISNULL(ArticleCertificate.Id)) ORDER BY Value', $Record['ArticleId'], $Record['SupplierCompanyId'], $Record['CertificateId']), 'width:400px')) ;  
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
