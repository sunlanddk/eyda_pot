<?php

//    require_once 'lib/navigation.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query colors to list
	    $query = sprintf ('SELECT ArticleCertificate.*, Certificate.Name, Certificate.Description, Certificate.ValidUntil, CertificateType.Name AS CertificateTypeName, CONCAT(HolderCompany.Name," (",HolderCompany.Number,")") AS HolderCompanyName FROM ArticleCertificate, Certificate LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId LEFT JOIN Company AS HolderCompany ON HolderCompany.Id=Certificate.HolderCompanyId WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 AND Certificate.Id=ArticleCertificate.CertificateId ORDER BY Name', $Id) ;
	    $Result = dbQuery ($query) ;

	    // Load Article (parent)
	    $query = sprintf ('SELECT Article.*, CONCAT(SupplierCompany.Name," (",SupplierCompany.Number,")") AS SupplierCompanyName FROM Article LEFT JOIN Company AS SupplierCompany ON SupplierCompany.Id=Article.SupplierCompanyId WHERE Article.Id=%d', $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch ($result) ;
	    dbQueryFree ($result) ;

	    return 0 ;

	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ("SELECT Article.*, Article.Id AS ArticleId FROM Article WHERE Article.Id=%d", $Id) ;
		    $Result = dbQuery ($query) ;
		    $Record = dbFetch ($Result) ;
		    dbQueryFree ($Result) ;
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Get record
	    $query = sprintf ("SELECT ArticleCertificate.*, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription, Article.SupplierCompanyId FROM ArticleCertificate LEFT JOIN Article ON Article.Id=ArticleCertificate.ArticleId WHERE ArticleCertificate.Id=%d AND ArticleCertificate.Active=1", $Id) ;
	    $Result = dbQuery ($query) ;
	    $Record = dbFetch ($Result) ;
	    dbQueryFree ($Result) ;

	    return 0 ;
    }

    return 0 ;
?>
