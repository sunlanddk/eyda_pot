<?php
 
    require_once 'lib/table.inc' ;
    require_once 'lib/html.inc' ;
    
    printf ("<H2>%s</H2>\n", htmlentities($Record["Name"])) ;
    printf ("<p style='padding-right:8px;'>%s</p>\n", nl2br(htmlentities($Record["Text"]))) ;

    printf ("<HR width=100%% size=1 noshade>\n") ;

    printf ("<table class=item>\n") ;
    printf ("<tr><td class=itemlabel>Category</td><td><p>%s</p></td></tr>\n", htmlentities(tableGetField("Category", "Name", $Record["CategoryId"]))) ;   
    if ($User["Internal"]) {
	printf ("<tr><td class=itemlabel>External</td><td class=itemfield>%s</td></tr>\n", ($Record["External"]) ? "Yes" : "No") ;
    }
    printf ("<tr><td class=itemlabel>News</td><td class=itemfield>%s</td></tr>\n", ($Record["News"]) ? "Yes" : "No") ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;

    return 0 ;
?>
