<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    unset($_POST["id"]);
    unset($_POST["nid"]);
    unset($_POST["clicked"]);

    foreach ($_POST as $key => $value) {
        $_POST[$key] = utf8_encode($value);
    }

    $content = json_encode($_POST);
    $post = array(
        'Content' => $content,
    );

    tableWrite('pickorder_texts', $post, $Id);
    
    return navigationCommandMark ('pickordertexts') ;
    return 0 ;    
