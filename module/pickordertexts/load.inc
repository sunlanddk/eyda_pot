<?php

    require_once 'lib/navigation.inc' ;
    
	global $User, $Navigation, $Record, $Size, $Id ;

    switch ($Navigation['Function']) {
		case 'list' :
					// List field specification
		$fields = array (
			NULL, // index 0 reserved
				array ('name' => 'Store',			'db' => 'ss.Name')
			) ;
	
			require_once 'lib/list.inc' ;
			$queryFields =  'ss.Name as Store, pot.Id as Id, pot.Content';
			
					$queryTables = '(pickorder_texts pot) 
					LEFT JOIN shopifyshops ss ON ss.id = pot.Shop';
	
			$queryClause = sprintf ("pot.Active = 1 ") ;
	
			$Result = listLoad ($fields, $queryFields, $queryTables, $queryClause) ;
			if (is_string($Result)) return $Result ;
			return listView ($Result, 'pickordertexts') ;
	
			return 0;
		case 'basic' :
		case 'basic-cmd' :
			$query = sprintf ("SELECT * FROM pickorder_texts WHERE Active=1 AND Id=%d", $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
	
			return 0 ;
		
		return 0 ;
	}
?>
