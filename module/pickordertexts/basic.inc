<?php

use function GuzzleHttp\json_encode;

require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch($Navigation["Parameters"]) {

        case "edit":
            // Form
            formStart () ;
            formCalendar () ;
            itemStart () ;
            itemHeader () ;

            $content = json_decode($Record['Content'], true);
            $xmas = false;

            itemFieldRaw ('Header1', formText ('header1', utf8_decode($content['header1']),400)) ;
            itemFieldRaw ('Line1', formtextArea ('line1',utf8_decode($content['line1']), 'width:100%;height:50px;')) ;
            itemSpace () ;


            itemFieldRaw ('Header2', formText ('header2', utf8_decode($content['header2']), 400)) ;
            itemFieldRaw ('Line2', formtextArea ('line2', utf8_decode($content['line2']), 'width:100%;height:50px;')) ;
            itemSpace () ;


            itemFieldRaw ('Header3', formText ('header3', utf8_decode($content['header3']), 400)) ;
            itemFieldRaw ('Line3', formtextArea ('line3', utf8_decode($content['line3']), 'width:100%;height:50px;')) ;
            itemSpace () ;


            itemFieldRaw ('Header4', formText ('header4', utf8_decode($content['header4']), 400)) ;
            itemFieldRaw ('Line4', formtextArea ('line4', utf8_decode($content['line4']), 'width:100%;height:50px;')) ;
            itemSpace () ;


            itemFieldRaw ('Header5', formText ('header5', utf8_decode($content['header5']), 400)) ;
            itemFieldRaw ('Line5', formtextArea ('line5', utf8_decode($content['line5']), 'width:100%;height:50px;')) ;
            itemSpace () ;

            if ($xmas) {
                itemFieldRaw ('LineGift', formtextArea ('linegift', utf8_decode($content['linegift']), 'width:100%;height:14px;')) ;
                
                itemFieldRaw ('Header6', formText ('header6', utf8_decode($content['header6']),400)) ;
                itemFieldRaw ('Line6', formtextArea ('line6', utf8_decode($content['line6']), 'width:100%;height:50px;')) ;

                itemFieldRaw ('Header7', formText ('header7', utf8_decode($content['header7']),400)) ;
                //itemFieldRaw ('Line7', formtextArea ('line7', utf8_decode($content['line7']), 'width:100%;height:50px;')) ;
            }

            


            itemEnd () ;
            formEnd () ;
        

    }
    return 0 ;
?>
