<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
  		   
    tableDelete ('ArticlePrice', $Id) ;

    // Load ArticlePrices
    $query = sprintf ('SELECT * FROM ArticlePrice WHERE ArticleId=%d AND Active=1 ORDER BY QuantityStart', $Record['ArticleId']) ;
    $result = dbQuery ($query) ;
    $Price = array () ;
    while ($row = dbFetch ($result)) $Price[] = $row ;
    dbQueryFree ($result) ;

    // Remove updated quantity from sequense
    foreach ($Price as $i => $p) {
	if (count($Price)-1 == $i) {
	    // Last quantity
	    // Must always end at 999999.999
	    if ($p['QuantityEnd'] == MAXQUANTITY) continue ;
	    $q = MAXQUANTITY ;
	} else {
	    // Next QuantityStart must immediately follow QuantityEnd
	    $q = $Price[$i+1]['QuantityStart'] ;
	    if ($p['QuantityEnd'] == $q) continue ;
	}

	// Repair
	$f = array ('QuantityEnd' => array ('type' => 'set', 'value' => $q)) ;
	saveFields ('ArticlePrice', (int)$p['Id'], $f) ;
    }
    
    return 0
?>
