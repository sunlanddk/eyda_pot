<?php

// HTTP Header
if (!headers_sent()) {
    switch ($_SERVER["SERVER_PROTOCOL"]) {
	case "HTTP/1.1":
	    header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
	    break ;
	
	case "HTTP/1.0":
	    header ("Pragma: no-cache") ; 
	    break ;
    }
}

// Posted login command
switch ($_POST["admin"]) {
    case "login":
	// Do login
	require_once "lib/log.inc" ;

	// Terminate any active logins at this session
	$query = sprintf ("SELECT Id FROM Login WHERE SessionId=%d AND Active=1", $Session["Id"]) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    $query = sprintf ("UPDATE Login SET Active=0, LogoutDate='%s' WHERE Id=%d", dbDateEncode(time()), $row["Id"]) ;
	    dbQuery ($query) ;
	    logPrintf (logINFO, "clean logout, LoginId %d", $row["Id"]) ; 
	}
	dbQueryFree ($result) ;

	// Look up user
	$query = sprintf ("SELECT Id, Password, Extern FROM User WHERE Loginname='%s' AND Active=1 AND Login=1", addslashes($_POST["Loginname"])) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if (!$row) {
	    $Error = "User not found" ;
	    break ;
	}

	// Verify password
	if (trim($row['Password']) == '' or $row["Password"] != addslashes($_POST["Password"])) {
	    $Error = "Invalid password" ;
	    break ;
	}

	// Create login
	$query = sprintf ("INSERT INTO Login SET LoginDate='%s', AccessDate='%s', SessionId=%d, UserId=%d, AcceptedMimeTypes='%s', Active=1", dbDateEncode (time()), dbDateEncode (time()), $Session["Id"], $row["Id"], $_SERVER["HTTP_ACCEPT"]) ;
	dbQuery ($query) ;

	if ($row['Extern']==1) {
//		$_tmp_curr  = tableGetField('Company', 'CurrencyId', (int)$row['CompanyId']);
       	setcookie('currencyId', NULL);
	}
	
	// Log
	logPrintf (logINFO, "login, LoginId %d, %s[%d]", dbInsertedId(), __FILE__, __LINE__) ;

	// Refresh page
	header (sprintf ("Location: %s", $_SERVER["REQUEST_URI"])) ;
	
//	printf ("<script language='javascript' type='text/javascript'>\n") ;
//	printf ("location.href='%s';\n", $_SERVER["REQUEST_URI"]) ;
//	printf ("window.open ('%s', 'prowide%s', 'scrollbars=yes,resizable=yes,directories=no,location=no,status=no,menubar=no,toolbar=no,personalbar=no,hotkeys=no');\n", $_SERVER["REQUEST_URI"], $_SERVER["UNIQUE_ID"]) ;
//	printf ("window.close() ;\n") ;
//	printf ("</script>\n") ;

	return ;
}

$Loginname = $_GET["user"] ;

?>
<html>
<head>
<title>PassOn - Business Portal Login</title>
<link href='layout/default/styles.css' rel='stylesheet' type='text/css'>
<style>
.uname {color:black}
.txtfld {background-color:white}
body {background:white;text-align:center}
table {border: none}
td {border: none}
.vcontainer {
    position: absolute;
    top: 50%;
    left: 50%;
    -moz-transform: translateX(-50%) translateY(-50%);
    -webkit-transform: translateX(-50%) translateY(-50%);
    transform: translateX(-50%) translateY(-50%);
}
</style>
</head>

<?php    
    printf ("<body  onload='document.LoginForm.%s.focus()'>\n", ($Loginname) ? "Password" : "Loginname") ;
    //printf ($Config["HTMLHeader"], "Login") ;
?>
<div class="vcontainer">
	<center>
	<table>
		<tr>
			<td colspan=2 style="vertical-align:middle;text-align:center">
				<img src="image/thumbnails/loginmain.jpg">
			</td>
		</tr>
		<tr>
			<td>
				<br><br>
				<form method="POST" name="LoginForm">
				<input type=hidden name="admin" value="login">
				<table>
					<tr><td></td><td><p class="uname"><b><?php echo $Error ; ?>&nbsp;</b></p></td></tr>
					<tr><td style="height:17px">&nbsp;</td></tr>
					<tr><td colspan=2><p class="uname"><b>Login to our Business Portal</b></p></td></tr>
					<tr><td style="height:17px">&nbsp;</td></tr>
					<tr ><td><p class="uname">User</p></td><td><input class="txtfld" type=text name="Loginname" size=20 maxlength=20 <?php if ($Loginname) printf ("value=\"%s\"", $Loginname) ?>></td><td style="color:white">username</td></tr>
					<tr style="height:4px"><td></td></tr>
					<tr ><td><p class="uname">Password</p></td><td><input class="txtfld" type=password name="Password" size=20 maxlength=20"></td><td style="color:white">password</td></tr>
					<tr><td colspan=3 align=center style="padding-top:10px"><input type=submit value='Log in'></td></tr>
				</table>
				</form>
			</td>
				<td style="vertical-align:bottom;text-align:right">Powered by <br><img height="50px" src="image/passonlogo.png">
			</td>
		</tr>
	</table>
	</center>
</div>
</body>
</html>
