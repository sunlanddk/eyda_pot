<?php

require_once "lib/log.inc" ;

// Check that a new cookie no allready has been send
if ($_GET["Cookie"] == "set") {
    logPrintf (logWARNING, "can't set cookie, IP %s", $_SERVER["REMOTE_ADDR"]) ;
    printf ("<b>Error:</b> your browser does not accept Cookies<br>Please correct !\n") ;
	header (sprintf ("Location: %s", $_SERVER["REQUEST_URI"])) ;
    exit () ;
}

//$uid = $_SERVER["UNIQUE_ID"] ;
$uid = time();
$host = $_SERVER["SERVER_NAME"] ;
if (strncmp($host, "www.", 4) == 0) {
    $host = substr($host, 3);
}
if (strncmp($host, ".", 1) != 0) {
    $host = '.'.$host ;
}
setcookie(COOKIE_NAME, $uid, time()+60*60*24*30, "/", null, false, true) ;//$host

$query = sprintf ("INSERT INTO Session SET CreateDate='%s', IP='%s', UID='%s', Language='%s', Agent='%s'",
		  dbDateEncode (time()),
		  $_SERVER["REMOTE_ADDR"],
		  $uid,
		  $_SERVER["HTTP_ACCEPT_LANGUAGE"],
		  $_SERVER["HTTP_USER_AGENT"]								      ) ;
dbQuery ($query) ;


logPrintf (logINFO, "new session, Id %d, IP %s, UID %s, domain %s", dbInsertedId(), $_SERVER["REMOTE_ADDR"], $uid, $host) ;
header (sprintf("Location: %s?%s%sCookie=set", $_SERVER["SCRIPT_NAME"], $_SERVER["QUERY_STRING"], ($_SERVER["QUERY_STRING"]!="") ? "&":"")) ;

exit () ;

?>
