<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Get highest DisplayOrder number
	    $query = sprintf ('SELECT MAX(ReportTable.DisplayOrder) as DisplayOrder FROM ReportTable WHERE ReportTable.ReportId=%d AND ReportTable.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $row = dbFetch ($r) ;
	    dbQueryFree ($r) ;

	    // Default values
	    $Record['DisplayOrder'] = ($row) ? $row['DisplayOrder']+1 : 1 ;
	    break ;
    }
   
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=40 size=40 value=\"%s\" style=\"width:175px;\"></td></tr>\n", htmlentities($Record['Name'])) ;
//  printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%;' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>DisplayOrder</td><td><input type=text name=DisplayOrder maxlength=3 size=3 value=%d></td></tr>\n", htmlentities($Record['DisplayOrder'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Table</td><td><textarea name=TableName style='width:100%%;height:120px;'>%s</textarea></td></tr>\n", $Record['TableName']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Left Join</td><td><input type=checkbox name=DoLeftJoin %s></td></tr>\n", ($Record['DoLeftJoin'])?'checked':'') ;
    printf ("<tr><td class=itemfield>Query</td><td><textarea name=Query style='width:100%%;height:120px;'>%s</textarea></td></tr>\n", $Record['Query']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
