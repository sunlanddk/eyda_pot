<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Get highest DisplayOrder number
	    $query = sprintf ('SELECT MAX(ReportParameter.DisplayOrder) as DisplayOrder FROM ReportParameter WHERE ReportParameter.ReportId=%d AND ReportParameter.Active=1', $Id) ;
	    $r = dbQuery ($query) ;
	    $row = dbFetch ($r) ;
	    dbQueryFree ($r) ;

	    // Default values
	    $Record['DisplayOrder'] = ($row) ? $row['DisplayOrder']+1 : 1 ;
	    $Record['Type'] = 'text' ;
	    break ;
    }

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=40 size=40 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%;' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Type</td><td>%s</td></tr>\n", htmlSelect ('Type style="width:100px;"', $Record['Type'], tableEnumExplode(tableGetType('ReportParameter','Type')))) ;
    itemFieldRaw ('AllowBlank', formCheckbox ('AllowBlank', (int)$Record['AllowBlank'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>DisplayOrder</td><td><input type=text name=DisplayOrder maxlength=3 size=3 value=%d></td></tr>\n", htmlentities($Record['DisplayOrder'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Size</td><td><input type=text name=Size maxlength=3 size=3 value=%d> characters</td></tr>\n", htmlentities($Record['Size'])) ;
    printf ("<tr><td class=itemlabel>Width</td><td><input type=text name=Width maxlength=3 size=3 value=%d> px</td></tr>\n", htmlentities($Record['Width'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Query</td><td><textarea name=Query style='width:100%%;height:150px;'>%s</textarea></td></tr>\n", $Record['Query']) ;
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
