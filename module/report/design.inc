<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;
    
    // Header
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ReportGroupName'])) ;
    printf ("<tr><td class=itemlabel>Report</td><td class=itemfield>%s (%s)</td></tr>\n", htmlentities($Record['Description']), htmlentities($Record['Name'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>ExecuteModule</td><td class=itemfield>%s</td></tr>\n", ($Record['ExecuteModule'])?'yes':'no') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Limit</td><td class=itemfield>%s rows</td></tr>\n", ($Record['Limit'] > 0) ? (int)$Record['Limit'] : 'no') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Layout</td><td class=itemfield>%s</td></tr>\n", ($Record['Landscape'])?'Landscape':'Portrait') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Freeze</td><td class=itemfield>%s column(s)</td></tr>\n", ($Record['FreezeColumn'] > 0) ? (int)$Record['FreezeColumn'] : 'no') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>FitWidth</td><td class=itemfield>%s page(s)</td></tr>\n", ($Record['FitWidth'] > 0 and $Record['FitHeight'] > 0) ? (int)$Record['FitWidth'] : 'no') ;
    printf ("<tr><td class=itemlabel>FitHeight</td><td class=itemfield>%s pages(s)</td></tr>\n", ($Record['FitWidth'] > 0 and $Record['FitHeight'] > 0) ? (int)$Record['FitHeight'] : 'no') ;
    print htmlItemInfo($Record) ;
    print htmlItemSpace() ;
    printf ("</table>\n") ;

    // Parameters
    printf ("<p><b>Parameters</b></p>\n") ;
    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 140) ;
    listHead ('Type') ;
    listHead ('Size', 40, 'align=right') ;
    listHead ('Width', 40, 'align=right') ;
    listHead ('DisplayOrder', 80, 'align=right') ;
    listHead ('', 8) ;
    foreach ($Parameter as $row) {
	listRow () ;
	listFieldIcon ('parameter.gif', 'paramedit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Type']) ;
	listField ((((int)$row['Size']) > 0) ? (int)$row['Size'] : '', 'align=right') ;
	listField ((((int)$row['Width']) > 0) ? (int)$row['Width'] : '', 'align=right') ;
	listField ((int)$row['DisplayOrder'], 'align=right') ;
    }
    if (count($Parameter) == 0) {
	listRow () ;
	listField () ;
	listField ('No Parameters', 'colspan=2') ;
    }
    listEnd () ;

    // Tables
    printf ("<br><p><b>Tables</b></p>\n") ;
    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 140) ;
    listHead ('Table', 100) ;
    listHead ('Query') ;
    listHead ('LeftJoin', 80, 'align=right') ;
    listHead ('DisplayOrder', 80, 'align=right') ;
    listHead ('', 8) ;
    foreach ($Table as $row) {
	listRow () ;
	listFieldIcon ('table.gif', 'tableedit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['TableName']) ;
	listField ($row['Query']) ;
	listField (($row['DoLeftJoin'])?'yes':'', 'align=right') ;
	listField ((int)$row['DisplayOrder'], 'align=right') ;
    }
    if (count($Table) == 0) {
	listRow () ;
	listField () ;
	listField ('No Tables', 'colspan=2') ;
    }
    listEnd () ;
    
    // Fields
    printf ("<br><p><b>Fields</b></p>\n") ;
    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 140) ;
    listHead ('Type', 100) ;
    listHead ('Query') ;
    listHead ('Format', 100) ;
    listHead ('Total', 40, 'align=right') ;
    listHead ('Sort', 40, 'align=right') ;
    listHead ('Group', 40, 'align=right') ;
    listHead ('Width', 40, 'align=right') ;
    listHead ('DisplayOrder', 80, 'align=right') ;
    listHead ('', 8) ;
    foreach ($Field as $row) {
	listRow () ;
	listFieldIcon ('field.gif', 'fieldedit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Type']) ;
	listField ($row['Query']) ;
	listField ($row['Format']) ;
	listField (($row['Total']) ? 'yes' : '', 'align=right') ;
	listField ((((int)$row['SortLevel']) > 0) ? (int)$row['SortLevel'] : '', 'align=right') ;
	listField ((((int)$row['GroupLevel']) > 0) ? (int)$row['GroupLevel'] : '', 'align=right') ;
	listField (($row['Type'] == 'none') ? '' : (int)$row['Width'], 'align=right') ;
	listField ((int)$row['DisplayOrder'], 'align=right') ;
    }
    if (count($Field) == 0) {
	listRow () ;
	listField () ;
	listField ('No Fields', 'colspan=2') ;
    }
    listEnd () ;
        
    return 0 ;
?>
