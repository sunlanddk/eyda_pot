<?php

    require_once 'lib/navigation.inc' ;
    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('Group', 175) ;
    listHead ('', 23) ;
    listHead ('Description') ;
    listHead ('Name', 175) ;
    $LastReportGroupName = '' ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	if ($row['ReportGroupName'] != $LastReportGroupName) {
	    listField ('', 'height=6') ;
	    listRow () ;
	    listField ($row['ReportGroupDescription']) ;
	    $LastReportGroupName = $row['ReportGroupName'] ;
	} else {
	    listField () ;
	}
	listFieldIcon ('report.gif', 'report', $row['Id']) ;
	listField ($row['Description']) ;
	listField ($row['Name']) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField ('No Reports', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
