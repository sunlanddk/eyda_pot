<?php

    require_once 'lib/table.inc' ;
   
    tableDelete ('ReportParameter', $Id) ;

    // Renumber other entries
    $query = sprintf ('SELECT Id, DisplayOrder FROM ReportParameter WHERE ReportParameter.ReportId=%d AND ReportParameter.Active=1 ORDER BY DisplayOrder', (int)$Record['ReportId']) ;
    $result = dbQuery ($query) ;
    $i = 1 ;
    while ($row = dbFetch ($result)) {
	if ($i != (int)$row['DisplayOrder']) {
	    $query = sprintf ("UPDATE ReportParameter SET DisplayOrder=%d WHERE Id=%d", $i, (int)$row['Id']) ;
	    dbQuery ($query) ;
	}
	$i++ ;
    }
    dbQueryFree ($result) ;

    return 0
?>
