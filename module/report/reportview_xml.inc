<?php
session_start();
global $Id,$navParameters;

$navParams = array();
foreach($navParameters as $param){
    $paramArr = explode('=',$param);
    if(isset($paramArr[0]) && !empty($paramArr[1])){
		if(isset($_GET[$paramArr[0]])){
			$navParams[$paramArr[0]] = $_GET[$paramArr[0]];
		}else{
			$navParams[$paramArr[0]] = $paramArr[1];
		}
    }
}
$_SESSION['_sf2_attributes']['paramSet'] = $navParams;
$_SESSION['_sf2_attributes']['navId'] = $Id;
header("Location: ".SFMODULE_PATH."/reports/xmlvalue?nid=2284"."&id=".$Id."&inst=".$_GET['inst']);
