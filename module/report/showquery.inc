<?php

    require_once 'lib/html.inc' ;
    require_once 'module/report/util.inc' ;

    // Generate SQL query and do request to database
    $error = '' ;
    $nothing = array () ;
    $query = reportGenerateQuery ($Record, $Table, $Field, $nothing, $error) ;
    if (is_null($query)) return $error ;
    
    // Header
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ReportGroupName'])) ;
    printf ("<tr><td class=itemlabel>Report</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("</table>\n") ;

    printf ("<br><p><b>Query</b></p>\n<p>%s</p>\n", $query) ;

    return 0 ;
?>
