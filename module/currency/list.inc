<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Name', 60) ;
    listHead ('Symbol', 50) ;
    listHead ('Rate', 80, 'align=right') ;
    listHead ('Description') ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['Name']) ;
	listField ($row['Symbol']) ;
	listField ($row['Rate'], 'align=right') ;
	listField ($row['Description']) ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Currencies', 'colspan=3') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
