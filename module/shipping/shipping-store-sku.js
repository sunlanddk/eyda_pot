
const url = weburl; 
const appstore = new Vue({
	el: "#appStoreSku",
	data: {
		stock 		: 32805,
		storedat 	: '',
		scanned 	: '',
		sscc 		: '',
		gtin 		: '',
		quantity	: '',
		position 	: '',
		stockquantity: '',
		str: '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		preloader: false
	},
	beforeMount(){
		var vm = this;
		// var getItems = getData('http://aub.dk/aub/sfmodule/sw/api/shipping/testalex');
		// vm.getAvailablePosition();
		console.log(this.url);
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		vm.str = '';
	        		if(vm.scanned != '' && vm.position != ''){
	        			vm.positionSku();
	        		}
	        		else{
	        			vm.message = 'Please scan a SKU or SSCC and thereafter a position.';
	        		}	        
	        		return;
	        	}

	            if(vm.str.substring(0,2) != '00' && vm.sscc == '' && vm.gtin == ''){
	            	vm.gtin = vm.str;
	            	vm.storedat = '';
	            	vm.str = '';
	            	vm.quantity = '';
	            	vm.stockquantity = '';
	            	vm.getAvailableStock();
	            	return;
	            }

	            // if(vm.quantity == '' && vm.gtin != ''){
	            // 	vm.quantity = vm.str;
	            // 	vm.str = '';
	            // 	return;
	            // }

	            if(vm.position == ''){
	            	vm.position = vm.str;
	            	vm.str = '';
	            }
	            else{
	            	vm.message = 'Please reset to scan again.';
	            	vm.str = '';	
	            }

	            if(vm.position != ''){
	            	vm.positionSku();
	            }
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.items.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    reset(){
	    	var vm = this;

	    	vm.sscc = '';
    		vm.gtin = '';
    		vm.storedat = '';
    		vm.quantity = '';
    		vm.stockquantity = '';
	    	vm.position = '';
	    	vm.str = '';
	    },
	    getAvailableStock(){
	    	var vm = this;
	    	vm.preloader = true;

	    	var form = {variantcode: vm.gtin, stock: vm.stock};
	    	vm.postData(url+'shipping/sku/get/stock', form, vm.getAvailableStockResponse);
	    },
	    getAvailableStockResponse(response){
	    	var vm = this;
	    	vm.preloader = false;
	    	if(response.error == false){
	    		vm.stockquantity = response.data.qty;
	    		vm.quantity = response.data.qty;
	    		vm.storedat = response.data.storedat;
	    	}
	    	else{
	    		vm.gtin = '';
	    		vm.storedat = '';
	    		vm.quantity = '';
	    		vm.message = response.data;
	    	}

	    },
	    positionSku(){
	    	var vm = this;
	    	vm.preloader = true;
	    	if(vm.gtin != '' && vm.quantity != '' && vm.position != ''){
	    		var form = {scanned: vm.gtin, quantity: vm.quantity, position: vm.position, type: 'gtin', stock: vm.stock, userid: $('.userid').val()};
	    		vm.postData(url+'shipping/store/sku', form, vm.positionSkuResponse);
	    	}
	    	else{
	    		vm.preloader = false;
	    		vm.message = 'Please scan a GTIN or SSCC and thereafter a position.';
	    	}
	    },
	    positionSkuResponse(response){
	    	var vm = this;
	    	// console.log(response);
	    	vm.preloader = false;
	    	if(response.error == false){
	    		if(response.data == 'replenish'){
	    			vm.done = 'Scanned: '+vm.scanned+' has been placed at Replenish';
	    			vm.donesecond = 'position: '+vm.position;
	    		}
	    		else{
	    			vm.done = 'Scanned: '+vm.scanned+' has been placed at Pick Position';
	    			vm.donesecond = 'position: '+vm.position;
	    		}
	    		vm.sscc = '';
	    		vm.gtin = '';
	    		vm.quantity = '';
	    		vm.storedat = '';
	    		vm.stockquantity = '';
	    		vm.position = '';
	    		// vm.getAvailablePosition();
	    	}
	    	else{
	    		vm.message = 'An error occured, try by scanning again. Error message: ' +JSON.stringify(response.errorcode);
	    		vm.sscc = '';
	    		vm.gtin = '';
	    		vm.quantity = '';
	    		vm.stockquantity = '';
	    		vm.storedat = '';
	    		vm.position = '';
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {
			    	console.log(data);
			    	callback(data);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	vm.message = 'An error occured, try by scanning again. Error message: ' +textStatus;
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			     	// return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});



