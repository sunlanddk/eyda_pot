<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

        

    ?>
    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   
    <div id="appStoreSku">
        <p class="hiddenkeyboard">{{str}}</p>
        <center>
            <h3 style="color: red">{{message}}</h3>
            <h3 style="color: green">{{done}}</h3>
            <h3 style="color: green">{{donesecond}}</h3>
            <h3>(scan GTIN and find position)</h3>
            

            <h2>Scanned: {{gtin}}</h2>
            <h2 v-if="positions.pick != ''" >Pick: {{positions.pick}} ( {{parseInt(positions.qty)}} ) </h2>
            <h2 v-if="positions.replenish != ''" >Replenish: {{positions.replenish}}</h2>
            <!-- <h2>Position: {{storedat}}</h2>
            <h2 v-if="gtin != ''">GTIN Quantity: {{stockquantity}} </h2> -->

        </center>
        </br>
        </br>
        </br>
        <div v-if="preloader" class="preloader"><div class="rotator"></div></div>
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js?v=2"></script>
<script src="<?php echo _LEGACY_URI; ?>/module/shipping/findgtin.js?v=5"></script>
<style>
    #appStore{
        position: relative;
    }
    #appStore table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        /*float: left;*/
        display: inline-block;
        cursor: pointer;
    }
    .form{
    	text-align: center;
    }
    h3{
        font-size: 15px;
    }
    h2{
        font-size: 18px;
    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .preloader{
        position: fixed;
        z-index: 99999999;
        left: 0;
        top: 0;
        background: rgba(255, 255, 255, 0.7);
        width: 100%;
        height: 100%;
    }
    .preloader .rotator{
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border: 5px solid #3b9ac4;
        border-radius: 50%;
        border-top: 5px solid #01205a;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
    select{
        padding: 10px 10px;
        border: solid 1px black;
        background: grey;
        color:  white;
        margin: 10px 0;
        border-radius: 10px;
    }
</style>
<?php

    return 0 ;

?>
