<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/variant.inc' ;

    ?>
    <script src="<?php echo _LEGACY_URI; ?>/lib/vue.js"></script>
   
    <div id="appStore">
        <p class="hiddenkeyboard">{{str}}</p>
        <center>
            <h3 style="color: red">{{message}}</h3>
            <h3 style="color: green">{{done}}</h3>
            <h3 style="color: green">{{donesecond}}</h3>
            <h2>Variants available at position: {{nextposition}}</h2>
            <h2 style="color: blue" v-for="(varitant, index) in nextpositions">
                Variant {{varitant.VariantCode}} at position: {{varitant.position}}
            </h2>
            <h2>(Scan Position and SSCC to replenish variant)</h2>
            <h2>Position scanned: {{position}}</h2>
            <h2 v-if="position != ''">SSCC scanned: {{sscc}}</h2>
            <h2 v-if="sscc != ''">(Scan Position again to replenish)</h2>
            <h2 v-if="sscc != ''">Replenish to position: {{nextposition}}</h2>
        </center>
        </br>
        </br>
        </br>
        <center>
            <div class="form">
                <div class="button" v-on:click="replenishVariant()" >Replenish variant</div>
                <br>
                <div class="button" v-on:click="reset()" >Reset</div>
            </div>
        </center>
        <div v-if="preloader" class="preloader"><div class="rotator"></div></div>
    </div>

<script src="<?php echo _LEGACY_URI; ?>/lib/config.js"></script>
<script src="<?php echo _LEGACY_URI; ?>/lib/closest-sscc.js"></script>

<style>
    #appStore{
        position: relative;
    }
    #appStore table.list tr:nth-child(even){
        background: #EEEEEE;
    }
    .listtall{
        height: 30px;
        line-height: 30px;
    }
    .button{
        padding: 0 20px !important;
        line-height: 30px !important;
        width: 100px;
        height: 30px !important;
        background: grey;
        color: white;
        border-radius: 5px;
        margin-right: 25px;
        /*float: left;*/
        cursor: pointer;
        margin: auto;

    }
    .hiddenkeyboard{
        position: absolute;
        top: 0;
        right: 0;
        font-size: 14px;
        color: black;
    }
    @keyframes spin {
        0% { transform: rotate(0deg); }
        100% { transform: rotate(360deg); }
    }
    .preloader{
        position: fixed;
        z-index: 99999999;
        left: 0;
        top: 0;
        background: rgba(255, 255, 255, 0.7);
        width: 100%;
        height: 100%;
    }
    .preloader .rotator{
        position: absolute;
        top: 50%;
        left: 50%;
        margin-top: -15px;
        margin-left: -15px;
        border: 5px solid #3b9ac4;
        border-radius: 50%;
        border-top: 5px solid #01205a;
        width: 30px;
        height: 30px;
        -webkit-animation: spin 2s linear infinite;
        animation: spin 2s linear infinite;
    }
</style>
<?php

    return 0 ;

?>
