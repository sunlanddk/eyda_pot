
const url = weburl; 
const appstore = new Vue({
	el: "#appStoreSku",
	data: {
		stock 		: '',
		storedat 	: '',
		scanned 	: '',
		sscc 		: '',
		gtin 		: '',
		quantity	: '',
		position 	: '',
		stockquantity: '',
		updatedQuantity : '',
		str: '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		preloader: false
	},
	beforeMount(){
		var vm = this;
		// var getItems = getData('http://aub.dk/aub/sfmodule/sw/api/shipping/testalex');
		// vm.getAvailablePosition();
		console.log(this.showButton)
		console.log(this.url);
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {

                if(vm.position != '' && vm.str.length < 10) {
                    vm.position = vm.str;
					vm.gtin = '';
					vm.quantity = '';
					vm.storedat = '';
					vm.stockquantity = '';
                    vm.str = '';
					return;
                }

				if(vm.position == '') {
                    vm.position = vm.str;
                    vm.str = '';
					return;
                }

				

                if(vm.gtin == '') {
                    vm.gtin = vm.str;
                    vm.str = '';
                }

                if(vm.position != '' && vm.gtin != '') {
                    vm.getStockByPosition();
					return;
                }

	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.items.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    reset(){
	    	var vm = this;

	    	vm.sscc = '';
    		vm.gtin = '';
    		vm.storedat = '';
    		vm.quantity = '';
    		vm.stockquantity = '';
	    	vm.position = '';
	    	vm.str = '';
	    },
	    getStockByPosition(){
            var vm = this;
            vm.preloader = true;

	    	var form = {variantcode: vm.gtin, position: vm.position};

	    	vm.postData(url+'shipping/get/stockbyposition', form, vm.getStockByPositionResponse);
	    },
	    getStockByPositionResponse(response){
	    	var vm = this;
	    	vm.preloader = false;
	    	if(response.error == false){
				vm.quantity = response.data.qty
	    		vm.updatedQuantity = vm.quantity
	    		
			}
	    	else{
	    		vm.gtin = '';
	    		vm.message = response.data;
	    	}
	    },
        changeQuantity(value) {
            var vm = this;
            vm.updatedQuantity = vm.updatedQuantity + value
        },
		postUpdateQuantity() {
			var vm = this
			vm.preloader = true

			var form = {position: vm.position, variantcode: vm.gtin, oldQuantity: vm.quantity, updatedQuantity: vm.updatedQuantity}

			vm.postData(url+'shipping/post/update/quantity/item', form, vm.postUpdateQuantityResponse)
		},

		postUpdateQuantityResponse(response) {
			var vm = this
			vm.preloader = false

			if(response.error == false) {
				var qty = (vm.quantity - vm.updatedQuantity)
				if(qty > 0){
					vm.done = "Stock updated. Added "+qty+" items"
				}else if(qty < 0){
					vm.done = "Stock updated. Removed "+qty+" items"
				}else {
					vm.done = "Stock not updated."
				}

				vm.quantity = '';
				vm.gtin = '';
				vm.position = '';
				vm.updatedQuantity = '';

			}else {	
				vm.error = "Error occurred"
			}
		},
	    postData(url, data, callback){
	 		var vm = this;
			 console.log(url)
			$.ajax({
			    url : url,
			    type: "POST",
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {
			    	console.log(data);
			    	callback(data);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	vm.message = 'An error occured, try by scanning again. Error message: ' +textStatus;
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			     	// return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});
