<?php 

function GetCheckDigitSscc($barcode){
	//Compute the check digit
	$sum=0;
	$barcodeArray = str_split($barcode);
	$i = 0;
	foreach ($barcodeArray as $key => $value) {
		$i ++;
		# code...
		if($i%2 == 0){
			$sum += ( 1 * $value);
			continue;
		}

		$sum += ( 3 * $value);
		continue;
	}
	$r=$sum%10;
	if($r>0)
		$r=10-$r;
	return $r;
}

function createSsccString($containerNumber){
	$ai = '00';
	$containerNumberWZero = sprintf('%09s', $containerNumber);
	// $checkDigit = GetCheckDigitSscc($containerNumberWZero);
	
	// $ssccCode = (string) '('.$ai.') '.$containerNumberWZero.' '.$checkDigit;
	$ssccCode = (string) '('.$ai.') '.$containerNumberWZero;
	return $ssccCode;
}


// function createSsccBarcodeString($containerNumber){
// 	$ai = '00';
// 	$prefix = 3;
// 	$companyPrefix = 5714139;
// 	$containerNumberWZero = sprintf('%09d', $containerNumber);
// 	$checkDigit = GetCheckDigitSscc($prefix.$companyPrefix.$containerNumberWZero);
	
// 	$ssccCode = (string) $ai.$prefix.$companyPrefix.$containerNumberWZero.$checkDigit;
// 	return $ssccCode;
// }

function createSsccBarcodeString($containerNumber){
	$ai = '00';
	$containerNumberWZero = sprintf('%09s', $containerNumber);
	// $checkDigit = GetCheckDigitSscc($containerNumberWZero);
	
	// $ssccCode = (string) $ai.$containerNumberWZero.$checkDigit;
	$ssccCode = (string) $ai.$containerNumberWZero;
	return $ssccCode;
}

function createSsccCode($containerNumber, $prefixGs1 = ''){
	require_once(_LEGACY_LIB.'/lib/parameter.inc');

	$prefix = 3;
	if($prefixGs1 != ''){
		$companyPrefix = $prefixGs1;
		$totalprefix = (string)$prefix . (string)$prefixGs1;
	}
	else{
		$companyPrefix = 5714139;
		$totalprefix = parameterGet('ssccprefix');
	}
	$containerNumberWZero = sprintf('%09s', $containerNumber);
	$checkDigit = GetCheckDigitSscc($containerNumberWZero);
	$ssccCode = (string) $totalprefix.$containerNumberWZero.$checkDigit;
	return $ssccCode;
}

function createVariantSscc($variant, $qty){
	$variant = str_replace(' ', '', $variant);
	$ean = sprintf('%014s', $variant);
	if($qty === false){
		return '(02) '.$ean;	
	}
	return '(02) '.$ean.' (37) '.$qty;
}
function createVariantSsccCode($variant, $qty){
	$variant = str_replace(' ', '', $variant);
	$ean = sprintf('%014s', $variant);

	if($qty === false){
		return '02'.$ean;	
	}
	return '02'.$ean.'37'.$qty;
}
function createAiSscc($ai, $variant){
	return $ai.''.$variant;
}

function createAiSsccCode($ai, $variant){
	return '('.$ai.') '.$variant;
}


$sscc = array(
	'containers' => [
		array(
			'id' => '666888', // container id
			'sscc' => '15705327000000001', // can be empty, then we create a number
			'checkdigit' => '7',
			'from' => array(
				'name' => 'Morsø Sko Import A/S',
				'street' => 'Fabriksvej 7',
				// 'street2' => '',
				'city' => 'Skive',
				'zip' => '7800',
				'country' => 'DK',
			),
			'to' => array(
				'name' => 'Coop Odense Nonfood Center',
				'street' => 'Hestehaven 51',
				'street2' => 'test street 2',
				'city' => 'Odense S',
				'zip' => '5260',
				'country' => 'DK',
			),
			'item' => array(
				'variantcode' => '1 575000 708066 7',
				'quantity' => '38',
				'no1text' => 'Coop dessin no.: ',
				'no1' => '708066',
				'no2text' => 'Coop PO no.: ',
				'no2' => '086-12345',
			),
		),
	],
);

function createSSCCLabelFromGui($id){
	define('FPDF_FONTPATH','lib/font/');

	require_once 'lib/fpdf.inc' ;
	require_once 'lib/barcode128.inc' ;

	$query = sprintf ("SELECT stock.*, country.Name as countryName FROM stock LEFT JOIN country ON country.Id=stock.CountryId  WHERE stock.Id=%d", $id) ;
	$Result = dbQuery ($query) ;
	$customsscc = dbFetch ($Result) ;
	dbQueryFree ($Result) ;

	$query = sprintf ("SELECT c.*, country.Name as countryName FROM Company c LEFT JOIN country ON country.Id=c.CountryId WHERE c.Id=%d", $customsscc['FromCompanyId']) ;
	$Result = dbQuery ($query) ;
	$company = dbFetch ($Result) ;
	dbQueryFree ($Result) ;

	$pdf = new PDF_Code128('P', 'mm', array(102,152));

	$count = 1;
	$firstPrefix = (int)$customsscc['FirstSSCCIndex'];
	while ($count <= (int)$customsscc['NoPallets']) {
		# code...
		$pdf->AddPage();
		$pdf->SetFont('helvetica','',9);
		// Ship from ( Zone A )

		$x = 8;
		$y = 0;
		$pdf->Image(_LEGACY_LIB.'/image/thumbnails/loginmain.jpg', 109, 8, 35, 16);
		// $pdf->Line( 51, 0, 51, 25 ) ;
		$pdf->Line( 0, 20, 102, 20 ) ;
		$pdf->Line( 0, 60, 144, 60 ) ;
		$pdf->Line( 0, 95, 144, 95 ) ;

		$pdf->SetFont('helvetica','',10);

		$x = 5;
		$y = 5;
		//Afhentes container
		$pdf->setXY ($x, $y);
		$pdf->SetFont('helvetica','B',14);
		$pdf->Cell(92, 4, $company['Name'], 0, 0, 'C') ;
		$pdf->SetFont('helvetica','',12);
		$pdf->setXY ($x, $y+=6);
		$pdf->Cell(92, 4, $company['Address1'] . ', '.$company['ZIP'].' '.$company['City'].' '.$company['countryName'], 0, 0, 'C') ;

		//Leveres container
		$y += 10;
		$pdf->SetFont('helvetica','',12);
		$pdf->setXY ($x+=10, $y+=4.5);
		$pdf->Cell(50, 4, 'Delivery:', 0, 0, 'L') ;
		$pdf->setXY ($x+=20, $y);
		$pdf->Cell(50, 4, $customsscc['Name'], 0, 0, 'L') ;
		$pdf->setXY ($x, $y+=4.5);
		$pdf->Cell(50, 4, $customsscc['Address1'], 0, 0, 'L') ;
		$pdf->setXY ($x, $y+=4.5);
		$pdf->Cell(50, 4, $customsscc['ZIP'].' '.$customsscc['City'].' '.$customsscc['countryName'], 0, 0, 'L') ;
		$x = 15;
		$pdf->setXY ($x, $y+=10);
		$pdf->MultiCell(72, 4, $customsscc['DeliveryComment'], 0, 'L', false) ;

		$pdf->SetFont('helvetica','',8);
		$x = 8;
		$pdf->setXY ( $x, 92);
		//$pdf->Cell(136, 4, $containerdata['footer']['name']. ' - ' . $containerdata['footer']['street'] . ' - ' .$containerdata['footer']['zip'].' '.$containerdata['footer']['city'] . ' - Tel.:' .$containerdata['footer']['phone'] , 0, 0, 'C') ;

		$y += 20;
		$x = 5;
		$pdf->setXY ($x, $y);
		$pdf->SetFont('helvetica','B',12);
		$pdf->Cell(50, 4, 'SSCC', 0, 0, 'L') ;
		$pdf->setXY ($x, $y+=6);
		$pdf->SetFont('helvetica','',12);
		$pdf->Cell(50, 4, createSsccCode($firstPrefix, $customsscc['GS1Prefix']), 0, 0, 'L') ;
		//$pdf->Cell(50, 4, createSsccString(createSsccCode($customsscc['GS1Prefix'])), 0, 0, 'L') ;

		$pdf->setXY ($x, $y+=9);
		$pdf->SetFont('helvetica','B',12);
		$pdf->Cell(50, 4, 'Content', 0, 0, 'L') ;
		$pdf->setXY ($x, $y+=6);
		$pdf->SetFont('helvetica','',12);
		$pdf->Cell(50, 4, sprintf('%014s', $customsscc['Content']), 0, 0, 'L') ;
		$pdf->Cell(40, 4, 'Count: '.$customsscc['ContentQty'], 0, 0, 'R') ;


		$y += 15;
		$x = 5;
		$pdf->SetFont('helvetica','', 10);
		$pdf->setXY ($x,$y);
		$pdf->Code128($x+15,$y, createAiSscc('02',sprintf('%014s', $customsscc['Content'])).createAiSscc('37',$customsscc['ContentQty']),62,15);
		$pdf->setXY ($x,$y+=16);
		$pdf->Cell(92, 4, createAiSsccCode('02', sprintf('%014s', $customsscc['Content'])).' '.createAiSsccCode('37', $customsscc['ContentQty']), 0, 0, 'C') ;

		$pdf->setXY ($x,$y+21);
		$pdf->Cell(92, 10, createSsccString(createSsccCode($firstPrefix, $customsscc['GS1Prefix'])), 0, 0, 'C') ;
		$pdf->Code128($x+15,$y+8, '00'.createSsccCode($firstPrefix, $customsscc['GS1Prefix']),62,15);

		$count++;
		$firstPrefix++;
	}

	return $pdf->Output('develstore/sscc/custom_label_'.$id.'.pdf', 'S');

}


function createShippingLabel($containerdata, $stockId){
	define('FPDF_FONTPATH','lib/font/');
	require_once 'lib/fpdf.inc' ;
	require_once 'lib/barcode128.inc' ;
	// require_once 'module/sscc.inc' ;


	// $stockId = $stockId; //$stock['stockId'];
	$pdf = new PDF_Code128('L', 'mm', array(102,152));


	$numberOfContainers = count($stock['containers']);
	$i = 0;

	foreach ($containerdata['containers'] as $key => $container) {
		$i ++;
		$pdf->AddPage();
		$pdf->SetFont('helvetica','',9);
		// Ship from ( Zone A )

		$x = 8;
		$y = 0;
		$pdf->Image(_LEGACY_LIB.'/layout/new/images/logopasson.jpg', 109, 8, 35, 16);
		// $pdf->Line( 51, 0, 51, 25 ) ;
		$pdf->Line( 8, 30, 144, 30 ) ;
		$pdf->Line( 8, 90, 144, 90 ) ;
		$pdf->Line( 8, 62.5, 144, 62.5 ) ;
		$pdf->Line( 8, 78.75, 110, 78.75 ) ;
		$pdf->Line( 110, 62.5, 110, 90 ) ;
		$pdf->Line( 8, 30, 8, 90 ) ;
		$pdf->Line( 76, 30, 76, 90 ) ;
		$pdf->Line( 144, 30, 144, 90 ) ;

		$pdf->SetFont('helvetica','',10);

		$x = 9;
		$y = 32;
		//Afhentes container
		$pdf->setXY ($x, $y);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(50, 4, 'Afhentes', 0, 0, 'L') ;
		$pdf->SetFont('helvetica','',9);
		$pdf->setXY ($x, $y+=4.5);
		$pdf->Cell(50, 4, $containerdata['from']['name'], 0, 0, 'L') ;
		$pdf->setXY ($x, $y+=4.5);
		$pdf->Cell(50, 4, $containerdata['from']['street'], 0, 0, 'L') ;
		$pdf->setXY ($x, $y+=4.5);
		$pdf->Cell(50, 4, $containerdata['from']['zip'].' '.$containerdata['from']['city'].' '.$containerdata['from']['country'], 0, 0, 'L') ;

		//Leveres container
		$y = 32;
		$pdf->setXY ($x+69, $y);
		$pdf->SetFont('helvetica','B',9);
		$pdf->Cell(50, 4, 'Leveres', 0, 0, 'L') ;
		$pdf->SetFont('helvetica','',9);
		$pdf->setXY ($x+69, $y+=4.5);
		$pdf->Cell(50, 4, $containerdata['to']['name'], 0, 0, 'L') ;
		$pdf->setXY ($x+69, $y+=4.5);
		$pdf->Cell(50, 4, $containerdata['to']['street'], 0, 0, 'L') ;
		$pdf->setXY ($x+69, $y+=4.5);
		$pdf->Cell(50, 4, $containerdata['to']['zip'].' '.$containerdata['to']['city'].' '.$containerdata['to']['country'], 0, 0, 'L') ;

		//Dato

		//Fragtbrev


		//Gods

		//Antal

		//Totalvægt

		//Total M3

		//Footer
		$pdf->SetFont('helvetica','',8);
		$x = 8;
		$pdf->setXY ( $x, 92);
		$pdf->Cell(136, 4, $containerdata['footer']['name']. ' - ' . $containerdata['footer']['street'] . ' - ' .$containerdata['footer']['zip'].' '.$containerdata['footer']['city'] . ' - Tel.:' .$containerdata['footer']['phone'] , 0, 0, 'C') ;


		/*if($containerdata['type'] != 'inbound'){
			$pdf->Line( 51, 0, 51, 25 ) ;
			$pdf->Line( 0 , $y+25, 152 ,$y+25 ) ;

			// From
			$pdf->SetFont('helvetica','',7.5);
			$pdf->setXY ( 3, 4);
			$pdf->Cell(46, 4, 'From', 0, 0, 'L') ;
			$pdf->setXY ( 3, 8);
			$pdf->Cell(46, 4, $containerdata['from']['name'], 0, 0, 'L') ;
			$pdf->setXY ( 3, 12 );
			$pdf->Cell(46, 4, $containerdata['from']['street'], 0, 0, 'L') ;
			if($containerdata['from']['street2'] != '' ){
				$pdf->setXY ( 3, 16 );
				$pdf->Cell(46, 4, $containerdata['from']['street2'], 0, 0, 'L') ;
				$pdf->setXY ( 3, 20 );
			}
			else{
				$pdf->setXY ( 3, 16 );
			}
			$pdf->Cell(46, 4, $containerdata['from']['zip'].' '.$containerdata['from']['city'] .' '. $containerdata['from']['country'], 0, 0, 'L') ;

			// To
			$pdf->setXY ( 54, 4);
			$pdf->Cell(46, 4, 'To', 0, 0, 'L') ;
			$pdf->setXY ( 54, 8);
			$pdf->Cell(46, 4, $containerdata['to']['name'], 0, 0, 'L') ;
			$pdf->setXY ( 54, 12 );
			$pdf->Cell(46, 4, $containerdata['to']['street'], 0, 0, 'L') ;
			if($containerdata['to']['street2'] != '' ){
				$pdf->setXY ( 54, 16 );
			$pdf->Cell(46, 4, $containerdata['to']['street2'], 0, 0, 'L') ;
				$pdf->setXY ( 54, 20 );
			}
			else{
				$pdf->setXY ( 54, 16 );
			}
			$pdf->Cell(46, 4, $containerdata['to']['zip'].' '.$containerdata['to']['city'] .' '. $containerdata['to']['country'], 0, 0, 'L') ;

			
			$y = 22;
		}*/
		/*
		$pdf->SetFont('helvetica','B',10) ;
		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+=5);
		if(isset($container['sscc'])){
			$ssccstring = $container['sscc'];
			// $sscc = $container['sscc'];
		}
		else{
			$ssccstring = createSsccCode($container['id']);
			// $sscc = $container['id'];
			// $ssccstring = 'test';
		}

		$pdf->setXY ($x,$y);
		$pdf->Cell(92, 4, 'Variant: '.$container['name'], 0, 0,'C') ;
		if(isset($container['ai']['37']) === true){
			$pdf->setXY ($x,$y+=4.5);
			$pdf->Cell(92, 4, 'Quantity: '.$container['ai']['37'], 0, 0, 'C') ;
		}
		if(isset($container['ai']['15']) === true){
			$pdf->setXY ($x,$y+=4.5);
			$pdf->Cell(92, 4, 'Best before: '.$container['ai']['15'], 0, 0, 'C') ;
		}
		if(isset($container['ai']['10']) === true){
			$pdf->setXY ($x,$y+=4.5);
			$pdf->Cell(92, 4, 'Batch: '.$container['ai']['10'], 0, 0, 'C') ;
		}
		

		$pdf->SetFont('helvetica','',10) ;
		if(isset($container['ai']['02']) === true){
			$pdf->setXY ($x,$y+=10);
			$pdf->Code128($x+15,$y, createVariantSsccCode($container['ai']['02'], (isset($container['ai']['37']) === true ? $container['ai']['37'] : false) ),62,10);
			$pdf->setXY ($x,$y+12);
			$pdf->Cell(92, 4, createVariantSscc($container['ai']['02'], (isset($container['ai']['37']) === true ? $container['ai']['37'] : false) ), 0, 0, 'C') ;
		}

		$ai10printed = false;
		if(isset($container['ai']['15']) === true){
			if(isset($container['ai']['10']) === true){
				$ai10printed = true;
				$y += 22;
				$pdf->setXY ($x,$y);
				$pdf->Code128($x+15,$y, createAiSscc('15',$container['ai']['15']).createAiSscc('10',$container['ai']['10']),62,10);
				$pdf->setXY ($x,$y+12);
				$pdf->Cell(92, 4, createAiSsccCode('15', $container['ai']['15']).' '.createAiSsccCode('10', $container['ai']['10']), 0, 0, 'C') ;
			}
			else{
				$y += 22;
				$pdf->setXY ($x,$y);
				$pdf->Code128($x+15,$y, createAiSscc('15',$container['ai']['15']),62,10);
				$pdf->setXY ($x,$y+12);
				$pdf->Cell(92, 4, createAiSsccCode('15', $container['ai']['15']), 0, 0, 'C') ;	
			}
		}

		if(isset($container['ai']['10']) === true && $ai10printed === false){
			$y += 22;
			$pdf->setXY ($x,$y);
			$pdf->Code128($x+15,$y, createAiSscc('10',$container['ai']['10']),62,10);
			$pdf->setXY ($x,$y+12);
			$pdf->Cell(92, 4, createAiSsccCode('10', $container['ai']['10']), 0, 0, 'C') ;
		}

		$y = 110;
		$pdf->setXY ($x,$y+=5);
		$pdf->SetFont('helvetica','B',12) ;
		$pdf->Cell(92, 4, 'SSCC', 0, 0, 'C') ;

		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+22);
		$pdf->Cell(92, 10, createSsccString($ssccstring), 0, 0, 'C') ;
		$pdf->Code128($x+15,$y+8, '00'.$ssccstring,62,15);
		$pdf->setX ($x);
		*/
		

		//$pdf->Output('develstore/sscc/sscc_'.$stockId.'.pdf', 'F');
	}




	return $pdf->Output('develstore/sscc/shipping_label_'.$stockId.'.pdf', 'S');
}

function createSsccLabelAndAi($containerdata, $stockId){

	define('FPDF_FONTPATH','lib/font/');
	require_once 'lib/fpdf.inc' ;
	require_once 'lib/barcode128.inc' ;
	// require_once 'module/sscc.inc' ;


	// $stockId = $stockId; //$stock['stockId'];
	$pdf = new PDF_Code128('P', 'mm', array(102,152));

	$numberOfContainers = count($stock['containers']);
	$i = 0;

	foreach ($containerdata['containers'] as $key => $container) {
		$i ++;
		$pdf->AddPage();
		$pdf->SetFont('helvetica','',10);
		// Ship from ( Zone A )

		$x = 5;
		$y = 0;
		/*if($containerdata['type'] != 'inbound'){
			$pdf->Line( 51, 0, 51, 25 ) ;
			$pdf->Line( 0 , $y+25, 152 ,$y+25 ) ;

			// From
			$pdf->SetFont('helvetica','',7.5);
			$pdf->setXY ( 3, 4);
			$pdf->Cell(46, 4, 'From', 0, 0, 'L') ;
			$pdf->setXY ( 3, 8);
			$pdf->Cell(46, 4, $containerdata['from']['name'], 0, 0, 'L') ;
			$pdf->setXY ( 3, 12 );
			$pdf->Cell(46, 4, $containerdata['from']['street'], 0, 0, 'L') ;
			if($containerdata['from']['street2'] != '' ){
				$pdf->setXY ( 3, 16 );
				$pdf->Cell(46, 4, $containerdata['from']['street2'], 0, 0, 'L') ;
				$pdf->setXY ( 3, 20 );
			}
			else{
				$pdf->setXY ( 3, 16 );
			}
			$pdf->Cell(46, 4, $containerdata['from']['zip'].' '.$containerdata['from']['city'] .' '. $containerdata['from']['country'], 0, 0, 'L') ;

			// To
			$pdf->setXY ( 54, 4);
			$pdf->Cell(46, 4, 'To', 0, 0, 'L') ;
			$pdf->setXY ( 54, 8);
			$pdf->Cell(46, 4, $containerdata['to']['name'], 0, 0, 'L') ;
			$pdf->setXY ( 54, 12 );
			$pdf->Cell(46, 4, $containerdata['to']['street'], 0, 0, 'L') ;
			if($containerdata['to']['street2'] != '' ){
				$pdf->setXY ( 54, 16 );
			$pdf->Cell(46, 4, $containerdata['to']['street2'], 0, 0, 'L') ;
				$pdf->setXY ( 54, 20 );
			}
			else{
				$pdf->setXY ( 54, 16 );
			}
			$pdf->Cell(46, 4, $containerdata['to']['zip'].' '.$containerdata['to']['city'] .' '. $containerdata['to']['country'], 0, 0, 'L') ;

			
			$y = 22;
		}*/

		$pdf->SetFont('helvetica','B',10) ;
		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+=5);
		if(isset($container['sscc'])){
			$ssccstring = $container['sscc'];
			// $sscc = $container['sscc'];
		}
		else{
			$ssccstring = createSsccCode($container['id']);
			// $sscc = $container['id'];
			// $ssccstring = 'test';
		}

		$pdf->setXY ($x,$y);
		$pdf->Cell(92, 4, 'Variant: '.$container['name'], 0, 0,'C') ;
		if(isset($container['ai']['37']) === true){
			$pdf->setXY ($x,$y+=4.5);
			$pdf->Cell(92, 4, 'Quantity: '.$container['ai']['37'], 0, 0, 'C') ;
		}
		if(isset($container['ai']['15']) === true){
			$pdf->setXY ($x,$y+=4.5);
			$pdf->Cell(92, 4, 'Best before: '.$container['ai']['15'], 0, 0, 'C') ;
		}
		if(isset($container['ai']['10']) === true){
			$pdf->setXY ($x,$y+=4.5);
			$pdf->Cell(92, 4, 'Batch: '.$container['ai']['10'], 0, 0, 'C') ;
		}
		

		$pdf->SetFont('helvetica','',10) ;
		if(isset($container['ai']['02']) === true){
			$pdf->setXY ($x,$y+=10);
			$pdf->Code128($x+15,$y, createVariantSsccCode($container['ai']['02'], (isset($container['ai']['37']) === true ? $container['ai']['37'] : false) ),62,10);
			$pdf->setXY ($x,$y+12);
			$pdf->Cell(92, 4, createVariantSscc($container['ai']['02'], (isset($container['ai']['37']) === true ? $container['ai']['37'] : false) ), 0, 0, 'C') ;
		}

		$ai10printed = false;
		if(isset($container['ai']['15']) === true){
			if(isset($container['ai']['10']) === true){
				$ai10printed = true;
				$y += 22;
				$pdf->setXY ($x,$y);
				$pdf->Code128($x+15,$y, createAiSscc('15',$container['ai']['15']).createAiSscc('10',$container['ai']['10']),62,10);
				$pdf->setXY ($x,$y+12);
				$pdf->Cell(92, 4, createAiSsccCode('15', $container['ai']['15']).' '.createAiSsccCode('10', $container['ai']['10']), 0, 0, 'C') ;
			}
			else{
				$y += 22;
				$pdf->setXY ($x,$y);
				$pdf->Code128($x+15,$y, createAiSscc('15',$container['ai']['15']),62,10);
				$pdf->setXY ($x,$y+12);
				$pdf->Cell(92, 4, createAiSsccCode('15', $container['ai']['15']), 0, 0, 'C') ;	
			}
		}

		if(isset($container['ai']['10']) === true && $ai10printed === false){
			$y += 22;
			$pdf->setXY ($x,$y);
			$pdf->Code128($x+15,$y, createAiSscc('10',$container['ai']['10']),62,10);
			$pdf->setXY ($x,$y+12);
			$pdf->Cell(92, 4, createAiSsccCode('10', $container['ai']['10']), 0, 0, 'C') ;
		}

		$y = 110;
		$pdf->setXY ($x,$y+=5);
		$pdf->SetFont('helvetica','B',12) ;
		$pdf->Cell(92, 4, 'SSCC', 0, 0, 'C') ;

		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+22);
		$pdf->Cell(92, 10, createSsccString($ssccstring), 0, 0, 'C') ;
		$pdf->Code128($x+15,$y+8, '00'.$ssccstring,62,15);
		$pdf->setX ($x);
		

		//$pdf->Output('develstore/sscc/sscc_'.$stockId.'.pdf', 'F');
	}

	return $pdf->Output('develstore/sscc/sscc_'.$stockId.'.pdf', 'S');
}


function createAdrLabel($containerdata, $stockId){

	define('FPDF_FONTPATH','lib/font/');
	require_once 'lib/fpdf.inc' ;
	require_once 'lib/barcode128.inc' ;
	// require_once 'module/sscc.inc' ;


	// $stockId = $stockId; //$stock['stockId'];
	$pdf = new PDF_Code128('P', 'mm', array(102,152));
//	$pdf = new PDF_Code128('P', 'mm', 'A4');

	$numberOfContainers = count($containerdata['containers']);
	$i = 0;

	foreach ($containerdata['containers'] as $key => $container) {
		$i ++;
		$pdf->AddPage();
		$pdf->SetFont('helvetica','',10);
		// Ship from ( Zone A )

		$x = 5;
		$y = 0;
//		$fontsize = 8 ;
	    $ImageString = sprintf (_LEGACY_LIB.'/image/logo/%d.jpg', (int)787) ;
		$pdf->Image ($ImageString,  61, 10, 30 ) ;
		if($containerdata['type'] != 'inbound'){

			$pdf->Line( 2 , $y+25, 100 ,$y+25 ) ;
			
			$pdf->Line( 2, 25, 2, 74 ) ;
			$pdf->Line( 51, 25, 51, 50 ) ;
			$pdf->Line( 100, 25, 100, 74 ) ;
			$y += 25;
			$pdf->Line( 2 , $y+25, 100 ,$y+25 ) ;

			$pdf->Line( 33, 50, 33, 74 ) ;
			$pdf->Line( 66, 50, 66, 74 ) ;
			// From
			$pdf->SetMargins(3,0,0) ;
			$pdf->SetY ($y) ;
			$pdf->SetFont('helvetica','B',8);
			//$pdf->ln();
			$pdf->Cell(46, 4, 'Afhentes', 0, 1, 'L') ;
			$pdf->SetFont('helvetica','',7.5);
			$pdf->Cell(46, 4, $containerdata['from']['name'], 0, 1, 'L') ;
			$pdf->Cell(46, 4, $containerdata['from']['street'], 0, 1, 'L') ;
			if($containerdata['from']['street2'] != '' ){
				$pdf->Cell(46, 4, $containerdata['from']['street2'], 0, 1, 'L') ;
			}
			$pdf->Cell(46, 4, $containerdata['from']['zip'].' '.$containerdata['from']['city'], 0, 1, 'L') ;
			$pdf->Cell(46, 4, $containerdata['from']['country'], 0, 1, 'L') ;

			// To
			$pdf->SetMargins(53,0,0) ;
			$pdf->SetY ($y) ;
			$pdf->SetFont('helvetica','B',8);
			$pdf->Cell(46, 4, 'Leveres', 0, 1, 'L') ;
			$pdf->SetFont('helvetica','',7.5);
			$pdf->Cell(46, 4, $containerdata['to']['name'], 0, 1, 'L') ;
			$pdf->Cell(46, 4, $containerdata['to']['street'], 0, 1, 'L') ;
			if($containerdata['to']['street2'] != '' ){
				$pdf->Cell(46, 4, $containerdata['to']['street2'], 0, 1, 'L') ;
			}
			$pdf->Cell(46, 4, $containerdata['to']['zip'].' '.$containerdata['to']['city'], 0, 1, 'L') ;
			$pdf->Cell(46, 4, $containerdata['to']['country'], 0, 1, 'L') ;
			$y += 22;

			// Date and reference
			$pdf->SetMargins(3,0,0) ;
			$pdf->SetY ($y) ;
			$pdf->SetFont('helvetica','B',8);
			$pdf->ln();
			$pdf->Cell(33, 4, 'Dato', 0, 0, 'L') ;
			$pdf->Cell(33, 4, 'Reference', 0, 0, 'L') ;
			$pdf->Cell(33, 4, 'Gods', 0, 1, 'L') ;
			$pdf->SetFont('helvetica','',7.5);
			$pdf->Cell(33, 4, $container['date'], 0, 0, 'L') ;
			$pdf->Cell(33, 4, $container['reference'], 0, 0, 'L') ;
			$pdf->Cell(33, 4, 'Fragt', 0, 1, 'L') ;
			$pdf->Line( 2 , $y+15, 100 ,$y+15 ) ;
			$y += 12;

			// Date and reference
			$pdf->SetMargins(3,0,0) ;
			$pdf->SetY ($y) ;
			$pdf->SetFont('helvetica','B',8);
			$pdf->ln();
			$pdf->Cell(33, 4, 'Antal', 0, 0, 'L') ;
			$pdf->Cell(33, 4, 'Brutto KG', 0, 0, 'L') ;
			$pdf->Cell(33, 4, 'Volume M3', 0, 1, 'L') ;
			$pdf->SetFont('helvetica','',7.5);
			$pdf->Cell(33, 4, $i . ' af ' . $numberOfContainers, 0, 0, 'L') ;
			$pdf->Cell(33, 4, number_format($container['brutto'],2), 0, 0, 'L') ;
			$pdf->Cell(33, 4, $container['volume'], 0, 1, 'L') ;
			$pdf->Line( 2 , $y+15, 100 ,$y+15 ) ;
			$y += 12;
			
			// Footer 
			$pdf->SetMargins(3,0,0) ;
			$pdf->SetY ($y) ;
			$pdf->SetFont('helvetica','',7.5);
			$pdf->ln();
			$pdf->Cell(99, 4, $containerdata['owner']['name'].' - '.$containerdata['owner']['street'].' - '.$containerdata['owner']['zip'].' - '.$containerdata['owner']['city'], 0, 1, 'C') ;
		}

		$pdf->SetFont('helvetica','B',10) ;
		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+=5);
		if(isset($container['sscc'])){
			$ssccstring = $container['sscc'];
			// $sscc = $container['sscc'];
		}
		else{
			$ssccstring = createSsccCode($container['id']);
			// $sscc = $container['id'];
			// $ssccstring = 'test';
		}
		$y = 80;
		$pdf->setXY ($x,$y+=5);
		$pdf->SetFont('helvetica','B',8) ;
		$pdf->Cell(92, 4, 'Pakkeliste', 0, 1, 'C') ;

		$pdf->SetFont('helvetica','',8) ;
		$pdf->Cell(92, 4, $container['stockid'], 0, 1, 'C') ;
		$pdf->Code128($x+30,$y+12, $container['stockid'],30,10);
		$pdf->setX ($x);

		$y = 110;
		$pdf->setXY ($x,$y+=5);
		$pdf->SetFont('helvetica','B',12) ;
		$pdf->Cell(92, 4, 'SSCC', 0, 0, 'C') ;

		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+22);
		$pdf->Cell(92, 10, createSsccString($ssccstring), 0, 0, 'C') ;
		$pdf->Code128($x+15,$y+8, '00'.$ssccstring,62,15);
		$pdf->setX ($x);
		

		//$pdf->Output('develstore/adrlabel/adr_'.$stockId.'.pdf', 'F');
	}

	return $pdf->Output('develstore/adrlabel/adr_'.$stockId.'.pdf', 'S');
}



/*
function createSsccLabel($containerdata, $stockId){
	define('FPDF_FONTPATH','lib/font/');
	require_once 'lib/fpdf.inc' ;
	require_once 'lib/barcode128.inc' ;
	// require_once 'module/sscc.inc' ;


	// $stockId = $stockId; //$stock['stockId'];
	$pdf=new PDF_Code128('P', 'mm', array(102,152));

	$numberOfContainers = count($stock['containers']);
	$i = 0;

	foreach ($containerdata['containers'] as $key => $container) {
		$i ++;
		$pdf->AddPage();
		$pdf->SetFont('helvetica','',10);
		// Ship from ( Zone A )
		$x = 5;
		$y = 5;
		$pdf->setXY ($x,$y);
		$pdf->SetFont('helvetica','',18);
		$pdf->Cell(92, 4, utf8_decode($container['from']['name']), 0, 0, 'C') ;
		$pdf->SetFont('helvetica','',13) ;
		$pdf->setXY ( $x ,$y+=8 );
		$pdf->Cell(92, 4, utf8_decode($container['from']['street'].' - '.$container['from']['zip'].' '.$container['from']['city']), 0, 0, 'C') ;

		$pdf->Line( 0 ,$y+10 ,200 ,$y+10 ) ;


		$x = 5;
		$y += 15;
		$pdf->setXY ($x,$y);
		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x+5,$y);
		$pdf->Cell(16, 4, 'Delivery:') ;
		$pdf->Cell(68, 4, utf8_decode($container['to']['name'])) ;
		$pdf->setXY ($x+21,$y+=5);
		$pdf->Cell(68, 4, utf8_decode($container['to']['street'])) ;
		$pdf->setXY ($x+21,$y+=5);
		$pdf->Cell(68, 4, utf8_decode($container['to']['country']).'-'.utf8_decode($container['to']['zip']).' '.utf8_decode($container['to']['city'])) ;


		$pdf->SetFont('helvetica','',13) ;
		if(isset($container['item']['no1text']) OR isset($container['item']['no1'])){
			$no1 = '';
			$no1text = '';
			if(isset($container['item']['no1text'])){
				$no1text = $container['item']['no1text'];
			}
			if(isset($container['item']['no1'])){
				$no1 = $container['item']['no1'];
			}

			$pdf->setXY ($x,$y+=8);
			$pdf->Cell(92, 4, utf8_decode($no1text).' '.utf8_decode($no1), 0, 0, 'C') ;
		}
		if(isset($container['item']['no2text']) OR isset($container['item']['no2'])){
			$no1 = '';
			$no1text = '';
			if(isset($container['item']['no2text'])){
				$no1text = $container['item']['no2text'];
			}
			if(isset($container['item']['no2'])){
				$no1 = $container['item']['no2'];
			}

			$pdf->setXY ($x,$y+=6);
			$pdf->Cell(92, 4, utf8_decode($no1text).' '.utf8_decode($no1), 0, 0, 'C') ;
		}

		$pdf->SetFont('helvetica','',10) ;

		$pdf->Line( 0 ,$y+=8 ,200 ,$y ) ;


		$pdf->SetFont('helvetica','B',10) ;
		$pdf->setXY ($x,$y+=8);
		$pdf->Cell(92, 4, 'SSCC') ;
		$pdf->SetFont('helvetica','',10) ;
		$pdf->setXY ($x,$y+=5);
		if(isset($container['sscc'])){
			$ssccstring = $container['sscc'];
			// $sscc = $container['sscc'];
		}
		else{
			$ssccstring = createSsccCode($container['id']);
			// $sscc = $container['id'];
			// $ssccstring = 'test';
		}
		$pdf->Cell(92, 4, $ssccstring) ;
		$pdf->setXY ($x,$y+=5);
		$pdf->SetFont('helvetica','B',10) ;
		$pdf->Cell(92, 4, 'Content') ;
		$pdf->setXY ($x,$y+=5);
		$pdf->SetFont('helvetica','',10) ;
		$pdf->Cell(46, 4, $container['item']['variantcode']) ;
		$pdf->Cell(46, 4, 'Count: '.$container['item']['quantity'], 0, 0, 'R') ;

		$pdf->Line( 0 ,$y+=8 ,200 ,$y ) ;

		$pdf->setXY ($x,$y+=5);
		$pdf->Code128($x+15,$y, createVariantSsccCode($container['item']['variantcode'], $container['item']['quantity']),62,15);
		$pdf->setXY ($x,$y+=16);
		$pdf->Cell(92, 4, createVariantSscc($container['item']['variantcode'], $container['item']['quantity']), 0, 0, 'C') ;
		$tempx = $x;
		$tempy = $y;
		$pdf->setXY ($x,$y+22);
		$pdf->Cell(92, 10, createSsccString($ssccstring), 0, 0, 'C') ;
		// $pdf->setXY ($x,$y);
		$pdf->Code128($x+15,$y+8, createSsccBarcodeString($ssccstring),62,15);
		// $pdf->setXY ($x,$y+8);
		$pdf->setX ($x);
		

		$pdf->Output('develstore/sscc/sscc_'.$stockId.'.pdf', 'F');
	}

	

	$pdf->Output('develstore/sscc/sscc_'.$stockId.'.pdf', 'F');
	return 'sscc_'.$stockId.'.pdf';
}*/

?>