<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=20 value=\"%s\" style=\"width:175px;\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print (htmlItemSpace ()) ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
	printf ("<tr><td class=itemlabel>%s</td><td><input type=checkbox name=Access[%d] %s>%s</td></tr>\n", ($n==0) ? 'Roles' : '', $row['Id'], ($row['ReportRoleId'])?'checked':'', $row['Name']) ;
	$n++ ;
    }
    if (dbNumRows($Result) == 0) {
	printf ("<tr><td class=itemlabel>No Roles</td></tr>\n") ;
    }
    dbQueryFree ($Result) ;

    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;


    
    return 0 ;
?>
