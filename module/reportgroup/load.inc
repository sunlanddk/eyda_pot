<?php

    switch ($Navigation['Function']) {
	case 'list' :
	    // Query list of Countries
	    $query = sprintf ('SELECT ReportGroup.* FROM ReportGroup WHERE ReportGroup.Active=1 ORDER BY ReportGroup.Name') ;
	    $Result = dbQuery ($query) ;
	    return 0 ;
	    
	case 'edit' :
	case 'save-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $Id = -1 ;
	    }

	    // Query Roles Associated the ReportGroup
	    $query = sprintf ('SELECT Role.Id, Role.Name, ReportRole.Id AS ReportRoleId FROM Role LEFT JOIN ReportRole ON ReportRole.ReportGroupId=%d AND ReportRole.RoleId=Role.Id AND ReportRole.Active=1 WHERE Role.Active=1 ORDER BY Role.Name', $Id) ;
	    $Result = dbQuery ($query) ;

	    switch ($Navigation['Parameters']) {
		case 'new' :
		    return 0 ;
	    }

	    // Fall through

	case 'delete-cmd' :
	    // Query ReportGroup
	    $query = sprintf ('SELECT ReportGroup.* FROM ReportGroup WHERE ReportGroup.Id=%d AND ReportGroup.Active=1', $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;
	    
	    return 0 ;
    }

    return 0 ;
?>
