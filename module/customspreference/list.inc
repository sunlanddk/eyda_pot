<?php

    require_once 'lib/list.inc' ;

    listStart () ;
    listRow () ;
    listHead ('', 23) ;
    listHead ('Material', 80) ;
    listHead ('Drop', 80) ;
    listHead ('Knit', 80) ;
    listHead ('Drop', 80) ;
    listHead ('Work', 80) ;
    listHead ('Drop', 80) ;
    listHead ('') ;
    while ($row = dbFetch($Result)) {
	listRow () ;
	listFieldIcon ($Navigation['Icon'], 'edit', $row['Id']) ;
	listField ($row['MaterialCountryGroup']) ;
	listField ($row['MaterialDrop'] ? 'yes' : 'no') ;
	listField ($row['KnitCountryGroup']) ;
	listField ($row['KnitDrop'] ? 'yes' : 'no') ;
	listField ($row['WorkCountryGroup']) ;
	listField ($row['WorkDrop'] ? 'yes' : 'no') ;
    }
    if (dbNumRows($Result) == 0) {
	listRow () ;
	listField () ;
	listField ('No Customs Preferences', 'colspan=6') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
