<?php
   
    // get current entry
    $id = $_POST["id"] ;
    $query = sprintf ("SELECT * FROM Navigation WHERE Id=%d", $id) ;
    $result = dbQuery ($query) ;
    $row = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if (!$row) return "Entry not found" ;

    // get entry below
    $query = sprintf ("SELECT * FROM Navigation WHERE ParentId=%d AND RefType='%s' AND DisplayOrder>%d ORDER BY DisplayOrder LIMIT 1", $row["ParentId"], $row["RefType"], $row["DisplayOrder"]) ;
    $result = dbQuery ($query) ;
    $row2 = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if (!$row2) return "The entry is at the bottom of the menu" ;

    // do the twist
    $query = sprintf ("UPDATE Navigation SET DisplayOrder=%d WHERE Id=%d", $row2["DisplayOrder"], $row["Id"]) ;
    dbQuery ($query) ;    
    $query = sprintf ("UPDATE Navigation SET DisplayOrder=%d WHERE Id=%d", $row["DisplayOrder"], $row2["Id"]) ;
    dbQuery ($query) ;

    return 0
?>
