<?php

    require "lib/save.inc" ;

    function checkfield ($fieldname, $value) {
	global $User, $fields, $Id ;
	switch ($fieldname) {
	    case "DisplayOrder":
		// Set new displayorder for all entries below the one to be saved
		if ($value === NULL) return true ;

		$query = sprintf ("SELECT Id FROM Navigation WHERE Active=1 AND Id<>%d AND RefType='%s' AND ParentId=%d AND DisplayOrder>=%d ORDER BY DisplayOrder", $Id, $fields["RefType"]["value"], $fields["ParentId"]["value"], $value) ;
//printf ("query %s<br>", $query) ;
		$result = dbQuery ($query) ;
		unset($ilist) ;
		while ($row = dbFetch($result)) {
		    $ilist[] = $row["Id"] ;
//printf ("found id %d<br>", $row["Id"]) ;
		}
		dbQueryFree ($result) ;

		if (isset($ilist)) {
		    $n = (integer)$value + 1 ;
		    foreach ($ilist as $i) {
			$query = sprintf ("UPDATE Navigation SET DisplayOrder=%d WHERE Id=%d", $n, $i) ;
//printf ("update %s<br>", $query) ;
		    	dbQuery ($query) ;
			$n++ ;
		    }
		}
		break ;

	    case 'ParentId':
		if ($Id > 0) break ;
		if ($value <= 0 and $fields['RefType']['value'] != 'm') return sprintf ('%s(%d) no parent for item', __FILE__, __LINE__) ;
		break ;
	}
	return true ;
    }

    $fields = array (
	"Name" 		=> array("mandatory" => true),
	"Mark"	 	=> array(),
	"Header" 	=> array(),
	"RefType" 	=> array("mandatory" => true),
	"RefIdInclude"	=> array("type" => "checkbox"),
	"RefConfirm" 	=> array(),
	"Icon" 		=> array(),
	"AccessKey"	=> array(),
	"NavigationTypeId" => array("type" => "integer"),
	"Module" 	=> array(),
	"Function" 	=> array(),
	"MustLoad"	=> array("type" => "checkbox"),
	"Width" 	=> array("type" => "integer"),
	"Height" 	=> array("type" => "integer"),
	"Back"	 	=> array("type" => "integer"),
	"Parameters" 	=> array(),
	"Focus" 	=> array(),
	"Operation" 	=> array(),
	"Disable"	=> array("type" => "checkbox"),
	"PermInternal"	=> array("type" => "checkbox"),
	"PermOwner" 	=> array("type" => "checkbox"),
	"FunctionId" 	=> array("mandatory" => true, 		"type" => "integer"),
	"ParentId" 	=> array("type" => "integer",		"check" => true),
	"DisplayOrder" 	=> array("type" => "integer",		"check" => true)
    ) ;
    
    return saveFields ("Navigation", $Id, $fields) ;
?>
