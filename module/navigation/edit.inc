<?php

    require_once "lib/html.inc" ;
    require_once "lib/table.inc" ;

    switch ($Navigation["Parameters"]) {
	case "new":
	    $query = sprintf ("SELECT * FROM Navigation WHERE Active=1 AND Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $r = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if (!$r) return "Navigation entry does not exist" ;

	    unset ($row) ;
	    $row["ParentId"] = $r["ParentId"] ;
	    $row["DisplayOrder"] = $r["DisplayOrder"] + 1 ;
	    $row["NavigationTypeId"] = tableDefaultId ("NavigationType") ;
	    $row["FunctionId"] = tableDefaultId ("Function") ;
	    $Id = -1 ;
	    break ;

	case "newsublevel":
	    unset ($row) ;
	    $row["ParentId"] = $Id ;
	    $row["DisplayOrder"] = 1 ;
	    $row["NavigationTypeId"] = tableDefaultId ("NavigationType") ;
	    $row["FunctionId"] = tableDefaultId ("Function") ;
	    $Id = -1 ;
	    break ;
	    
	case "newtop":
	    unset ($row) ;
	    $row["ParentId"] = 0 ;
	    $row["DisplayOrder"] = 1 ;
	    $Id = -1 ;
	    break ;

	default:
	    $query = sprintf ("SELECT * FROM Navigation WHERE Active=1 AND Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $row = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if (!$row) return "Navigation entry does not exist" ;	    
    }

    function Mark2Id ($table, $mark) {
	$query = sprintf ("SELECT Id FROM %s WHERE Mark='%s'", $table, $mark) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	return $row["Id"] ;
    }   

    function displaypath ($id, $level) {
	if ($level > 10) return ;
	if ($id == 0) {
	    printf ("%s%s", "HOME", htmlentities(" -> ")) ;
	    return ;
	}
	$query = sprintf ("SELECT * FROM Navigation WHERE Id=%d", $id) ;
	$result = dbQuery ($query) ;
	$row = dbFetch($result) ;
	dbQueryFree ($result) ;
	if ($row) {
	    displaypath ($row["ParentId"], $level+1) ;
	    printf ("%s%s", htmlentities($row["Name"]), htmlentities(" -> ")) ;
	}
    }
     
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<input type=hidden name=command value=save>") ;		// Backward compatibility
    if ($Id == -1) {
	printf ("<input type=hidden name=ParentId value=%d>", $row["ParentId"]) ;
        printf ("<input type=hidden name=DisplayOrder value=%d>", $row["DisplayOrder"]) ;
    }
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    if ($Navigation["LayoutPopup"]) {
	printf ("<tr><td width=80><p><b>Menupath</b></p></td><td><p><b>") ;
	displaypath($row["ParentId"],0) ;
	printf ("</b></p></td></tr>\n") ;
	print htmlItemSpace() ;
    }
    printf ("<tr><td width=80><p>Name</p></td><td><input type=text name=Name size=20 value=\"%s\"></td></tr>\n", htmlentities($row["Name"])) ;
    printf ("<tr><td><p>Mark</p></td><td><input type=text name=Mark size=15 value=\"%s\"></td></tr>\n", htmlentities($row["Mark"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td></td><td><p><b>Module</b></p></td><tr>\n") ;
    printf ("<tr><td><p>Type</p></td><td>%s</td></tr>\n", htmlDBSelect ("NavigationTypeId style='width:150px;'", $row["NavigationTypeId"], "SELECT Id, Name AS Value FROM NavigationType WHERE Active=1 ORDER BY Name"));
    printf ("<tr><td><p>Header</p></td><td><input type=text name=Header size=40 value=\"%s\"></td></tr>\n", htmlentities($row["Header"])) ;
    printf ("<tr><td><p>Module</p></td><td><input type=text name=Module size=20 value=\"%s\"></td></tr>\n", htmlentities($row["Module"])) ;
    printf ("<tr><td><p>Function</p></td><td><input type=text name=Function size=20 value=\"%s\"></td></tr>\n", htmlentities($row["Function"])) ;
    printf ("<tr><td><p>Parameters</p></td><td><input type=text name=Parameters size=40 value=\"%s\"></td></tr>\n", htmlentities($row["Parameters"])) ;
    printf ("<tr><td><p>MustLoad</p></td><td><input type=checkbox name=MustLoad %s></td></tr>\n", ($row["MustLoad"])?"checked":"") ;
    printf ("<tr><td><p>Width</p></td><td><table><tr><td width=170><input type=text name=Width size=5 value=\"%s\">\n", htmlentities($row["Width"])) ;
    printf ("<td width=80><p>Height</p></td><td><input type=text name=Height size=5 value=\"%s\"></td></tr></table></td></tr>\n", htmlentities($row["Height"])) ;
    printf ("<tr><td><p>Focus</p></td><td><input type=text name=Focus size=20 value=\"%s\"></td></tr>\n", htmlentities($row["Focus"])) ;
    printf ("<tr><td><p>Back</p></td><td><input type=text name=Back size=5 value=\"%s\"></td></tr>\n", htmlentities($row["Back"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td></td><td><p><b>Referance</b></p></td><tr>\n") ;
    printf ("<tr><td><p>Disable</p></td><td><input type=checkbox name=Disable %s></td></tr>\n", ($row["Disable"])?"checked":"") ;
    printf ("<tr><td><p>Type</p></td><td><select size=1 name=RefType style='width:150px;'>\n");
    printf ("<option%s value='m'>Menu</option>\n", ($row["RefType"]=="m")?" selected":"") ;
    printf ("<option%s value='d'>Dropdown</option>\n", ($row["RefType"]=="d")?" selected":"") ;
    printf ("<option%s value='t'>Toolbar</option>\n", ($row["RefType"]=="t")?" selected":"") ;
    printf ("<option%s value='l'>Link</option>\n", ($row["RefType"]=="l")?" selected":"") ;
    printf ("</select></td></tr>\n") ;
    printf ("<tr><td><p>Icon</p></td><td><input type=text name=Icon size=20 value=\"%s\"></td></tr>\n", htmlentities($row["Icon"])) ;
    printf ("<tr><td><p>AccessKey</p></td><td><input type=text name=AccessKey size=1 maxlength=1 style='width:17px;' value=\"%s\"></td></tr>\n", htmlentities($row["AccessKey"])) ;
    printf ("<tr><td><p>Confirm</p></td><td><input type=text name=RefConfirm size=60 value=\"%s\"></td></tr>\n", htmlentities($row["RefConfirm"])) ;
    printf ("<tr><td><p>Id parameter</p></td><td><input type=checkbox name=RefIdInclude %s></td></tr>\n", ($row["RefIdInclude"])?"checked":"") ;
    print htmlItemSpace() ;
    printf ("<tr><td></td><td><p><b>Permissions</b></p></td><tr>\n") ;
    printf ("<tr><td><p>Function</p></td><td>%s</td></tr>\n", htmlDBSelect ("FunctionId style='width:150px;'", $row["FunctionId"], "SELECT Id, Name AS Value FROM Function ORDER BY DisplayOrder, Name"));
    printf ("<tr><td><p>Operation</p></td><td><select size=1 name=Operation style='width:150px;'>\n");
    printf ("<option value=''>- none -</option>\n") ;
    printf ("<option%s value='r'>Read</option>\n", ($row["Operation"]=="r")?" selected":"") ;
    printf ("<option%s value='w'>Write</option>\n", ($row["Operation"]=="w")?" selected":"") ;
    printf ("<option%s value='c'>Create</option>\n", ($row["Operation"]=="c")?" selected":"") ;
    printf ("<option%s value='d'>Delete</option>\n", ($row["Operation"]=="d")?" selected":"") ;
    printf ("</select></td></tr>\n") ;
    printf ("<tr><td><p>Owner</p></td><td><input type=checkbox name=PermOwner %s></td></tr>\n", ($row["PermOwner"])?"checked":"") ;
    printf ("<tr><td><p>Internal</p></td><td><input type=checkbox name=PermInternal %s></td></tr>\n", ($row["PermInternal"])?"checked":"") ;
    print (htmlItemInfo ($row)) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;
  
    return 0 ;
?>
