<?php

    require 'lib/save.inc' ;

    $fields = array (
		'Name'			=> array ('mandatory' => true,	'check' => true),
		'Description'		=> array (),
		'PayDays'		=> array ('type' => 'integer'),
		'PayMonths'		=> array ('type' => 'integer'),
		'RunningMonth'		=> array ('type' => 'checkbox'),
		'PrePay'		=> array ('type' => 'checkbox'),
		'DiscountDays'	=> array('type' => 'integer'),
		'DiscountPct'	=> array('type' => 'decimal', 'format' => '3.2')
    ) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value) {
	global $Id ;
	switch ($fieldname) {
	    case 'Name':
		// Check that name does not allready exist
		$query = sprintf ('SELECT Id FROM PaymentTerm WHERE Active=1 AND Name="%s" AND Id<>%d', addslashes($value), $Id) ;
		$result = dbQuery ($query) ;
		$count = dbNumRows ($result) ;
		dbQueryFree ($result) ;
		if ($count > 0) return 'PaymentTerm allready existing' ;
		return true ;
	}
	return false ;
    }
     
    return saveFields ('PaymentTerm', $Id, $fields) ;
?>
