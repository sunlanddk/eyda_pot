<?php
   
    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/perm.inc' ;
    
	if ($Record['Id']>0) {
		$i = 1 ;
		$_companyid=$Record['CompanyId']  ;
	} else {
		$_companyid=$Record['CompanyId']  ;
		unset ($Record) ;
		$Id = -1 ;
		$Record["TimeZoneId"] = tableDefaultId ("TimeZone") ;
//		$Record['CompanyId']  = $_companyid ;
	}
	
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Id) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;
	print htmlItemHeader() ;
	if ($Id>0) {
		printf ("<tr><td><p>Loginname</p></td><td><p>%s</p></td></tr>\n", htmlentities($Record["Loginname"])) ;  
		printf ("<tr><td><p>Password</p></td><td><input type=password name=Password size=20></td><td><p>Leave blank to maintain same password</p></td></tr>\n") ;
		printf ("<tr><td><p>Login</p></td><td><input type=checkbox name=Login %s></td></tr>\n", ($Record["Login"])?"checked":"") ;
	} else {
		printf ("<tr><td><p>Loginname</p></td><td><input type=text name=Loginname size=20 value=\"%s\"></td></tr>\n",  htmlentities(tableGetField('Company','Number',$_companyid))) ;
		printf ("<tr><td><p>Password</p></td><td><input type=password name=Password size=20></td><td><p>Mandatory</p></td></tr>\n") ;
		printf ("<tr><td><p>Login</p></td><td><input type=checkbox name=Login %s></td></tr>\n", "checked") ;
	}
    print htmlItemSpace() ;
    printf ("<tr><td><p>Title</p></td><td><input type=text name=Title size=40 value=\"%s\"></td></tr>\n",  htmlentities($Record["Title"])) ;
    printf ("<tr><td><p>Firstname</p></td><td><input type=text name=FirstName size=50 value=\"%s\"></td><td><p>Mandatory</p></td></tr>\n",  htmlentities($Record["FirstName"])) ;
    printf ("<tr><td><p>Lastname</p></td><td><input type=text name=LastName size=50 value=\"%s\"></td><td><p>Mandatory</p></td></tr>\n",  htmlentities($Record["LastName"])) ;
//    if ($Id>0) {
		printf ("<tr><td><p>Company</p></td><td><p>%s</p></td></tr>\n", htmlentities(tableGetField("Company", "Name", (int)$_companyid))) ;  
		printf ("<tr><td><p></p></td><td><input type=hidden name=CompanyId size=50 value=\"%d\"></td></tr>\n",  (int)$_companyid) ;
//    } else {
//		printf ("<tr><td><p>Company</p></td><td>%s</td></tr>\n", htmlDBSelect ("CompanyId style='width:250px'", $Record["CompanyId"], "SELECT Id, Name AS Value FROM Company WHERE Active=1 ORDER BY Name")) ;  
//    }	
    print htmlItemSpace() ;
    printf ("<tr><td><p>E-mail</p></td><td><input type=text name=Email size=30 value=\"%s\"></td></tr>\n",  htmlentities($Record["Email"])) ;    
    print htmlItemSpace() ;
	if ($Id>0)
		printf ("<tr><td><p>Timezone</p></td><td>%s</td></tr>\n", htmlDBSelect ("TimeZoneId style='width:200px;'", $Record["TimeZoneId"], sprintf("SELECT Id, Name AS Value FROM TimeZone WHERE Active=1 ORDER BY Name"))) ;   
	else
		printf ("<tr><td><p>Timezone</p></td><td>%s</td></tr>\n", htmlDBSelect ("TimeZoneId style='width:200px;'", 328, sprintf("SELECT Id, Name AS Value FROM TimeZone WHERE Active=1 ORDER BY Name"))) ;   
//    print htmlItemSpace() ;
//	printf ("<tr><td><p>Role</p></td><td>%s</td></tr>\n", htmlDBSelect ("RoleId style='width:150px;'", $Record["RoleId"], 'SELECT Role.Id, Role.Name AS Value FROM Role WHERE Role.ProjectId=0 AND Role.Active=1 ORDER BY Role.Name')) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Business</p></td><td><input type=text name=PhoneBusiness size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhoneBusiness"])) ;
    printf ("<tr><td><p>Direct</p></td><td><input type=text name=PhoneDirect size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhoneDirect"])) ;
    printf ("<tr><td><p>Mobile</p></td><td><input type=text name=PhoneMobile size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhoneMobile"])) ;
    printf ("<tr><td><p>Private</p></td><td><input type=text name=PhonePrivate size=20 value=\"%s\"></td></tr>\n",  htmlentities($Record["PhonePrivate"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Address</p></td><td><input type=text name=Address1 size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["Address1"])) ;
    printf ("<tr><td></td><td><input type=text name=Address2 size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["Address2"])) ;
    printf ("<tr><td><p>ZIP</p></td><td><input type=text name=ZIP size=15 value=\"%s\"></td></tr>\n",  htmlentities($Record["ZIP"])) ;
    printf ("<tr><td><p>City</p></td><td><input type=text name=City size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["City"])) ;
    printf ("<tr><td><p>Country</p></td><td><input type=text name=Country size=50 value=\"%s\"></td></tr>\n",  htmlentities($Record["Country"])) ; 
// printf ("<tr><td class=itemlabel>Extern</td><td><input type=checkbox name=Extern size=50 %s></td></tr>\n", $Record["Extern"]?'checked':'') ;

	
print htmlItemInfo($Record) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
