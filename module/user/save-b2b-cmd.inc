<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/perm.inc' ;
    
    function checkfield ($fieldname, $value) {
		global $User, $Id;
		switch ($fieldname) {
			case 'Login' :
				if ($value == 0 and $Id == $User['Id']) return "Login can not be reset for your own account" ;
				return true ;

			case "Loginname":
				$query = sprintf ("SELECT * FROM User WHERE Active=1 AND Loginname=\"%s\" AND Id<>%d", $value, $Id) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Loginname is allready in use" ;
				return true ;

			case "Password":
				if (empty($value)) return 'Password is mandatory' ;
				return true ;

			default:
				return true ;
		}
		return false ;
    }

    $fields = array (
		"Loginname"	=> array ("mandatory" => true),
		"Password"	=> array ("check" => true),
		"Title"		=> array (),
		"FirstName"	=> array ("mandatory" => true,"check" => true),
		"LastName"	=> array ("mandatory" => true),
		"CompanyId"	=> array ("mandatory" => true,		),	
		"Email"		=> array ("check" => true),
		"PhoneBusiness"	=> array (),
		"PhoneDirect"	=> array (),
		"PhoneMobile"	=> array (),
		"PhonePrivate"	=> array (),
		"Address1"	=> array (),
		"Address2"	=> array (),
		"ZIP"		=> array (),
		"City"		=> array (),
		"Country"	=> array (),
		"TimeZoneId"	=> array (),
		"RoleId"	=> array ("type" => "set"),
		"Login"		=> array ("type" => "checkbox"),
		"Extern" => array ("type" => "set")
    ) ;

	$fields["Extern"]['value'] = 1 ;
	$fields["RoleId"]['value'] = 125 ;
	
	return saveFields ("User", $Id, $fields) ;
	
//    return saveFields ("User", $Record['Id'], $fields) ;
?>
