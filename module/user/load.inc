<?php

    require_once 'lib/perm.inc' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    $query = sprintf ("SELECT User.Id AS Id, User.Loginname AS Loginname, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, CONCAT(AvatarUser.FirstName, ' ', AvatarUser.LastName) AS AvatarFullName,
									User.Title AS Title, Company.Name AS AgentCompanyName, Role.Name AS RoleName, User.SalesRef, User.AgentManager, User.AvatarId  
							FROM User 
							LEFT JOIN Company ON User.AgentCompanyId=Company.Id 
							LEFT JOIN Role ON User.RoleId=Role.Id%s 
							LEFT JOIN User AvatarUser On AvatarUser.Id=User.AvatarId
							WHERE User.Active=1 and User.Extern=0 ORDER BY Loginname", permClauseOwner('User')) ;
	    $Result = dbQuery ($query) ;
	    return 0 ;

	case 'online' :
	    $query = sprintf ("SELECT Login.Id, User.Loginname, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, User.Title, Company.Name AS CompanyName, Login.LoginDate, Login.AccessDate FROM (User, Login) LEFT JOIN Company ON User.CompanyId=Company.Id WHERE Login.UserId=User.Id AND Login.Active=1%s ORDER BY Name", permClauseOwner('User')) ;
	    $Result = dbQuery ($query) ;
	    return 0 ;

	case 'onlinedetail' :
	    $query = sprintf ("SELECT Login.Id, User.Loginname, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, User.Title, Company.Name AS CompanyName, Login.LoginDate, Login.AccessDate, Login.AcceptedMimeTypes, Session.UID, Session.IP, Session.Language, Session.Agent FROM (User, Login, Session) LEFT JOIN Company ON User.CompanyId=Company.Id WHERE Session.Id=Login.SessionId AND Login.UserId=User.Id AND Login.Active=1 AND Login.Id=%d", $Id) ;
	    $result = dbQuery ($query) ;
	    $Record = dbFetch($result) ;
	    dbQueryFree ($result) ;
	    return 0 ;

	case 'edit-b2b' :
		$query = sprintf ("SELECT * FROM User WHERE Active=1 AND CompanyId=%d AND Extern=1", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Record['CompanyId'] = $Id ;
		$Id=(int)$Record['Id'] ;
	    return 0 ;
		
	case 'save-b2b-cmd' :
		$query = sprintf ("SELECT * FROM User WHERE Active=1 AND Id=%d", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
		$Id = $Record['Id']>0?$Record['Id']:-1;
//		$Record['Id'] = 0 ;
	    return 0 ;
    }

    switch ($Navigation["Parameters"]) {
	case "userid":
		$query = sprintf ("SELECT * FROM User WHERE Active=1 AND Id=%d", $Id) ;
		$result = dbQuery ($query) ;
		$Record = dbFetch ($result) ;
		dbQueryFree ($result) ;
	    return 0 ;

	case "new":
	    return 0 ;
	    
	case "my":
	    $Id = $User["Id"] ;
	    break ;
    }
    
//    if ($Id <= 0) return 0 ;
    $Id = $User["Id"] ;
    $query = sprintf ("SELECT * FROM User WHERE Active=1 AND Id=%d", $Id) ;
    $result = dbQuery ($query) ;
    $Record = dbFetch ($result) ;
    dbQueryFree ($result) ;
    
    return 0 ;
?>
