<?php

    require "lib/save.inc" ;

	function generateRandomKey($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$salt = '';
		for ($i = 0; $i < $length; $i++) {
			$salt .= $characters[rand(0, $charactersLength - 1)];
		}
		return $salt;
	}

	function generatePasswordHash($password, $salt){
        $password = hash('sha512', $salt.$password);
        $password = password_hash($password, PASSWORD_DEFAULT);

        return $password;
    }

    function checkfield ($fieldname, $value) {
		global $User, $fields ;
		
		switch ($fieldname) {
			case "Password1":
			return false ;

			case "Password":
			if ($fields["Password1"]["value"] != $value) return "You have not re-entered the same password" ;
			if ($value == $fields["Password"]["db"]) return "You have entered the existing password" ;
			if (strlen ($value) < 5) return "Your password must contain at least 5 characters" ;
			if ($value == $User["Loginname"]) return "Your password must be different from your Loginname" ;
			return true ;
		}
		return false ;
    }

	function checkPasswords($password, $password1) {
		global $User;
		if ($password != $password1) return "You have not re-entered the same password" ;
		if (strlen($password) < 5) return "Your password must contain at least 5 characters" ;
		if ($password == $User["Loginname"]) return "Your password must be different from your Loginname" ;
		return true;
	}


	if($User["2FA"] == "") {
		$fields = array (
			"Password1"			=> array ("check" => true),
			"Password"			=> array ("check" => true),
			"HashedPassword" 	=> array ("type" => "set" , "value" => ""),
		);
	} else {
		$password =  $_POST["Password"];
		$password1 = $_POST["Password1"];
		$check = checkPasswords($password, $password1);

		if(gettype($check) == "string") {
			return $check;
		}

		$randomKey = generateRandomKey(10);
		$hashedPassword = generatePasswordHash($password, $randomKey);

		$fields = array (
			"Password"			=> array ("type" => "set" , "value" => $hashedPassword),
			"HashedPassword" 	=> array ("type" => "set" , "value" => $hashedPassword),
			"RandomKey" 		=> array ("type" => "set" , "value" => $randomKey)
		) ;
	}
    return saveFields ("User", $User["Id"], $fields) ;
?>
