<?php
   
    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/perm.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    

    formStart () ;
    itemStart () ;
    itemHeader () ;
    
    itemSpace () ;
    $_query = sprintf('SELECT 0 as Id, "-None-" as Value Union SELECT Workstation.Id, Workstation.Name AS Value FROM Workstation WHERE active=1 ORDER BY Value');
    itemFieldRaw ('Workstation', formDBSelect ('WorkstationId', (int)$Login['WorkstationId'], $_query, 'width:250px;')) ;  
    itemEnd () ;
    formEnd () ;



    return 0 ;
?>
