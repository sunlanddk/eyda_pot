<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;

    $CountryGroup = tableEnumExplode(tableGetType('Country','CountryGroup')) ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($Record) ;

	    // Default values
	    $Record['VATPercentage'] = 0 ;
	    $Record['CountryGroup'] = $CountryGroup[0] ;
	    break ;
    }
     
    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Name</td><td><input type=text name=Name maxlength=3 size=3 value=\"%s\"></td></tr>\n", htmlentities($Record['Name'])) ;
    printf ("<tr><td class=itemlabel>IsoName</td><td><input type=text name=DisplayName maxlength=3 size=3 value=\"%s\"></td></tr>\n", htmlentities($Record['DisplayName'])) ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>EU Member</td><td><input type=checkbox name=EUMember %s></td></tr>\n", ($Record['EUMember'])?'checked':'') ;
    printf ("<tr><td class=itemfield>EFTA Member</td><td><input type=checkbox name=EFTAMember %s></td></tr>\n", ($Record['EFTAMember'])?'checked':'') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Group</td><td>%s</td></tr>\n", htmlSelect ('CountryGroup style="width:100px;"', $Record['CountryGroup'], $CountryGroup)) ;    
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>VAT</td><td><input type=text name=VATPercentage maxlength=4 size=4' value=%s>%%</td></tr>\n", $Record['VATPercentage']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Freight Price</td><td><input type=text name=FreightPrice maxlength=4 size=4' value=%s></td></tr>\n", $Record['FreightPrice']) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Freigt Currency</td><td>%s</td></tr>\n", htmlDBSelect ('CurrencyId style="width:300px"', $Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value')) ;  
//	itemFieldRaw ('Currency', formDBSelect ('CurrencyId', (int)$Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value', 'width:200px;')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>InvoiceFooter</td><td><textarea name=InvoiceFooter style='width:100%%;height:150px;'>%s</textarea></td></tr>\n", $Record['InvoiceFooter']) ;
    printf ("<tr><td class=itemfield>CustomsInfo</td><td><input type=checkbox name=InvoiceCustomsInfo %s></td></tr>\n", ($Record['InvoiceCustomsInfo'])?'checked':'') ;
    print htmlItemSpace() ;
	// Customer defaults on create
    $query = 'SELECT * FROM companydefaults WHERE CountryId=' . $Id ;
    $res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
    printf ("<tr><td class=itemlabel>Currency</td><td>%s</td></tr>\n", htmlDBSelect ('DefaultCurrencyId style="width:300px"', $row['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value')) ;  
    printf ("<tr><td class=itemlabel>PaymentTerm</td><td>%s</td></tr>\n", htmlDBSelect ('PaymentTermId style="width:300px"', $row['PaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value')) ;  
    printf ("<tr><td class=itemlabel>DeliveryTerm</td><td>%s</td></tr>\n", htmlDBSelect ('DeliveryTermId style="width:300px"', $row['DeliveryTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Value')) ;  
    printf ("<tr><td class=itemlabel>Carrier</td><td>%s</td></tr>\n", htmlDBSelect ('CarrierId style="width:300px"', $row['CarrierId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Carrier WHERE Active=1 ORDER BY Value')) ;  
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
