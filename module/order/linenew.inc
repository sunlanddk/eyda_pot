<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

	if ($User['SalesRef']) {
		return 'use Shop/Cart to add new lines to an order' ;
	}

    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['Id']) ;     
    itemField ('Company', $Record['CompanyName']) ;     
    itemSpace () ;
    itemEnd () ;
    
    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
//    itemFieldRaw ('Article', formText ('ArticleNumber', '', 10)) ;
	$reload = '' ; //sprintf('onchange="appLoad(%d,%d,\'ArticleId=\'+document.appform.ArticleId.options[document.appform.ArticleId.selectedIndex].value);"', (int)$Navigation['Id'], $Id) ;
	if ($Record['SeasonId']>0) {
	
		$query = sprintf('SELECT Article.Id, concat(Article.Number, " - ", Article.Description) AS Value 
						FROM collection c, collectionmember cm, Article 
						LEFT JOIN orderline ol ON ol.id=%d and ol.articleid=Article.Id and ol.active=1
						WHERE ((c.seasonid=%d AND cm.collectionid=c.id AND Article.id=cm.articleid 
							AND c.Active=1 AND cm.Active=1 AND Article.Active=1) or (Article.ArticleTypeId=6 AND Article.Active=1))
							AND (isnull(ol.id) or ol.id=0)
						GROUP BY Article.Id
						ORDER BY Value', $Id, $Record['SeasonId']) ;

		$query = sprintf('SELECT Article.Id, concat(Article.Number, " - ", Article.Description) AS Value 
						FROM collection c, collectionmember cm, Article 
						WHERE ((c.seasonid=%d AND cm.collectionid=c.id AND Article.id=cm.articleid 
							AND c.Active=1 AND cm.Active=1 AND Article.Active=1) or (Article.ArticleTypeId=6 AND Article.Active=1))
						GROUP BY Article.Id
						ORDER BY Value', $Record['SeasonId']) ;
	}
	else
		$query = 'SELECT Article.Id, concat(Article.Number, " - ", Article.Description) AS Value FROM Article WHERE Active=1 ORDER BY Value' ;
	itemFieldRaw ('Article', formDBSelect ('ArticleId', $article_id, $query, 'width:250px;', NULL, $reload)) ; 
    itemSpace () ;
	itemFieldRaw ('', 'If style is not found in drop down, then please edit existing line in this order.') ; 
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
