<?php

    require_once 'lib/save.inc' ;
    $fields = array (
	'StockId'		=> array ('type' => 'integer')
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $User, $Navigation, $Record, $Id, $fields ;
		switch ($fieldname) {
	    case 'StockId' :
			if (!$changed) return false ;
			$fields['StockId']['value']=$value ;
			return true ;
		}
	}
    
     // Get Fields
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;

    // Validate destination location
    if ($fields['StockId']['value'] == 0) return 'please select shipment' ;
		
    // Get new Stock
    $query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1 AND type="shipment" AND done=0 AND ready=0', $fields['StockId']['value']) ;
    $result = dbQuery ($query) ;
    $Stock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ($Stock['Id'] <= 0) return sprintf ('%s(%d) Shipment not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;

    // Get Orders to assign
	 // Get Id's from posted checkboxes
	if (!is_array($_POST['Orderlineid'])) return 'please select Orderline(s) to be assigned' ;
	foreach ($_POST['Orderlineid'] as $id => $flag) {
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$query = sprintf ('UPDATE Orderline SET ShipmentId=%d WHERE Orderline.Id=%d', (int)$fields['StockId']['value'], $id) ;
		dbQuery ($query) ;
	}
    return 0 ;
?>
