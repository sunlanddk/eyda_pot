<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;

    // Validate state of order
    if ($Record['Done']) return 'Order Done' ;
    if (!$Record['Ready']) return 'Order not Ready' ;

    // Variables
    $Container = array () ;
    $Item = array () ;
    $ErrorNote = '' ;
    
    $fields = array (
	'StockId'		=> array ('type' => 'integer',	'mandatory' => true),
	'ShipmentId'		=> array ('type' => 'integer',	'mandatory' => true),
	
	'LimitDate'		=> array ('type' => 'date',	'mandatory' => true),
	
	'Allocated'		=> array ('type' => 'checkbox'),
	'Produced'		=> array ('type' => 'checkbox'),
	'Free'		=> array ('type' => 'checkbox'),

	'FirstSort'		=> array ('type' => 'checkbox'),

	'IgnoreShipment'	=> array ('type' => 'checkbox'),
	'IgnoreContainer'	=> array ('type' => 'checkbox'),
	'IgnoreQuantity'	=> array ('type' => 'checkbox'),
	'IgnoreWeight'		=> array ('type' => 'checkbox'),
	
	'ExecuteAllocate'	=> array ('type' => 'checkbox'),
	'ExecuteMove'		=> array ('type' => 'checkbox')
		
    ) ;
     
    // Process posted fields
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;

    // Get Stock to move from
    if ($fields['StockId']['value'] == 0) return 'please select Stock' ;
    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $fields['StockId']['value']) ;
    $result = dbQuery ($query) ;
    $Stock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Stock['Id'] <= 0) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
    if ($Stock['Type'] != 'fixed') return sprintf ('%s(%d) invalid Stock, type "%s"', __FILE__, __LINE__, $Stock['Type']) ;

    // Get Shipment to move to
    $query = sprintf ('SELECT Stock.* FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $fields['ShipmentId']['value']) ;
    $result = dbQuery ($query) ;
    $Shipment = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Shipment['Id'] <= 0) return sprintf ('%s(%d) Shipment not found, id %d', __FILE__, __LINE__, $fields['ShipmentId']['value']) ;
    if ($Shipment['Type'] != 'shipment') return sprintf ('%s(%d) invalid Shipment, type "%s"', __FILE__, __LINE__, $Shipment['Type']) ;
    if ($Shipment['Done']) return sprintf ('%s(%d) Shipment Done', __FILE__, __LINE__) ;
    if ($Shipment['Ready']) return sprintf ('%s(%d) Shipment Ready', __FILE__, __LINE__) ;

    // Check if the parameters for the Shipment fits the Order
    if ((int)$Record['CompanyId'] != (int)$Shipment['CompanyId']) return 'Shipment not for the same Customer' ;
    if (!$fields['IgnoreShipment']['value']) {
	if ((int)$Record['DeliveryTermId'] != (int)$Shipment['DeliveryTermId']) return 'Shipment does not have the Ordered DeliveryTerm' ;
	if ((int)$Record['CarrierId'] != (int)$Shipment['CarrierId']) return 'Shipment does not have the Ordered Carrier' ;
	if ($Record['Address1'] != $Shipment['Address1'] or
	    $Record['Address2'] != $Shipment['Address2'] or
	    $Record['ZIP'] != $Shipment['ZIP'] or
	    $Record['City'] != $Shipment['City'] or
	    (int)$Record['CountryId'] != (int)$Shipment['CountryId']) return 'Shipment does not have the Ordered Delivery Address' ;
    }
    
    // Handle each OrderLine
    foreach ($Line as $i => $l) {
	// Initialize
	$Line[$i]['ItemsNotAllocated'] = array () ;
	
	// Compute real quantities
	if ($l['VariantSize'] or $l['VariantDimension']) {
	    $Quantity = (float)0 ;
	    foreach ($l['Size'] as $j => $s) {
		$Quantity += ($Line[$i]['Size'][$j]['QuantityRemaining'] = ((float)$s['Quantity'] * (100 + (int)$l['Surplus']) / 100)) ;
	    }
	} else {
	    $Quantity = ((float)$l['Quantity'] * (100 + (int)$l['Surplus']) / 100) ;
	}
	$Line[$i]['QuantityRemaining'] = $Quantity ;
	
	// Delivery after specified LimitDate
	if (dbDateDecode($l['DeliveryDate']) > dbDateDecode($fields['LimitDate']['value'])) continue ;

//printf ("Processing Article %s<br>\n", $l['ArticleNumber']) ;

	// Construct Query
	$query = 'SELECT Item.Id AS ItemId, Item.Quantity, Item.ArticleSizeId, Item.Dimension, Item.OrderLineId, 
						Container.Id AS ContainerId' ;
	$query .= ' FROM Item
				INNER JOIN Article ON Article.id=item.ArticleId
				INNER JOIN Container ON Container.Id=Item.ContainerId 
				INNER JOIN Stock ON Stock.Id=Container.StockId' ;

	$query .= sprintf (' WHERE Item.ArticleId=%d AND Item.CaseId=0 AND Item.Active=1', (int)$l['ArticleId']) ;
	if ($l['VariantColor']) $query .= sprintf (' AND Item.ArticleColorId=%d', (int)$l['ArticleColorId']) ;	
	if ($fields['FirstSort']['value'] and $l['VariantSortation']) $query .= sprintf (' AND Item.Sortation=1') ;

	if ($fields['Produced']['value'] and $fields['Allocated']['value']) {
		$query .= sprintf (" AND (Item.OrderLineId=%d OR (Item.FromProductionId=%d and Item.OrderLineId=0))", (int)$l['Id'], (int)$l['ProductionId']) ;
	} else if ($fields['Allocated']['value']) {
	    $query .= sprintf (' AND Item.OrderLineId=%d', (int)$l['Id']) ;
	} else if ($fields['Produced']['value']) {
		$query .= sprintf (" AND Item.FromProductionId=%d and (Item.OrderLineId=0 OR Item.OrderLineId=%d)", (int)$l['ProductionId'], (int)$l['Id']) ;
	} else
	    $query .= ' AND Item.OrderLineId=0'  ;

	$query .= sprintf (' AND Stock.Id=%d ORDER BY IF(OrderLineId>0,0,1), Item.Id', $fields['StockId']['value']) ;
//printf ("%s<br>\n", $query) ;
$tmp = $query ;
	// Get the Items
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    // Check Item
	    $ItemId = (int)$row['ItemId'] ;
	    $ContainerId = (int)$row['ContainerId'] ;
	    $Quantity = (float)$row['Quantity'] ;

	    // Query Validation
	    if ((int)$row['OrderLineId'] != 0 AND (int)$row['OrderLineId'] != $i) return sprintf ('%s(%d) program error, Item (%d) allready allocated', __FILE__, __LINE__, $ItemId) ;
	    
	    // Adjust quantities
	    if ($l['VariantSize'] or $l['VariantSize']) {
			if ($l['VariantSize'])
				$s = &$Line[$i]['Size'][$row['ArticleSizeId']] ;
			else
				$s = &$Line[$i]['Size'][$row['Dimension']] ;
			
			if ($s['QuantityRemaining'] == 0) continue; // KC 2008.09.03
			if ($s['QuantityRemaining'] < $Quantity) {
			    if (!$fields['IgnoreQuantity']['value']) $ErrorNote .= sprintf ('Item %d, Container %d for Line %d exceeds ordered Quantity<br>', $ItemId, $ContainerId, (int)$l['No']) ;
			}
			$s['QuantityRemaining'] -= $Quantity ;   
	    } else {
			if ($Line[$i]['QuantityRemaining'] < $Quantity) {
				if (!$fields['IgnoreQuantity']['value']) $ErrorNote .= sprintf ('Item %d, Container %d for Line %d exceeds ordered Quantity<br>', $ItemId, $ContainerId, (int)$l['No']) ;
			}
	    }
	    $Line[$i]['QuantityRemaining'] -= $Quantity ;

	    // Save Item
	    $Item[$ItemId] = array ('Id' => $ItemId, 'Quantity' => $Quantity, 'OrderLineId' => $i, 'ContainerId' => $ContainerId, 'ArticleSizeId' => (int)$row['ArticleSizeId']) ; 

	    // Save Items Not allocated
	    if ((int)$row['OrderLineId'] == 0) $Line[$i]['ItemsNotAllocated'][] = $ItemId ;
	    
	    // Save Container
	    if (!isset($Container[$ContainerId])) $Container[$ContainerId] = array ('Id' => $ContainerId) ; 
	    $Container[$ContainerId]['Item'][] = $ItemId ;
	}
	dbQueryFree ($result) ;

//printf ("<br>\n") ;
    }
 
    // Any Items at all ?
    if (count($Item) > 0) {
	if (!$fields['IgnoreContainer']['value']) {
	    // Verify other content of containers
	    // Generate Query
	    $query = sprintf ('SELECT Item.Id, Item.ContainerId FROM Item WHERE Item.ContainerId IN (%s) AND Item.Id NOT IN (%s) AND Item.Active=1', implode (',', array_keys ($Container)), implode (',', array_keys ($Item))) ;
//printf ("Container validate query: %s<br>\n", $query) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
		$ErrorNote .= sprintf ('Item %d in Containter %d not required for Order<br>', (int)$row['Id'], (int)$row['ContainerId']) ;
	    }
	    dbQueryFree ($result) ;
	}
    } else {
	// No items found
	$ErrorNote .= 'No Items found for Order<br>' ;
    }

    // Verift net weight of containers
    if (!$fields['IgnoreWeight']['value'] and count($Container) > 0) {
	// Generate Query
	$query = sprintf ('SELECT Container.Id FROM Container WHERE Container.Id IN (%s) AND Container.GrossWeight<=Container.TaraWeight', implode (',', array_keys ($Container))) ;
//printf ("Container weight validate query: %s<br>\n", $query) ;
	    $result = dbQuery ($query) ;
	    while ($row = dbFetch ($result)) {
		$ErrorNote .= sprintf ('Containter %d has zero net Weight<br>', (int)$row['Id']) ;
	    }
	    dbQueryFree ($result) ;
	}

//logPrintVar ($Item, 'Item') ;
//logPrintVar ($Container, 'Container') ;
//logPrintVar ($Line, 'Line') ;

    // Errors ?
    if ($ErrorNote != '') return 'processing stopped<br><br>' . $ErrorNote ;

    // Set Modify Date
    $ModifyDate = dbDateEncode(time()) ;
    
    // Allocate Items to Order
    if ($fields['ExecuteAllocate']['value']) {
	foreach ($Line as $id => $line) {
	    if (count($line['ItemsNotAllocated']) > 0) {
		// Construct query
		$query = sprintf ('UPDATE Item SET OrderLineId=%d, ModifyDate="%s", ModifyUserId=%d WHERE Id IN (%s)', $id, $ModifyDate, $User["Id"], implode (',', $line['ItemsNotAllocated'])) ;
//printf ("Item allocateion, line %d, query '%s'<br>\n", (int)$line['No'], $query) ;
		dbQuery ($query) ;
	    }
	}
    }

    // Move containers
    if ($fields['ExecuteMove']['value']) {
	if (count($Container) > 0) {
	    // Construct query
	    $query = sprintf ('UPDATE Container SET StockId=%d, PreviousStockId=%d, Position="", ModifyDate="%s", ModifyUserId=%d WHERE Id IN (%s)', (int)$Shipment['Id'], (int)$Stock['Id'], $ModifyDate, $User["Id"], implode (',', array_keys ($Container))) ;
//printf ("Container movement query '%s'<br>\n", $query) ;
	    dbQuery ($query) ;
	}
	
	// Show containers in shipment
	return navigationCommandMark ('shipmentcontainers', (int)$Shipment['Id']) ;
    }
    
    return 0 ;
?>
