<?php

    require_once 'lib/http.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/parameter.inc' ;

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/fpdf.inc' ;
    
    global $Line, $Record;

    class PDF1 extends FPDF {

	function Header() {
	    global $Record, $Company, $CompanyMy, $Now, $ProformaInvoice, $ProformaCustoms  ;

	    $this->SetAutoPageBreak(false) ;

	    // Borders
	    $this->Line(15,97,205,97) ;
	    $this->Line(15,115,205,115) ;
	    $this->Line(15,269,205,269) ;
	    $this->Line(15,272,205,272) ;
	    $this->Line(15,282,205,282) ;
	    $this->Line(15,97,15,269) ;
	    $this->Line(15,272,15,282) ;
	    $this->Line(205,97,205,269) ;
	    $this->Line(205,272,205,282) ;

	    // My Company Header
// More layouts Company Logos
//	    $this->Image ('image/novotex/A4Logo.jpg', 140, 13, 64) ;
//	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['ToCompanyId']) ;
//	    $this->Image ($ImageString, 140, 13, 64) ;
	    $ImageString = sprintf ('image/logo/%d.jpg', (int)$Record['ToCompanyId']) ;

		if ((int)$Record['ToCompanyId']==787)
	    	$this->Image ($ImageString, 160, 13, 35 ) ;
		else if ((int)$Record['ToCompanyId']==1430)
	    	$this->Image ($ImageString, 162, 10, 35 ) ;
		else if ((int)$Record['ToCompanyId']==5060)
	    	$this->Image ($ImageString, 162, 10, 35 ) ;
		else if ((int)$Record['ToCompanyId']==516)
	    	$this->Image ($ImageString, 162, 10, 35 ) ;
		else
	    	$this->Image ($ImageString, 140, 13, 64 ) ;
	    $this->SetFont('Arial','',10);
	    $this->SetMargins(146,0,0) ;
	    $this->SetY (34) ;
//	    $this->Cell(49, 5, $CompanyMy['NameExpanded'], 0, 1, 'R') ;
	    $this->SetFont('Arial','',8);
	    $this->Cell(49, 4, $CompanyMy['Name'], 0, 1, 'R') ;
	    $this->Cell(49, 4, $CompanyMy['Address1'], 0, 1, 'R') ;
	    if ($CompanyMy['Address2']) $this->Cell(48, 4, $CompanyMy['Address2'], 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('%s-%s %s', $CompanyMy['CountryName'], $CompanyMy['ZIP'], $CompanyMy['City']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Telephone %s', $CompanyMy['PhoneMain']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Fax %s', $CompanyMy['PhoneFax']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Reg. No %s', $CompanyMy['RegNumber']), 0, 1, 'R') ;
	    $this->Cell(49, 4, sprintf ('Email %s', $CompanyMy['Mail']), 0, 1, 'R') ;

	    // Invoice Address
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(25,0,0) ;
	    $this->SetY (40) ;
	    $this->Cell(80, 4, $Company['Name'], 0, 1) ;
	    $this->Cell(80, 4, $Company['Address1'], 0, 1) ;
	    if ($Company['Address2']) $this->Cell(80, 4, $Company['Address2'], 0, 1) ;
	    $this->Cell(80, 4, sprintf ('%s %s', $Company['ZIP'], $Company['City']), 0, 1) ;
	    $this->Cell(80, 4, $Company['CountryDescription'], 0, 1) ;

	    // Delivery Address
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(25,0,0) ;
	    $this->SetY (68) ;
	    $this->Cell(80, 4, 'Delivery address:', 0, 1) ;
	    $this->Cell(80, 4, $Company['Name'], 0, 1) ;
	    $this->Cell(80, 4, $Record['Address1'], 0, 1) ;
	    if ($Record['Address2']) $this->Cell(80, 4, $Record['Address2'], 0, 1) ;
	    $this->Cell(80, 4, sprintf ('%s %s', $Record['ZIP'], $Record['City']), 0, 1) ;
	    $this->Cell(80, 4, $Record['CountryDescription'], 0, 1) ;

	    // Text
	    $this->SetFont('Arial','B',16);
	    $this->SetMargins(120,0,0) ;
	    $this->SetY (75) ;
	    $orderType = '';
	    if ($ProformaInvoice == 0 and $ProformaCustoms == 0) {
	    	if ($Record['Proposal']) {
	    		$orderType = 'Proposal';
	    	} else {
	    		$orderType = sprintf('Order Confirmation %s', !$Record['Ready'] ? '(draft)' : '');
	    	}
	    } else {
	    	$orderType = 'Proforma Invoice';
	    }
	    $this->Cell(30, 8, $orderType, 0, 1) ;
	    
	    $this->SetFont('Arial','',9);
	    $this->Cell(20, 4, 'Number') ;
	    $this->Cell(50, 4, $Record['Id'], 0, 1) ;
	    $this->Cell(20, 4, 'Date') ;
//    	    $this->Cell(50, 4, ($Record['Ready']) ? date('Y.m.d', dbDateDecode($Record['ReadyDate'])) : '') ;
	    $this->Cell(50, 4, date('Y.m.d', ($Record['Ready']) ? dbDateDecode($Record['ReadyDate']) : $Now)) ;

	    // Header
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,0,0) ;
	    $this->SetY (98) ;
	    $this->Cell(30, 4, 'Customer') ;
	    $this->Cell(74, 4, $Company['Number']) ;
	    $this->Cell(20, 4, 'Page') ;
	    $this->Cell(50, 4, $this->PageNo().' of {nb}', 0, 1) ;

	    $this->Cell(30, 4, 'Send By') ;
	    $this->Cell(74, 4, $Record['CarrierName']) ;
	    $this->Cell(20, 4, 'Sales Rep') ;
	    $this->Cell(50, 4, $Record['SalesUserName'], 0, 1) ;

	    $this->Cell(30, 4, 'Terms of Delivery') ;
	    $this->Cell(74, 4, $Record['DeliveryTermDescription']) ;
	    $this->Cell(20, 4, 'Reference') ;
	    $this->Cell(50, 4, $Record['Reference'], 0, 1) ;

	    $this->Cell(30, 4, 'Terms of Payment') ;
	    $this->Cell(74, 4, $Record['PaymentTermDescription']) ;
	    $this->Cell(20, 4, 'Date Due') ;
	    $this->Cell(80, 4, '', 0, 1) ;

	    // Initialize for main page
	    $this->SetFont('Arial','',9);
	    $this->SetMargins(16,118,6) ;
	    $this->SetY (118) ;
	    $this->SetAutoPageBreak(true, 30) ;
	}

	function Footer () {
	    global $Record, $Total, $LastPage ;

	    if (!$LastPage) {
		$this->SetXY (16, 273) ;
		$this->Cell(25, 4, 'Carried over') ;
	    }
	    $this->SetXY (127, 273) ;
	    $this->Cell(25, 4, ($Total['UnitName'] != '') ? 'Total Quantity' : '', 0, 0, 'R') ;
	    $this->Cell(52, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
	    $this->SetXY (127, 277) ;
	    $this->Cell(25, 4, ($Total['UnitName'] != '') ? number_format ($Total['Quantity'], 2, ',', '.') . ' ' . $Total['UnitName'] :
									 	number_format ($Total['Quantity'], 2, ',', '.'), 0, 0, 'R') ;
	    $this->Cell(52, 4, number_format ($Total['Amount'], 2, ',', '.'), 0, 0, 'R') ;

	}

	function RequireSpace ($space) {
	    if ($this->y > ($this->fh-$this->bMargin-$space)) $this->AddPage() ;
	}

	function TruncString ($s, $w) {
	    // Truncate string to specified width
	    $s = (string)$s ;
	    $w *= 1000/$this->FontSize ;
	    $cw = &$this->CurrentFont['cw'] ;
	    $l = strlen ($s) ;
	    for ($i = 0 ; $i < $l ; $i++) {
		$w -= $cw[$s{$i}] ;
		if ($w < 0) break ;
	    }
	    return substr($s, 0, $i) ;
	}
    }


// Use previously generated pdf-file ?
    if ($Record['Ready'] and $ProformaInvoice==0 and $ProformaCustoms==0) {
	$file = fileName ('order', (int)$Record['Id']) ;
	if (is_file($file)) {
	    $pdfdoc = file_get_contents($file) ;
    	    httpNoCache ('pdf') ;
	    httpContent ('application/pdf', sprintf('Order%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
	    print ($pdfdoc) ;
	    return 0 ;
	}
    }

    // Variables
    $Total = array () ;
    $CustomsPosition = array () ;
    $ProformaCustomsFactor = 0.1 ;
    $LastPage = false ;
    $Now = time () ;

    // Get this orders Company information
//    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', parameterGet('CompanyMy')) ;
    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['ToCompanyId']) ;
    $result = dbQuery ($query) ;
    $CompanyMy = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$CompanyMy['Id'] <= 0) return 'CompanyMy not found' ;

    // Insert tripple-space in name
    if ($CompanyMy['Name'] != '') {
	$s = $CompanyMy['Name']{0} ;
	$n = 1 ;
	while ($CompanyMy['Name']{$n}) $s .= '  ' . $CompanyMy['Name']{$n++} ;
	$CompanyMy['NameExpanded'] = strtoupper($s) ;
    }

    // Get Full Company information
	// Insert Country invoiceFooter
	$query = sprintf ('SELECT Company.*, Country.Id as CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage, PaymentInformation.Description AS CountryInvoiceFooter
						FROM Company
						LEFT JOIN Country ON Country.Id=Company.CountryId LEFT JOIN PaymentInformation ON PaymentInformation.CompanyId=Company.Id and PaymentInformation.ToCompanyId=%d
						WHERE Company.Id=%d', (int)$Record['ToCompanyId'], (int)$Record['CompanyId']) ;
    $result = dbQuery ($query) ;
    $Company = dbFetch ($result) ;
    dbQueryFree ($result) ;
	if ($Company['CountryInvoiceFooter'] == '') {
		if ((parameterGet('CompanyMy') != $Record['ToCompanyId']))  {
			$query = sprintf ('SELECT Company.*, Country.Id as CountryId, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage, PaymentInformation.Description AS CountryInvoiceFooter FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId LEFT JOIN PaymentInformation ON PaymentInformation.CountryId=Country.Id WHERE Company.Id=%d and PaymentInformation.ToCompanyId=%d', (int)$Record['CompanyId'], (int)$Record['ToCompanyId']) ;
			$result = dbQuery ($query) ;
			$Company = dbFetch ($result) ;
			dbQueryFree ($result) ;
			if ($Company['CountryInvoiceFooter'] == '')
				$query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage,PaymentInformation.Description AS CountryInvoiceFooter FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId LEFT JOIN PaymentInformation ON PaymentInformation.CountryId=9999 WHERE Company.Id=%d and PaymentInformation.ToCompanyId=%d', (int)$Record['CompanyId'], (int)$Record['ToCompanyId']) ;
		} else
			    $query = sprintf ('SELECT Company.*, Country.Name AS CountryName, Country.Description AS CountryDescription, Country.EUMember, Country.VATPercentage, Country.InvoiceFooter AS CountryInvoiceFooter FROM Company LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Company.Id=%d', (int)$Record['CompanyId']) ;
		$result = dbQuery ($query) ;
		$Company = dbFetch ($result) ;
		dbQueryFree ($result) ;
	}
    // Get Country information for Record
    $Record['CountryDescription'] = tableGetField ('Country', 'Description', (int)$Record['CountryId']) ;
    $Record['CarrierName'] = tableGetField ('Carrier', 'Name', (int)$Record['CarrierId']) ;
    $Record['DeliveryTermDescription'] = tableGetField ('DeliveryTerm', 'Description', (int)$Record['DeliveryTermId']) ;
    $Record['PaymentTermDescription'] = tableGetField ('PaymentTerm', 'Description', (int)$Record['PaymentTermId']) ;
    $Record['CurrencyName'] = tableGetField ('Currency', 'Name', (int)$Record['CurrencyId']) ;
    $Record['SalesUserName'] = tableGetField ('User', 'CONCAT(FirstName," ",LastName)', (int)$Record['SalesUserId']) ;


	    // Loop lines to do calculations
	if ($ProformaCustoms==1) {
    $CustomsPosition[0][''] = array ('Id' => 0, 'Name' => 'None', 'CustomsPositionDesc' => 'None', 'Amount' => 0, 'Quantity' => 0) ;
    foreach ($Line as $id => $line) {
		if ($line['CustomsPos'] == '99999999') continue ;
		// Set Unit for Total
		if ($line['No'] == 1) {
	    	$Total['UnitName'] = $line['UnitName'] ;
		    $Total['UnitDecimals'] = (int)$line['UnitDecimals'] ;
		} else {
		    if ($Total['UnitName'] != $line['UnitName']) $Total['UnitName'] = '' ;
		}

		$quantity = $line['Quantity'] ;
		$amount = $quantity * $line['PriceSale'] ;
		if ((int)$line['Discount'] > 0) $amount -= $amount * (int)$line['Discount'] / 100 ;

		// Save Totals
		$Total['Amount'] += $amount * $ProformaCustomsFactor;
		$Total['Quantity'] += $quantity ;

		// Handle Customs Positions
		$j = (int)$line['CustomsPositionId'] ;
//		$j = $line['CustomsPos'] ;
		$o = $line['WorkCountryId'] ;
		if (!isset($CustomsPosition[$j][$o])) {
			$CustomsPosition[$j][$o] = array ('Id' => $j, 'Name' => $line['CustomsPos'], 'CustomsPositionDesc' => $line['CustomsPositionDesc'], 'Amount' => 0, 'Quantity' => 0, 'Origin' => $line['WorkCountryName']) ;
		}
		$CustomsPosition[$j][$o]['Quantity'] += $quantity ;
		$CustomsPosition[$j][$o]['Amount'] += $amount ;
		$CustomsPosition[$j][$o]['UnitName'] = $line['UnitName'] ;
		$CustomsPosition[$j][$o]['UnitDecimals'] = (int)$line['UnitDecimals'] ;
    }
    }


    // Make PDF
    $pdf=new PDF('P', 'mm', 'A4') ;
    $pdf->AliasNbPages() ;
    $pdf->SetAutoPageBreak(true, 30) ;
    $pdf->AddPage() ;

	if ($ProformaCustoms==0) {
    // Order Invoice Header
    if ($Record['InvoiceHeader']) {
	$pdf->MultiCell (186, 4, $Record['InvoiceHeader']) ;
	$pdf->Ln (4) ;
    }

    foreach ($Line as $line) {
	if ($ProformaInvoice == 1) {
	    $tquery = sprintf ('select * from (season s, collection c, collectionmember cm) where s.id=%d and c.seasonid=s.id and c.active=1 and cm.collectionid=c.id and cm.active=1 and cm.articleid=%d and cm.articlecolorid=%d and cm.cancel=1', (int)$Record['SeasonId'], $line['ArticleId'], $line['ArticleColorId']) ;
		$tresult = dbQuery ($tquery) ;
		$Cancelled = dbNumRows($tresult) ;
		dbQueryFree ($tresult) ;
	}

	If ($Cancelled>0) continue ;	
	// Set Unit for Total
	if ($line['No'] == 1) {
	    $Total['UnitName'] = $line['UnitName'] ;
	    $Total['UnitDecimals'] = (int)$line['UnitDecimals'] ;
	} else {
	    if ($Total['UnitName'] != $line['UnitName']) $Total['UnitName'] = '' ;
	}

	// Compute required space for the Position
	$space = 14 ;
	if ((int)$line['CaseId'] > 0) $space += ((int)$line['ArticleCertificateId'] > 0) ? 8 : 4 ;
	if ($line['VariantColor']) $space += 4 ;
	if (($line['VariantSize']) or ($line['VariantDimension'])) $space += (int)((((count($line['Size'])+5)/6)*10)+2) ;
	if ($line['InvoiceFooter']) $space += 10 ;
	if ($Record['VariantCodes'] and ($line['VariantSize'] or $line['VariantDimension'])) $space += (int)count($line['Size']) + 2;
	$pdf->RequireSpace ($space) ;


	//Image
	$article_image_link = sprintf("%s\\thumbnails\\%s_%s.jpg", $Config['imageStore'], $line['ArticleNumber'], $line['ColorNumber']);
	if (file_exists($article_image_link)) {
	    $pdf->Image($article_image_link, $pdf->GetX(), $pdf->GetY(), 20, 20);
	    $image_exists = true;
	    $image_bottom = $pdf->GetY() + 20;
	    $image_width  = 20;
	} else {
		$image_exists = false;
		$image_width  = 0;
	}

	// Pos
	$pdf->SetX($pdf->GetX() + $image_width);
	$pdf->Cell(15, 4, 'Pos') ;
	$pdf->Cell(26, 4, count($line['No'])) ;// $line['No']) ;
	$pdf->Ln () ;
	if ((int)$line['CaseId'] > 0) {
		$pdf->SetX($pdf->GetX() + $image_width);
	    $pdf->Cell(15, 4, 'Case') ;
	    $pdf->Cell(26, 4, $line['CaseId']) ;
	    if ($line['CustomerReference'] != '') {
		$pdf->Cell(100, 4, 'Customer reference: ' . $line['CustomerReference']) ;
	    }
	    $pdf->Ln () ;
	}

	// Article
	$pdf->SetX($pdf->GetX() + $image_width);
	$pdf->Cell(15, 4, 'Article') ;
	$pdf->Cell(26, 4, $line['ArticleNumber']) ;
	$pdf->Cell(81, 4, $pdf->TruncString($line['Description'], 70)) ;
//	$pdf->Ln () ;
	// CustomsPosition
	if ($ProformaCustoms==1) {
		$pdf->Cell(27, 4, 'Customs Posit�on:') ;
		$pdf->Cell(18, 4, $line['CustomsPos']) ;
		$pdf->Cell(12, 4, 'Origin:') ;
		$pdf->Cell(15, 4, $line['WorkCountryName']) ;
	}
	$pdf->Ln () ;


	// Certificate
	if ((int)$line['CaseId'] > 0 and (int)$line['ArticleCertificateId'] > 0) {
		$pdf->SetX($pdf->GetX() + $image_width);
	    $pdf->Cell(15, 4, 'Certificate') ;
	    $pdf->Cell(70, 4, tableGetFieldWhere (
		'ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId',
		'CertificateType.Name',
		sprintf ('ArticleCertificate.Id=%d', (int)$line['ArticleCertificateId'])
	    )) ;
	    $pdf->Ln () ;
	}

	// Color
	if ($line['VariantColor']) {
		$pdf->SetX($pdf->GetX() + $image_width);
	    $pdf->Cell(15, 4, 'Colour') ;
	    if ((int)$line['ArticleColorId'] > 0) {
		$pdf->Cell(26, 4, $line['ColorNumber']) ;
		$pdf->Cell(70, 4, $pdf->TruncString($line['ColorDescription'], 70)) ;
	    } else {
		$pdf->Cell(10, 4, '-') ;
	    }
	    $pdf->Ln () ;
	}

	if($image_exists) {
	 	$pdf->SetY($image_bottom);
	}

	// Sizes
	if ($line['VariantSize'] or $line['VariantDimension'] ) {

	    $n = 0 ;
	    foreach ($line['Size'] as $size) {

			if ($size['Quantity']==0) {
				continue;
			}
			if (($n % 6) == 0) {
				if ($n > 0) $pdf->Ln () ;
				$pdf->Ln (2) ;
				$pdf->RequireSpace (8) ;
				$pdf->Cell(15, 4, $line['VariantSize']?'Size':'Dim') ;
				$pdf->Ln () ;
				$pdf->Cell(15, 4, 'Quantity') ;
			}

			$pdf->SetXY ($pdf->GetX(), $pdf->GetY()-4) ;
	       		$pdf->Cell(16, 4, $size['Name']) ;
			$pdf->SetXY ($pdf->GetX()-16, $pdf->GetY()+4) ;
			$pdf->Cell(16, 4, number_format ((float)$size['Quantity'], (int)$line['UnitDecimals'], ',', '.')) ;
			$n++ ;
	    }
	    $pdf->Ln () ;
	}

	// Price header
	$pdf->SetXY (137, $pdf->GetY()-8) ;
	if (((float)$line['Quantity'] == 1 and $Record['ToCompanyId'] == 5060)) {
		$pdf->Cell(15, 4, '', 0, 0, 'R') ;
		$pdf->Cell(15, 4, '', 0, 0, 'R') ;
	} else {
		$pdf->Cell(15, 4, 'Quantity', 0, 0, 'R') ;
		$pdf->Cell(15, 4, 'Price', 0, 0, 'R') ;
	}
	$pdf->Cell(15, 4, ((int)$line['Discount'] > 0) ? 'Discount' : '', 0, 0, 'R') ;
	$pdf->Cell(22, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
	$pdf->Ln () ;

	// Prices
	if ($ProformaCustoms==1) $line['PriceSale']=$line['PriceSale']*$ProformaCustomsFactor ;
	$pdf->SetX (137) ;
	if (((float)$line['Quantity'] == 1 and $Record['ToCompanyId'] == 5060))
		$pdf->Cell(15, 4, '', 0, 0, 'R') ;
	else
		$pdf->Cell(15, 4, number_format((float)$line['Quantity'], (int)$line['UnitDecimals'], ',', '.') . ' ' . $line['UnitName'], 0, 0, 'R') ;
	$Total['Quantity'] += $line['Quantity'] ;
	if (((float)$line['Quantity'] == 1 and $Record['ToCompanyId'] == 5060))
		$pdf->Cell(15, 4, '', 0, 0, 'R') ;
	else
		if ($line['PriceSale']>0.9)
			$pdf->Cell(15, 4, number_format($line['PriceSale'], 2, ',', '.'), 0, 0, 'R') ;
		else
			$pdf->Cell(15, 4, number_format($line['PriceSale'], 4, ',', '.'), 0, 0, 'R') ;
	$pdf->Cell(15, 4, ((int)$line['Discount'] > 0) ? ($line['Discount'] . ' %') : '', 0, 0, 'R') ;
	$v = $line['Quantity'] * $line['PriceSale'] ;
	if ((int)$line['Discount'] > 0) $v -= $v * (int)$line['Discount'] / 100 ;
	$Total['Amount'] += $v ;
	$pdf->Cell(22, 4, number_format ($v, 2, ',', '.'), 0, 0, 'R') ;
	$pdf->Ln () ;

	// Delivery time
	$pdf->Ln (2) ;
	$pdf->Cell(15, 4, 'Delivery') ;
	$pdf->Cell(60, 4, date('Y.m.d', dbDateDecode($line['DeliveryDate']))) ;
	$pdf->Ln () ;

	// OrderLine Invoice Footer
	if ($line['InvoiceFooter']) {
	    $pdf->Ln (2) ;
	    $pdf->MultiCell (186, 4, $line['InvoiceFooter']) ;
	}
	if ($Record['VariantCodes'] and ($line['VariantSize'] or $line['VariantDimension'])) {
      $pdf->Ln (2) ;
	$pdf->Cell(15, 4, 'EANs') ;
	foreach ($line['Size'] as $size) {
		if ($size['Quantity']==0) {
			continue;
		}
            $pdf->Cell(26, 4, $size['Name'] . ': ') ;
            $pdf->Cell(60, 4, $size['VariantCode']) ;
		$pdf->Ln () ;
		$pdf->Cell(15, 4, '') ;
//return sprintf('codes: %d, size %d',$Record['VariantCodes'],$line['VariantSize'] ) ;
	}
      $pdf->Ln (1) ;
	}

	$pdf->Ln (4) ;
    }

	// Order Invoice Footer
	if ($Record['InvoiceFooter']) {
	$pdf->MultiCell (186, 4, $Record['InvoiceFooter']) ;
	}

//Insert Country InvoiceFooter
	// Company Footer
	if ($Company['CountryInvoiceFooter'] != '') {
	$pdf->RequireSpace (12) ;
	$pdf->MultiCell (186, 4, $Company['CountryInvoiceFooter']) ;
	$pdf->Ln(4) ;
	}
	} // END Normal not Proforma customs!!!

	if ($ProformaInvoice==1) {
		// VAT
		if ((int)$Company['VATPercentage'] > 0) {
			$pdf->RequireSpace (10) ;
			$v = $Total['Amount'] * $Company['VATPercentage'] / 100 ;
			$Total['Amount'] += $v ;
			$Total['CarryOver'] += $v ;
			$pdf->Cell(188, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
			$pdf->Ln () ;
			$pdf->Cell(100, 4, sprintf ('VAT %s %%', $Company['VATPercentage'])) ;
			$pdf->Cell(88, 4, number_format ($v, 2, ',', '.'), 0, 0, 'R') ;
			$pdf->Ln () ;
			$pdf->Ln (4) ;
		}
		// VAT info
		$pdf->RequireSpace (10);
		if ((int)$Company['EUMember'] && ($Company['CountryName'] !== 'DK')) {
		  //$pdf->RequireSpace (10) ;
			$pdf->Cell(20, 4, 'Customer') ;
			$pdf->Cell(80, 4, 'VAT No.: '. $Company['VATNumber']) ;
			$pdf->Ln () ;
		} //20060602 jle always show Novotex VAT No
		$pdf->Cell(20, 4, $CompanyMy['Name'] . ' VAT No.: '. $CompanyMy['VATNumber']) ;
	//	$pdf->Cell(80, 4, 'VAT No.: '. $CompanyMy['VATNumber']) ;
		$pdf->Ln () ;
		$pdf->Ln (4) ;
	}

    // CustomsPositions
	if ($ProformaCustoms==1) {
//		if ((int)$Company['InvoiceCustomsInfo']) {
		$pdf->RequireSpace (16) ;
		$pdf->Cell (80, 4, 'Customs information') ;
    		$pdf->Ln () ;
		foreach ($CustomsPosition as $position) {
			foreach ($position as $pos) {
				if ((int)$pos['Quantity'] == 0) continue ;
				if ((int)$pos['Name'] == '99999999') continue ;
				$pdf->Cell (10, 4, 'Pos') ;
				$pdf->Cell (20, 4, $pos['Name'], 0, 0, 'R') ;
				$pdf->Cell (58, 4, $pdf->TruncString($pos['CustomsPositionDesc'], 57), 0, 0) ;
				$pdf->Cell (13, 4, $pos['Origin'], 0, 0) ;
				$pdf->Cell (9, 4, 'Qty', 0, 0) ;
				$pdf->Cell (25, 4, number_format ((float)$pos['Quantity'], $pos['UnitDecimals'], ',', '.') . ' ' . $pos['UnitName'], 0, 0, 'R') ;
				$pdf->Cell (30, 4, 'Amount ' . $Record['CurrencyName'], 0, 0, 'R') ;
				$pdf->Cell (20, 4, number_format ((float)$pos['Amount']*$ProformaCustomsFactor, 2, ',', '.'), 0, 0, 'R') ;
    			$pdf->Ln () ;
			}
		}
		$pdf->Ln(4) ;
//		}
    }

    // Last Page generated
    $LastPage = True ;

    // Generate PDF document
    $pdfdoc = $pdf->Output('', 'S') ;

    // Download
	if ($Config['savePdfToFile'] !== true) {
		if (headers_sent()) return 'stop' ;
		httpNoCache ('pdf') ;
		httpContent ('application/pdf', sprintf('Order%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
		print ($pdfdoc) ;
	}

    // Save file
    if ($Record['Ready'] and $ProformaInvoice==0 and $ProformaCustoms==0) {
	$file = fileName ('order', (int)$Record['Id'], true) ;
	$resource = fopen ($file, 'w') ;
	fwrite ($resource, $pdfdoc) ;
	fclose ($resource) ;
	chmod ($file, 0640) ;
    }
?>
