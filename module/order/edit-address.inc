<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;

    itemSpaceRaw('<h2>Shipping Address</h2>');
    $query = sprintf ('SELECT * FROM orderadresses WHERE OrderId=%d AND Active=1 AND Type="shipping"', $Id) ;
    $res = dbQuery ($query) ;
    $shipping = dbFetch ($res) ;
    dbQueryFree ($res) ;
    itemFieldRaw ('Name', formText ('AltCompanyName', $shipping['AltCompanyName'], 50)) ;
    itemFieldRaw ('Street', formText ('Address1', $shipping['Address1'], 50)) ;
    itemFieldRaw ('', formText ('Address2', $shipping['Address2'], 50)) ;
    itemFieldRaw ('ZIP', formText ('ZIP', $shipping['ZIP'], 20)) ;
    itemFieldRaw ('City', formText ('City', $shipping['City'], 50)) ;
    itemFieldRaw ('Country',  formDBSelect ('CountryId', (int)$shipping['CountryId'], 'SELECT Id, CONCAT(Description," ",Name) AS Value FROM Country WHERE Active=1 ORDER BY Name', 'width:250px;')) ;
    itemFieldRaw ('Packageshop', formText ('PackageShop', $shipping['PackageShop'], 50)) ;
    itemSpace () ;
    itemSpace () ;

    itemSpaceRaw('<h2>Billing Address</h2>');
    $query = sprintf ('SELECT * FROM orderadresses WHERE OrderId=%d AND Active=1 AND Type="invoice"', $Id) ;
    $res = dbQuery ($query) ;
    $billing = dbFetch ($res) ;
    dbQueryFree ($res) ;

    itemFieldRaw ('Name', formText ('billing_AltCompanyName', $billing['AltCompanyName'], 50)) ;
    itemFieldRaw ('Street', formText ('billing_Address1', $billing['Address1'], 50)) ;
    itemFieldRaw ('', formText ('billing_Address2', $billing['Address2'], 50)) ;
    itemFieldRaw ('ZIP', formText ('billing_ZIP', $billing['ZIP'], 20)) ;
    itemFieldRaw ('City', formText ('billing_City', $billing['City'], 50)) ;
    itemFieldRaw ('Country',  formDBSelect ('billing_CountryId', (int)$billing['CountryId'], 'SELECT Id, CONCAT(Description," ",Name) AS Value FROM Country WHERE Active=1 ORDER BY Name', 'width:250px;')) ;
    // itemFieldRaw ('Packageshop', formText ('PackageShop', $billing['PackageShop'], 50)) ;

    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
