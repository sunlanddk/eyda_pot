<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    function flag (&$Record, $field) {
	$s = ($Record[$field]) ? sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) : 'No' ;
	itemField ($field, $s) ;
    }

    itemStart () ;
    itemHeader () ;
    itemFieldIcon ('Order', (int)$Record['OrderId'], 'order.png', 'orderview', (int)$Record['OrderId']) ;     

    itemSpace () ;
	if ($User['Extern']) 
		itemField('Customer', $Record['CompanyNumber']) ;
	else
		itemFieldIcon ('Customer', $Record['CompanyNumber'], 'company.gif', 'customerview', $Record['CompanyId']) ;
    itemField ('Name', $Record['CompanyName']) ;     
 
    itemSpace () ;
	if ($User['Extern'] OR $User['SalesRef']) 
    itemField('Article', $Record['ArticleNumber']) ;     
	else
    itemFieldIcon ('Article', $Record['ArticleNumber'], 'article.gif', 'articleview', (int)$Record['ArticleId']) ;     
//    itemField ('Description', $Record['ArticleDescription']) ;     
    if ((int)$Record['CaseId'] > 0) {
	itemSpace () ;
	itemFieldIcon ('Case', (int)$Record['CaseId'], 'case.gif', 'caseview', (int)$Record['CaseId']) ;     
    }
if ($Record['ProductionId']) {
	itemSpace () ;
	itemFieldIcon ('Production', (int)$Record['ProductionNo'], 'production.gif', 'productionview', (int)$Record['ProductionId']) ;     
    }
    
    itemSpace () ;
    itemField ('Pos', (int)$Record['No']) ;
    itemField ('Description', $Record['Description']) ;

    itemSpace () ;
//	$tp = dbDateDecode($Record['PrefDeliveryDate']) ;
//    $tc = dbDateDecode($Record['ConfirmedDate']) ;
    $t = dbDateDecode($Record['DeliveryDate']) ;
/*
    if (!$User['Extern'] AND !$User['SalesRef']) 
		itemField ('Requested', ($Record['PrefDeliveryDate'] > '2000-01-01') ? $Record['PrefDeliveryDate'] : '') ;
*/
	if (!$User['Extern'] AND !$User['SalesRef']) 
		itemField ('Confirmed', ($Record['ConfDeliveryDate'] > '2000-01-01') ? $Record['ConfDeliveryDate'] : '') ;
    itemField ('Delivery', ($t > 0) ? date('Y-m-d', $t) : '') ;

	// Table of delay dates if some
/*
	$query = sprintf('select * from orderdelay where orderlineid=%d and active=1 order by delayeddate DESC', $Id) ;
	$res = dbQuery ($query) ;
	
	if (dbNumRows($res) > 0) {
		while ($row = dbFetch ($res)) {
		    $Delay[] = $row ;
		}
		$field = '<table><tr>' ;
		foreach ($Delay as $i => $s)	$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : ' width=70') . '>' . date('Y-m-d',dbDateDecode($s['DelayedDate'])) . '</td>' ;
		$field .= '<td width=60></td>' ;
		$field .= '</tr>' ;
		$field .= '<tr>' ;
		foreach ($Delay as $i => $s) $field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : '') . '>' . number_format ((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
		$field .= '<td style="padding-left=8px">' . htmlentities($Record['UnitName']) . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Planned', $field) ; 
		itemSpace () ;
	}
	dbQueryFree ($res) ;
*/

	
	itemSpace();
/*
	if (!$User['Extern']  AND !$User['SalesRef']) 
		itemField ('Request Comment', $Record['RequestComment']);
	itemSpace();
 */   
    if ($Record['VariantColor']) {
		$field = '' ;
		if ((int)$Color['Id'] > 0) {
			$field .= sprintf ('<p class=list style="display:inline;">%s (%s)</p>', $Color['ColorNumber'], $Color['ColorDescription']) ;
			if ((int)$Color['ColorGroupId'] > 0) {
			$field .= sprintf ('<div style="width:80px;height:15px;margin-left:8px;margin-top:1px;background:#%02x%02x%02x;display:inline;"></div>', $Color['ValueRed'], $Color['ValueGreen'], $Color['ValueBlue']) ;
			}
		}
		itemSpace () ;
		itemFieldRaw ('Colour', $field) ;
		}
    itemSpace () ;

    if (($Record['VariantSize'])or($Record['VariantDimension'])) {
		$field = '<table><tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : ' width=60') . '>' . htmlentities($s['Name']) . '</td>' ;
		$field .= '<td width=60></td>' ;
		$field .= '</tr>' ;
		$field .= '<tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' : '') . '>' . number_format ((float)$s['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
		$field .= '<td style="padding-left=8px">' . htmlentities($Record['UnitName']) . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Quantity', $field) ; 
		itemSpace () ;
		$field = '<table><tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' :  ' width=60') . '>' . number_format ((float)$s['QuantityReceived'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
		$field .= '<td style="padding-left=8px"  width=60>' . htmlentities($Record['UnitName']) . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Prepacked', $field) ; 
		itemSpace () ;
		$field = '<table><tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' :  ' width=60') . '>' . number_format ((float)$s['QuantityUnpack'], (int)$Record['UnitDecimals'], ',', '.') . '</td>' ;
		$field .= '<td style="padding-left=8px"  width=60>' . htmlentities($Record['UnitName']) . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('Unpacked', $field) ; 
		itemSpace () ;
		$field = '<table><tr>' ;
		foreach ($Size as $i => $s) 
			$field .= '<td align=right' . (($i == 0) ? ' style="padding-left=8px"' :  ' width=60') . '>' .$s['BoxNumber'] . '</td>' ;
		$field .= '<td style="padding-left=8px"  width=60>' . '' . '</td>' ;
		$field .= '</tr></table>' ;	
		itemFieldRaw ('BoxNumber', $field) ; 
		itemSpace () ;

		itemField ('Quantity Total', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ;
    } else {
		itemField ('Quantity', number_format((float)$Record['Quantity'], (int)$Record['UnitDecimals'], ',', '.') . ' ' . $Record['UnitName']) ;
    }
    if (!$User['Extern'] AND !$User['SalesRef']) 
		itemField ('Surplus', (int)$Record['Surplus'] . ' %') ;

    itemSpace () ;
//    itemField ('Cost Price', number_format ((float)$Record['PriceCost'], 2, ',', '.') . ' Kr') ;
    itemField ('Sales Price', number_format ((float)$Record['PriceSale'], 2, ',', '.') . ' ' . $Record['CurrencySymbol']) ;
    itemField ('Discount', (int)$Record['Discount'] . ' %') ;

    itemSpace () ;
    if (!$User['Extern'] AND !$User['SalesRef']) 
		itemFieldText ('CustArtRef', $Record['CustArtRef']) ;
    if (!$User['Extern'] AND !$User['SalesRef']) 
		itemFieldText ('LineFooter', $Record['InvoiceFooter']) ;
    itemSpace () ;
    flag ($Record, 'Done') ; 

    if (!$User['Extern']) 
		itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
