<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Get next DeliveryDate 
    $query = sprintf ('SELECT MIN(OrderLine.DeliveryDate) AS DeliveryDate FROM OrderLine WHERE OrderLine.OrderId=%d AND OrderLine.Active=1 AND OrderLine.DeliveryDate>="%s"', (int)$Record['Id'], dbDateOnlyEncode(time()), $Id) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    dbQueryFree ($res) ;
    $LimitDate = ($row['DeliveryDate']) ? dbDateDecode($row['DeliveryDate']) : time() ; 
    
    itemStart () ;
    itemHeader () ;
    itemField ('Order', (int)$Record['Id']) ;
    itemField ('Company', sprintf ('%s (%d)', $Record['CompanyName'], $Record['CompanyNumber'])) ;     
    itemSpace () ;
    itemEnd () ;
     
    formStart () ;
    formCalendar () ;

    itemStart () ;    
    itemHeader() ;

    // From Stock
//    itemField ('Default', 'Id 16') ;
    itemFieldRaw ('From Stock', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock 
	WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1  AND Stock.FromCompanyId=%d ORDER BY Value', $Record['ToCompanyId']), 'width:250px;')) ;

    // To Shipment
    itemSpace () ;
    $query = sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="shipment" AND Active=1 AND CompanyId=%d ORDER BY Value', (int)$Record['CompanyId']) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    $count = dbNumRows ($res) ;
    dbQueryFree ($res) ;
    switch ($count) {
	case 0 :
	    itemField ('To Shipment', 'None') ;
	    break ;	    

	case 1 :
	    itemField ('To Shipment', $row['Value']) ;
	    print formHidden ('ShipmentId', $row['Id']) ;
	    break ;

	default :
	    itemFieldRaw ('To Shipment', formDBSelect ('ShipmentId', 0, $query, 'width:250px')) ;
	    break ;
    } 
    
    itemSpace () ;
    itemSpace () ;
    itemFieldRaw ('Orders until', formDate ('LimitDate', $LimitDate)) ;
    itemSpace () ;
    itemFieldRaw ('Items', formCheckBox ('Allocated', 0) . 'Allocated to order') ;
    itemFieldRaw ('', formCheckBox ('Produced', 1). 'Produced to order') ;
    itemSpace () ;
    itemFieldRaw ('Variants', formCheckBox ('FirstSort', 1) . '1st Sortation Only') ;
    itemSpace () ;
    itemFieldRaw ('Ignore', formCheckBox ('IgnoreShipment', 0) . 'Errors from Shipment mismatch') ;
    itemFieldRaw ('', formCheckBox ('IgnoreContainer', 0) . 'Errors from other Items in Containers') ;
    itemFieldRaw ('', formCheckBox ('IgnoreQuantity', 0) . 'Errors from Items above Ordered Quantity') ;
    itemFieldRaw ('', formCheckBox ('IgnoreWeight', 0) . 'Zero net weight for Container') ;
    itemSpace () ;
    itemFieldRaw ('Execute', formCheckBox ('ExecuteAllocate', 1) . 'Allocation') ;
    itemFieldRaw ('', formCheckBox ('ExecuteMove', 1) . 'Container move') ;    
    itemEnd () ;   

    formEnd () ;

    return 0 ;
?>
