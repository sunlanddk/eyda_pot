<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc';
    

    $fields = array (
  	'CompanyId'		=> array ('type' => 'set'),
  	'ToCompanyId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'Description'		=> array (						'check' => true),
	'Reference'		=> array (						'check' => true),

	'SalesUserId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'CurrencyId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'PaymentTermId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'DeliveryTermId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'CarrierId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),

	'Address1'		=> array (						'check' => true),
	'Address2'		=> array (						'check' => true),
	'ZIP'			=> array (						'check' => true),
	'City'			=> array (						'check' => true),
	'CountryId'		=> array ('type' => 'integer',				'check' => true),
	
	'InvoiceHeader'		=> array (						'check' => true),
	'InvoiceFooter'		=> array (						'check' => true),

	'OrderDoneId'		=> array ('type' => 'integer'),

	'Ready'			=> array ('type' => 'checkbox',				'check' => true),
	'ReadyUserId'		=> array ('type' => 'set'),
	'ReadyDate'		=> array ('type' => 'set'),
	'Done'			=> array ('type' => 'checkbox',				'check' => true),
	'DoneUserId'		=> array ('type' => 'set'),
	'DoneDate' 		=> array ('type' => 'set'),

	'ExchangeRate'		=> array ('type' => 'set')	
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $User, $Navigation, $Record, $Id, $fields ;

	switch ($fieldname) {
	    case 'Ready':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Any Order Positions
		$query = sprintf ("SELECT OrderLine.*, Article.Number AS ArticleNumber, Article.VariantColor, Article.CustomsPositionId, 
			Article.MaterialCountryId, Article.KnitCountryId, Article.WorkCountryId 
			FROM OrderLine LEFT JOIN Article ON Article.Id=OrderLine.ArticleId WHERE OrderLine.OrderId=%d AND OrderLine.Active=1", $Id) ;
	
		$result = dbQuery ($query) ;
		$n = 0 ;
		while ($row = dbFetch ($result)) {
		    // Validate OrderLine
		    if ($row['VariantColor'] and (int)$row['ArticleColorId'] == 0) return sprintf ('line %d, article %s: no color assigned', (int)$row['No'], $row['ArticleNumber']) ;
		    if ((int)$row['Quantity'] == 0) return sprintf ('line %d, article %s: zero Quantity', (int)$row['No'], $row['ArticleNumber']) ;
//		    if ($row['PriceSale'] == 0) return sprintf ('line %d, article %s: zero Price', (int)$row['No'], $row['ArticleNumber']) ;
		    
		    // Validate Article
		    if ((int)$row['CustomsPositionId'] <= 0) return sprintf ('line %d, article %s: no Customs Position', (int)$row['No'], $row['ArticleNumber']) ;
//		    if ((int)$row['MaterialCountryId'] <= 0) return sprintf ('line %d, article %s: no Material Country', (int)$row['No'], $row['ArticleNumber']) ;
		    if ((int)$row['KnitCountryId'] <= 0) return sprintf ('line %d, article %s: no Knit Country', (int)$row['No'], $row['ArticleNumber']) ;
		    if ((int)$row['WorkCountryId'] <= 0) return sprintf ('line %d, article %s: no Work Country', (int)$row['No'], $row['ArticleNumber']) ;

			}
		    $n++ ;    
		}
		dbQueryFree ($result) ;

		if ($n == 0) return 'the Order can not be set Ready when it has no OrderLines' ;

		// generate PickOrder?
		if($_POST['HandlerCompanyId']>0) {
			$query = sprintf ("
				SELECT oq.id as OrderQuantityId, ol.Description as ArticleDescription, vc.id as VariantCodeId, 
					vc.VariantCode as VariantCode, o.companyid as CompanyId, ol.deliverydate as DeliveryDate,
					ol.articleid,  ol.articlecolorid, c.description as color, oq.articlesizeid, az.name as size, oq.quantity as quantity
				FROM (orderline ol, orderquantity oq, articlecolor ac, color c, articlesize az)
				LEFT JOIN Article ON Article.Id=OrderLine.ArticleId and article.Active=1
				LEFT JOIN VariantCode vc ON ol.articleid=vc.articleid and ol.articlecolorid=vc.articlecolorid and oq.articlesizeid=vc.articlesizeid and vc.active=1
				WHERE ol.orderid=%d
					and oq.orderlineid=ol.id and ol.articlecolorid=ac.id and ac.colorid=c.id and oq.articlesizeid=az.id
					and ol.active=1 and oq.active=1 and ac.active=1 and c.active=1 and az.active=1",
				 $Id) ;
			$res = dbQuery ($query) ;	
			while ($orderrow = dbFetch ($res)) {
				$rows[$orderrow['OrderQuantityId']] = $orderrow ;
				if ($orderrow['VariantCodeId'] > 0) continue ;
				return sprintf ('Can't generate pick Order - No variant defined for article %s, color %s, size %s', 
							$orderrow['ArticleDescription'], $orderrow['color'], $orderrow['size']) ;
			}
			dbQueryFree ($res) ;
	
			// Create new pickorder
			$PickOrder = array (	
				'Type'			=>  "SalesOrder",			// Pick order from internal customer
				'Reference'			=>  $Record['Id'] ,		// Order number 
				'ReferenceId'		=>  $Record['Id'] ,		// Order number 
				'FromId'			=>  0,				// Set Picking stock?
				'CompanyId'			=>  $Record['CompanyId'] ,	// Shop Code	
				'OwnerCompanyId'		=>  $Record['ToCompanyId'],		
				'HandlerCompanyId'	=>  $_POST['HandlerCompanyId'],// Novotex
				'Packed'			=>  0,
				'DeliveryDate'		=>  $Record['DeliveryDate'] 	// Expected pick and pack complete
			) ;
			$PickOrderId=tableWrite ('PickOrder', $PickOrder, 0) ;

			foreach ($rows as $row) { 
				$PickOrderLine = array (	
					'VariantCodeId'		=>  $row['VariantCodeId'] ,			//
					'VariantCode'		=>  $row['VariantCode'] ,			// EAN code 
					'OrderedQuantity'		=>  $row['quantity'] ,			//			
					'PickedQuantity'		=>  0,					//
					'PackedQuantity'		=>  0,					//
					'PickOrderId'		=>	$PickOrderId,
					'VariantDescription'	=>	$row['ArticleDescription'] ,
					'VariantColor'		=>	$row['color'] ,
					'VariantSize'		=>	$row['size'] 
				) ;
				tableWrite ('PickOrderLine', $PickOrderLine, 0) ;
			}
 		} 

		// Get and save exchangerate
		$fields['ExchangeRate']['value'] = tableGetField ('Currency', 'Rate', (int)$Record['CurrencyId']) ;
		
		// Set tracking information
		$fields['ReadyUserId']['value'] = $User['Id'] ;
		$fields['ReadyDate']['value'] = dbDateEncode(time()) ;
		return true ;

	    case 'Done':
		// Ignore if not setting flag
		if (!$changed or !$value) return false ;

		// Check for Allocation
		if (!$Record['Ready']) return 'the Order can not be Done before it has been set Ready' ;	

		// Check that OrderDone condition exists
		if ((int)$Record['OrderDoneId'] <= 0) return 'Done condition has not been set' ;

		// Set tracking information
		$fields['DoneUserId']['value'] = $User['Id'] ;
		$fields['DoneDate']['value'] = dbDateEncode(time()) ;
		
		// Get orderlines
	    $query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0', $Record['Id']) ;
	    $res = dbQuery ($query) ;

		// Set Orderlines done
		$OrderLine = array (	
			'Done'			=>  1,
			'DoneUserId'	=>  $User['Id'],
			'DoneDate'		=>  dbDateEncode(time())
	    ) ;
	    while ($row = dbFetch ($res)) {
			tableWrite ('OrderLine', $OrderLine, $row['id']) ;
		}		
		return true ;
	}

	if (!$changed) return false ;


// Ken 2006.04.27	if ($Record['Ready']) return sprintf ('the Order can not be modified when Ready, field %s', $fieldname) ;
	return true ;	
    }
    
    function createorderlines ($data, $startrow, $endrow, $LineNo) {
	global $User, $Navigation, $Record, $Id, $fields ;
		
		$OrderLineNo=(int)$LineNo;
		
		for ($i = $startrow; $i <= $endrow; $i++) {
			unset ($SizeQty) ;
			$OrderQty=0 ;
				// read ordered qty's
			for ($j = 11; $j <= 17; $j++) {
				if ((int)$data->sheets[0]['cells'][$i][$j] > 0) {
					$SizeQty[($j-10)] = (int)$data->sheets[0]['cells'][$i][$j];
					$OrderQty += (int)$data->sheets[0]['cells'][$i][$j] ;
				}
			}
			if ($OrderQty==0) continue ; // Only create orderline if something ordered!
			
			$OrderLineNo++;
			// Find article, color and price info
			unset($OrderLineNew) ;
			$OrderLineNew = array (
				'No' => $OrderLineNo,
				'Description' => '',
				'ArticleId' => 0,
				'ArticleColorId' => 0,
				'OrderId' => (int)$Record['Id'],
				'PriceCost' => 0,
				'PriceSale' => 0,
				'Discount' => (int)$_POST['Discount'],
				'DeliveryDate' => $_POST['DeliveryDate'],
				'Quantity' => $OrderQty,
				'Surplus'	=> 0,
				'CustArtRef' => '',
				'InvoiceFooter' => ''
			) ;
			switch ((int)$Record['CurrencyId']) {
				case 1: // DKK
					$OrderLineNew['PriceSale']=(float)str_replace (',', '.', $data->sheets[0]['cells'][$i][6]) ; 
					break ;
				case 2: // EUR
					$OrderLineNew['PriceSale']=(float)str_replace (',', '.', $data->sheets[0]['cells'][$i][7]) ; 
					break ;
				case 10: // USD
					$OrderLineNew['PriceSale']=(float)str_replace (',', '.', $data->sheets[0]['cells'][$i][9]) ; 
					break ;
				default:
					$OrderLineNew['PriceSale']=(float)0 ; 
					break ;
			}

			$whereclausemodelref = sprintf ("VariantModelRef='%s' and Active=1", $data->sheets[0]['cells'][$i][4]) ;
			$OrderLineNew['ArticleId']=(int)tableGetFieldWhere ('VariantCode', 'ArticleId', $whereclausemodelref ) ;
			if ($OrderLineNew['ArticleId']==0) {
				$OrderLineNew['Description']=sprintf('Article not found in Excel line %d orderline %d', $i, $OrderLineNo ) ;
				$OrderLineNew['No']=0 ;
			}  else {
				$whereclause = sprintf ("Id=%d and Active=1", $OrderLineNew['ArticleId']) ;
				$OrderLineNew['Description']=tableGetFieldWhere ('Article', 'Description', $whereclause ) ;

				$OrderLineNew['ArticleColorId']=(int)tableGetFieldWhere ('VariantCode', 'ArticleColorId', $whereclausemodelref ) ;
				
				if (((int)tableGetFieldWhere ('Article', 'VariantColor', $whereclause )==1) and $OrderLineNew['ArticleColorId']==0) {
					$OrderLineNew['Description']=$OrderLineNew['Description'] . sprintf('ArticleColor not found in Excel line %d, orderline %d', $i, $OrderLineNo) ;
					$OrderLineNew['No']=0 ;
				}
			}
			$OrderLineId = tableWrite ('OrderLine', $OrderLineNew) ;

//return sprintf('writing ordeline %d, %d, %s',$OrderLineNew['ArticleId'], $OrderLineNew['ArticleColorId'],$data->sheets[0]['cells'][$i][4]) ;
			// Get and save size quantities
			$query = sprintf ('SELECT * FROM articlesize WHERE ArticleId=%d AND Active=1 ORDER BY DisplayOrder', (int)$OrderLineNew['ArticleId']) ;
			$result = dbQuery ($query) ;
			$displayno = 0 ;
			while ($row = dbFetch ($result)) {
			    if ((int)$SizeQty[(int)$row['DisplayOrder']] > 0) {
				    // Create new record
					$OrderQuantity = array (
						'OrderLineId'		=> (int)$OrderLineId,
						'ArticleSizeId'		=> (int)$row['Id'],
						'Quantity'		=> (int)$SizeQty[(int)$row['DisplayOrder']]
					) ;
					$OrderQuantityId = tableWrite ('OrderQuantity', $OrderQuantity) ;
				}
			}
		    dbQueryFree ($result) ;

/*			for ($j = 1; $j <= $data->sheets[0]['numCols']; $j++) {
				$readdata= $readdata.'<br>'.$j.': '.$data->sheets[0]['cells'][$i][$j];
			}
*/
		}
		return $OrderLineNo; 
    }
    switch ($Navigation['Parameters']) {
	case 'new' :
	    unset ($fields['Ready']) ;
	    unset ($fields['Done']) ;
	    $fields['CompanyId']['value'] = (int)$Record['Id'] ;
	    $Id = -1 ;
	    break ;

	default :
	    if ($Record['Done']) return 'the Order can not be modified when Done' ;
	    break ;
    }
    
	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 
	
     
    $res = saveFields ('Order', $Id, $fields, true) ;
    if ($res) return $res ;

	 if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);
		
		/*	Excel read help!
			 $data->sheets[0]['numRows'] - count rows
			 $data->sheets[0]['numCols'] - count columns
			 $data->sheets[0]['cells'][$i][$j] - data from $i-row $j-column

			 $data->sheets[0]['cellsInfo'][$i][$j] - extended info about cell
			    
			$data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
				if 'type' == "unknown" - use 'raw' value, because  cell contain value with format '0.00';
			$data->sheets[0]['cellsInfo'][$i][$j]['raw'] = value if cell without format 
			$data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
			$data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 
		*/

		/* Drappa SS09 order format
			Colomns:
				Reference	4			(Drappa styleno refers to commalist in case.customerreference)
				Price		6
				Sizes		11 to 17
				
			Rows:
				Girls		18 to 102
		*/
		$LiNo=createorderlines ($data, 18, 102, 0) ;
		$LiNo=createorderlines ($data, 109, 124,$LiNo ) ;
		$LiNo=createorderlines ($data, 151, 199,$LiNo) ;
		$LiNo=createorderlines ($data, 207, 226,$LiNo) ;
		$LiNo=createorderlines ($data, 245, 299,$LiNo) ;
		$LiNo=createorderlines ($data, 308, 320,$LiNo) ;
		$LiNo=createorderlines ($data, 330, 400,$LiNo) ;
		$LiNo=createorderlines ($data, 407, 422,$LiNo) ;
		$LiNo=createorderlines ($data, 431, 442,$LiNo) ;
		$LiNo=createorderlines ($data, 452, 453,$LiNo) ;
		$LiNo=createorderlines ($data, 457, 458,$LiNo ) ;
	 } 


    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View
	    return navigationCommandMark ('orderview', (int)$Record['Id']) ;
    }

//		return $readdata ;
    return 0 ;    
?>
