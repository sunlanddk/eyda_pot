<?php 
require_once('lib/table.inc');

$shipping = array(
	'AltCompanyName'	=> $_POST['AltCompanyName'],
	'Address1'			=> $_POST['Address1'],
	'Address2'			=> $_POST['Address2'],
	'ZIP'				=> $_POST['ZIP'],
	'City'				=> $_POST['City'],
	'CountryId'			=> $_POST['CountryId'],
	'Packageshop'		=> $_POST['Packageshop'],
);

tableWrite('orderadresses', $shipping, (int)tableGetFieldWhere('orderadresses', 'Id', sprintf("Active=1 AND OrderId=%d AND Type='shipping'", $Id)));

$billing = array(
	'AltCompanyName'	=> $_POST['billing_AltCompanyName'],
	'Address1'			=> $_POST['billing_Address1'],
	'Address2'			=> $_POST['billing_Address2'],
	'ZIP'				=> $_POST['billing_ZIP'],
	'City'				=> $_POST['billing_City'],
	'CountryId'			=> $_POST['billing_CountryId']
);

tableWrite('orderadresses', $billing, (int)tableGetFieldWhere('orderadresses', 'Id', sprintf("Active=1 AND OrderId=%d AND Type='invoice'", $Id)));



return 0;
?>