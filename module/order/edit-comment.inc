<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Comment', formtextArea ('Comment', $Record['Comment'], 'width:100%;height:50px;')) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
