<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;
    require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc';
    
    if ($Record['Ready']) return 'Delivery dates can not be set when Ready' ;	
	// Get orderlines
    $query = sprintf('SELECT id from OrderLine where OrderId=%d and Done=0 and Active=1', $Record['Id']) ;
    $res = dbQuery ($query) ;

	// Set Orderlines done
	$OrderLine = array (	
		'DeliveryDate' => $_POST['DeliveryDate']
    ) ;
    while ($row = dbFetch ($res)) {
		tableWrite ('OrderLine', $OrderLine, $row['id']) ;
	}		
	return 0 ;
?>
