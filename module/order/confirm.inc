<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;
    
    function flag (&$Record, $field, $name='') {
		if ($name=='') $name=$field ;
		if ($Record[$field]) {
			itemField ($name, sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId']))) ;
		} else {
			itemFieldRaw ($name, formCheckbox ($field, $Record[$field])) ;
		}
    }


	
	// in cmd: nl2br($_POST['Body']) . '<br>'
	//'Please confirm the attached order confirmation from Eyda to your sales agent.
	if ($Record['OrderTypeId']==10) {
		$BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="weborderconf"') ;
	} else {
        if ($Record['Ready']) {
    		$BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="b2borderconf"') ;
        } else {
            if (isset($_GET['ready'])) { // Revert value if set in url
                 if ($_GET['ready']=='1') { 
                    $_ready = 0 ; 
                    $BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="b2borderconfdraft"') ;
                } else {
                    $_ready = 1 ;
                    $BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="b2borderconf"') ;
                }
            } else {
                $_ready = 1 ; 
                $BodyTxt =  tableGetFieldWhere('maillayout','Body_default','Mark="b2borderconf"') ;
            }
        }
	}

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Order', (int)$Record['Id']) ;
    itemField ('Customer', $Record['CompanyName']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemEnd () ;
  
    formStart () ;
    itemStart () ;
    itemHeader() ;
	
	if ($Record['Ready'])
			itemField ('Confirmed', sprintf ('%s, %s', date('Y-m-d H:i:s', dbDateDecode($Record['ReadyDate'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record['ReadyUserId']))) ;
	itemFieldRaw ($Record['Ready']?'':'Confirm', formCheckbox ('Ready', $Record['Ready']?1:$_ready, '', $Record['Ready']?'Hidden':sprintf('onchange="appLoad(%d,%d,\'ready=\'+%d);"', (int)$Navigation['Id'], $Id,$_ready))) ;
    itemSpace () ;
    itemFieldRaw ('Email', formText ('Mail', $Record['Mail'], 100, 'width:100%;')) ;
    itemSpace () ;
    itemFieldRaw ('BodyText', formtextArea ('BodyTxt', $BodyTxt, 'width:100%;height:200px;')) ;

    itemInfo ($Record) ;
    itemEnd () ;
    
    formEnd () ;
   
    return 0 ;
?>
