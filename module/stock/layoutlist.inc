<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;

	itemStart () ;
	itemSpace () ;
	itemFieldIcon ('Stock', $Record['StockNo'] . ' - ' . $Record['Name'], 'stock.gif', 'stockview', $Record['Id']) ;
	itemField ('Description', $Record["Description"]) ;
	itemSpace () ;
	itemEnd () ;

	if ($Navigation['Parameters']=='occupation') {
		$_showContent = 1 ;
	} 
    listFilterBar () ;
	listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Position', 100) ;
    listHead ('Type', 60) ;
    listHead ('Capacity', 60) ;
	if ($_showContent) {
		listHead ('PickPosition on Position', 150) ;
		listHead ('Variant', 200) ;
	    listHead ('# on Position', 80) ;
	    listHead ('Content') ;
	} else {
		listHeadIcon () ;
		listHead () ;
	}
    while ($row = dbFetch($Result)) {
		listRow () ;
		listFieldIcon ('edit.gif', 'layoutedit', $row['Id']) ;
		listField ($row['Position']) ;
		listField ($row['Type']) ;
		listField ((int)$row['Max']) ;
		if ($_showContent) {
			switch ($row['Type']) {
				case 'sscc':
					$query = sprintf("select count(id) as NoContainers, group_concat(cast(sscc as char)) as SSCCs from container where active=1 and stockid=%d and position='%s'", $Id, $row['Position']) ;
					$result = dbQuery($query) ;
					$_content = dbFetch($result) ;
					listField ($_content['NoContainers']) ;
					$_many = $_content['NoContainers']>3?' ...':'' ;
					listField (substr($_content['SSCCs'],0,57).$_many) ;
					break ; 
				case 'gtin':
				$query = sprintf("
					SELECT count(v.Id) as Total, group_concat(v.VariantCode) as Codes
					FROM (variantcode v, container c, collectionmember cm)
					WHERE c.Active=1 AND c.StockId=%d AND c.Position='%s' AND v.PickContainerId=c.Id AND v.Active=1 AND cm.Active=1 AND cm.ArticleId=v.ArticleId AND cm.ArticleColorId=v.ArticleColorId
					GROUP BY c.Id
					", $Id, $row['Position']) ;
//		die($query) ;
					$result = dbQuery($query) ;
					$_pcikOnContentDb = dbFetch($result) ;
					$_pcikOn = 0;
					$_pcikOnContent = '';
					if(isset($_pcikOnContentDb['Total'])){
						$_pcikOn = (int)$_pcikOnContentDb['Total'];
						$_pcikOnContent = (string)$_pcikOnContentDb['Codes'];
					}
					listField ((int)$_pcikOn) ;
					listField ($_pcikOnContent) ;

					$query = sprintf("select *, group_concat(cast(variantcode as char),': ',cast(Qty as unsigned), '  ') as VariantCodes, sum(Qty) as Qty, count(id) as NoVars
					from (
						select vc.id, vc.variantcode, sum(i.quantity) as Qty, c.position
						from (container c, item i, variantcode vc)
						where c.active=1 and i.active=1 and i.containerid=c.id and c.stockid=%d and c.position='%s' and vc.articleid=i.articleid and vc.articlecolorid=i.articlecolorid and vc.articlesizeid=i.articlesizeid AND vc.active=1
						group by vc.id) tab group by tab.Position", $Id, $row['Position']) ;
//		die($query) ;
					$result = dbQuery($query) ;
					$_noVariants = dbNumRows($result) ;
					$_content = dbFetch($result) ;
					// listField ($_noVariants) ;
					$_noVariants = $_content['NoVars']>1 ? ($_content['NoVars'] . ' Gtin ') : '' ;
					listField ((int)$_content['Qty']) ;
					$_many = strlen($_content['VariantCodes'])>67 ? ' ...' : '' ;
					listField ($_noVariants . substr($_content['VariantCodes'],0,55).$_many) ;
					break ; 
				default:
					listField('') ;
					break ;
			}
		} else {
			listFieldIcon ('delete.gif', 'layoutdelete', $row['Id']) ;
			listField() ;
		}
	}
    if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Positions configured', 'colspan=2') ;
    }
    listEnd () ;

    dbQueryFree ($Result) ;

    return 0 ;
?>
