<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Stock', $Record['StockNumber'] . ' - ' . $Record['StockName']) ;
	
    if ($Navigation['Parameters'] != 'new') {
		$gryf=1 ; ;
    }
    itemSpace () ;
    itemEnd () ;
    
    switch ($Navigation['Parameters']) {
		case 'new' :
			unset ($Record) ;
			$Record['Max'] = 1 ;
			$Record['Type'] = 'gtin' ;
			break ;
	}
 
    formStart () ;

    itemStart () ;    
    itemHeader() ;
    if ($Navigation['Parameters'] == 'new') {
		itemFieldRaw ('Position', formText ('Position', $Record['Position'], 20)) ;
    } else {
		itemField ('Position', $Record['Position']) ;
		formHidden ('Position', $Record['Position']) ;
	}
//    itemFieldRaw ('Type',  formDBSelect ('Type', $Record['Type'], "select 'sscc' as Id, 'sscc' as Value union select 'gtin' as Id, 'gtin' as Value", 'width:150px; ')) ; 
    itemFieldRaw ('Type', formText ('Type', $Record['Type'], 20)) ;
    itemFieldRaw ('Capacity', formText ('Max', $Record['Max'], 20)) ;
    itemSpace () ;
    itemEnd () ;   

    formEnd () ;

    return 0 ;
?>
