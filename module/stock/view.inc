<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/item.inc' ;

    require_once 'module/container/include.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

    itemStart () ;
    itemHeader () ;
    itemField ('Name', $Record['Name']) ;
    itemField ('Stock No', $Record['StockNo']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    itemFieldIcon ('Location', $Record['CompanyName'], 'company.gif', 'companyview', $Record['CompanyId']) ;
    if ($Record['CompanyAddress1']) itemField ('', $Record['CompanyAddress1']) ;
    if ($Record['CompanyAddress2']) itemField ('', $Record['CompanyAddress2']) ;
    itemField ('', $Record['CompanyCity']) ;
    itemField ('', $Record['CompanyCountry']) ;
    itemInfo ($Record) ;
    
    if (!empty($PickLayout[$Record['Name']])) {
        itemSpace () ;
	    itemSpace () ;
		itemFieldIcon ('Complete layout', $Record['Name'], 'report.gif', 'layoutlist', $Record['Id']) ;
		itemSpace () ;
		itemSpace () ;
    } else {
		itemSpace () ;
		itemSpace () ;
    }
    itemEnd () ;
    if (!empty($PickLayout[$Record['Name']])) {
		// Get current stored status
		$query = sprintf ("select Street, count(Pos) as NoPos, sum(Rooms) as Rooms 
							from (select upper(substring(c.`position`,1,1)) as Street, 
									upper(substring(c.`position`,1,5)) as Pos, 
									count(c.id) as Rooms 
									from container c where c.stockid=%d and active=1 group by Pos) used
					group by Street", $Record['Id']) ;
		$res = dbQuery($query) ;
		while ($row = dbFetch($res)) {
		 		$UsedPos[$row['Street']] = $row ;
		}
		dbQueryFree($res) ;

		// List layout
		if (empty($PickLayout[$Record['Name']])) return 'No Pick layout defined for this stock' ;
		listStart () ;
		listRow () ;
		listHead ('', 40) ;
		listHead ('Street', 50) ;
		listHead ('Free positions:', 90) ;
		listHead ('Positions:', 60) ;
		listHead ('Stored SKUs:', 75) ;
		listHead ('Number of Houses:', 110) ;
		listHead ('Floors on Even side:', 110) ;
		listHead ('Floors on odd side:', 110) ;

		foreach ($PickLayout[$Record['Name']] as $StreetName => $Street) {
			listRow () ;
			listFieldIcon ('report.gif', 'streetlist', $Record['Id'], 'StreetName='.$StreetName ) ;
			listField ($StreetName) ; 
			$NoPositions = $Street['EvenNoFloors']*$Street['NoStreetNo']/2+$Street['OddNoFloors']*$Street['NoStreetNo']/2 ;
			listField ($NoPositions-$UsedPos[$StreetName]['NoPos'], 'align=right') ; 
			listField ($NoPositions, 'align=right') ; 
			listField ($UsedPos[$StreetName]['Rooms'], 'align=right') ; 
			listField ($Street['NoStreetNo'], 'align=right') ; 
			listField ($Street['EvenNoFloors'], 'align=right') ; 
			listField ($Street['OddNoFloors'], 'align=right') ; 
			listField ('') ; 
			listField ('') ;
			$TotPos += $NoPositions ;
			$TotFree += $NoPositions-$UsedPos[$StreetName]['NoPos'] ;
			$TotSkus += $UsedPos[$StreetName]['Rooms'] ;
		}
		listRow () ;
		listField ('Total:') ;
		listField ('') ; 
		listField ($TotFree, 'align=right') ;
		listField ($TotPos, 'align=right') ; 
		listField ($TotSkus, 'align=right') ; 
		listField ('') ;

		listEnd() ;
	} else {
		$_query = sprintf('select upper(substring(`Position`,1,1)) as Street, 
									count(Id) as NoPositions, 
									sum(if(occupied=1,0,1)) as FreePosistions
							from stocklayout 
							where stockid=%d
							group by upper(substring(`Position`,1,1))
							order by upper(substring(`Position`,1,1))', $Id);
		$_result = dbQuery($_query) ;

		if (dbNumRows($_result)>0) {
			listStart () ;
			listRow () ;
			listHead ('', 40) ;
			listHead ('Street', 50) ;
			listHead ('Positions:', 55, 'align=right') ;
			listHead ('Free positions:', 83, 'align=right') ;
//			listHead ('Stored SKUs:', 75, 'align=right') ;

			while ($_row = dbFetch($_result)) {
				listRow () ;
				listField ('') ; 
				listField ($_row['Street']) ; 
				listField ($_row['NoPositions'], 'align=right') ; 
				listField ($_row['FreePosistions'], 'align=right') ; 
				listField ('') ; 
				listField ('') ; 
			}
			dbQueryFree($_result) ;

			listEnd() ;
		}
	}

    return 0 ;
?>
