<?php

    $companylimit = ''; //sprintf(' And stock.FromCompanyId = %d', $User['CompanyId']);
    switch ($Navigation['Function']) {
		case 'list' :
			// Query list
			$query = sprintf('SELECT Stock.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName 
						FROM (Stock) LEFT JOIN Company ON Company.Id=Stock.CompanyId 
						WHERE Stock.Type="fixed" AND Stock.Id NOT IN(6971,5718) AND Stock.Active=1 %s ORDER BY Stock.Name', $companylimit ) ;
			$Result = dbQuery ($query) ;
			return 0 ;
			
		case 'listpicks' :
			// Query list
			$query = 'SELECT Stock.*, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName 
						FROM (Stock, Pickstocks) LEFT JOIN Company ON Company.Id=Stock.CompanyId 
						WHERE Stock.Type="fixed" AND Stock.Active=1 AND Stock.Id=Pickstocks.StockId ORDER BY Stock.Name' ;
			$Result = dbQuery ($query) ;
			return 0 ;
			
		case 'layoutedit' :
		case 'layoutedit-cmd' :
			switch ($Navigation['Parameters']) {
				case 'new' :
					$query = sprintf ("SELECT Stock.Name as StockName, Stock.StockNo as StockNumber 
										FROM (Stock) WHERE Stock.Id=%d", $Id) ;
					break ;
				default:
					$query = sprintf ("SELECT StockLayout.*, Stock.Name as StockName, Stock.StockNo as StockNumber 
										FROM (StockLayout, Stock) WHERE StockLayout.Id=%d And Stock.Id=StockLayout.StockId", $Id) ;
					break ;
			}
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;
			break ;
			
			
		case 'layoutlist' :
			// Query Stock
			$query = sprintf('SELECT Stock.* 
							FROM Stock 
							WHERE Stock.Id=%d AND Stock.Active=1', $Id) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;

			// Query list
			$fields = array (
				NULL, // index 0 reserved
				array ('name' => 'Position',		'db' => 'StockLayout.Position'),
				array ('name' => 'Type',		'db' => 'StockLayout.Type')
//				array ('name' => 'Capacity',		'db' => 'StockLayout.Max')
			) ;
			require_once 'lib/list.inc' ;
			$queryFields = 'StockLayout.*';
			$queryTables = 'StockLayout' ;
			$queryClause = sprintf('StockId=%d', $Id);
			$Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, '', '') ;
			if (is_string($Result)) return $Result ;
			return 0 ;
			
		case 'view' :
			// Query Stock
			$query = sprintf('SELECT Stock.*, Company.Id as CompanyId, CONCAT(Company.Name," (",Company.Number,")") AS CompanyName, Company.Address1 AS CompanyAddress1, Company.Address2 AS CompanyAddress2, CONCAT(Company.ZIP," ",Company.City) AS CompanyCity, CONCAT(Country.Name," - ",Country.Description) AS CompanyCountry FROM Stock LEFT JOIN Company ON Company.Id=Stock.CompanyId LEFT JOIN Country ON Country.Id=Company.CountryId WHERE Stock.Id=%d AND Stock.Active=1 %s', $Id, $companylimit ) ;
			$res = dbQuery ($query) ;
			$Record = dbFetch ($res) ;
			dbQueryFree ($res) ;

			// Validate type
			if ($Record['Type'] != 'fixed') return sprintf ('%s(%d) invalid type, value "%s"', __FILE__, __LINE__, $Record['Type']) ;

			return 0 ;
    }

    return 0 ;
?>
