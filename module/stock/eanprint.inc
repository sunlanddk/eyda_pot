<script type='text/javascript'>
  function printLabels (nav, id) {
        var key = event.keyCode || event.charCode;
        if (key != 13)  return ; // CR is 13, tab is 9
        appSubmit(2802,document.getElementsByName ('id')[0].value,null); // execute  command.

}

</script>
<?php
    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    // Form
	formStart() ;
    itemStart () ;
    itemSpace () ;
    itemFieldRaw ('EAN', formText('ean', '')  ) ;
    itemSpace () ;
    itemFieldRaw ('Quantity', formText ('Qty', 2, 10,'', sprintf('onkeypress="printLabels(%d,%d);"',(int)$Navigation['Id'], $Id))) ;
    itemSpace () ;
      $query = sprintf('SELECT 0 as Id, "-Screen-" as Value Union SELECT Workstation.Id, Workstation.Name AS Value FROM Workstation WHERE active=1 ORDER BY Value');
    itemFieldRaw ('Workstation', formDBSelect ('WorkstationId', (int)$Login['WorkstationId'], $query, 'width:250px;')) ;  
    itemEnd () ;
	formEnd() ;

    return 0 ;
?>
