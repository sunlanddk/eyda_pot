<?php

    require_once 'lib/table.inc' ;

    // Ensure no Containers in Position
    $query = sprintf ("SELECT StockLayout.Id FROM (StockLayout, Container) WHERE StockLayout.Id=%d And Container.StockId=StockLayout.StockId AND Container.Position=StockLayout.Position AND Container.Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "can not be deleted when containers on position" ;
    
	$query = sprintf ("DELETE FROM StockLayout WHERE Id=%d", $Id) ;
	dbQuery ($query) ;

    return 0 ;
?>
