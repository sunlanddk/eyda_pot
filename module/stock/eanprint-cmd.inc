<?php

    define('FPDF_FONTPATH','lib/font/');
    require_once 'lib/http.inc' ;
    require_once 'lib/fpdf.inc' ;
    require_once 'lib/barcode.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/file.inc' ;
    require_once 'lib/db.inc';


    // if ($ws == 'none') {
    //     // don't print any if workstation not set
    //     $_folder = 'PDF' ;
    //     $_subfolder = '/NOPOS' ;
    // } else {
    //     $_folder = 'PDF/CU1' ;
    //     $_subfolder = $ws . '/DEL' ;
    // }

    $variantDetails = getVariantDetails($_POST['ean']);
    if (!$variantDetails){
    	return 'not a valid EAN';
    }	

    $qty = (int)$_POST['Qty'] ;
    $pdf = new PDF('L','mm',[40,50]);

//        $pdf->AddPage() ;
    $count = 1;
    while ($qty >= $count) {
        $pdf->SetMargins(2,2);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage();

        $pdf->SetFont('Arial','B',8);

        $pdf->SetXY(2,6) ;
        $pdf->Cell(8,3, 'STYLE:' ,0,0,'L', 0, 1);
        $pdf->SetXY(2,14) ;
        $pdf->Cell(8,3, 'ARTICLE:' ,0,0,'L', 0, 1);
        $pdf->SetXY(2,18) ;
        $pdf->Cell(8,3, 'COLOR:' ,0,0,'L', 0, 1);
        $pdf->SetXY(2,22) ;
        $pdf->Cell(8,3, 'SIZE:',0,0,'L', 0, 1);

        $pdf->SetFont('Arial','',8);

        $pdf->SetXY(18,6) ;
        if ( strlen($variantDetails['style']) > 16 ) {
        	$pdf->SetXY(18,5) ;
        	$pdf->MultiCell(29,3, $variantDetails['style'], 0,'L');
        } else {
        	$pdf->SetXY(18,6) ;
        	$pdf->Cell(8,3, $variantDetails['style'] ,0,0,'L', 0, 1);
        }
        
        $pdf->SetXY(18,14) ;
        $pdf->Cell(8,3, $variantDetails['article'] ,0,0,'L', 0, 1);
        $pdf->SetXY(18,18) ;
        $pdf->Cell(8,3, $variantDetails['color'] ,0,0,'L', 0, 1);
        $pdf->SetXY(18,22) ;
        $pdf->Cell(8,3, $variantDetails['size'],0,0,'L', 0, 1);

        $pdf->EAN13(8,29,$variantDetails['ean'],6, .35, true) ;

        $pdf->Ln() ;
        $count ++;  
    }

    if ( (int)$_POST['WorkstationId'] > 0) {

    	$workstationQuery = sprintf("UPDATE `login` SET `WorkstationId`=%d WHERE `Id`=%d" , (int)$_POST['WorkstationId'], (int)$Login['Id']);
    	$workstationResult = dbQuery($workstationQuery);
		dbQueryFree($workstationResult);

// 
    	$file = fileName ('label', $_POST['ean'], true) ;
    	$pdfdoc = $pdf->Output('', 'S') ;
    	$resource = fopen ($file, 'w') ;
		fwrite ($resource, $pdfdoc) ;
		fclose ($resource) ;
		chmod ($file, 0640) ;

		$fileinfo['localePath'] = $file;

		if ($Config['Instance']=='Production') {
			$_folder = 'NetSolution/CU1' ;
		} else {
			$_folder = 'Test/Alex';
		}
		
		$_ws = TableGetField('Workstation','Name', (int)$_POST['WorkstationId']) ;

		$fileinfo['ftpPath'] = $_folder.'/'. $_ws .'_Return' . substr($_ws, 2, 2) . '_' . time() .'_1.pdf';
		$logininfo['host'] = 'vion.dk';
		$logininfo['usr'] = 'vion.dk04';
		$logininfo['pwd'] = 'Nutrinic1' ;
		ftpUpload($logininfo, $fileinfo, true) ;

		return navigationCommandMark ('eanprint');
    } else {
    	// Generate PDF document
        $pdfdoc = $pdf->Output('', 'S') ;

        // Download
        if (headers_sent()) return 'stop' ;
        httpNoCache ('pdf') ;
        httpContent ('application/pdf', sprintf('label_%d.pdf', (int)$_POST['ean']), strlen($pdfdoc)) ;
        print ($pdfdoc) ; 
        return 0 ;
    }

//     If ((int)$_POST['PrinterId']>1) {
//         // Generate PDF document
//         $fileinfo['localePath'] = $Config['fileStore'].'\pickorder/etiket'.'_'.$Id.'.pdf';
//         $pdf->Output($fileinfo['localePath'], 'F');

//         // Auto print
//         $_folder = 'NetSolution/CU2' ;
// //        $_folder = 'PDF/CU2' ;
//         $_printerName = tableGetField('Printers','Name',(int)$_POST['PrinterId']) ;
//         $fileinfo['ftpPath'] = $_folder.'/'. 'Sys_' . $_printerName . '_etiket' . $Id . '.pdf';
//         $logininfo['host'] = 'vion.dk';
//         $logininfo['usr'] = 'vion.dk04';
//         $logininfo['pwd'] = 'Nutrinic1' ;

//         ftpUpload($logininfo, $fileinfo);
//         return 0 ; //navigationCommandMark ('outboundpreadvice', 0) ; 
//    } else {
//         // Generate PDF document
//         $pdfdoc = $pdf->Output('', 'S') ;

//         // Download
//         if (headers_sent()) return 'stop' ;
//         httpNoCache ('pdf') ;
//         httpContent ('application/pdf', sprintf('label.pdf', (int)$Record['VariantCode']), strlen($pdfdoc)) ;
//         print ($pdfdoc) ; 
//         return 0 ;
//     }



//     $pdf->Output($Config['fileStore'].'\eanlabels/labels'.'_'.$Record['VariantCode'].'.pdf', 'F');

/*
    $count = 1;
    while ($qty >= $count) {
//            $_folder = 'Test' ;
//            $_subfolder = 'alex';
        $fileinfo['localePath'] = $_SERVER['DEVELSTORE'].$refundId.'_'.$refundLineId.'.pdf';

        $fileinfo['ftpPath'] = $_folder.'/'. $_subfolder .'/' .  $refundId.'_'.$refundLineId .'_'.$count. '.pdf';

        $logininfo['host'] = 'vion.dk';
        $logininfo['usr'] = 'vion.dk04';
        $logininfo['pwd'] = 'Nutrinic1' ;

        $this->ftpUpload($logininfo, $fileinfo);
        $count ++;  
    }

    return $variant['StockNo'].$variant['Position'];
*/

    function getVariantDetails($ean) {
		$query = sprintf("
			SELECT a.Number AS 'Article', a.Description AS 'Style', c.Description AS 'Color', asz.Name AS 'Size'
			FROM `variantcode` vc 
			LEFT JOIN article a ON a.Id = vc.ArticleId AND a.Active = 1
			LEFT JOIN articlecolor ac ON ac.Id = vc.ArticleColorId AND ac.Active = 1
			LEFT JOIN color c ON c.Id = ac.ColorId AND c.Active = 1
			LEFT JOIN articlesize asz ON asz.Id = vc.ArticleSizeId AND asz.ArticleId = vc.ArticleId AND asz.Active = 1
			WHERE vc.`VariantCode` = %s",
			$ean);

		$result = dbQuery($query);
		$row = dbFetch($result);
		dbQueryFree($result);
		if (empty($row)) {
			return false;
		}

		$style_val 		= $row['Style'];
		$article_val 	= $row['Article'];
		$color_val 		= $row['Color'];
		$size_val 		= $row['Size'];

		$output = array(
			'ean' 			=> $ean,
			'style' 		=> mb_strtoupper($style_val),
			'article' 		=> $article_val,
			'color' 		=> mb_strtoupper($color_val),
			'size' 			=> mb_strtoupper($size_val)
		);
		return $output;
	}
	return navigationCommandMark ('eanprint');
    return 0 ;
?>
