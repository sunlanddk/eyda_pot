<?php

    require_once 'lib/save.inc' ;

    $fields = array (
		'Max'			=> array (),
		'Type'			=> array ('check'	=> true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
		global $Id, $fields, $Record, $User, $_stockId ;
		switch ($fieldname) {
			case 'Position':
				$query = sprintf ("SELECT StockLayout.Id FROM (StockLayout) WHERE StockLayout.StockId=%d AND StockLayout.Position='%s' AND StockLayout.Active=1", $_stockId, $value) ;
				$result = dbQuery ($query) ;
				$count = dbNumRows ($result) ;
				dbQueryFree ($result) ;
				if ($count > 0) return "Position allready exist on this Stock" ;
				return true ;
			case 'Type':
				if (!($value=='sscc') and !($value=='gtin')) return 'Illegal type:'  . $value . ' - Please enter gtin or sscc';
				return true ;
			default:
				return true ;
		}
		return false ;
    }

    switch ($Navigation['Parameters']) {
		case 'new' :
			 $_stockId = $Id ;
			$fields['StockId'] = array ('type' => 'set', 'value' => $Id) ;
			$fields['Position']= array ('type' => 'set', 'value' => $_POST['Position'], 'check'=> true) ;
			$Id = -1 ;
			unset ($Record) ;
			break ;

		default :
			break ;
    }
     
	$fields['Type']= array ('type' => 'set', 'value' => $_POST['Type'], 'check'	=> true) ;
    // Save record
    $res = saveFields ('StockLayout', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
		case 'new' :
		default:
			break ;
    }

    return 0 ;
?>
