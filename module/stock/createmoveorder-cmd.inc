<?php
	require_once 'lib/file.inc' ;
	require_once 'lib/table.inc' ;
	require_once 'lib/reader.inc' ;
	
/*
	Adjustment is calculated as diffence between stock Qty and Actual Qty in Excel sheet.
	
	Hence if real Stock Qty is different from Excel only difference is updated.

	This means Stock only has to be blocked from generating excel untill counted 
	(dosnt have to wait untill system is updated).

	Updating stock balance is:
		Adding qty by creating a new item as a clone of an existing and marking it with "StatusId" in batchnumber
		Consume loss qty by splitting item and mark with "StatusId" in batchnumber

	Qty that is not represented on the stock before status (not in the generated Excel Sheet) is created manuel
	with article->new item with batchnumber="StatusId"
	
	Updating position is:
		Container is named the description entered in "New position" colomn.
		
		If "delete" is entered container is set active=0 and pick pos is released (containerid in variantcode table is set to 0)
		
		If more lines is tagged with same position name a running number is appended to description according to display order.
*/

     set_time_limit (300) ;

    // Create ItemOperation referance
    $ItemOperation = array (
		'Description' => sprintf ('Move order list from %s', tableGetField ('Stock','Name', $Id))
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

	switch ($_FILES['Doc']['error']) {
	    case 0: 
		if (!is_uploaded_file ($_FILES['Doc']['tmp_name']))
		    return sprintf(' upload failed, file not uploaded %s !',$_FILES['Doc']['tmp_name'] ) ;
		if ($_FILES['Doc']['size'] != filesize($_FILES['Doc']['tmp_name']))
		    return sprintf('invalid size %d error %d',$_FILES['Doc']['size'], filesize($_FILES['Doc']['tmp_name'])) ;
		if ($_FILES['Doc']['size'] > 700000)
		    return 'file too big, max 700 kB' ;

		// Set flag for file upload	
		$FileUpload = true ;		    
		break ;
		
	    case 4: 		// No file specified
		$FileUpload = false ;
		break ;
		
	    default: return sprintf ('document upload failed, code %d', $_FILES['Doc']['error']) ;
	} 

	if ($FileUpload) {
		// ExcelFile($filename, $encoding);
		$data = new Spreadsheet_Excel_Reader();

		// Set output Encoding.
		$data->setOutputEncoding('CP1251');
		$data->read($_FILES['Doc']['tmp_name']);

		//	Excel read help!
		//	 $data->sheets[0]['numRows'] - count rows
		//	 $data->sheets[0]['numCols'] - count columns
		//	 $data->sheets[0]['cells'][$i][$j] - data from $i-row $j-column
		//	 $data->sheets[0]['cellsInfo'][$i][$j] - extended info about cell
		//	    
		//	$data->sheets[0]['cellsInfo'][$i][$j]['type'] = "date" | "number" | "unknown"
		//	if 'type' == "unknown" - use 'raw' value, because  cell contain value with format '0.00';
		//	$data->sheets[0]['cellsInfo'][$i][$j]['raw'] = value if cell without format 
		//	$data->sheets[0]['cellsInfo'][$i][$j]['colspan'] 
		//	$data->sheets[0]['cellsInfo'][$i][$j]['rowspan'] 

		$Error_txt=0 ;
		for ($k = 0; $k <= 0; $k++) {
			for ($i = 6; $i <= $data->sheets[$k]['numRows']; $i++) {
				// Initializing data
				$ArticleNumber = $data->sheets[0]['cells'][$i][1] ;
				$ArticleColorNumber = $data->sheets[0]['cells'][$i][3] ;
				$ArticleSize = $data->sheets[0]['cells'][$i][5] ;
																												
				$Reservation	= $data->sheets[0]['cells'][$i][11] ;
				if ($Reservation == '') $Reservation = 'None' ;

				$Allocation		= explode('/',$data->sheets[0]['cells'][$i][13]) ;
				$NewStock		= $data->sheets[0]['cells'][$i][15] ;
				$Release = strcasecmp($data->sheets[0]['cells'][$i][16],'release')==0 ? 1 : 0 ;

				$AllocCase		= (int)$Allocation[0];
				$AllocPO		= (int)$Allocation[1];
				
//return 'newstock (' . $NewStock . ') reservation ' . $Reservation . ' release ' . $Release ;					

				$ReservationId = 0 ;
				$MoveIt=0 ;

				// Move request?
				if ($NewStock <> '') {
					if (($Reservation <> 'None') and ($Release == 0)) {
						$Error_txt=$Error_txt . 'Line ' .$i .  ': Moving reserved items is not allowed<br>' ;
						continue ;
					} else {
						$MoveIt=1 ;
					}
				} else {
					// Release request?
					if (($Reservation=='None') or ($Release == 0)) 
						continue ;
				}
				
				if ($Reservation <> 'None') {
					$ReservationId=tableGetFieldWhere ('Company','Id', sprintf("Name='%s' and active=1 and ToCompanyId in (0,2)", $Reservation)) ;
					if ($ReservationId<=0)
						return 'Items in line ' . $i . ' was reserved for "' . $Reservation . '" but not found in company database 2 ' ;
				} else
					$ReservationId=0 ;

								
				// Get items in question.
				if ($ArticleColorNumber == '') {
					$QueryTables = '' ;
					$QueryWhere = '' ;
				} else {
					$QueryTables = ", articlecolor ac, color co" ;
					$QueryWhere = sprintf (" and i.articlecolorid=ac.id and ac.active=1 and ac.colorid=co.id and co.number='%s'", $ArticleColorNumber) ;
				}
				
				if ($ArticleSize== '') {
					$x = 1 ;
				} else {
					$VariantDimension=tableGetFieldWhere ('Article','VariantDimension', sprintf('Number=%s', $ArticleNumber)) ;
					if ($VariantDimension) {
						$QueryWhere = $QueryWhere . sprintf (" and i.dimension='%s'", $ArticleSize) ;
					} else {
						$QueryTables = $QueryTables . ", articlesize az" ;
						$QueryWhere = $QueryWhere . sprintf (" and i.articlesizeid=az.id and az.active=1 and az.name='%s'", $ArticleSize) ;
					}
				}
				
				if ($AllocPO>0) {
					$ProductionId=tableGetFieldWhere ('Production','Id', sprintf('Number=%d and CaseId=%d', (int)$AllocPO, (int)$AllocCase)) ;
					$QueryWhere = $QueryWhere . sprintf (" and i.productionid=%d", (int)$ProductionId) ;
				} else if ($AllocCase>0) {
					$QueryWhere = $QueryWhere . sprintf (" and i.caseid=%d", (int)$AllocCase) ;
				} else if ($ReservationId>0) {
					$QueryWhere = $QueryWhere . sprintf (" and i.reservationid=%d", (int)$ReservationId) ;
				} else
					$QueryWhere = $QueryWhere . " and i.reservationid=0 and i.productionid=0 and i.caseid=0" ;
				
				$query = sprintf ("SELECT i.* FROM (item i, container c, article a %s)
									WHERE i.containerid=c.id and i.active=1 and c.stockid=%d
									and i.articleid=a.id and a.active=1 and a.number='%s' %s",
								  $QueryTables, $Id, $ArticleNumber, $QueryWhere) ;			 
// if ($i==17) return $query . '<br><br>' .
// 'newstock (' . $NewStock . ') reservation ' . $Reservation . ' release ' . $Release . ' reservationid ' . $ReservationId ;
				$res = dbQuery ($query) ;

				// Set new values for items
				While ($Item = dbFetch ($res)) {
//return 'newstock (' . $NewStock . ') reservation ' . $Reservation . ' release ' . $Release . ' reservationid ' . $ReservationId ;					
					unset ($ItemUpd) ;
					if ($Reservation<>'None' and $Release==1) {
						$ItemUpd['ReservationId'] = 0 ;
						$ItemUpd['ReservationUserId']	= $User['Id'] ;
						$ItemUpd['ReservationDate']	= dbDateEncode(time()) ;

						$ItemUpd['ProductionId'] = 0 ;
						$ItemUpd['CaseId'] = 0 ;
					}
					if ($MoveIt) {
						$AltStockId=tableGetFieldWhere ('Stock','Id', sprintf("Name='%s' and type='fixed' and active=1", $NewStock)) ;
						if ($AltStockId<=0)
							return 'Items in line ' . $i . ' was requested to moved to "' . $NewStock . '" which is not found in stock database ' ;
						if ($AltStockId==$Id) {
							$ItemUpd['AltStockId'] = 0 ;
						} else { 			
							$ItemUpd['AltStockId'] = $AltStockId ;
							$ItemUpd['ItemOperationId'] = $ItemOperation['Id'];
						}
					}
					tableWrite ('Item', $ItemUpd, $Item['Id']) ;
				}
				dbQueryFree ($res) ;					

				// Move containers to new stock?				

			} // End >Row loop
		} // End Sheet loop
	} // End File uploaded

	return $Error_txt ;
?>
