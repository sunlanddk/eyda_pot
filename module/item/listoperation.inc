<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;

    // Page Header
    itemStart () ;
    itemSpace () ;
    itemField ('Description', $Record['Description']) ;
    itemInfo ($Record) ;
    itemSpace () ;
    itemEnd () ;

    // List
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Article', 70) ;
    listHead ('Colour', 90) ;
    listHead ('Size', 50) ;
    listHead ('Dim.', 50) ;
    listHead ('Sort', 50) ;
    listHead ('Certificate', 70) ;
//  listHead ('Produced', 80) ;
    listHead ('Allocation', 70) ;
    listHead ('', 8, 'style="border-right:1px solid #cdcabb;"') ;
    listHead ('From Stock', 100) ;
    listHead ('Container', 55, 'align=right') ;
    listHead ('Item', 55, 'align=right') ;
    listHead ('', 8, 'style="border-right:1px solid #cdcabb;"') ;
    listHead ('To Stock', 100) ;
    listHead ('To Position', 55) ;
    listHead ('Container', 55, 'align=right') ;
    listHead ('Item', 55, 'align=right') ;
    listHead ('', 8, 'style="border-right:1px solid #cdcabb;"') ;
    listHead ('Qty', 50, 'align=right') ;
    listHead ('Unit', 50) ;
    listHead () ;

    while ($row = dbFetch ($Result)) {
	listRow () ;
	listFieldIcon ('item.gif', 'view', $row['Id']) ;
	listField ($row['ArticleNumber']) ;			// Article Number
	if ($row['VariantColor']) {
	    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorNumber']) ;	    
	    listFieldRaw ($field) ;
	} else {
	    listField () ;
	}
	if ($row['VariantSize']) {
	    listField ($row['SizeName']) ;			// Size
	} else {
	    listField () ;
	}
	if ($row['VariantDimension']) {
	    listField ($row['Dimension']) ;		// Dimension
	} else {
	    listField () ;
	}
	if ($row['VariantSortation']) {
	    listField ((int)$row['Sortation']) ;		// Sort
	} else {
	    listField () ;
	}
	if ($row['VariantCertificate']) {
	    listField ($row['CertificateTypeName']) ;		// Certification
	} else {
	    listField () ;
	}
//	listField ($row['Produced']) ;				// Produced
	listField ($row['Allocation']) ;			// Allocation
	listField ('', 'style="border-right:1px solid #cdcabb;"') ;
	if ((int)$row['PreviousContainerId'] > 0 and (int)$row['PreviousContainerId'] != (int)$row['ContainerId']) {
	    listField ($row['PreviousStockName']) ;
	    listField ((int)$row['PreviousContainerId'], 'align=right') ;
	} else {
	    listField ('', 'colspan=2') ;
	}
	if ((int)$row['PreviousId'] > 0) {
	    listField ($row['PreviousId'], 'align=right') ;
	} else {
	    listField () ;
	}
	listField ('', 'style="border-right:1px solid #cdcabb;"') ;
	listField ($row['StockName']) ;				// Stock
	listField ($row['ContainerPosition']) ;	
	if ((int)$row['ContainerId'] > 0) {
	    listField ((int)$row['ContainerId'], 'align=right') ;	// Container
	} else {
	    listField () ;
	}	    
	if ((int)$row['PreviousId'] != (int)$row['Id']) {
	    listField ($row['Id'], 'align=right') ;		// Id
	} else {
	    listField () ;
	}	    
	listField ('', 'style="border-right:1px solid #cdcabb;"') ;
    	listField (number_format($row['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;	// Quantity
	listField ($row['UnitName']) ;				// Unit
    } 

    if (dbNumRows($Result) == 0) {
	// No Items found
	listRow () ;
	listField () ;
	listField ('No Items', 'colspan=8') ;
    }
    
    dbQueryFree ($Result) ;
   
    listEnd () ;
 
    return 0 ;
?>
