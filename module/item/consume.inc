<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Item', (int)$Record['Id']) ;
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Quantity', (int)$Record['Quantity'] . ' ' . htmlentities($Record['UnitName'])) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;


    // Get Productions in mention
    if ($Record['ProductionId']>0) 
	    $query = sprintf ('SELECT Production.Id, CONCAT(Production.Number," (",Production.Description,")") AS Value 
						FROM Production 
						LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId 
						WHERE Production.Id=%d AND Production.Active=1 AND Production.Done=0 AND Production.Ready=1 
						      AND (Production.Approved=1 OR ProductionLocation.TypeSample=1) ORDER BY Value', $Record['ProductionId']) ;
    else
	    $query = sprintf ('SELECT Production.Id, CONCAT(Production.Number," (",Production.Description,")") AS Value 
						FROM Production 
						LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId 
						WHERE Production.CaseId=%d AND Production.Active=1 AND Production.Done=0 AND Production.Ready=1 
						      AND (Production.Approved=1 OR ProductionLocation.TypeSample=1) ORDER BY Value', $Record['CaseId']) ;
    $res = dbQuery ($query) ;
    $row = dbFetch ($res) ;
    $count = dbNumRows ($res) ;
    dbQueryFree ($res) ;

    switch ($count) {
	case 0 :
	    if ($Record['ProductionId']>0) 
		    itemField ('Production', 'Wrong state') ;
		else
		    itemField ('Production', 'None') ;
	    print formHidden ('ConsumeProductionId', 0) ;
	    break ;	    

	case 1 :
	    itemField ('Production', sprintf ('%d/%s', (int)$Record['CaseId'], $row['Value'])) ;
	    print formHidden ('ConsumeProductionId', $row['Id']) ;
	    break ;

	default :
	    itemField ('Case', (int)$Record['CaseId']) ;
	    itemFieldRaw ('Production', formDBSelect ('ConsumeProductionId', 0, $query, 'width:250px')) ;
	    break ;
    }

    // Quantity
    itemSpace() ;
    itemFieldRaw ('Quantity left', formText ('Quantity', number_format (0, (int)$Record['UnitDecimals'], ',', ''), 10, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 

    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
