<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;

    // Initialization
    $Line = array () ;

    // Get Fields
    $fields = array (
	'ContainerId'		=> array ('type' => 'decimal',	'format' => '12.4'),
	'StockId'		=> array ('type' => 'integer'),
	'ContainerTypeId'	=> array ('type' => 'integer'),
	'Position'		=> array (),
	'ReqStockId'		=> array ('type' => 'integer')
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
    
    // Get Id's of Items to set price
    $Line = array () ;
    if (!is_array($_POST['Item'])) return 'please select Items which are requested to be moved' ;
    foreach ($_POST['Item'] as $id => $flag) {
	if ($flag != 'on') continue ;
	if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
	$Line[(int)$id] = NULL ;
    }
    if (count($Line) == 0) return 'please select Items which are requested to be moved 2' ;

    // Get Item information
    foreach ($Line as $id => $l) {
	$line = &$Line[$id] ;
	
	// Lookup Item
	$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone 
				FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId 
				LEFT JOIN Stock ON Stock.Id=Container.StockId 
				WHERE Item.Id=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$line['Item'] = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$line['Item']['Id'] != $id) return sprintf ('Item %d not found', $id) ;
	if ((int)$line['Item']['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more', $id) ;
	if ($line['Item']['StockDone'] or ($line['Item']['StockReady'] and !$line['Item']['StockVerified'])) return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
    }    
 
    // Do the setting
    foreach ($Line as $id => $line) {
	    $Item = array (
	    'AltStockId' => $fields['ReqStockId']['value'] 
	    ) ;
	tableWrite ('Item', $Item, $id) ;
    }

    // View 
    return 0 ;
?>
