<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;

    // Initialization
    $Line = array () ;

    // Get Fields
    $fields = array (
	'ContainerId'		=> array ('type' => 'integer'),
	'StockId'		=> array ('type' => 'integer'),
	'ContainerTypeId'	=> array ('type' => 'integer'),
	'Position'		=> array ()
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
    
    // Get Id's of Items to move
    $Line = array () ;
    if (!is_array($_POST['Item'])) return 'please select Items to move' ;
    foreach ($_POST['Item'] as $id => $flag) {
	if ($flag != 'on') continue ;
	if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
	$Line[(int)$id] = NULL ;
    }
    if (count($Line) == 0) return 'please select Items to move' ;

    // Get Item information
    foreach ($Line as $id => $l) {
	$line = &$Line[$id] ;
	
	// Lookup Item
	$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Item.Id=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$line['Item'] = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$line['Item']['Id'] != $id) return sprintf ('Item %d not found', $id) ;
	if ((int)$line['Item']['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more', $id) ;
	if ($line['Item']['StockDone'] or ($line['Item']['StockReady'] and !$line['Item']['StockVerified'])) return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
    }    

    // Container
    if ($fields['ContainerId']['value'] > 0) {
	// Existing Container !
	// Lookup information
	$query = sprintf ('SELECT Container.*, Stock.Name AS StockName, Stock.Ready AS StockReady FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $fields['ContainerId']['value']) ;
	$res = dbQuery ($query) ;
	$Container = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$Container['Id'] != $fields['ContainerId']['value']) return sprintf ('Container %d not found', $fields['ContainerId']['value']) ;
	if ($Container['StockReady']) return sprintf ('no operations allowed on Container %d', (int)$Container['Id']) ;
 
    } else {
	// New Container
	// Any Stock location ?
	if ($fields['StockId']['value'] == 0) return 'please select new Location' ;
		
	// Get Stock
	$query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
	$result = dbQuery ($query) ;
	$Stock = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
	if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on Stock for new Container, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

	// Any ContainerType ?
	if ($fields['ContainerTypeId']['value'] == 0) return 'please type for new Container' ;
		
	// Get ContainerType
	$query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $fields['ContainerTypeId']['value']) ;
	$result = dbQuery ($query) ;
	$ContainerType = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$ContainerType['Id'] != $fields['ContainerTypeId']['value']) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $fields['ContainerTypeId']['value']) ;

	// Create new Container
	$Container = array (
	    'StockId' => (int)$Stock['Id'],
	    'ContainerTypeId' => (int)$ContainerType['Id'],
	    'Position' => $fields['Position']['value'],
	    'TaraWeight' => $ContainerType['TaraWeight'],
	    'GrossWeight' => $ContainerType['TaraWeight'],
	    'Volume' => $ContainerType['Volume']
	) ;
	$Container['Id'] = tableWrite ('Container', $Container) ;
	$Container['StockName'] = $Stock['Name'] ;
    }

    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => sprintf ('Multible Item Move to Container %d at %s', (int)$Container['Id'], $Container['StockName'])
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
        
    // Do the moving
    foreach ($Line as $id => $line) {
	// Any to upgrade ?
	if ((int)$Container['Id'] == (int)$line['Item']['ContainerId']) continue ;

	// Update the Item to the new location
	$Item = array (
	    'ItemOperationId' => $ItemOperation['Id'],
	    'PreviousId' => $id,
	    'PreviousContainerId' => (int)$line['Item']['ContainerId'],
	    'ContainerId' => (int)$Container['Id']
	) ;
	tableWrite ('Item', $Item, $id) ;

	if (tableGetFieldWhere('VariantCode','Id',sprintf('PickContainerId=%d',(int)$line['Item']['ContainerId']))==0) // Do not delete assigned Pick Containers
		containerDeleteEmpty ((int)$line['Item']['ContainerId']) ;
    }

    // View pick
    return navigationCommandMark ('itemlistoperation', $ItemOperation['Id']) ;
?>
