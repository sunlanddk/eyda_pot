<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Limit number in list
    if (dbNumRows($Result) > 12000) return sprintf ('%d Items are to many for upgrading (max. 2100)', dbNumRows($Result)) ;

    formStart () ;
 
    // Header fields
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('From', $Record['Name']) ;
    itemSpace () ;
    $query = sprintf ('SELECT Id, CONCAT(Name," (",Type,")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Type="fixed" AND Active=1 AND FromCompanyId=%d ORDER BY Value', $User['CompanyId']) ;
//	  itemFieldRaw ('Receive to:', formDBSelect ('ToStockId', 14321, $query, 'width:250px;', NULL	                )) ;
    itemSpace () ;

    itemEnd () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Article', 200) ;
    listHead ('Colour', 140) ;
    listHead ('Size', 50) ;
//    listHead ('Dim.', 50) ;
//    listHead ('Sort', 50) ;
//    listHead ('Produced', 80) ;
    listHead ('Item', 45, 'align=right') ;
    listHead ('Allocation', 60) ;
    listHead ('Stock', 100) ;
//    listHead ('Pos', 45, 'align=right') ;
//    listHead ('Container', 55, 'align=right') ;
    listHead ('Quantity', 50, 'align=right') ;
    listHead ('Unit', 35) ;
    listHead ('Return Cmd', 80, 'align=right') ;
    listHead ('Return Qty', 80, 'align=right') ;
    listHead ('') ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
		// Check for operatable stock location
//		if (!$row['StockReady']) continue ;
		
		listRow () ;
		listField ($row['ArticleNumber'] . ' (' . $row['ArticleDescription'] . ')') ;			// Article Number
		if ($row['VariantColor']) {
		    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
		    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
		    $field .= 'display:inline;"></div>' ;
		    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s (%s)</p>', $row['ColorDescription'], $row['ColorNumber']) ;	    
		    listFieldRaw ($field) ;
		} else {
		    listField () ;
		}
		if ($row['VariantSize']) {
		    listField ($row['SizeName']) ;			// Size
		} else {
		    listField () ;
		}
		listField ($row['Id'], 'align=right') ;			// Id
	//	if ($row['VariantDimension']) {
	//	    listField ($row['Dimension']) ;		// Dimension
	//	} else {
	//	    listField () ;
	//	}
	//	if ($row['VariantSortation']) {
	//	    listField ((int)$row['Sortation']) ;		// Sort
	//	} else {
	//	    listField () ;
	//	}
	//	listField ($row['Produced']) ;				// Produced
		listField ($row['Allocation']) ;			// Allocation
		
		if ((int)$row['ContainerId'] > 0) {
		    listField ($row['StockName']) ;				// Stock
	//	    listField ($row['ContainerPosition'], 'align=right') ;	// Container
	//	    listField ((int)$row['ContainerId'], 'align=right') ;	// Container
		} else {
		    listField ('', 'colspan=1') ;
		}
		
		listField (number_format($row['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;	// Quantity
		listField ($row['UnitName']) ;				// Unit
		listFieldRaw (formCheckbox (sprintf('Item[%d]', (int)$row['Id']), 0, 'margin:0;padding:0;'), 'align=right') ;
		listFieldRaw (formText (sprintf('ItemQty[%d]', (int)$row['Id']), 
	                          number_format($row['Quantity'], (int)$Record['UnitDecimals'], ',', ''),
	                          10, 'align=right')) ;
	  
	//	listField ($row['Price'], 'align=right') ;				// Stock Value
		$n++ ;
    } 

    if ($n == 0) {
		// No Items found
		listRow () ;
		listField ('No Items to return - Only valid for ready shipments', 'colspan=8') ;
    }

    dbQueryFree ($Result) ;
   
    listEnd () ;
    formEnd () ;

?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php
    
    return 0 ;
?>
