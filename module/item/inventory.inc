<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Item', (int)$Record['Id']) ;
    itemField ('Article', sprintf ('%d (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Quantity', (int)$Record['Quantity'] . ' ' . htmlentities($Record['UnitName'])) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;

    // Quantity
    itemFieldRaw ('Remaning Qty', formText ('Quantity', number_format ($Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 12, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 
    itemSpace() ;

    // Desc
    itemFieldRaw ('Status ID', formText ('BatchNumber', 'Inventory', 10, 'text-align:right;')) ; 
    itemSpace() ;
	
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
