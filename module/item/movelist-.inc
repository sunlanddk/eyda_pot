<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Limit number in list
    if (dbNumRows($Result) > 2100) return sprintf ('%d Items are to many for upgrading (max. 2100)', dbNumRows($Result)) ;

    formStart () ;
 
    // Header fields
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Container/Price', formText ('ContainerId', '', 6)) ;
    itemField ('', 'or new container at') ;
    itemFieldRaw ('Stock', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    itemFieldRaw ('Type', formDBSelect ('ContainerTypeId', 0, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:150px;')) ;
    itemFieldRaw ('Position', formText ('Position', '', 20)) ;
    itemSpace () ;
    itemEnd () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Article', 70) ;
    listHead ('Item', 45, 'align=right') ;
    listHead ('Colour', 90) ;
    listHead ('Size', 50) ;
    listHead ('Dim.', 50) ;
    listHead ('Sort', 50) ;
    listHead ('Produced', 80) ;
    listHead ('Allocation', 70) ;
    listHead ('Stock', 100) ;
    listHead ('Pos', 45, 'align=right') ;
  	listHead ('MoveTo', 90) ;
    listHead ('Container', 55, 'align=right') ;
    listHead ('Quantity', 50, 'align=right') ;
    listHead ('Unit', 35) ;
    listHead ('Cmd', 35, 'align=right') ;
    listHead ('Unit Value', 80, 'align=right') ;
    listHead ('') ;

    $n = 0 ;
    while ($row = dbFetch($Result)) {
	// Check for operatable stock location
	if (($row['StockDone'] or ($row['StockReady'] and !$row['StockVerified'])) or !$row['ContainerTypeName'] 
		or (int)$row['ContainerId'] == ParameterGet('ProdContainerId') or (int)$row['ContainerId'] == ParameterGet('ReqContainerId') ) continue ;
	
	listRow () ;
	listField ($row['ArticleNumber']) ;			// Article Number
	listField ($row['Id'], 'align=right') ;			// Id
	if ($row['VariantColor']) {
	    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorNumber']) ;	    
	    listFieldRaw ($field) ;
	} else {
	    listField () ;
	}
	if ($row['VariantSize']) {
	    listField ($row['SizeName']) ;			// Size
	} else {
	    listField () ;
	}
	if ($row['VariantDimension']) {
	    listField ($row['Dimension']) ;		// Dimension
	} else {
	    listField () ;
	}
	if ($row['VariantSortation']) {
	    listField ((int)$row['Sortation']) ;		// Sort
	} else {
	    listField () ;
	}
	listField ($row['Produced']) ;				// Produced
	listField ($row['Allocation']) ;			// Allocation
	if ((int)$row['ContainerId'] > 0) {
	    listField ($row['StockName']) ;				// Stock
	    listField ($row['ContainerPosition'], 'align=right') ;	// Container
	    if ($row['StockId'] == $row['AltStockId'])
	        listField ('') ;			// Stock
	    else
	        listField ($row['MoveToName']) ;			// Stock
	    listField ((int)$row['ContainerId'], 'align=right') ;	// Container
	} else {
	    listField ('', 'colspan=4') ;
	}
	listField (number_format($row['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;	// Quantity
	listField ($row['UnitName']) ;				// Unit
	listFieldRaw (formCheckbox (sprintf('Item[%d]', (int)$row['Id']), 0, 'margin:0;padding:0;'), 'align=right') ;
	listField (number_format($row['Price'], 4, ',', '.'), 'align=right') ;	// Stock Value
//	listField ($row['Price'], 'align=right') ;				// Stock Value
	$n++ ;
    } 

    if ($n == 0) {
	// No Items found
	listRow () ;
	listField ('No Itens', 'colspan=8') ;
    }

    dbQueryFree ($Result) ;
   
    listEnd () ;
    formEnd () ;

?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php
    
    return 0 ;
?>
