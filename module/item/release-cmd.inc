<?php

    require_once 'lib/save.inc' ;

    if ($Navigation['Parameters'] == 'all')
		$fields = array (
			'ReservationId'		=> array ('type' => 'set',	'value' => 0),
			'CaseId'		=> array ('type' => 'set',	'value' => 0),
			'ProductionId'		=> array ('type' => 'set',	'value' => 0),
			'OrderLineId'		=> array ('type' => 'set',	'value' => 0)
		) ;
	else
		$fields = array (
			'CaseId'		=> array ('type' => 'set',	'value' => 0),
			'ProductionId'		=> array ('type' => 'set',	'value' => 0),
			'OrderLineId'		=> array ('type' => 'set',	'value' => 0)
		) ;
	
	
    // Is Item real
    if ($Record['ContainerId'] == 0) return sprintf ('%s(%d) Item not real, id %d', __FILE__, __LINE__, $Record['Id']) ;

    // Is it allowed operate Items on this Stock
    if ($Record['StockType'] != 'fixed') return 'only Items on fixed Stock Location can be released' ;

    // Save record
    return saveFields ('Item', $Id, $fields) ;
?>
