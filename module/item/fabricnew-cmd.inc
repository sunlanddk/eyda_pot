<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'lib/string.inc' ;
    require_once 'module/container/include.inc' ;

    // Initialization
    $Line = array () ;

    // Validate Article
    if (!$Record['ArticleTypeFabric']) return 'the Article must be of the type Fabric' ;
    if ($Record['WidthFull'] <= 0) return 'Full Width not entered for this Article' ;
    if ($Record['M2Weight'] <= 0) return 'M2 Weight not entered for this Article' ;
   
    // Get Fields
    $fields = array (
	'ArticleColorId'	=> array ('type' => 'integer'),
	'ArticleSizeId'		=> array ('type' => 'integer'),
	'ArticleCertificateId'	=> array ('type' => 'integer'),
	'Dimension'		=> array ('type' => 'decimal'),
	'Sortation'		=> array ('type' => 'integer'),
	'BatchNumber'		=> array (),
	'Comment'		=> array (),
	'StockId'		=> array ('type' => 'integer'),
	'ContainerTypeId'	=> array ('type' => 'integer'),
	'Position'		=> array (),
	'Unit'			=> array ('type' => 'checkbox')
    ) ;
    $fields['Dimension']['format'] = sprintf ('9.2', '') ;

    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;
    
    // Decimales for Quantity
    // Kg (default) => 3
    // Units (checkbox clicked) => Decimals for ArticleUnit
    $Decimals = ($fields['Unit']['value']) ? (int)$Record['UnitDecimals'] : 3 ;

    // Last Case used
    $Case = array () ;
    
    // Get Id's of Items to move
    if (!is_array($_POST['Quantity'])) return 'please specify Quantity' ;
    foreach ($_POST['Quantity'] as $n => $s) {
	// Quantity
//	$s = str_replace (',', '.',ltrim(trim($s), '0')) ;
//	if ($s == '' or $s == '.') $s = '0' ;
//	$format = sprintf ('(^[0-9]{1,10}$)|(^[0-9]{0,10}\.[0-9]{0,%d}$)', $Decimals) ;
//	if (!ereg($format,$s)) return sprintf ('invalid Quantity format, line %d', (int)$n) ;
//	$Quantity = (float)$s ;
	$Quantity = strFloat ($s, $Decimals) ;
	if ($Quantity == 0) continue ;
	
	// Save
	$Line[(int)$n] = array ('Quantity' => $Quantity) ;

	// Case
	$CaseId = strInteger ($_POST['Case'][$n]) ;
	if (!is_null ($CaseId)) {
	    if ($CaseId === false) return sprintf ('invalid Case number in line %d', $n) ;

	    // Lookup
	    $query = sprintf ('SELECT * FROM `Case` WHERE Id=%d AND Active=1', $CaseId) ;
	    $result = dbQuery ($query) ;
	    $Case = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    
	    // Validate case
	    if ((int)$Case['Id'] != $CaseId) return sprintf ('Case %d not found, line %d', $CaseId, $n) ;
	    if ($Case['Done']) return sprintf ('Case %d is Done, line %d', $CaseId, $n) ;
	}

	// Save Case if selected
	if ((int)$Case['Id'] > 0) $Line[$n]['Case'] = $Case ;
    }
    if (count($Line) == 0) return 'please specify Quantity' ;

    // Get Color
    if ($Record['VariantColor']) {
	if ($fields['ArticleColorId']['value'] == 0) return 'please select Colour' ;
	$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.Id=%d AND ArticleColor.Active=1', $fields['ArticleColorId']['value']) ;
	$result = dbQuery ($query) ;
	$Color = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$Color['Id'] != $fields['ArticleColorId']['value']) return sprintf ('%s(%d) ArticleColor not found, id %d', __FILE__, __LINE__, $fields['ArticleColorId']['value']) ;
	if ((int)$Color['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleColor, id %d', __FILE__, __LINE__, $fields['ArticleColorId']['value']) ;
    }

    // Get Size
    if ($Record['VariantSize']) {
	if ($fields['ArticleSizeId']['value'] == 0) return 'please select Size' ;
	$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.Id=%d AND ArticleSize.Active=1', $fields['ArticleSizeId']['value']) ;
	$result = dbQuery ($query) ;
	$Size = dbFetch ($result) ;
	dbQueryFree ($result) ;
	if ((int)$Size['Id'] != $fields['ArticleSizeId']['value']) return sprintf ('%s(%d) ArticleSize not found, id %d', __FILE__, __LINE__, $fields['ArticleSizeId']['value']) ;
	if ((int)$Size['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleSize, id %d', __FILE__, __LINE__, $fields['ArticleSizeId']['value']) ;
    }

    // Get Certificate
    if ($Record['VariantCertificate']) {
	if ($fields['ArticleCertificateId']['value'] > 0) {
	    // Get Article Certificate
	    $query = sprintf ('SELECT ArticleCertificate.* FROM ArticleCertificate WHERE ArticleCertificate.Id=%d AND ArticleCertificate.Active=1', $fields['ArticleCertificateId']['value']) ;
	    $result = dbQuery ($query) ;
	    $Certificate = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	    if ((int)$Certificate['Id'] != $fields['ArticleCertificateId']['value']) return sprintf ('%s(%d) ArticleCertificate not found, id %d', __FILE__, __LINE__, $fields['ArticleCertificateId']['value']) ;
	    if ((int)$Certificate['ArticleId'] != (int)$Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleCertificate, id %d', __FILE__, __LINE__, $fields['ArticleCertificateId']['value']) ;
	} else {
	    $Certificate = array ('Id' => 0) ;
	}
    }

    // Get Dimension
    if ($Record['VariantDimension']) {
	if ($fields['Dimension']['value'] <= 0) return 'invalid Dimension' ;
    }

    // Get Sortation
    if ($Record['VariantSortation']) {
	if ($fields['Sortation']['value'] < 1 or $fields['Sortation']['value'] > 3) return 'invalid Sortation' ;
    }

    // Get Stock location
    if ($fields['StockId']['value'] == 0) return 'please select Stock location' ;
    $query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
    $result = dbQuery ($query) ;
    $Stock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
    if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on Stock for new Container, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

    // Get Container Type ?
    if ($fields['ContainerTypeId']['value'] == 0) return 'please select type for Container' ;
    $query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $fields['ContainerTypeId']['value']) ;
    $result = dbQuery ($query) ;
    $ContainerType = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ((int)$ContainerType['Id'] != $fields['ContainerTypeId']['value']) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $fields['ContainerTypeId']['value']) ;
    
//logPrintVar ($Line, 'line') ;
    
    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => sprintf ('Fabrics %s Registered on Stock', $Record['Number'])
    ) ;
    if ($fields['BatchNumber']['value'] != '') $ItemOperation['Description'] .= sprintf (', Batch %s', $fields['BatchNumber']['value']) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;
        
    // Do create
    foreach ($Line as $id => $line) {
	// Compute Quantity and Weight
	if ($fields['Unit']['value']) {
	    // Quantity directly entered
	    $Quantity = $line['Quantity'] ;
	    $Weight = $Quantity*(int)$Record['WidthFull']*(int)$Record['M2Weight']/100/1000 ;
	} else {
	    $Weight = $line['Quantity'] ;
	    $Quantity = number_format ($Weight / ((int)$Record['WidthFull']*(int)$Record['M2Weight']/100/1000), (int)$Record['UnitDecimals'], '.', '') ;
	}	

	// Create new Container
	$Container = array (
	    'StockId' => (int)$Stock['Id'],
	    'ContainerTypeId' => (int)$ContainerType['Id'],
	    'Position' => $fields['Position']['value'],
	    'TaraWeight' => $ContainerType['TaraWeight'],
	    'GrossWeight' => (float)$ContainerType['TaraWeight'] + $Weight,
	    'Volume' => $ContainerType['Volume']
	) ;
	$Container['Id'] = tableWrite ('Container', $Container) ;
//	logPrintVar ($Container,'Container') ;

	// Create new Item
	$Item = array (
	    'ArticleId' => (int)$Record['Id'],
	    'Quantity' => $Quantity,
	    'ItemOperationId' => (int)$ItemOperation['Id'],
	    'ContainerId' => (int)$Container['Id'],
	    'BatchNumber' => $fields['BatchNumber']['value'],
	    'Comment' => $fields['Comment']['value']
	) ;
	if ($Record['VariantColor']) 		$Item['ArticleColorId'] = (int)$Color['Id'] ;
	if ($Record['VariantSize']) 		$Item['ArticleSizeId'] = (int)$Size['Id'] ;
	if ($Record['VariantCertificate'])	$Item['ArticleCertificateId'] = (int)$Certificate['Id'] ;
	if ($Record['VariantDimension']) 	$Item['Dimension'] = $fields['Dimension']['value'] ;
	if ($Record['VariantSortation'])	$Item['Sortation'] = $fields['Sortation']['value'] ;
	if ((int)$line['Case']['Id'] > 0)	$Item['CaseId'] = (int)$line['Case']['Id'] ;
	tableWrite ('Item', $Item) ;
//	logPrintVar ($Item, 'Item') ;
    }

    // View pick
    return navigationCommandMark ('itemlistoperation', $ItemOperation['Id']) ;
?>
