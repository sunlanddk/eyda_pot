<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Limit number in list
    if (dbNumRows($Result) > 100) return sprintf ('%d Items are to many for upgrading (max. 100)', dbNumRows($Result)) ;
 
    // List
    listClear () ;
    formStart () ;
    listStart () ;
    listRow () ;
    listHead ('Article', 70) ;
    listHead ('Item', 45, 'align=right') ;
    listHead ('Colour', 90) ;
    listHead ('Size', 50) ;
//  listHead ('Dim.', 50) ;
//  listHead ('Sort', 50) ;
    listHead ('Produced', 80) ;
    listHead ('Allocation', 60) ;
    listHead ('Stock', 100) ;
    listHead ('Container', 55, 'align=right') ;
    listHead ('Quantity', 50, 'align=right') ;
    listHead ('Unit', 35) ;
    listHead ('To Stock', 100) ;
    listHead ('Container', 60, 'align=right') ;
    listHead ('Quantity', 50, 'align=right') ;
    listHead ('') ;

    while ($row = dbFetch($Result)) {
	// Lookup default continernumber for this line
	$query = sprintf ('SELECT Container.Id, Container.StockId FROM Item INNER JOIN Container ON Container.Id=Item.ContainerId INNER JOIN Stock ON Stock.Id=Container.StockId AND Stock.Ready=0 WHERE Item.FromProductionId=%d AND Item.Sortation=1', (int)$row['FromProductionId']) ;
	if ($row['VariantColor']) $query .= sprintf (' AND ArticleColorId=%d', (int)$row['ArticleColorId']) ;
	if ($row['VariantSize']) $query .= sprintf (' AND ArticleSizeId=%d', (int)$row['ArticleSizeId']) ;
	$query .= ' AND Item.Active=1 ORDER BY Item.Quantity LIMIT 1' ;
	$res = dbQuery ($query) ;
	$Container = dbFetch ($res) ;
	dbQueryFree ($res) ;

	listRow () ;
	listField ($row['ArticleNumber']) ;			// Article Number
	listField ($row['Id'], 'align=right') ;			// Id
	if ($row['VariantColor']) {
	    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorNumber']) ;	    
	    listFieldRaw ($field) ;
	} else {
	    listField () ;
	}
	if ($row['VariantSize']) {
	    listField ($row['SizeName']) ;			// Size
	} else {
	    listField () ;
	}
//	if ($row['VariantDimension']) {
//	    listField ((int)$row['Dimension']) ;		// Dimension
//	} else {
//	    listField () ;
//	}
//	if ($row['VariantSortation']) {
//	    listField ((int)$row['Sortation']) ;		// Sort
//	} else {
//	    listField () ;
//	}
	listField ($row['Produced']) ;				// Produced
	listField ($row['Allocation']) ;			// Allocation
	if ((int)$row['ContainerId'] > 0) {
	    listField ($row['StockName']) ;				// Stock
	    listField ((int)$row['ContainerId'], 'align=right') ;	// Container
	} else {
	    listField ('', 'colspan=2') ;
	}
	listField (number_format($row['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;	// Quantity
	listField ($row['UnitName']) ;				// Unit
	if ($Container['StockId']>0)
	    	listField (tableGetField('Stock', 'Name', $Container['StockId'])) ;				// To Stock
	else
	    listField ('');
	listFieldRaw (formText (sprintf('Container[%d]', (int)$row['Id']), $Container['Id'], 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	listFieldRaw (formText (sprintf('Quantity[%d]', (int)$row['Id']), '', 3, 'text-align:right;width:30px;margin-right:0;'), 'align=right') ;
    } 

    if (dbNumRows($Result) == 0) {
	// No Items found
	listRow () ;
	listField ('No 2. Sortation Items', 'colspan=8') ;
    }

    dbQueryFree ($Result) ;
   
    listEnd () ;
    formEnd () ;
    
    return 0 ;
?>
