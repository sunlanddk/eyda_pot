<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/navigation.inc' ;

    // Form
    printf ("<form method=GET name=selectform>\n") ;
    printf ("<input type=hidden name=nid value=%d>\n", navigationMark('itemview')) ;
    printf ("<table class=item>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Item</td><td><input type=text name=id size=8 maxlength=8 value='%s'></td></tr>\n", ($Id > 0)?$Id:'') ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
