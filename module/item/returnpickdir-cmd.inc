<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/db.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'lib/parameter.inc' ;
    require_once 'module/container/include.inc' ;
    require_once 'lib/inventory.inc' ;

    // Initialization
    $Line = array () ;
    $articlesToUpdate = [];

    // Get Id's of Items to move
    $Line = array () ;
    if (!is_array($_POST['Item'])) return 'please select Items to return' ;
    foreach ($_POST['Item'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$Line[(int)$id] = NULL ;
    }
    if (count($Line) == 0) return 'please select Items to return' ;

    // Get Item information
    foreach ($Line as $id => $l) {
	$line = &$Line[$id] ;
	
	// Lookup Item
	$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone 
	FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId LEFT JOIN Stock ON Stock.Id=Container.StockId 
	WHERE Item.Id=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$line['Item'] = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$line['Item']['Id'] != $id) return sprintf ('Item %d not found', $id) ;
	if ((int)$line['Item']['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more', $id) ;

    }    

    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => 'Direct return Items to Pick Positions'
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

    $error_note='' ;
    
    // Do the moving
	$n = 0 ;
    foreach ($Line as $id => $line) {
    $n++ ;
		// Lookup Item
		$query = sprintf ('SELECT * FROM Item WHERE Item.Id=%d AND Item.Active=1 And Item.Sortation<2', $id) ;
		$res = dbQuery ($query) ;
		$Item = dbFetch ($res) ;
		dbQueryFree ($res) ;
		if ($Item['Id'] > 0) {
			// Find Pick position (container)
			$query=sprintf('SELECT * FROM variantcode WHERE ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d And Active=1',
						(int)$Item['ArticleId'],(int)$Item['ArticleColorId'],(int)$Item['ArticleSizeId']) ;
			$res = dbQuery ($query) ;
			if (dbNumRows($res)>1) return 'Urgh - something is wrong: ' . $query ; 
			$VariantCode = dbFetch ($res) ;
			dbQueryFree ($res) ;

			if ($VariantCode['PickContainerId']>0) {
				if ((int)$VariantCode['PickContainerId'] == (int)$line['Item']['ContainerId']) continue ;
			} else {
				$VariantCode['PickContainerId'] =  (int)ParameterGet('PickContainerId') ;
			}	
			$ReturnQty = (float)$_POST['ItemQty'][$id] ;
			if ($ReturnQty < 0) $ReturnQty = 0 ;
			if ($ReturnQty > $Item['Quantity']) $ReturnQty = $Item['Quantity'] ;

			$ShipQty = (float)$Item['Quantity'] - (float)$ReturnQty ;

			// Split item and create new item on shipment
			if ($ShipQty > 0) {
				unset ($Item['Id']) ;
				$Item['Quantity'] = $ShipQty ;
				tableWrite ('Item', $Item) ;
			}

			// Update the Item to the new location
			$Item = array (
				'ItemOperationId' => $ItemOperation['Id'],
				'PreviousId' => $id,
				'PreviousContainerId' => (int)$Item['ContainerId'],
				'OrderLineid' => 0,
				'Quantity' => (int)$ReturnQty,
				'ContainerId' => (int)$VariantCode['PickContainerId']
			) ;
			tableWrite ('Item', $Item, $id) ;
			array_push(
				$articlesToUpdate, 
				array('aId' => (int)$Item['ArticleId'], 'acId' => (int)$Item['ArticleColorId'], 'asId' => (int)$Item['ArticleSizeId'], 'qty' => (int)$ReturnQty)
			);
			// Maintain containers even if empty				containerDeleteEmpty ((int)$Item['PreviousContainerId']) ;
		}
    }
    $ItemOperation['Description']= $ItemOperation['Description'] . $error_note ;
    $ItemOperation['Description']= substr($ItemOperation['Description'],0,99) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation, $ItemOperation['Id']) ;

    updateInventoryOnArticles($articlesToUpdate, true);
    // View pick
    return 0 ; // navigationCommandMark ('itemlistopreturn', $ItemOperation['Id']) ;
?>
