<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;

    $fields = array (
	'Quantity'		=> array ('type' => 'decimal',	'check' => true),
	'ContainerId'		=> array ('type' => 'integer',	'check' => true),
	'PreviousContainerId'	=> array ('type' => 'set'),
	'CaseId'		=> array ('type' => 'integer',	'check' => true),
	'ProductionId'		=> array ('type' => 'integer',				'check' => true),
	'OrderLineId'		=> array ('type' => 'integer',	'check' => true)
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields, $Quantity ;
	switch ($fieldname) {
	    case 'ProductionId':
		if (!$changed) return false ;
 
		if ($value == 0) {
		    $fields['ProductionId']['value'] = 0 ;
		    return true ;
		}

		// Get Production
		$query = sprintf ("SELECT Id FROM Production WHERE Number=%d And CaseId=%d And Active=1", $value, $fields['CaseId']['value']) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;

 		if ($row['Id'] <= 0)
			return sprintf ('Production %d not found on case %d', $value, $fields['CaseId']['value']) ;

	    $fields['ProductionId']['value'] = $row['Id'] ;

		return true ;

	    case 'Quantity':
		// Validate Quantity
		if ($value == 0) return 'please specify Quantity' ;		
		if ($value > $Quantity) return sprintf ('requested Quantity (%d) higher than Item quantity (%d)', $value, $Quantity) ;
		
		if ($value == $Quantity) {
		    // The whole Item are requested to be split
		    // Just leave original Quantity
		    return false ;    
		}

		// Set remaining Quantity form the parent Item
		$fields['Quantity']['value'] = number_format ($Quantity - $value, (int)$Record['UnitDecimals'], '.', '') ;
		return true ;

	    case 'ContainerId' :
		if (!$changed) return false ;
		if ($value == 0) return 'please specify Container' ;
		
		// Get Container
		$query = sprintf ('SELECT Container.*, Stock.Id AS StockId, Stock.Type AS StockType, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone FROM Container LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] == 0) return sprintf ('Container not found, id %d', $value) ;

	        // Is it allowed operate Items on this Stock
		if ($row['StockDone']) return sprintf ('now Stock Location is done') ;
		if ($row['StockReady'] and !$row['StockVerified']) return sprintf ('new Stock Location is locked') ;

		// Just exit witout modifying ContainerId if a new split Item are to be created
		if ($fields['Quantity']['value'] < $Quantity) return false ;
		
		// Set the old container
		$fields['PreviousContainerId']['value'] = $Record['ContainerId'] ;

		return true ;

	    case 'CaseId' :
		if ($value == 0) return false ;
		
		// Get Case
		$query = sprintf ('SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('Case not found, number %d', $value) ;
		if ($row['Done']) return sprintf ('Case is Done, number %d', $value) ;

		// Just exit witout modifying ContainerId if a new split Item are to be created
		if ($fields['Quantity']['value'] < $Quantity) return false ;

		return true ;		

	    case 'OrderLineId' :
		if ($value == 0) return false ;

		// Varify that the item hasn't been allocated twice
		if ($fields['CaseId']['value'] > 0) return 'please only select either Case og OrderLine' ; 	

		// Get Customer (if any) for the Item
		$CompanyId = 0 ;
		if (false and $Record['ArticleTypeProduct'] and (int)$Record['FromProductionId'] > 0) {
		    // A production is allocated to the Item
		    // Get the Customer
		    $query = sprintf ('SELECT Case.CompanyId FROM Production LEFT JOIN `Case` ON Case.Id=Production.CaseId WHERE Production.Id=%d', (int)$Record['FromProductionId']) ;
		    $res = dbQuery ($query) ;
		    $row = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    $CompanyId = (int)$row['CompanyId'] ;
		}	
    
		// Construct query for ComboBox
		$query = sprintf ('SELECT OrderLine.*, Order.CompanyId FROM OrderLine LEFT JOIN `Order` ON Order.Id=OrderLine.OrderId WHERE OrderLine.Id=%d AND OrderLine.Active=1', $value) ;
		$res = dbQuery ($query) ;
		$row = dbFetch ($res) ;
		dbQueryFree ($res) ;
		if ((int)$row['Id'] != $value) return sprint ('%s(%d) OrderLine not found, id %d', __FILE__, __LINE__, $value) ; 
		if ((int)$row['ArticleId'] != (int)$Record['ArticleId']) return sprint ('%s(%d) ArticleId mismatch', __FILE__, __LINE__) ; 
		if ($Record['VariantColor'] and (int)$row['ArticleColorId'] != (int)$Record['ArticleColorId']) return sprint ('%s(%d) ArticleColorId mismatch', __FILE__, __LINE__) ; 
		if ($CompanyId > 0 and (int)$row['CompanyId'] != $CompanyId) return sprint ('%s(%d) CompanyId mismatch', __FILE__, __LINE__) ; 

		// Just exit witout modifying ContainerId if a new split Item are to be created
		if ($fields['Quantity']['value'] < $Quantity) return false ;

		return true ;		
	}
    }

    // Update field list
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;
    
    // Is Item real
    if ($Record['ContainerId'] == 0) return sprintf ('%s(%d) Item not real, id %d', __FILE__, __LINE__, $Record['Id']) ;

    // Is it allowed operate Items on this Stock
//    if ($Record['StockType'] != 'fixed') return 'only Items on fixed Stock Location can be split' ;
		
    // Get Original Item quantity
    $Quantity = (float)$Record['Quantity'] ;
    
    // Save record
    $res = saveFields ('Item', $Id, $fields, true) ;
    if ($res) return $res ;

    // Check is the whole Quantity of the parent Item are to be split
    if ((float)$fields['Quantity']['value'] == $Quantity) return 0 ;
    
    // Create specific item for consumption to Production
    // Copy columns for Items
    unset ($item) ;
    $query = 'SHOW COLUMNS FROM Item' ;
    $result = dbQuery ($query) ;
    while ($column = dbFetch ($result)) {
        $field = $column['Field'] ;
        $item[$field] = $Record[$field] ;
    }
    dbQueryFree ($result) ;
	
    // Update for consumption
    $item['ContainerId'] = $fields['ContainerId']['value'] ;
    $item['Quantity'] = number_format ($Quantity - $fields['Quantity']['value'], (int)$Record['UnitDecimals'], '.', '') ;
    $item['ParentId'] = $Record['Id'] ;
    $item['CaseId'] = $fields['CaseId']['value'] ;
    $item['OrderLineId'] = $fields['OrderLineId']['value'] ;
    $item['PreviousContainerId'] = 0 ;
    $item['PreviousId'] = 0 ;
    $item['ItemOperationId'] = 0 ;
	
    if ($Record['TestDone']) {
        $item['TestDone'] = 0 ;
        $item['TestItemId'] = $Record['Id'] ;
    }
	
    // Create Consumption Item
    tableWrite ('Item', $item) ;
    
    return 0 ;    
?>
