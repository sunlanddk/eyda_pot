<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Item', (int)$Record['Id']) ;
    itemField ('Article', sprintf ('%d (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemField ('Quantity', (int)$Record['Quantity'] . ' ' . htmlentities($Record['UnitName'])) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;

    // Quantity
    itemFieldRaw ('Quantity', formText ('Quantity', number_format ($Record['Quantity'], (int)$Record['UnitDecimals'], ',', ''), 12, 'text-align:right;') . ' ' . htmlentities($Record['UnitName'])) ; 
    itemSpace() ;

    // Container
    itemFieldRaw ('Container', formText ('ContainerId', (int)$Record['ContainerId'], 12, 'text-align:right;')) ; 
    itemSpace() ;
	
    // Case Allocation
    if ((int)$Record['ArticleTypeMaterial']) {
	itemFieldRaw ('Case', formText ('CaseId', '', 6, 'text-align:right;')) ; 
//	itemFieldRaw ('Case', formText ('CaseId', ((int)$Record['CaseId'] > 0) ? $Record['CaseId'] : '' , 6, 'text-align:right;')) ; 
	itemFieldRaw ('Production', formText ('ProductionId', ((int)$Record['ProductionId'] > 0) ? (int)tableGetField ('Production', 'Number', $Record['ProductionId']) : '', 6, 'text-align:right;')) ; 
	itemSpace() ;
    }

    // OrderLine Allocation
    $CompanyId = 0 ;
    if (false and $Record['ArticleTypeProduct'] and (int)$Record['FromProductionId'] > 0) {
	// A production is allocated to the Item
	// Get the Customer
	$query = sprintf ('SELECT Case.CompanyId FROM Production LEFT JOIN `Case` ON Case.Id=Production.CaseId WHERE Production.Id=%d', (int)$Record['FromProductionId']) ;
//	itemFieldText ('query', $query) ;
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	$CompanyId = (int)$row['CompanyId'] ;
    }	
    
    // Construct query for ComboBox
    $query = 'SELECT OrderLine.Id, CONCAT(Order.Id," (",Company.Name,"), ",OrderLine.No," (",OrderLine.DeliveryDate,")") AS Value
    FROM (`Order`, OrderLine) LEFT JOIN Company ON Company.Id=Order.CompanyId
    WHERE OrderLine.ArticleId=' . (int)$Record['ArticleId'] . ' AND OrderLine.Active=1 AND Order.Id=OrderLine.OrderId' ;
    if ($Record['VariantColor']) $query .= ' AND OrderLine.ArticleColorId=' . (int)$Record['ArticleColorId'] ;
    if ($CompanyId > 0) $query .= ' AND Order.CompanyId=' . $CompanyId ;
    $query .= ' ORDER BY Value' ;
//  itemFieldText ('query', $query) ;
    itemFieldRaw ('Order', formDBSelect ('OrderLineId', 0, $query, 'width:350px;')) ; 
    
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
