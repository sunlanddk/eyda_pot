<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/navigation.inc' ;

    $fields = array (
	'ArticleId'		=> array ('type' => 'set'),
	'Quantity'		=> array ('type' => 'decimal',	'mandatory' => true,	'check' => true),
	'Price'		=> array ('type' => 'decimal',	'check' => true),
	'ContainerId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'PreviousContainerId'	=> array ('type' => 'set'),
	'ArticleColorId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'ArticleSizeId'		=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'ArticleCertificateId'	=> array ('type' => 'integer',	'mandatory' => true,	'check' => true),
	'Dimension'		=> array ('type' => 'decimal',	'mandatory' => true),
	'Sortation'		=> array ('type' => 'integer',	'mandatory' => true),
	'CaseId'		=> array ('type' => 'integer',				'check' => true),
	'TestDone'		=> array ('type' => 'checkbox',	'check' => true),
	'TestDoneUserId'	=> array ('type' => 'set'),
	'TestDoneDate' 		=> array ('type' => 'set'),
	'BatchNumber'		=> array (),
	'ReceivalNumber'		=> array (),
	'RequisitionId'		=> array ('type' => 'integer',				'check' => true),
	'ProductionId'		=> array ('type' => 'integer',				'check' => true),
	'RequisitionLineId'		=> array ('type' => 'integer', 'check' => true),
	'Comment'		=> array ()
    ) ;

    function checkfield ($fieldname, $value, $changed) {
	global $Id, $Record, $User, $fields ;
	switch ($fieldname) {
	    case 'ProductionId':
		if (!$changed) return false ;
 
		if ($value == 0) {
		    $fields['ProductionId']['value'] = 0 ;
		    return true ;
		}

		// Get Production
		$query = sprintf ("SELECT Id FROM Production WHERE Number=%d And CaseId=%d And Active=1", $value, $fields['CaseId']['value']) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;

 		if ($row['Id'] <= 0)
			return sprintf ('Production %d not found on case %d', $value, $fields['CaseId']['value']) ;

	    $fields['ProductionId']['value'] = $row['Id'] ;

		return true ;

	    case 'RequisitionId':
		if (!$changed) return false ;

		if ($value == 0) {
		    $fields['RequisitionId']['value'] = 0 ;
		    return true ;
		}

		// Get Requisition
		$query = sprintf ("SELECT Requisition.* FROM Requisition WHERE Requisition.Id=%d", $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('Requistion not found, id %d', $value) ;

		return true ;

	    case 'RequisitionLineId':
		if (!$changed) return false ;

		if ($value == 0) {
		    $fields['RequisitionLineId']['value'] = 0 ;
		    return true ;
		}

		// Get Requisition
		$query = sprintf ("SELECT RequisitionLine.* FROM RequisitionLine WHERE RequisitionLine.No=%d AND RequisitionLine.Requisitionid=%d", $value, $fields['RequisitionId']['value']) ;

		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;

 		if ($row['RequisitionId'] != $fields['RequisitionId']['value']) 
			return sprintf ('RequisitionLine %d not found on requisition %d', $value, $fields['RequisitionId']['value']) ;
		if ($row['ArticleId'] != $Record['ArticleId']) 
			return sprintf ('RequisitionLine article not identical %d, %d', $row['ArticleId'], $Record['ArticleId']) ;

	    $fields['RequisitionLineId']['value'] = $row[Id] ;

		return true ;

	    case 'Quantity':
		if (!$changed) return false ;
		if ($value <= 0) return 'invalid Quantity' ;
		return true ;

	    case 'Price':
		if (!$changed) return false ;
		if ($value <= 0) return 'invalid Price' ;
		return true ;

	    case 'Dimension':
		if (!$changed) return false ;
		if ($value <= 0) return 'invalid Dimension' ;
		return true ;

	    case 'ContainerId' :
		if (!$changed) return false ;
		
		// Get container
		$query = sprintf ('SELECT Container.*, Stock.Type AS StockType FROM Container LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('Container not found, id %d', $value) ;
		
		// Validate storage type
		if ($row['StockId'] <= 0 or $row['StockType'] != 'fixed') return 'new Items must be created at a fixed Stock Location' ;

		// Set the old container
		$fields['PreviousContainerId']['value'] = $Record['ContainerId'] ;
		
		return true ;

	    case 'ArticleColorId' :
		if (!$changed) return false ;
		if ($value == 0) return 'please select Color' ;
		
		// Get Article Color
		$query = sprintf ('SELECT ArticleColor.* FROM ArticleColor WHERE ArticleColor.Id=%d AND ArticleColor.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) ArticleColor not found, id %d', __FILE__, __LINE__, $value) ;
		if ($row['ArticleId'] != $Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleColor, id %d', __FILE__, __LINE__, $value) ;
		
		return true ;

	    case 'ArticleSizeId' :
		if (!$changed) return false ;
		if ($value == 0) return 'please select Size' ;
		
		// Get Article Size
		$query = sprintf ('SELECT ArticleSize.* FROM ArticleSize WHERE ArticleSize.Id=%d AND ArticleSize.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('%s(%d) ArticleSize not found, id %d', __FILE__, __LINE__, $value) ;
		if ($row['ArticleId'] != $Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleSize, id %d', __FILE__, __LINE__, $value) ;
		
		return true ;

	    case 'ArticleCertificateId' :
		if (!$changed) return false ;
		
		if ($value != 0) {
		    // Get Article Certificate
		    $query = sprintf ('SELECT ArticleCertificate.* FROM ArticleCertificate WHERE ArticleCertificate.Id=%d AND ArticleCertificate.Active=1', $value) ;
		    $result = dbQuery ($query) ;
		    $row = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    if ($row['Id'] != $value) return sprintf ('%s(%d) ArticleCertificate not found, id %d', __FILE__, __LINE__, $value) ;
		    if ($row['ArticleId'] != $Record['ArticleId']) return sprintf ('%s(%d) invalid ArticleId in ArticleCertificate, id %d', __FILE__, __LINE__, $value) ;
		}
		
		return true ;

	    case 'CaseId' :
		if (!$changed) return false ;
		if ($value == 0) {
		    $fields['CaseId']['value'] = 0 ;
		    return true ;
		}
		
		// Get Case
		$query = sprintf ('SELECT `Case`.* FROM `Case` WHERE Case.Id=%d AND Case.Active=1', $value) ;
		$result = dbQuery ($query) ;
		$row = dbFetch ($result) ;
		dbQueryFree ($result) ;
		if ($row['Id'] != $value) return sprintf ('Case not found, number %d', $value) ;
		if ($row['Done']) return sprintf ('Case is Done, number %d', $value) ;
		
		return true ;
		
	    case 'TestDone' :
		if (!$changed) return false ;

		// Allow resetting flag
		if (!$value) return true ;
		
		// Set tracking information when setting flag
		$fields['TestDoneUserId']['value'] = $User['Id'] ;
		$fields['TestDoneDate']['value'] = dbDateEncode(time()) ;
		return true ;
	}
	return false ;
    }

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $Id = -1 ;
	    $fields['ArticleId']['value'] = (int)$Record['ArticleId'] ;
	    unset ($Record['TestDone']) ;
	    break ;

	default :
	    // Validate
	    if ((int)$Record['ContainerId'] == 0) return 'consumed Items can not be modified' ;
	    if ($Record['StockType'] == 'shipment') return 'shipped Items can not be modified' ;
	    break ;
    }

    // Update field list
    if (!$Record['VariantColor']) unset ($fields['ArticleColorId']) ;
    if (!$Record['VariantSize']) unset ($fields['ArticleSizeId']) ;
    if (!$Record['VariantCertificate']) unset ($fields['ArticleCertificateId']) ;
    if (!$Record['VariantDimension']) unset ($fields['Dimension']) ;
    if (!$Record['VariantSortation']) unset ($fields['Sortation']) ;
    if (!$Record['ArticleTypeTest']) unset ($fields['TestDone']) ;
    if (!$Record['ArticleTypeMaterial']) unset ($fields['CaseId']) ;
//    if (!$Record['ArticleTypeFabric']) unset ($fields['BatchNumber']) ;
    $fields['Quantity']['format'] = sprintf ('9.%d', $Record['UnitDecimals']) ;
    $fields['Dimension']['format'] = sprintf ('9.2', '') ;
    $fields['Price']['format'] = sprintf ('9.4', '') ;
    unset ($fields['RequisitionLineNo']) ;
    
    // Save record
    $res = saveFields ('Item', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new Item
	    return navigationCommandMark ('itemview', $Record['Id']) ;
    }
    
    return 0 ;    
?>
