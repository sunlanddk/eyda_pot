<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/table.inc' ;

    // Get Item and check they belong to the same Case
    $Line = array () ;
    $CaseId = 0 ;
    $InitProductionId = 0 ;
    while ($row = dbFetch($Result)) {
	// Check if opratable stock
	if ($row['StockReady']) continue ;
	
	if ($CaseId == 0) {
	    $CaseId = (int)$row['CaseId'] ;
	} else {
	    if ($CaseId != (int)$row['CaseId']) return 'all selected Items must be allocated to the same Case' ;
	}
	$Line[(int)$row['Id']] = $row ;
	if (($InitProductionId == 0) AND ((int)$row['ProductionId'] > 0)) $InitProductionId = (int)$row['ProductionId'] ;
    }
    dbQueryFree ($Result) ;

    // Limit number in list
    if (count($Line) > 200) return sprintf ('%d Items are to many for upgrading (max. 200)', count($Line)) ;
    
    // Get Productions in mention
    $query = sprintf ('SELECT Production.Id, CONCAT(Production.Number," (",Production.Description,")") AS Value FROM Production LEFT JOIN ProductionLocation ON ProductionLocation.Id=Production.ProductionLocationId WHERE Production.CaseId=%d AND Production.Active=1 AND Production.Done=0 AND Production.Ready=1 AND (Production.Approved=1 OR ProductionLocation.TypeSample=1) ORDER BY Value', $CaseId) ;
//return $query ;
    $res = dbQuery ($query) ;
    $Production = dbFetch ($res) ;
    $ProductionCount = dbNumRows ($res) ;
    dbQueryFree ($res) ;

    formStart () ;
 
    // Header fields
    itemStart () ;
    itemHeader () ;
    itemField ('Case', ($CaseId > 0) ? $CaseId : 'None') ;
    switch ($ProductionCount) {
	case 0 :
	    itemField ('Production', 'None') ;
	    break ;	    

	case 1 :
	    itemField ('Production', $Production['Value']) ;
	    print formHidden ('ProductionId', $Production['Id']) ;
	    break ;

	default :
	    itemFieldRaw ('Production', formDBSelect ('ProductionId', $InitProductionId, $query, 'width:250px')) ;
	    break ;
    }
    itemSpace () ;
    itemEnd () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('Article', 70) ;
    listHead ('Item', 45, 'align=right') ;
    listHead ('Colour', 90) ;
    listHead ('Size', 35) ;
    listHead ('Dim.', 35) ;
    listHead ('Sort', 30) ;
    listHead ('Stock', 90) ;
    listHead ('Container', 55, 'align=right') ;
    listHead ('Alloc', 60) ;
    listHead ('Quantity', 50, 'align=right') ;
    listHead ('Unit', 35) ;
    listHead ('Left', 70, 'align=right') ;
    listHead ('') ;

    foreach ($Line as $row) {
	listRow () ;
	listField ($row['ArticleNumber']) ;			// Article Number
	listField ($row['Id'], 'align=right') ;			// Id
	if ($row['VariantColor']) {
	    $field = '<div style="width:20px;height:15px;margin-left:8px;margin-top:1px;' ;
	    if ((int)$row['ColorGroupId'] > 0) $field .= sprintf ('background:#%02x%02x%02x;', (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue']) ;
	    $field .= 'display:inline;"></div>' ;
	    $field .= sprintf ('<p class=list style="padding-left:4px;display:inline;">%s</p>', $row['ColorNumber']) ;	    
	    listFieldRaw ($field) ;
	} else {
	    listField () ;
	}
	if ($row['VariantSize']) {
	    listField ($row['SizeName']) ;			// Size
	} else {
	    listField () ;
	}
	if ($row['VariantDimension']) {
	    listField ($row['Dimension']) ;		// Dimension
	} else {
	    listField () ;
	}
	if ($row['VariantSortation']) {
	    listField ((int)$row['Sortation']) ;		// Sort
	} else {
	    listField () ;
	}
	if ((int)$row['ContainerId'] > 0) {
	    listField ($row['StockName']) ;				// Stock
	    listField ((int)$row['ContainerId'], 'align=right') ;	// Container
	} else {
	    listField ('', 'colspan=2') ;
	}
	
	if ((int)$row['ProductionId'] > 0)
		listField (sprintf("%d/%d", $row['CaseId'], (int)tableGetField ('Production', 'Number', $row['ProductionId']))) ;
	else
		listField (sprintf("%d", $row['CaseId'])) ;
	listField (number_format((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', '.'), 'align=right') ;	// Quantity
	listField ($row['UnitName']) ;				// Unit
	listFieldRaw (formText (sprintf('Quantity[%d]', (int)$row['Id']), number_format ((float)$row['Quantity'], (int)$row['UnitDecimals'], ',', ''), 7, 'text-align:right;width:60px;margin-right:0;'), 'align=right') ;
    } 

    if (count($Line) == 0) {
	// No Items found
	listRow () ;
	listField ('No Itens', 'colspan=8') ;
    }
   
    listEnd () ;
    formEnd () ;

?>
<script type='text/javascript'>
function ZeroAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'text') continue ;
	e.value = '0' ;
    }
}
</script>
<?php
    
    return 0 ;
?>
