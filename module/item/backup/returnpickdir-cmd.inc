<?php

    require_once 'lib/table.inc' ;
    require_once 'lib/db.inc' ;
    require_once 'lib/save.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;

    // Initialization
    $Line = array () ;

    // Get Id's of Items to move
    $Line = array () ;
    if (!is_array($_POST['Item'])) return 'please select Items to return' ;
    foreach ($_POST['Item'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$Line[(int)$id] = NULL ;
//return $_POST['ItemQty'][$id] ;
    }
    if (count($Line) == 0) return 'please select Items to return' ;

    // Get Item information
    foreach ($Line as $id => $l) {
	$line = &$Line[$id] ;
	
	// Lookup Item
	$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady, Stock.Verified AS StockVerified, Stock.Done AS StockDone FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId LEFT JOIN Stock ON Stock.Id=Container.StockId WHERE Item.Id=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$line['Item'] = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$line['Item']['Id'] != $id) return sprintf ('Item %d not found', $id) ;
	if ((int)$line['Item']['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more', $id) ;
//	if ($line['Item']['StockType']=='transport' or $line['Item']['StockType']=='shipment') return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
//	if ($line['Item']['StockDone'] or ($line['Item']['StockReady'] and !$line['Item']['StockVerified'])) return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;
    }    

    // Create ItemOperation referance
    $ItemOperation = array (
	'Description' => 'Direct return Items to Pick Positions'
    ) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

    $error_note='' ;
    
    // Do the moving
    foreach ($Line as $id => $line) {
		// Lookup Item
		$query = sprintf ('SELECT * FROM Item WHERE Item.Id=%d AND Item.Active=1 And Item.Sortation<2', $id) ;
		$res = dbQuery ($query) ;
		$Item = dbFetch ($res) ;
		dbQueryFree ($res) ;
		if ($Item['Id'] > 0) {
			// Find Pick position (container)
			$query=sprintf('SELECT * FROM variantcode WHERE ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d And Active=1',
						(int)$Item['ArticleId'],(int)$Item['ArticleColorId'],(int)$Item['ArticleSizeId']) ;
			$res = dbQuery ($query) ;
			if (dbNumRows($res)>1) return 'Urgh Ken: ' . $query ; 
			$VariantCode = dbFetch ($res) ;
			dbQueryFree ($res) ;

			if ($VariantCode['PickContainerId']>0) {
				if ((int)$VariantCode['PickContainerId'] == (int)$line['Item']['ContainerId']) continue ;
//return 'Debug 1 ' . $_POST['ItemQty'][$id] ;
	
				$ReturnQty = (float)$_POST['ItemQty'][$id] ;
				if ($ReturnQty < 0) $ReturnQty = 0 ;
				if ($ReturnQty > $Item['Quantity']) $ReturnQty = $Item['Quantity'] ;
//		return 'Debug ' . $ReturnQty . ' Item ' . $Item['Quantity'] ;
				
				$ShipQty = (float)$Item['Quantity'] - (float)$ReturnQty ;
//		return 'Debug ' . $ShipQty ;

				// Split item and create new item on shipment
				if ($ShipQty > 0) {
					unset ($Item['Id']) ;
					$Item['Quantity'] = $ShipQty ;
					tableWrite ('Item', $Item) ;
				}
	
				// Update the Item to the new location
				$Item = array (
					'ItemOperationId' => $ItemOperation['Id'],
					'PreviousId' => $id,
					'PreviousContainerId' => (int)$Item['ContainerId'],
					'OrderLineid' => 0,
					'Quantity' => (int)$ReturnQty,
					'ContainerId' => (int)$VariantCode['PickContainerId']
				) ;
				tableWrite ('Item', $Item, $id) ;
				containerDeleteEmpty ((int)$Item['PreviousContainerId']) ;
			} else {
				if ($VariantCode['Id']>0) 
					$error_note=$error_note.sprintf('<br>Item %d not moved to pick due to no pick position assigned', (int)$line['Item']['Id']) ;
				else 
					$error_note=$error_note.sprintf('<br>Item %d not moved to pick due to no variantcode', (int)$line['Item']['Id']) ;
			}
		}
    }
    $ItemOperation['Description']= $ItemOperation['Description'].$error_note ;
    $ItemOperation['Description']= substr($ItemOperation['Description'],0,99) ;
    $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation, $ItemOperation['Id']) ;

    // View pick
    return navigationCommandMark ('itemlistopreturn', $ItemOperation['Id']) ;
?>
