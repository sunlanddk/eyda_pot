<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    if ((int)$Record['Id'] > 0) {
	itemField ('Item', (int)$Record['Id']) ;
    }
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;

    // Variants
    $n = 0 ;
    if ($Record['VariantColor']) {
	$n++ ;
	itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', (int)$Record['ArticleColorId'], sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value FROM ArticleColor, Color WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Value', $Record['ArticleId']), 'width:200px')) ; 
	if ($Navigation['Focus'] == '') $Navigation['Focus'] = 'ArticleColorId' ;
    }
    if ($Record['VariantSize']) {
	$n++ ;
	itemFieldRaw ('Size', formDBSelect ('ArticleSizeId', (int)$Record['ArticleSizeId'], sprintf ('SELECT ArticleSize.Id, ArticleSize.Name AS Value FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder', $Record['ArticleId']), 'width:70px')) ; 
	if ($Navigation['Focus'] == '') $Navigation['Focus'] = 'ArticleSizeId' ;
    }
    if ($Record['VariantCertificate']) {
	$n++ ;
        itemFieldRaw ('Certificate', formDBSelect ('ArticleCertificateId', (int)$Record['ArticleCertificateId'], sprintf ('SELECT ArticleCertificate.Id, CONCAT(Certificate.Name," (",CertificateType.Name,", ",DATE_FORMAT(Certificate.ValidUntil, "%%Y.%%m.%%d"),")") AS Value FROM ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Value', $Record['ArticleId']), 'width:250px')) ;
	if ($Navigation['Focus'] == '') $Navigation['Focus'] = 'ArticleCertificateId' ;
    }
    if ($Record['VariantDimension']) {
	$n++ ;
	itemFieldRaw ('Dimension', formText ('Dimension', $Record['Dimension'], 6) . ' cm') ; 
	if ($Navigation['Focus'] == '') $Navigation['Focus'] = 'Dimension' ;
    }
    if ($Record['VariantSortation']) {
	$n++ ;
	itemFieldRaw ('Sortation', formText ('Sortation', (int)$Record['Sortation'], 1)) ; 
 	if ($Navigation['Focus'] == '') $Navigation['Focus'] = 'Sortation' ;
    }
    if ($Record['BatchNumber']) {
	$n++ ;
	itemFieldRaw ('Batch', formText ('BatchNumber', $Record['BatchNumber'], 10)) ; 
	itemSpace() ;
    }
    if ($n == 0) {
	itemField ('No Variants') ; 
    }

    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
