<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    if ((int)$Record['Id'] > 0) {
	itemField ('Item', (int)$Record['Id']) ;
    }
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemField ('Stock', $Record['StockName']) ;
    itemField ('Container type', $Record['ContainerTypeName']) ;     
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Weight', formText ('Weight', '', 6) . ' kg (net.)') ; 
    itemFieldRaw ('Quantity', formText ('Quantity', '', 6) . ' ' . $Record['UnitName']) ; 
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
