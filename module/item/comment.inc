<?php

    require_once 'lib/form.inc' ;
    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemSpace () ;
    itemField ('Item', (int)$Record['Id']) ;
    itemField ('Article', sprintf ('%s (%s)', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemEnd () ;

    // Form
    formStart () ;
    itemStart () ;
    itemHeader() ;
    itemFieldRaw ('Comment', formTextArea ('Comment', $Record['Comment'], 'width:100%;height:150px;')) ;
    itemInfo ($Record) ;
    itemEnd () ;
    formEnd () ;

    return 0 ;
?>
