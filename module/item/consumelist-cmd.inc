<?php

    require_once 'lib/save.inc' ;
    require_once 'lib/table.inc' ;
    require_once 'lib/log.inc' ;
    require_once 'module/container/include.inc' ;

    // Initialization
    $Line = array () ;

    // Get fields header
    $fields = array (
	'ProductionId'		=> array ('type' => 'integer'),
    ) ;
    $res = saveFields (NULL, NULL, $fields) ;
    if ($fields['ProductionId']['value'] <= 0) return 'please select ProductionOrder to consume Items' ;
    if ($res) return $res ;

    // Get Production
    $query = sprintf ('SELECT Production.* FROM Production WHERE Production.Id=%d AND Production.Active=1', $fields['ProductionId']['value']) ;
    $result = dbQuery ($query) ;
    $Production = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ($Production['Id'] != $fields['ProductionId']['value']) return sprintf ('%s(%d) Production not found, id %d', __FILE__, __LINE__, $value) ;

    // Get Item and Quantity left for each line
    if (!is_array($_POST['Quantity'])) return 'no Items' ;
    foreach ($_POST['Quantity'] as $id => $s) {
	// Lookup Item
	$query = sprintf ('SELECT Item.*, Stock.Ready AS StockReady, Unit.Decimals AS UnitDecimals FROM Item LEFT JOIN Container ON Container.Id=Item.ContainerId LEFT JOIN Stock ON Stock.Id=Container.StockId     LEFT JOIN Article ON Article.Id=Item.ArticleId LEFT JOIN Unit ON Unit.Id=Article.UnitId WHERE Item.Id=%d AND Item.Active=1', $id) ;
	$res = dbQuery ($query) ;
	$row = dbFetch ($res) ;
	dbQueryFree ($res) ;
	if ((int)$row['Id'] != $id) return sprintf ('Item %d not found', $id) ;
	if ((int)$row['ContainerId'] <= 0) return sprintf ('Item %d does not exist any more') ;
	if ((int)$row['CaseId'] != (int)$Production['CaseId']) return sprintf ('Item %d not allocated to Case %d', (int)$id, (int)$Production['CaseId']) ;
	if ($line['Item']['StockReady']) return sprintf ('no operations allowed on Item %d in Container %d', $id, $line['Item']['ContainerId']) ;

	// Quantity
	$s = str_replace (',', '.',ltrim(trim($s), '0')) ;
	if ($s == '' or $s == '.') $s = '0' ;
	$format = sprintf ('(^[0-9]{1,10}$)|(^[0-9]{0,10}\.[0-9]{0,%d}$)', (int)$row['UnitDecimals']) ;
	if (!ereg($format,$s)) return sprintf ('invalid Quantity format for Item %d', (int)$id) ;
	$Quantity = (float)$s ;

	// Validate Quanity
	if ($Quantity > (float)$row['Quantity']) return sprintf ('Quantity left is too high for Item %d', (int)$id) ;
	
	// Allready specified
	if (isset($Line[(int)$id])) return sprintf ('Item %d specified twice', (int)$id) ;
	
	// Save
	$Line[(int)$id] = array ('Quantity' => $Quantity, 'Item' => $row) ;
    }

    // Anything selected
    if (count($Line) == 0) return 'no Items' ;

//logPrintVar ($Line, 'Line') ;
 
    // Do the opgrade
    foreach ($Line as $id => $line) {
	if ($line['Quantity'] >= (float)$line['Item']['Quantity']) continue ;

	if ($line['Quantity'] == 0) {
	    // The whole Quantity has been consumed
	    // Just update the Item
	    $Item = array (
		'ContainerId' => 0,
		'PreviousContainerId' => (int)$line['Item']['ContainerId'],
		'CaseId' => 0,
		'ConsumeProductionId' => (int)$Production['Id'],
		'ItemOperationId' => 0,
		'PreviousId' => 0
	    ) ;
	    tableWrite ('Item', $Item, (int)$id) ;	    
//	    logPrintVar ($Item, sprintf ('tableWrite, id %d', $id)) ;

	} else {
	    // Only a part of the Item has been consumed
	    // Update remaining Quantity for original Item
	    $Item = array (
		'Quantity' => number_format($line['Quantity'], (int)$line['Item']['UnitDecimals'], '.', '')
	    ) ;
	    tableWrite ('Item', $Item, $id) ;	    
//	    logPrintVar ($Item, sprintf ('tableWrite, id %d', $id)) ;

	    // Create new Item for the Quantity consumed
	    $Item = $line['Item'] ;
	    unset ($Item['StockReady']) ;
	    unset ($Item['UnitDecimals']) ;
	    $Item['Quantity'] = number_format((float)$line['Item']['Quantity'] - $line['Quantity'], (int)$line['Item']['UnitDecimals'], '.', '') ;
	    $Item['ContainerId'] = 0 ;
	    $Item['PreviousContainerId'] = (int)$line['Item']['ContainerId'] ;
	    $Item['CaseId'] = 0 ;
	    $Item['ConsumeProductionId'] = (int)$Production['Id'] ;
	    $Item['ItemOperationId'] = 0 ;
	    $Item['PreviousId'] = 0 ;
	    $Item['ParentId'] = (int)$id ;
	    if ($Item['TestDone']) {
		$Item['TestItemId'] = (int)$id ;
		unset ($Item['TestDone']) ;
		unset ($Item['TestDoneUserId']) ;
		unset ($Item['TestDoneDate']) ;
	    }

	    tableWrite ('Item', $Item) ;
//	    logPrintVar ($Item, sprintf ('tableWrite, new')) ;
	}

	containerDeleteEmpty ((int)$line['Item']['ContainerId']) ;
    }

    return 0 ;
?>
