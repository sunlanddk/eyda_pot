<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/form.inc' ;

    // Validate Article
    if (!$Record['ArticleTypeFabric']) return 'the Article must be of type Fabric' ;

    formStart () ;
 
    // Header fields
    itemStart () ;
    itemSpace () ;
    itemField ('Article', sprintf ('%s, %s', $Record['ArticleNumber'], $Record['ArticleDescription'])) ;
    itemSpace () ;
    itemHeader () ;

    // Batch
    itemFieldRaw ('Batch', formText ('BatchNumber', '', 10)) ;
    itemSpace () ;

    // Variants
    $n = 0 ;
    if ($Record['VariantColor']) {
	$n++ ;
	itemFieldRaw ('Colour', formDBSelect ('ArticleColorId', 0, sprintf ('SELECT ArticleColor.Id, CONCAT(Color.Number," (",Color.Description,")") AS Value FROM ArticleColor, Color WHERE ArticleColor.ArticleId=%d AND ArticleColor.Active=1 AND Color.Id=ArticleColor.ColorId ORDER BY Value', $Record['ArticleId']), 'width:200px')) ; 
    }
    if ($Record['VariantSize']) {
	$n++ ;
	itemFieldRaw ('Size', formDBSelect ('ArticleSizeId', 0, sprintf ('SELECT ArticleSize.Id, ArticleSize.Name AS Value FROM ArticleSize WHERE ArticleSize.ArticleId=%d AND ArticleSize.Active=1 ORDER BY ArticleSize.DisplayOrder', $Record['ArticleId']), 'width:70px')) ; 
    }
    if ($Record['VariantCertificate']) {
	$n++ ;
	itemFieldRaw ('Certificate', formDBSelect ('ArticleCertificateId', 0, sprintf ('SELECT ArticleCertificate.Id, CONCAT(Certificate.Name," (",CertificateType.Name,", ",DATE_FORMAT(Certificate.ValidUntil, "%%Y.%%m.%%d"),")") AS Value FROM ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId WHERE ArticleCertificate.ArticleId=%d AND ArticleCertificate.Active=1 ORDER BY Value', $Record['ArticleId']), 'width:250px', array (0 => 'none'))) ;
    }
    if ($Record['VariantDimension']) {
	$n++ ;
	itemFieldRaw ('Dimension', formText ('Dimension',  '', 4) . ' cm') ; 
    }
    if ($Record['VariantSortation']) {
	$n++ ;
	itemFieldRaw ('Sortation', formText ('Sortation', 1, 1)) ; 
    }
    if ($n > 0) {
    	itemSpace() ;
    }

    // Stock and Container
    itemFieldRaw ('Stock', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    itemFieldRaw ('Type', formDBSelect ('ContainerTypeId', 0, 'SELECT Id, Name AS Value FROM ContainerType WHERE Active=1 ORDER BY Value', 'width:150px;')) ;
    itemFieldRaw ('Position', formText ('Position', '', 20)) ;
    itemSpace () ;
    itemFieldRaw ('Comment', formTextArea ('Comment', '', 'width:100%;height:80px;')) ;
    itemSpace () ;
    itemFieldRaw (sprintf ('Use %s', $Record['UnitName']), formCheckbox ('Unit', 1) . ' (if not checked: Unit for Quantity is Kg.)') ;
    itemSpace () ;
    itemEnd () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHead ('No.', 80) ;
    listHead ('Case', 55, 'align=right') ;
    listHead ('Quantity', 60, 'align=right') ;
    listHead ('') ;

    for ($n = 1 ; $n <= 50 ; $n++) {
	listRow () ;
	listField ($n) ;
	listFieldRaw (formText (sprintf('Case[%d]', $n), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
	listFieldRaw (formText (sprintf('Quantity[%d]', $n), '', 6, 'text-align:right;width:46px;margin-right:0;'), 'align=right') ;
    } 
   
    listEnd () ;
    formEnd () ;
    
    return 0 ;
?>
