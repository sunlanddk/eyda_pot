<?php

    require_once 'lib/navigation.inc' ;

    $queryFields = '`Case`.*, Company.Id AS CustomerId, Company.Number AS CustomerNumber, Company.Name AS CustomerName, Article.Number AS ArticleNumber, Article.Description AS ArticleDescription' ;
    $queryTables = '(`Case` LEFT JOIN Article ON Article.Id=Case.ArticleId LEFT JOIN Company ON Company.Id=Case.CompanyId)' ;

    switch ($Navigation['Function']) {
	case 'list' :
	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Number',		'db' => 'Case.Id',	'desc' => true,		'direct' => 'Case.Active=1'),
		array ('name' => 'Description',		'db' => 'Case.Description'),
		array ('name' => 'Article',		'db' => 'Article.Number'),
		array ('name' => 'Article description',	'db' => 'Article.Description'),
		array ('name' => 'Reference',		'db' => 'Case.CustomerReference'),
		array ('name' => 'Customer',		'db' => 'Company.Number'),
		array ('name' => 'Name',		'db' => 'Company.Name'),
		array ('name' => 'Done',		'db' => 'Case.DoneDate')
	    ) ;

	    switch ($Navigation['Parameters']) {
	        case 'article' :
		    $query = sprintf ('SELECT * FROM Article WHERE Article.Id=%d AND Article.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $queryClause = sprintf ('Case.ArticleId=%d AND Case.Active=1', $Id) ;

		    // Navigation
		    navigationEnable ('new') ;
		    break ;

	        case 'company' :
		    $query = sprintf ('SELECT * FROM Company WHERE Company.Id=%d AND Company.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    $queryClause = sprintf ('Case.CompanyId=%d AND Case.Active=1', $Id) ;
		    break ;

        case 'season' :
		    $query = sprintf ('SELECT * FROM Season WHERE Season.Id=%d AND Season.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		   $queryFields = $queryFields . ', max(ccp.id) as CaseCostPriceId';
		    $queryTables = sprintf("
								   (select c.* from production p, `case` c where p.caseid=c.id and p.seasonid=%d and p.active=1 and c.active=1
									 union
									 select c.* from `Order` O, Orderline ol, `case` c where ol.orderid=o.Id and ol.active=1 and o.active=1 and o.seasonId=%d and ol.caseid=c.id group by c.id) `Case`
									LEFT JOIN Article ON Article.Id=`Case`.ArticleId
									LEFT JOIN Company ON Company.Id=`Case`.CompanyId
									LEFT JOIN casecostprice ccp ON ccp.caseid=`Case`.id"
								  ,$Id,$Id) ;
		    $queryClause = '1=1' ;

		    break ;

		default :
		    // Display
		    $HideDone = true ;

		    // Clause
		    $queryClause = 'Case.Done=0 AND Case.Active=1' ;
		    break ;
	    }

	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, 'Id', 'Id') ;
	    if (is_string($Result)) return $Result ;

	    $res = listView ($Result, 'caseview') ;
	    return $res ;

	case 'basic' :
	case 'basic-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    // Get Article
		    $query = sprintf ("SELECT Article.Id, Article.Number AS ArticleNumber FROM Article WHERE Article.Id=%d AND Article.Active=1", $Id) ;
		    $result = dbQuery ($query) ;
		    $Record = dbFetch ($result) ;
		    dbQueryFree ($result) ;
		    return 0 ;
	    }

	    // Fall through

	case 'detail' :
	case 'detail-cmd' :
	case 'delete-cmd' :
	case 'view' :
	    // Get Case
	    $query = sprintf ("SELECT %s FROM %s WHERE Case.Id=%d AND Case.Active=1", $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Navigation
	    if ($Record['Done']) {
		navigationPasive ('new') ;
	    }

	    break ;
    }

    return 0 ;
?>
