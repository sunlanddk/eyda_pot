<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/table.inc' ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    $n = $Record['ArticleNumber'] ;
	    unset ($Record) ;
	    $Record['ArticleNumber'] = $n ;
	    break ;

	default :
	    // Header
	    printf ("<br><table class=item>\n") ;
	    printf ("<tr><td class=itemlabel>Number</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Id']) ;
	    printf ("</table><br>\n") ;
	    break ;
    }
 
    function htmlFlag (&$Record, $field) {
	$s = sprintf ("<tr><td class=itemfield>%s</td>", $field) ;
	if ($Record[$field]) {
	    $s .= sprintf ("<td class=itemfield>%s, %s</td>", date("Y-m-d H:i:s", dbDateDecode($Record[$field.'Date'])), tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $Record[$field.'UserId'])) ;
	} else {
	    $s .= sprintf ("<td><input type=checkbox name=%s%s></td>", $field, ($Record[$field])?' checked':'') ;
	}
	$s .= "</tr>\n" ;
	return $s ;
    }

    // Form
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>\n", $Id) ;
    printf ("<input type=hidden name=nid>\n") ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Description</td><td><input type=text name=Description maxlength=100 style='width:100%%' value=\"%s\"></td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Article</td><td><input type=text name=ArticleNumber maxlength=10 size=10 value=\"%s\"></td></tr>\n", htmlentities($Record['ArticleNumber'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Customer</td><td>%s</td></tr>\n", htmlDBSelect ("CompanyId style='width:250px'", $Record['CompanyId'], 'SELECT Id, CONCAT(Name," (",Number,")") AS Value FROM Company WHERE TypeCustomer=1 AND Active=1 ORDER BY Name')) ;  
    printf ("<tr><td class=itemlabel>Reference</td><td><input type=text name=CustomerReference size=20 maxlength=20' value=\"%s\"></td></tr>\n", htmlentities($Record['CustomerReference'])) ;
    if ($Record['Id'] > 0) {
	print htmlItemSpace() ;
	print htmlFlag ($Record, 'Done') ;
    }	
    print (htmlItemInfo ($Record)) ;
    printf ("</table>\n") ;
    
    printf ("</form>\n") ;

    return 0 ;
?>
