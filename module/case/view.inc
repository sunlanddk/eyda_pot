<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

    // Header
    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Number</td><td class=itemfield>%d</td></tr>\n", (int)$Record['Id']) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['Description'])) ;
    print htmlItemSpace() ;
    itemFieldIcon ('Article', $Record['ArticleNumber'], 'article.gif', 'articleview', $Record['ArticleId']) ;
    printf ("<tr><td class=itemlabel>Description</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['ArticleDescription'])) ;
    print htmlItemSpace() ;
    if ($Record['ArticleCertificateId'] > 0) {
	itemField ('Certificate', tableGetFieldWhere ('ArticleCertificate LEFT JOIN Certificate ON Certificate.Id=ArticleCertificate.CertificateId AND Certificate.Active=1 LEFT JOIN CertificateType ON CertificateType.Id=Certificate.CertificateTypeId', 'CONCAT(Certificate.Name," (",CertificateType.Name,", ",DATE_FORMAT(Certificate.ValidUntil, "%Y.%m.%d"),")")', sprintf ('ArticleCertificate.Id=%d AND ArticleCertificate.Active=1', $Record['ArticleCertificateId']))) ; 
    } else {
	itemField ('Certificate', 'None') ;  
    }
    itemSpace() ;
    itemFieldIcon ('Customer', $Record['CustomerNumber'], 'company.gif', 'companyview', $Record['CustomerId']) ;
    printf ("<tr><td class=itemlabel>Name</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['CustomerName'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Reference</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['CustomerReference'])) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Comment</td><td><p style='padding-right:8px;'>%s</p></td></tr>\n", str_replace('  ', '&nbsp;&nbsp;', nl2br(htmlentities($Record['Comment'])))) ;
    print htmlItemSpace() ;
    function htmlFlag (&$row, $field) {
	require_once 'lib/table.inc' ;
	printf ("<tr><td class=itemlabel>%s</td><td class=itemfield>%s</td></tr>", $field, ($row[$field]) ? date("Y-m-d H:i:s", dbDateDecode($row[$field.'Date'])) . ', ' . tableGetField ('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', $row[$field.'UserId']) :'no') ;
    }
    htmlFlag ($Record, 'Done') ;
    print htmlItemInfo($Record) ;
    printf ("</table>\n") ;

    return 0 ;
?>
