<?php

    require_once 'lib/table.inc' ;

    // Ensure no ProductionOrders attached
    $query = sprintf ("SELECT Id FROM Production WHERE CaseId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "can not be deleted when ProductionOrders are attached" ;

    // Ensure no Order
    $query = sprintf ("SELECT Id FROM OrderLine WHERE CaseId=%d AND Active=1", $Id) ;
    $result = dbQuery ($query) ;
    $count = dbNumRows ($result) ;
    dbQueryFree ($result) ;
    if ($count > 0) return "can not be deleted when Orders are attached" ;
    
    tableDelete ('Case', $Id) ;
    
    return 0
?>
