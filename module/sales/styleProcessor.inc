<?php
require_once  "lib/list.inc" ;
require_once  "lib/table.inc" ;
require_once  "lib/db.inc";
require_once "module/article/include.inc" ;

class StyleProcessor {
	
	private $style;
	private $skipErrors = false;
	private $error_raised = false;
	private $error_message;

    private $created_articles_count = 0;
    private $reused_articles_count = 0;
    private $created_collection_members_count = 0;

	function __construct($style1, $skipErrors = false){
		$this->style = $style1;
		$this->skipErrors = $skipErrors;
	}
	
	public function process(){
	    try {
	       bdStartTransaction();
	       $style = $this->style;

			// Ken
			switch ($style['state']) {
			  case 'Deleted':
			  case 'Cancelled':
			  case 'Unpublished':
			    return ;
			  default:
			    break ;
			}
		   
	       $company_id = $this->process_supplier_company($style['supplier']['name']);
			// Ken 	       $programId = $this->process_object_by_name('program', $style['group']['name']);
			$season_id = tableGetFieldWhere('season', 'Id', sprintf("Active=1 AND Name='%s' AND CompanyId=787",  $style['season']['name'])) ;
			if (empty($season_id))
				throw new Exception(sprintf("Season: %s not found.", $style['season']['name']) );
			$programId = $this->process_program('program', $style['group']['name'], $season_id);
	       
	       $colors = $this->process_colors($style['styleColors']);

			unset($article_id) ;
		   // Process article
			if(!empty($style['id'])) { 
				$article_id = tableGetFieldWhere('article', 'Id', sprintf("Active=1 AND ImportIdRef=%d", (int)$style['id']) );
				// resuse if already imported before otherwise use articlenumber syntax to create new
				if(empty($article_id)){ 
					if(strlen($style['number'])>1) {
						$a_number = $style['number'] ;
						$article_id = tableGetFieldWhere('article', 'Id', sprintf("Active=1 AND Number=%s", $style['number']) );
					} else {
						$a_number = substr($style['categories'][0]['name'], 0, 4);
					}
					$res = articleNumberFill ($a_number) ;
					if(empty($a_number) || !is_numeric($a_number) || empty($style['name']) || is_string ($res)){
						throw new Exception(sprintf("Wrong format of style number: %s or Name: %s state: %s.", $a_number, $style['name'], $style['state']) );
					}			   
				} else {
					$a_number = tableGetFieldWhere('article', 'Number', sprintf("Active=1 AND ImportIdRef=%d", (int)$style['id']) );
					unset($article_id) ;
					$n = (int)substr($a_number, 8,2) ;
					$n++ ;			    
					$a_number = sprintf ('%s%02d', substr($a_number, 0,8), $n) ;
					unset($article_id) ;
				}
			} else {
				throw new Exception(sprintf("Style id missing for: %s, Name: %s, state: %s.", $style['number'], $style['name'], $style['state']) );
			}
			
			$style['number'] = $a_number ;
            if(!empty($article_id)){
				$this->reused_articles_count++;
            } else {
				$this->created_articles_count++;
				$Article = array (
					'Number' => $a_number,
					'Description'=> $style['name'] ,
					'SupplierCompanyId'=> $company_id,
					'ImportIdRef'=> $style['id'],
					'ArticleTypeId' => 1 ,
					'VariantColor' => 1 ,
					'VariantSize' => 1 ,
					'VariantSortation' => 1 
				);
				$article_id = tableWrite ('article', $Article, $article_id);
            }

           $this->process_article_colors_coll_members_programs($article_id, $programId, $style, $colors);
           $this->process_article_components($article_id, $style['description']);
           $this->process_article_size_range($article_id, $style['sizeRange']);

	       dbCommit();
	    } catch (Exception $e) {
	       $this->error_raised = true;
	       $this->error_message = $e->getMessage();
	       dbRollBack();
	    }

	}
	
	private function process_article_num(){
		// Process article
		if(!empty($style['id'])) {
			$article_id = tableGetFieldWhere('article', 'Id', sprintf("Active=1 AND ImportIdRef=%d", (int)$style['id']) );
			// resuse if imported before otherwise use articlenumber syntax to create
			if(!empty($article_id)){
				if(empty($style['number'])) {
					$a_number = substr($style['categories'][0]['name'], 0, 4);
				} else {
					$a_number = $style['number'] ;
					$article_id = tableGetFieldWhere('article', 'Id', sprintf("Active=1 AND Number=%s", $style['number']) );
				}
				$res = articleNumberFill ($a_number) ;
				if(empty($a_number) || !is_numeric($a_number) || empty($style['name']) || is_string ($res)){
					throw new Exception(sprintf("Wrong format of style number: %s or Name: %s state: %s.", $a_number, $style['name'], $style['state']) );
				}
			}
		} else {
			throw new Exception(sprintf("Style id missing for: %s, Name: %s, state: %s.", $style['number'], $style['name'], $style['state']) );
		}
		if(!empty($article_id)){
			$this->reused_articles_count++;
		} else {
			$this->created_articles_count++;
			$Article = array (
					'Number' => $a_number,
					'Description'=> $style['name'],
					'SupplierCompanyId'=> $company_id,
					'ImportIdRef'=> $style['id'],
					'ArticleTypeId' => 1 ,
					'VariantColor' => 1 ,
					'VariantSize' => 1 ,
					'VariantSortation' => 1
			);
			$article_id = tableWrite ('article', $Article, $article_id);
		}
	}
	
	public function get_error_raised(){
	   return $this->error_raised;
	}
	
	public function get_error_message(){
	   return $this->error_message;
	}

    public function get_created_articles_count() {
        return $this->created_articles_count;
    }

    public function get_reused_articles_count() {
        return $this->reused_articles_count;
    }

    public function get_created_collection_members_count() {
        return $this->created_collection_members_count;
    }
	
	private function process_article_colors_coll_members_programs($articleId, $programId, $style, $colors){
		foreach($colors as $color) {
			$article_color_id = $this->process_article_color($articleId, $color['Id']);
			$this->copy_style_image($style['number'], $color['Number'], $style['pictureUrl']);
			$collMemberId = $this->process_article_collection_member($articleId, $article_color_id, $style);
			$this->process_coll_member_program($collMemberId, $programId);	
		}
	}
	
	private function process_article_collection_member($articleId, $articleColorId, $style){
        $brand_name = $style['brand']['name'];
        $brand_id = tableGetFieldWhere('brand', 'Id', sprintf("Active=1 AND Name='%s'", $brand_name));
        if (empty($brand_id)) {
            throw new Exception(sprintf("There is no '%s' brand in database", $brand_name));
        }
        $season_name = $style['season']['name'];
        $season_id = tableGetFieldWhere('season', 'Id', sprintf("Active=1 AND Name='%s' AND CompanyId=787", $season_name));
        if (empty($season_id)) {
            throw new Exception(sprintf("There is no %s season in database", $season_name));
        }

        $collection_id = tableGetFieldWhere('collection', 'Id',
            sprintf("Active=1 AND SeasonId=%d AND BrandId=%d", $season_id, $brand_id));

        if (empty($collection_id)) {
            throw new Exception(sprintf("There is no collection which corresponds to %s brand and %s season in database %d - %d - %d",
                $brand_name, $season_name,$collection_id, $season_id,$brand_id));
        }

        // checking if such collection member already exists
        $collection_member_id = tableGetFieldWhere('collectionmember', 'Id',
            sprintf("Active=1 AND ArticleId=%d AND ArticleColorId=%d AND CollectionId=%d", $articleId, $articleColorId, $collection_id));

        if (empty($collection_member_id)) {
            $collection_member_record = array (
                'ArticleId' => $articleId,
                'ArticleColorId' => $articleColorId,
                'CollectionId' => $collection_id
            );

            $collection_member_id = tableWrite('collectionmember', $collection_member_record);
            $this->created_collection_members_count++;

        }

        return $collection_member_id;
	}
	
	private function process_article_color($articleId, $colorId){
		$id = tableGetFieldWhere('articlecolor', 'Id', sprintf("Active=1 AND ArticleId=%d AND ColorId=%d", $articleId, $colorId) );
		$Record = array (
				'ArticleId' =>	$articleId,
				'ColorId'=> $colorId
		);
		return tableWrite ('articlecolor', $Record, $id);
	}
	
	private function get_image_ext_from_url($img_url){
		if(empty($img_url)){
			throw new Exception("Bad Image Url");
		}
		return strrchr($img_url, '.');
	}
	
	private function process_colors($colors){
		$res = array();
		// parse colors
		foreach ($colors as $styleColor) {
//			throw new Exception("Color status " . $styleColor['status']);
//			if ($styleColor['status']=='Active') {
				$color = $this->process_color($styleColor['name']);
				array_push($res, $color);
//			}
		}
		return $res;
	}
	private function process_color($color){
		$number = substr($color, 0, 9);
		$desc = substr($color, 6);
		if(empty($number) || !is_numeric($number) || empty($desc)){
			throw new Exception(sprintf("Wrong format of Style Color Name: %s.", $color) );
		}
		
		$colorId = tableGetFieldWhere('color', 'Id', sprintf("Active=1 AND Number='%s'", $number) );
		if(empty($colorId)){
			throw new Exception(sprintf("Cannot retrieve Color: %s.", $color) );
		}
		$Color['Number'] = $number;
		$Color['Id'] = $colorId;
		return $Color;
		
	}

	private function process_color_modif($color){
		$number = substr($color, 0, 6);
		$desc = substr($color, 6);
		if(empty($number) || !is_numeric($number) || empty($desc)){
			throw new Exception(sprintf("Wrong format of Style Color Name: %s.", $color) );
		}
		$number = '0'.$number.'00';
		
		$colorId = tableGetFieldWhere('color', 'Id', sprintf("Active=1 AND Number='%s'", $number) );
		$Color = array (
				'Number' =>		$number,
				//'ColorGroupId'=> ? ,
				'Description'=> $desc
		);
		$colorId = tableWrite ('color', $Color, $colorId);
		if(empty($colorId)){
			throw new Exception(sprintf("Cannot retrieve Color: %s.", $color) );
		}
		$Color['Id'] = $colorId;
		return $Color;
		
	}
	
	private function process_object_by_name($table, $name){
		$id = tableGetFieldWhere($table, 'Id', sprintf("Active=1 AND Name='%s'", $name) );
		$Record = array (
				'Name' =>	$name
		);
		return tableWrite ($table, $Record, $id);
	}
	
	private function process_program($table, $name, $seasonid){ //Ken
		$id = tableGetFieldWhere($table, 'Id', sprintf("Active=1 AND Name='%s' AND Seasonid=%d", $name,  $seasonid) );
		$Record = array (
				'Name' =>	$name,
				'SeasonId' =>	$seasonid
		);
		return tableWrite ($table, $Record, $id);
	}
	
	private function process_article_component($article_id, $component_id, $percent){
		$id = tableGetFieldWhere('articlecomponent', 'Id', sprintf("Active=1 AND ArticleId=%d AND ComponentId=%d", $article_id, $component_id) );
		$Record = array (
				'ArticleId' =>	$article_id,
				'ComponentId' => $component_id,
				'Percent' =>	$percent
		);
		return tableWrite ('articlecomponent', $Record, $id);
	}
	
	private function process_coll_member_program($coll_member_id, $program_id){
		$id = tableGetFieldWhere('collectionmemberprogram', 'Id', sprintf("Active=1 AND CollectionmemberId=%d AND ProgramId=%d", $coll_member_id, $program_id) );
		$Record = array (
				'CollectionmemberId' =>	$coll_member_id,
				'ProgramId' =>	$program_id
		);
		return tableWrite ('collectionmemberprogram', $Record, $id);
	}
	
	private function process_supplier_company($company_name){
		$id = tableGetFieldWhere('company', 'Id', sprintf("Active=1 AND Name='%s'", $company_name) );
		if(empty($id)){
			throw new Exception(sprintf("Cannot find Company %s.", $company_name), 1 );
		}
		return $id;
	}
	
	private function process_category($category, $articleNumber){
		$cat = tableGetFieldWhere('articlecategory', 'Category', sprintf("Name='%s'", $category) );
		if(empty($cat)){
			throw new Exception(sprintf("Cannot retrieve articlecategory: %s.", $category));
		}
		$num_substr = substr($articleNumber, 1, 3);
		if($cat != $num_substr){
			throw new Exception(sprintf("Wrong category %s for article number %s.", $cat, $articleNumber));
		}
		
		return $cat;
	}
	
	private function copy_style_image($style_numb, $color_numb, $img_url){
		global $Config;
		$img_ext = $this->get_image_ext_from_url($img_url);
		if($img_ext == ".jpg" || $img_ext == ".png"){
			$path = $Config['imageStore'].'\thumbnails\\'.$style_numb .'_'.$color_numb.'.jpg';
			// If allow_url_fopen set to true:
			$ch = curl_init();
			$fp = fopen($path, 'wb');
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
			curl_setopt($ch, CURLOPT_HEADER, false);
			curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($ch, CURLOPT_URL, $img_url);
			curl_setopt($ch, CURLOPT_REFERER, $img_url);
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_exec($ch);
			curl_close($ch);
			fclose($fp);
		} else {
			throw new Exception(sprintf("Wrong Image type of %s.", $img_ext) );
		}
	}
	
	private function process_article_components($article_id, $value){
		$vals = explode(" ", $value);
		$names = explode("/", $vals[0]);
		$percents = explode("/", $vals[1]);
		if (!is_numeric($percents[0])) return ; // Ken
		for($i=0; $i<count($names) ; $i++){
			$name = $names[$i];
			$percent = $percents[$i];
			$component_id = $this->process_object_by_name('component', $name);
			$this->process_article_component($article_id, $component_id, $percent);
		}
	}
	
	private function process_article_size_range($article_id, $sizeRange){
	   if(!empty($sizeRange) && !empty($sizeRange['sizes'])){
	      foreach ($sizeRange['sizes'] as $size){
			 $id = tableGetFieldWhere('articlesize', 'Id', sprintf("Active=1 AND Name='%s' AND ArticleId=%d", $size['name'], $article_id) );
			 if(empty($id)){
				$displayOrder = tableGetFieldWhere('articlesize', 'IFNULL(MAX(DisplayOrder),0)+1', sprintf("ArticleId=%d", $article_id) );
				$id = tableWrite ('articlesize', array('Name' => $size['name'], 'ArticleId' => $article_id, 'DisplayOrder' => $displayOrder), $id);
			 }else{
				$id = tableWrite ('articlesize', array('Name' => $size['name'], 'ArticleId' => $article_id), $id);
			 }
	      }      
	   }
	}
	
}

return 0;
?>