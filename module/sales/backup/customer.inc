<?php
	require_once 'lib/fieldtype.inc' ;
	require_once 'lib/list.inc' ;
	require_once 'lib/array.inc' ;
	require_once 'lib/lookups.inc' ;
	
   class Customer {
   	  private $_user;

   	  public function __construct(){
   	  	global $User;
   	  	$this->_user = $User;
   	  }

      public function create() {
         printf('Ajax -> Customer Create();');
         return 0;
      }

      public function get_all_list() {
		global $nid, $smarty, $filterValue, $listSort, $totalRecords;

		$fields = $this->getFields();
	    $queryFields = $this->getQueryFields();
	    $queryTables = $this->getQueryTables();
	    $whereClause = $this->getWhereClause();
	    
	    $result = listLoad ($fields, $queryFields, $queryTables, $whereClause, '', 'c.id, cur.Id', true);

	    $items = array();
    	while ($row = dbFetch($result)) {
    		array_push($items, $row);
    	}
    	dbQueryFree($result);

    	//Displaying filtering toolbar
    	listFilterBar ();

    	$total_pages = ceil($totalRecords/LINES);
    	$current_page = 1;
		if ($_GET['offset'] > 0) {
    		$current_page = ($_GET['offset']+LINES)/LINES;
    	}
    	
    	$headers = array_key_values($fields, 'name', array('noheader' => 1));

    	//Assigning values to template engine and preparing HTML output
    	$smarty->assign(array(
    	   //params for list
      	   'listItems'    => $items,
    	   'headers'      => $headers,
 	       'styles'       => array_assoc_key_values($fields, 'name', 'class'),
    	   'sort'         => $listSort,
    	   'itemLinkMark' => 'customerview',

    	   //params for paging
    	   'overall_pages'  => $total_pages,
    	   'current_page'   => $current_page,
    	   'lines_per_page' => LINES,
    	
    	   //params for totals
    	   'total' => array(array_search('Ordered', $headers) => $this->get_total_ordered($whereClause))
        ));

        $smarty->display('lib/paging_bar.tpl');
        $smarty->display('modules/sales/order_list.tpl');
        $smarty->display('lib/paging_bar.tpl');

      	return 0;
      }

      public function get_prospects() {
		global $nid, $smarty, $filterValue, $listSort, $totalRecords;

		$fields = $this->getFields();
	    $queryFields = $this->getQueryFields();
	    $queryTables = $this->getQueryTables();
	    $whereClause = $this->getWhereClause(true);

	    $result = listLoad ($fields, $queryFields, $queryTables, $whereClause, '', 'c.id, cur.Id', true);

	    $items = array();
    	while ($row = dbFetch($result)) {
    		array_push($items, $row);
    	}
    	dbQueryFree($result);

    	//Displaying filtering toolbar
    	listFilterBar ();

    	$total_pages = ceil($totalRecords/LINES);
    	$current_page = 1;
		if ($_GET['offset'] > 0) {
    		$current_page = ($_GET['offset']+LINES)/LINES;
    	}
    	
    	$headers = array_key_values($fields, 'name', array('noheader' => 1));

    	//Assigning values to template engine and preparing HTML output
    	$smarty->assign(array(
    	   //params for list
      	   'listItems'    => $items,
    	   'headers'      => $headers,
 	       'styles'       => array_assoc_key_values($fields, 'name', 'class'),
    	   'sort'         => $listSort,
    	   'itemLinkMark' => 'customerview',

    	   //params for paging
    	   'overall_pages'  => $total_pages,
    	   'current_page'   => $current_page,
    	   'lines_per_page' => LINES,
    	
    	   //params for totals
    	   'total' => array(array_search('Ordered', $headers) => $this->get_total_ordered($whereClause))
        ));

        $smarty->display('lib/paging_bar.tpl');
        $smarty->display('modules/sales/order_list.tpl');
        $smarty->display('lib/paging_bar.tpl');

      	return 0;
      }

      private function get_seasons() {
      	 $result = mysql_query('SELECT Id AS Id, Name AS Value FROM season where Active=1 order by Name');
		 $items = array();
	     while ($row = mysql_fetch_assoc($result)) {
	     	$items[$row['Id']] = $row['Value'];
	     }
		 mysql_free_result ($result);
		 return $items;
      }
      
      public function download(){
      	require_once 'PEAR/Spreadsheet/Excel/Writer.php';

      	$fields = array (
			NULL, // index 0 reserved
			array ('name' => 'Customer', 		'db' => 'c.name'),
            array ('name' => 'Contact',   'db' => 'c.ContactPerson'),
			array ('name' => 'Address1', 		'db' => 'c.Address1', 'style' => array('Border' => 1, 'Align' => 'right')),
			array ('name' => 'Address2', 		'db' => 'c.Address2', 'style' => array('Border' => 1, 'Align' => 'right')),
			array ('name' => 'Zip code', 		'db' => 'c.ZIP', 'style' => array('Border' => 1, 'Align' => 'right')),
			array ('name' => 'City',     		'db' => 'c.City'),
			array ('name' => 'Country',  		'db' => 'country.description'),
			array ('name' => 'Phone',     		'db' => 'c.PhoneMain'),
			array ('name' => 'Email',     		'db' => 'c.Mail'),
            array ('name' => 'CompanyNumber',   'db' => 'c.Number'),
            array ('name' => 'Sales Person', 	'db' => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")'),
			array ('name' => 'Prospect',   		'db' => 'if(c.Prospect>0,"Prospect","")'),
			array ('name' => 'Styles',   		'db' => 'COUNT(DISTINCT ol.ArticleId, ol.ArticleColorId)',   'class' => 'right', 'nofilter'=>true, 	'style' => array('Border' => 1, 'Align' => 'right')),
			array ('name' => 'Quantity', 		'db' => 'IFNULL(SUM(oq.quantity), 0)',   'class' => 'right', 'nofilter'=>true, 'style' => array('Border' => 1, 'Align' => 'right')),
            array ('name' => 'Ordered', 		'db' => 'SUM(oq.quantity*ol.PriceSale*(1-ol.Discount/100))*cur.Rate', 'nofilter'=>true, 'class'=>'right', 'style' => array('Border' => 1, 'Align' => 'right', 'NumFormat' => '# ##0.00')),
//			array ('name' => 'Currency', 		'db' => 'cur.id', 'type' => FieldType::SELECT, 'options' => getCurrencies(), 'style' => array('Border' => 1, 'Align' => 'right'))
			array ('name' => 'Currency',     		'db' => 'cur.Name'),
	    );
	    $queryFields = "c.Id as Number, c.Number as CompanyNumber, c.name AS Customer, c.ContactPerson as Contact, c.Mail as Email, c.PhoneMain as Phone, if(c.Prospect>0,'Prospect','') as Prospect, country.description AS Country, c.City AS City, COUNT(DISTINCT ol.ArticleId, ol.ArticleColorId) AS `Styles`,"
              	." REPLACE(FORMAT(IFNULL(SUM(oq.quantity), 0), 0), ',', ' ') AS Quantity, c.ZIP AS `Zip code`, c.Address1 AS `Address1`, c.Address2 AS `Address2`, cur.Name AS Currency,"
	          	." SUM(oq.quantity*ol.PriceSale*(1-ol.Discount/100)) AS Ordered,"
                ." CONCAT(user.FirstName,' ',user.LastName,'(', user.Loginname,')') AS 'Sales Person'";
	    $queryTables = $this->getQueryTables();
	    $whereClause = $this->getWhereClause();
	    
	    $result = listLoad ($fields, $queryFields, $queryTables, $whereClause, '', 'c.id, cur.Id');
	    
	    $headers = array_key_values($fields, 'name', array('noheader' => 1));
	    $styles = array_assoc_key_values($fields, 'name', 'style');
	    
		// Creating a workbook
		$workbook = new Spreadsheet_Excel_Writer();
		
		$format_title = $workbook->addFormat(array('Align' => 'center', 'Bold' => 700, 'Border' => 1));
		$format_cell = $workbook->addFormat(array('Border' => 1));
		
		// sending HTTP headers
		$workbook->send('Customer.xls');
		
		// Creating a worksheet
		$worksheet =& $workbook->addWorksheet('Customer');
		
		$worksheet->setColumn(0,0,50);
		$worksheet->setColumn(1,count($headers)-1,20);
		
		// Captions
		foreach ($headers as $k =>  $header){
			$worksheet->write(0, $k, $header, $format_title);
			$styles[$header] = isset($styles[$header]) ? $workbook->addFormat($styles[$header]) : $format_cell;
		}

		// The data
		$i=1;
		while ($row = dbFetch($result)) {
			foreach ($headers as $k =>  $header){
				$worksheet->write($i, $k, $row[$header], $styles[$header]);
			}
			$i+=1;
		}
		dbQueryFree($result);
		
		// Let's send the file
		$workbook->close();
      }

      private function get_total_ordered($whereClause = '1 = 1') {
      	global $listSettings;
		global $User;
      	
      	$currency_filter = $listSettings['filter']['Currency'];
      	$select_clause = 'SELECT REPLACE(REPLACE(FORMAT(SUM(oq.Quantity*ol.PriceSale*(1 - ol.Discount/100)*(cur.Rate/100)), 2), \",\", \" \"), \".\", \",\") AS \'total price\', \'EUR\' AS Currency';
      	
      	if (isset($currency_filter) && $currency_filter['id'] > 0) {
            $select_clause = 'SELECT REPLACE(REPLACE(FORMAT(SUM(oq.Quantity*ol.PriceSale*(1 - ol.Discount/100)), 2), ",", " "), ".", ",") AS \'total price\', max(cur.Name) AS Currency';
			$whereClause .= sprintf(' AND cur.Id = %s', $currency_filter['id']);
			$currency = $currency_filter;
      	} else if ($User['SalesRef']==1 and $User['AgentCompanyId']>0) {
			$query = 'select Currency.Name, Currency.Rate from `Currency`, Company where Currency.Id=Company.CurrencyId and Company.Id=' . $User['AgentCompanyId'];
			$result = mysql_query($query);
			if ($row = mysql_fetch_assoc($result)) {
				$_curency_rate = $row['Rate'];
				$_curency_name = $row['Name'];
			}
			mysql_free_result ($result);
			
			if ($_curency_rate > 0)
				$scr=1 ;
			else {
				$_curency_rate = 100 ;
				$_curency_name = 'EUR';
			}

			$select_clause = 'SELECT REPLACE(REPLACE(FORMAT(SUM(oq.Quantity*ol.PriceSale*(1 - ol.Discount/100)*(cur.Rate/'
							. $_curency_rate
							. ')), 2), ",", " "), ".", ",") AS \'total price\', \'' 
							. $_curency_name 
							. '\' AS Currency';

		}
      	 
      	$query = sprintf('%s FROM company c
								LEFT JOIN `order` o    ON o.companyId = c.id AND o.active=1 AND o.done=0
								LEFT JOIN orderline ol ON ol.orderId = o.Id AND ol.active=1 AND ol.done=0
								LEFT JOIN orderquantity oq ON oq.OrderLineId = ol.Id AND oq.Active = 1
								LEFT JOIN country      ON country.Id = c.CountryId
								LEFT JOIN currency cur ON cur.Id = o.currencyId 
								WHERE %s', $select_clause, $whereClause);
      	$result = dbRead(stripcslashes($query));
      	return sprintf('%s %s', $result[0]['total price'], $result[0]['Currency']);
      }
      private function getFields(){
      	return array (
			NULL, // index 0 reserved
			array ('name' => 'Customer', 		'db' => 'c.name'),
            array ('name' => 'CompanyNumber',   'db' => 'c.Number'),
            array ('name' => 'Sales Person', 	'db' => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")'),
			array ('name' => 'Zip code', 		'db' => 'c.ZIP'),
			array ('name' => 'City',     		'db' => 'c.City'),
			array ('name' => 'Country',  		'db' => 'country.description'),
			array ('name' => 'Styles',   		'db' => 'COUNT(DISTINCT ol.ArticleId, ol.ArticleColorId)',   'class' => 'right', 'nofilter'=>true),
			array ('name' => 'Quantity', 		'db' => 'IFNULL(SUM(oq.quantity), 0)',   'class' => 'right', 'nofilter'=>true),
            array ('name' => 'Ordered', 		'db' => 'SUM(oq.quantity*ol.PriceSale*(1-ol.Discount/100))*cur.Rate', 'nofilter'=>true, 'class'=>'right'),
			array ('name' => 'Currency', 		'db' => 'cur.id', 'noheader'=>true, 'type' => FieldType::SELECT, 'options' => getCurrencies())
	    );
      }
      
      private function getQueryFields(){
      	return "c.Id as Number, c.Number as CompanyNumber, c.name AS Customer, country.description AS Country, c.City AS City, COUNT(DISTINCT ol.ArticleId, ol.ArticleColorId) AS `Styles`,"
              	." REPLACE(FORMAT(IFNULL(SUM(oq.quantity), 0), 0), ',', ' ') AS Quantity, c.ZIP AS `Zip code`, cur.Name AS Currency,"
	          	." CONCAT(REPLACE(REPLACE(FORMAT(SUM(oq.quantity*ol.PriceSale*(1-ol.Discount/100)), 2), \",\", \" \"), \".\", \",\"), \" \", cur.Name) AS Ordered,"
                ." CONCAT(user.FirstName,' ',user.LastName,'(', user.Loginname,')') AS 'Sales Person'";
      }
      private function getQueryTables(){
      	return "company c"
				." LEFT JOIN `order` o    ON o.companyId = c.id AND o.active=1 AND o.done=0"
				." LEFT JOIN orderline ol ON ol.orderId = o.Id AND ol.active=1 AND ol.done=0"
                ." LEFT JOIN orderquantity oq ON oq.OrderLineId = ol.Id AND oq.Active = 1"
				." LEFT JOIN country      ON country.Id = c.CountryId"
				." LEFT JOIN currency cur ON cur.Id = o.currencyId"
                ." LEFT JOIN user         ON user.Id = c.SalesUserId ";
      }
      private function getWhereClause($onlyProspects=false) {
      	$whereClause = sprintf('c.typecustomer=1 AND c.listhidden=0 AND c.Active = 1 AND c.ToCompanyId = %d', $this->_user['CompanyId']);
      	if ($onlyProspects) {
      		$whereClause .= ' AND c.Prospect=1';
      	}
      	if ( $this->_user['SalesRef']) {
			if ( $this->_user['AgentManager']) {
				$whereClause .= sprintf(' AND c.AgentId=%d', $this->_user['AgentCompanyId']) ;
			} else {
				$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
				$whereClause .= sprintf(' AND c.SalesUserId=%d', $_userid) ;
			}
      	}
      	return $whereClause;
      }
   }

   return 0;
?>