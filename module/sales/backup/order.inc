<?php
   require_once 'lib/fieldtype.inc';
   require_once 'lib/list.inc';
   require_once 'lib/array.inc';
   require_once 'lib/lookups.inc';
   require_once 'lib/currency.inc';

   class Order {
      private $_user;

      public function __construct() {
         global $User;
         $this->_user = $User;
      }

      public function get_shipped_list() {
         global $nid, $smarty, $filterValue, $listSort, $totalRecords;

         $fields = array(
            NULL,
            // index 0 reserved
            array('name'   => 'Number',
                  'db'     => '`Order`.Id',
                  'desc'   => true,
                  'direct' => 'Order.Active=1',
                  'type'   => FieldType::INTEGER),
            array('name' => 'Company',
                  'db'   => 'CONCAT(Company.Name," (",Company.Number,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Country',
                  'db'   => 'Country.Description',
                  'type' => FieldType::STRING),
            array('name'    => 'Season',
                  'db'      => 'Season.Id',
                  'type'    => FieldType::SELECT,
                  'options' => getOwnerSeasons($this->_user['CompanyId'])),
            array('name' => 'Reference',
                  'db'   => 'Order.Reference',
                  'type' => FieldType::STRING),
            array('name' => 'Description',
                  'db'   => 'Order.Description',
                  'type' => FieldType::STRING),
            array('name'     => 'Discount',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name'     => 'Total Price',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name' => 'Sales Person',
                  'db'   => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Created',
                  'db'   => 'Order.CreateDate',
                  'type' => FieldType::DATE),
            array('name'     => 'ReadyDate',
                  'db'       => '`ORDER`.ReadyDate',
                  'nofilter' => true,
                  'type'     => FieldType::DATE),
            array('name'      => 'ReadyDateFrom',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '>=',
                  'type'      => FieldType::DATE),
            array('name'      => 'ReadyDateTo',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '<=',
                  'type'      => FieldType::DATE));

         $queryFields = "Order.Id AS Number,
	                    Company.Number AS \"Company Number\",
	                    Company.Name AS Company,
	                    Country.Description AS Country,
      					Season.Name AS \"Season\",
      					Order.Reference AS Reference,
      					Order.Description AS Description,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\", `Order`.CreateDate AS Created,
      					SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'Total Price',
      					Currency.Name as CurrencyName, `Order`.Discount AS Discount,
                        IF(UNIX_TIMESTAMP(Order.ReadyDate)=0, '', Order.ReadyDate) as ReadyDate";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN Country ON Country.Id=Company.CountryId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
      					LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					LEFT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
			$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
			$companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d)', $_userid, $_userid);
         }
         if ($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

         $queryClause = sprintf("`ORDER`.Active=1 AND `ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE '%%finished%%') %s", $companyClause);

         $result = listLoad($fields, $queryFields, $queryTables, $queryClause, '', '`Order`.Id', true);

         $items = array();
         while ($row = dbFetch($result)) {
            $row['Total Price'] = float_to_currency($row["Total Price"]) . " " . $row["CurrencyName"];
            $row['Discount']    = $row['Discount'] . "%";
            $row['Created']     = date('Y-m-d H:i:s', dbDateDecode($row['Created'])) ; 
            $row['ReadyDate']     = date('Y-m-d H:i:s', dbDateDecode($row['ReadyDate'])) ; 
            array_push($items, $row);
         }
         dbQueryFree($result);

         //Displaying filtering toolbar
         listFilterBar();

         $total_pages  = ceil($totalRecords / LINES);
         $current_page = 1;
         if ($_GET['offset'] > 0) {
            $current_page = ($_GET['offset'] + LINES) / LINES;
         }

         //Assigning values to template engine and preparing HTML output
         $smarty->assign(array(
                              //params for list
                              'listItems'      => $items,
                              'headers'        => array_key_values($fields, 'name', array('noheader' => 1)),
                              'styles'         => array_assoc_key_values($fields, 'name', 'class'),
                              'sort'           => $listSort,

                              //params for paging
                              'overall_pages'  => $total_pages,
                              'current_page'   => $current_page,
                              'lines_per_page' => LINES
                         ));

         $smarty->display('lib/paging_bar.tpl');
         $smarty->display('modules/sales/order_list.tpl');
         $smarty->display('lib/paging_bar.tpl');

         return 0;
      }

      public function get_cancelled_list() {
         global $nid, $smarty, $filterValue, $listSort, $totalRecords;

         $fields = array(
            NULL,
            // index 0 reserved
            array('name'   => 'Number',
                  'db'     => '`Order`.Id',
                  'desc'   => true,
                  'direct' => 'Order.Active=1',
                  'type'   => FieldType::INTEGER),
            array('name' => 'Company',
                  'db'   => 'CONCAT(Company.Name," (",Company.Number,")")',
                  'type' => FieldType::STRING),
            array('name'    => 'Season',
                  'db'      => 'Season.Id',
                  'type'    => FieldType::SELECT,
                  'options' => getOwnerSeasons($this->_user['CompanyId'])),
            array('name' => 'Reference',
                  'db'   => 'Order.Reference',
                  'type' => FieldType::STRING),
            array('name' => 'Description',
                  'db'   => 'Order.Description',
                  'type' => FieldType::STRING),
            array('name'     => 'Discount',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name'     => 'Total Price',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name' => 'Sales Person',
                  'db'   => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Created',
                  'db'   => 'Order.CreateDate',
                  'type' => FieldType::DATE),
            array('name'     => 'ReadyDate',
                  'db'       => '`ORDER`.ReadyDate',
                  'nofilter' => true,
                  'type'     => FieldType::DATE),
            array('name'      => 'ReadyDateFrom',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '>=',
                  'type'      => FieldType::DATE),
            array('name'      => 'ReadyDateTo',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '<=',
                  'type'      => FieldType::DATE)
         );

         $queryFields = "Order.Id AS Number,
                        Company.Number AS \"Company Number\",
                        Company.Name AS Company,
      					Season.Name AS \"Season\",
      					Order.Reference AS Reference,
      					Order.Description AS Description,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\",
      					Order.CreateDate AS Created,
      					SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'Total Price',
      					Currency.Name as CurrencyName, `Order`.Discount AS Discount ,
                        IF(UNIX_TIMESTAMP(Order.ReadyDate)=0, '', Order.ReadyDate) as ReadyDate";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
      					LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					LEFT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
				$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
				$companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d)', $_userid, $_userid);
         }
         if ($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

         $queryClause = sprintf("`ORDER`.Active=1 AND `ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE '%%cancelled%%') %s", $companyClause);
         $result      = listLoad($fields, $queryFields, $queryTables, $queryClause, '', '`Order`.Id', true);

         $items = array();
         while ($row = dbFetch($result)) {
            $row['Total Price'] = float_to_currency($row["Total Price"]) . " " . $row["CurrencyName"];
            $row['ReadyDate']     = date('Y-m-d H:i:s', dbDateDecode($row['ReadyDate'])) ; 
            $row['Discount']    = $row['Discount'] . "%";
            array_push($items, $row);
         }
         dbQueryFree($result);

         //Displaying filtering toolbar
         listFilterBar();

         $total_pages  = ceil($totalRecords / LINES);
         $current_page = 1;
         if ($_GET['offset'] > 0) {
            $current_page = ($_GET['offset'] + LINES) / LINES;
         }

         //Assigning values to template engine and preparing HTML output
         $smarty->assign(array(
                              //params for list
                              'listItems'      => $items,
                              'headers'        => array_key_values($fields, 'name', array('noheader' => 1)),
                              'styles'         => array_assoc_key_values($fields, 'name', 'class'),
                              'sort'           => $listSort,

                              //params for paging
                              'overall_pages'  => $total_pages,
                              'current_page'   => $current_page,
                              'lines_per_page' => LINES
                         ));

         $smarty->display('lib/paging_bar.tpl');
         $smarty->display('modules/sales/order_list.tpl');
         $smarty->display('lib/paging_bar.tpl');

         return 0;
      }

      public function get_confirmed_list() {
         global $nid, $smarty, $filterValue, $listSort, $totalRecords;

         $fields = array(
            NULL,
            // index 0 reserved
            array('name'   => 'Number',
                  'db'     => '`Order`.Id',
                  'desc'   => true,
                  'direct' => 'Order.Active=1',
                  'type'   => FieldType::INTEGER),
            array('name' => 'Company',
                  'db'   => 'CONCAT(Company.Name," (",Company.Number,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Country',
                  'db'   => 'Country.Description',
                  'type' => FieldType::STRING),
            array('name'    => 'Season',
                  'db'      => 'Season.Id',
                  'type'    => FieldType::SELECT,
                  'options' => getOwnerSeasons($this->_user['CompanyId'])),
            array('name' => 'Reference',
                  'db'   => 'Order.Reference',
                  'type' => FieldType::STRING),
            array('name' => 'Description',
                  'db'   => 'Order.Description',
                  'type' => FieldType::STRING),
            array('name'     => 'Discount',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name'     => 'Total Price',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name' => 'Sales Person',
                  'db'   => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Created',
                  'db'   => 'Order.CreateDate',
                  'type' => FieldType::DATE),
            array('name'     => 'ReadyDate',
                  'db'       => 'ORDER.ReadyDate',
                  'nofilter' => true,
                  'type'     => FieldType::DATE),
            array('name'      => 'ReadyDateFrom',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '>=',
                  'type'      => FieldType::DATE),
            array('name'      => 'ReadyDateTo',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '<=',
                  'type'      => FieldType::DATE)
         );

         $queryFields = "`Order`.Id AS Number,
	                    Company.Number AS \"Company Number\",
	                    Company.Name AS Company,	                    
						Country.Description AS Country,
      					Season.Name AS \"Season\",
      					Order.Reference AS Reference,
      					Order.Description AS Description,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\", `Order`.CreateDate AS Created,
      					SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'Total Price',
      					Currency.Name as CurrencyName,
      					Order.Discount AS Discount,
                        IF(UNIX_TIMESTAMP(Order.ReadyDate)=0, '', Order.ReadyDate) as ReadyDate";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN Country ON Country.Id=Company.CountryId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
      					LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					LEFT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
				$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
				$companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d)', $_userid, $_userid);
         }
         if ($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

         $queryClause = sprintf("`ORDER`.Active=1 AND `ORDER`.Done=0 AND `ORDER`.Ready=1 %s", $companyClause);

         $result = listLoad($fields, $queryFields, $queryTables, $queryClause, '', '`Order`.Id', true);

         $items = array();
         while ($row = dbFetch($result)) {
            $row['Total Price'] = float_to_currency($row["Total Price"]) . " " . $row["CurrencyName"];
            $row['Discount']    = $row['Discount'] . "%";
            $row['Created']     = date('Y-m-d H:i:s', dbDateDecode($row['Created'])) ; 
            $row['ReadyDate']     = date('Y-m-d H:i:s', dbDateDecode($row['ReadyDate'])) ; 
            array_push($items, $row);
         }
         dbQueryFree($result);

         //Displaying filtering toolbar
         listFilterBar();

         $total_pages  = ceil($totalRecords / LINES);
         $current_page = 1;
         if ($_GET['offset'] > 0) {
            $current_page = ($_GET['offset'] + LINES) / LINES;
         }

         //Assigning values to template engine and preparing HTML output
         $smarty->assign(array(
                              //params for list
                              'listItems'      => $items,
                              'headers'        => array_key_values($fields, 'name', array('noheader' => 1)),
                              'styles'         => array_assoc_key_values($fields, 'name', 'class'),
                              'sort'           => $listSort,

                              //params for paging
                              'overall_pages'  => $total_pages,
                              'current_page'   => $current_page,
                              'lines_per_page' => LINES
                         ));

         $smarty->display('lib/paging_bar.tpl');
         $smarty->display('modules/sales/order_list.tpl');
         $smarty->display('lib/paging_bar.tpl');

         return 0;
      }

      public function get_proposal_list() {
         global $nid, $smarty, $filterValue, $listSort, $totalRecords;

         $fields = array(
            NULL,
            // index 0 reserved
            array('name'   => 'Number',
                  'db'     => '`Order`.Id',
                  'desc'   => true,
                  'direct' => 'Order.Active=1',
                  'type'   => FieldType::INTEGER),
            array('name' => 'Company',
                  'db'   => 'CONCAT(Company.Name," (",Company.Number,")")',
                  'type' => FieldType::STRING),
            array('name'    => 'Season',
                  'db'      => 'Season.Id',
                  'type'    => FieldType::SELECT,
                  'options' => getOwnerSeasons($this->_user['CompanyId'])),
            array('name' => 'Reference',
                  'db'   => 'Order.Reference',
                  'type' => FieldType::STRING),
            array('name' => 'Description',
                  'db'   => 'Order.Description',
                  'type' => FieldType::STRING),
            array('name'     => 'Discount',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name'     => 'Total Price',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name' => 'Sales Person',
                  'db'   => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Created',
                  'db'   => 'Order.CreateDate',
                  'type' => FieldType::DATE)
         );

         $queryFields = "`Order`.Id AS Number, Company.Number AS \"Company Number\", Company.Name AS Company,
      					Season.Name AS \"Season\", `Order`.Reference AS Reference, `Order`.Description AS Description,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\", `Order`.CreateDate AS Created,
      					SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'Total Price',
      					Currency.Name as CurrencyName, `Order`.Discount AS Discount";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
      					LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					LEFT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
				$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
				$companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d)', $_userid, $_userid);
         }
         if ($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

         $queryClause = sprintf("`ORDER`.Active=1 AND `ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=1 %s", $companyClause);

         $result = listLoad($fields, $queryFields, $queryTables, $queryClause, '', '`Order`.Id', true);

         $items = array();
         while ($row = dbFetch($result)) {
            $row['Total Price'] = float_to_currency($row["Total Price"]) . " " . $row["CurrencyName"];
            $row['Discount']    = $row['Discount'] . "%";
            $row['Created']     = date('Y-m-d H:i:s', dbDateDecode($row['Created'])) ; 
            array_push($items, $row);
         }
         dbQueryFree($result);

         //Displaying filtering toolbar
         listFilterBar();

         $total_pages  = ceil($totalRecords / LINES);
         $current_page = 1;
         if ($_GET['offset'] > 0) {
            $current_page = ($_GET['offset'] + LINES) / LINES;
         }

         //Assigning values to template engine and preparing HTML output
         $smarty->assign(array(
                              //params for list
                              'listItems'      => $items,
                              'headers'        => array_key_values($fields, 'name', array('noheader' => 1)),
                              'styles'         => array_assoc_key_values($fields, 'name', 'class'),
                              'sort'           => $listSort,

                              //params for paging
                              'overall_pages'  => $total_pages,
                              'current_page'   => $current_page,
                              'lines_per_page' => LINES
                         ));

         $smarty->display('lib/paging_bar.tpl');
         $smarty->display('modules/sales/order_list.tpl');
         $smarty->display('lib/paging_bar.tpl');

         return 0;
      }

      //prints table of draft orders
      public function get_draft_list() {
         global $nid, $smarty, $filterValue, $listSort, $totalRecords;

         $fields = array(
            NULL,
            // index 0 reserved
            array('name'   => 'Number',
                  'db'     => '`Order`.Id',
                  'desc'   => true,
                  'direct' => 'Order.Active=1',
                  'type'   => FieldType::INTEGER),
            array('name' => 'Company',
                  'db'   => 'CONCAT(Company.Name," (",Company.Number,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Country',
                  'db'   => 'Country.Description',
                  'type' => FieldType::STRING),
            array('name'    => 'Season',
                  'db'      => 'Season.Id',
                  'type'    => FieldType::SELECT,
                  'options' => getOwnerSeasons($this->_user['CompanyId'])),
            array('name' => 'Reference',
                  'db'   => 'Order.Reference',
                  'type' => FieldType::STRING),
            array('name' => 'Description',
                  'db'   => 'Order.Description',
                  'type' => FieldType::STRING),
            array('name'     => 'Discount',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name'     => 'Total Price',
                  'db'       => '',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name' => 'Sales Person',
                  'db'   => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Created',
                  'db'   => 'Order.CreateDate',
                  'type' => FieldType::DATE)
         );

         $queryFields = "`Order`.Id AS Number, Company.Number AS \"Company Number\", Company.Name AS Company,Country.Description AS Country,
      					Season.Name AS \"Season\", `Order`.Reference AS Reference, `Order`.Description AS Description,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\", `Order`.CreateDate AS Created,
      					SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'Total Price',
      					Currency.Name as CurrencyName, `Order`.Discount AS Discount";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN Country ON Country.Id=Company.CountryId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
      					LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					LEFT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
				$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
				$companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d)', $_userid, $_userid);
         }
         if ($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

         $queryClause = sprintf("`ORDER`.Active=1 AND `ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0 %s", $companyClause);

         $result = listLoad($fields, $queryFields, $queryTables, $queryClause, '', '`Order`.Id', true);

         $items = array();
         while ($row = dbFetch($result)) {
            $row['Total Price'] = float_to_currency($row["Total Price"]) . " " . $row["CurrencyName"];
            $row['Discount']    = $row['Discount'] . "%";
            $row['Created']     = date('Y-m-d H:i:s', dbDateDecode($row['Created'])) ; 
            array_push($items, $row);
         }
         dbQueryFree($result);

         //Displaying filtering toolbar
         listFilterBar();

         $total_pages  = ceil($totalRecords / LINES);
         $current_page = 1;
         if ($_GET['offset'] > 0) {
            $current_page = ($_GET['offset'] + LINES) / LINES;
         }

         //Assigning values to template engine and preparing HTML output
         $smarty->assign(array(
                              //params for list
                              'listItems'      => $items,
                              'headers'        => array_key_values($fields, 'name', array('noheader' => 1)),
                              'styles'         => array_assoc_key_values($fields, 'name', 'class'),
                              'sort'           => $listSort,

                              //params for paging
                              'overall_pages'  => $total_pages,
                              'current_page'   => $current_page,
                              'lines_per_page' => LINES
                         ));

         $smarty->display('lib/paging_bar.tpl');
         $smarty->display('modules/sales/order_list.tpl');
         $smarty->display('lib/paging_bar.tpl');

         return 0;
      }

      public function get_all_list() {
         global $nid, $smarty, $filterValue, $listSort, $totalRecords;

         $state_field    = 'IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=0, "Draft",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=0 AND `ORDER`.Proposal=1, "Proposal",
                       IF(`ORDER`.Done=0 AND `ORDER`.Ready=1, "Confirmed",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%finished%%"), "Shipped",
                       IF(`ORDER`.Done=1 AND OrderDoneId IN (SELECT id FROM orderdone WHERE name LIKE "%%cancelled%%"),"Cancelled", "Undefined" )))))';
         $state_options = array(''          => '- select -',
                                'Draft'     => 'Draft',
                                'Proposal'  => 'Proposal',
                                'Confirmed' => 'Confirmed',
                                'Shipped'   => 'Shipped',
                                'Cancelled' => 'Cancelled');

         $fields = array(
            NULL,
            // index 0 reserved
            array('name'   => 'Number',
                  'db'     => '`Order`.Id',
                  'desc'   => true,
                  'direct' => 'Order.Active=1',
                  'type'   => FieldType::INTEGER),
            array('name' => 'Company',
                  'db'   => 'CONCAT(Company.Name," (",Company.Number,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Country',
                  'db'   => 'Country.Description',
                  'type' => FieldType::STRING),
            array('name'    => 'State',
                  'db'      => $state_field,
                  'type'    => FieldType::SELECT,
                  'options' => $state_options),
            array('name'    => 'Season',
                  'db'      => 'Season.Id',
                  'type'    => FieldType::SELECT,
                  'options' => getOwnerSeasons($this->_user['CompanyId'])),
            array('name' => 'Reference',
                  'db'   => 'Order.Reference',
                  'type' => FieldType::STRING),
            array('name' => 'Description',
                  'db'   => 'Order.Description',
                  'type' => FieldType::STRING),
            array('name'     => 'Discount',
                  'db'       => 'Order.Discount',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name'     => 'Total Price',
                  'db'       => 'SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount))',
                  'nofilter' => true,
                  'class'    => 'right'),
            array('name' => 'Sales Person',
                  'db'   => 'CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")")',
                  'type' => FieldType::STRING),
            array('name' => 'Created',
                  'db'   => '`Order`.CreateDate',
                  'type' => FieldType::DATE),
            array('name'     => 'ReadyDate',
                  'db'       => '`ORDER`.ReadyDate',
                  'nofilter' => true,
                  'type'     => FieldType::DATE),
            array('name'      => 'ReadyDateFrom',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '>=',
                  'type'      => FieldType::DATE),
            array('name'      => 'ReadyDateTo',
                  'db'        => 'Order.ReadyDate',
                  'noheader'  => true,
                  'operation' => '<=',
                  'type'      => FieldType::DATE)
         );

         $queryFields = "Order.Id AS Number,
	                    Company.Number AS \"Company Number\",
	                    Company.Name AS Company,
						Country.Description AS Country,
      					$state_field AS State,
      					Season.Name AS \"Season\",
      					Order.Reference AS Reference,
      					Order.Description AS Description,
      					CONCAT(User.FirstName,\" \",User.LastName,\" (\",User.Loginname,\")\") AS \"Sales Person\",
      					Order.CreateDate AS Created,
      					SUM(ol.PriceSale*ol.Quantity*(100-ol.Discount)*0.01) AS 'Total Price',
      					Currency.Name as CurrencyName,
      					Order.Discount AS Discount,
	                    IF(UNIX_TIMESTAMP(Order.ReadyDate)=0, '', Order.ReadyDate) as ReadyDate";

         $queryTables = "`Order`
      					LEFT JOIN Company ON Company.Id=Order.CompanyId
      					LEFT JOIN Country ON Country.Id=Company.CountryId
      					LEFT JOIN User ON User.Id=Order.SalesUserId
      					LEFT JOIN Season ON Season.Id=Order.SeasonId
      					LEFT JOIN Currency ON Currency.Id=Order.CurrencyId
      					RIGHT JOIN Orderline ol ON ol.OrderId = Order.Id AND ol.Active = 1";

         $companyClause = sprintf('AND `Order`.ToCompanyId = %d', $this->_user['CompanyId']);
         if ($this->_user['SalesRef']) {
            if ($this->_user['AgentManager']) {
               $companyClause = sprintf('AND (`Company`.AgentId=%d OR `Order`.SalesUserId = %d)', $this->_user['AgentCompanyId'], $this->_user['Id']);
            } else {
				$_userid = ($this->_user['AvatarId']>0) ? $this->_user['AvatarId'] : $this->_user['Id'] ;
				$companyClause = sprintf('AND (`Company`.SalesUserId=%d OR `Order`.SalesUserId = %d)', $_userid, $_userid);
            }

         }
         if ($this->_user['Extern']) {
            $companyClause = sprintf('AND `Order`.CompanyId=%d', $this->_user['CompanyId']);
         }

         $queryClause = sprintf("`ORDER`.Active=1 %s", $companyClause);

		 switch ($Navigation['Parameters']) {
	 		case 'company' :
				$queryClause .= sprintf (' AND Order.CompanyId=%d', $Id) ;
				break ;
			default:
				break ;
		 }
		 
         $result = listLoad($fields, $queryFields, $queryTables, $queryClause, '', '`Order`.Id', true);

         $items = array();
         while ($row = dbFetch($result)) {
            $row['Total Price'] = float_to_currency($row["Total Price"]) . " " . $row["CurrencyName"];
            $row['Discount']    = $row['Discount'] . "%";
            $row['Created']     = date('Y-m-d H:i:s', dbDateDecode($row['Created'])) ; 
//            $row['ReadyDate']     = date('Y-m-d H:i:s', dbDateDecode($row['ReadyDate'])) ; 
            array_push($items, $row);
         }
         dbQueryFree($result);

         //Displaying filtering toolbar
         listFilterBar();

         $total_pages  = ceil($totalRecords / LINES);
         $current_page = 1;
         if ($_GET['offset'] > 0) {
            $current_page = ($_GET['offset'] + LINES) / LINES;
         }

         //Assigning values to template engine and preparing HTML output
         $smarty->assign(array(
                              //params for list
                              'listItems'      => $items,
                              'headers'        => array_key_values($fields, 'name', array('noheader' => 1)),
                              'styles'         => array_assoc_key_values($fields, 'name', 'class'),
                              'sort'           => $listSort,

                              //params for paging
                              'overall_pages'  => $total_pages,
                              'current_page'   => $current_page,
                              'lines_per_page' => LINES
                         ));

         $smarty->display('lib/paging_bar.tpl');
         $smarty->display('modules/sales/order_list.tpl');
         $smarty->display('lib/paging_bar.tpl');

         return 0;
      }
   }

   return 0;
?>