<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    itemStart () ;
    itemSpace () ;
    if ($Navigation['Parameters'] == 'free')
	    itemField ('Position', $Record['Position']) ;
	else
	    itemField ('Container', $Record['Id']) ;
    itemField ('Description', sprintf ('%s (%s)', $Record['Description'], $Record['ContainerTypeName'])) ;
    itemSpace () ;
    itemEnd () ;
     
    formStart () ;

    itemStart () ;    
    itemHeader() ;
    itemField ('Current Stock', $Record['StockName']) ;
    itemSpace () ;
    itemFieldRaw ('Stock', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type, IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 AND Id<>%d ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    itemFieldRaw ('Position', formText ('Position', '', 20)) ;
    itemEnd () ;   

    formEnd () ;

    return 0 ;
?>
