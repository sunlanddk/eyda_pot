<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;


/*
    itemStart () ;
    itemSpace () ;
    itemField ('Container', $Record['Id']) ;
    itemField ('Description', sprintf ('%s (%s)', $Record['Description'], $Record['ContainerTypeName'])) ;
    itemSpace () ;
    itemEnd () ;
 */    
    formStart () ;

    itemStart () ;    
    itemHeader() ;
    itemSpace () ;
    itemFieldRaw ('Quantity', formText ('Quantity', '', 20)) ;
    itemEnd () ;   

    formEnd () ;

    return 0 ;
?>
