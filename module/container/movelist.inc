<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;

    formStart () ;
    
    itemStart () ;
    itemHeader () ;
    itemFieldRaw ('Stock', formDBSelect ('StockId', 0, sprintf ('SELECT Id, CONCAT(Name," (",Type,IF(Type<>"fixed",DATE_FORMAT(DepartureDate," %%Y.%%m.%%d"),""),")") AS Value FROM Stock WHERE Done=0 AND Ready=0 AND Active=1 ORDER BY Type, Value', $Record['StockId']), 'width:250px;')) ;
    itemFieldRaw ('Position', formText ('Position', '', 20)) ;
    itemFieldRaw ('Keep Position',formCheckbox ('KeepPosition', 0)) ;
    itemSpace () ;
    itemEnd () ;

    // List
    listClear () ;
    listStart () ;
    listRow () ;
    listHeadIcon () ;
    listHead ('Type', 90) ;
    listHead ('Description') ;
    listHead ('Batch', 80, 'align=right') ;

    listHead ('Items', 90, 'align=right') ;
    listHead ('Article', 80, 'align=right') ;
    listHead ('Color', 80, 'align=right') ;
    listHead ('Size', 40, 'align=right') ;
    listHead ('Pick?', 40) ;
    listHead ('Qty', 60, 'align=right') ;
    listHead ('Gross (kg)', 90, 'align=right') ;
    listHead ('SKU assigned', 100, 'align=right') ;
    listHead ('Items ok?', 50) ;
    listHead ('Position', 50) ;
		listHead ('MoveTo', 120) ;
    listHead ('Container', 80, 'align=right') ;
    listHead ('', 30) ;

    $n = 0 ;
    while ($n++ < $listLines and ($row = dbFetch($Result))) {
	    listRow () ;
	    listFieldIcon ('container.gif', 'containerview', 0) ;
	    listField ($row['ContainerTypeName']) ;
	    listField ($row['Description']) ;
	    listField ($row['BatchNumber'], 'align=right') ;
	    listField ($row['ItemCount'], 'align=right') ;
	    listField ($row['ItemArticleNumber'], 'align=right') ;
	    listField ($row['ItemColorNumber'], 'align=right') ;
	    listField ($row['ItemSize'], 'align=right') ;
	    listField ($row['ItemPickAssigned']?'Yes':'', 'align=right') ;
      listField ($row['ItemQty'], 'align=right') ;
	    listField ($row['GrossWeight'], 'align=right') ;
	    listField ($row['VariantCode'], 'align=right') ;
	    listField ($row['ItemPickAssigned']?'Yes':($row['ItemQty']>0?'No':($row['VariantCode']<>''?'Yes':'')), 'align=right') ;
      listField ($row['Position']) ;
			// Is something to move and is all items to be moved to same stock?
			if (($row['MaxAltStockId'] > 0) and ($row['MaxAltStockId'] <> $row['StockId'])) {
				if ($row['MinAltStockId']==$row['MaxAltStockId']) {
				    $q = sprintf('select max(altstockid) as MaxId, min(altstockid) as MinId from item where containerid=%d and active=1', (int)$row['Id']) ;
	    		    $res = dbQuery ($q) ;
				    $AltStocks = dbFetch ($res) ;
				    dbQueryFree ($res) ;
				    if ($AltStocks['MaxId'] == $AltStocks['MinId'])
						listField ($row['AltStockName']) ;
					else 
						listField ('*****') ;
				} else 
					listField ('*****') ;
			 } else { 
				listField ('') ;
			}
	    listField ((int)$row['Id'], 'align=right') ;
	    listFieldRaw (formCheckbox (sprintf('Container[%d]', (int)$row['Id']), 0)) ;
    }
    if (dbNumRows($Result) == 0) {
	    listRow () ;
	    listField () ;
	    listField ('No Containers', 'colspan=4') ;
    }
    listEnd () ;
    dbQueryFree ($Result) ;

    formEnd () ;

?>
<script type='text/javascript'>
function CheckAll () {
    var col = document.appform.elements ;
    var n ;
    for (n = 0 ; n < col.length ; n++) {
	var e = col[n] ;
	if (e.type != 'checkbox') continue ;
	e.checked = true ;
    }
}
</script>
<?php

    return 0 ;
?>
