<?php

    require_once 'lib/item.inc' ;

    // Header
    itemStart () ;
    itemHeader () ;
    itemField ('Container', $Record['Id']) ;
    itemSpace () ;
    itemField ('Type', $Record['ContainerTypeName']) ;
    itemField ('Description', $Record['Description']) ;
    itemSpace () ;
    switch ($Record['StockType']) {
	case 'fixed' :
	    itemFieldIcon ('Stock', $Record['StockName'], 'stock.gif', 'stockview', $Record['StockId']) ;
	    break ;

	case 'transport' :
	    itemFieldIcon ('Transport', $Record['StockName'], 'transport.gif', 'transportview', $Record['StockId']) ;
	    break ;

	case 'shipment' :
	    itemFieldIcon ('Stock', $Record['StockName'], 'shipment.gif', 'shipmentview', $Record['StockId']) ;
	    break ;
    }
    itemField ('Position', $Record['Position']) ;
    itemSpace () ;
    itemField ('Volume', $Record['Volume'] . ' m3') ;
    itemSpace () ;
    itemField ('Tara Weight', $Record['TaraWeight'] . ' kg') ;
    itemField ('Gross Weight', $Record['GrossWeight'] . ' kg') ;
    itemInfo ($Record) ;
    itemEnd () ;

    return 0 ;
?>
