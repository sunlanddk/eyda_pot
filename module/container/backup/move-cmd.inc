<?php

    require_once 'lib/save.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

    $fields = array (
	'StockId'		=> array ('type' => 'integer'),
	'Position'		=> array ()
    ) ;

    // Get Fields
    $res = saveFields (NULL, NULL, $fields) ;
    if ($res) return $res ;

    if ($fields['StockId']['value'] == 0) // return 'please select new Location' ;
		$fields['StockId']['value'] = $Record['StockId'];
		
    // Get new Stock
    $query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
    $result = dbQuery ($query) ;
    $Stock = dbFetch ($result) ;
    dbQueryFree ($result) ;
    if ($Stock['Id'] <= 0) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;

    // Validate destination location/position
    if (($Navigation['Parameters'] == 'free') and $fields['StockId']['value'] == $Record['StockId']) {
		$AssignPos = $fields['Position']['value'] ;
    		$Street = strtoupper(substr($AssignPos, 0, 1)) ;
		$StreetNo = (int)substr($AssignPos, 1, 3) ;
		$Floor = strtolower(substr($AssignPos, 4, 1)) ;
		$Room = (int)substr($AssignPos, 5, 2) ;
		if ($PickLayout[$Stock['Name']][$Street]['NoStreetNo'] > $StreetNo) {
			$NoFloors = ($StreetNo/2 > (int)($StreetNo/2)) ? 'OddNoFloors' : 'EvenNoFloors' ;
			if ($PickLayout[$Stock['Name']][$Street][$NoFloors] > (ord($Floor)-ord('a'))) {
			    if ($Room > 0) {
				    if ($Room > 25) return 'Only 1-25 rooms allowed. Specified is ()' . substr($AssignPos, 5, 2) ;
					$fields['Position']['value'] = sprintf ('%s%03d%s%d', $Street,  $StreetNo, $Floor, $Room) ;
			    } else {
					$fields['Position']['value'] = sprintf ('%s%03d%s', $Street,  $StreetNo, $Floor) ;
				}
			} else {
				return sprintf('Floor %s not found in pick layout on Street number %s for position (%s). Expect format A001a or A001a1', substr($AssignPos, 4, 1), substr($AssignPos, 1, 3), $AssignPos) ;
			}
		} else { 
			return sprintf('Street Number %s not found on street %s in Pick layout for position (%s). Expect format A001a', substr($AssignPos, 1, 3), $Street, $AssignPos) ;
		}
	}

    // Is it allowed to move containers into the new Stock Location
    if ($Stock['Done']) return sprintf ('%s(%d) new Stock is done, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;
    if ($Stock['Ready'] and !$Stock['Verified']) return sprintf ('%s(%d) new Stock is locked, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

    // Get Containers to move
    switch ($Navigation['Parameters']) {
	case 'current' :
	case 'free' :
	    // Id for Container to move is supplied in URL
	    $Container[$Id] = array ('NewPosition' => $fields['Position']['value']) ;
	    break ;

	case 'list' :
	    // List of Containers
	    // Get Id's from posted checkboxes
	    if (!is_array($_POST['Container'])) return 'please select Container(s) to move' ;
	    $Container = array () ;
	    foreach ($_POST['Container'] as $id => $flag) {
		if ($flag != 'on') continue ;
		if ($id <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $id) ;
		$Container[$id] = array ('NewPosition' => $fields['Position']['value']) ;
		if (count($Container) == 0) return 'please select Container(s) to move' ;
	    }
	    break ;

	default:
	    return sprintf ('%s(%d) invalid parameter "%s"', __FILE__, __LINE__, $Navigation['Parameters']) ;
    }

    // Get Container information
    foreach ($Container as $id => $row) {
	// Get Container
	$query = sprintf ("SELECT %s FROM %s WHERE Container.Id=%d AND Container.Active=1", $queryFields, $queryTables, $id) ;
	$res = dbQuery ($query) ;
	$Container[$id] = array_merge ($Container[$id], dbFetch ($res)) ;
	dbQueryFree ($res) ;
	if ($Container[$id]['Id'] != $id) return sprintf ('%s(%d) Container not found, id %d', __FILE__, __LINE__, $id) ;
    }

    // Validate that containers can be moved
    foreach ($Container as $id => $row) {
		if ($row['StockDone']) return sprintf ('Location for container %d is done', $id) ;
		if ($row['StockReady'] and !$row['StockVerified']) return sprintf ('Location for container %d is locked', $id) ;
    }

//  require_once 'lib/log.inc' ;
//  logPrintVar ($Container, 'Container') ;

    // Do the moving
    foreach ($Container as $id => $row) {
		$fields = array (
			'Position'		=> array ('type' => 'set',	'value' => $row['NewPosition'])
		) ;

    	$DeleteContainer = 0 ;
		// Moving to the same Location ?
		if ((int)$Stock['Id'] != (int)$row['StockId']) {
			// Set new Stock location
			$fields['StockId'] = array ('type' => 'set', 'value' => (int)$Stock['Id']) ;
			$fields['PreviousStockId'] = array ('type' => 'set', 'value' => (int)$row['StockId']) ;
		    
			if ($row['StockType'] == 'transport' and $row['StockVerified']) {
			// The containers are moved out from a real finished transportation
			$fields['PreviousTransportId'] = array ('type' => 'set', 'value' => (int)$row['StockId']) ;

			// Update Items with Transportation
			$query = sprintf ('UPDATE Item SET TransportId=%d WHERE ContainerId=%d AND Active=1', (int)$row['StockId'], $id) ;
			dbQuery ($query) ;
			}

			if ($Stock['Type'] == 'shipment') {
			// The Container has been moved to a shipment
			// Update OrderPos for Items when moving to shipment
			}
			$query = sprintf ("SELECT * FROM VariantCode vc WHERE vc.PickContainerId=%d AND vc.Active=1", $id) ;
			$res = dbQuery ($query) ;
			if (dbNumRows($res) > 1)
				return sprintf('More than one variantcode assigned to same pickcontainer (%d) - Please clean before moving', $id) ;
			if (dbNumRows($res)== 1) {
				$VariantCode = dbFetch ($res) ;
			    $VariantCodeUpdate = array (
					'PickContainerId' => 0
			    ) ;
				tablewrite ('VariantCode', $VariantCodeUpdate, $VariantCode['Id']) ; // Free Pick location assignment
				$query = sprintf ("SELECT sum(Quantity) as Qty FROM Item WHERE ContainerId=%d AND Active=1", $id) ;
				$res2 = dbQuery ($query) ;
				$Qty = dbFetch ($res2) ;
				$DeleteContainer = $Qty['Qty']>0?0:1 ;
				dbQueryFree ($res2) ;
			}
			dbQueryFree ($res) ;
		}
	    		  
		// Update Container
		if ($DeleteContainer) {
		    tableDelete ('Container', $id) ;
		} else {
			$res = saveFields ('Container', $id, $fields) ;
			if ($res) return $res ;
		}
    }

    if ($Navigation['Parameters'] == 'free') 
	    return navigationCommandMark ('layoutlist', $Record['StockId'], '&StreetName=' . $_GET['StreetName']) ;

    return 0 ;
?>
