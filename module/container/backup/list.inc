<?php

    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'module/storing/include.inc' ; // Pick stock layout

    // Filter Bar
	if ($Navigation['Parameters'] <> 'layout') 
	    listFilterBar () ;

    switch ($Navigation['Parameters']) {
	case 'layout' :
	case 'stock' :
	    itemStart () ;
	    itemSpace () ;
	    switch ($Record['Type']) {
		case 'fixed' :
		    itemFieldIcon ('Stock', $Record['Name'], 'stock.gif', 'stockview', $Record['Id']) ;
		    break ;

		case 'transport' :
		    itemFieldIcon ('Transport', $Record['Name'], 'transport.gif', 'transportview', $Record['Id']) ;
		    break ;

		case 'shipment' :
		    itemFieldIcon ('Shipment', $Record['Name'], 'shipment.gif', 'shipmentview', $Record['Id']) ;
		    break ;
	    }
	    itemField ('Description', $Record["Description"]) ;
	    itemEnd () ;
	    printf ('<br>') ;
	    break ;
    }

	if ($Navigation['Parameters'] <> 'layout') {
		// List
		listStart () ;
		listRow () ;
		listHeadIcon () ;
		listHead ('Type', 90) ;
		listHead ('Description') ;
		if (!$HideStock) {
		listHead ('Stock', 100) ;
		}
		listHead ('Batch', 90) ;
		listHead ('Items', 90, 'align=right') ;
		listHead ('Article', 80, 'align=right') ;
		listHead ('Color', 80, 'align=right') ;
		listHead ('Size', 40, 'align=right') ;
		listHead ('Pick?', 40) ;
		listHead ('Qty', 60, 'align=right') ;
		listHead ('Volume (m3)', 90, 'align=right') ;
		listHead ('Tara (kg)', 90, 'align=right') ;
		listHead ('Gross (kg)', 90, 'align=right') ;
		listHead ('SKU assigned', 100, 'align=right') ;
		listHead ('Items ok?',50) ;
		listHead ('Position', 50) ;
		listHead ('Container', 80, 'align=right') ;
		listHead ('', 8) ;
		$n = 0 ;
		while ($n++ < $listLines and ($row = dbFetch($Result))) {
			listRow () ;
			listFieldIcon ($Navigation['Icon'], 'containerview', $row['Id']) ;
			listField ($row['ContainerTypeName']) ;
			listField ($row['Description']) ;
			if (!$HideStock) {
				listField ($row['StockName']) ;
			}
			listField ($row['BatchNumber']) ;
			listField ($row['ItemCount'], 'align=right') ;
			listField ($row['ItemArticleNumber'], 'align=right') ;
			listField ($row['ItemColorNumber'], 'align=right') ;
			listField ($row['ItemSize'], 'align=right') ;
			listField ($row['ItemPickAssigned']?'Yes':'', 'align=right') ;
			listField ($row['ItemQty'], 'align=right') ;
			listField ($row['Volume'], 'align=right') ;
			listField ($row['TaraWeight'], 'align=right') ;
			listField ($row['GrossWeight'], 'align=right') ;
			listField ($row['VariantCode'], 'align=right') ;
			if ($row['VariantCode']<>'') {
				if ($row['ItemQty']>0) {
					if($row['ItemPickAssigned']) 
						$ItemsOk = 'Yes' ;
					else
						$ItemsOk = 'No' ;
				} else {
					$ItemsOk = 'Yes' ;
				}
			} else {
				$ItemsOk = '' ;
			}
			listField ($ItemsOk) ; 
			listField ($row['Position']) ;
			listField ((int)$row['Id'], 'align=right') ;
		}
		if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Containers', 'colspan=4') ;
		}
		listEnd () ;
		dbQueryFree ($Result) ;

	// Layout list.
	} else {
		// List
		listStart () ;
		listRow () ;
		listHead ('', 10) ;
		listHead ('Position', 50) ;
		listHead ('SKU assigned', 80, 'align=left') ;
		listHead ('Items ok?',55) ;
		listHead ('Article', 80, 'align=left') ;
		listHead ('Color', 80, 'align=left') ;
		listHead ('Size', 40, 'align=left') ;
		listHead ('Qty', 60, 'align=left') ;
//		listHead ('Container', 80, 'align=right') ;
		listHead ('', 200) ;

		if (empty($PickLayout[$Record['Name']])) return 'No Pick layout defined for this stock' ;

		while (($row = dbFetch($Result))) {
			$row['Position'] = strtoupper($row['Position']) ;
			$Lines[$row['Position']] = $row ;
			$Addr = substr($row['Position'], 0, 5) ;
			if (strlen($row['Position'])==6 or strlen($row['Position'])==7) {
				$Addr = strtoupper(substr($row['Position'], 0, 5)) ;
				$Room = (int)substr($row['Position'], 5, 2) ;
				if ($Room>0 and $Room<25) $Rooms[$Addr][$Room] = 1 ;
				$Rooms[$Addr][0] = 1 ;
			} else
				$Rooms[$Addr][0] = 0 ;
		}

		foreach ($PickLayout[$Record['Name']] as $StreetName => $Street) {
			if (isset($_GET['StreetName']))
				if ($_GET['StreetName'] <> $StreetName) continue ;
 			for ($StreetNo=1; $StreetNo<=$Street['NoStreetNo']; $StreetNo++) {
				$NoFloors = ($StreetNo/2 > (int)($StreetNo/2)) ? $Street['OddNoFloors'] : $Street['EvenNoFloors'] ;
				for ($i=0; $i<$NoFloors; $i++) {
					$Floor = chr(ord('a') + $i) ;
					$Pos = strtoupper(sprintf ('%s%03d%s', $StreetName,  $StreetNo, $Floor)) ;
					$Pos_Floor = $Pos ;

//					foreach ($Rooms[$Pos] as $RoomNo => $value) {
					for ($j=0; $j<25; $j++) {
						 if ($j>0) {
							if ($Rooms[$Pos_Floor][$j]>0) 
								$Pos = strtoupper(sprintf ('%s%03d%s%d', $StreetName,  $StreetNo, $Floor, $j)) ;
							else continue;
						} else {
							if ($Rooms[$Pos][0]>0) continue;
						}
						
						listRow () ;
						if ($Lines[$Pos]['VariantCode'] <> '') {
							if ((int)$Lines[$Pos]['ItemQty'] == 0)
								listFieldIcon ('delete.gif', 'freeposition', $Lines[$Pos]['Id'], 'StreetName=' . $_GET['StreetName']) ;
							else
								listFieldIcon ('move.gif', 'movefreeposition', $Lines[$Pos]['Id'], 'StreetName=' . $_GET['StreetName']) ;
						} else {
							listField ('') ;
						}
						listField ($Pos) ;
						listField ($Lines[$Pos]['VariantCode'], 'align=left') ;
						if ($Lines[$Pos]['VariantCode']<>'') {
							if ($Lines[$Pos]['ItemQty']>0) {
								if($Lines[$Pos]['ItemPickAssigned']) 
									$ItemsOk = 'Yes' ;
								else
									$ItemsOk = 'No' ;
							} else {
								$ItemsOk = 'Yes' ;
							}
						} else {
							$ItemsOk = '' ;
						}
						listField ($ItemsOk) ; 
						listField ($Lines[$Pos]['ItemArticleNumber'], 'align=left') ;
						listField ($Lines[$Pos]['ItemColorNumber'], 'align=left') ;
						listField ($Lines[$Pos]['ItemSize'], 'align=left') ;
						listField (number_format($Lines[$Pos]['ItemQty'],0), 'align=rigth') ;
	//					listField ((int)$Lines[$Pos]['Id'], 'align=right') ;
						listField ('') ;
					} 
				} 
			} 
		}
		
		if (dbNumRows($Result) == 0) {
		listRow () ;
		listField () ;
		listField ('No Containers', 'colspan=4') ;
		}
		listEnd () ;
		dbQueryFree ($Result) ;
	}   
    return 0 ;
?>
