<?php

    require_once 'lib/navigation.inc' ;

    $queryFields = 'Container.*, Stock.Name AS StockName, Stock.Type AS StockType, Stock.Ready AS StockReady, Stock.Arrived AS StockArrived, Stock.Verified AS StockVerified, ContainerType.Name AS ContainerTypeName' ;
    $queryTables = 'Container LEFT JOIN ContainerType ON ContainerType.Id=Container.ContainerTypeId LEFT JOIN Stock ON Stock.Id=Container.StockId' ;
    $queryGroup = 'Container.Id' ;

    switch ($Navigation['Function']) {
	case 'movelist' :
	    // Overload the Navigation Parameter with the one supplied in the URL
	    $Navigation['Parameters'] = $_GET['param'] ;

	    // Fall through
	     
	case 'streetlist' :
	case 'list' :
	case 'containerlist' :
	    // Expand query
	    $queryTables .= ' LEFT JOIN Item ON Item.ContainerId=Container.Id AND Item.Active=1
					LEFT JOIN Stock AltStock ON AltStock.Id = Item.AltStockId
				    LEFT JOIN Article ON Item.ArticleId=Article.Id 
				    LEFT JOIN OrderLine ol ON Item.OrderLineId>0 and ol.id=Item.OrderLineId
				    LEFT JOIN ArticleColor ON Item.ArticleColorId=ArticleColor.Id 
				    LEFT JOIN Color ON ArticleColor.ColorId=Color.Id 
					LEFT JOIN ArticleSize ON Item.ArticleSizeId=ArticleSize.Id 
					LEFT JOIN VariantCode ON VariantCode.PickContainerId=Container.Id AND VariantCode.Active=1';
//				    LEFT JOIN VariantCode on VariantCode.ArticleColorId=Item.ArticleColorId AND VariantCode.ArticleSizeId=Item.ArticleSizeId AND VariantCode.Active=1' ;

	    $queryFields .= ', COUNT(Item.Id) AS ItemCount, SUM(Item.Quantity) as ItemQty, Item.BatchNumber, MIN(Item.AltStockId) as MinAltStockId, 
					MAX(Item.AltStockId) as MaxAltStockId, Max(AltStock.Name) as AltStockName,
					Article.Number as ItemArticleNumber, Color.Number as ItemColorNumber, ArticleSize.Name as ItemSize, 
					VariantCode.VariantCode, VariantCode.PickContainerId, ol.orderid as OrderId,
					IF (VariantCode.ArticleColorId=Item.ArticleColorId AND VariantCode.ArticleSizeId=Item.ArticleSizeId, 1, 0) AS ItemPickAssigned' ;
//					Article.Number as ItemArticleNumber, Color.Number as ItemColorNumber, ArticleSize.Name as ItemSize, VariantCode.VariantCode, VariantCode.PickContainerId' ;

	    // List field specification
	    $fields = array (
		NULL, // index 0 reserved
		array ('name' => 'Container',		'db' => 'Container.Id', 			'direct' => 'Container.Active=1'),
//		array ('name' => 'Description',		'db' => 'Container.Description'),
		array ('name' => 'Order',			'db' => 'ol.orderid'),
//		array ('name' => 'Type',			'db' => 'ContainerType.Name'),
		array ('name' => 'Position',		'db' => 'Container.Position'),
		array ('name' => 'Stock',			'db' => 'Stock.Name'),
//		array ('name' => 'Batch',			'db' => 'Item.BatchNumber'),
//		array ('name' => 'Items',			'db' => 'COUNT(Item.Id)'),
		array ('name' => 'Article',			'db' => 'Article.Number'),
		array ('name' => 'Color',			'db' => 'Color.Number'),
		array ('name' => 'Size',			'db' => 'ArticleSize.Name'),
		array ('name' => 'SKU assigned',	'db' => 'VariantCode.VariantCode'),
//		array ('name' => 'Qty',				'db' => 'ItemQty',				'nofilter' => true),
//		array ('name' => 'Volume (m3)',		'db' => 'Container.Volume'),
//		array ('name' => 'Tara (kg)',		'db' => 'Container.TaraWeight'),
//		array ('name' => 'MoveTo',			'db' => 'AltStock.Name'),
//		array ('name' => 'Gross (kg)',		'db' => 'Container.GrossWeight')
	    ) ;

	    switch ($Navigation['Parameters']) {
	        case 'layout' :
	        case 'stock' :
		    $query = sprintf ('SELECT * FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;

		    // Navigation
		    if ($Record['Type'] == 'fixed') {
			navigationEnable ('new') ;
		    }

		    // Display
		    $HideStock = true ;

		    $queryClause = sprintf ('Container.StockId=%d AND Container.Active=1', $Id) ;
		    break ;

		default :
		    $queryClause = 'Container.Active=1' ;
		    break ;
	    }
	    
	    require_once 'lib/list.inc' ;
	    $Result = listLoad ($fields, $queryFields, $queryTables, $queryClause, '', $queryGroup) ;
	    if (is_string($Result)) return $Result ;

	    if ($Navigation['Function'] == 'list') {
		// Navigation
		// Pass all parameters for the list function to the move function
		$param = 'param=' . $Navigation['Parameters'] ;
		if ($listParam != '') $param .= '&' . $listParam ;
		navigationSetParameter ('movelist', $param) ;
	    
		if (dbNumRows($Result) == 0) {
		    navigationPasive ('movelist') ;
		}
	    
		return listView ($Result, 'containerview') ;
	    }
	    return 0 ;

	case 'basic' :
	case 'basic-cmd' :
	    switch ($Navigation['Parameters']) {
		case 'new' :
		    $query = sprintf ('SELECT Stock.*, Stock.Name AS StockName FROM Stock WHERE Stock.Id=%d AND Stock.Active=1', $Id) ;
		    $res = dbQuery ($query) ;
		    $Record = dbFetch ($res) ;
		    dbQueryFree ($res) ;
		    return 0 ;
	    }

	    // Fall through
		
	case 'view' :
	case 'move' :
	case 'move-cmd' :
	case 'delete-cmd' :
	    // Get Container
	    $query = sprintf ("SELECT %s FROM %s WHERE Container.Id=%d AND Container.Active=1", $queryFields, $queryTables, $Id) ;
	    $res = dbQuery ($query) ;
	    $Record = dbFetch ($res) ;
	    dbQueryFree ($res) ;

	    // Navigation
	    if ($Record['StockType'] != 'fixed') {
		navigationPasive ('edit') ;
	    }

	    if ($Record['StockDone'] or ($Record['StockReady'] and !$Record['StockVerified'])) {
		navigationPasive ('move') ;
	    }		
		
	    break ;

	case 'move-cmd' :
	    // Everything handled in the command module
	    break ;    
    }
 
    return 0 ;
?>
