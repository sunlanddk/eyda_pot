<?php

    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;
	Global $User ;

    switch ($Navigation["Parameters"]) {
		case "new":
		case 'neworder' :
		case 'newproposal' :
			unset ($Record) ;
			$Record["Shop"]=1;
			$Record["InvoicePerEmail"]=1;
			$Record['CategoryId'] = 3 ;		 // Kunde cathegory C
			$Record['PaymentStatusId'] = 1 ; // Payment status OK
			$Record['CollectionTypeId'] = 3 ;// Baby, Kids and Women
			$new=1 ;
	    break ;
			$new=0 ;   }

    require_once "lib/html.inc" ;
	$prefix = '' ;
    $MyCompanyId = $User['CompanyId'] ;
	

	$_countryid = 0 ;
	if (isset($_GET['countryid'])) {
		$_countryid = (int)$_GET['countryid'] ;
	} else if ($User['SalesRef']) {
		if ($User['AgentCompanyId']>0)
			$_usercompanyid = $User['AgentCompanyId'];
		else
			$_usercompanyid = $User['CompanyId'];
		$_countryid = tableGetField('Company', 'CountryId', $_usercompanyid) ;
	}
	if ($_countryid>0) {
		$query = sprintf("SELECT * FROM companydefaults cd WHERE CountryId=%d", $_countryid);
		$res = dbQuery ($query) ;
	    $_companydefaults = dbFetch ($res) ;
		dbQueryFree ($res) ;
		$Record["CountryId"] = $_countryid ;
		$Record["CurrencyId"] = $_companydefaults['CurrencyId'] ;
		$Record["PaymentTermId"] = $_companydefaults['PaymentTermId'] ;
		$Record["DeliveryTermId"] = $_companydefaults['DeliveryTermId'] ;
		$Record["CarrierId"] = $_companydefaults['CarrierId'] ;
	}
    printf ("<form method=POST name=appform>\n") ;
    printf ("<input type=hidden name=id value=%d>", $Record['Id']) ;
    printf ("<input type=hidden name=nid>") ;
    printf ("<table class=item>\n") ;

	$_tmp = ($User['AgentCompanyId']>0) ? " and INSTR(Name, '+')=0 and Id in (select countryid from company where active=1 and agentid=" . $User['AgentCompanyId'] . " group by agentid,countryid)" : "" ;
    $query_c = "SELECT Id, concat(Description, ' (', Name, ')') AS Value FROM Country WHERE Active=1" . $_tmp . " ORDER BY Value" ;
	
if (($User['AgentCompanyId']>0) And $new==0) {
	$do_smt = 1; 
//	printf ("<tr><td class=itemlabel>Name</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["Name"])) ;
//	print htmlItemSpace() ;
    print htmlItemHeader() ;
    printf ("<tr><td><p>Name</p></td><td><input type=text name=Name size=50 maxlength=55 value=\"%s\"></td></tr>\n", htmlentities($Record["Name"])) ;
    printf ("<tr><td class=itemlabel>Street</td><td><input type=text name=%sAddress1 size=50  maxlength=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address1"])) ;
    printf ("<tr><td></td><td><input type=text name=%sAddress2 size=50  maxlength=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address2"])) ;
    printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=%sZIP size=20 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."ZIP"])) ;
    printf ("<tr><td class=itemlabel>City</td><td><input type=text name=%sCity size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."City"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Contact</p></td><td><input type=text name=ContactPerson size=50 maxlength=55 value=\"%s\"></td></tr>\n", htmlentities($Record["ContactPerson"])) ;
	print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Category</td><td>%s</td></tr>\n", htmlDBSelect ('CategoryId style="width:140px"', $Record['CategoryId'], 'SELECT Id, Name AS Value FROM CustomerCategory ORDER BY Value')) ;  

} else {
    print htmlItemHeader() ;
	itemFieldRaw ('Country', formDBSelect ('CountryId', (int)$Record[$prefix."CountryId"], $query_c, 'width:140px;', NULL, sprintf('onchange="appLoad(%d,%d,\'countryid=\'+document.appform.CountryId.options[document.appform.CountryId.selectedIndex].value);"', (int)$Navigation['Id'], $Id)) . '(always select Country first)') ; 
	print htmlItemSpace() ;

	if (!$User['SalesRef'])
		printf ("<tr><td><p>Number</p></td><td><input type=text name=Number maxlength=10 value=\"%s\" style=\"width:130px\"></td></tr>\n", htmlentities($Record["Number"])) ;
	print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Category</td><td>%s</td></tr>\n", htmlDBSelect ('CategoryId style="width:140px"', $Record['CategoryId'], 'SELECT Id, Name AS Value FROM CustomerCategory ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>CollectionType</td><td>%s</td></tr>\n", htmlDBSelect ('CollectionTypeId style="width:140px"', $Record['CollectionTypeId'], 'SELECT Id, Name AS Value FROM CollectionType ORDER BY Id')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Payment status</td><td>%s</td></tr>\n", htmlDBSelect ('PaymentStatusId style="width:140px"', $Record['PaymentStatusId'], 'SELECT Id, Name AS Value FROM PaymentStatus ORDER BY Id')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td><p>Name</p></td><td><input type=text name=Name size=50 maxlength=55 value=\"%s\"></td></tr>\n", htmlentities($Record["Name"])) ;
    printf ("<tr><td class=itemlabel>Street</td><td><input type=text name=%sAddress1 size=50  maxlength=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address1"])) ;
    printf ("<tr><td></td><td><input type=text name=%sAddress2 size=50  maxlength=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address2"])) ;
    printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=%sZIP size=20 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."ZIP"])) ;
    printf ("<tr><td class=itemlabel>City</td><td><input type=text name=%sCity size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."City"])) ;
    print htmlItemSpace() ;
    printf ("<tr><td><p>Contact</p></td><td><input type=text name=ContactPerson size=50 maxlength=55 value=\"%s\"></td></tr>\n", htmlentities($Record["ContactPerson"])) ;
	

//    printf ("<tr><td class=itemlabel>Country</td><td>%s</td></tr>\n", htmlDBSelect ($prefix."CountryId style='width:280px'", $Record[$prefix."CountryId"], "SELECT Id, Description AS Value FROM Country WHERE Active=1 ORDER BY Description")) ;  
}
	print htmlItemSpace() ;
    printf ("<tr><td><p>Email</p></td><td><input type=text name=Mail size=60 value=\"%s\"></td></tr>\n", htmlentities($Record["Mail"])) ;
    printf ("<tr><td><p>InvoicePerEmail</p></td><td><input type=checkbox name=InvoicePerEmail %s></td></tr>\n", ($Record["InvoicePerEmail"])?"checked":"") ;
    printf ("<tr><td><p>Invoice Email adress</p></td><td><input type=text name=MailInvoice size=60 value=\"%s\"></td></tr>\n", htmlentities($Record["MailInvoice"])) ;
    printf ("<tr><td><p>Book keeping Email adress</p></td><td><input type=text name=MailBookkeeping size=60 value=\"%s\"></td></tr>\n", htmlentities($Record["MailBookkeeping"])) ;
    printf ("<tr><td><p>Phone</p></td><td><input type=text name=PhoneMain size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["PhoneMain"])) ;
	print htmlItemSpace() ;
    printf ("<tr><td><p>Physical shop</p></td><td><input type=checkbox name=Shop %s></td></tr>\n", ($Record["Shop"])?"checked":"") ;
    printf ("<tr><td><p>Webshop</p></td><td><input type=checkbox name=Webshop %s></td></tr>\n", ($Record["Webshop"])?"checked":"") ;
    printf ("<tr><td><p>Web adress</p></td><td><input type=text name=Web size=60 value=\"%s\"></td></tr>\n", htmlentities($Record["Web"])) ;
	
//    printf ("<tr><td><p>Fax</p></td><td><input type=text name=PhoneFax size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["PhoneFax"])) ;
if (($User['AgentCompanyId']>0) And $new==0) {
	$do_smt = 1; 
} else {
    print htmlItemSpace() ;
    printf ("<tr><td><p>Reg.Number</p></td><td><input type=text name=RegNumber size=20 value=\"%s\"></td></tr>\n", htmlentities($Record["RegNumber"])) ;
    printf ("<tr><td class=itemlabel>VAT Number</td><td><input type=text name=VATNumber size=20 maxlength=20 value=\"%s\"></td></tr>\n", htmlentities($Record['VATNumber'])) ;
    print htmlItemSpace() ;
//    printf ("<tr><td><p>Customer</p></td><td><input type=checkbox name=TypeCustomer %s></td></tr>\n", ($Record["TypeCustomer"])?"checked":"") ;
    printf ("<tr><td><p>Prospect</p></td><td><input type=checkbox name=Prospect %s></td></tr>\n", ($Record["Prospect"])?"checked":"") ;
    printf ("<tr><td><p>InvoiceDraft</p></td><td><input type=checkbox name=InvoiceDraft %s> (Invoice is left in draft after pack/ship information is imported from stock)</td></tr>\n", ($Record["InvoiceDraft"])?"checked":"") ;
    printf ("<tr><td><p>BatchPick</p></td><td><input type=checkbox name=IncludeBatchPick %s> (Orders to this customer are included in batch-generate of pick orders)</td></tr>\n", ($Record["IncludeBatchPick"])?"checked":"") ;
//    printf ("<tr><td><p>Supplier</p></td><td><input type=checkbox name=TypeSupplier %s></td></tr>\n", ($Record["TypeSupplier"])?"checked":"") ;
//    printf ("<tr><td><p>Carrier</p></td><td><input type=checkbox name=TypeCarrier %s></td></tr>\n", ($Record["TypeCarrier"])?"checked":"") ;
//    print htmlItemSpace() ;
//    printf ("<tr><td><p>Internal</p></td><td><input type=checkbox name=Internal %s></td></tr>\n", ($Record["Internal"])?"checked":"") ;
//    print htmlItemSpace() ;
    switch ($Navigation["Parameters"]) {
		case "new":
		case 'neworder' :
		case 'newproposal' :
			break ;
		default:
			if (!$User['SalesRef']) 
				printf ("<tr><td><p>Hide from list</p></td><td><input type=checkbox name=ListHidden %s></td></tr>\n", ($Record["ListHidden"])?"checked":"") ;
			break ;
   }
    print htmlItemSpace() ;
	
	if ($User['SalesRef']) {
		if ($User['AgentManager']) {
			printf ("<tr><td class=itemlabel>Sales Ref</td><td>%s</td></tr>\n", htmlDBSelect ('SalesUserId style="width:250px"', $User['Id'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM User WHERE User.AgentCompanyId=%d AND User.Active=1',$User['AgentCompanyId']))) ;  
		} else {
			$_userid = $User['AvatarId']>0 ? $User['AvatarId'] : $User['Id'] ;
			printf ("<tr><td class=itemlabel>Sales Ref</td><td>%s</td></tr>\n", htmlDBSelect ('SalesUserId style="width:250px"', $_userid, sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM User WHERE User.Id=%d AND User.Active=1',$_userid))) ;  
		}
	} else {
		printf ("<tr><td class=itemlabel>Sales Ref</td><td>%s</td></tr>\n", htmlDBSelect ('SalesUserId style="width:250px"', $Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=%d AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 ORDER BY Value',$MyCompanyId))) ;  
    }
	print htmlItemSpace() ;
	if (!($User['SalesRef']))
		printf ("<tr><td class=itemlabel>Agent</td><td>%s</td></tr>\n", htmlDBSelect ('AgentId style="width:250px"', $Record['AgentId'], 
			sprintf('SELECT 0 as Id, "-- None --" AS Value UNION SELECT Company.Id, CONCAT(Company.Name," "," (",Company.Number,")") AS Value FROM Company WHERE  Company.TypeAgent=1 AND Company.Active=1 AND Company.ToCompanyId=%d ORDER BY Value',$MyCompanyId))) ;  
//		printf ("<tr><td class=itemlabel>Agent</td><td>%s</td></tr>\n", htmlDBSelect ('AgentId style="width:250px"', $Record['AgentId'], 'SELECT Company.Id, CONCAT(Company.Name," "," (",Company.Number,")") AS Value FROM Company, User WHERE Company.Id=%d AND Company.TypeAgent=1 AND Company.Active=1 ORDER BY Value')) ;  
	if (!$User['SalesRef']) {		
		itemFieldRaw ('Agent Fee', formText ('Fee', $new ? '' : (int)$Record['Fee'], 2, 'text-align:right;') . ' %' . ($new ? ' - Leave blank for default agent fee' : '')) ;
	}
    print htmlItemSpace() ;
	$_tmp = ($User['AgentCompanyId']>0) ? ' And Id='.$Record['CurrencyId'] : '' ;
    printf ("<tr><td class=itemlabel>Currency</td><td>%s</td></tr>\n", htmlDBSelect ('CurrencyId style="width:250px"', $Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1' . $_tmp . ' ORDER BY Value')) ;  
    print htmlItemSpace() ;
	$_tmp = ($User['AgentCompanyId']>0) ? ' And Id='.$Record['PaymentTermId'] : '' ;
    printf ("<tr><td class=itemlabel>Payment Term</td><td>%s</td></tr>\n", htmlDBSelect ('PaymentTermId style="width:250px"', $Record['PaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1' . $_tmp . ' ORDER BY Value')) ;  
    print htmlItemSpace() ;
	$_tmp = ($User['AgentCompanyId']>0) ? ' And Id='.$Record['DeliveryTermId'] : '' ;
    printf ("<tr><td class=itemlabel>Delivery Term</td><td>%s</td></tr>\n", htmlDBSelect ('DeliveryTermId style="width:250px"', $Record['DeliveryTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM DeliveryTerm WHERE Active=1' . $_tmp . ' ORDER BY Value')) ;  
    print htmlItemSpace() ;
	$_tmp = ($User['AgentCompanyId']>0) ? ' And Id='.$Record['CarrierId'] : '' ;
    printf ("<tr><td class=itemlabel>Carrier</td><td>%s</td></tr>\n", htmlDBSelect ('CarrierId style="width:250px"', $Record['CarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1' . $_tmp . ' ORDER BY Value')) ;  
//    itemSpace () ;
    print htmlItemSpace() ;
//    itemSpace () ;
//    itemHeader ('Invoice') ;
	if (!($User['SalesRef'])) {
		printf ("<tr><td class=itemlabel colspan=2><U>Invoice info:</U></td></tr>\n") ;
	//    itemFieldRaw ('Split', formCheckbox ('InvoiceSplit', $Record['InvoiceSplit'])) ;
		itemFieldRaw ('Origin', formCheckbox ('InvoiceOrigin', $Record['InvoiceOrigin'])) ;
		itemFieldRaw ('Composition', formCheckbox ('InvoiceComposition', $Record['InvoiceComposition'])) ;
		itemFieldRaw ('Declaration', formCheckbox ('CustomsDeclaration', $Record['CustomsDeclaration'])) ;
		itemSpace () ;

	//    itemHeader ('Delivery Note') ;
		printf ("<tr><td class=itemlabel colspan=2><U>Packinglist info:</U></td></tr>\n") ;
		itemFieldRaw ('InvoiceHeader', formCheckbox ('InvoiceHeader', $Record['InvoiceHeader'])) ;
		itemFieldRaw ('OrderLineFooter', formCheckbox ('OrderLineFooter', $Record['OrderLineFooter'])) ;
		print htmlItemSpace() ;
	}

    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>CompanyFooter</td><td><textarea name=CompanyFooter style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['CompanyFooter']) ;
    print htmlItemSpace() ;

//    itemHeader ('Alternative Delivery Address') ;
}
	$prefix = 'Delivery' ;
    printf ("<tr><td class=itemlabel colspan=2><U>Alternative delivery address:</U></td></tr>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Company Name</td><td><input type=text name=%sAddress1 size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address1"])) ;
    printf ("<tr><td class=itemlabel>Street</td><td><input type=text name=%sAddress2 size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address2"])) ;
    printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=%sZIP size=15 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."ZIP"])) ;
    printf ("<tr><td class=itemlabel>City</td><td><input type=text name=%sCity size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."City"])) ;
    printf ("<tr><td class=itemlabel>Country</td><td>%s</td></tr>\n", htmlDBSelect ($prefix."CountryId style='width:250px'", $Record[$prefix."CountryId"], "SELECT 0 as Id, '-- None --' AS Value UNION " . $query_c)) ;  
    itemSpace () ;
	$prefix = 'Invoice' ;
    printf ("<tr><td class=itemlabel colspan=2><U>Alternative invoice address:</U></td></tr>\n") ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Company Name</td><td><input type=text name=%sAddress1 size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address1"])) ;
    printf ("<tr><td class=itemlabel>Street</td><td><input type=text name=%sAddress2 size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."Address2"])) ;
    printf ("<tr><td class=itemlabel>ZIP</td><td><input type=text name=%sZIP size=15 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."ZIP"])) ;
    printf ("<tr><td class=itemlabel>City</td><td><input type=text name=%sCity size=50 value=\"%s\"></td></tr>\n", $prefix, htmlentities($Record[$prefix."City"])) ;
    printf ("<tr><td class=itemlabel>Country</td><td>%s</td></tr>\n", htmlDBSelect ($prefix."CountryId style='width:250px'", $Record[$prefix."CountryId"], "SELECT 0 as Id, '-- None --' AS Value UNION " . $query_c)) ;  
    itemSpace () ;

    print htmlItemSpace() ;
    printf ("<tr><td class=itemfield>Comment</td><td><textarea name=Comment style='width:100%%;height:170px;'>%s</textarea></td></tr>\n", $Record['Comment']) ;
    print htmlItemSpace() ;
	
    print htmlItemInfo($Record) ;
    printf ("</table>\n") ;
    printf ("</form>\n") ;

    return 0 ;
?>
