<?php

    require 'lib/save.inc' ;
	Global $User ;

    switch ($Navigation['Parameters']) {
	case 'new':
	    unset ($Record) ;
	    $Id = -1 ;
	    break ;
    }

    function checkfield ($fieldname, $value, $changed) {
		global $Id ;
		switch ($fieldname) {
			case 'Number':
				if (!$changed) return false ;
				if ($number <> '') {
					// Check that number does not allready exist
					$query = sprintf ('SELECT Id FROM Company WHERE Active=1 AND Number="%s" AND Id<>%d', addslashes($value), $Id) ;
					$result = dbQuery ($query) ;
					$count = dbNumRows ($result) ;
					dbQueryFree ($result) ;
					if ($count > 0) return 'Company allready existing' ;
				}
				return true ;
			case 'DeliveryAddress1':
				if (!$changed) return false ;
				if (strlen($value)>0) {
					if (!((int)$_POST['DeliveryCountryId']>0)) 
						return 'Country for delivery adress is mandatory' ;
				}
				return true ;
			default:
				if (!$changed) return false ;
				return true ;
		}
		return false ;
    }
     
    $fields = array (
		'Number'	=> array ('mandatory' => false,	'check' => true),
		'Name'		=> array ('mandatory' => true),
		'Address1'	=> array (),
		'Address2'	=> array (),
		'ZIP'		=> array (),
		'City'		=> array (),
		'CountryId'	=> array ('mandatory' => true,'type' => 'integer'),
		'ContactPerson'	=> array (),
		'DeliveryAddress1'	=> array ('check' => true),
		'DeliveryAddress2'	=> array (),
		'DeliveryZIP'		=> array (),
		'DeliveryCity'		=> array (),
		'DeliveryCountryId'	=> array ('type' => 'integer',	'check' => true),
		'InvoiceAddress1'	=> array (),
		'InvoiceAddress2'	=> array (),
		'InvoiceZIP'		=> array (),
		'InvoiceCity'		=> array (),
		'InvoiceCountryId'	=> array ('type' => 'integer'),
		'PhoneMain'	=> array (),
		'PhoneFax'	=> array (),
		'Mail'		=> array (),
		'MailInvoice'		=> array (),
		'MailBookkeeping'		=> array (),
		'RegNumber'	=> array (),
	//	'Internal'	=> array ('type' => 'checkbox'),
		'TypeCustomer'	=> array ('type' => 'set'),
		'Prospect'	=> array ('type' => 'checkbox'),
		'InvoiceDraft'	=> array ('type' => 'checkbox'),
		'IncludeBatchPick'	=> array ('type' => 'checkbox'),
		'TypeCarrier'	=> array ('type' => 'checkbox'),
		'TypeSupplier'	=> array ('type' => 'checkbox'),
		'ListHidden'	=> array ('type' => 'checkbox'),
		'InvoicePerEmail'	=> array ('type' => 'checkbox'),
		'Shop'	=> array ('type' => 'checkbox'),
		'Webshop'	=> array ('type' => 'checkbox'),
		'Web'	=> array (),
		'Comment'		=> array (),
		'CompanyFooter'		=> array (),
		'SalesUserId'		=> array ('type' => 'integer'),
		'AgentId'		=> array ('type' => 'integer'),
		'CategoryId'		=> array ('type' => 'integer'),
		'CollectionTypeId'		=> array ('type' => 'integer'),
		'PaymentStatusId'		=> array ('type' => 'integer'),
		'DeliveryTermId'	=> array ('type' => 'integer'),
		'CarrierId'		=> array ('type' => 'integer'),
		'CurrencyId'		=> array ('type' => 'integer'),
		'PaymentTermId'		=> array ('type' => 'integer'),
		'VATNumber'		=> array (),
		'InvoiceSplit'		=> array ('type' => 'checkbox'),
		'InvoiceComposition'	=> array ('type' => 'checkbox'),
		'InvoiceOrigin'		=> array ('type' => 'checkbox'),
		'CustomsDeclaration'	=> array ('type' => 'checkbox'),
		'OrderLineFooter'		=> array ('type' => 'checkbox'),
		'InvoiceHeader'		=> array ('type' => 'checkbox'),
		'Surplus'		=> array ('type' => 'integer'),
		'Fee'		=> array ('type' => 'integer'),
		'tocompanyid'		=> array ('tocompanyid' => 'integer',  type => set),
    ) ;
  
	
    switch ($Navigation['Parameters']) {
	case 'neworder' :
	case 'newproposal' :
	case 'new' :
		if ($_POST['Number'] == '') {
			$query = sprintf ('select min(l.number + 1) as start
								from company as l
								left outer join company as r on r.tocompanyid=%d and r.number>600000 and r.number<610000 and l.number + 1 = r.number
								where r.number is null and l.tocompanyid=%d and l.number>600000 and l.number<610000', $User['CompanyId'], $User['CompanyId']) ;
			$result = dbQuery ($query) ;
			$row = dbFetch ($result) ;
			$_POST['Number'] = $row['start'] ;
			dbQueryFree ($result) ;

		}
		if ($User['AgentCompanyId']>0) {
			$_POST['AgentId'] = $User['AgentCompanyId'] ;
			$_POST['Fee'] = (int) tableGetField('Company','Fee', $User['AgentCompanyId']) ;
		} else {
			if ((int)$_POST['AgentId']>0)
				$_POST['Fee'] = ((int)$_POST['Fee'] == '') ? (int)tableGetField('Company','Fee', (int)$_POST['AgentId']) : $_POST['Fee'] ;		
		}
		$fields['tocompanyid']['value'] = $User['CompanyId'] ;
		$fields['TypeCustomer']['value'] = 1 ;
		break;
	default:
		break ;
	}
    // Save record
    $res = saveFields ('Company', $Id, $fields, true) ;
    if ($res) return $res ;

    switch ($Navigation['Parameters']) {
	case 'new' :
	    // View new article
	    require_once 'lib/navigation.inc' ;
	    return navigationCommandMark ('customerview', $Record['Id']) ;
	case 'neworder' :
	    // View new article
	    require_once 'lib/navigation.inc' ;
		navigationSetParameter ('neworder.newcustomer',sprintf('&CompanyId=%s',(int)$Record['Id'])) ;
	    return navigationCommandMark ('sales.ordercart.neworder', $Record['Id']) ;
	case 'newproposal' :
	    // View new article
	    require_once 'lib/navigation.inc' ;
	    return navigationCommandMark ('sales.ordercart.newproposal', $Record['Id']) ;
	default :
	    require_once 'lib/navigation.inc' ;
	    return navigationCommandMark ('customerview', $Record['Id']) ;
    }
    
    return 0 ;    

    
?>
