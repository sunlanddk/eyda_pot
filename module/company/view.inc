<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/list.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/table.inc' ;

    printf ("<table class=item>\n") ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Number</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["Number"])) ;
    printf ("<tr><td class=itemlabel>Name</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["Name"])) ;
    printf ("<tr><td class=itemlabel>Contact</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["ContactPerson"])) ;
    printf ("<tr><td class=itemlabel>Category</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('customercategory','Name',$Record["CategoryId"]))) ;
    printf ("<tr><td class=itemlabel>CollectionType</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('CollectionType','Name',$Record["CollectionTypeId"]))) ;
    $query = sprintf ("SELECT * FROM paymentstatus WHERE Id=%d", $Record['PaymentStatusId']) ;
    $result = dbQuery ($query) ;
	$row = dbFetch($result);
    printf ("<tr><td class=itemlabel>Payment status</td><td class=itemfield bgcolor='#%02X%02X%02X'>%s</td></tr>\n", (int)$row['ValueRed'], (int)$row['ValueGreen'], (int)$row['ValueBlue'],$row["Name"]) ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Phone</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["PhoneMain"])) ;
    if ($Record["PhoneFax"]) printf ("<tr><td class=itemlabel>Fax</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record["PhoneFax"])); 
    else itemField ('Fax', "") ;
    itemField ('Email', $Record['Mail']) ;
    if ($Record['MailBookkeeping'] <> '') itemField ('Email bookkeeping', $Record['MailBookkeeping']) ;
    itemSpace () ;
    itemField ('Reg.Number', $Record['RegNumber']) ;
    print htmlItemInfo($Record) ;
    printf ("</table>\n") ;

    printf ("<br><table class=item><tr><td>\n") ;
    
    printf ("<table class=item>\n") ;
    print htmlItemHeader(NULL, NULL, 'Type') ;
    printf ("<tr><td class=itemlabel>Customer</td><td class=itemfield>%s</td></tr>\n", ($Record['TypeCustomer'])?"yes":"no") ;
    printf ("<tr><td class=itemlabel>Supplier</td><td class=itemfield>%s</td></tr>\n", ($Record['TypeSupplier'])?"yes":"no") ;
    printf ("<tr><td class=itemlabel>Carrier</td><td class=itemfield>%s</td></tr>\n", ($Record['TypeCarrier'])?"yes":"no") ;
    printf ("<tr><td class=itemlabel>Agent</td><td class=itemfield>%s</td></tr>\n", ($Record['TypeAgent'])?"yes":"no") ;
    print htmlItemSpace() ;
//    printf ("<tr><td class=itemlabel>Internal</td><td class=itemfield>%s</td></tr>\n", ($Record['Internal'])?"yes":"no") ;
    printf ("</table>\n") ;

    printf ("<br></td>") ;
    
    if ($Record['TypeCustomer']) {
	printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;
	
    itemStart () ;
	itemHeader('Sales information') ;
        printf ("<tr><td class=itemlabel>Sales Ref</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('User', 'CONCAT(FirstName," ",LastName," (",Loginname,")")', (int)$Record['SalesUserId']))) ;
        printf ("<tr><td class=itemlabel>Agent Company</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('Company', 'CONCAT(Name," "," (",Number,")")', (int)$Record['AgentId']))) ;
        printf ("<tr><td class=itemlabel>Delivery Term</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('DeliveryTerm', 'CONCAT(Description," (",Name,")")', (int)$Record['DeliveryTermId']))) ;
        printf ("<tr><td class=itemlabel>Carrier</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('Carrier', 'Name', (int)$Record['CarrierId']))) ;
		if (!$User['SalesRef']) 		
			printf ("<tr><td class=itemlabel>Agent Fee %%</td><td class=itemfield>%s</td>%s</tr>\n", htmlentities((int)$Record['Fee']), " ") ;
		print htmlItemSpace() ;
		itemField ('Shop', ($Record['Shop']) ? 'yes' : 'No') ;
		itemField ('Webshop', ($Record['Webshop']) ? 'yes' : 'No') ;
		itemField ('Web adress', $Record['Web']) ;
	itemEnd () ;

	printf ("<br></td>") ;
	printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

    itemStart () ;
	itemHeader('Invoice information') ;
	printf ("<tr><td class=itemlabel>Currency</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('Currency', 'CONCAT(Description," (",Name,")")', (int)$Record['CurrencyId']))) ;
	printf ("<tr><td class=itemlabel>Payment Term</td><td class=itemfield>%s</td></tr>\n", htmlentities(tableGetField('PaymentTerm', 'CONCAT(Description," (",Name,")")', (int)$Record['PaymentTermId']))) ;
    printf ("<tr><td class=itemlabel>VAT Number</td><td class=itemfield>%s</td></tr>\n", htmlentities($Record['VATNumber'])) ;
	itemField ('Invoice', ($Record['InvoiceSplit']) ? 'Split' : 'Together') ;
	itemField ('InvoicePerEmail', ($Record['InvoicePerEmail']) ? 'yes' : 'No') ;
    if ($Record['MailInvoice'] <> '') itemField ('Invoices email', $Record['MailInvoice']) ;
	itemField ('Origin', ($Record['InvoiceOrigin']) ? 'Yes' : 'No') ;
	itemField ('Composition', ($Record['InvoiceComposition']) ? 'Yes' : 'No') ;
	itemField ('Declaration', ($Record['CustomsDeclaration']) ? 'Yes' : 'No') ;
	itemEnd () ;

	printf ("<br></td>\n") ;
	printf ("<td style=\"border-left: 1px solid #cdcabb;\">\n") ;

	itemStart () ;
	itemHeader('Packing list information') ;
	itemField ('InvoiceHeader', ($Record['InvoiceHeader']) ? 'Yes' : 'No') ;
	itemField ('OrderLineFooter', ($Record['OrderLineFooter']) ? 'Yes' : 'No') ;
	itemEnd () ;
    }

    printf ("</tr></table>\n") ;
    
    listStart () ;
    listHeader ('Addresses') ;
    listRow () ;
    listHead ('', 80) ;
    listHead ('Company') ;
    if ($Record['TypeCustomer']) {
	listHead ('Delivery') ;
	listHead ('Invoice') ;	
    }
    listRow () ;
    listField ('Name') ;
    listField ($Record['Address1']) ;
    if ($Record['TypeCustomer']) {
	listField ($Record['DeliveryAddress1']) ;
	listField ($Record['InvoiceAddress1']) ;
    }
    listRow () ;
    listField ('Street') ;
    listField ($Record['Address2']) ;
    if ($Record['TypeCustomer']) {
	listField ($Record['DeliveryAddress2']) ;
	listField ($Record['InvoiceAddress2']) ;
    }
    listRow () ;
    listField ('City') ;
    listField ($Record['ZIP'].' '.$Record['City']) ;
    if ($Record['TypeCustomer']) {
	listField ($Record['DeliveryZIP'].' '.$Record['DeliveryCity']) ;
	listField ($Record['InvoiceZIP'].' '.$Record['InvoiceCity']) ;
    }
    listRow () ;
    listField ('Country') ;
    listField (tableGetField('Country','CONCAT(Name," - ",Description)',(int)$Record['CountryId'])) ;
    if ($Record['TypeCustomer']) {
	listField (tableGetField('Country','CONCAT(Name," - ",Description)',(int)$Record['DeliveryCountryId'])) ;
        listField (tableGetField('Country','CONCAT(Name," - ",Description)',(int)$Record['InvoiceCountryId'])) ;
    }
    listEnd () ;

    // Associated users
    $query = sprintf ("SELECT Loginname, CONCAT(FirstName, ' ', LastName) AS FullName FROM User WHERE Active=1 AND Login=1 AND CompanyId=%d", $Record['Id']) ;
    $result = dbQuery ($query) ;
    if (dbNumRows ($result) > 0) {
	// List
	printf ("<br>\n") ;
	listStart () ;
	listHeader ('Associated Users') ;
	listRow () ;
	listHeadIcon () ;
	listHead ('Login', 90) ;
	listHead ('Name') ;
	while ($row = dbFetch($result)) {
	    listRow () ;
	    listFieldIcon ('user.gif', 'userview', $row['Id']) ;
	    listField ($row['Loginname']) ;
	    listField ($row['FullName']) ;
	}
    listEnd () ;
	}
    dbQueryFree ($result) ;

    printf ("<br>") ;

    // Footer
    printf ("<td style=\"border-bottom: 1px solid #cdcabb;\">\n") ;

	itemStart () ;
	itemHeader ('General') ;
	itemFieldText ('Comment', $Record['Comment']) ;
	itemFieldText ('Doc Footer', $Record['CompanyFooter']) ;
	itemEnd () ;
 
    return 0 ;
?>
