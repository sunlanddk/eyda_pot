<?php

    require_once 'lib/html.inc' ;
    require_once 'lib/item.inc' ;
    require_once 'lib/form.inc' ;
    require_once 'lib/parameter.inc' ;

    itemStart () ;
    itemSpace () ;
    itemField ('Company', $Record['Number'] . ', ' . $Record['Name']) ;
    itemSpace () ;
    itemEnd () ;

    $MyCompanyId = parameterGet ('CompanyMy') ;

    formStart () ;

    itemStart () ;
    print htmlItemHeader() ;
    printf ("<tr><td class=itemlabel>Sales Ref</td><td>%s</td></tr>\n", htmlDBSelect ('SalesUserId style="width:250px"', $Record['SalesUserId'], sprintf('SELECT User.Id, CONCAT(User.FirstName," ",User.LastName," (",User.Loginname,")") AS Value FROM Company, User WHERE Company.Id=%d AND User.CompanyId=Company.Id AND User.Active=1 AND User.Login=1 ORDER BY Value',$MyCompanyId))) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Delivery Term</td><td>%s</td></tr>\n", htmlDBSelect ('DeliveryTermId style="width:200px"', $Record['DeliveryTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM DeliveryTerm WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Carrier</td><td>%s</td></tr>\n", htmlDBSelect ('CarrierId style="width:250px"', $Record['CarrierId'], 'SELECT Id, Name AS Value FROM Carrier WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Currency</td><td>%s</td></tr>\n", htmlDBSelect ('CurrencyId style="width:150px"', $Record['CurrencyId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM Currency WHERE Active=1 ORDER BY Value')) ;  
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>Payment Term</td><td>%s</td></tr>\n", htmlDBSelect ('PaymentTermId style="width:200px"', $Record['PaymentTermId'], 'SELECT Id, CONCAT(Description," (",Name,")") AS Value FROM PaymentTerm WHERE Active=1 ORDER BY Value')) ;  
    itemSpace () ;
    itemFieldRaw ('Surplus', formText ('Surplus', (int)$Record['Surplus'], 2, 'text-align:right;') . ' %') ;
    print htmlItemSpace() ;
    printf ("<tr><td class=itemlabel>VAT Number</td><td><input type=text name=VATNumber size=20 maxlength=20 value=\"%s\"></td></tr>\n", htmlentities($Record['VATNumber'])) ;
    itemSpace () ;
    itemHeader ('Invoice') ;
    itemFieldRaw ('Split', formCheckbox ('InvoiceSplit', $Record['InvoiceSplit'])) ;
    itemFieldRaw ('Origin', formCheckbox ('InvoiceOrigin', $Record['InvoiceOrigin'])) ;
    itemFieldRaw ('Composition', formCheckbox ('InvoiceComposition', $Record['InvoiceComposition'])) ;
    itemFieldRaw ('Declaration', formCheckbox ('CustomsDeclaration', $Record['CustomsDeclaration'])) ;
    itemSpace () ;

    itemHeader ('Delivery Note') ;
    itemFieldRaw ('InvoiceHeader', formCheckbox ('InvoiceHeader', $Record['InvoiceHeader'])) ;
    itemFieldRaw ('OrderLineFooter', formCheckbox ('OrderLineFooter', $Record['OrderLineFooter'])) ;
    itemEnd () ;

    formEnd () ;

    return 0 ;
?>
