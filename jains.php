<?php
//test_mode = 'true';
//make a return label on shipment/order
//either return, or shipment if not possible
//new shipment with is_return = 'true' with reference to a past order/shipment
//depending on the shop, (1 or 2), the return address is different

require_once('lib/config.inc');
require_once('lib/file.inc');
require_once('lib/table.inc');
require_once('lib/inventory.inc');
require_once('lib/db.inc');
require_once('lib/http.inc');

$Record = array(
	'Id'	=> 4,
	'LabelUrl' => 'https://eyda-aps.api.webshipper.io/v2/labels/',
	'LabelId'	=> 165277
);


$ch = curl_init($Record['LabelUrl'].$Record['LabelId']);
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer 5f1ec30a4aee3be406c9160070bd5929667d19b7f5843fdc5cac67b1239b9a76', 'Content-Type: application/vnd.api+json'));
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);    
$resultObj = json_decode($result, true);

$pdfdoc =  base64_decode($resultObj['data']['attributes']['base64']);
httpNoCache ('pdf') ;
httpContent ('application/pdf', sprintf('Order%06d.pdf', (int)$Record['Id']), strlen($pdfdoc)) ;
print ($pdfdoc) ;

return 0;
die();

//$order_id - Webshipper order id, used to get order attributes via 'relationships' 
function createReturnShipment($order_id){
	$data = array(
		'data' => array(
			'type' 						=> 'shipments',
			'attributes' 				=> array(
				'reference' 			=> 'DEV '.$order_id.' TEST TEST TETS',
				'packages'				=> [],
				// 'test_mode' 			=> true,
				'is_return' 			=> true,				
			),
			'relationships' 	=> array(
	            'order' 		=> array(
	                'data' 		=> array(
	                    'id' 	=> $order_id,
	                    'type' 	=> 'orders'
	                )
	            )
	        )
		)
	);

	$url = 'https://eyda-aps.api.webshipper.io/v2/shipments?include=labels';
	$ch = curl_init($url);

	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer 5f1ec30a4aee3be406c9160070bd5929667d19b7f5843fdc5cac67b1239b9a76', 'Content-Type: application/vnd.api+json'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch); 
	$ch = null;

	

	$resultObj = json_decode($result, true);
	// echo $result;

	// print_r($resultObj);
	// die();

	if(isset($resultObj['included']) === true){
		foreach ($resultObj['included'] as $key => $label) {
			if($label['type'] == 'labels'){
				$shipmentId = $label['attributes']['shipment_id'];
				$webshipperLabelId = (int)tableGetFieldWhere('webshipperlabel', 'Id',  sprintf("Active=1 AND StockId=%d AND LabelId=%d", $shipmentId, $label['id']));
				$post = array(
					'StockId' => $shipmentId,
					'LabelId' => $label['id'],
					'LabelUrl' => $label['links']['self'],
					'Type' => 'Return' //FIX in enum in DB `webshipperlabel`
				);
				tableWrite('webshipperlabel', $post, $webshipperLabelId);
				//uploadWebshipperLabel($shipmentId);
			}
		}		
	}
	else{
		return 'An error occured when creating shipment in webshipper:' . json_encode($result);
	}
	return true;
}
$test_order_id = 69086;
$test = createReturnShipment($test_order_id);
//print_r($test);
?>