<?php


require "lib/config.inc" ;

// Initialization
$Error = false ;
list($usec, $sec) = explode(" ",microtime()) ;
$startexec = (float)$usec + (float)$sec ;
$PermMap = array ("r"=>"PermRead", "w"=>"PermWrite", "d"=>"PermDelete", "c"=>"PermCreate") ;
$PermMapOther = array ("r"=>"PermReadOther", "w"=>"PermWriteOther", "d"=>"PermDeleteOther", "c"=>"PermCreate") ;
$PermFields = array ("PermRead", "PermWrite", "PermCreate", "PermDelete", "PermReadOther", "PermWriteOther", "PermDeleteOther") ;
$NavigationParam = array () ;

require "lib/db.inc" ;

// Session
if (empty($_COOKIE["UID"])) {
  // Establish new session
  include "module/main/session.inc" ;
}
$uid = $_COOKIE["UID"] ;
$query = sprintf ("SELECT Id, CreateDate, IP FROM Session WHERE UID='%s'", $uid) ;
$result = dbQuery ($query) ;
$Session = dbFetch ($result) ;
dbQueryFree ($result);
if (!$Session) {
  require_once "lib/log.inc" ;
  logPrintf (logWARNING, "session not found, IP %s, uid %s", $_SERVER["REMOTE_ADDR"], $uid) ;
  include "module/main/session.inc" ;
}
if ($Session["IP"] != $_SERVER["REMOTE_ADDR"]) {
  require_once "lib/log.inc" ;
  logPrintf (logWARNING, "session mismatch, IP %s, uid %s", $_SERVER["REMOTE_ADDR"], $uid) ;
  include "module/main/session.inc" ;
}

// Get login
$query = sprintf ("SELECT Login.* FROM (Login) WHERE SessionId=%d AND Active=1", $Session["Id"]) ;
$result = dbQuery ($query) ;
$Login = dbFetch($result) ;
dbQueryFree ($result) ;
if ($Login and dbDateDecode($Login["AccessDate"]) < (time()-60*120)) {
  include "module/main/logout.inc" ;
  unset ($Login) ;
}

// Do Login ?
if (!$Login) {
  include "module/main/login.inc" ;
  if ($Config['debugDump']) {
    require_once './lib/log.inc' ;
    logDump () ;
  }
  exit ;
}

// Navigation reference
if ($_POST["nid"] > 0) $nid = (int)$_POST["nid"] ;
else if ($_GET["nid"] > 0) $nid = (int)$_GET["nid"] ;
else $nid = 1 ;

// Get navigation information
$q = 'SELECT Navigation.*, NavigationType.Mark AS Type, NavigationType.LayoutRaw, NavigationType.LayoutMain, NavigationType.LayoutPopup, NavigationType.LayoutSubMenu, Function.PermRecord, Function.PermPublic, Function.PermAdminSystem, Function.PermAdminProject, Function.PermProjectRole, Function.Mark AS FunctionMark FROM (Navigation, NavigationType, Function) WHERE %s AND Navigation.FunctionId=Function.Id AND Function.Active=1 AND Navigation.Active=1 AND Navigation.NavigationTypeId=NavigationType.Id' ;
$query = sprintf ($q, 'Navigation.Id='.$nid) ;
$result = dbQuery ($query) ;
$Navigation = dbFetch ($result) ;
dbQueryFree ($result);
if ($Navigation['Type'] == 'link') {
  $p = $Navigation ;
  $query = sprintf ($q, 'Navigation.Mark="'.$Navigation['Function'].'"') ;
  $result = dbQuery ($query) ;
  $Navigation = dbFetch ($result) ;
  dbQueryFree ($result);
  if ($Navigation) {

    if ($p['Header'] != '') $Navigation['Header'] = $p['Header'] ;
    if ($p['Parameters'] != '') $Navigation['Parameters'] = $p['Parameters'] ;
    $Navigation['MustLoad'] = $p['MustLoad'] ;
    $Navigation['RefIdInclude'] = $p['RefIdInclude'] ;
  }
}
if (!$Navigation) {
  require_once "lib/log.inc" ;
  logPrintf (logFATAL, "no module, nid %d\n", $nid) ;
  if ($Config['debugDump']) {
    require_once './lib/log.inc' ;
    logDump () ;
  }
  exit ;
}
if (strlen($Navigation["Module"]) == 0) {
  require_once "lib/log.inc" ;
  logPrintf (logFATAL, "no module, nid %d\n", $nid) ;
  if ($Config['debugDump']) {
    require_once './lib/log.inc' ;
    logDump () ;
  }
  exit ;
}

// Get user information
$query = sprintf ("SELECT User.*, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, Company.Name AS CompanyName, Company.Internal AS Internal, TimeZone.Name AS TimeZone, Role.Name AS RoleName FROM User LEFT JOIN Company ON User.CompanyId=Company.Id LEFT JOIN TimeZone ON User.TimeZoneId=TimeZone.Id LEFT JOIN Role ON Role.Id=User.RoleId AND Role.Active=1 WHERE User.Id=%d AND User.Active=1 AND User.Login=1", $Login["UserId"]) ;
$result = dbQuery ($query) ;
$User = dbFetch($result) ;
dbQueryFree ($result) ;
if (!$User) {
  require_once "lib/log.inc" ;
  logPrintf (logFATAL, "user not found, id %d", $Login["UserId"]) ;
  if ($Config['debugDump']) {
    require_once './lib/log.inc' ;
    logDump () ;
  }
  exit ;
}


// Timezone
if ($User["TimeZone"]) {
  putenv (sprintf("TZ=%s", $User["TimeZone"])) ;
  $_ENV["TZ"] = $User["TimeZone"] ;
}

// Id
if ($Navigation["Type"]=="command") $Id = (int)$_POST["id"] ;
else if (isset($_GET["id"])) $Id = (int)$_GET["id"] ;
else $Id = -1 ;


$roleid = $User['RoleId'] ;
if ($roleid == 0) $Error = "No Role selecetd for user" ;

// Load Permissions
$query = sprintf ("SELECT Permission.* FROM Permission WHERE Permission.RoleId=%d AND Permission.Active=1 AND Permission.FunctionId=%d", $roleid, $Navigation['FunctionId']) ;
$result = dbQuery ($query) ;
$Permission = dbFetch ($result) ;
dbQueryFree ($result) ;
if ($Permission['RoleId'] != $roleid) $Permission['RoleId'] = $roleid ;

// Load application record
if (!$Error) {
  $name = sprintf("module/%s.inc", $Navigation['Module']); //the new way
  if (file_exists ($name)) {
    $Error = include $name ;
    if (!$Error)$Application = new $Navigation['Module'];
  } else {												//the old way
    $name = sprintf ("module/%s/load.inc", $Navigation["Module"]) ;
    if (file_exists ($name)) {
      $Error = include $name ;
      if (!$Error) {
	if (is_array($Record)) {
	  foreach ($Record as $key => $val) {
	    $Application->$key = $val;
	  }
	}
      }
    }
  }
}


// Verify that record has been loaded
if (!$Error and ($Navigation['MustLoad'] or $Navigation['PermInternal'] or $Navigation['PermOwner'] or $Navigation['PermRecord'])) {
  if (!($Id > 0) or $Id != $Application->Id) $Error = sprintf ("record not found, nid %d, id %d, appId %d, rid %d", $nid, $Id, $Application->Id, $Record['Id']) ;
}


// Permissions
if (!$Error) {
  if (!$User["AdminSystem"]) {
    if ($Navigation["PermAdminSystem"]) {
      $Error = "You do not have access to this function (index, step 0)" ;
    } else {
      if (!$User["AdminProject"]) {
	if ($Navigation["PermAdminProject"]) {
	  $Error = "You do not have access to this function (index, step 1)" ;
	} else {
	  if (!$Navigation["PermPublic"]) {
	    if ($Navigation["PermInternal"] and !$User["Internal"] and !$Application->External) {
	      $Error = "You do not have permission to access this function (index, step 2)" ;
	    } else {
	      if ($Navigation["PermOwner"] and $Application->CreateUserId != $User["Id"]) {
		if (!$Permission[$PermMapOther[$Navigation["Operation"]]]) {
		  $Error = "You do not have permission to access this function (index, step 3)" ;
		}
	      } else {
		if (!$Permission[$PermMap[$Navigation["Operation"]]]) {
		  $Error = "You do not have permission to access this function (index, step 4)" ;
		}
	      }
	    }
	  }
	}
      }
    }
  }
}

// Update status
if (!$Error) {

  // Access information
  $query = sprintf ("UPDATE Login SET AccessDate='%s' WHERE Id=%d AND Active=1", dbDateEncode(time()), $Login["Id"]) ;
  dbRawQuery ($query) ;
}

// make function name
if (!$Error) {
  $funcname = 'F' . preg_replace('/\-c/', 'C', $Navigation['Function']);
}

// Command execution
if (!$Error and $Navigation["Type"]=="command") {
  $Id = (int)$_POST["id"] ;

  if (method_exists($Application, $funcname)) {
    $Error = $Application->$funcname();
  } else {
    $Error = include sprintf ("module/%s/%s.inc", $Navigation["Module"], $Navigation["Function"]) ;
  }
  if (!is_string($Error)) {
    switch ($Error) {
    case 0 :
      if (headers_sent()) {
	$Error = 'Header allready send, stopping execution' ;
	break ;
      }

      // On success, use browser history to step back
      printf ("<script type='text/javascript' src='lib/app.js'></script>\n") ;
      printf ("<script type='text/javascript'>\nappCommandEnd(%d)\n</script>\n", $Navigation["Back"]) ;

    case -1 :
      // next step handled by application (ex. navigationCommandMark)
      if ($Config['debugDump']) {
	require_once './lib/log.inc' ;
	logDump () ;
      }
      exit ;

    default :
      $Error = 'code ' . (int)$Error ;
      break ;
    }
  }
}

// Handle raw layout
if (!$Error and $Navigation["LayoutRaw"]) {
  if (method_exists($Application,$funcname) ) {
    $Error = $Application->$funcname();
  } else {
    $Error = include sprintf ("module/%s/%s.inc", $Navigation["Module"], $Navigation["Function"]) ;
  }
}

// Display Errors properly for raw pages
if ($Error and !headers_sent() and $Navigation["LayoutRaw"]) {
  $Navigation["LayoutRaw"] = 0 ;
  if (!$Navigation['LayoutPopup']) $Navigation["LayoutMain"] = 1 ;
}

// Menus
function navigationItems (&$menuarray, $parentid, $type, $mark=null) {
  global $Permission, $User, $Project, $NavigationParam, $PermFields, $PermMap ;
  $query = "SELECT Navigation.*, NavigationType.Mark AS Type, NavigationType.LayoutPopup, Function.PermRecord, Function.PermPublic, Function.PermAdminSystem, Function.PermAdminProject, Function.PermProjectRole" ;
  foreach ($PermFields AS $perm) $query .= ", Permission.".$perm ;
  $query .= " FROM (Navigation, NavigationType, Function)" ;
  $query .= sprintf (" LEFT JOIN Permission ON Navigation.FunctionId=Permission.FunctionId AND Permission.Active=1 AND ((Permission.RoleId=%d AND (NOT Function.PermProjectRole)) OR (Permission.RoleId=%d AND Function.PermProjectRole)) AND Permission.Active=1", $User['RoleId'], $Project['RoleId']) ;
  $query .= " WHERE" ;
  if ($parentid > 0) $query .= " Navigation.ParentId=".$parentid." AND"  ;
  if (!is_null($mark)) $query .= " Navigation.Mark='".$mark."' AND" ;
  if (!is_null($type)) $query .= " Navigation.RefType='".$type."' AND" ;
  $query .= " Navigation.Active=1 AND Navigation.NavigationTypeId=NavigationType.Id AND Navigation.FunctionId=Function.Id AND Function.Active=1" ;
  $query .= " ORDER BY Navigation.DisplayOrder" ;
  $result = dbQuery ($query) ;
  while ($row = dbFetch ($result)) {
    if ($row["Disable"] and !$NavigationParam[$row["Mark"]]["Enable"] and is_null($mark)) continue ;
    if (!$User["AdminSystem"]) {
      if ($row["PermAdminSystem"]) continue ;
      if (!$User["AdminProject"]) {
	if ($row["PermAdminProject"]) continue ;
	if (!$row["PermPublic"]) {
	  if ($row["PermRecord"] and $row["RefType"] != "m") {
	    if (!$Permission[$PermMap[$row["Operation"]]]) continue ;
	  } else {
	    if (!$row[$PermMap[$row["Operation"]]]) continue ;
	  }
	}
      }
    }
    $menuarray[((int)$row["Id"])] = $row ;
  }
  dbQueryFree ($result);
}

function navigationParameters (&$row, $id=0, $param=null) {
  global $Navigation, $NavigationParam, $Application, $nid ;

  if (isset($NavigationParam[$row["Mark"]]["Parameter"])) {
    if (is_null($param)) {
      $param = $NavigationParam[$row["Mark"]]["Parameter"] ;
    } else {
      $param .= '&' . $NavigationParam[$row["Mark"]]["Parameter"] ;
    }
  }

  switch ($row["Type"]) {
  case 'back':
    $func = sprintf ("appBack(%d)", $row["Back"]) ;
    break ;

  case 'submit':
    if ($row['Function'] != '') {
      $query = sprintf ("SELECT Id FROM Navigation WHERE Mark='%s'", $row['Function']) ;
      $result = dbQuery ($query) ;
      $r = dbFetch ($result) ;
      dbQueryFree ($result) ;
      $row['Id'] = $r['Id'] ;
    } else {
      $row['Id'] = $row['ParentId'] ;
    }
    // Fall through

  case 'command':
    $func = sprintf("appSubmit(%d,%s,%s)", $row["Id"], ($row["RefIdInclude"])?"id":"null", ($row["RefConfirm"]!="") ? "'".$row["RefConfirm"]."'" : "null") ;
    break ;

  case 'main':
  case 'raw':
  case 'explore':
    $func = sprintf("appLoadLegacy(%s,%d,%s%s)",($row['Legacy']?'true':'false'), $row["Id"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "" : (",'".$param."'")) ;
    break ;

  case 'parent':
    $func = sprintf("appLoad(%d,%s%s)", $Navigation["ParentId"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "" : (",'".$param."'")) ;
    $id = $Application->ParentId ;	// Some what very dirty !!
    break ;

  case 'reload':
    $func = sprintf("appLoadLegacy(%s,%d,%s%s)",($row['Legacy']?'true':'false'), $nid, 'id', (is_null($param)) ? "" : (",'".$param."'")) ;
    break ;

  case 'popup':
  case 'rawpopup':
    $func = sprintf("appLoadLegacy(%s,%d,%s,%s,%d,%d)",($row['Legacy']?'true':'false'), $row["Id"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "null" : ("'".$param."'"), $row["Width"], $row["Height"]) ;
    break ;

  case 'view':
    $func = sprintf("window.open('index.php?nid=%d&id='+id%s,'view','')", $row["Id"], (is_null($param)) ? "" : "+'&".$param."'") ;
    break ;

  case 'link':
    $r = array () ;
    navigationItems ($r, NULL, NULL, $row['Function']) ;
    $n = current($r) ;
    $n['Id'] = $row['Id'] ;
    $n['Parameter'] = $row['Parameter'] ;
    $n['RefIdInclude'] = $row['RefIdInclude'] ;
	$n['Legacy'] = $row['Legacy'];
    return navigationParameters ($n, $id, $param) ;

  case 'script':
    $func = $row['Function'] ;
    break ;

  case 'none':
  default:
    $func = '' ;
  }

  return sprintf ("'%s',%s", addslashes($func), ($id > 0) ? $id : "null") ;
}

function htmlOnClick ($func, &$row, $id=0, $param=null) {
  global $NavigationParam ;
  if ($NavigationParam[$row["Mark"]]["Pasive"]) return "" ;
  return sprintf ("onclick=\"%s(event,%s)\"", $func, navigationParameters ($row, $id, $param)) ;
}

if (!$Navigation["LayoutRaw"]) {
  // HTTP Header
  if (!headers_sent()) {
    switch ($_SERVER["SERVER_PROTOCOL"]) {
    case "HTTP/1.1":
      //	    	    header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
      //	    	    header ("Last-Modified: " . gmdate("D, d M Y H:i:s", time() + 12*60*60) . " GMT");
      header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
      break ;

    case "HTTP/1.0":
      header ("Pragma: no-cache") ;
      break ;
    }
  }

  // HTML page header
  printf ("<html>\n<head>\n<title>PassOnTex - %s [%s]</title>\n", $Navigation["Header"], $Config['Instance']) ;
  printf ("<link href='layout/default/styles.css' rel='stylesheet' type='text/css'>\n") ;
  printf ("<script type='text/javascript' src='lib/app.js'></script>\n") ;
  printf ("<link rel='stylesheet' type='text/css' href='layout/default/menu.css'>\n") ;
printf ('<link rel="stylesheet" type="text/css" media="print" href="layout/default/print.css">') ;
  //printf ("<script type='text/javascript' src='lib/menu.js'></script>\n") ;
  printf ("<script type='text/javascript' src='lib/kmenu.js'></script>\n") ;


  // HTML Page Body
  printf ("</head>\n<body onload='appLoaded()'>\n") ;
  printf ("<!--\n");
  printf ("id: %d, nid: %d \n ApId: %d, NavId: %d \n -->", $Id, $nid, $App->Id, $Navigation['Id']);
  printf ("<table height=100%% width=100%%>\n") ;

  // Application header, line 1
  printf ("<tr><td width=100%%>\n") ;
  printf ($Config["HTMLHeader"], $Navigation["Header"]) ;
  printf ("</td></tr>\n") ;
    printf ("<tr><td>\n<div class=bar id=mainmenu></div>\n</td></tr>\n") ;
  if ($Navigation["LayoutMain"]) {
    // Menu Bar, line 2


    // Generate menu structure
    $menu = array () ;
    navigationItems ($menu, -1, "m") ;
    $child = array () ;
    foreach ($menu as $row) $child[((int)$row["ParentId"])][] = (int)$row["Id"] ;

    // Output menu structure
    // [icon,title,null,[func,id], .....submenus		]
    function menustruct ($parentid, $level) {
      global $menu, $child ;
      if ($level++ > 5) return ;
      $n = 0 ;
      if ($child[$parentid]) {
	foreach ($child[$parentid] as $id) {
	  printf ("%s\n%s[%s,'%s',%s,%s", ($level>1 or $n++>0)?",":"", str_repeat("\t", $level), ($menu[$id]["Icon"] != "") ? "'".$menu[$id]["Icon"]."'" : "null", $menu[$id]["Name"], "null", ($menu[$id]["Module"] != "")?"[".navigationParameters($menu[$id], null)."]":"null") ;
	  menustruct ($id, $level) ;
	  printf ("\n%s]", str_repeat("\t", $level)) ;
	}
      }
    }
    printf ("<script type='text/javascript'>\nvar MainMenu =\n[") ;
    menustruct (0,0) ;
    printf ("\n];\n\nkmDraw ('mainmenu', MainMenu, 'k');\n</script>\n") ;
    unset ($child) ;
    unset ($menu) ;
		
    // Status Bar, line 3
    //printf ("<tr><td>\n<div class=bar>\n<table height=100%%>\n<tr>\n") ;

    // Status Bar, line 3, User Id
    //printf ("<td class=barfield style='color:#7f7c73;'>User</td>\n<td class=barfield style='padding-left:0px'>%s</td>\n", $User["Loginname"]) ;
    //printf ("<td class=barfield style='color:#7f7c73;'>Role</td><td class=barfield style='padding-left:0px'>%s</td>\n", $User['RoleName']) ;
    //if ($User["AdminSystem"]) printf ("<td class=barfield style='padding-left:0px'>(administrator)</td>\n") ;
    //printf ("<td class=barfield><div class=barsplit></div></td>\n") ;
		

    // Status Bar, line 3, application
    //		if (!$Error) {
    //$name = 'module/main/status.inc' ;
    //if (file_exists ($name)) {
    //	$Error = include $name ;
    //}
    //}

    // Status Bar, line 3, End
    //printf ("</tr>\n</table>\n</div>\n</td>\n</tr>\n") ;
  }

  // Tool Bar, line 4
  printf ("<tr><td>\n<div class=bar>\n<table>\n<tr>\n") ;

  $buttoncount = 0 ;
  if (!$Error) {
    // Buttons
    unset ($rows) ;
    navigationItems ($rows, $Navigation["Id"], "t") ;
    if (isset($rows)) {
      printf ("<td style='height:29px;vertical-align:middle;'><div id=toolmenu></div></td>\n") ;
      printf ("<script type='text/javascript'>\nvar ToolMenu = [\n") ;

      foreach ($rows AS $row) {
	if ($buttoncount > 0) printf (",\n") ;
	printf ("\t[%s,'%s',%s,%s", ($row["Icon"] != "") ? "'".$row["Icon"]."'" : "null", $row["Name"], (trim($row['AccessKey']) == '') ? 'null' : "'".strtoupper($row['AccessKey'])."'", ($row['Type'] != 'none' and !$NavigationParam[$row["Mark"]]["Pasive"])?"[".navigationParameters($row, $Id)."]":"null") ;
	if ($row['Type'] == 'none') {
	  // Button drop down menu
	  unset ($subbuttons) ;
	  navigationItems ($subbuttons, $row["Id"], "t") ;
	  if (isset($subbuttons)) {
	    foreach ($subbuttons as $button) {
	      printf (",\n\t\t[%s,'%s',null,%s]", ($button["Icon"] != "") ? "'".$button["Icon"]."'" : "null", $button["Name"], (!$NavigationParam[$button["Mark"]]["Pasive"])?"[".navigationParameters($button, $Id)."]":"null") ;
	    }
	  }
	}

	printf ("]") ;
	$buttoncount++ ;
      }

      printf ("\n] ;\nkmDraw ('toolmenu', ToolMenu, 'm');\n</script>\n") ;
    }
    unset ($rows) ;
    unset ($subbuttons) ;
  }

  if ($Error or $buttoncount == 0) {
    if ($Navigation["LayoutPopup"]) printf ("<td class=barbutton><table onclick='appBack(%d)'><tr><td class=barlabel><img src='image/toolbar/close.gif'></td><td class=barlabel>Close</td></tr></table></td>\n", ($Navigation["LayoutPopup"]) ? 0 : $Navigation["Back"]) ;
    printf ("<td class=barbutton><table onclick='history.back()'><tr><td class=barlabel><img src='image/toolbar/back.gif'></td><td class=barlabel>Back</td></tr></table></td>\n") ;
  }

  // Tool Bar end
  printf ("</tr>\n</table>\n</div>\n<div class=\"shadowline\"></div></td></tr>\n") ;
  // shadow
  //printf ("<tr class=\"shadowline\"><td>&nbsp;</td></tr>");

  // Application window
  printf ("<tr>\n<td height=100%%>\n<table height=100%% width=100%%>\n<tr>\n") ;

  // Sub navigation
  if ($Navigation["LayoutSubMenu"]) {
    include "module/".$Navigation["Module"]."/submenu.inc" ;
  }

  // App Window page
  printf ("<td class='main' height=100%% valign='top'>\n") ;

  // Function
  if (!$Error) {
    if (method_exists($Application,$funcname) ) {
      $Error = $Application->$funcname();
    } else {
      $Error = include sprintf ("module/%s/%s.inc", $Navigation["Module"], $Navigation["Function"]) ;
    }
  }

}

if ($Error) {
  require_once './lib/log.inc' ;
  logPrintf (logERROR, $Error) ;
  printf ("<br><p><b>Error: %s</b></p>\n", $Error) ;
}

if (!$Navigation["LayoutRaw"]) {
  printf ("</td>\n</tr>\n</table>\n") ;

  if ($Navigation["LayoutMain"]) {
    printf ("</td></tr>\n</table>\n") ;
  }

  if (!$Error) {
    printf ("<script type='text/javascript'>\n") ;
    if ($Navigation['Focus'] != '')  printf ("appFocus ('%s') ;\n", $Navigation['Focus']) ;
    //else printf ("baat('toolmenu') ;\n") ; //ken 28.05.2006
    printf ("appRestore() ;\n") ;
    printf ("</script>\n") ;
  }

  printf ("</body>\n</html>\n") ;
}

if ($Config['debugDump']) {
  require_once './lib/log.inc' ;
  logDump () ;
}
?>
