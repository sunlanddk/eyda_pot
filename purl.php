<?php
   require_once "lib/config.inc";
   require_once "lib/url.inc";

   printf("Base Url: <b>%s</b> <br>", $Config['base_url']);

   if (isLocalPath($Config['fileStore'])) {
      $_imagesPath = $Config['base_url'];
      printf("FileStore path: %s is local.", $Config['fileStore']);
   } elseif (isURL($Config['fileStore'])) {
      $_imagesPath = $Config['fileStore'];
      printf("FileStore path: %s is URL.", $Config['fileStore']);

   } else {
      printf("FileStore path: %s is relative.", $Config['fileStore']);
      //fileStore is relative path
      $_imagesPath = joinPaths($Config['base_url'], $Config['fileStore']);
   }
   print_r("<hr>");
   printf("Image path: <b>%s</b> <br>", $_imagesPath);

   $_image_name = "1500124900_017184201.jpg";

   $url = $_imagesPath."/thumbnails/".$_image_name;
   printf("Image url: <b>%s</b> <br>", $url);

   $url2 = $Config['fileStore']."\\thumbnails\\".$_image_name;
   printf("Image file: <b>%s</b> <br>", $url2);

   if (!file_exists($url2)) {
      print("Image <b>File NOT EXISTS</b>. <br>");
   } else {
      print("Image File exists. <br>");
   }

   $file_headers = @get_headers($url);
   var_dump($file_headers);

   if (!urlExists($url)) {
      print("Image <b>URL NOT EXISTS</b>. <br>");
   } else {
      print("Image URL exists. <br>");
   }

   printf("<img  src='%s' height='80px' width='80px'>", $url);
