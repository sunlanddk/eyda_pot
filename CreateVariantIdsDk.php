<?php 
die();
require_once('lib/config.inc');
require_once('lib/db.inc');
require_once('lib/table.inc');

// DK
//$ch = curl_init('https://f0c1dc1fcc573851a741f993dc31ab42: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@eyda-development.myshopify.com/admin/api/2021-01/products.json?limit=250');
// EU
$ch = curl_init('https://bf1f28a61dbc759b0442f45f68aafa7f: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@eyda-development-eur.myshopify.com/admin/api/2021-04/products.json?limit=250');

curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// DK
//curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: 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", "Content-Type: application/json"));
// EU
curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: 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", "Content-Type: application/json"));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);


// die();

// $file = file_get_contents('products-dk.json');
$json = json_decode($result, true);
?>
<table>
	<thead>
		<tr>
			<th style="text-align:left">Title</th>
			<th style="text-align:left">Id</th>
			<th style="text-align:left">Variant Id</th>
			<th style="text-align:left">SKU</th>
			<th style="text-align:left">Barcode</th>
			<th style="text-align:left">Message</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$products = [];
		foreach ($json['products'] as $key => $product) {
			$shopifyProductId = $product['id'];
			foreach ($product['variants'] as $vkey => $variant) {
				if($variant['sku'] == '' || $variant['sku'] == ' '){
					echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>Sku er tomt</td></tr>';
					// echo $variant['title'].' '.$variant['id'].' '.$variant['sku'];
					// echo '<br/>';
					continue;
				}
				$shopifyVariantId = $variant['id'];
				$iteminventoryId = $variant['inventory_item_id'];
				$variantCodeId = tableGetFieldWhere('variantcode', 'Id', 'Active=1 AND SKU="'.$variant['sku'].'"');
				$articleId = tableGetFieldWhere('variantcode', 'ArticleId', 'Active=1 AND SKU="'.$variant['sku'].'"');
				if((int)$variantCodeId > 0){
					if((int)tableGetFieldWhere('shopifyvariant', 'Id', sprintf("ShopifyshopId=%d AND VariantCodeId=%d AND ArticleId=%d", 1, $variantCodeId, $articleId)) > 0){
						continue;
					}
					
					$post = array(
						"ShopifyshopId" => 2,
						"VariantCodeId" => $variantCodeId,
						"ArticleId" => $articleId,
						"ShopifyVariantId" => $shopifyVariantId,
						"ShopifyProductId" => $shopifyProductId,
						"InventoryItemId" => $iteminventoryId,
					);
					tableWriteCron('shopifyvariant', $post);
					echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>Er opdateret i POT</td></tr>';
				}
				else{
					echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>Sku er ikke tastet i POT</td></tr>';
					// echo $variant['title'].' '.$variant['id'].' '.$variant['sku'];
					// echo '<br/>';
				}
			}
		}

		?>
	</tbody>
</table>
<?php
