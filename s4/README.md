# LATEST CHANGES



## 2023-08-24

* 1 - new migrations

* 2 - frontend .env variable (.env.example) - APP_RETURN_LABEL_GENERATE_DEFAULT_TYPE
* 3 - frontend update dependencies in package.json

* 4 - backend .env variables (.env.example) - MAILER_DSN, MAILER_FROM_ADDRESS, MAILER_FROM_NAME
