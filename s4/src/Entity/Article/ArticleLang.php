<?php

namespace App\Entity\Article;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Article\ArticleLangRepository")
 */
class ArticleLang
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="articleLangs")
     * @ORM\JoinColumn(name="articleid", referencedColumnName="id")
     */
    private $articleid;

    /**
     * @ORM\Column(type="integer")
     */
    private $languageid;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $description;

    /**
     * @ORM\Column(type="string")
     */
    private $webdescription;

    /**
     * @ORM\Column(type="smallint")
     */
    private $onwebshop;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getArticleid(): ?int
    {
        return $this->articleid;
    }

    public function setArticleid(int $articleid): self
    {
        $this->articleid = $articleid;

        return $this;
    }

    public function getLanguageid(): ?int
    {
        return $this->languageid;
    }

    public function setLanguageid(int $languageid): self
    {
        $this->languageid = $languageid;

        return $this;
    }

    public function getOnwebshop(): ?int
    {
        return $this->onwebshop;
    }

    public function setOnwebshop(int $onwebshop): self
    {
        $this->onwebshop = $onwebshop;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getWebdescription(): ?string
    {
        return $this->webdescription;
    }

    public function setWebdescription(string $webdescription): self
    {
        $this->webdescription = $webdescription;

        return $this;
    }
}
