<?php

namespace App\Entity\Article;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="color")
 */
class Color
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=9)
     */
    private $number;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $colorgroupid;

    /**
     * @ORM\Column(type="string", length=200)
     */
    private $altdescription;

    /**
     * @ORM\OneToMany(mappedBy="color", targetEntity=ArticleColor::class)
     */
    private Collection $articleColors;

    public function __construct()
    {
        $this->articleColors = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedate(): ?DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getNumber(): string
    {
        return $this->number;
    }

    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getColorgroupid(): int
    {
        return $this->colorgroupid;
    }

    public function setColorgroupid(int $colorgroupid): self
    {
        $this->colorgroupid = $colorgroupid;

        return $this;
    }

    public function getAltdescription(): string
    {
        return $this->altdescription;
    }

    public function setAltdescription(string $altdescription): self
    {
        $this->altdescription = $altdescription;

        return $this;
    }

    public function getArticleColors(): Collection
    {
        return $this->articleColors;
    }

    public function setArticleColors(Collection $articleColors): self
    {
        $this->articleColors = $articleColors;

        return $this;
    }
}
