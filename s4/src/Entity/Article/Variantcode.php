<?php

namespace App\Entity\Article;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Article\VariantcodeRepository")
 */
class Variantcode
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $articleid;

    /**
     * @ORM\Column(type="integer")
     */
    private $articlecolorid;

    /**
     * @ORM\Column(type="integer")
     */
    private $articlesizeid;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $type;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $sku;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $variantcode;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $shopifyvariantid;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $shopifyproductid;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $inventoryitemid;

    /**
     * @ORM\Column(type="integer")
     */
    private $pickcontainerid;

    /**
     * @ORM\Column(type="decimal", precision=8, scale=4)
     */
    private $weight;

    /**
     * @ORM\Column(type="smallint")
     */
    private $presale;

    public function __construct()
    {
        $this->presale = 0;
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getArticleid(): ?int
    {
        return $this->articleid;
    }

    public function setArticleid(int $articleid): self
    {
        $this->articleid = $articleid;

        return $this;
    }

    public function getArticlecolorid(): ?int
    {
        return $this->articlecolorid;
    }

    public function setArticlecolorid(int $articleid): self
    {
        $this->articlecoorid = $articleid;

        return $this;
    }

    public function getArticlesizeid(): ?int
    {
        return $this->articlesizeid;
    }

    public function setArticlesizeid(int $articlesizeid): self
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getVariantcode(): ?string
    {
        return $this->variantcode;
    }

    public function setVariantcode(string $variantcode): self
    {
        $this->variantcode = $variantcode;

        return $this;
    }

    public function getShopifyvariantid(): ?string
    {
        return $this->shopifyvariantid;
    }

    public function setShopifyvariantid(string $shopifyvariantid): self
    {
        $this->shopifyvariantid = $shopifyvariantid;

        return $this;
    }

    public function getShopifyproductid(): ?string
    {
        return $this->shopifyproductid;
    }

    public function setShopifyproductid(string $shopifyproductid): self
    {
        $this->shopifyproductid = $shopifyproductid;

        return $this;
    }

    public function getInventoryitemid(): ?string
    {
        return $this->inventoryitemid;
    }

    public function setInventoryitemid(string $inventoryitemid): self
    {
        $this->inventoryitemid = $inventoryitemid;

        return $this;
    }

    public function getSku(): ?string
    {
        return $this->sku;
    }

    public function setSku(string $sku): self
    {
        $this->sku = $sku;

        return $this;
    }

    public function getPickcontainerid(): ?int
    {
        return $this->pickcontainerid;
    }

    public function setPickcontainerid(int $pickcontainerid): self
    {
        $this->pickcontainerid = $pickcontainerid;

        return $this;
    }

    public function getWeight(): float
    {
        return $this->weight;
    }

    public function setWeight(?float $weight = 0.0000): self
    {
        $this->weight = $weight;

        return $this;
    }

    public function getPresale(): ?int
    {
        return $this->presale;
    }

    public function setPresale(int $presale): self
    {
        $this->presale = $presale;

        return $this;
    }

}
