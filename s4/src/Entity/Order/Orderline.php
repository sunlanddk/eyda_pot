<?php

namespace App\Entity\Order;

use App\Entity\Article\Article;
use App\Entity\Article\ArticleColor;
use App\Entity\Stock\Item;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\OrderlineRepository")
 */
class Orderline
{
    public const SHIPPING_LINE_ONE = 1;
    public const SHIPPING_LINE_TWO = 2;

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $no;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $done;

    /**
     * @ORM\Column(type="integer")
     */
    private $doneuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $donedate;

    /**
     * @ORM\Column(type="decimal")
     */
    private $pricesale;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deliverydate;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderLines")
     * @ORM\JoinColumn(name="orderId", referencedColumnName="id")
     */
    private Order $order;

    /**
     * @ORM\OneToOne(mappedBy="orderLine", targetEntity=Orderquantity::class)
     */
    private $orderQuantity;

    /**
     * @ORM\ManyToOne(targetEntity=Article::class, inversedBy="orderLines")
     * @ORM\JoinColumn(name="articleId", referencedColumnName="id")
     */
    private Article $article;

    /**
     * @ORM\ManyToOne(targetEntity=ArticleColor::class, inversedBy="orderLines")
     * @ORM\JoinColumn(name="articlecolorid", referencedColumnName="id", nullable=false, columnDefinition="INT NOT NULL DEFAULT 0")
     */
    private ArticleColor $articleColor;

    /**
     * @ORM\OneToMany(mappedBy="orderLine", targetEntity=Item::class)
     */
    private Collection $items;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getNo(): ?int
    {
        return $this->no;
    }

    public function setNo(int $no): self
    {
        $this->no = $no;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getDone(): ?int
    {
        return $this->done;
    }

    public function setDone(int $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getDoneuserid(): ?int
    {
        return $this->doneuserid;
    }

    public function setDoneuserid(int $doneuserid): self
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    public function getDonedate(): ?DateTimeInterface
    {
        return $this->donedate;
    }

    public function setDonedate(DateTimeInterface $donedate): self
    {
        $this->donedate = $donedate;

        return $this;
    }

    public function getPricesale(): ?string
    {
        return $this->pricesale;
    }

    public function setPricesale(string $pricesale): self
    {
        $this->pricesale = $pricesale;

        return $this;
    }

    public function getDeliverydate(): ?DateTimeInterface
    {
        return $this->deliverydate;
    }

    public function setDeliverydate(DateTimeInterface $deliverydate): self
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(?Order $order = null): self
    {
        $this->order = $order;

        return $this;
    }

    public function getOrderQuantity(): OrderQuantity
    {
        return $this->orderQuantity;
    }

    public function setOrderQuantity(OrderQuantity $orderQuantity): self
    {
        $this->orderQuantity = $orderQuantity;

        return $this;
    }

    public function getArticle(): Article
    {
        return $this->article;
    }

    public function setArticle(?Article $article = null): self
    {
        $this->article = $article;

        return $this;
    }

    public function getArticleColor(): ?ArticleColor
    {
        return ($this->articleColor !== 0 && $this->articleColor !== null) ? $this->articleColor : null;
    }

    public function setArticleColor(?ArticleColor $articleColor = null): self
    {
        $this->articleColor = $articleColor;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }
}
