<?php

namespace App\Entity\Order;

use App\Entity\Article\ArticleSize;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\OrderquantityRepository")
 */
class Orderquantity
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string")
     */
    private $shopifyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $webshipperid;

    /**
     * @ORM\OneToOne(targetEntity=Orderline::class, mappedBy="orderQuantity")
     * @ORM\JoinColumn(name="orderLineId", referencedColumnName="id", nullable=false, columnDefinition="INT DEFAULT 0")
     */
    private Orderline $orderLine;

    /**
     * @ORM\ManyToOne(targetEntity=ArticleSize::class, inversedBy="orderQuantities")
     * @ORM\JoinColumn(name="articlesizeid", referencedColumnName="id", nullable=false, columnDefinition="INT DEFAULT 0")
     */
    private ArticleSize $articleSize;

    /**
     * @ORM\OneToMany(mappedBy="orderQuantity", targetEntity=Pickorderline::class)
     */
    private Collection $pickOrderLines;

    public function __construct()
    {
        $this->pickOrderLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getShopifyid(): ?string
    {
        return $this->shopifyid;
    }

    public function setShopifyid(string $shopifyid): self
    {
        $this->shopifyid = $shopifyid;

        return $this;
    }

    public function getWebshipperid(): ?int
    {
        return $this->webshipperid;
    }

    public function setWebshipperid(int $webshipperid): self
    {
        $this->webshipperid = $webshipperid;

        return $this;
    }

    public function getOrderLine(): ?Orderline
    {
        return $this->orderLine;
    }

    public function setOrderLine(?Orderline $orderLine = null): self
    {
        $this->orderLine = $orderLine;

        return $this;
    }

    public function getArticleSize(): ?ArticleSize
    {
        return ($this->articleSize !== 0 && $this->articleSize !== null) ? $this->articleSize : null;
    }

    public function setArticleSize(?ArticleSize $articleSize = null): self
    {
        $this->articleSize = $articleSize;

        return $this;
    }

    /**
     * @return Collection|PickOrderLine[]
     */
    public function getPickOrderLines(): Collection
    {
        return $this->pickOrderLines;
    }
}
