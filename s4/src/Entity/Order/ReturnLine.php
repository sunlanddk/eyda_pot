<?php

namespace App\Entity\Order;

use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\ReturnLineRepository")
 * @ORM\Table(name="returnline")
 */
class ReturnLine
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="createDate", type="datetime")
     */
    private DateTimeInterface $createDate;

    /**
     * @ORM\Column(name="createUserId", type="integer")
     */
    private int $createUserId;

    /**
     * @ORM\Column(name="modifyDate", type="datetime")
     */
    private DateTimeInterface $modifyDate;

    /**
     * @ORM\Column(name="modifyUserId", type="integer")
     */
    private int $modifyUserId;

    /**
     * @ORM\Column(name="active", type="smallint")
     */
    private bool $active;

    /**
     * @ORM\Column(name="quantity", type="integer")
     */
    private int $quantity;

    /**
     * @ORM\Column(name="comment", type="string", length=255, nullable=true)
     */
    private ?string $comment = null;

    /**
     * @ORM\ManyToOne(targetEntity=ReturnEntity::class, inversedBy="returnLines")
     * @ORM\JoinColumn(name="returnId", referencedColumnName="id")
     */
    private ReturnEntity $return;

    /**
     * @ORM\ManyToOne(targetEntity=Orderline::class)
     * @ORM\JoinColumn(name="orderLineId", referencedColumnName="id")
     */
    private Orderline $orderLine;

    /**
     * @ORM\ManyToOne(targetEntity=RefundReason::class, inversedBy="returnLines")
     * @ORM\JoinColumn(name="refundReasonId", referencedColumnName="id")
     */
    private RefundReason $refundReason;

    public function __construct()
    {
        $this->createDate = new DateTime();
        $this->createUserId = 1;
        $this->modifyDate = new DateTime();
        $this->modifyUserId = 1;
        $this->active = 1;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreateDate(): DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getCreateUserId(): int
    {
        return $this->createUserId;
    }

    public function setCreateUserId(int $createUserId): self
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    public function getModifyDate(): DateTimeInterface
    {
        return $this->modifyDate;
    }

    public function setModifyDate(DateTimeInterface $modifyDate): self
    {
        $this->modifyDate = $modifyDate;

        return $this;
    }

    public function getModifyUserId(): int
    {
        return $this->modifyUserId;
    }

    public function setModifyUserId(int $modifyUserId): self
    {
        $this->modifyUserId = $modifyUserId;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getReturn(): ReturnEntity
    {
        return $this->return;
    }

    public function setReturn(ReturnEntity $return): self
    {
        $this->return = $return;

        return $this;
    }

    public function getOrderLine(): Orderline
    {
        return $this->orderLine;
    }

    public function setOrderLine(Orderline $orderLine): self
    {
        $this->orderLine = $orderLine;

        return $this;
    }

    public function getRefundReason(): RefundReason
    {
        return $this->refundReason;
    }

    public function setRefundReason(RefundReason $refundReason): self
    {
        $this->refundReason = $refundReason;

        return $this;
    }

    public function getQuantity(): int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }
}
