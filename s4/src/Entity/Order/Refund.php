<?php

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\RefundRepository")
 */
class Refund
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint", options={"default":"1"})
     */
    private $active;

    /**
     * @ORM\Column(type="string", length=55, options={"default" : ""})
     */
    private $shopifyrefundid;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $orderid;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $ready;

    /**
     * @ORM\Column(type="datetime", options={"default" : "0000-00-00 00:00:00"})
     */
    private $readydate;

    /**
     * @ORM\Column(type="integer", options={"default" : 0})
     */
    private $readyuserid;

    /**
     * @ORM\Column(type="smallint", options={"default" : 0})
     */
    private $done;

    /**
     * @ORM\Column(type="datetime", options={"default" : 0})
     */
    private $donedate;

    /**
     * @ORM\Column(type="integer", options={"default" : "0000-00-00 00:00:00"})
     */
    private $doneuserid;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private $type;

    /**
     * @ORM\Column(type="decimal")
     */
    private $amount;

    /**
     * @ORM\Column(type="string", length=255, options={"default" : ""})
     */
    private $note;

    /**
     * @ORM\OneToOne(mappedBy="refund", targetEntity=ReturnEntity::class)
     */
    private ?ReturnEntity $return = null;

    public function __construct()
    {
        $this->createdate = new \DateTime();
        $this->createuserid = 1;
        $this->modifydate = new \DateTime();
        $this->modifyuserid = 1;
        $this->active = 1;
        $this->shopifyrefundid = '';
        $this->orderid = 0;
        $this->ready = 0;
        $this->readydate = new \DateTime('2000-01-01 00:00:00');
        $this->readyuserid = 0;
        $this->done = 0;
        $this->donedate = new \DateTime('2000-01-01 00:00:00');
        $this->doneuserid = 0;
        $this->amount = '0.00';
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getAmount(): ?string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getOrderid(): ?int
    {
        return $this->orderid;
    }

    public function setOrderid(int $orderid): self
    {
        $this->orderid = $orderid;

        return $this;
    }

    public function getShopifyrefundid(): ?string
    {
        return $this->shopifyrefundid;
    }

    public function setShopifyrefundid(string $shopifyrefundid): self
    {
        $this->shopifyrefundid = $shopifyrefundid;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getNote(): ?string
    {
        return $this->note;
    }

    public function setNote(string $note): self
    {
        $this->note = $note;

        return $this;
    }

    public function getReady(): ?int
    {
        return $this->ready;
    }

    public function setReady(int $ready): self
    {
        $this->ready = $ready;

        return $this;
    }

    public function getReadyuserid(): ?int
    {
        return $this->readyuserid;
    }

    public function setReadyuserid(int $readyuserid): self
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    public function getReadydate(): ?\DateTimeInterface
    {
        return $this->readydate;
    }

    public function setReadydate(\DateTimeInterface $readydate): self
    {
        $this->readydate = $readydate;

        return $this;
    }

    public function getDone(): ?int
    {
        return $this->done;
    }

    public function setDone(int $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getDoneuserid(): ?int
    {
        return $this->doneuserid;
    }

    public function setDoneuserid(int $doneuserid): self
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    public function getDonedate(): ?\DateTimeInterface
    {
        return $this->donedate;
    }

    public function setDonedate(\DateTimeInterface $donedate): self
    {
        $this->donedate = $donedate;

        return $this;
    }


}
