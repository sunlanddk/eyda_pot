<?php

namespace App\Entity\Order;

use App\Entity\Language;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="refundreason_lang")
 */
class RefundReasonLang
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="RefundReason", inversedBy="refundReasonLangs")
     * @ORM\JoinColumn(name="refundreason_id", referencedColumnName="id")
     */
    private RefundReason $refundReason;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Language")
     * @ORM\JoinColumn(name="language_id", referencedColumnName="id")
     */
    private Language $language;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=55, nullable=true)
     */
    private ?string $description = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getRefundReason(): RefundReason
    {
        return $this->refundReason;
    }

    public function setRefundReason(RefundReason $refundReason): self
    {
        $this->refundReason = $refundReason;

        return $this;
    }

    public function getLanguage(): Language
    {
        return $this->language;
    }

    public function setLanguage(Language $language): self
    {
        $this->language = $language;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description = null): self
    {
        $this->description = $description;

        return $this;
    }
}
