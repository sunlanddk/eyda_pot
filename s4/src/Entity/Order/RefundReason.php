<?php

namespace App\Entity\Order;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\RefundReasonRepository")
 * @ORM\Table(name="refundreason")
 */
class RefundReason
{
    public const TYPE_CUSTOMER = 'customer';
    public const TYPE_WAREHOUSE = 'warehouse';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private int $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private DateTime $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private int $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private bool $active;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=55)
     */
    private string $description;

    /**
     * @ORM\Column(type="smallint")
     */
    private int $restock;

    /**
     * @ORM\Column(type="string", length=55, nullable=false)
     */
    private string $type;

    /**
     * @ORM\OneToMany(mappedBy="refundReason", targetEntity=ReturnLine::class)
     */
    private Collection $returnLines;

    /**
     * @ORM\OneToMany(targetEntity=RefundReasonLang::class, mappedBy="refundReason", orphanRemoval=true)
     */
    private Collection $refundReasonLangs;

    public function __construct()
    {
        $this->returnLines = new ArrayCollection();
        $this->createdate = new DateTime();
        $this->createuserid = 1;
        $this->modifydate = new DateTime();
        $this->modifyuserid = 1;
        $this->active = 1;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getRestock(): ?int
    {
        return $this->restock;
    }

    public function setRestock(int $restock): self
    {
        $this->restock = $restock;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        if (!in_array($type, [self::TYPE_WAREHOUSE, self::TYPE_CUSTOMER])) {
            throw new InvalidArgumentException("Invalid type");
        }

        $this->type = $type;

        return $this;
    }

    /**
     * @return Collection|ReturnLine[]
     */
    public function getReturnLines(): Collection
    {
        return $this->returnLines;
    }

    public function addReturnLine(ReturnLine $returnLine): self
    {
        if (!$this->returnLines->contains($returnLine)) {
            $this->returnLines[] = $returnLine;
            $returnLine->setRefundReason($this);
        }

        return $this;
    }

    public function removeReturnLine(ReturnLine $returnLine): self
    {
        if ($this->returnLines->contains($returnLine)) {
            $this->returnLines->removeElement($returnLine);
            // set the owning side to null (unless already changed)
            if ($returnLine->getRefundReason() === $this) {
                $returnLine->setRefundReason(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|RefundReasonLang[]
     */
    public function getRefundReasonLangs(): Collection
    {
        return $this->refundReasonLangs;
    }

    public function addRefundReasonLang(RefundReasonLang $refundReasonLang): self
    {
        if (!$this->refundReasonLangs->contains($refundReasonLang)) {
            $this->refundReasonLangs[] = $refundReasonLang;
            $refundReasonLang->setRefundReason($this);
        }

        return $this;
    }

    public function removeRefundReasonLang(RefundReasonLang $refundReasonLang): self
    {
        if ($this->refundReasonLangs->contains($refundReasonLang)) {
            $this->refundReasonLangs->removeElement($refundReasonLang);
            // set the owning side to null (unless already changed)
            if ($refundReasonLang->getRefundReason() === $this) {
                $refundReasonLang->setRefundReason(null);
            }
        }

        return $this;
    }
}
