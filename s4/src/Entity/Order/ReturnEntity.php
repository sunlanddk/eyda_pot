<?php

namespace App\Entity\Order;

use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use InvalidArgumentException;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\ReturnRepository")
 * @ORM\Table(name="`return`")
 */
class ReturnEntity
{
    public const LABEL_TYPE_DEFAULT = 'qr';
    public const LABEL_TYPE_PDF = 'pdf';
    public const LABEL_TYPE_QR = 'qr';

    public const FUNDS_TYPE_DEFAULT = 'default';
    public const FUNDS_TYPE_REFUND = 'refund';
    public const FUNDS_TYPE_GIFT_CARD = 'gift_card';

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="createDate", type="datetime")
     */
    private DateTimeInterface $createDate;

    /**
     * @ORM\Column(name="createUserId", type="integer")
     */
    private int $createUserId ;

    /**
     * @ORM\Column(name="modifyDate", type="datetime")
     */
    private DateTimeInterface $modifyDate;

    /**
     * @ORM\Column(name="modifyUserId", type="integer")
     */
    private int $modifyUserId;

    /**
     * @ORM\Column(name="active", type="smallint")
     */
    private bool $active;

    /**
     * @ORM\Column(name="trackingCode", type="string", length=255, nullable=true)
     */
    private ?string $trackingCode = null;

    /**
     * @ORM\Column(name="fundsType", type="string", columnDefinition="ENUM('default', 'refund', 'gift_card')")
     */
    private string $fundsType;

    /**
     * @ORM\Column(name="labelType", type="string")
     */
    private string $labelType;

    /**
     * @ORM\Column(name="email", type="string", length=50, nullable=true)
     */
    private ?string $email = null;

    /**
     * @ORM\Column(name="emailSendDate", type="datetime", nullable=true)
     */
    private DateTimeInterface $emailSendDate;

    /**
     * @ORM\Column(name="emailLanguage", type="string", length=50, nullable=true)
     */
    private ?string $emailLanguage = null;

    /**
     * @ORM\Column(name="emailLog", type="text", nullable=true)
     */
    private ?string $emailLog = null;

    /**
     * @ORM\ManyToOne(targetEntity=Order::class, inversedBy="orderLines")
     * @ORM\JoinColumn(name="orderId", referencedColumnName="id")
     */
    private Order $order;

    /**
     * @ORM\OneToOne(targetEntity=Refund::class, mappedBy="return")
     * @ORM\JoinColumn(name="refundId", referencedColumnName="id", nullable=true)
     */
    private ?Refund $refund = null;

    /**
     * @ORM\OneToMany(mappedBy="return", targetEntity=ReturnLine::class, fetch="EAGER")
     */
    private Collection $returnLines;

    public function __construct()
    {
        $this->returnLines = new ArrayCollection();
        $this->createDate = new DateTime();
        $this->createUserId = 1;
        $this->modifyDate = new DateTime();
        $this->modifyUserId = 1;
        $this->active = 1;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreateDate(): DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getCreateUserId(): int
    {
        return $this->createUserId;
    }

    public function setCreateUserId(int $createUserId): self
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    public function getModifyDate(): DateTimeInterface
    {
        return $this->modifyDate;
    }

    public function setModifyDate(DateTimeInterface $modifyDate): self
    {
        $this->modifyDate = $modifyDate;

        return $this;
    }

    public function getModifyUserId(): int
    {
        return $this->modifyUserId;
    }

    public function setModifyUserId(int $modifyUserId): self
    {
        $this->modifyUserId = $modifyUserId;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return Collection<int, ReturnLine>
     */
    public function getReturnLines(): Collection
    {
        return $this->returnLines;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getRefund(): ?Refund
    {
        return $this->refund;
    }

    public function setRefund(?Refund $refund): self
    {
        $this->refund = $refund;

        return $this;
    }

    public function getTrackingCode(): ?string
    {
        return $this->trackingCode;
    }

    public function setTrackingCode(?string $trackingCode): self
    {
        $this->trackingCode = $trackingCode;

        return $this;
    }

    public function getFundsType(): string
    {
        return $this->fundsType;
    }

    public function setFundsType(string $fundsType): self
    {
        if (!in_array($fundsType, [self::FUNDS_TYPE_DEFAULT, self::FUNDS_TYPE_REFUND, self::FUNDS_TYPE_GIFT_CARD], true)) {
            throw new InvalidArgumentException("Invalid funds type");
        }

        $this->fundsType = $fundsType;

        return $this;
    }

    public function getLabelType(): string
    {
        return $this->labelType;
    }

    public function setLabelType(string $labelType): self
    {
        if (!in_array($labelType, [self::LABEL_TYPE_DEFAULT, self::LABEL_TYPE_PDF, self::LABEL_TYPE_QR], true)) {
            throw new InvalidArgumentException("Invalid label type");
        }

        $this->labelType = $labelType;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email = null): self
    {
        $this->email = $email;

        return $this;
    }

    public function getEmailSendDate(): ?DateTimeInterface
    {
        return $this->emailSendDate;
    }

    public function setEmailSendDate(?DateTimeInterface $emailSendDate = null): self
    {
        $this->emailSendDate = $emailSendDate;

        return $this;
    }

    public function getEmailLanguage(): ?string
    {
        return $this->emailLanguage;
    }

    public function setEmailLanguage(?string $emailLanguage = null): self
    {
        $this->emailLanguage = $emailLanguage;

        return $this;
    }

    public function getEmailLog(): ?string
    {
        return $this->emailLog;
    }

    public function setEmailLog(?string $emailLog = null): self
    {
        $this->emailLog = $emailLog;

        return $this;
    }
}
