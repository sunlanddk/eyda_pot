<?php

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\PickorderRepository")
 */
class Pickorder
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $deliverydate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $expecteddate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $packeddate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="string")
     */
    private $type;

    /**
     * @ORM\Column(type="string")
     */
    private $reference;

    /**
     * @ORM\Column(type="integer")
     */
    private $referenceid;

    /**
     * @ORM\Column(type="integer")
     */
    private $consolidatedid;

    /**
     * @ORM\Column(type="integer")
     */
    private $ownercompanyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $handlercompanyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $companyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $fromid;

    /**
     * @ORM\Column(type="integer")
     */
    private $toid;

    /**
     * @ORM\Column(type="string")
     */
    private $instructions;

    /**
     * @ORM\Column(type="integer")
     */
    private $packed;

    /**
     * @ORM\Column(type="integer")
     */
    private $updated;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getDeliverydate(): ?\DateTimeInterface
    {
        return $this->deliverydate;
    }

    public function setDeliverydate(\DateTimeInterface $deliverydate): self
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    public function getExpecteddate(): ?\DateTimeInterface
    {
        return $this->expecteddate;
    }

    public function setExpecteddate(\DateTimeInterface $expecteddate): self
    {
        $this->expecteddate = $expecteddate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getPackeddate(): ?\DateTimeInterface
    {
        return $this->packeddate;
    }

    public function setPackeddate(\DateTimeInterface $packeddate): self
    {
        $this->packeddate = $packeddate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getReferenceid(): ?int
    {
        return $this->referenceid;
    }

    public function setReferenceid(int $referenceid): self
    {
        $this->referenceid = $referenceid;

        return $this;
    }

    public function getConsolidatedid(): ?int
    {
        return $this->consolidatedid;
    }

    public function setConsolidatedid(int $consolidatedid): self
    {
        $this->consolidatedid = $consolidatedid;

        return $this;
    }

    public function getOwnercompanyid(): ?int
    {
        return $this->ownercompanyid;
    }

    public function setOwnercompanyid(int $ownercompanyid): self
    {
        $this->ownercompanyid = $ownercompanyid;

        return $this;
    }

    public function getHandlercompanyid(): ?int
    {
        return $this->handlercompanyid;
    }

    public function setHandlercompanyid(int $handlercompanyid): self
    {
        $this->handlercompanyid = $handlercompanyid;

        return $this;
    }

    public function getCompanyid(): ?int
    {
        return $this->companyid;
    }

    public function setCompanyid(int $companyid): self
    {
        $this->companyid = $companyid;

        return $this;
    }

    public function getFromid(): ?int
    {
        return $this->fromid;
    }

    public function setFromid(int $fromid): self
    {
        $this->fromid = $fromid;

        return $this;
    }

    public function getToid(): ?int
    {
        return $this->toid;
    }

    public function setToid(int $toid): self
    {
        $this->toid = $toid;

        return $this;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function setInstructions(string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    public function getPacked(): ?string
    {
        return $this->packed;
    }

    public function setPacked(string $packed): self
    {
        $this->packed = $packed;

        return $this;
    }

    public function getUpdated(): ?int
    {
        return $this->updated;
    }

    public function setUpdated(int $updated): self
    {
        $this->updated = $updated;

        return $this;
    }





    // public function getArticlecolorid(): ?int
    // {
    //     return $this->articlecolorid;
    // }

    // public function setArticlecolorid(int $articleid): self
    // {
    //     $this->articlecoorid = $articlecolorid;

    //     return $this;
    // }

    // public function getSku(): ?string
    // {
    //     return $this->sku;
    // }

    // public function setSku(string $sku): self
    // {
    //     $this->sku = $sku;

    //     return $this;
    // }

}
