<?php

namespace App\Entity\Order;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\PickorderlineRepository")
 */
class Pickorderline
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $pickorderid;

    /**
     * @ORM\Column(type="integer")
     */
    private $variantcodeid;

    /**
     * @ORM\Column(type="string")
     */
    private $variantcode;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderedquantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $pickedquantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $packedquantity;

    /**
     * @ORM\Column(type="integer")
     */
    private $done;

    /**
     * @ORM\Column(type="string")
     */
    private $variantcolor;

    /**
     * @ORM\Column(type="string")
     */
    private $variantsize;

    /**
     * @ORM\Column(type="string")
     */
    private $variantdescription;

    /**
     * @ORM\ManyToOne(targetEntity=Orderquantity::class, inversedBy="pickOrderLines")
     * @ORM\JoinColumn(name="qlid", referencedColumnName="id")
     */
    private Orderquantity $orderQuantity;

    public function __construct()
    {
        $this->done = 0;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getPickorderid(): ?int
    {
        return $this->pickorderid;
    }

    public function setPickorderid(int $pickorderid): self
    {
        $this->pickorderid = $pickorderid;

        return $this;
    }

    public function getVariantcodeid(): ?int
    {
        return $this->variantcodeid;
    }

    public function setVariantcodeid(int $variantcodeid): self
    {
        $this->variantcodeid = $variantcodeid;

        return $this;
    }

    public function getVariantcode(): ?string
    {
        return $this->variantcode;
    }

    public function setVariantcode(string $variantcode): self
    {
        $this->variantcode = $variantcode;

        return $this;
    }

    public function getOrderedquantity(): ?int
    {
        return $this->orderedquantity;
    }

    public function setOrderedquantity(int $orderedquantity): self
    {
        $this->orderedquantity = $orderedquantity;

        return $this;
    }

    public function getPickedquantity(): ?int
    {
        return $this->pickedquantity;
    }

    public function setPickedquantity(int $pickedquantity): self
    {
        $this->pickedquantity = $pickedquantity;

        return $this;
    }

    public function getPackedquantity(): ?int
    {
        return $this->packedquantity;
    }

    public function setPackedquantity(int $packedquantity): self
    {
        $this->packedquantity = $packedquantity;

        return $this;
    }

    public function getVariantsize(): ?string
    {
        return $this->variantsize;
    }

    public function setVariantsize(string $variantsize): self
    {
        $this->variantsize = $variantsize;

        return $this;
    }

    public function getVariantcolor(): ?string
    {
        return $this->variantcolor;
    }

    public function setVariantcolor(string $variantcolor): self
    {
        $this->variantcolor = $variantcolor;

        return $this;
    }

    public function getVariantdescription(): ?string
    {
        return $this->variantdescription;
    }

    public function setVariantdescription(string $variantdescription): self
    {
        $this->variantdescription = $variantdescription;

        return $this;
    }

    public function getDone(): ?int
    {
        return $this->done;
    }

    public function setDone(string $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getOrderQuantity(): Orderquantity
    {
        return $this->orderQuantity;
    }

    public function setOrderQuantity(?Orderquantity $orderQuantity = null): self
    {
        $this->orderQuantity = $orderQuantity;

        return $this;
    }
}
