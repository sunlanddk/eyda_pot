<?php

namespace App\Entity\Order;

use App\Entity\Company\Company;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Order\OrderRepository")
 * @ORM\Table(name="`order`")
 */
class Order
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $consolidatedid;

    /**
     * @ORM\Column(type="string")
     */
    private $description;

    /**
     * @ORM\Column(type="integer")
     */
    private $languageid;

    /**
     * @ORM\Column(type="integer")
     */
    private $carrierid;

    /**
     * @ORM\Column(type="integer")
     */
    private $deliverytermid;

    /**
     * @ORM\Column(type="integer")
     */
    private $paymenttermid;

    /**
     * @ORM\Column(type="integer")
     */
    private $currencyid;

    /**
     * @ORM\Column(type="integer")
     */
    private $salesuserid;

    /**
     * @ORM\Column(type="integer")
     */
    private $discount;

    /**
     * @ORM\Column(type="string")
     */
    private $reference;

    /**
     * @ORM\Column(type="decimal")
     */
    private $exchangerate;

    /**
     * @ORM\Column(type="string")
     */
    private $altcompanyname;

    /**
     * @ORM\Column(type="string")
     */
    private $address1;

    /**
     * @ORM\Column(type="string")
     */
    private $address2;

    /**
     * @ORM\Column(type="string")
     */
    private $zip;

    /**
     * @ORM\Column(type="string")
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $countryid;

    /**
     * @ORM\Column(type="integer")
     */
    private $ready;

    /**
     * @ORM\Column(type="integer")
     */
    private $readyuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $readydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $done;

    /**
     * @ORM\Column(type="integer")
     */
    private $doneuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $donedate;

    /**
     * @ORM\Column(type="integer")
     */
    private $orderdoneid;

    /**
     * @ORM\Column(type="integer")
     */
    private $draft;

    /**
     * @ORM\Column(type="integer")
     */
    private $tocompanyid;

    /**
     * @ORM\Column(type="string")
     */
    private $instructions;

    /**
     * @ORM\Column(type="string")
     */
    private $email;

    /**
     * @ORM\Column(type="string")
     */
    private $shippingshop;

    /**
     * @ORM\Column(type="integer")
     */
    private $ordertypeid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $receiveddate;

    /**
     * @ORM\Column(type="integer")
     */
    private $seasonid;

    /**
     * @ORM\Column(type="integer")
     */
    private $variantcodes;

    /**
     * @ORM\Column(type="integer")
     */
    private $webshopno;

    /**
     * @ORM\Column(type="integer")
     */
    private $proposal;

    /**
     * @ORM\Column(type="string")
     */
    private $phone;

    /**
     * @ORM\Column(type="string")
     */
    private $shopifyid;

    /**
     * @ORM\Column(type="decimal")
     */
    private $shopifytotal;

    /**
     * @ORM\Column(type="decimal")
     */
    private $shopifytaxtotal;

    /**
     * @ORM\Column(type="integer")
     */
    private $webshipperid;

    /**
     * @ORM\Column(type="string")
     */
    private $comment;

    /**
     * @ORM\OneToMany(mappedBy="order", targetEntity=Orderline::class, fetch="EAGER")
     */
    private Collection $orderLines;

    /**
     * @ORM\ManyToOne(targetEntity=Company::class, inversedBy="orders")
     * @ORM\JoinColumn(name="companyId", referencedColumnName="id")
     */
    private Company $company;

    public function __construct()
    {
        $this->comment = '';
        $this->orderLines = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getConsolidatedid(): ?int
    {
        return $this->consolidatedid;
    }

    public function setConsolidatedid(int $consolidatedid): self
    {
        $this->consolidatedid = $consolidatedid;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLanguageid(): ?int
    {
        return $this->languageid;
    }

    public function setLanguageid(int $languageid): self
    {
        $this->languageid = $languageid;

        return $this;
    }

    public function getCarrierid(): ?int
    {
        return $this->carrierid;
    }

    public function setCarrierid(int $carrierid): self
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    public function getDeliverytermid(): ?int
    {
        return $this->deliverytermid;
    }

    public function setDeliverytermid(int $deliverytermid): self
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    public function getPaymenttermid(): ?int
    {
        return $this->paymenttermid;
    }

    public function setPaymenttermid(int $paymenttermid): self
    {
        $this->paymenttermid = $paymenttermid;

        return $this;
    }

    public function getCurrencyid(): ?int
    {
        return $this->currencyid;
    }

    public function setCurrencyid(int $currencyid): self
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    public function getSalesuserid(): ?int
    {
        return $this->salesuserid;
    }

    public function setSalesuserid(int $salesuserid): self
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    public function getDiscount(): ?int
    {
        return $this->discount;
    }

    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    public function getReference(): ?string
    {
        return $this->reference;
    }

    public function setReference(string $reference): self
    {
        $this->reference = $reference;

        return $this;
    }

    public function getExchangerate(): ?string
    {
        return $this->exchangerate;
    }

    public function setExchangerate(string $exchangerate): self
    {
        $this->exchangerate = $exchangerate;

        return $this;
    }

    public function getAltcompanyname(): ?string
    {
        return $this->altcompanyname;
    }

    public function setAltcompanyname(string $altcompanyname): self
    {
        $this->altcompanyname = $altcompanyname;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountryid(): ?int
    {
        return $this->countryid;
    }

    public function setCountryid(int $countryid): self
    {
        $this->countryid = $countryid;

        return $this;
    }

    public function getReady(): ?int
    {
        return $this->ready;
    }

    public function setReady(int $ready): self
    {
        $this->ready = $ready;

        return $this;
    }

    public function getReadyuserid(): ?int
    {
        return $this->readyuserid;
    }

    public function setReadyuserid(int $readyuserid): self
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    public function getReadydate(): ?\DateTimeInterface
    {
        return $this->readydate;
    }

    public function setReadydate(\DateTimeInterface $readydate): self
    {
        $this->readydate = $readydate;

        return $this;
    }


    public function getDone(): ?int
    {
        return $this->done;
    }

    public function setDone(int $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getDoneuserid(): ?int
    {
        return $this->doneuserid;
    }

    public function setDoneuserid(int $doneuserid): self
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    public function getDonedate(): ?\DateTimeInterface
    {
        return $this->donedate;
    }

    public function setDonedate(\DateTimeInterface $donedate): self
    {
        $this->donedate = $donedate;

        return $this;
    }

    public function getOrderdoneid(): ?int
    {
        return $this->orderdoneid;
    }

    public function setOrderdoneid(int $orderdoneid): self
    {
        $this->orderdoneid = $orderdoneid;

        return $this;
    }

    public function getDraft(): ?int
    {
        return $this->draft;
    }

    public function setDraft(int $draft): self
    {
        $this->draft = $draft;

        return $this;
    }

    public function getTocompanyid(): ?int
    {
        return $this->tocompanyid;
    }

    public function setTocompanyid(int $tocompanyid): self
    {
        $this->tocompanyid = $tocompanyid;

        return $this;
    }

    public function getInstructions(): ?string
    {
        return $this->instructions;
    }

    public function setInstructions(string $instructions): self
    {
        $this->instructions = $instructions;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getShippingshop(): ?string
    {
        return $this->shippingshop;
    }

    public function setShippingshop(string $shippingshop): self
    {
        $this->shippingshop = $shippingshop;

        return $this;
    }

    public function getOrdertypeid(): ?int
    {
        return $this->ordertypeid;
    }

    public function setOrdertypeid(int $ordertypeid): self
    {
        $this->ordertypeid = $ordertypeid;

        return $this;
    }

    public function getReceiveddate(): ?\DateTimeInterface
    {
        return $this->receiveddate;
    }

    public function setReceiveddate(\DateTimeInterface $receiveddate): self
    {
        $this->receiveddate = $receiveddate;

        return $this;
    }

    public function getSeasonid(): ?int
    {
        return $this->seasonid;
    }

    public function setSeasonid(int $seasonid): self
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    public function getVariantcodes(): ?int
    {
        return $this->variantcodes;
    }

    public function setVariantcodes(int $variantcodes): self
    {
        $this->variantcodes = $variantcodes;

        return $this;
    }

    public function getWebshopno(): ?int
    {
        return $this->webshopno;
    }

    public function setWebshopno(int $webshopno): self
    {
        $this->webshopno = $webshopno;

        return $this;
    }

    public function getProposal(): ?int
    {
        return $this->proposal;
    }

    public function setProposal(int $proposal): self
    {
        $this->proposal = $proposal;

        return $this;
    }

    public function getPhone(): ?string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): self
    {
        $this->phone = $phone;

        return $this;
    }

    public function getShopifyid(): ?string
    {
        return $this->shopifyid;
    }

    public function setShopifyid(string $shopifyid): self
    {
        $this->shopifyid = $shopifyid;

        return $this;
    }

    public function getShopifytotal(): ?string
    {
        return $this->shopifytotal;
    }

    public function setShopifytotal(string $shopifytotal): self
    {
        $this->shopifytotal = $shopifytotal;

        return $this;
    }

    public function getShopifytaxtotal(): ?string
    {
        return $this->shopifytaxtotal;
    }

    public function setShopifytaxtotal(string $shopifytaxtotal): self
    {
        $this->shopifytaxtotal = $shopifytaxtotal;

        return $this;
    }

    public function getWebshipperid(): ?int
    {
        return $this->webshipperid;
    }

    public function setWebshipperid(int $webshipperid): self
    {
        $this->webshipperid = $webshipperid;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getOrderLines(): Collection
    {
        return $this->orderLines;
    }

    public function getCompany(): Company
    {
        return $this->company;
    }

    public function setCompany(Company $company): self
    {
        $this->company = $company;

        return $this;
    }
}
