<?php

namespace App\Entity\Shopify;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Shopify\ShopifyvariantRepo")
 */
class Shopifyvariant
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $Id;

    /**
     * @ORM\Column(type="integer")
     */
    private $Shopifyshopid;

    /**
     * @ORM\Column(type="integer")
     */
    private $Variantcodeid;

    /**
     * @ORM\Column(type="integer")
     */
    private $Articleid;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $Shopifyvariantid;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $Shopifyproductid;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $Inventoryitemid;

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function getShopifyshopid(): ?int
    {
        return $this->Shopifyshopid;
    }

    public function setShopifyshopid(int $Shopifyshopid): self
    {
        $this->Shopifyshopid = $Shopifyshopid;

        return $this;
    }

    public function getVariantcodeid(): ?int
    {
        return $this->Variantcodeid;
    }

    public function setVariantcodeid(int $Variantcodeid): self
    {
        $this->Variantcodeid = $Variantcodeid;

        return $this;
    }

    public function getArticleid(): ?int
    {
        return $this->Articleid;
    }

    public function setArticleid(int $Articleid): self
    {
        $this->Articleid = $Articleid;

        return $this;
    }

    public function getShopifyvariantid(): ?string
    {
        return $this->Shopifyvariantid;
    }

    public function setShopifyvariantid(string $Shopifyvariantid): self
    {
        $this->Shopifyvariantid = $Shopifyvariantid;

        return $this;
    }

    public function getShopifyproductid(): ?string
    {
        return $this->Shopifyproductid;
    }

    public function setShopifyproductid(string $Shopifyproductid): self
    {
        $this->Shopifyproductid = $Shopifyproductid;

        return $this;
    }

    public function getInventoryitemid(): ?string
    {
        return $this->Inventoryitemid;
    }

    public function setInventoryitemid(string $Inventoryitemid): self
    {
        $this->Inventoryitemid = $Inventoryitemid;

        return $this;
    }


}
