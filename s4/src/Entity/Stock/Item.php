<?php

namespace App\Entity\Stock;

use App\Entity\Order\Orderline;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Stock\ItemRepository")
 */
class Item
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $articleid;

    /**
     * @ORM\Column(type="integer")
     */
    private $articlecolorid;

    /**
     * @ORM\Column(type="integer")
     */
    private $articlesizeid;

    /**
     * @ORM\Column(type="integer")
     */
    private $previouscontainerid;

    /**
     * @ORM\Column(type="integer")
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $comment;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $price;

    /**
     * @ORM\Column(type="integer")
     */
    private $sortation;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $batchnumber;

    /**
     * @ORM\Column(type="integer")
     */
    private $requisitionid;

    /**
     * @ORM\Column(type="integer")
     */
    private $requisitionlineid;

    /**
     * @ORM\Column(type="integer")
     */
    private $ownerid;

    /**
     * @ORM\Column(type="integer")
     */
    private $transactionitemid;

    /**
     * @ORM\ManyToOne(targetEntity=Orderline::class, inversedBy="items")
     * @ORM\JoinColumn(name="orderLineId", referencedColumnName="id", nullable=true, columnDefinition="INT DEFAULT 0")
     */
    private Orderline $orderLine;

    /**
     * @ORM\ManyToOne(targetEntity=Container::class, inversedBy="items")
     * @ORM\JoinColumn(name="containerId", referencedColumnName="id", nullable=true, columnDefinition="INT DEFAULT 0")
     */
    private Container $container;

    /**
     * @ORM\OneToMany(mappedBy="parent", targetEntity=Item::class)
     */
    private Collection $childrens;

    /**
     * @ORM\ManyToOne(targetEntity=Item::class, inversedBy="childrens")
     * @ORM\JoinColumn(name="parentid", referencedColumnName="id", nullable=true, columnDefinition="INT DEFAULT 0")
     */
    private self $parent;

    public function __construct()
    {
        $this->childrens = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedate(): ?DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getArticleid(): ?int
    {
        return $this->articleid;
    }

    public function setArticleid(int $articleid): self
    {
        $this->articleid = $articleid;

        return $this;
    }

    public function getArticlecolorid(): ?int
    {
        return $this->articlecolorid;
    }

    public function setArticlecolorid(int $articlecolorid): self
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    public function getArticlesizeid(): ?int
    {
        return $this->articlesizeid;
    }

    public function setArticlesizeid(int $articlesizeid): self
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    public function getPreviouscontainerid(): ?int
    {
        return $this->previouscontainerid;
    }

    public function setPreviouscontainerid(int $previouscontainerid): self
    {
        $this->previouscontainerid = $previouscontainerid;

        return $this;
    }

    public function getComment(): ?string
    {
        return $this->comment;
    }

    public function setComment(string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getSortation(): ?int
    {
        return $this->sortation;
    }

    public function setSortation(int $sortation): self
    {
        $this->sortation = $sortation;

        return $this;
    }

    public function getBatchnumber(): ?string
    {
        return $this->batchnumber;
    }

    public function setBatchnumber(string $batchnumber): self
    {
        $this->batchnumber = $batchnumber;

        return $this;
    }

    public function getPrice(): ?string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getRequisitionid(): ?int
    {
        return $this->requisitionid;
    }

    public function setRequisitionid(int $requisitionid): self
    {
        $this->requisitionid = $requisitionid;

        return $this;
    }

    public function getRequisitionlineid(): ?int
    {
        return $this->requisitionlineid;
    }

    public function setRequisitionlineid(int $requisitionlineid): self
    {
        $this->requisitionlineid = $requisitionlineid;

        return $this;
    }

    public function getOwnerid(): ?int
    {
        return $this->ownerid;
    }

    public function setOwnerid(int $ownerid): self
    {
        $this->ownerid = $ownerid;

        return $this;
    }

    public function getTransactionitemid(): ?int
    {
        return $this->transactionitemid;
    }

    public function setTransactionitemid(int $transactionitemid): self
    {
        $this->transactionitemid = $transactionitemid;

        return $this;
    }

    public function getOrderLine(): ?Orderline
    {
        return $this->orderLine ?? null;
    }

    public function setOrderLine(?Orderline $orderLine = null): self
    {
        $this->orderLine = $orderLine;

        return $this;
    }

    public function getContainer(): ?Container
    {
        return $this->container ?? null;
    }

    public function setContainer(?Container $container = null): self
    {
        $this->container = $container;

        return $this;
    }

    public function getParent(): Item
    {
        return $this->parent;
    }

    public function setParent(Item $parent): self
    {
        $this->parent = $parent;

        return $this;
    }

    public function getChildrens(): Collection
    {
        return $this->childrens;
    }

    public function setChildrens(Collection $childrens): self
    {
        $this->childrens = $childrens;

        return $this;
    }
}
