<?php

namespace App\Entity\Stock;

use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Stock\ContainerRepository")
 */
class Container
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active;

    /**
     * @ORM\Column(type="integer")
     */
    private $containertypeid;

    /**
     * @ORM\Column(type="integer")
     */
    private $previoustransportid;

    /**
     * @ORM\Column(type="integer")
     */
    private $previousstockid;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $position;

    /**
     * @ORM\Column(type="string", length=100)
     */
    private $description;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=3)
     */
    private $taraweight;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=3)
     */
    private $grossweight;

    /**
     * @ORM\Column(type="decimal", precision=12, scale=3)
     */
    private $volume;

    /**
     * @ORM\OneToMany(mappedBy="container", targetEntity=Item::class)
     */
    private Collection $items;

    /**
     * @ORM\ManyToOne(targetEntity=Stock::class, inversedBy="containers")
     * @ORM\JoinColumn(name="stockId", referencedColumnName="id")
     */
    private Stock $stock;

    public function __construct()
    {
        $this->items = new ArrayCollection();
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreatedate(): ?DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getContainertypeid(): ?int
    {
        return $this->containertypeid;
    }

    public function setContainertypeid(int $containertypeid): self
    {
        $this->containertypeid = $containertypeid;

        return $this;
    }

    public function getPosition(): ?string
    {
        return $this->position;
    }

    public function setPosition(string $position): self
    {
        $this->position = $position;

        return $this;
    }

    public function getPreviousTransportId(): int
    {
        return $this->previoustransportid;
    }

    public function setPreviousTransportId(int $value): self
    {
        $this->previoustransportid = $value;

        return $this;
    }

    public function getPreviousStockId(): int
    {
        return $this->previousstockid;
    }

    public function setPreviousStockId(int $value): self
    {
        $this->previousstockid = $value;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $value): self
    {
        $this->description = $value;

        return $this;
    }

    public function getTaraWeight(): float
    {
        return $this->taraweight;
    }

    public function setTaraWeight(float $value): self
    {
        $this->taraweight = $value;

        return $this;
    }

    public function getGrossWeight(): float
    {
        return $this->grossweight;
    }

    public function setGrossWeight(float $value): self
    {
        $this->grossweight = $value;

        return $this;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }

    public function setVolume(float $value): self
    {
        $this->volume = $value;

        return $this;
    }

    /**
     * @return Collection|Item[]
     */
    public function getItems(): Collection
    {
        return $this->items;
    }

    public function getStock(): Stock
    {
        return $this->stock;
    }

    public function setStock(?Stock $value = null): self
    {
        $this->stock = $value;

        return $this;
    }
}
