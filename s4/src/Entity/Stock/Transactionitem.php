<?php

namespace App\Entity\Stock;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Stock\TransactionitemRepository")
 */
class Transactionitem
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active = 1;

    /**
     * @ORM\Column(type="integer")
     */
    private $objectlineid;

    /**
     * @ORM\Column(type="integer")
     */
    private $objectquantityid;

    /**
     * @ORM\Column(type="integer")
     */
    private $transactionid;

    /**
     * @ORM\Column(type="integer")
     */
    private $fromstockid;

    /**
     * @ORM\Column(type="integer")
     */
    private $tostockid;

    /**
     * @ORM\Column(type="integer")
     */
    private $auto;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $quantity;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $articlesizeid;

    /**
     * @ORM\Column(type="integer")
     */
    private $variantcodeid;

    /**
     * @ORM\Column(type="string", length=10)
     */
    private $value;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getAuto(): ?int
    {
        return $this->auto;
    }

    public function setAuto(int $auto): self
    {
        $this->auto = $auto;

        return $this;
    }

    public function getVariantcodeid(): ?int
    {
        return $this->variantcodeid;
    }

    public function setVariantcodeid(int $variantcodeid): self
    {
        $this->variantcodeid = $variantcodeid;

        return $this;
    }

    public function getTransactionid(): ?int
    {
        return $this->transactionid;
    }

    public function setTransactionid(int $transactionid): self
    {
        $this->transactionid = $transactionid;

        return $this;
    }

    public function getQuantity(): ?string
    {
        return $this->quantity;
    }

    public function setQuantity(string $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getValue(): ?string
    {
        return $this->value;
    }

    public function setValue(string $value): self
    {
        $this->value = $value;

        return $this;
    }

    public function getObjectlineid(): ?int
    {
        return $this->objectlineid;
    }

    public function setObjectlineid(int $objectlineid): self
    {
        $this->objectlineid = $objectlineid;

        return $this;
    }

    public function getObjectquantityid(): ?int
    {
        return $this->objectquantityid;
    }

    public function setObjectquantityid(int $objectquantityid): self
    {
        $this->objectquantityid = $objectquantityid;

        return $this;
    }

    public function getArticlesizeid(): ?int
    {
        return $this->articlesizeid;
    }

    public function setArticlesizeid(int $articlesizeid): self
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    public function getFromstockid(): ?int
    {
        return $this->fromstockid;
    }

    public function setFromstockid(int $fromstockid): self
    {
        $this->fromstockid = $fromstockid;

        return $this;
    }

    public function getTostockid(): ?int
    {
        return $this->tostockid;
    }

    public function setTostockid(int $tostockid): self
    {
        $this->tostockid = $tostockid;

        return $this;
    }

}
