<?php

namespace App\Entity\Stock;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Stock\ShipmentInvoiceRepository")
 */
class Stock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="datetime")
     */
    private $createdate;

    /**
     * @ORM\Column(type="integer")
     */
    private $createuserid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $modifydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $modifyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $active = 1;

    /**
     * @ORM\Column(type="string", length=30)
     */
    private $name;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $altcompanyname;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $address1;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $address2;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $zip;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $city;

    /**
     * @ORM\Column(type="integer")
     */
    private $countryid;

    /**
     * @ORM\Column(type="integer")
     */
    private $companyid;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $type;

    /**
     * @ORM\Column(type="integer")
     */
    private $deliverytermid;

    /**
     * @ORM\Column(type="integer")
     */
    private $carrierid;

    /**
     * @ORM\Column(type="datetime")
     */
    private $departuredate;

    /**
     * @ORM\Column(type="smallint")
     */
    private $ready;

    /**
     * @ORM\Column(type="datetime")
     */
    private $readydate;

    /**
     * @ORM\Column(type="integer")
     */
    private $readyuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $departed;

    /**
     * @ORM\Column(type="datetime")
     */
    private $departeddate;

    /**
     * @ORM\Column(type="integer")
     */
    private $departeduserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $done;

    /**
     * @ORM\Column(type="datetime")
     */
    private $donedate;

    /**
     * @ORM\Column(type="integer")
     */
    private $doneuserid;

    /**
     * @ORM\Column(type="smallint")
     */
    private $invoiced;

    /**
     * @ORM\Column(type="datetime")
     */
    private $invoiceddate;

    /**
     * @ORM\Column(type="integer")
     */
    private $invoiceduserid;

    /**
     * @ORM\Column(type="integer")
     */
    private $fromcompanyid;

    /**
     * @ORM\Column(type="string", length=50)
     */
    private $inventorypct;

    /**
     * @ORM\OneToMany(mappedBy="stock", targetEntity=Container::class)
     */
    private Collection $containers;

    public function __construct()
    {
        $this->containers = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getCreatedate(): ?\DateTimeInterface
    {
        return $this->createdate;
    }

    public function setCreatedate(\DateTimeInterface $createdate): self
    {
        $this->createdate = $createdate;

        return $this;
    }

    public function getCreateuserid(): ?int
    {
        return $this->createuserid;
    }

    public function setCreateuserid(int $createuserid): self
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    public function getModifydate(): ?\DateTimeInterface
    {
        return $this->modifydate;
    }

    public function setModifydate(\DateTimeInterface $modifydate): self
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    public function getModifyuserid(): ?int
    {
        return $this->modifyuserid;
    }

    public function setModifyuserid(int $modifyuserid): self
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    public function getActive(): ?int
    {
        return $this->active;
    }

    public function setActive(int $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getAltcompanyname(): ?string
    {
        return $this->altcompanyname;
    }

    public function setAltcompanyname(string $altcompanyname): self
    {
        $this->altcompanyname = $altcompanyname;

        return $this;
    }

    public function getAddress1(): ?string
    {
        return $this->address1;
    }

    public function setAddress1(string $address1): self
    {
        $this->address1 = $address1;

        return $this;
    }

    public function getAddress2(): ?string
    {
        return $this->address2;
    }

    public function setAddress2(string $address2): self
    {
        $this->address2 = $address2;

        return $this;
    }

    public function getZip(): ?string
    {
        return $this->zip;
    }

    public function setZip(string $zip): self
    {
        $this->zip = $zip;

        return $this;
    }

    public function getCity(): ?string
    {
        return $this->city;
    }

    public function setCity(string $city): self
    {
        $this->city = $city;

        return $this;
    }

    public function getCountryid(): ?int
    {
        return $this->countryid;
    }

    public function setCountryid(int $countryid): self
    {
        $this->countryid = $countryid;

        return $this;
    }

    public function getCompanyid(): ?int
    {
        return $this->companyid;
    }

    public function setCompanyid(int $companyid): self
    {
        $this->companyid = $companyid;

        return $this;
    }

    public function getDeliverytermid(): ?int
    {
        return $this->deliverytermid;
    }

    public function setDeliverytermid(int $deliverytermid): self
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    public function getCarrierid(): ?int
    {
        return $this->carrierid;
    }

    public function setCarrierid(int $carrierid): self
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    public function getDeparturedate(): ?\DateTimeInterface
    {
        return $this->departuredate;
    }

    public function setDeparturedate(\DateTimeInterface $departuredate): self
    {
        $this->departuredate = $departuredate;

        return $this;
    }

    public function getReady(): ?int
    {
        return $this->ready;
    }

    public function setReady(int $ready): self
    {
        $this->ready = $ready;

        return $this;
    }

    public function getReadydate(): ?\DateTimeInterface
    {
        return $this->readydate;
    }

    public function setReadydate(\DateTimeInterface $readydate): self
    {
        $this->readydate = $readydate;

        return $this;
    }

    public function getReadyuserid(): ?int
    {
        return $this->readyuserid;
    }

    public function setReadyuserid(int $readyuserid): self
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    public function getDeparted(): ?int
    {
        return $this->departed;
    }

    public function setDeparted(int $departed): self
    {
        $this->departed = $departed;

        return $this;
    }

    public function getDeparteddate(): ?\DateTimeInterface
    {
        return $this->departeddate;
    }

    public function setDeparteddate(\DateTimeInterface $departeddate): self
    {
        $this->departeddate = $departeddate;

        return $this;
    }

    public function getDeparteduserid(): ?int
    {
        return $this->departeduserid;
    }

    public function setDeparteduserid(int $departeduserid): self
    {
        $this->departeduserid = $departeduserid;

        return $this;
    }

    public function getDone(): ?int
    {
        return $this->done;
    }

    public function setDone(int $done): self
    {
        $this->done = $done;

        return $this;
    }

    public function getDonedate(): ?\DateTimeInterface
    {
        return $this->donedate;
    }

    public function setDonedate(\DateTimeInterface $donedate): self
    {
        $this->donedate = $donedate;

        return $this;
    }

    public function getDoneuserid(): ?int
    {
        return $this->doneuserid;
    }

    public function setDoneuserid(int $doneuserid): self
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    public function getInvoiced(): ?int
    {
        return $this->invoiced;
    }

    public function setInvoiced(int $invoiced): self
    {
        $this->invoiced = $invoiced;

        return $this;
    }

    public function getInvoiceddate(): ?\DateTimeInterface
    {
        return $this->invoiceddate;
    }

    public function setInvoiceddate(\DateTimeInterface $invoiceddate): self
    {
        $this->invoiceddate = $invoiceddate;

        return $this;
    }

    public function getInvoiceduserid(): ?int
    {
        return $this->invoiceduserid;
    }

    public function setInvoiceduserid(int $invoiceduserid): self
    {
        $this->invoiceduserid = $invoiceduserid;

        return $this;
    }

    public function getFromcompanyid(): ?int
    {
        return $this->fromcompanyid;
    }

    public function setFromcompanyid(int $fromcompanyid): self
    {
        $this->fromcompanyid = $fromcompanyid;

        return $this;
    }

    public function getInventorypct(): ?string
    {
        return $this->inventorypct;
    }

    public function setInventorypct(string $inventorypct): self
    {
        $this->inventorypct = $inventorypct;

        return $this;
    }

    /**
     * @return Collection|Container[]
     */
    public function getContainers(): Collection
    {
        return $this->containers;
    }
}
