<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity()
 * @ORM\Table(name="language")
 */
class Language
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="createDate", type="datetime")
     */
    private DateTimeInterface $createDate;

    /**
     * @ORM\Column(name="createUserId", type="integer")
     */
    private int $createUserId;

    /**
     * @ORM\Column(name="modifyDate", type="datetime")
     */
    private DateTimeInterface $modifyDate;

    /**
     * @ORM\Column(name="modifyUserId", type="integer")
     */
    private int $modifyUserId;

    /**
     * @ORM\Column(name="isoCode", type="string", length=2)
     */
    private string $isoCode;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreateDate(): ?DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getCreateUserId(): ?int
    {
        return $this->createUserId;
    }

    public function setCreateUserId(int $createUserId): self
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    public function getModifyDate(): ?DateTimeInterface
    {
        return $this->modifyDate;
    }

    public function setModifyDate(DateTimeInterface $modifyDate): self
    {
        $this->modifyDate = $modifyDate;

        return $this;
    }

    public function getModifyUserId(): ?int
    {
        return $this->modifyUserId;
    }

    public function setModifyUserId(int $modifyUserId): self
    {
        $this->modifyUserId = $modifyUserId;

        return $this;
    }

    public function getIsoCode(): ?string
    {
        return $this->isoCode;
    }

    public function setIsoCode(string $isoCode): self
    {
        $this->isoCode = $isoCode;

        return $this;
    }
}
