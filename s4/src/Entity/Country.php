<?php

namespace App\Entity;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CountryRepository")
 * @ORM\Table(name="country")
 */
class Country
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(name="createDate", type="datetime")
     */
    private DateTimeInterface $createDate;

    /**
     * @ORM\Column(name="createUserId", type="integer")
     */
    private int $createUserId;

    /**
     * @ORM\Column(name="modifyDate", type="datetime")
     */
    private DateTimeInterface $modifyDate;

    /**
     * @ORM\Column(name="modifyUserId", type="integer")
     */
    private int $modifyUserId;

    /**
     * @ORM\Column(name="active", type="smallint")
     */
    private bool $active;

    /**
     * @ORM\Column(name="name", type="string", length=5)
     */
    private string $name;

    /**
     * @ORM\Column(name="description", type="string", length=100)
     */
    private string $description;

    /**
     * @ORM\Column(name="isoCode", type="string", length=45)
     */
    private string $isoCode;

    /**
     * @ORM\Column(name="countryNum", type="integer", nullable=true)
     */
    private ?int $countryNum = null;

    public function getId(): int
    {
        return $this->id;
    }

    public function getCreateDate(): ?DateTimeInterface
    {
        return $this->createDate;
    }

    public function setCreateDate(DateTimeInterface $createDate): self
    {
        $this->createDate = $createDate;

        return $this;
    }

    public function getCreateUserId(): ?int
    {
        return $this->createUserId;
    }

    public function setCreateUserId(int $createUserId): self
    {
        $this->createUserId = $createUserId;

        return $this;
    }

    public function getModifyDate(): ?DateTimeInterface
    {
        return $this->modifyDate;
    }

    public function setModifyDate(DateTimeInterface $modifyDate): self
    {
        $this->modifyDate = $modifyDate;

        return $this;
    }

    public function getModifyUserId(): ?int
    {
        return $this->modifyUserId;
    }

    public function setModifyUserId(int $modifyUserId): self
    {
        $this->modifyUserId = $modifyUserId;

        return $this;
    }

    public function getActive(): bool
    {
        return $this->active;
    }

    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDescription(): string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getIsoCode(): string
    {
        return $this->isoCode;
    }

    public function setIsoCode(string $isoCode): self
    {
        $this->isoCode = $isoCode;

        return $this;
    }

    public function getCountryNum(): ?int
    {
        return $this->countryNum;
    }

    public function setCountryNum(?int $countryNum): self
    {
        $this->countryNum = $countryNum;

        return $this;
    }
}
