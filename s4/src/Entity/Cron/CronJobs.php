<?php

namespace App\Entity\Cron;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Cron\CronJobRepo")
 */
class CronJobs
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $Id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $CronName;

    /**
     * @ORM\Column(type="string", length=45)
     */
    private $CronFunction;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $CronValue;

    /**
     * @ORM\Column(type="datetime")
     */
    private $CronScheduledAt;

    /**
     * @ORM\Column(type="string", length=25)
     */
    private $Type;

    /**
     * @ORM\Column(type="smallint")
     */
    private $Internalid = 0;

    /**
     * @ORM\Column(type="datetime")
     */
    private $CronRanAt = null;

    /**
     * @ORM\Column(type="string")
     */
    private $Error = null;

    /**
     * @ORM\Column(type="smallint")
     */
    private $Done = 0;

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function getCronName(): ?string
    {
        return $this->CronName;
    }

    public function setCronName(string $CronName): self
    {
        $this->CronName = $CronName;

        return $this;
    }

    public function getCronFunction(): ?string
    {
        return $this->CronFunction;
    }

    public function setCronFunction(string $CronFunction): self
    {
        $this->CronFunction = $CronFunction;

        return $this;
    }

    public function getCronValue(): ?string
    {
        return $this->CronValue;
    }

    public function setCronValue(string $CronValue): self
    {
        $this->CronValue = $CronValue;

        return $this;
    }

    public function getCronScheduledAt(): ?DateTimeInterface
    {
        return $this->CronScheduledAt;
    }

    public function setCronScheduledAt(DateTimeInterface $CronScheduledAt): self
    {
        $this->CronScheduledAt = $CronScheduledAt;

        return $this;
    }

    public function getCronRanAt(): ?DateTimeInterface
    {
        return $this->CronRanAt;
    }

    public function setCronRanAt(DateTimeInterface $CronRanAt): self
    {
        $this->CronRanAt = $CronRanAt;

        return $this;
    }

    public function getInternalid(): ?int
    {
        return $this->Internalid;
    }

    public function setInternalid(int $Internalid): self
    {
        $this->Internalid = $Internalid;

        return $this;
    }

    public function getDone(): ?int
    {
        return $this->Done;
    }

    public function setDone(int $Done): self
    {
        $this->Done = $Done;

        return $this;
    }

    public function getType(): ?string
    {
        return $this->Type;
    }

    public function setType(string $Type): self
    {
        $this->Type = $Type;

        return $this;
    }

    public function getError(): ?string
    {
        return $this->Error;
    }

    public function setError(string $Error): self
    {
        $this->Error = $Error;

        return $this;
    }

}
