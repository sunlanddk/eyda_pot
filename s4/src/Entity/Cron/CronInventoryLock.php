<?php

namespace App\Entity\Cron;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Cron\CronInventoryLockRepo")
 */
class CronInventoryLock
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $Id;

    /**
     * @ORM\Column(type="smallint")
     */
    private $Running;

    /**
     * @ORM\Column(type="datetime")
     */
    private $StartDate;

    /**
     * @ORM\Column(type="datetime")
     */
    private $EndDate = null;

    public function getId(): ?int
    {
        return $this->Id;
    }

    public function getStartDate(): ?DateTimeInterface
    {
        return $this->StartDate;
    }

    public function setStartDate(DateTimeInterface $StartDate): self
    {
        $this->StartDate = $StartDate;

        return $this;
    }

    public function getEndDate(): ?DateTimeInterface
    {
        return $this->EndDate;
    }

    public function setEndDate(DateTimeInterface $EndDate): self
    {
        $this->EndDate = $EndDate;

        return $this;
    }

    public function getRunning(): ?int
    {
        return $this->Running;
    }

    public function setRunning(int $Running): self
    {
        $this->Running = $Running;

        return $this;
    }

}
