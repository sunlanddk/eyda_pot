<?php

namespace App\Entity\Company;

use App\Entity\Order\Order;
use DateTime;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\Company\CompanyRepository")
 * @ORM\Table(name="company")
 */
class Company
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(name="id", type="integer")
     */
    private int $id;

    /**
     * @ORM\OneToMany(mappedBy="company", targetEntity=Order::class)
     */
    private Collection $orders;

    /**
     * @ORM\Column(name="createDate", type="datetime")
     */
    private DateTimeInterface $createDate;

    /**
     * @ORM\Column(name="createUserId", type="integer")
     */
    private int $createUserId;

    /**
     * @ORM\Column(name="modifyDate", type="datetime")
     */
    private DateTimeInterface $modifyDate;

    /**
     * @ORM\Column(name="modifyUserId", type="integer")
     */
    private int $modifyUserId;

    /**
     * @ORM\Column(name="active", type="smallint")
     */
    private bool $active;

    /**
     * @ORM\Column(name="name", type="string", length=61)
     */
    private string $name;

    /**
     * @ORM\Column(name="phoneMain", type="string", length=20)
     */
    private string $phoneMain;

    public function __construct()
    {
        $this->orders = new ArrayCollection();
        $this->createDate = new DateTime();
        $this->createUserId = 1;
        $this->modifyDate = new DateTime();
        $this->modifyUserId = 1;
        $this->active = 1;
    }

    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Collection<int, Order>
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getPhoneMain(): string
    {
        return $this->phoneMain;
    }
}
