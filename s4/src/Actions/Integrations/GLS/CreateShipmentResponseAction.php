<?php

namespace App\Actions\Integrations\GLS;

use App\Dto\Api\GLS\CreateShipmentResponse\CreateShipmentDto;
use App\Dto\Api\GLS\CreateShipmentResponse\ParcelDto;

class CreateShipmentResponseAction
{
    private array $response;

    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return array
     */
    public function getResponse(): array
    {
        return $this->response;
    }

    /**
     * @return CreateShipmentDto
     */
    public function execute(): CreateShipmentDto
    {
        $dto = new CreateShipmentDto();
        $dto->setResponse($this->getResponse());

        if (!empty($dto->getParcels()) && count($dto->getParcels()) > 0) {
            $parcels = [];

            foreach ($dto->getParcels() as $parcel) {
                $parcelDto = new ParcelDto();
                $parcelDto->setUniqueNumber($parcel['UniqueNumber']);
                $parcelDto->setParcelNumber($parcel['ParcelNumber']);
                $parcelDto->setParcelNumber12Digit($parcel['ParcelNumber12Digit']);
                $parcelDto->setNdiNumber($parcel['NdiNumber']);
                $parcelDto->setRouting($parcel['Routing']);
                $parcelDto->setNdiNumber2($parcel['NdiNumber2']);
                $parcelDto->setNdiNumber3($parcel['NdiNumber3']);
                $parcelDto->setNdiNumber4($parcel['NdiNumber4']);

                $parcels[] = $parcelDto;
            }

            $dto->setParcels($parcels);
        }

        return $dto;
    }
}
