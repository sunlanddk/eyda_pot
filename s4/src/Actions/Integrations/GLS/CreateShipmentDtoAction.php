<?php

namespace App\Actions\Integrations\GLS;

use App\Dto\Api\GLS\CreateShipmentRequest\AddressesDeliveryDto;
use App\Dto\Api\GLS\CreateShipmentRequest\AddressesDto;
use App\Dto\Api\GLS\CreateShipmentRequest\AddressesPickupDto;
use App\Dto\Api\GLS\CreateShipmentRequest\CreateShipmentDto;
use App\Dto\Api\GLS\CreateShipmentRequest\ParcelsDto;
use App\Dto\Api\GLS\CreateShipmentRequest\ServicesDto;
use App\Entity\Article\Article;
use App\Entity\Article\ArticleSize;
use App\Entity\Article\Variantcode;
use App\Entity\Country;
use App\Entity\Order\Orderadresses;
use App\Entity\Order\Orderline;
use App\Entity\Order\Orderquantity;
use App\Entity\Order\ReturnEntity;
use App\Entity\Order\ReturnLine;
use App\Entity\Stock\Container;
use App\Entity\Stock\Item;
use App\Repository\Article\VariantcodeRepository;
use App\Repository\CountryRepository;
use App\Repository\Order\OrderaddressesRepository;
use RuntimeException;

class CreateShipmentDtoAction
{
    private CountryRepository $countryRepository;
    private OrderaddressesRepository $orderAddressesRepository;
    private VariantcodeRepository $variantcodeRepository;
    private string $labelType;

    public function __construct(
        CountryRepository $countryRepository,
        OrderaddressesRepository $orderAddressesRepository,
        VariantcodeRepository $variantcodeRepository
    ) {
        $this->countryRepository = $countryRepository;
        $this->orderAddressesRepository = $orderAddressesRepository;
        $this->variantcodeRepository = $variantcodeRepository;
    }

    /**
     * @return CountryRepository
     */
    public function getCountryRepository(): CountryRepository
    {
        return $this->countryRepository;
    }

    /**
     * @return OrderaddressesRepository
     */
    public function getOrderAddressesRepository(): OrderaddressesRepository
    {
        return $this->orderAddressesRepository;
    }

    /**
     * @return string
     */
    public function getLabelType(): string
    {
        return $this->labelType;
    }

    /**
     * @param string $labelType
     * @return void
     */
    public function setLabelType(string $labelType): void
    {
        if (!in_array($labelType, [
            ReturnEntity::LABEL_TYPE_PDF,
            ReturnEntity::LABEL_TYPE_QR,
        ], true)) {
            throw new RuntimeException('Invalid return type.');
        }

        $this->labelType = strtoupper($labelType);
    }

    /**
     * @param  ReturnEntity  $returnEntity
     * @return CreateShipmentDto
     */
    public function execute(ReturnEntity $returnEntity): CreateShipmentDto
    {
        $rollback = true;
        $order = $returnEntity->getOrder();

        $company = $order->getCompany();
        $name = $company->getName();
        if (!empty($order->getAltCompanyName())) {
            $name = $order->getAltCompanyName();
        }

        $phone = $company->getPhoneMain();
        if (!empty($order->getPhone())) {
            $phone = $order->getPhone();
        }

        if (empty($order->getEmail())) {
            $order->setEmail('sales@eyda.dk');
        }

        if (empty($phone)) {
            $phone = 70702503;
        }
        $phone = str_replace(' ', '', $phone);

//        switch ($order->getCompany()->getId()) {
//            case 7621: // My toys
//                $username = '2080075347';
//                $password = '&/DK!%/ksL&/(D!';
//                $customerId = '2080075347';
//                $contactId = '208a15ccmZ';
//                break;
//            case 7622: // Limango
//                $username = '2080075342';
//                $password = 'SG@nhxmp+8D7';
//                $customerId = '2080075342';
//                $contactId = '2080008609';
//                break;
//            default: // Bgc id'er Nye
//                $username = '2080075340';
//                $password = '@6ysRTt5uXCr';
//                $customerId = '2080075340';
//                $contactId = '208a15ccmY';
//
//                // From 62b4009a commit (VM1112\Eyda-server on 19.10.2023 at 10:42)
////                $username = '2080066327';
////                $password = '@PE5aY2BP5ykGkZ';
////                $customerId = '2080066327';
////                $contactId = '208a15cckN';
//        }

        $username = '2080066327';
        $password = 'PE5aY2BP5ykGkZ!';
        $customerId = '2080066327';
        $contactId = '208a15cckN';

        $address = $order->getAddress1() . ', ' . $order->getAddress2();
        $address = substr($address, 0, 39);

        /** @var Country $country */
        $country = $this->getCountryRepository()->find($order->getCountryId());
        if (!$country->getCountryNum()) {
            throw new RuntimeException('Country unit number cannot be empty.');
        }

        $createShipmentDto = new CreateShipmentDto();
        $createShipmentDto->setUsername($username);
        $createShipmentDto->setPassword($password);
        $createShipmentDto->setCustomerId($customerId);
        $createShipmentDto->setContactId($contactId);
        $createShipmentDto->setShipmentDate(date('Ymd'));
        $createShipmentDto->setReference($order->getReference());

        $addressesDeliveryDto = new AddressesDeliveryDto();
        $addressesDeliveryDto->setName1(utf8_encode($name));
        $addressesDeliveryDto->setStreet1(utf8_encode($address));
        $addressesDeliveryDto->setCountryNum($country->getCountryNum());
        $addressesDeliveryDto->setZipCode($order->getZip());
        $addressesDeliveryDto->setCity(utf8_encode($order->getCity()));
        $addressesDeliveryDto->setContact(utf8_encode($name));
        $addressesDeliveryDto->setEmail($order->getEmail());
        $addressesDeliveryDto->setPhone($phone);
        $addressesDeliveryDto->setMobile($phone);

        $pickUpInStore = false;

        if ($orderAddress = $this->getOrderAddressesRepository()->findByOrderIdAndType($order->getId(), 'shipping')) {
            /** @var Orderadresses $orderAddress */
            $address = $orderAddress->getAddress1() . ', ' . $orderAddress->getAddress2();
            $address = substr($address, 0, 39);

            /** @var Country $country */
            $country = $this->getCountryRepository()->getCountryById($orderAddress->getCountryId());

            if ($orderAddress->getPackageshop() > 0) {
                $pickUpInStore = $orderAddress->getPackageshop();
            }

            $addressesDeliveryDto->setName1(utf8_encode($orderAddress->getAltcompanyname()));
            $addressesDeliveryDto->setStreet1(utf8_encode($address));
            $addressesDeliveryDto->setZipCode($orderAddress->getZip());
            $addressesDeliveryDto->setCity(utf8_encode($orderAddress->getCity()));
            $addressesDeliveryDto->setContact(utf8_encode($name));
            $addressesDeliveryDto->setEmail(utf8_encode($order->getEmail()));
            $addressesDeliveryDto->setPhone($phone);
            $addressesDeliveryDto->setMobile($phone);

            $addressesDeliveryDto->setCountryNum($country->getCountryNum());
            if ((int)$country->getCountryNum() < 100) {
                $addressesDeliveryDto->setCountryNum(0 . $country->getCountryNum());
            }
        }

        $parcels = [];

//        /** @var ReturnLine $returnLine */
//        foreach ($returnEntity->getReturnLines() as $returnLine) {
//            /** @var Orderline $orderLine */
//            $orderLine = $returnLine->getOrderLine();
//
//            if ($orderLine->getItems()->count() < 1) {
//                continue;
//            }
//
//            /** @var Item $item */
//            foreach ($orderLine->getItems() as $item) {
//                /** @var Container $container */
//                $container = $item->getContainer();
//
//                if ($item->getParentid() > 0) {
//                    $parent = $item->getParent();
//                    $container = $parent->getContainer();
//                }
//
//                $parcelsDto = new ParcelsDto();
//                $parcelsDto->setWeight($container->getGrossWeight() < 1.0 ? 1.0 : $container->getGrossWeight());
//                $parcelsDto->setReference($container->getId());
//
//                $parcels[] = $parcelsDto;
//            }
//        }

        $parcelsDto = new ParcelsDto();
        $parcelsDto->setWeight($this->calculateWeight($returnEntity));
        $parcelsDto->setReference($returnEntity->getId());
        $parcels[] = $parcelsDto;

        if (empty($parcels)) {
            throw new RuntimeException('No parcels found for order: ' . $order->getId());
        }
        $createShipmentDto->setParcels($parcels);

        $servicesDto = new ServicesDto();
        if (!empty($order->getEmail())) {
            $servicesDto->setNotificationEmail($order->getEmail());
        }

//        TODO: CountryNum is not in the country table
//        if($countryRecord['CountryNum'] == 208){
//            if( $carrier['ParcelType'] == 'PrivateDeliveryService'){
//                $parcel['Services']['PrivateDelivery'] =  "Y";
//            }
//            else{
        if ($pickUpInStore !== false) {
            $servicesDto->setShopDelivery($pickUpInStore);
        }
//            }
//        }

        $createReturn = false;
        if ($order->getCompany()->getId() === 7779) {
            if ($country->getCountryNum() === 208 || $country->getCountryNum() === 752) {
                $createReturn = true;
            }
        }

        $createShipmentDto->setServices($servicesDto);

        if ($rollback === true) {
            $addressesPickupDto = new AddressesPickupDto();
            $addressesPickupDto->setName1($addressesDeliveryDto->getName1());
            $addressesPickupDto->setStreet1($addressesDeliveryDto->getStreet1());
            $addressesPickupDto->setCountryNum($addressesDeliveryDto->getCountryNum());
            $addressesPickupDto->setZipCode($addressesDeliveryDto->getZipCode());
            $addressesPickupDto->setCity($addressesDeliveryDto->getCity());
            $addressesPickupDto->setContact($addressesDeliveryDto->getContact());
            $addressesPickupDto->setEmail($addressesDeliveryDto->getEmail());
            $addressesPickupDto->setPhone($addressesDeliveryDto->getPhone());
            $addressesPickupDto->setMobile($addressesDeliveryDto->getMobile());

            $addressesDeliveryDto = new AddressesDeliveryDto();
            $addressesDeliveryDto->setName1('Eyda C/O Logistix');
            $addressesDeliveryDto->setStreet1('Sandagervej 10');
            $addressesDeliveryDto->setCountryNum(208);
            $addressesDeliveryDto->setZipCode('7400');
            $addressesDeliveryDto->setCity('Herning');

            $servicesDto->setShopReturn('Y');

            $addressesDto = new AddressesDto();
            $addressesDto->setPickup($addressesPickupDto);
            $addressesDto->setDelivery($addressesDeliveryDto);

            $createShipmentDto->setAddresses($addressesDto);
            $createShipmentDto->setServices($servicesDto);
            $createShipmentDto->setLabelFormat($this->getLabelType());

            return $createShipmentDto;
        }
    }

    /**
     * @param ReturnEntity $returnEntity
     * @return float
     */
    private function calculateWeight(ReturnEntity $returnEntity): float
    {
        $weight = 0; // grams

        foreach ($returnEntity->getReturnLines() as $returnLine) {
            /** @var Orderline $orderLine */
            $orderLine = $returnLine->getOrderLine();
            /** @var OrderQuantity $orderQuantity */
            $orderQuantity = $orderLine->getOrderQuantity();
            /** @var ArticleSize $articleSize */
            $articleSize = $orderQuantity->getArticleSize();
            /** @var Article $article */
            $article = $articleSize->getArticle();
            /** @var Variantcode $variantCode */
            $variantCode = $this->variantcodeRepository->findOneBy(
                [
                    'articleid' => $article->getId(),
                    'articlesizeid' => $articleSize->getId()
                ]
            );

            $weight += $variantCode->getWeight() * $returnLine->getQuantity();
        }

        $weight /= 1000; // kg

        if ($weight < 0.1) {
            $weight = 0.1;
        }

        return (float)$weight;
    }
}
