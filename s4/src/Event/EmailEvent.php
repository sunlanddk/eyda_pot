<?php

namespace App\Event;

use Symfony\Contracts\EventDispatcher\Event;

class EmailEvent extends Event
{
    public const TYPE_RETURN_LABEL = 'return_label';

    protected string $recipient;
    protected array $parameters;
    protected ?string $userLocale = null;

    public function __construct(string $recipient, array $parameters, ?string $userLocale = null)
    {
        $this->recipient = $recipient;
        $this->parameters = $parameters;
        $this->userLocale = $userLocale;
    }

    /**
     * @return string
     */
    public function getRecipient(): string
    {
        return $this->recipient;
    }

    /**
     * @return array
     */
    public function getParameters(): array
    {
        return $this->parameters;
    }

    /**
     * @return string|null
     */
    public function getUserLocale(): ?string
    {
        return $this->userLocale;
    }
}
