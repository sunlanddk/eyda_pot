<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230925122437 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Preadvised to Refund navigation';
    }

    public function up(Schema $schema): void
    {
        // Add Preadvised to Refund navigation
        $this->addSql(
            "
            SET @parentId = (SELECT `Id` FROM `navigation` WHERE `Name` = 'Refund' AND `ParentId` = '0');

            INSERT INTO `navigation` (
                `Id`, `Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, 
                `Header`, `FunctionId`, `Active`, `CreateDate`, `ModifyDate`, `Operation`, `Focus`, `CreateUserId`, 
                `ModifyUserId`, `NavigationTypeId`, `RefParameters`, `Width`, `Height`, `Back`, `PermOwner`, `PermInternal`, 
                `Mark`, `Disable`, `Icon`, `RefConfirm`, `RefIdInclude`, `RefTable`, `ProjectSelect`, `MustLoad`, 
                `AccessKey`, `Legacy`, `ShowCart`, `isDefault`
            ) VALUES (
                DEFAULT, 'Preadvised', @parentId, 1, 'refund', '', 'list-preadvised', 'm', 'Preadvised', 40, 1, 
                '0001-01-01 12:00:00', '0001-01-01 12:00:00', 'r', '', 1, 1, 1, '', 0, 0, 0, 0, 0, '', 
                0, 'edit.gif', '', 0, '', 0, 0, '', 1, 0, 0
            )
            "
        );

        // Add Preadvised View to Refund navigation
        $this->addSql(
            "
            SET @parentId = (SELECT `Id` FROM `navigation` WHERE `Name` = 'Refund' AND `ParentId` = '0');
            SET @preadvisedId = (SELECT `Id` FROM `navigation` WHERE `Name` = 'Preadvised' AND `ParentId` = @parentId);

            INSERT INTO `navigation` (
                `Id`, `Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, 
                `Header`, `FunctionId`, `Active`, `CreateDate`, `ModifyDate`, `Operation`, `Focus`, `CreateUserId`, 
                `ModifyUserId`, `NavigationTypeId`, `RefParameters`, `Width`, `Height`, `Back`, `PermOwner`, `PermInternal`, 
                `Mark`, `Disable`, `Icon`, `RefConfirm`, `RefIdInclude`, `RefTable`, `ProjectSelect`, `MustLoad`, 
                `AccessKey`, `Legacy`, `ShowCart`, `isDefault`
            ) VALUES (
                DEFAULT, 'Preadvised View', @preadvisedId, 1, 'refund', '', 'view-preadvised', 'l', 'Preadvised View', 40, 1, 
                '0001-01-01 12:00:00', '0001-01-01 12:00:00', 'r', '', 1, 1, 1, '', 0, 0, 0, 0, 0, 'refundview', 
                0, 'edit.gif', '', 1, '', 0, 1, '', 1, 0, 0
            )
            "
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql("DELETE FROM `navigation` WHERE `Name` = 'Preadvised' AND `Module` = 'refund' AND `Function` = 'list-preadvised'");
        $this->addSql("DELETE FROM `navigation` WHERE `Name` = 'Preadvised View' AND `Module` = 'refund' AND `Function` = 'view-preadvised'");
    }
}
