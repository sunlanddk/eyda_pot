<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230417122158 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create returnline table';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('returnline');
        $table->addColumn('id', 'integer', ['autoincrement' => true, 'unsigned' => true]);
        $table->addColumn('createDate', 'datetime')->setDefault('CURRENT_TIMESTAMP');
        $table->addColumn('createUserId', 'integer');
        $table->addColumn('modifyDate', 'datetime')->setDefault('CURRENT_TIMESTAMP');
        $table->addColumn('modifyUserId', 'integer');
        $table->addColumn('active', 'integer')->setDefault(1);
        $table->addColumn('returnId', 'integer')->setUnsigned(true);
        $table->addColumn('orderLineId', 'integer')->setUnsigned(true);
        $table->addColumn('refundReasonId', 'integer')->setUnsigned(true);
        $table->addColumn('quantity', 'integer');
        $table->addColumn('comment', 'string', ['length' => 255])->setNotnull(false);
        $table->setPrimaryKey(['id']);
        $table->addForeignKeyConstraint('return', ['returnId'], ['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('returnline');
    }
}
