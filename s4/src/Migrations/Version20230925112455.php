<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Throwable;

final class Version20230925112455 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add presale column to variantcode table';
    }

    public function up(Schema $schema): void
    {
        try {
            $this->addSql('ALTER TABLE variantcode ADD Presale SMALLINT DEFAULT 0');
        } catch (Throwable $t) {
            //throw $t;
        }
    }

    public function down(Schema $schema): void
    {
        try {
            $this->addSql('ALTER TABLE variantcode DROP Presale');
        } catch (Throwable $t) {
            //throw $t;
        }
    }
}
