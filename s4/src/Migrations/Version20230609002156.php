<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230609002156 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add fundsType to return';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` ADD fundsType TINYINT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP fundsType');
    }
}
