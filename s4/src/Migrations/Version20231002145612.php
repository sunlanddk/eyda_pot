<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231002145612 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add default values to tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "
            INSERT INTO articlecolor (
                Active,
                Comment,
                Reference,
                HeavyMetals,
                Line00Description,
                Line01Description,
                Line02Description,
                Line03Description,
                Line04Description,
                Line05Description,
                Line06Description,
                Line07Description,
                Line08Description,
                Line09Description,
                Line10Description,
                Line11Description,
                Line12Description,
                LabdyeComment
            ) VALUES (
                0,
                '',
                '',
                0,
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                '',
                ''
            );
            SET @max_id = (SELECT MAX(Id) FROM articlecolor);
            UPDATE articlecolor SET id = 0 WHERE id = @max_id;
        "
        );


        $this->addSql(
            "
            INSERT INTO articlesize (
                Active,
                Name
            )
            VALUES (
                0,
                ''
            );
            SET @max_id = (SELECT MAX(Id) FROM articlesize);
            UPDATE articlesize SET id = 0 WHERE id = @max_id;
        "
        );


        $this->addSql(
            "
            INSERT INTO orderline (
                Active,
                Description,
                InvoiceFooter,
                CustArtRef
            )
            VALUES (
                0,
                '',
                '',
                ''
            );
            SET @max_id = (SELECT MAX(Id) FROM orderline);
            UPDATE orderline SET id = 0 WHERE id = @max_id;
        "
        );


        $this->addSql(
            "
            INSERT INTO container (
                Active,
                Position,
                Description
            )
            VALUES (
                0,
                '',
                ''
            );
            SET @max_id = (SELECT MAX(Id) FROM container);
            UPDATE container SET id = 0 WHERE id = @max_id;
        "
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM articlecolor WHERE Id = 0');
        $this->addSql('DELETE FROM articlesize WHERE Id = 0');
        $this->addSql('DELETE FROM orderline WHERE Id = 0');
        $this->addSql('DELETE FROM container WHERE Id = 0');
    }
}
