<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231101133121 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add email to return table and change comment field in returnline table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` ADD email VARCHAR(50) DEFAULT NULL AFTER `labelType`');
        $this->addSql('ALTER TABLE `returnline` MODIFY COLUMN comment VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_danish_ci');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP email');
        $this->addSql('ALTER TABLE `returnline` MODIFY COLUMN comment VARCHAR(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci');
    }
}
