<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230922160155 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add refundreason_lang table';
    }

    public function up(Schema $schema): void
    {
        // Add isoCode column to language table
        $this->addSql(
            'ALTER TABLE language
            ADD isoCode VARCHAR(2) NOT NULL AFTER ModifyUserId'
        );

        // Update language table with isoCode values
        $this->addSql(
            'UPDATE language SET isoCode = "en" WHERE id = 1'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "ar" WHERE id = 2'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "ua" WHERE id = 3'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "da" WHERE id = 4'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "de" WHERE id = 5'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "en" WHERE id = 6'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "no" WHERE id = 7'
        );
        $this->addSql(
            'UPDATE language SET isoCode = "se" WHERE id = 8'
        );

        // Add refundreason_lang table
        $this->addSql(
            'CREATE TABLE refundreason_lang (
                id INT AUTO_INCREMENT NOT NULL,
                refundreason_id INT NOT NULL,
                language_id INT NOT NULL,
                name VARCHAR(255) NOT NULL,
                description VARCHAR(255) DEFAULT NULL,
                INDEX IDX_2F9B3A4F6A2B367E (refundreason_id),
                INDEX IDX_2F9B3A4F7E3C61F9 (language_id),
                PRIMARY KEY(id)
            )
            DEFAULT CHARACTER SET utf8mb4
            COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB'
        );

        // Add foreign keys to refundreason_lang table
        $this->addSql(
            'INSERT INTO refundreason_lang (refundreason_id, language_id, name, description)
            SELECT id, 4, name, description FROM refundreason'
        );
        $this->addSql(
            'INSERT INTO refundreason_lang (refundreason_id, language_id, name, description)
            SELECT id, 6, name, description FROM refundreason'
        );
        $this->addSql(
            'INSERT INTO refundreason_lang (refundreason_id, language_id, name, description)
            SELECT id, 8, name, description FROM refundreason'
        );
        $this->addSql(
            'INSERT INTO refundreason_lang (refundreason_id, language_id, name, description)
            SELECT id, 7, name, description FROM refundreason'
        );
    }

    public function down(Schema $schema): void
    {
        // Remove isoCode column from language table
        $this->addSql('ALTER TABLE language DROP isoCode');

        // Remove refundreason_lang table
        $this->addSql('DROP TABLE refundreason_lang');
    }
}
