<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230613122134 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add type column to refundreason table';
    }

    public function up(Schema $schema): void
    {
        if (!$schema->getTable('refundreason')->hasColumn('type')) {
            $this->addSql('ALTER TABLE refundreason ADD `type` ENUM("customer", "warehouse") NOT NULL');
            $this->addSql('UPDATE refundreason SET `type` = "warehouse"');
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE refundreason DROP `type`');
    }
}
