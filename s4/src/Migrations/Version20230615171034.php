<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230615171034 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Refactor fundsType to ENUM in return table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP fundsType');
        $this->addSql('ALTER TABLE `return` ADD fundsType ENUM("default", "refund", "gift_card") DEFAULT "default"');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP fundsType');
        $this->addSql('ALTER TABLE `return` ADD fundsType TINYINT DEFAULT NULL');
    }
}
