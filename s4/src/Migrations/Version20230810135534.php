<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230810135534 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add labelType to return table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` ADD labelType VARCHAR(255) DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP labelType');
    }
}
