<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230614114858 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add refundId to return';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('DELETE FROM doctrine_migration_versions WHERE version LIKE "%Version20231209110456%"');
        $this->addSql('DELETE FROM doctrine_migration_versions WHERE version LIKE "%Version20231309122134%"');

        $this->addSql('ALTER TABLE `return` ADD refundId INT UNSIGNED DEFAULT NULL AFTER orderId');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM doctrine_migration_versions WHERE version LIKE "%Version20231209110456%"');
        $this->addSql('DELETE FROM doctrine_migration_versions WHERE version LIKE "%Version20231309122134%"');

        $this->addSql('ALTER TABLE `return` DROP refundId');
    }
}
