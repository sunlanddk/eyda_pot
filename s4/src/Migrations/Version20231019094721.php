<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231019094721 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add emailLog to return table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` ADD emailLog TEXT DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP emailLog');
    }
}
