<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230417122148 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Create return table';
    }

    public function up(Schema $schema): void
    {
        $table = $schema->createTable('return');
        $table->addColumn('id', 'integer', ['autoincrement' => true, 'unsigned' => true]);
        $table->addColumn('createDate', 'datetime')->setDefault('CURRENT_TIMESTAMP');
        $table->addColumn('createUserId', 'integer');
        $table->addColumn('modifyDate', 'datetime')->setDefault('CURRENT_TIMESTAMP');
        $table->addColumn('modifyUserId', 'integer');
        $table->addColumn('active', 'integer')->setDefault(1);
        $table->addColumn('orderId', 'integer', ['unsigned' => true]);
        $table->addColumn('trackingCode', 'string', ['length' => 255])->setNotnull(false);
        $table->setPrimaryKey(['id']);
    }

    public function down(Schema $schema): void
    {
        $schema->dropTable('return');
    }
}
