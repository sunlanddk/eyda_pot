<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231005083737 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add default values to tables';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "
            INSERT INTO item (
                Active,
                Comment
            )
            VALUES (
                0,
                ''
            );
            SET @max_id = (SELECT MAX(Id) FROM item);
            UPDATE item SET id = 0 WHERE id = @max_id;
        "
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM item WHERE Id = 0');
    }
}
