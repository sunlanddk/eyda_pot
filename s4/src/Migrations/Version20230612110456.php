<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20230612110456 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Fill countryNum column in Country table with data';
    }

    public function up(Schema $schema): void
    {
        if (!$schema->getTable('country')->hasColumn('countryNum')) {
            $this->addSql('ALTER TABLE country ADD countryNum SMALLINT DEFAULT NULL');
        }

        $data = [
            'AD' => 20,
            'AE' => 784,
            'AT' => 40,
            'AU' => 36,
            'BA' => 70,
            'BE' => 56,
            'BG' => 100,
            'BHR' => 48,
            'BY' => 112,
            'CA' => 124,
            'CH' => 756,
            'CL' => 152,
            'CN' => 156,
            'CR' => 188,
            'CZ' => 203,
            'DE' => 276,
            'DK' => 208,
            'EE' => 233,
            'EG' => 818,
            'ES' => 724,
            'FI' => 246,
            'FO' => 234,
            'FR' => 250,
            'GB' => 826,
            'GE' => 268,
            'GL' => 304,
            'GR' => 300,
            'HK' => 344,
            'HU' => 348,
            'ID' => 360,
            'IE' => 372,
            'IL' => 376,
            'IN' => 356,
            'IS' => 352,
            'IT' => 380,
            'JP' => 392,
            'KR' => 410,
            'KW' => 414,
            'LB' => 422,
            'LI' => 438,
            'LT' => 440,
            'LU' => 442,
            'LV' => 428,
            'MC' => 492,
            'MT' => 470,
            'MX' => 484,
            'MY' => 458,
            'NG' => 566,
            'NL' => 528,
            'NO' => 578,
            'NZ' => 554,
            'PA' => 591,
            'PE' => 604,
            'PK' => 586,
            'PL' => 616,
            'PT' => 620,
            'RO' => 642,
            'RU' => 643,
            'SE' => 752,
            'SG' => 702,
            'SI' => 705,
            'SK' => 703,
            'TH' => 764,
            'TN' => 788,
            'TR' => 792,
            'TW' => 158,
            'UA' => 804,
            'UG' => 800,
            'US' => 840,
            'VI' => 850,
            'ZA' => 710,
        ];

        $this->addSql('START TRANSACTION');
        try {
            foreach ($data as $key => $value) {
                $this->addSql("UPDATE country SET countryNum = $value WHERE IsoCode = '$key'");
            }
            $this->addSql('COMMIT');
        } catch (Throwable $th) {
            $this->addSql('ROLLBACK');
            dd($th);
        }
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE country DROP countryNum');
    }
}
