<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231123094057 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add emailLanguage to return table';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` ADD emailLanguage VARCHAR(255) DEFAULT NULL AFTER emailSendDate');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP emailLanguage');
    }
}
