<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231013144756 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add emailSendDate to return';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` ADD emailSendDate DATETIME DEFAULT NULL');
    }

    public function down(Schema $schema): void
    {
        $this->addSql('ALTER TABLE `return` DROP emailSendDate');
    }
}
