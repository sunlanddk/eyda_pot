<?php

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

final class Version20231010141821 extends AbstractMigration
{
    public function getDescription(): string
    {
        return 'Add Preadvised Resend Email to Refund navigation';
    }

    public function up(Schema $schema): void
    {
        $this->addSql(
            "
            SET @parentId = (SELECT `Id` FROM `navigation` WHERE `Name` = 'Refund' AND `ParentId` = '0');
            SET @preadvisedId = (SELECT `Id` FROM `navigation` WHERE `Name` = 'Preadvised' AND `ParentId` = @parentId);

            INSERT INTO `navigation` (
                `Id`, `Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, 
                `Header`, `FunctionId`, `Active`, `CreateDate`, `ModifyDate`, `Operation`, `Focus`, `CreateUserId`, 
                `ModifyUserId`, `NavigationTypeId`, `RefParameters`, `Width`, `Height`, `Back`, `PermOwner`, `PermInternal`, 
                `Mark`, `Disable`, `Icon`, `RefConfirm`, `RefIdInclude`, `RefTable`, `ProjectSelect`, `MustLoad`, 
                `AccessKey`, `Legacy`, `ShowCart`, `isDefault`
            ) VALUES (
                DEFAULT, 'Preadvised Resend Email', @preadvisedId, 1, 'refund', '', 'preadvised-email', 'l', 'Preadvised Resend Email', 38, 1, 
                '0001-01-01 12:00:00', '0001-01-01 12:00:00', 'w', '', 1, 1, 3, '', 600, 550, 0, 0, 0, 'preadvisedresendemail', 
                0, 'email.gif', '', 1, '', 0, 0, '', 1, 0, 0
            )
            "
        );

        $this->addSql(
            "
            SET @preadvisedId = (SELECT `Id` FROM `navigation` WHERE `Name` = 'Preadvised Resend Email' AND `Module` = 'refund' AND `Function` = 'preadvised-email');

            INSERT INTO `navigation` (
                `Id`, `Name`, `ParentId`, `DisplayOrder`, `Module`, `Parameters`, `Function`, `RefType`, 
                `Header`, `FunctionId`, `Active`, `CreateDate`, `ModifyDate`, `Operation`, `Focus`, `CreateUserId`, 
                `ModifyUserId`, `NavigationTypeId`, `RefParameters`, `Width`, `Height`, `Back`, `PermOwner`, `PermInternal`, 
                `Mark`, `Disable`, `Icon`, `RefConfirm`, `RefIdInclude`, `RefTable`, `ProjectSelect`, `MustLoad`, 
                `AccessKey`, `Legacy`, `ShowCart`, `isDefault`
            ) VALUES (
                DEFAULT, 'Preadvised Resend Email', @preadvisedId, 2, 'refund', '', 'resendEmail()', 't', 'Preadvised Resend Email', 40, 1, 
                '0001-01-01 12:00:00', '0001-01-01 12:00:00', 'w', '', 1, 1, 17, '', 0, 0, 0, 0, 0, '', 
                0, 'email.gif', '', 0, '', 0, 0, '', 1, 0, 0
            )
            "
        );

        $this->addSql(
            "
            INSERT INTO maillayout
                (CreateDate, CreateUserId, ModifyDate, ModifyUserId, Active, Mark, Subject, Header, Body, Description, Body_default)
            VALUES
                ('0001-01-01 12:00:00', 0, '0001-01-01 12:00:00', 1, 1, 'preadvisedresendemail', 'Return QR code for order #1234567890', '', 
                    '<div class=\"container\">
                        <!--[if gte mso 9]>
                        <v:background xmlns:v=\"urn:schemas-microsoft-com:vml\" fill=\"t\">
                            <v:fill type=\"tile\" color=\"#ffffff\"></v:fill>
                        </v:background>
                        <![endif]-->
                        <div class=\"content\">
                            <h1>Welcome example@gmail.com!</h1>
                            <h2>Your return has been processed.</h2>
                            <br>
                            <p>Your tracking code: EX1234567890</p>
                        </div>
        
                        </br>
                        </br>
        
                        <div class=\"container-table\">
                            <table class=\"table\">
                                <thead class=\"table-thead\">
                                    <tr>
                                        <th>
                                            <p>Product Name</p>
                                        </th>
                                        <th style=\"text-align: center;\">
                                            <p>Returned</p>
                                        </th>
                                        <th style=\"text-align: center;\">
                                            <p>Quantity</p>
                                        </th>
                                        <th>
                                            <p>Reason</p>
                                        </th>
                                        <th>
                                            <p>Comment</p>
                                        </th>
                                    </tr>
                                </thead>
        
                                <tbody class=\"table-tbody\">
                                    <tr>
                                        <td>
                                            <p>Order Item Name</p>
                                        </td>
                                        <td style=\"text-align: center;\">
                                            <p>1</p>
                                        </td>
                                        <td style=\"text-align: center;\">
                                            <p>1</p>
                                        </td>
                                        <td>
                                            <p>Return reason</p>
                                        </td>
                                        <td>
                                            <p>Example comment</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
        
                        </br>
                        </br>
        
                        <div class=\"content\">
                            <p>You can find your return QR code in the attachment.</p>
                        </div>
                        
                        </br>
                        </br>
                    
                        <div class=\"content\">
                            <img src=\"https://api.qrserver.com/v1/create-qr-code/?data=HelloWorld&amp;size=100x100\" alt=\"QR Code\" width=\"70\" height=\"70\" />
                        </div>
                    </div>', 
                    '',
                    ''
            );
            "
        );
    }

    public function down(Schema $schema): void
    {
        $this->addSql('DELETE FROM `navigation` WHERE `Name` = "Preadvised Resend Email" AND `Module` = "refund" AND `Function` = "preadvised-email"');
        $this->addSql('DELETE FROM `navigation` WHERE `Name` = "Preadvised Resend Email" AND `Module` = "refund" AND `Function` = "resendEmail()"');
        $this->addSql('DELETE FROM `maillayout` WHERE `Mark` = "preadvisedresendemail"');
    }
}
