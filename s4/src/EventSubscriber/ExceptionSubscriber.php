<?php

namespace App\EventSubscriber;

use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

class ExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * @return array[]
     */
    public static function getSubscribedEvents(): array
    {
        // return the subscribed events, their methods and priorities
        return [
            KernelEvents::EXCEPTION => [
                ['processException', 10],
                ['logException', 0],
                ['notifyException', -10],
            ],
        ];
    }

    /**
     * @param ExceptionEvent $event
     * @return void
     */
    public function processException(ExceptionEvent $event): void
    {
        // ...
    }

    /**
     * @param ExceptionEvent $event
     * @return void
     */
    public function logException(ExceptionEvent $event): void
    {
        // ...
    }

    /**
     * @param ExceptionEvent $event
     * @return void
     */
    public function notifyException(ExceptionEvent $event): void
    {
        // ...
    }
}
