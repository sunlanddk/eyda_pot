<?php

namespace App\Dto\Api\GLS\CreateShipmentResponse;

class CreateShipmentDto
{
    private ?array $response = null;
    private string $consignmentId;
    private ?string $PDF = null;
    private array $parcels;
    private ?string $PNG = null;

    public function getResponse(): array { return $this->response; }

    public function getConsignmentId(): string { return $this->consignmentId; }
    public function setConsignmentId(string $value): void { $this->consignmentId = $value; }

    public function getPDF(): ?string { return $this->PDF; }
    public function setPDF(?string $value): void { $this->PDF = $value ?? null; }

    public function getParcels(): array { return $this->parcels; }
    public function setParcels(array $value): void { $this->parcels = $value; }

    public function getPNG(): ?string { return $this->PNG; }
    public function setPNG(?string $value): void { $this->PNG = $value; }

    /**
     * @param  array  $response
     * @return void
     */
    public function setResponse(array $response): void
    {
        $this->response = $response;
        $this->setConsignmentId($response['ConsignmentId']);
        $this->setPDF($response['PDF']);
        $this->setParcels($response['Parcels']);
        $this->setPNG($response['PNG']);
    }
}
