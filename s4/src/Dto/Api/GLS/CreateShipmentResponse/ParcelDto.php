<?php

namespace App\Dto\Api\GLS\CreateShipmentResponse;

class ParcelDto
{
    private string $uniqueNumber;
    private string $parcelNumber;
    private string $parcelNumber12Digit;
    private string $ndiNumber = '';
    private array $routing;
    private string $ndiNumber2 = '';
    private string $ndiNumber3 = '';
    private string $ndiNumber4 = '';

    public function getUniqueNumber(): string { return $this->uniqueNumber; }
    public function setUniqueNumber(string $value): void { $this->uniqueNumber = $value; }

    public function getParcelNumber(): string { return $this->parcelNumber; }
    public function setParcelNumber(string $value): void { $this->parcelNumber = $value; }

    public function getParcelNumber12Digit(): string { return $this->parcelNumber12Digit; }
    public function setParcelNumber12Digit(string $value): void { $this->parcelNumber12Digit = $value; }

    public function getNdiNumber(): string { return $this->ndiNumber; }
    public function setNdiNumber(string $value): void { $this->ndiNumber = $value; }

    public function getRouting(): array { return $this->routing; }
    public function setRouting(array $value): void { $this->routing = $value; }

    public function getNdiNumber2(): string { return $this->ndiNumber2; }
    public function setNdiNumber2(string $value): void { $this->ndiNumber2 = $value; }

    public function getNdiNumber3(): string { return $this->ndiNumber3; }
    public function setNdiNumber3(string $value): void { $this->ndiNumber3 = $value; }

    public function getNdiNumber4(): string { return $this->ndiNumber4; }
    public function setNdiNumber4(string $value): void { $this->ndiNumber4 = $value; }
}
