<?php

namespace App\Dto\Api\GLS\CreateShipmentRequest;

class AddressesDto
{
    /**
     * @var AddressesPickupDto
     */
    private AddressesPickupDto $pickup;

    /**
     * @var AddressesDeliveryDto
     */
    private AddressesDeliveryDto $delivery;

    public function getPickup(): AddressesPickupDto { return $this->pickup; }
    public function setPickup(AddressesPickupDto $value): void { $this->pickup = $value; }

    public function getDelivery(): AddressesDeliveryDto { return $this->delivery; }
    public function setDelivery(AddressesDeliveryDto $value): void { $this->delivery = $value; }

    /**
     * @return array
     */
    public function toArray(): array
    {
        return [
            'pickup' => $this->getPickup()->toArray(),
            'delivery' => $this->getDelivery()->toArray(),
        ];
    }
}
