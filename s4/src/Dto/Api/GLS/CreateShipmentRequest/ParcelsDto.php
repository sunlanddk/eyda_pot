<?php

namespace App\Dto\Api\GLS\CreateShipmentRequest;

class ParcelsDto
{
    private float $weight;
    private string $reference;
    private ?string $comment = null;
    private ?float $cashService = null;
    private ?float $addOnLiabilityService = null;
    private ?string $weightCategory = null;
    private ?float $height = null;
    private ?float $width = null;
    private ?float $length = null;

    public function getWeight(): float { return $this->weight; }
    public function setWeight(float $value): void { $this->weight = $value; }

    public function getReference(): string { return $this->reference; }
    public function setReference(string $value): void { $this->reference = $value; }

    public function getComment(): ?string { return $this->comment; }
    public function setComment(?string $value): void { $this->comment = $value; }

    public function getCashService(): ?float { return $this->cashService; }
    public function setCashService(?float $value): void { $this->cashService = $value; }

    public function getAddOnLiabilityService(): ?float { return $this->addOnLiabilityService; }
    public function setAddOnLiabilityService(?float $value): void { $this->addOnLiabilityService = $value; }

    public function getWeightCategory(): ?string { return $this->weightCategory; }
    public function setWeightCategory(?string $value): void { $this->weightCategory = $value; }

    public function getHeight(): ?float { return $this->height; }
    public function setHeight(?float $value): void { $this->height = $value; }

    public function getWidth(): ?float { return $this->width; }
    public function setWidth(?float $value): void { $this->width = $value; }

    public function getLength(): ?float { return $this->length; }
    public function setLength(?float $value): void { $this->length = $value; }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'weight' => $this->getWeight(),
            'reference' => $this->getReference(),
            'comment' => $this->getComment(),
            'cashService' => $this->getCashService(),
            'addOnLiabilityService' => $this->getAddOnLiabilityService(),
            'weightCategory' => $this->getWeightCategory(),
            'height' => $this->getHeight(),
            'width' => $this->getWidth(),
            'length' => $this->getLength(),
        ];

        return array_filter($data);
    }
}
