<?php

namespace App\Dto\Api\GLS\CreateShipmentRequest;

class CreateShipmentDto
{
    private string $username;
    private string $password;
    private string $customerId;
    private string $contactId;
    private string $shipmentDate;
    private string $reference;
    private ?string $system = null;
    private ?string $labelSize = null;
    private ?string $labelFormat = null;

    /**
     * @var AddressesDto
     */
    private AddressesDto $addresses;

    /**
     * @var ServicesDto
     */
    private ServicesDto $services;

    /**
     * @var array ParcelsDto[]
     */
    private array $parcels = [];

    public function getUsername(): string { return $this->username; }
    public function setUsername(string $value): void { $this->username = $value; }

    public function getPassword(): string { return $this->password; }
    public function setPassword(string $value): void { $this->password = $value; }

    public function getCustomerId(): string { return $this->customerId; }
    public function setCustomerId(string $value): void { $this->customerId = $value; }

    public function getContactId(): string { return $this->contactId; }
    public function setContactId(string $value): void { $this->contactId = $value; }

    public function getShipmentDate(): string { return $this->shipmentDate; }
    public function setShipmentDate(string $value): void { $this->shipmentDate = $value; }

    public function getReference(): string { return $this->reference; }
    public function setReference(string $value): void { $this->reference = $value; }

    public function getSystem(): ?string { return $this->system; }
    public function setSystem(?string $value): void { $this->system = $value; }

    public function getLabelSize(): ?string { return $this->labelSize; }
    public function setLabelSize(?string $value): void { $this->labelSize = $value; }

    public function getLabelFormat(): ?string { return $this->labelFormat; }
    public function setLabelFormat(?string $value): void { $this->labelFormat = $value; }

    public function getAddresses(): AddressesDto { return $this->addresses; }
    public function setAddresses(AddressesDto $value): void { $this->addresses = $value; }

    public function getServices(): ServicesDto { return $this->services; }
    public function setServices(ServicesDto $value): void { $this->services = $value; }

    /**
     * @return ParcelsDto[]
     */
    public function getParcels(): array { return $this->parcels; }
    /**
     * @param ParcelsDto[] $value
     */
    public function setParcels(array $value): void { $this->parcels = $value; }

    /**
     * @return array
     */
    public function toArray(): array
    {
        $data = [
            'username' => $this->getUsername(),
            'password' => $this->getPassword(),
            'customerId' => $this->getCustomerId(),
            'contactId' => $this->getContactId(),
            'shipmentDate' => $this->getShipmentDate(),
            'reference' => $this->getReference(),
            'system' => $this->getSystem(),
            'labelSize' => $this->getLabelSize(),
            'labelFormat' => $this->getLabelFormat(),
            'addresses' => $this->getAddresses()->toArray(),
            'services' => $this->getServices()->toArray(),
            'parcels' => array_map(fn(ParcelsDto $parcel) => $parcel->toArray(), $this->getParcels()),
        ];

        return array_filter($data);
    }
}
