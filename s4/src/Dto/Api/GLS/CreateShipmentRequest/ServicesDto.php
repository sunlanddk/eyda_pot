<?php

namespace App\Dto\Api\GLS\CreateShipmentRequest;

class ServicesDto
{
    private ?string $shopDelivery = null;
    private ?string $notificationEmail = null;
    private ?string $deposit = null;
    private ?string $shopReturn = null;
    private ?string $pickAndReturn = null;
    private ?string $C2X = null;
    private ?string $express10 = null;
    private ?string $express12 = null;
    private ?string $addresseeOnly = null;
    private ?string $directShop = null;
    private ?string $privateDelivery = null;
    private ?string $G24 = null;
    private ?string $flexDelivery = null;
    private ?string $thinkGreen = null;
    private ?string $sameDay = null;
    private ?string $parcelShop = null;

    public function getShopDelivery(): ?string { return $this->shopDelivery; }
    public function setShopDelivery(?string $value): void { $this->shopDelivery = $value; }

    public function getNotificationEmail(): ?string { return $this->notificationEmail; }
    public function setNotificationEmail(?string $value): void { $this->notificationEmail = $value; }

    public function getDeposit(): ?string { return $this->deposit; }
    public function setDeposit(?string $value): void { $this->deposit = $value; }

    public function getShopReturn(): ?string { return $this->shopReturn; }
    public function setShopReturn(?string $value): void { $this->shopReturn = $value; }

    public function getPickAndReturn(): ?string { return $this->pickAndReturn; }
    public function setPickAndReturn(?string $value): void { $this->pickAndReturn = $value; }

    public function getC2X(): ?string { return $this->C2X; }
    public function setC2X(?string $value): void { $this->C2X = $value; }

    public function getExpress10(): ?string { return $this->express10; }
    public function setExpress10(?string $value): void { $this->express10 = $value; }

    public function getExpress12(): ?string { return $this->express12; }
    public function setExpress12(?string $value): void { $this->express12 = $value; }

    public function getAddresseeOnly(): ?string { return $this->addresseeOnly; }
    public function setAddresseeOnly(?string $value): void { $this->addresseeOnly = $value; }

    public function getDirectShop(): ?string { return $this->directShop; }
    public function setDirectShop(?string $value): void { $this->directShop = $value; }

    public function getPrivateDelivery(): ?string { return $this->privateDelivery; }
    public function setPrivateDelivery(?string $value): void { $this->privateDelivery = $value; }

    public function getG24(): ?string { return $this->G24; }
    public function setG24(?string $value): void { $this->G24 = $value; }

    public function getFlexDelivery(): ?string { return $this->flexDelivery; }
    public function setFlexDelivery(?string $value): void { $this->flexDelivery = $value; }

    public function getThinkGreen(): ?string { return $this->thinkGreen; }
    public function setThinkGreen(?string $value): void { $this->thinkGreen = $value; }

    public function getSameDay(): ?string { return $this->sameDay; }
    public function setSameDay(?string $value): void { $this->sameDay = $value; }

    public function getParcelShop(): ?string { return $this->parcelShop; }
    public function setParcelShop(?string $value): void { $this->parcelShop = $value; }

    /**
     * @return array<string, string|null>
     */
    public function toArray(): array
    {
        $data = [
            'shopDelivery' => $this->getShopDelivery(),
            'notificationEmail' => $this->getNotificationEmail(),
            'deposit' => $this->getDeposit(),
            'shopReturn' => $this->getShopReturn(),
            'pickAndReturn' => $this->getPickAndReturn(),
            'C2X' => $this->getC2X(),
            'express10' => $this->getExpress10(),
            'express12' => $this->getExpress12(),
            'addresseeOnly' => $this->getAddresseeOnly(),
            'directShop' => $this->getDirectShop(),
            'privateDelivery' => $this->getPrivateDelivery(),
            'G24' => $this->getG24(),
            'flexDelivery' => $this->getFlexDelivery(),
            'thinkGreen' => $this->getThinkGreen(),
            'sameDay' => $this->getSameDay(),
            'parcelShop' => $this->getParcelShop(),
        ];

        return array_filter($data);
    }
}
