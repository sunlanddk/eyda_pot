<?php

namespace App\Dto\Api\GLS\CreateShipmentRequest;

class AddressesPickupDto
{
    private string $name1;
    private ?string $name2 = null;
    private ?string $name3 = null;
    private string $street1;
    private int $countryNum;
    private string $zipCode;
    private string $city;
    private ?string $contact = null;
    private ?string $email = null;
    private ?string $phone = null;
    private ?string $mobile = null;

    public function getName1(): string { return $this->name1; }
    public function setName1(string $value): void { $this->name1 = $value; }

    public function getName2(): ?string { return $this->name2; }
    public function setName2(?string $value): void { $this->name2 = $value; }

    public function getName3(): ?string { return $this->name3; }
    public function setName3(?string $value): void { $this->name3 = $value; }

    public function getStreet1(): string { return $this->street1; }
    public function setStreet1(string $value): void { $this->street1 = $value; }

    public function getCountryNum(): int { return $this->countryNum; }
    public function setCountryNum(int $value): void { $this->countryNum = $value; }

    public function getZipCode(): string { return $this->zipCode; }
    public function setZipCode(string $value): void { $this->zipCode = $value; }

    public function getCity(): string { return $this->city; }
    public function setCity(string $value): void { $this->city = $value; }

    public function getContact(): ?string { return $this->contact; }
    public function setContact(?string $value): void { $this->contact = $value; }

    public function getEmail(): ?string { return $this->email; }
    public function setEmail(?string $value): void { $this->email = $value; }

    public function getPhone(): ?string { return $this->phone; }
    public function setPhone(?string $value): void { $this->phone = $value; }

    public function getMobile(): ?string { return $this->mobile; }
    public function setMobile(?string $value): void { $this->mobile = $value; }

    /**
     * @return array<string, mixed>
     */
    public function toArray(): array
    {
        $data = [
            'name1' => $this->getName1(),
            'name2' => $this->getName2(),
            'name3' => $this->getName3(),
            'street1' => $this->getStreet1(),
            'countryNum' => $this->getCountryNum(),
            'zipCode' => $this->getZipCode(),
            'city' => $this->getCity(),
            'contact' => $this->getContact(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'mobile' => $this->getMobile(),
        ];

        return array_filter($data);
    }
}
