<?php

namespace App\Service\Order;

use App\Entity\Order\RefundReason;
use App\Repository\Order\RefundReasonRepository;
use Doctrine\ORM\EntityManagerInterface;

class RefundReasonService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return RefundReasonRepository
     */
    public function getRefundReasonRepository(): RefundReasonRepository
    {
        return $this->getEntityManager()->getRepository(RefundReason::class);
    }

    /**
     * @param  string  $type
     * @return array
     */
    public function getRefundReasons(string $type = RefundReason::TYPE_CUSTOMER): array
    {
        return $this->getRefundReasonRepository()->findBy(
            [
                'type' => $type,
                'active' => true,
            ]
        );
    }
}
