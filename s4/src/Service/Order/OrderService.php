<?php

namespace App\Service\Order;

use App\Entity\Order\Order;
use App\Exception\OrderNotFoundException;
use App\Repository\Order\OrderRepository;
use App\Service\ParameterService;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;

class OrderService
{
    private EntityManagerInterface $entityManager;
    private ParameterService $parameterService;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return OrderRepository
     */
    public function getOrderRepository(): OrderRepository
    {
        return $this->getEntityManager()->getRepository(Order::class);
    }

    /**
     * @return ParameterService
     */
    public function getParameterService(): ParameterService
    {
        return $this->parameterService;
    }

    /**
     * @param ParameterService $parameterService
     * @return void
     * @required
     */
    public function setParameterService(ParameterService $parameterService): void
    {
        $this->parameterService = $parameterService;
    }

    /**
     * @param string $orderNumber
     * @param string $orderEmail
     * @param string $orderPostcode
     * @return Order|null
     */
    public function findOrderByParametersOrFail(string $orderNumber, string $orderEmail, string $orderPostcode): ?Order
    {
        $order = $this->getOrderRepository()->findOneBy([
            'reference' => $orderNumber,
            'email' => $orderEmail,
            'zip' => $orderPostcode,
            'active' => true,
        ]);

        if (!$order) {
            throw new OrderNotFoundException(
                "No order found with order number: {$orderNumber}, order email: {$orderEmail}, and order postcode: {$orderPostcode}"
            );
        }

        return $order;
    }

    /**
     * @param string $orderNumber
     * @param string $orderEmail
     * @param string|null $orderPostcode
     * @return Order|null
     */
    public function findOneForReturn(string $orderNumber, string $orderEmail, ?string $orderPostcode = null): ?Order
    {
        $order = $this->getOrderRepository()->findOneForReturn(
            $orderNumber,
            $orderEmail,
            $orderPostcode,
            (new DateTime())
                ->modify($this->getParameterService()->getReturnExpiryTime() . ' DAYS')
                ->format('Y-m-d H:i:s')
        );

        if (!$order) {
            $message = "No order found with order number {$orderNumber} and email {$orderEmail}";
            if (!empty($orderPostcode)) {
                $message .= " and postcode {$orderPostcode}";
            }

            throw new OrderNotFoundException($message);
        }

        return $order;
    }
}
