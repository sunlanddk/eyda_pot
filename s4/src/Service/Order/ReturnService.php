<?php

namespace App\Service\Order;

use App\Entity\Order\Order;
use App\Entity\Order\Orderline;
use App\Entity\Order\RefundReason;
use App\Entity\Order\ReturnEntity;
use App\Entity\Order\ReturnLine;
use App\Event\EmailEvent;
use App\Exception\ReturnException;
use App\Repository\Order\OrderlineRepository;
use App\Repository\Order\OrderRepository;
use App\Repository\Order\ReturnLineRepository;
use App\Repository\Order\ReturnRepository;
use App\Support\Integrations\GLSService;
use App\Support\Integrations\IntegrationService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ReturnService
{
    private EntityManagerInterface $entityManager;
    private IntegrationService $integrationService;

    public function __construct(
        EntityManagerInterface $entityManager,
        IntegrationService $integrationService
    ) {
        $this->entityManager = $entityManager;
        $this->integrationService = $integrationService;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return IntegrationService
     */
    public function getIntegrationService(): IntegrationService
    {
        return $this->integrationService;
    }

    /**
     * @return ReturnRepository
     */
    public function getReturnRepository(): ReturnRepository
    {
        return $this->getEntityManager()->getRepository(ReturnEntity::class);
    }

    /**
     * @return ReturnLineRepository
     */
    public function getReturnLineRepository(): ReturnLineRepository
    {
        return $this->getEntityManager()->getRepository(ReturnLine::class);
    }

    /**
     * @return OrderRepository
     */
    public function getOrderRepository(): OrderRepository
    {
        return $this->getEntityManager()->getRepository(Order::class);
    }

    /**
     * @return OrderlineRepository
     */
    public function getOrderLineRepository(): OrderlineRepository
    {
        return $this->getEntityManager()->getRepository(Orderline::class);
    }

    /**
     * @param  Order  $order
     * @return ReturnEntity[]
     */
    public function getReturnsByOrder(Order $order): array
    {
        return $this->getReturnRepository()->findBy([
            'order' => $order,
            'active' => true
        ]);
    }

    /**
     * @param  Order  $order
     * @param  array  $orderReturn
     * @param  array  $orderReturnItems
     * @return ReturnEntity
     */
    public function storeReturnByOrder(
        Order $order,
        array $orderReturn,
        array $orderReturnItems
    ): ReturnEntity {
        $entity = new ReturnEntity();
        $entity->setOrder($order);

        $orderReturnFundsType = ReturnEntity::FUNDS_TYPE_DEFAULT;
        if (array_key_exists('orderReturnFundsType', $orderReturn) && !empty($orderReturn['orderReturnFundsType'])) {
            $orderReturnFundsType = $orderReturn['orderReturnFundsType'];
        }
        $entity->setFundsType($orderReturnFundsType);

        $orderReturnLabelType = ReturnEntity::LABEL_TYPE_DEFAULT;
        if (array_key_exists('orderReturnLabelType', $orderReturn) && !empty($orderReturn['orderReturnLabelType'])) {
            $orderReturnLabelType = $orderReturn['orderReturnLabelType'];
        }
        $entity->setLabelType($orderReturnLabelType);

        $this->getEntityManager()->getConnection()->beginTransaction();
        try {
            $this->getReturnRepository()->save($entity, true);
            $this->storeReturnLineByOrderLineItems($entity, $order, $orderReturnItems);

            $this->getEntityManager()->getConnection()->commit();
        } catch (Exception $e) {
            $this->getEntityManager()->getConnection()->rollBack();

            throw new ReturnException($e->getMessage());
        }

        return $entity;
    }

    /**
     * @param  ReturnEntity  $return
     * @param  Order  $order
     * @param  array  $orderLineItems
     * @return void
     */
    public function storeReturnLineByOrderLineItems(ReturnEntity $return, Order $order, array $orderLineItems): void
    {
        foreach ($orderLineItems as $orderLineItem) {
            /** @var Orderline $orderLine */
            $orderLine = $this->getOrderLineRepository()->findOneBy(
                [
                    'id' => $orderLineItem['orderLineId'],
                    'order' => $order,
                    'active' => true,
                ]
            );
            if (!$orderLine) {
                throw new ReturnException('Order line not found.', Response::HTTP_NOT_FOUND);
            }

            $returnLines = $this->getReturnLineRepository()->findBy([
                'orderLine' => $orderLine,
                'active' => true,
            ]);
            if (count($returnLines) > 0) {
                $returnLinesQuantity = array_reduce($returnLines, static function($accumulator, $returnLine) {
                    return $accumulator + $returnLine->getQuantity();
                }, 0);

                $packedQuantity = 0;
                $pickOrderLines = $orderLine->getOrderQuantity()->getPickOrderLines();
                foreach ($pickOrderLines as $pickOrderLine) {
                    $packedQuantity += $pickOrderLine->getPackedQuantity();
                }

                if (($returnLinesQuantity + (int)$orderLineItem['quantity']) > (int)$packedQuantity) {
                    throw new ReturnException('Return quantity cannot be greater than order quantity.');
                }
            }

            $entity = new ReturnLine();
            $entity->setReturn($return);
            $entity->setOrderLine($orderLine);
            $entity->setQuantity($orderLineItem['quantity']);
            $entity->setComment($orderLineItem['comment'] ?? null);
            $entity->setRefundReason(
                $this->getEntityManager()->getReference(RefundReason::class, $orderLineItem['refundReasonId'])
            );

            $this->getReturnLineRepository()->save($entity, true);
        }
    }

    /**
     * @param Request $request
     * @param int $orderId
     * @param int $returnId
     * @param string $labelType
     * @return BinaryFileResponse|null
     */
    public function generateReturnLabel(
        Request $request,
        int $orderId,
        int $returnId,
        string $labelType = ReturnEntity::LABEL_TYPE_QR
    ): ?BinaryFileResponse {
        $returnEntity = $this->getReturnRepository()->findOneBy(
            [
                'id' => $returnId,
                'order' => $orderId,
                'active' => true,
            ]
        );
        if (!$returnEntity) {
            throw new ReturnException('Return not found.', Response::HTTP_NOT_FOUND);
        }

        $integrationService = $this->getIntegrationService()->resolveService(GLSService::SERVICE_NAME);

        return $integrationService->resolveReturnLabel($request, $returnEntity, $labelType);
    }
}
