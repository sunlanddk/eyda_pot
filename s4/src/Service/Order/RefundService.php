<?php

namespace App\Service\Order;

use App\Entity\Order\Refund;
use App\Entity\Order\ReturnEntity;
use App\Repository\Order\RefundRepository;
use Doctrine\ORM\EntityManagerInterface;

class RefundService
{
    private EntityManagerInterface $entityManager;
    private ReturnService $returnService;

    public function __construct(
        EntityManagerInterface $entityManager,
        ReturnService $returnService
    ) {
        $this->entityManager = $entityManager;
        $this->returnService = $returnService;
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @return ReturnService
     */
    public function getReturnService(): ReturnService
    {
        return $this->returnService;
    }

    /**
     * @return RefundRepository
     */
    public function getRefundRepository(): RefundRepository
    {
        return $this->getEntityManager()->getRepository(Refund::class);
    }

    /**
     * @param  array  $post
     * @param  int  $headerUserId
     * @return int
     */
    public function createRefund(array $post, int $headerUserId): int
    {
        $refund = $this->getRefundRepository()->createPassonRefund($post, $headerUserId);

        /** @var ReturnEntity $return */
        $return = $this->getReturnService()->getReturnRepository()->findOneBy(
            [
                'order' => $refund->getOrderid(),
            ],
            [
                'createDate' => 'DESC',
            ]
        );

        if ($return) {
            $return->setRefund($refund);
            $this->getEntityManager()->persist($return);
            $this->getEntityManager()->flush();
        }

        return $refund->getId();
    }
}
