<?php

namespace App\Service;

use App\Entity\Parameter;
use App\Repository\ParameterRepository;
use Doctrine\ORM\EntityManagerInterface;

class ParameterService
{
    private EntityManagerInterface $entityManager;

    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @return ParameterRepository
     */
    public function getParameterRepository(): ParameterRepository
    {
        return $this->getEntityManager()->getRepository(Parameter::class);
    }

    /**
     * @return EntityManagerInterface
     */
    public function getEntityManager(): EntityManagerInterface
    {
        return $this->entityManager;
    }

    /**
     * @param string $mark
     * @return string|null
     */
    public function getByMark(string $mark): ?string
    {
        $parameter = $this->getParameterRepository()->findOneByMark($mark);
        if (!$parameter) {
            return null;
        }

        return $parameter->getValue();
    }

    /**
     * @return int
     */
    public function getReturnExpiryTime(): int
    {
        $returnExpiryTime = (int)$this->getByMark(Parameter::MARK_RETURN_EXPIRY_TIME);
        if ($returnExpiryTime === 0) {
            return -35;
        }
        if ($returnExpiryTime > 0) {
            return -1 * $returnExpiryTime;
        }

        return $returnExpiryTime;
    }
}
