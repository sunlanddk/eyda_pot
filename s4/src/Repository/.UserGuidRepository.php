<?php

namespace App\Repository;

use App\Entity\UserGuid;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use App\Repository\UserLoginRepository;
use Psr\Log\LoggerInterface;
use App\Repository\ErrorRepository;
use App\Repository\MailRepository;

/**
 * @method UserGuid|null find($id, $lockMode = null, $lockVersion = null)
 * @method UserGuid|null findOneBy(array $criteria, array $orderBy = null)
 * @method UserGuid[]    findAll()
 * @method UserGuid[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class UserGuidRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    protected $_userLoginRepository;
    private $_errorRepository;
    private $_mailRepository;
    const _dynamicsurl = 'https://gs1dk.crm4.dynamics.com';
    const _bcurl = 'http://nav.gs1.dk:7447/GS1NAV-WEBSHOPTEST/WS/GS1';
    const _wsdl = 'https://shop.gs1.dk/wsdl';
    private $_wsdl;
    private $_bcurl;
    private $_dynamicsurl;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        UserLoginRepository $userLoginRepository,
        LoggerInterface $logger,
        ErrorRepository $errorRepository,
        MailRepository $mailRepository
    )
    {
        parent::__construct($registry, UserGuid::class);
        $this->_em = $em;
        $this->_userLoginRepository = $userLoginRepository;
        $this->_logger = $logger;
        $this->_errorRepository = $errorRepository;
        $this->_mailRepository = $mailRepository;
        $this->_wsdl = $_SERVER['BC_WSDL'];
        $this->_bcurl = $_SERVER['BC_URL'];
        $this->_dynamicsurl = $_SERVER['CRM_URL'];
    }

    public function transform(UserGuid $user) {
        return [
            'id'                => (int) $user->getId(),
            'guid'              => (string) $user->getGuid(),
            'userloginid'       => (int) $user->getUserLoginId(),
            'bcuserid'          => (string) $user->getBcUserId(),
            'bccompanyid'       => (string) $user->getBcCompanyId(),
            'bccontactid'       => (string) $user->getBcContactId(),
            'admin'             => (int) $user->getAdmin(),
            'dev'               => (int) $user->getDev(),
            'active'            => (int) $user->getActive(),
        ];
    }

    public function getDistinctCompanies($active = 1){
        $usersArray = array();
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.Active = :val')
            // ->andWhere('u.bc_company_id > 105461')
            ->setParameter('val', $active)
            ->groupBy('u.bc_company_id')
            ->getQuery()
            ->getResult();

        foreach ($guiduser as $user) {
            $usersArray[] = $this->transform($user);
        }

        return $usersArray;
    }

    public function isCompanyInDb($bccompanyid){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_company_id = :val')
            ->setParameter('val', $bccompanyid)
            ->getQuery()
            ->getResult();

        $inDB = false;
        foreach ($guiduser as $user) {
            $inDB = true;
        }

        return $inDB;
    }

    public function getGuidUser($userid, $companyid, $active = 1){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.user_login_id = :val')
            ->andWhere('u.bc_company_id = :val2')
            ->andWhere('u.Active = :val3')
            ->setParameter('val', $userid)
            ->setParameter('val2', $companyid)
            ->setParameter('val3', $active)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            return $this->transform($guiduser);
        }
        return 0;
    }

    public function getGuidUserByKId($userid){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $userid)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $user = $this->transform($guiduser);
            $userlogin = $this->_userLoginRepository->getFormatedUser($user['userloginid']);
            return $userlogin['email'];
        }
        return 0;
    }

    public function getGuidTokenByKId($contactPerson){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $contactPerson->No)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $user = $this->transform($guiduser);
            $userlogin = $this->_userLoginRepository->getFormatedUser($user['userloginid']);
            return $userlogin['token'];
        }
        return '';
    }

    public function isUserAdmin($contactPerson){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $contactPerson->No)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $user = $this->transform($guiduser);
            return $user['admin'];
        }
        return 0;
    }

    public function isUserActive($contactPerson){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $contactPerson->No)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $user = $this->transform($guiduser);
            return (bool)$user['active'];
        }
        return false;
    }

    public function isUserInDb($contactPerson){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $contactPerson->No)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            return true;
        }
        return false;
    }

    public function deactivateUser($bcuserid){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $bcuserid)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $guiduser->setActive(0)
            ->setAdmin(0);
            $this->_em->persist($guiduser);
            $this->_em->flush();
            return true;
        }
        return false;
    }

    public function activateUser($bcuserid){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $bcuserid)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $guiduser->setActive(1);
            $this->_em->persist($guiduser);
            $this->_em->flush();
            return true;
        }
        return false;
    }

    public function setUserAdmin($bcuserid, $admin){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->setParameter('val', $bcuserid)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            $guiduser->setAdmin((int)$admin);
            $this->_em->persist($guiduser);
            $this->_em->flush();
            return true;
        }
        return false;
    }




    public function getGuidUserCheck($userid, $companyid, $active = 1){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.user_login_id = :val')
            ->andWhere('u.bc_company_id = :val2')
            ->andWhere('u.Active = :val3')
            ->setParameter('val', $userid)
            ->setParameter('val2', $companyid)
            ->setParameter('val3', $active)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            return 1;
        }
        return 0;
    }

    public function getGuidUserByContactId($contactid, $active = 1){
        $guiduser = $this->createQueryBuilder('u')
            ->andWhere('u.bc_user_id = :val')
            ->andWhere('u.Active = :val2')
            ->setParameter('val', $contactid)
            ->setParameter('val2', $active)
            ->getQuery()
            ->getOneOrNullResult();
        if(getType($guiduser) == 'object'){
            return $this->transform($guiduser);
        }
        return 0;
    }


    public function createExistingUsers($companies){

        foreach ($companies as $key => $value) {
            # code...
        }


    }
    public function createBcAndCrmUsersAlreadyInBc($bcCompanyId, $contactmail){
        $loginUser = $this->_userLoginRepository->createLoginUser($contactmail['email']);
        $guid = str_replace('}', '', str_replace('{', '', $contactmail['guid']));
        $user = $this->createUserGuidAlreadyInBc($loginUser, $guid, $bcCompanyId, $contactmail);

        return $user;
    }

    public function createBcAndCrmUsers($bcCompanyId, $contacts, $admin = 0, $lang = 'da'){
        $created = array();
        $count = 1;
        $icontactno = true;
        foreach ($contacts as $key => $contactemail) {
            $alreadyInSystem = $this->_userLoginRepository->checkIfUserExists($contactemail['email']);
            $loginUser = $this->_userLoginRepository->createLoginUser($contactemail['email']);
            $guiduserCheck = $this->getGuidUserCheck($loginUser['id'], $bcCompanyId);
            if($guiduserCheck === 1){
                continue;
            }
            $guid = $this->generateGuid();
            if($count == 1){
                // If $admin set only on first
                $users = $this->createUserGuid($loginUser, $guid, $bcCompanyId, $contactemail, $admin, $alreadyInSystem, $lang);
                if($admin === 1 && isset($users['bcuserid']) === true){
                    $icontactno = array($bcCompanyId, $users['bcuserid']);
                }
            }
            else{
                $users = $this->createUserGuid($loginUser, $guid, $bcCompanyId, $contactemail, 0, $alreadyInSystem, $lang);
            }
            array_push($created, array($users, $guid, $loginUser));
            $count ++;
        }

        $error = true;
        foreach ($created as $key => $value) {
            if($value[0] === false){
                // return $value;
                $error = false;
            }
        }

        if($error === false){
            return $error;
        }


        return $icontactno;
    }


    public function updateBcAndCrmUsers($userdata, $userloginid){
        $uniqueUsers = $this->getAllGuiUsersByLoginId($userloginid);

        foreach ($uniqueUsers as $key => $user) {
            $return = $this->UpdateUserInBc($user, $userdata);
            $return = $this->UpdateUserInCrm($user['guid'], $userdata->permissions);
            // return $return;
        }

        return true;
    }

    public function getAllGuiUsersByLoginId($loginid, $active = 1){
        $usersArray = array();

        $users = $this->createQueryBuilder('u')
            ->andWhere('u.user_login_id = :val')
            ->andWhere('u.Active = :val2')
            ->setParameter('val', $loginid)
            ->setParameter('val2', $active)
            ->getQuery()
            ->getResult()
        ;

        foreach ($users as $user) {
            $usersArray[] = $this->transform($user);
        }

        return $usersArray;
    }

    public function getCompanyList($user){
        $companylist = array();
        $guids = $this->getAllGuiUsersByLoginId($user['id']);

        foreach ($guids as $key => $value) {
            $userStillActive = $this->checkIfUserInCompany($value['bcuserid'], $value['bccontactid']);
            if($userStillActive == false){
                continue;
            }

            $company = $this->getBcCompanyInfo($value['bccompanyid']);
            $array = array(
                'id'           => $value['bccompanyid'],
                'contactid'    => $company->iContactCompNo,
                'name'         => $company->iName,
                'cvr'          => $company->iVAT_Registration_Noa46,
            );
            array_push($companylist, $array);
        }

        return $companylist;
    }

    public function createUserGuidAlreadyInBc($loginUser, $guid, $bcCompanyId, $userdata){
        $bccompanyinfo = $this->getBcCompanyInfo($bcCompanyId);

        $user = new UserGuid;
        $user->setUserLoginId($loginUser['id']);
        $user->setGuid($guid);
        $user->setBcCompanyId($bcCompanyId);
        $user->setBcUserId($userdata['no']);
        $user->setBcContactId($bccompanyinfo->iContactCompNo);
        $user->setAdmin(1);
        $user->setDev(0);
        $user->setActive(1);
        $this->_em->persist($user);
        $this->_em->flush();

        $this->_mailRepository->newUserMailFirst($loginUser, $userdata['name'], $bccompanyinfo->iName, 'da');

        return true;
    }

    public function createUserGuid($loginUser, $guid, $bcCompanyId, $userdata, $admin = 0, $newuser = 0, $lang = 'da'){
        $check = $this->getGuidUser($loginUser['id'], $bcCompanyId);
        if($check !== 0){
            return $check;
        }
        $bccompanyinfo = $this->getBcCompanyInfo($bcCompanyId);
        $bcuser = $this->checkForUserInBc($loginUser['email'], $guid, $bccompanyinfo->iContactCompNo);
        if(isset($bcuser->ContactPersons) === true){
            return false;
        }

        $bcUserId = $this->createUserInBc($loginUser['email'], $guid, $bcCompanyId, $userdata['name']);
        $crmusercreated = $this->createUserInCrm($loginUser['email'], $guid);

        if($bcUserId === false || $crmusercreated === false){
            return false;
        }

        $user = new UserGuid;
        $user->setUserLoginId($loginUser['id']);
        $user->setGuid($guid);
        $user->setBcCompanyId($bcCompanyId);
        $user->setBcUserId($bcUserId);
        $user->setBcContactId($bccompanyinfo->iContactCompNo);
        $user->setAdmin($admin);
        $user->setDev(0);
        $user->setActive(1);
        $this->_em->persist($user);
        $this->_em->flush();

        if($newuser === 0){
            // Send new user email
            $this->_mailRepository->newUserMail($loginUser, $userdata['name'], $bccompanyinfo->iName, $lang);
        }
        else{
            // Send already user email
            $this->_mailRepository->recurringUserMail($loginUser, $userdata['name'], $bccompanyinfo->iName, $lang);
        }

        // return true;
        return $this->transform($user);
    }

    public function generateGuid(){
        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public function getBcCompanyInfo($bcCompanyId){
        $location = $this->_bcurl."/Codeunit/GS1WS";
        $wsdl = $this->_wsdl."/GS1WS.wsdl?12121222";

        $options = array(
            'location' => $location,
            'trace' => 1,
            'login' => $_SERVER['BC_USERNAME'], //"Nav_WebService",
            'password' => $_SERVER['BC_PASSWORD'], //"6GERDGAnnEhR",
        );

        $soapClient = new \SoapClient($wsdl, $options);
        $GetCompanyInfoData = array(
            "custNo" => $bcCompanyId,
            "iContactNo" => "",
            "iName" => "",
            "iName2" => "",
            "iAddress" => "",
            "iAddress_2" => "",
            "iPost_Code" => "",
            "iCity" => "",
            "iCounty" => "",
            "iCountrya47Region_Code" => "",
            "iVAT_Registration_Noa46" => "",
            "iGLN" => "",
            "iMail" => "",
            "iPhone" => "",
            "iContactName" => "",
            "iTurnover"  => 0,
            "iOIOUBL"  => "",
            "iBranch_Noa46" => "",
            "iContactCompNo" => "",
          );

        try {


          $result = $soapClient->GetCompanyInfo($GetCompanyInfoData);
          return $result;
          return false;
        } catch (Exception $e) {
            $this->_logger->error('** Sunland ** getBcCompanyInfo() Returned error: ' . $e);
            $this->_errorRepository->sendErrorEmail(
                'getBcCompanyInfo()',
                $bcCompanyId,
                json_encode($GetCompanyInfoData),
                json_encode($e)
            );
          return $e;
        }
    }

    public function checkIfUserInCompany($bcuserid, $bcContactId){
        $location = $this->_bcurl."/Page/ContactPersonsWS";
        $wsdl = $this->_wsdl."/ContactPersons.wsdl?121";

        $options = array(
            'location' => $location,
            'trace' => 1,
            'login' => $_SERVER['BC_USERNAME'], //"Nav_WebService",
            'password' => $_SERVER['BC_PASSWORD'], //"6GERDGAnnEhR",
        );

        $soapClient = new \SoapClient($wsdl, $options);

        $ContactPersons_Fields = array(
            array(
              "Field" => "No",
              "Criteria" => $bcuserid,
            ),
            array(
              "Field" => "Company_No",
              "Criteria" => $bcContactId,
            ),
          );

        try {

          // $result = $soapClient->Read($ReadData);
          $result = $soapClient->ReadMultiple(array("filter" => $ContactPersons_Fields, "setSize" => 100));
          if(isset($result->ReadMultiple_Result->ContactPersonsWS) === true){
            return true;
          }
          else{
            return false;
          }
          // return $result->ReadMultiple_Result;
          // var_dump($result);
          return false;
        } catch (Exception $e) {
            $this->_logger->error('** Sunland ** checkForUserInBc() Returned error: ' . $e);
            $this->_errorRepository->sendErrorEmail(
                'checkForUserInBc()',
                json_encode(array($email, $guid, $bcContactId)),
                json_encode($ContactPersons_Fields),
                json_encode($e)
            );
          return $e;
        }

    }

    public function checkForUserInBc($email, $guid, $bcContactId){
        $location = $this->_bcurl."/Page/ContactPersonsWS";
        $wsdl = $this->_wsdl."/ContactPersons.wsdl?121";

        $options = array(
            'location' => $location,
            'trace' => 1,
            'login' => $_SERVER['BC_USERNAME'], //"Nav_WebService",
            'password' => $_SERVER['BC_PASSWORD'], //"6GERDGAnnEhR",
        );

        $soapClient = new \SoapClient($wsdl, $options);

        $ContactPersons_Fields = array(
            array(
              "Field" => "E_Mail",
              "Criteria" => $email,
            ),
            array(
              "Field" => "Company_No",
              "Criteria" => $bcContactId,
            ),
          );

        try {

          // $result = $soapClient->Read($ReadData);
          $result = $soapClient->ReadMultiple(array("filter" => $ContactPersons_Fields, "setSize" => 100));
          return $result->ReadMultiple_Result;
          // var_dump($result);
          return false;
        } catch (Exception $e) {
            $this->_logger->error('** Sunland ** checkForUserInBc() Returned error: ' . $e);
            $this->_errorRepository->sendErrorEmail(
                'checkForUserInBc()',
                json_encode(array($email, $guid, $bcContactId)),
                json_encode($ContactPersons_Fields),
                json_encode($e)
            );
          return $e;
        }

    }

    public function createUserInBc($email, $guid, $bcCompanyId, $name = ''){
        $location = $this->_bcurl."/Codeunit/GS1WS";
        $wsdl = $this->_wsdl."/GS1WS.wsdl?12121";

        $options = array(
            'location' => $location,
            'trace' => 1,
            'login' => $_SERVER['BC_USERNAME'], //"Nav_WebService",
            'password' => $_SERVER['BC_PASSWORD'], //"6GERDGAnnEhR",
        );

        $soapClient = new \SoapClient($wsdl, $options);

        $createUser = array(
            "iCustomerNo" => $bcCompanyId,
            "iName" => $name,
            "iJobTitle" => '',
            "iMobilePhone" => '',
            "iExternal_ID" => '',
            "iMail" => $email,
            "iPhone_Noa46" => '',
            "contactGUID" => $guid,
          );

        try {


          // $result = $soapClient->Read($ReadData);
          // var_dump($result);
          $result = $soapClient->CreateContact($createUser);
          return $result->return_value;
          // var_dump($result);
          return false;
        } catch (Exception $e) {
            $this->_logger->error('** Sunland ** createUserInBc() Returned error: ' . $e);
            $this->_errorRepository->sendErrorEmail(
                'createUserInBc()',
                json_encode(array($email, $guid, $bcCompanyId)),
                json_encode($createUser),
                json_encode($e)
            );
            return false;
        }


        return false;

    }

    public function getBcContactInfo($user){
        $guids = $this->getAllGuiUsersByLoginId($user['id']);

        foreach ($guids as $key => $value) {
            return $this->getBcUserById($value['bcuserid']);
        }
    }

    public function getCrmContactInfo($user){
        $guids = $this->getAllGuiUsersByLoginId($user['id']);

        foreach ($guids as $key => $value) {
            return $this->getUserInCrm($value['guid']);
        }
    }

    public function getBcUserById($bcuserid){
        $location = $this->_bcurl."/Page/ContactPersonsWS";
        $wsdl = $this->_wsdl."/ContactPersons.wsdl?121";

        $options = array(
            'location' => $location,
            'trace' => 1,
            'login' => $_SERVER['BC_USERNAME'], //"Nav_WebService",
            'password' => $_SERVER['BC_PASSWORD'], //"6GERDGAnnEhR",
        );

        $soapClient = new \SoapClient($wsdl, $options);

        $ContactPersons_Fields = array(
            array(
              "Field" => "No",
              "Criteria" => $bcuserid,
            ),
        );

        try {
          $result = $soapClient->ReadMultiple(array("filter" => $ContactPersons_Fields, "setSize" => 100));
          if(isset($result->ReadMultiple_Result->ContactPersonsWS) === true){
            return $result->ReadMultiple_Result->ContactPersonsWS;
          }
          // return $result;
          return false;
        } catch (\Throwable $e) {
            $this->_logger->error('** Sunland ** getBcUserById() Returned error: ' . $e);
            $this->_errorRepository->sendErrorEmail(
                'getBcUserById()',
                json_encode(array($bcuserid)),
                json_encode($ContactPersons_Fields),
                json_encode($e)
            );
            return $e;
        }

        return false;
    }

    public function UpdateUserInBc($user, $userdata){
        $location = $this->_bcurl."/Codeunit/GS1WS";
        $wsdl = $this->_wsdl."/GS1WS.wsdl?121";

        $bcuser = $this->getBcUserById($user['bcuserid']);

        if($bcuser === false){
            return false;
        }

        if(isset($bcuser->No) !== true ){
            $this->_logger->error('** Sunland ** UpdateUserInBc('.$user['bcuserid'].') Returned error: No on user not set.');
            $this->_errorRepository->sendErrorEmail(
                'UpdateUserInBc('.$user['bcuserid'].')',
                json_encode(array($user, $userdata)),
                json_encode($user),
                json_encode($bcuser)
            );
            return false;
        }

        $update = array(
            "contactNo" => $bcuser->No,
            "iName" => (isset($userdata->name) === true ? $userdata->name : ''),
            "iJobTitle" => (isset($bcuser->Job_Title) === true ? $bcuser->Job_Title : ''),
            "iMobilePhone" => (isset($userdata->phone) === true ? $userdata->phone : ''),
            "iExternal_ID" => '',
            "iMail" => (isset($bcuser->E_Mail) === true ? $bcuser->E_Mail : ''),
            "iPhone_Noa46" => (isset($bcuser->Phone_No) === true ? $bcuser->Phone_No : ''),
        );

        $options = array(
            'location' => $location,
            'trace' => 1,
            'login' => $_SERVER['BC_USERNAME'], //"Nav_WebService",
            'password' => $_SERVER['BC_PASSWORD'], //"6GERDGAnnEhR",
        );

        $soapClient = new \SoapClient($wsdl, $options);

        try {

          // $result = $soapClient->Read($ReadData);
          // var_dump($result);
          $result = $soapClient->UpdateContact($update);
          return $result->return_value;
          // var_dump($result);
          return false;
        } catch (\Throwable $e) {
            $this->_logger->error('** Sunland ** UpdateUserInBc() Returned error: ' . $e);
            $this->_errorRepository->sendErrorEmail(
                'UpdateUserInBc()',
                json_encode(array($user, $userdata)),
                json_encode($update),
                json_encode($e)
            );
            return $e;
            // return $e;
        }


        return false;
    }

    public function createUserInCrm($email, $guid){
        $oathToken = $this->getOathKey();
        $header = array("Authorization: Bearer ".$oathToken, "Content-Type: application/json");
        $url = $this->_dynamicsurl.'/api/data/v9.1/contacts';
        $content = array(
            'emailaddress1'                 => $email,
            'contactid'                     => $guid,
            // 'consit_pstregen'               => '',
            // 'consit_gs1verden'              => '',
            // 'consit_gs1tradeservice'        => '',
            // 'consit_gs1isundhedssektoren'   => '',
        );
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => 'POST',
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POSTFIELDS => json_encode($content)
        ));
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
          $info = curl_getinfo($curl);
          if((int)$info['http_code'] === 204){
            return true;
          }
        }
        curl_close($curl);
        $array = array(
            "consit_pstregen" => array('digital' => false, 'trykt' => true),
            "consit_gs1tradeservice" => false,
            "consit_gs1verden" => false,
            "consit_gs1isundhedssektoren" => false,
        );
        $json = json_encode($array);
        $obj = json_decode($json);
        $this->updateUserInCrm($guid, $array);
        return false;

    }

    public function updateUserInCrm($guid, $permission){

        $crm1 = $this->updateConsitPstregen($guid, $permission->consit_pstregen);
        $crm2 = $this->updateConsitVerden($guid, $permission->consit_gs1verden);
        $crm3 = $this->updateConsitTradeservice($guid, $permission->consit_gs1tradeservice);
        $crm4 = $this->updateConsitgs1sundhed($guid, $permission->consit_gs1isundhedssektoren);
        return array($crm1, $crm2, $crm3, $crm4);
    }

    private function updateConsitgs1sundhed($guid, $value){
        $oathToken = $this->getOathKey();
        $header = array("Authorization: Bearer ".$oathToken, "Content-Type: application/json");
        $url = $this->_dynamicsurl.'/api/data/v9.0/contacts('.$guid.')/consit_gs1isundhedssektoren';
        $content = array(
            'value' => (bool)$value,
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => 'PUT',
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POSTFIELDS => json_encode($content)
        ));
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
          $info = curl_getinfo($curl);
          if((int)$info['http_code'] === 204){
            return true;
          }
        }
        curl_close($curl);

        return json_decode($response);
    }

    private function updateConsitTradeservice($guid, $value){
        $oathToken = $this->getOathKey();
        $header = array("Authorization: Bearer ".$oathToken, "Content-Type: application/json");
        $url = $this->_dynamicsurl.'/api/data/v9.0/contacts('.$guid.')/consit_gs1tradeservice';
        $content = array(
            'value' => (bool)$value,
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => 'PUT',
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POSTFIELDS => json_encode($content)
        ));
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
          $info = curl_getinfo($curl);
          if((int)$info['http_code'] === 204){
            return true;
          }
        }
        curl_close($curl);

        return $response;
    }

    private function updateConsitVerden($guid, $value){
        $oathToken = $this->getOathKey();
        $header = array("Authorization: Bearer ".$oathToken, "Content-Type: application/json");
        $url = $this->_dynamicsurl.'/api/data/v9.0/contacts('.$guid.')/consit_gs1verden';
        $content = array(
            'value' => (bool)$value,
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => 'PUT',
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POSTFIELDS => json_encode($content)
        ));
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
          $info = curl_getinfo($curl);
          if((int)$info['http_code'] === 204){
            return true;
          }
        }
        curl_close($curl);

        return $response;
    }

    private function updateConsitPstregen($guid, $value){
        $pstregen = 358380002;

        if($value->digital === true){
            $pstregen = 358380002;
        }
        else{
            $pstregen = 358380001;
        }

        // 358380002,358380001,358380000
        $oathToken = $this->getOathKey();
        $header = array("Authorization: Bearer ".$oathToken, "Content-Type: application/json");
        $url = $this->_dynamicsurl.'/api/data/v9.0/contacts('.$guid.')/consit_pstregen';
        $content = array(
            'value' => (int)$pstregen,
        );

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => 'PUT',
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
            CURLOPT_POSTFIELDS => json_encode($content)
        ));
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
          $info = curl_getinfo($curl);
          if((int)$info['http_code'] === 204){
            return true;
          }
        }
        curl_close($curl);

        return $response;
    }




    public function getUserInCrm($guid, $test = ''){
        $oathToken = $this->getOathKey();
        $header = array("Authorization: Bearer ".$oathToken);
        $url = $this->_dynamicsurl.'/api/data/v9.1/contacts('.str_replace('}', '', str_replace('{', '', $guid)).')?$select=consit_pstregen,consit_gs1verden,consit_gs1tradeservice,consit_gs1isundhedssektoren';
        // $grant_type = "client_credentials";
        // $content = "grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret&resource=$resource";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL             => $url,
            CURLOPT_CUSTOMREQUEST   => 'GET',
            CURLOPT_HTTPHEADER      => $header,
            CURLOPT_SSL_VERIFYPEER  => false,
            CURLOPT_RETURNTRANSFER  => true,
        ));
        $response = curl_exec($curl);
        if (!curl_errno($curl)) {
          $info = curl_getinfo($curl);
          if((int)$info['http_code'] === 204){
            return json_decode($response);
          }
        }

        curl_close($curl);
        // return array($info, $oathToken, $response);
        return json_decode($response);


        return false;
    }

    public function getOathKey(){

        $client_id = $_SERVER['client_id'];
        $client_secret = $_SERVER['client_secret'];
        $resource = $this->_dynamicsurl;
        $header = array("Content-Type: application/x-www-form-urlencoded");
        $grant_type = "client_credentials";
        $content = "grant_type=$grant_type&client_id=$client_id&client_secret=$client_secret&resource=$resource";
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $_SERVER['CRM_TOKEN'],
            CURLOPT_HTTPHEADER => $header,
            CURLOPT_SSL_VERIFYPEER => false,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_POST => true,
            CURLOPT_POSTFIELDS => $content
        ));
        $response = curl_exec($curl);
        curl_close($curl);

        return json_decode($response)->access_token;
    }


}
