<?php


namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;

class AuthRepository {

    protected $_em;
    protected $_logger;
    private   $_cipher_algo;
    private   $_encrypt_key;
    private   $_iv;

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger
    )
    {
        $this->_em = $em;
        $this->_logger = $logger;
        $this->_cipher_algo = openssl_get_cipher_methods()[0];
        $this->_encrypt_key = '2846djabwaY#%"/Ehbdiskisl';
        $this->_iv = base64_decode("IlY9Ia3OB+6Z3Q53yR49\/w==");
    }

    public function decodePasswordToken($token) {

        $decodedToken = base64_decode($token);
        $decodedToken = openssl_decrypt($decodedToken, $this->_cipher_algo, $this->_encrypt_key, OPENSSL_RAW_DATA, $this->_iv);
        $decodedToken = explode("-", $decodedToken);

        $userId = $decodedToken[0];
        $timeStamp = $decodedToken[1];

        $expiredTime = 36000; //10h
        try {
            if(time() - $timeStamp > $expiredTime) {
                return false;
            }
        } catch(\Exception $e) {
            return false;
        }

        return $userId;
    }

    public function resetPassword($userId, $password) {

        $query = "SELECT * FROM user WHERE Active = 1 AND id = :userId";
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute(["userId" => $userId]);

        $user = $stmt->fetch();

        if(!$user) {
            return "User doesn't exist";
        }

        $salt = $this->generateRandomString();
        $hashedPassword = $this->generatePasswordHash($password, $salt);

        $query = "UPDATE user SET hashedPassword = :hashedPassword, RandomKey = :salt WHERE id = :userId";
        $stmt = $conn->prepare($query);

        return $stmt->execute(["hashedPassword" => $hashedPassword, "salt" => $salt, "userId" => $userId]);
    }

    public function sendPasswordRecoveryEmail($email) {
        $query = "SELECT * FROM user WHERE Active = 1 AND Mailer = :email";
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute(["email" => $email]);

        $user = $stmt->fetch();

        if(!$user) {
            return "Invalid email";
        }

        $timeStamp = time();
        $userId = $user["Id"];
        $token = openssl_encrypt($userId . "-" . $timeStamp, $this->_cipher_algo, $this->_encrypt_key, OPENSSL_RAW_DATA, $this->_iv);
        // $hashed = openssl_encrypt($loginname . '-' . $timeStamp, $cipherMethod, $salt, OPENSSL_RAW_DATA, $iv);

        return base64_encode($token);

    }

    public function checkCookie($clientIp, $cookie) {

        $conn = $this->_em->getConnection();

        $query = "SELECT id, IP FROM session WHERE  UID = :uid";
        $stmt = $conn->prepare($query);
        $stmt->execute(['uid' => $cookie]);

        $session = $stmt->fetch();

        if(!$session){
            return false;
        }

        if($session["IP"] !== $clientIp) {
            return false;
        }

        $query = "SELECT id FROM login WHERE Active = 1 AND SessionId = :sessionId";
        $stmt = $conn->prepare($query);
        $stmt->execute([":sessionId" => $session["id"]]);

        $login = $stmt->fetch();

        if(!$login) {
            return false;
        }
        return true;
    }

    public function createSession($loginname) {

        $uid = time();
        $time = new \DateTime();

        $conn = $this->_em->getConnection();

        $query = "SELECT id FROM user WHERE loginname = :loginname";
        $stmt = $conn->prepare($query);
        $stmt->execute(["loginname" => $loginname]);

        $user = $stmt->fetch();
        if(!$user) {
            return false;
        }

        $userId = $user['id'];

        $query = "INSERT INTO Session SET CreateDate=:createdate, IP=:ip, UID=:uid, Language=:lang, Agent=:agent";

        $stmt = $conn->prepare($query);
        $stmt->execute(
            array(
                "createdate"    => $time->format('Y-m-d H:i:s'),
                "ip"            => $_SERVER["REMOTE_ADDR"],
                "uid"           => $uid,
                "lang"          => $_SERVER["HTTP_ACCEPT_LANGUAGE"],
                "agent"         => $_SERVER["HTTP_USER_AGENT"]
            )
        );

        $query = "SELECT * FROM Session WHERE uid = :uid";
        $stmt = $conn->prepare($query);
        $stmt->execute(["uid" => $uid]);

        $Session = $stmt->fetch();

        if($Session) {
            $time = new \DateTime();
            $formatedTime = $time->format('Y-m-d H:i:s');
            $query =  "INSERT INTO Login  (LoginDate, AccessDate, SessionId, UserId, AcceptedMimeTypes, Active)
                                VALUES (:LoginDate, :AccessDate , :SessionId, :UserId, :AcceptedMimeTypes, :Active)";
            $stmt = $conn->prepare($query);
            $stmt->execute(
                array(
                    "LoginDate"             => $formatedTime,
                    "AccessDate"            => $formatedTime,
                    "SessionId"             => $Session["Id"],
                    "UserId"                => $userId,
                    "AcceptedMimeTypes"     => $_SERVER["HTTP_ACCEPT"],
                    "Active"                => 1
                )
            );
        }
        return $uid;
    }

    public function getUserByLoginnameAndPassword($loginname, $password) {
        $query = "SELECT * FROM user
                    WHERE Active = 1 AND loginname = :loginname";

        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute(["loginname" => $loginname]);
        $user = $stmt->fetch();
        if(empty($user)) {
            return "User not found";
        }

        $hashedPassword = $user["HashedPassword"];
        $salt = $user["RandomKey"];

        if($this->checkPassword($hashedPassword, $password, $salt)) {
            return $user;
        }

        return "Incorrect password";
    }

    public function generateRandomString($length = 10) {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    private function checkPassword($_hasword, $_password, $_salt){
        $verify = hash('sha512', $_salt.$_password);
        $verify = password_verify($verify, $_hasword);

        return $verify;
    }

    private function generatePasswordHash($_password, $_salt){
        $password = hash('sha512', $_salt.$_password);
        $password = password_hash($password, PASSWORD_DEFAULT);

        return $password;
    }

    // public function getLoginnameByToken($token) {
    //     $decodedToken = base64_decode($token);
    //     $decodedToken = openssl_decrypt($decodedToken, $this->_cipher_algo, $this->_encrypt_key, OPENSSL_RAW_DATA, $this->_iv);
    //     $decodedToken = explode("-", $decodedToken);

    //     return $decodedToken[0];
    // }

    public function hashedAllPassword() {

        $query = "SELECT * FROM user WHERE HashedPassword = ''";
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $users = $stmt->fetchAll();

        foreach($users as $user) {

            $salt = $this->generateRandomString(10);
            $hashedPassword = $this->generatePasswordHash($user["Password"], $salt);

            $query = "UPDATE user SET password = :hashedPassword, HashedPassword = :hashedPassword, RandomKey = :salt
                     WHERE id = :userId";
            $stmt = $conn->prepare($query);
            $stmt->execute(["hashedPassword" => $hashedPassword, "salt" => $salt, "userId" => $user["Id"]]);
        }
    }
}
