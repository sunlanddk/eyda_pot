<?php

namespace App\Repository\Article;

use App\Entity\Article\Article;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Article::class);
        $this->_em = $em;
    }

    public function transform(Article $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
            'articletypeid'     => (int) $item->getArticletypeid(),
            'number'      		=> (string) $item->getNumber(),
            'description'      	=> (string) $item->getDescription(),
            'suppliercompanyid' => (string) $item->getSuppliercompanyid(),
        ];
    }


    public function createArticle($gtin){

    	$variant = new Article;
        $variant->setCreatedate(new \DateTime());
        $variant->setCreateuserid(1);
		$variant->setModifydate(new \DateTime());
        $variant->setModifyuserid(1);
        $variant->setActive(1);
        $variant->setComment((string)$gtin);
        $variant->setArticletypeid(1);
        $variant->setNumber((string)$gtin);
        $variant->setDescription((string)$gtin);
        $variant->setSuppliercompanyid(787);

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }

}
