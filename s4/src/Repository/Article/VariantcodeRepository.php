<?php

namespace App\Repository\Article;

use App\Entity\Article\Variantcode;
use App\Repository\HelperFunctions\SqlhelperRepo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Variantcode|null find($id, $lockMode = null, $lockVersion = null)
 * @method Variantcode|null findOneBy(array $criteria, array $orderBy = null)
 * @method Variantcode[]    findAll()
 * @method Variantcode[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class VariantcodeRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_sqlhelperRepo;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        SqlhelperRepo $sqlhelperRepo
    )
    {
        parent::__construct($registry, Variantcode::class);
        $this->_em = $em;
        $this->_sqlhelperRepo = $sqlhelperRepo;
    }

    public function transform(Variantcode $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
            'type'      		=> (string) $item->getType(),
            'articleid'      	=> (int) $item->getArticleid(),
            'articlecolorid'    => (int) $item->getArticlecolorid(),
            'articlesizeid'     => (int) $item->getArticlesizeid(),
            'sku'               => (string) $item->getSku(),
            'variantcode'      	=> (string) $item->getVariantcode(),
            'ownercompanyid'    => (string) $item->getOwnercompanyid(),
            'pickcontainerid'   => (string) $item->getPickcontainerid(),
            'presale'           => (int) $item->getPresale(),
        ];
    }

    public function transformSimple(Variantcode $item) {
        return [
            'id'                => (int) $item->getId(),
            'articleid'         => (int) $item->getArticleid(),
            'articlecolorid'    => (int) $item->getArticlecolorid(),
            'articlesizeid'     => (int) $item->getArticlesizeid(),
            'sku'               => (string) $item->getSku(),
            'variantcode'       => (string) $item->getVariantcode(),
            'shopifyvariantid'  => (string) $item->getShopifyvariantid(),
            'shopifyproductid'  => (string) $item->getShopifyproductid(),
            'inventoryitemid'   => (string) $item->getInventoryitemid(),
            'pickcontainerid'   => (string) $item->getPickcontainerid(),
            'presale'           => (int) $item->getPresale(),
        ];
    }

    public function transformSimpleIds(Variantcode $item, $shopifyid, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId, $purchasePrice = 0) {
        $sku = ((string)$item->getSku() == '' ? $this->_sqlhelperRepo->getNextSku() : $item->getSku());
        $item->setSku($sku);
        $this->_em->persist($item);
        $this->_em->flush();

        return [
            'id'                => (int) $item->getId(),
            'articleid'         => (int) $item->getArticleid(),
            'articlecolorid'    => (int) $item->getArticlecolorid(),
            'articlesizeid'     => (int) $item->getArticlesizeid(),
            'sku'               => (string) $item->getSku(),
            'variantcode'       => (string) $item->getVariantcode(),
            'shopifyid'         => (int)    $shopifyid,
            'shopifyvariantid'  => (string) $shopifyVariantId,
            'shopifyproductid'  => (string) $shopifyProductId,
            'inventoryitemid'   => (string) $shopifyInventoryId,
            'pickcontainerid'   => (string) $item->getPickcontainerid(),
            'cost'              => $purchasePrice,
        ];
    }

    public function transformIds(Variantcode $item, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
        return [
            'id'                => (int) $item->getId(),
            'articleid'         => (int) $item->getArticleid(),
            'articlecolorid'    => (int) $item->getArticlecolorid(),
            'articlesizeid'     => (int) $item->getArticlesizeid(),
            'sku'               => (string) $item->getSku(),
            'variantcode'       => (string) $item->getVariantcode(),
            'pickcontainerid'   => (string) $item->getPickcontainerid(),
            'shopifyvariantid'  => (string) $shopifyVariantId,
            'shopifyproductid'  => (string) $shopifyProductId,
            'inventoryitemid'   => (string) $shopifyInventoryId,
        ];
    }

    public function findGtinByString($gtin){
        $variant = $this->findOneBy(array('variantcode' => $gtin));
        if($variant !== null){
            return $this->transformSimple($variant);
        }

        return false;

    }

    public function getVariantCodeBySku($gtin){
        $variant = $this->findOneBy(array('sku' => $gtin, 'active' => 1));
        if($variant !== null){
            return $this->transformSimple($variant);
        }

        return false;

    }

    public function getCollectionmemberBySku($gtin){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT cm.*
                FROM (variantcode v, collectionmember cm)
                WHERE v.VariantCode='%s' AND v.Active=1 AND cm.Active=1 AND cm.ArticleId=v.ArticleId AND cm.ArticleColorId=v.ArticleColorId
            ",
            $gtin
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variants = $stmt->fetch();
        if($variants !== false){
            return $variants;
        }

        return array('Cancel' => 1);
    }


    public function getVariantCodeByGtin($gtin){
        $variant = $this->findOneBy(array('variantcode' => $gtin, 'active' => 1));
        if($variant !== null){
            return $this->transformSimple($variant);
        }

        return false;

    }


    public function getSingleVariantByArticleId($articleId, $shopId){
        // $variants = $this->findBy(
        //     array(
        //         'articleid' => $articleId,
        //         'active'   => 1,
        //     )
        // );

        // foreach ($variants as $key => $value) {
        //     if($value->getShopifyproductid() !== ''){
        //         return $this->transformSimple($value);
        //     }
        // }

        // return false;

        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT *
                FROM shopifyvariant
                WHERE ArticleId=%d AND ShopifyshopId=%d
            ",
            $articleId,
            $shopId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variants = $stmt->fetchAll();
        foreach ($variants as $key => $value) {
            if((string)$value['ShopifyProductId'] !== ''){
                $variant = $this->find($value['VariantCodeId']);
                return $this->transformIds($variant, $value['ShopifyProductId'], $value['ShopifyVariantId'], $value['InventoryItemId']);
            }
        }

        return false;
    }

    public function getAllVariantByArticleColor($articleId, $articleColorId, $shopId){

        // $variantsDb = $this->findBy(
        //     array(
        //         'articleid'         => $articleId,
        //         'articlecolorid'    => $articleColorId,
        //         'active'            => 1,
        //     )
        // );

        // $variants = [];
        // foreach ($variantsDb as $key => $value) {
        //     array_push($variants, $this->transformSimple($value));
        // }
        // return $variants;
        if((int)$shopId === 5){
            $conn = $this->_em->getConnection();
            $sql = sprintf("
                SELECT v.Id, s.Id as ShopifyId, s.ShopifyProductId, s.ShopifyVariantId, s.InventoryItemId, cmps.PurchasePrice
                FROM variantcode v
                LEFT JOIN shopifyvariant s ON s.VariantCodeId=v.Id AND s.ShopifyshopId=%d
                LEFT JOIN shopifyshops ss on ss.Id=s.ShopifyshopId
                LEFT JOIN collectionmember cm on cm.CollectionId=1 AND cm.Active=1 AND cm.ArticleId=v.ArticleId AND cm.ArticleColorId=v.ArticleColorId
                LEFT JOIN collectionmemberprice cmp on cmp.CurrencyId=ss.CurrencyId AND cmp.Active=1 AND cmp.CollectionMemberId=cm.Id
                LEFT JOIN collectionmemberpricesize cmps on cmps.Active=1 AND cmps.ArticleSizeId=v.ArticleSizeId AND cmps.CollectionMemberPriceId=cmp.Id
                WHERE v.ArticleId=%d AND v.ArticleColorId=%d AND v.Active=1 AND v.Disabled=0
                ",
                $shopId,
                $articleId,
                $articleColorId
            );
        }
        else{
            $conn = $this->_em->getConnection();
            $sql = sprintf("
                    SELECT v.Id, s.Id as ShopifyId, s.ShopifyProductId, s.ShopifyVariantId, s.InventoryItemId 
                    FROM variantcode v
                    LEFT JOIN shopifyvariant s ON s.VariantCodeId=v.Id AND s.ShopifyshopId=%d
                    WHERE v.ArticleId=%d AND v.ArticleColorId=%d AND v.Active=1 AND v.Disabled=0
                ",
                $shopId,
                $articleId,
                $articleColorId
            );
        }

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variantsDb = $stmt->fetchAll();

        $variants = [];
        foreach ($variantsDb as $key => $value) {
            $variant = $this->find($value['Id']);
            if((int)$shopId === 5){
                array_push($variants, $this->transformSimpleIds($variant, $value['ShopifyId'], $value['ShopifyProductId'], $value['ShopifyVariantId'], $value['InventoryItemId'], $value['PurchasePrice']));
            }
            else{
                array_push($variants, $this->transformSimpleIds($variant, $value['ShopifyId'], $value['ShopifyProductId'], $value['ShopifyVariantId'], $value['InventoryItemId']));
            }
        }
        return $variants;


    }

    public function createVariantcode($articleId, $gtin){

    	$variant = new Variantcode;
        $variant->setCreatedate(new \DateTime());
        $variant->setCreateuserid(1);
		$variant->setModifydate(new \DateTime());
        $variant->setModifyuserid(1);
        $variant->setActive(1);
        $variant->setType('case');
        $variant->setArticleid((int)$articleId);
        $variant->setVariantcode((string)$gtin);
        $variant->setShopifyvariantid('');
        $variant->setShopifyproductid('');
        $variant->setInventoryitemid('');
        $variant->setPickcontainerid(0);

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }

    public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
        $variant = $this->find($variantCodeId);
        $variant->setShopifyvariantid($shopifyVariantId);
        $variant->setShopifyproductid($shopifyProductId);
        $variant->setInventoryitemid($shopifyInventoryId);

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }

}
