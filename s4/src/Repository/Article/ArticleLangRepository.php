<?php

namespace App\Repository\Article;

use App\Entity\Article\ArticleLang;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method ArticleLang|null find($id, $lockMode = null, $lockVersion = null)
 * @method ArticleLang|null findOneBy(array $criteria, array $orderBy = null)
 * @method ArticleLang[]    findAll()
 * @method ArticleLang[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleLangRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    ) {
        parent::__construct($registry, ArticleLang::class);
        $this->_em = $em;
    }

    public function createArticleLangFromShop($articleId, $languageId = 4, $webdescription = '')
    {
        $lang = $this->findOneBy([
            'languageid' => $languageId,
            'articleid' => $articleId
        ]);
        if ($lang !== null) {
            return $this->updateArticleLangFromShop($lang->getId(), $articleId, $languageId, $webdescription);
        }

        $lang = new ArticleLang;
        $lang->setArticleid($articleId);
        $lang->setLanguageid($languageId);
        $lang->setOnwebshop(1);
        $lang->setDescription('');
        $lang->setWebDescription((string)$webdescription);

        $this->_em->persist($lang);
        $this->_em->flush();

        return $lang->getId();
    }

    public function updateArticleLangFromShop($id, $articleId, $languageId = 4, $webdescription = '')
    {
        if ($id > 0) {
            $lang = $this->find($id);
        } else {
            $lang = $this->findOneBy([
                'languageid' => $languageId,
                'articleid' => $articleId
            ]);
        }
        $lang->setWebDescription((string)$webdescription);

        $this->_em->persist($lang);
        $this->_em->flush();

        return $lang->getId();
    }

    public function createArticleLang($articleId, $languageId = 4, $description = '', $webdescription = '', $onwebshop = 0)
    {
        $lang = $this->findOneBy([
            'languageid' => $languageId,
            'articleid' => $articleId
        ]);
        if ($lang !== false) {
            return $this->updateArticleLang($lang->getId(), $articleId, $languageId, $description, $webdescription, $onwebshop);
        }

        $lang = new ArticleLang;
        $lang->setArticleid($articleId);
        $lang->setLanguageid($languageId);
        $lang->setOnwebshop($onwebshop);
        $lang->setDescription((string)$description);
        $lang->setWebDescription((string)$webdescription);

        $this->_em->persist($lang);
        $this->_em->flush();

        return $lang->getId();
    }

    public function updateArticleLang($id, $articleId, $languageId = 4, $description = '', $webdescription = '', $onwebshop = 0)
    {
        if ($id > 0) {
            $lang = $this->find($id);
        } else {
            $lang = $this->findOneBy([
                'languageid' => $languageId,
                'articleid' => $articleId
            ]);
        }
        $lang->setOnwebshop($onwebshop);
        $lang->setDescription((string)$description);
        $lang->setWebDescription((string)$webdescription);

        $this->_em->persist($lang);
        $this->_em->flush();

        return $lang->getId();
    }

    public function getArticleLang($languageId, $articleId)
    {
        $lang = $this->findOneBy(array(
            'languageid' => $languageId,
            'articleid' => $articleId
        ));
        if ($lang === null) {
            return array(
                'id' => 0,
                'articleid' => 0,
                'languageid' => 0,
                'onwebshop' => 1,
                'webdescription' => '',
                'description' => '',
            );
        }

        return $this->transform($lang);
    }

    public function transform(ArticleLang $item)
    {
        return [
            'id' => (int)$item->getId(),
            'articleid' => (int)$item->getArticleid(),
            'languageid' => (int)$item->getLanguageid(),
            'onwebshop' => (int)$item->getOnwebshop(),
            'webdescription' => (string)$item->getWebdescription(),
            'description' => (string)$item->getDescription(),
        ];
    }


}
