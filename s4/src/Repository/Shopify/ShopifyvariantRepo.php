<?php

namespace App\Repository\Shopify;

use App\Entity\Shopify\Shopifyvariant;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Shopifyvariant|null find($id, $lockMode = null, $lockVersion = null)
 * @method Shopifyvariant|null findOneBy(array $criteria, array $orderBy = null)
 * @method Shopifyvariant[]    findAll()
 * @method Shopifyvariant[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShopifyvariantRepo extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_shopifyUrl;
    private $_shopifyToken;
    private $_variantRepo;
    private $_articleLangRepo;
    private $_collectionMemberRepo;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Shopifyvariant::class);
        $this->_em = $em;
    }

    public function test(){
    	return 'asd';
    }

    public function transform(Shopifyvariant $item) {
        return [
            'id'                => (int) $item->getId(),
            'shopifyshopid'     => (int) $item->getShopifyshopid(),
            'variantcodeid'     => (int) $item->getVariantcodeid(),
            'shopifyvariantid'  => (string) $item->getShopifyvariantid(),
            'shopifyproductid'     => (string) $item->getShopifyproductid(),
            'inventoryitemid'   => (string) $item->getInventoryitemid()
        ];
    }

    public function getShopifyVariant($variantcodeId, $shopifyshopId){
        $variant = $this->findOneBy(array('Variantcodeid' => $variantcodeId, 'Shopifyshopid' => $shopifyshopId));

        if($variant !== null){
            return $this->transform($variant);
        }

        return false;
    }

    public function deleteShopifyVariant($shopifyvariantId){
        $variant = $this->find($shopifyvariantId);
        $variant->setShopifyvariantid("");
        $variant->setShopifyproductid("");
        $variant->setInventoryitemid("");

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }

    public function createShopifyVariant($shopifyId, $variantCodeId, $articleId, $shopifyvariantid, $shopifyproductid, $inventoryitemid){

        if($this->findOneBy(array('Variantcodeid' => $variantCodeId, 'Shopifyshopid' => $shopifyId)) !== null){
            $variant = $this->findOneBy(array('Variantcodeid' => $variantCodeId, 'Shopifyshopid' => $shopifyId));
            $variant->setShopifyvariantid($shopifyvariantid);
            $variant->setShopifyproductid($shopifyproductid);
            $variant->setInventoryitemid($inventoryitemid);

            $this->_em->persist($variant);
            $this->_em->flush();

            return $variant->getId();
        }

    	$variant = new Shopifyvariant;
        $variant->setShopifyshopid($shopifyId);
        $variant->setVariantcodeid($variantCodeId);
    	$variant->setArticleid($articleId);
    	$variant->setShopifyvariantid($shopifyvariantid);
    	$variant->setShopifyproductid($shopifyproductid);
        $variant->setInventoryitemid($inventoryitemid);

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }

    public function updateShopifyVariant($id, $shopifyvariantid, $shopifyproductid, $inventoryitemid){

    	$variant = $this->find($id);
    	$variant->setShopifyvariantid($shopifyvariantid);
    	$variant->setShopifyproductid($shopifyproductid);
        $variant->setInventoryitemid($inventoryitemid);

        $this->_em->persist($variant);
        $this->_em->flush();

        return $variant->getId();
    }


}
