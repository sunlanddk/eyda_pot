<?php

namespace App\Repository;

use Doctrine\ORM\EntityManagerInterface;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;

class TwoFactorRepository
{   
    protected $_em;
    private $_cipher_algo;
    private $_encrypt_key;
    private $_iv;

    public function __construct(
        EntityManagerInterface $em
    )
    {
        $this->_cipher_algo = openssl_get_cipher_methods()[0];
        $this->_encrypt_key = '2846dja5waY#%"/Ehbdiskisl';
        $this->_iv = base64_decode("IlY9Ia3OB+6Z3Q53yR49\/w==");
        $this->_em = $em;
    }
    public function createQRCode($token, $reset = false) {
        
        $decodedToken = base64_decode($token);
        $decodedToken = openssl_decrypt($decodedToken, $this->_cipher_algo, $this->_encrypt_key, OPENSSL_RAW_DATA, $this->_iv);
        $decodedToken = explode("-", $decodedToken);

        $loginname = $decodedToken[0];
        $timeStamp = $decodedToken[1];

        $expirationTimeInSec = 600; 

        if(time() - $timeStamp > $expirationTimeInSec) {
            return false;
        }
        if($reset) {
            $query = "SELECT * FROM user WHERE Active = 1 AND loginname = :loginname";
            $conn = $this->_em->getConnection();
            $stmt = $conn->prepare($query);
            $stmt->execute(["loginname" => $loginname]);
            $user = $stmt->fetch();

            if($user["2FAToken"] != $token) {
                return array("codeVerification" => true);
            }
        }

        $g = new GoogleAuthenticator();

        $salt = $g->generateSecret();

        $query = "UPDATE user SET 2FA = :2FA, 2FAToken = :token WHERE loginname  = :loginname AND 2FAToken != :token";
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute(["2FA" => $salt, "loginname" => $loginname, "token" => $token ]);


        $query = "SELECT * FROM user WHERE Active = 1 AND loginname = :loginname";
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute(["loginname" => $loginname]);
        $user = $stmt->fetch();

        $qr =  $g->getURL($loginname, 'eyda.dk', $user["2FA"]);

        return array("code" => $user["2FA"], "qr" => $qr);
    }


    public function verifyCode($loginname, $code) {
        $g = new GoogleAuthenticator();

        $query = sprintf("SELECT 2FA FROM user WHERE Active = 1 AND loginname = '%s'", $loginname);
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $result = $stmt->fetch();
        $salt = $result['2FA'];
        return  $g->checkCode($salt, (string)$code);
    }

    public function getLoginnameByToken($token) {
        $decodedToken = base64_decode($token);
        $decodedToken = openssl_decrypt($decodedToken, $this->_cipher_algo, $this->_encrypt_key, OPENSSL_RAW_DATA, $this->_iv);
        $decodedToken = explode("-", $decodedToken);

        return $decodedToken[0];
    }
}
