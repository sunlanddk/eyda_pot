<?php

namespace App\Repository\Collection;

use App\Entity\Collection\Collectionmember;
use App\Repository\Article\VariantcodeRepository;
use App\Repository\Shopify\ShopifyvariantRepo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Collectionmember|null find($id, $lockMode = null, $lockVersion = null)
 * @method Collectionmember|null findOneBy(array $criteria, array $orderBy = null)
 * @method Collectionmember[]    findAll()
 * @method Collectionmember[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CollectionMemberRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $sizeScaleId;
    private $styleGroup;
    private $_shopifyUrl;
    private $_shopifyToken;
    private $_variantcodeRepository;
    private $_shopifyvariantRepo;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        VariantcodeRepository $variantcodeRepository,
        ShopifyvariantRepo $shopifyvariantRepo
    )
    {
        parent::__construct($registry, Collectionmember::class);
        $this->_em = $em;
        $this->sizeScaleId = 8;
        $this->styleGroup = 5;
        $this->_shopifyUrl = '';//$_SERVER['SHOPIFY_URL'];
        $this->_shopifyToken = '';
        $this->_variantcodeRepository = $variantcodeRepository;
        $this->_shopifyvariantRepo = $shopifyvariantRepo;
    }

    public function transform(Collectionmember $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'            => (int) $item->getActive(),
            'articleid'         => (int) $item->getArticleid(),
            'articlecolorid'    => (int) $item->getArticlecolorid(),
        ];
    }


    public function getCollectionMember($id){
        $variant = $this->find($id);
        return $this->transform($variant);
    }

    private function setUrl($shop){

        if($shop['shopname'] == 'da'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_DA'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_DA'];
            return true;
        }

        if($shop['shopname'] == 'eu'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_EU'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_EU'];
            return true;
        }

        if($shop['shopname'] == 'no'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NO'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NO'];
            return true;
        }

        if($shop['shopname'] == 'se'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_SE'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_SE'];
            return true;
        }

        if($shop['shopname'] == 'am'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_AMBASSADOR'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_AMBASSADOR'];
            return true;
        }

        if($shop['shopname'] == 'b2b'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_B2B'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_B2B'];
            return true;
        }

        if($shop['shopname'] == 'nl'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NL'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NL'];
            return true;
        }

        return false;
    }


    public function deleteProductInShopify($colMember, $variants, $shopifyProductId, $shop){
        $url = $this->setUrl($shop);
        $lang = (int)$shop['languageid'];

        if($url === false){
            return "Cant find shop url";
        }


        foreach ($variants as $key => $variant) {
            if($variant['shopifyvariantid'] == ""){
                continue;
            }

            $ch = curl_init($this->_shopifyUrl.'products/'.$shopifyProductId.'/variants/'.$variant['shopifyvariantid'].'.json');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
                curl_close($ch);
                return $error_msg;
            }
            $info = curl_getinfo($ch);
            curl_close($ch);

            $this->_shopifyvariantRepo->deleteShopifyVariant($variant['shopifyid']);

            if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
                return '(Delete variant: '.$variant['shopifyvariantid'].') '.$result;
            }


        }

        return true;
    }

    private function updateMetaField($shopifyProductId, $articleId, $test = false){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
            SELECT color.Description
            FROM `collectionmember` 
            LEFT JOIN `articlecolor` ON collectionmember.ArticleColorId = articlecolor.id
            LEFT JOIN `color` ON articlecolor.ColorId = color.Id
            LEFT JOIN `colorgroup` ON color.ColorGroupId = colorgroup.Id
            WHERE collectionmember.discontinued = 1 AND collectionmember.active = 1 AND collectionmember.ArticleId=%d",
            $articleId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        $return = $stmt->fetchAll();
        $colors = array();

        foreach($return as $value){
            array_push($colors, $value['Description']);
        }

        if(implode(',', $colors) == ''){
            return true;
        }

        $updateMetafield = array(
            "metafield" => array(
                "key"       =>"discontinued",
                "value"     => implode(',', $colors),
                "type"      =>"multi_line_text_field",
                "namespace" =>"fields"
            )
        );

        if($test === true){
            return $updateMetafield;
        }

        $ch = curl_init($this->_shopifyUrl.'products/'.$shopifyProductId.'/metafields.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateMetafield));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);


        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return '(Main product metafield: '.$shopifyProductId.')'.$error_msg;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
            return '(Main product metafield: '.$shopifyProductId.')'.$result;
        }
        return true;

    }

    private function getCollectionMemberColorGroup($colMemberId, $shopId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("SELECT 
            IF(colorgroup.Name IS NOT NULL, colorgroup.Name, color.Description) as Color
            FROM (`collectionmember`, `inshopifyshops`)
            LEFT JOIN `articlecolor` ON `collectionmember`.ArticleColorId = `articlecolor`.id
            LEFT JOIN `color` ON `articlecolor`.ColorId = `color`.Id
            LEFT JOIN `colorgroup` ON `color`.ColorGroupId = `colorgroup`.Id
            WHERE collectionmember.ArticleId=%d AND collectionmember.Active=1 AND inshopifyshops.ShopifyShopId=%d AND inshopifyshops.active=1 AND inshopifyshops.CollectionMemberId=collectionmember.Id
            GROUP BY colorgroup.Id",
            $colMemberId,
            $shopId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $return = $stmt->fetchAll();
        $colors = array();
        foreach($return as $value){
            array_push($colors, 'color:'.$value['Color']);
        }

        // return print_r($colors);
        if(count($colors) > 0){
            return implode(",",$colors);
        }

        return false;

    }

    private function getProductTags($colMember, $shopId){
        $tags = '';
        $tags .= $this->getSizeScale($colMember['articleid']);

        if($this->getCollectionMemberColorGroup($colMember['articleid'], $shopId) !== false) {
            if($tags !== ''){
                $tags .= ",";
            }
            $tags .= $this->getCollectionMemberColorGroup($colMember['articleid'], $shopId);
        }

        if($this->getCustomTags($colMember['articleid']) !== false) {
            if($tags !== ''){
                $tags .= ",";
            }
            $tags .= $this->getCustomTags($colMember['articleid']);
        }


        return $tags;

    }

    public function updateProductTest($colMember, $variants, $shopifyProductId, $shop){
        $url = $this->setUrl($shop);
        $lang = (int)$shop['languageid'];

        if($url === false){
            return "Cant find shop url";
        }


        $productType = $this->getProductType($colMember['articleid']);
        $productDescription = $this->getProductDescription($colMember['articleid']);
        $productStatus = $this->getProductStatus($colMember['articleid']);
        $colorName = $this->getColorName($colMember['articlecolorid'], $lang);
        $colorName['Name'] = ((string)$colorName['LangName'] !== '' ? $colorName['LangName'] : $colorName['Name']);
        return $shopifyVariants = $this->getVariants($variants, $colMember['id'], $colorName, (int)$shop['currencyid']);
        $tags = $this->getProductTags($colMember, $shop['shopid']);

        if(getType($shopifyVariants) == 'string'){
            return $shopifyVariants;
        }

        // $shopifyVariants = $this->getAllVariants($colMember['article']);

        if(isset($productType['name']) === false){
            return 'No product type set';
        }

        $name = (isset($productDescription['lang'][$lang]['name']) === true ? $productDescription['lang'][$lang]['name'] : $productDescription['name']);
        if($name == ''){
            $name = $productDescription['name'];
        }

         $updateProduct = array(
            "product" => array(
                "id" => $shopifyProductId,
                "title" => utf8_encode($name),
                "body_html" => (isset($productDescription['lang'][$lang]['description']) === true ? $productDescription['lang'][$lang]['description'] : ''),
                "product_type" => $productType['name'],
                "status" => strtolower($productStatus),
                "tags" => $tags,
                //"variants" => $shopifyVariants,
            )
        );



        //return $updateMetafield = $this->updateMetaField($shopifyProductId,$colMember['articleid'], true);

    }


    public function updateProduct($colMember, $variants, $shopifyProductId, $shop){
        $url = $this->setUrl($shop);
        $lang = (int)$shop['languageid'];

        if($url === false){
            return "Cant find shop url";
        }

        $productType = $this->getProductType($colMember['articleid']);
        $productDescription = $this->getProductDescription($colMember['articleid']);
        $productStatus = $this->getProductStatus($colMember['articleid']);
        $colorName = $this->getColorName($colMember['articlecolorid'], $lang);
        $colorName['Name'] = ((string)$colorName['LangName'] !== '' ? $colorName['LangName'] : $colorName['Name']);
        $shopifyVariants = $this->getVariants($variants, $colMember['id'], $colorName, (int)$shop['currencyid']);
        $tags = $this->getProductTags($colMember, $shop['shopid']);

        if(getType($shopifyVariants) == 'string'){
            return $shopifyVariants;
        }

        // $shopifyVariants = $this->getAllVariants($colMember['article']);

        if(isset($productType['name']) === false){
            return 'No product type set';
        }

        $name = (isset($productDescription['lang'][$lang]['name']) === true ? $productDescription['lang'][$lang]['name'] : $productDescription['name']);
        if($name == ''){
            $name = $productDescription['name'];
        }

         $updateProduct = array(
            "product" => array(
                "id" => $shopifyProductId,
                "title" => utf8_encode($name),
                "body_html" => (isset($productDescription['lang'][$lang]['description']) === true ? $productDescription['lang'][$lang]['description'] : ''),
                "product_type" => $productType['name'],
                "status" => strtolower($productStatus),
                "tags" => $tags,
                //"variants" => $shopifyVariants,
            )
        );

        $ch = curl_init($this->_shopifyUrl.'products/'.$shopifyProductId.'.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateProduct));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return '(Main product: '.$shopifyProductId.')'.$error_msg;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
            return '(Main product: '.$shopifyProductId.')'.$result;
        }

        $updateMetafield = $this->updateMetaField($shopifyProductId,$colMember['articleid']);
        if($updateMetafield !== true){
            return $updateMetafield;
        }

        $variantsUpdated = $this->updateVariants($shopifyVariants, $shopifyProductId, $shop);
        if($variantsUpdated !== true){
            return $variantsUpdated;
        }

        return $this->updateInventoryItems($colMember, $shop['shopid']);
    }

    private function updateInventoryItems($colMember, $shopId){
        $variants = $this->_variantcodeRepository->getAllVariantByArticleColor($colMember['articleid'], $colMember['articlecolorid'], $shopId);
        $countryHs = $this->getCountryAndHsCode($colMember['articleid']);
        foreach ($variants as $key => $value) {
            $updateProduct = array(
                "inventory_item" => array(
                    "id" => $value['inventoryitemid'],
                    //"harmonized_system_code" => (string)$countryHs['hscode'],
                    //"country_code_of_origin" => (string)$countryHs['country']
                )
            );

            if((string)$countryHs['hscode'] !== ''){
                $updateProduct['inventory_item']['harmonized_system_code'] = (string)$countryHs['hscode'];
            }

            if((string)$countryHs['country'] !== ''){
                $updateProduct['inventory_item']['country_code_of_origin'] = (string)$countryHs['country'];
            }

            // if((string)$countryHs['country'] == '' && (string)$countryHs['hscode'] == '' && (int)$shopId !== 5){
            if((string)$countryHs['country'] == '' && (string)$countryHs['hscode'] == ''){
                continue;
            }

            if((int)$shopId === 5){
                // $updateProduct['inventory_item']['cost'] = (string)$value['cost'];
            }

            $ch = curl_init($this->_shopifyUrl.'inventory_items/'.$value['inventoryitemid'].'.json');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateProduct));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
                curl_close($ch);
                return $error_msg;
            }
            $info = curl_getinfo($ch);
            curl_close($ch);
            if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
                return $result;
            }
        }

        return true;
    }

    private function getCountryAndHsCode($articleId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT c.IsoCode as country, cp.Name as hscode
                FROM article a
                LEFT JOIN country c ON c.Id=a.WorkCountryId
                LEFT JOIN customsposition cp ON cp.Id=a.CustomsPositionId
                WHERE a.Active=1 AND a.Id=%d
            ",
            $articleId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetch();
        return $typesDb;
    }

    private function updateVariants($shopifyVariants, $shopifyProductId, $shop){
        foreach ($shopifyVariants as $key => $variant) {
            if(isset($variant['id']) === true){
                // update variant
                $return = $this->updateVariantInShopify($variant, $shopifyProductId);
                if($return !== true){
                    return '(Update Variant: '.$variant['id'].')' . $return;
                }

            }
            else{
                // create new variant
                $return = $this->createNewVariantInShopify($variant, $shopifyProductId, $shop['shopid']);
                if($return !== true){
                    return '(New Variant: '.$variant['sku'].')' . $return;
                }
            }
        }

        return true;
    }

    private function createNewVariantInShopify($variant, $shopifyProductId, $shopifyId){
        $updateProduct = array(
            "variant" => $variant
        );

        $ch = curl_init($this->_shopifyUrl.'products/'.$shopifyProductId.'/variants.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateProduct));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);

        if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
            return $result;
        }

        $resultObj = json_decode($result, true);
        if(isset($resultObj['variant']['id']) === false){
            return $result;
        }

        $shopifyVariantId = $resultObj['variant']['id'];
        $shopifyInventoryId = $resultObj['variant']['inventory_item_id'];

        $variantId = $this->getVariantIdBySku($variant['barcode']);
        $articleId = $this->getArticleIdBySku($variant['barcode']);
        $this->_shopifyvariantRepo->createShopifyVariant($shopifyId, $variantId, $articleId, $shopifyVariantId, $shopifyProductId, $shopifyInventoryId);
        // $this->_variantcodeRepository->updateVariantcode($variantId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId);
        return true;
    }

    private function updateVariantInShopify($variant, $shopifyProductId){
        $updateProduct = array(
            "variant" => $variant
        );

        $ch = curl_init($this->_shopifyUrl.'variants/'.$variant['id'].'.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateProduct));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }
        $info = curl_getinfo($ch);
        curl_close($ch);
        if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
            return $result;
        }

        $resultObj = json_decode($result, true);
        if(isset($resultObj['variant']['id']) === false){
            return $result;
        }

        return true;
    }

    public function createNewProduct($colMember, $variants, $shop){
        $url = $this->setUrl($shop);
        $lang = (int)$shop['languageid'];

        if($url === false){
            return "Cant find shop url";
        }
        $productType = $this->getProductType($colMember['articleid']);
        $productStatus = $this->getProductStatus($colMember['articleid']);
        $productDescription = $this->getProductDescription($colMember['articleid']);
        $colorName = $this->getColorName($colMember['articlecolorid'], $lang);
        $shopifyVariants = $this->createVariants($colMember['id'], $variants, $colorName, (int)$shop['currencyid']);
        $tags = $this->getProductTags($colMember,$shop['shopid']);

        if(getType($shopifyVariants) == 'string'){
            return $shopifyVariants;
        }

        if(isset($productType['name']) === false){
            return 'No product type set';
        }

        $name = (isset($productDescription['lang'][$lang]['name']) === true ? $productDescription['lang'][$lang]['name'] : $productDescription['name']);
        if($name == ''){
            $name = $productDescription['name'];
        }

        $newProduct = array(
            "product" => array(
                "title" => utf8_encode($name),
                "body_html" => (isset($productDescription['lang'][$lang]['description']) === true ? $productDescription['lang'][$lang]['description'] : ''),
                "vendor" => "EYDA",
                "product_type" => $productType['name'],
                "status" => strtolower($productStatus),
                "tags" => $tags,
                "variants" => $shopifyVariants,
                "options" => [
                    array(
                        "name" => "Color",
                        "position" => 1,
                        "values" => []
                    ),
                    array(
                        "name" => "Size",
                        "position" => 2,
                        "values" => []
                    )
                ]
            )
        );

        $ch = curl_init($this->_shopifyUrl.'products.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($newProduct));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }

        $info = curl_getinfo($ch);
        curl_close($ch);
        if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
            return $result;
        }

        $result = json_decode($result, true);

        $updateMetafield = $this->updateMetaField($result['product']['id'],$colMember['articleid']);
        if($updateMetafield !== true){
            return $updateMetafield;
        }

        return $this->saveShopifyIds($colMember, $variants, $result, $shop['shopid']);
    }

    private function saveShopifyIds($colMember, $variants, $result, $shopId){

        // return $variants;
        foreach ($result['product']['variants'] as $key => $variant) {
            $variantId = $this->getVariantId($variants, $variant);
            $articleId = $this->getArticleId($variants, $variant);
            $this->_shopifyvariantRepo->createShopifyVariant($shopId, $variantId['id'], $articleId, $variant['id'], $variant['product_id'], $variant['inventory_item_id']);
            // $this->_variantcodeRepository->updateVariantcode($variantId['Id'], $variant['product_id'], $variant['id'], $variant['inventory_item_id']);
        }
        return true;
    }

    private function getVariantIdBySku($variantcode){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT v.Id
                FROM variantcode v
                WHERE v.Active=1 AND v.VariantCode='%s' AND v.Active=1
            ",
            $variantcode
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetch();
        return (int)$typesDb['Id'];
    }

    private function getArticleIdBySku($variantcode){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT v.ArticleId
                FROM variantcode v
                WHERE v.Active=1 AND v.VariantCode='%s' AND v.Active=1
            ",
            $variantcode
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetch();
        return (int)$typesDb['ArticleId'];
    }

    private function getArticleId($variants, $shopifyVariant){
        foreach ($variants as $key => $value) {
            if($value['variantcode'] == $shopifyVariant['barcode']){
                return $this->getArticleIdBySku($value['variantcode']);
            }
        }
        return false;
    }

    private function getVariantId($variants, $shopifyVariant){
        foreach ($variants as $key => $value) {
            if($value['variantcode'] == $shopifyVariant['barcode']){
                return $value;
            }
        }
        return false;
    }

    private function getVariants($variantcodes, $colMemberId, $colorName, $currencyId){
        $variants = [];
        foreach ($variantcodes as $key => $value) {
            $prices = $this->getPrice($colMemberId, $value['articlesizeid']);
            if(isset($prices[$currencyId]) !== true){
                return 'Price not set on currency';
            }
            if((int)$prices[$currencyId]['RetailPrice'] <= 0){
                return 'Price cant be zero';
            }
            if((int)$prices[$currencyId]['RetailPrice'] <= 0 && (int)$prices[$currencyId]['CampaignPrice'] <= 0){
                return 'Price cant be zero';
            }

            $weight = $this->getWeight($value['id']);

            $size = $this->getSizeName($value['articlesizeid']);
            if($value['shopifyvariantid'] !== ''){
                $tempVar = array(
                    "id"                    => $value['shopifyvariantid'],
                    "price"                 => $prices[$currencyId]['RetailPrice'],
                    "option1"               => $colorName['Name'],
                    "option2"               => $size,
                    "option3"               => null,
                    "barcode"               => $value['variantcode'],
                    "grams"                 => $weight,
                    "weight"                => $weight,
                );
            }
            else{
               $tempVar = array(
                    "price"                 => $prices[$currencyId]['RetailPrice'],
                    "sku"                   => $value['sku'],
                    "inventory_policy"      => "deny",
                    "fulfillment_service"   => "manual",
                    "inventory_management"  => "shopify",
                    "option1"               => $colorName['Name'],
                    "option2"               => $size,
                    "option3"               => null,
                    "taxable"               => true,
                    "barcode"               => $value['variantcode'],
                    "grams"                 => $weight,
                    "weight"                => $weight,
                    "weight_unit"           => "g",
                    // "inventory_quantity"    => 0,
                    "requires_shipping"     => true
                );
            }

            if((int)$prices[$currencyId]['CampaignPrice'] > 0 && (int)$prices[$currencyId]['CampaignPrice'] !== (int)$prices[$currencyId]['RetailPrice']){
                $tempVar['price'] = $prices[$currencyId]['CampaignPrice'];
                $tempVar['compare_at_price'] = $prices[$currencyId]['RetailPrice'];
            }
            else{
                $tempVar['compare_at_price'] = '';
            }
            array_push($variants, $tempVar);
        }

        return $variants;
    }

    private function getVariantsTest($variantcodes, $colMemberId, $colorName, $currencyId){
        $variants = [];
        foreach ($variantcodes as $key => $value) {
            return sprintf("
            SELECT cmps.RetailPrice, cmps.CampaignPrice, c.Name, c.Id
            FROM (`collectionmemberprice` cmp, `collectionmemberpricesize` cmps, currency c)
            WHERE cmp.Active=1 AND cmp.CollectionMemberId=%d AND cmps.CollectionMemberPriceId=cmp.Id ANd cmps.Active=1 AND cmp.CurrencyId IN(1,2,4,5) AND c.Id=cmp.CurrencyId ANd cmps.ArticleSizeId=%d
            ",
            $colMemberId,
            $value['articlesizeid']
        );
            return $prices = $this->getPrice($colMemberId, $value['articlesizeid']);
            if(isset($prices[$currencyId]) !== true){
                return 'Price not set on currency';
            }
            if((int)$prices[$currencyId]['RetailPrice'] <= 0){
                return 'Price cant be zero';
            }
            if((int)$prices[$currencyId]['RetailPrice'] <= 0 && (int)$prices[$currencyId]['CampaignPrice'] <= 0){
                return 'Price cant be zero';
            }

            $weight = $this->getWeight($value['id']);

            $size = $this->getSizeName($value['articlesizeid']);
            if($value['shopifyvariantid'] !== ''){
                $tempVar = array(
                    "id"                    => $value['shopifyvariantid'],
                    "price"                 => $prices[$currencyId]['RetailPrice'],
                    "option1"               => $colorName['Name'],
                    "option2"               => $size,
                    "option3"               => null,
                    "barcode"               => $value['variantcode'],
                    "grams"                 => $weight,
                    "weight"                => $weight,
                );
            }
            else{
               $tempVar = array(
                    "price"                 => $prices[$currencyId]['RetailPrice'],
                    "sku"                   => $value['sku'],
                    "inventory_policy"      => "deny",
                    "fulfillment_service"   => "manual",
                    "inventory_management"  => "shopify",
                    "option1"               => $colorName['Name'],
                    "option2"               => $size,
                    "option3"               => null,
                    "taxable"               => true,
                    "barcode"               => $value['variantcode'],
                    "grams"                 => $weight,
                    "weight"                => $weight,
                    "weight_unit"           => "g",
                    // "inventory_quantity"    => 0,
                    "requires_shipping"     => true
                );
            }

            if((int)$prices[$currencyId]['CampaignPrice'] > 0 && (int)$prices[$currencyId]['CampaignPrice'] !== (int)$prices[$currencyId]['RetailPrice']){
                $tempVar['price'] = $prices[$currencyId]['CampaignPrice'];
                $tempVar['compare_at_price'] = $prices[$currencyId]['RetailPrice'];
            }
            else{
                $tempVar['compare_at_price'] = '';
            }
            array_push($variants, $tempVar);
        }

        return $variants;
    }

    private function createVariants($colMemberId, $variantcodes, $colorName, $currencyId){

        $variants = [];
        $sizes = [];
        foreach ($variantcodes as $key => $value) {
            $prices = $this->getPrice($colMemberId, $value['articlesizeid']);
            if(isset($prices[$currencyId]) !== true){
                return 'Price not set on currency';
            }
            if((int)$prices[$currencyId]['RetailPrice'] <= 0){
                return 'Price cant be zero';
            }
            if((int)$prices[$currencyId]['RetailPrice'] <= 0 && (int)$prices[$currencyId]['CampaignPrice'] <= 0){
                return 'Price cant be zero';
            }
            $weight = $this->getWeight($value['id']);
            $size = $this->getSizeName($value['articlesizeid']);
            array_push($sizes, $size);
            $tempVar = array(
                "price"                 => $prices[$currencyId]['RetailPrice'],
                "sku"                   => $value['sku'],
                "inventory_policy"      => "deny",
                "fulfillment_service"   => "manual",
                "inventory_management"  => "shopify",
                "option1"               => $colorName['Name'],
                "option2"               => $size,
                "option3"               => null,
                "taxable"               => true,
                "barcode"               => $value['variantcode'],
                "grams"                 => $weight,
                "weight"                => $weight,
                "weight_unit"           => "g",
                // "inventory_quantity"    => 0,
                "requires_shipping"     => true
            );

            if((int)$prices[$currencyId]['CampaignPrice'] > 0 && (int)$prices[$currencyId]['CampaignPrice'] !== (int)$prices[$currencyId]['RetailPrice']){
                $tempVar['price'] = $prices[$currencyId]['CampaignPrice'];
                $tempVar['compare_at_price'] = $prices[$currencyId]['RetailPrice'];
            }
            else{
                $tempVar['compare_at_price'] = '';
            }
            array_push($variants, $tempVar);

        }
        return $variants;
    }

    private function getProductDescription($articleId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT al.Description as Name, al.WebDescription as Description, l.Culture as LangName, l.Id, a.Description as AName
                FROM (article_lang al, language l, article a)
                WHERE al.ArticleId=%d AND l.Id=al.LanguageId AND a.Id=al.ArticleId
            ",
            $articleId
        );

        $sql = sprintf("
                SELECT al.Description as Name, al.WebDescription as Description, l.Culture as LangName, l.Id, a.Description as AName
                FROM (article a)
                LEFT JOIN article_lang al ON al.ArticleId=a.Id
                LEFT JOIN language l ON l.Id=al.LanguageId
                WHERE a.Id=%d
            ",
            $articleId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetchAll();
        $types = [];
        foreach ($typesDb as $key => $value) {
            $types['name'] = (string)$value['AName'];
            $types['description'] = '';
            $types['lang'][(int)$value['Id']]['name'] = (string)$value['Name'];
            $types['lang'][(int)$value['Id']]['description'] = utf8_encode(base64_decode((string)$value['Description']));
        }
        return $types;
    }

    private function getWeight($variantCodeId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT v.Weight
                FROM variantcode v
                WHERE v.Id=%d
            ",
            $variantCodeId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetch();
        return $typesDb['Weight'];
    }

    private function getProductStatus($articleId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT c.Name
                FROM (cluster c, clusterarticle ca)
                WHERE ca.ArticleId=%d ANd ca.TypeId=%d AND c.Id=ca.ClusterId AND ca.Active=1 
            ",
            $articleId,
            9
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetch();
        return ($typesDb === false ? 'draft' : $typesDb['Name']);
    }

    private function getProductType($articleId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT c.Id, c.Name, l.Culture as LangName, cl.Name as cLangName
                FROM (cluster c, clusterarticle ca)
                LEFT JOIN cluster_lang cl ON cl.ClusterId=c.Id ANd cl.Active=1
                LEFT JOIN language l ON l.Id=cl.LangId
                WHERE ca.ArticleId=%d ANd ca.TypeId=%d AND c.Id=ca.ClusterId AND ca.Active=1 
            ",
            $articleId,
            5
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $typesDb = $stmt->fetchAll();
        $types = [];
        foreach ($typesDb as $key => $value) {
            $types['name'] = $value['Name'];
            if((string)$value['LangName'] !== '' && (string)$value['cLangName'] !== ''){
                $types['lang'][$value['LangName']] = $value['cLangName'];
            }
        }
        return $types;
    }

    private function getCustomTags($articleId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT ca.Name
                FROM (clusterarticle ca)
                WHERE ca.ArticleId=%d ANd ca.Active=1 AND ca.TypeId=11 AND ca.ClusterId=33
            ",
            $articleId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $sizesDb = $stmt->fetch();
        if(isset($sizesDb['Name']) === true){
            return $sizesDb['Name'];
        }
        return false;
    }

    private function getSizeScale($articleId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
                SELECT c.Name
                FROM (cluster c, clusterarticle ca, clustertype ct)
                WHERE ca.ArticleId=%d ANd c.Id=ca.ClusterId AND ca.Active=1 AND c.TypeId=ct.Id AND ct.ClusterGroupId=1 AND ct.Id!=11
            ",
            $articleId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $sizesDb = $stmt->fetchAll();
        $sizes = [];
        foreach ($sizesDb as $key => $value) {
            array_push($sizes, $value['Name']);
        }

        return implode(',', $sizes);
        $size = 'size-scale-1';
        return $size = $sizesDb['Name'];
        switch ($sizesDb['Name']) {
            case '1':
                $size = 'size-scale-1';
                break;
            case '2':
                $size = 'size-scale-2';
                break;
            case '3':
                $size = 'size-scale-3';
                break;
            case '4':
                $size = 'size-scale-4';
                break;
            case '5':
                $size = 'size-scale-5';
                break;
            default:
                $size = 'size-scale-1';
                break;
        }

        return $size;
    }

    private function getPrice($colMemberId, $sizeId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
            SELECT cmps.RetailPrice, cmps.CampaignPrice, c.Name, c.Id
            FROM (`collectionmemberprice` cmp, `collectionmemberpricesize` cmps, currency c)
            WHERE cmp.Active=1 AND cmp.CollectionMemberId=%d AND cmps.CollectionMemberPriceId=cmp.Id ANd cmps.Active=1 AND cmp.CurrencyId IN(1,2,4,5) AND c.Id=cmp.CurrencyId ANd cmps.ArticleSizeId=%d
            ",
            $colMemberId,
            $sizeId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $pricesDb = $stmt->fetchAll();
        $prices = [];
        foreach ($pricesDb as $key => $value) {
            $prices[(int)$value['Id']] = $value;
        }
        return $prices;
    }

    private function getColorName($articleColorId, $languageId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
            SELECT c.Id, c.Description as Name, cg.ValueRed as Red, cg.ValueGreen as Green, cg.ValueBlue as Blue, cl.Description as LangName
            FROM (`articlecolor` ac, `color` c) 
            LEFT JOIN colorgroup cg ON cg.Id=c.ColorGroupId AND cg.Active=1
            LEFT JOIN color_lang cl ON cl.ColorId=c.Id AND cl.LanguageId=%d
            WHERE 
                ac.`Id` = %d AND 
                c.Id=ac.ColorId
            ",
            $languageId,
            $articleColorId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetch();
    }

    private function getSizeName($articleSizeId){
        $conn = $this->_em->getConnection();
        $sql = sprintf("
            SELECT az.Name as Name
            FROM (`articlesize` az) 
            WHERE 
                az.`Id` = %d
            ",
            $articleSizeId
        );


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $size = $stmt->fetch();
        return $size['Name'];
    }

}
/*
{
    "product": {
        "title": "Amaya Crop Tee Test Create",
        "body_html": "",
        "vendor": "EYDA",
        "product_type": "Tee",
        "handle": "amaya-crop-tee",
        "status": "active",
        "tags": "size-scale-3",
        "variants": [
            {
                "title": "Grey Melange / XS",
                "price": "249.00",
                "sku": "636",
                "position": 1,
                "inventory_policy": "deny",
                "fulfillment_service": "manual",
                "inventory_management": "shopify",
                "option1": "Grey Melange",
                "option2": "XS",
                "option3": null,
                "taxable": true,
                "barcode": "",
                "grams": 150,
                "weight": 150.0,
                "weight_unit": "g",
                "inventory_quantity": 0,
                "old_inventory_quantity": 0,
                "requires_shipping": true
            },
            {
                "title": "Bubble Gum / XL",
                "price": "249.00",
                "sku": "635",
                "position": 15,
                "inventory_policy": "deny",
                "fulfillment_service": "manual",
                "inventory_management": "shopify",
                "option1": "Bubble Gum",
                "option2": "XL",
                "option3": null,
                "taxable": true,
                "barcode": "",
                "grams": 180,
                "weight": 180.0,
                "weight_unit": "g",
                "inventory_quantity": 0,
                "old_inventory_quantity": 0,
                "requires_shipping": true
            }
        ],
        "options": [
            {
                "name": "Color",
                "position": 1,
                "values": [
                    "Grey Melange",
                    "Bubble Gum"
                ]
            },
            {
                "name": "Size",
                "position": 2,
                "values": [
                    "XS",
                    "S",
                    "M",
                    "L",
                    "XL"
                ]
            }
        ]
    }
}

*/
