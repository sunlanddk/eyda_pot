<?php

namespace App\Repository;

use App\Entity\Pim;
use App\Entity\Stock\Item;
use App\Repository\Article\ArticleLangRepository;
use App\Repository\Article\VariantcodeRepository;
use App\Repository\Collection\CollectionMemberRepository;
use App\Repository\Cron\InventoryRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PimRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_shopifyUrl;
    private $_shopifyToken;
    private $_variantRepo;
    private $_articleLangRepo;
    private $_collectionMemberRepo;
    private $_inventoryRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        VariantcodeRepository $variantRepo,
        ArticleLangRepository $articleLangRepo,
        CollectionMemberRepository $collectionMemberRepo,
        InventoryRepository $inventoryRepository
    )
    {
        parent::__construct($registry, Pim::class);
        $this->_em = $em;
        $this->_shopifyUrl = '';
        $this->_shopifyToken = '';
        $this->_variantRepo = $variantRepo;
        $this->_articleLangRepo = $articleLangRepo;
        $this->_collectionMemberRepo = $collectionMemberRepo;
        $this->_inventoryRepository = $inventoryRepository;
    }

    private function setUrl($shop){

        if($shop['shopname'] == 'da'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_DA'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_DA'];
            return true;
        }

        if($shop['shopname'] == 'eu'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_EU'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_EU'];
            return true;
        }

        if($shop['shopname'] == 'no'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NO'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NO'];
            return true;
        }

        if($shop['shopname'] == 'se'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_SE'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_SE'];
            return true;
        }

        if($shop['shopname'] == 'am'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_AMBASSADOR'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_AMBASSADOR'];
            return true;
        }

        if($shop['shopname'] == 'b2b'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_B2B'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_B2B'];
            return true;
        }

        if($shop['shopname'] == 'nl'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NL'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NL'];
            return true;
        }

        return false;
    }

    public function setDescriptionOnShop($array){
        $languageId = $array[0];
        $articleId = $array[1];

        foreach ($this->getShopifyShops() as $key => $shop) {
            $variant = $this->_variantRepo->getSingleVariantByArticleId($articleId, $shop['shopid']);
            if($variant === false){
                return 'No variant found.';
            }

            $this->setUrl($shop);

            $articleLang = $this->_articleLangRepo->getArticleLang($shop['languageid'], $articleId);

            if((int)$articleLang['onwebshop'] === 0){
                continue;
            }
            $updateProduct = array(
                "product" => array(
                    "id"        => $variant['shopifyproductid'],
                    "title"     => utf8_encode($articleLang['description']),
                    "body_html" => addslashes(utf8_encode(base64_decode($articleLang['webdescription'])))
                )
            );

            $ch = curl_init($this->_shopifyUrl.'products/'.$variant['shopifyproductid'].'.json');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateProduct));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
                curl_close($ch);
                return $error_msg;
            }
            $info = curl_getinfo($ch);
            curl_close($ch);
            if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
                return $result;
            }
            $result = json_decode($result);

        }

        return $result;
    }

    public function getDescriptionFromShop($array){
        $languageId = $array[0];
        $articleId = $array[1];

        $variant = $this->_variantRepo->getSingleVariantByArticleId($articleId);
        if($variant === false){
            return 'No variant found.';
        }

        if($variant['shopifyproductid'] == ''){
            return 'Not published to shopify.';
        }


        $ch = curl_init($this->_shopifyUrl.'products/'.$variant['shopifyproductid'].'.json');
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        // curl_setopt($ch, CURLOPT_POSTFIELDS, $potdata);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }
        curl_close($ch);

        $result = json_decode($result);
        return $this->_articleLangRepo->createArticleLangFromShop($articleId, $languageId, base64_encode(utf8_decode($result->product->body_html)));
    }

    public function deleteProduct($array){

        $colMemberId = $array[0];

        foreach ($this->getShopifyShops() as $key => $shop) {
            // $return = $this->createOnSeperateShops($colMemberId, $shop);
            $return = $this->deleteOnSeperateShops($colMemberId, $shop);
            if($return !== true){
                return $shop['shopname']. ': ' . $return;
            }
        }

        return true;
    }

    public function publishProduct($array){

        $colMemberId = $array[0];

        if(empty($this->getShopifyShops((int)$colMemberId)) === true){
            return "Can't push to shopify because no shops are set active.";
        }

        foreach ($this->getShopifyShops((int)$colMemberId) as $key => $shop) {
            $return = $this->createOnSeperateShops($colMemberId, $shop);
            if($return !== true){
                return $shop['shopname']. ': ' . $return;
            }
        }

        $this->updateStock($colMemberId);

        return true;
    }

    public function publishProductTest($array){

        $colMemberId = $array[0];

        if(empty($this->getShopifyShops((int)$colMemberId)) === true){
            return "Can't push to shopify because no shops are set active.";
        }

        foreach ($this->getShopifyShops((int)$colMemberId) as $key => $shop) {
            return $return = $this->createOnSeperateShopsTEST($colMemberId, $shop);
            if($return !== true){
                return $shop['shopname']. ': ' . $return;
            }
        }

        //$this->updateStock($colMemberId);

        return true;
    }

    private function updateStock($colMemberId){
        $colMember = $this->_collectionMemberRepo->getCollectionMember($colMemberId);

        $variants = $this->_variantRepo->getAllVariantByArticleColor($colMember['articleid'], $colMember['articlecolorid'], 1);

        foreach ($variants as $key => $value) {
            $this->_inventoryRepository->createInventoryLine($value['id']);
        }
    }


    private function deleteOnSeperateShops($colMemberId, $shop){
        $colMember = $this->_collectionMemberRepo->getCollectionMember($colMemberId);
        $variant = $this->_variantRepo->getSingleVariantByArticleId($colMember['articleid'], $shop['shopid']);
        if($variant === false){
            return true;
        }

        $shopifyProductId = $variant['shopifyproductid'];
        $variants = $this->_variantRepo->getAllVariantByArticleColor($colMember['articleid'], $colMember['articlecolorid'], $shop['shopid']);

        return $this->_collectionMemberRepo->deleteProductInShopify($colMember, $variants, $shopifyProductId, $shop);

    }

    public function testCasper($colMemberId, $shop){
        // return $this->createOnSeperateShops($colMemberId, $shop);
    }

    private function createOnSeperateShops($colMemberId, $shop){

        $colMember = $this->_collectionMemberRepo->getCollectionMember($colMemberId);

        $variant = $this->_variantRepo->getSingleVariantByArticleId($colMember['articleid'], $shop['shopid']);

        if($variant === false){
            $shopifyProductId = '';
        }
        else{
            $shopifyProductId = $variant['shopifyproductid'];
        }

        $variants = $this->_variantRepo->getAllVariantByArticleColor($colMember['articleid'], $colMember['articlecolorid'], $shop['shopid']);


        if($shopifyProductId == ''){
            // Create new product
            return $this->_collectionMemberRepo->createNewProduct($colMember, $variants, $shop);
        }
        else{
            // update product
            return $this->_collectionMemberRepo->updateProduct($colMember, $variants, $shopifyProductId, $shop);
        }

        return true;
    }

    private function createOnSeperateShopsTEST($colMemberId, $shop){

        $colMember = $this->_collectionMemberRepo->getCollectionMember($colMemberId);

        $variant = $this->_variantRepo->getSingleVariantByArticleId($colMember['articleid'], $shop['shopid']);

        if($variant === false){
            $shopifyProductId = '';
        }
        else{
            $shopifyProductId = $variant['shopifyproductid'];
        }

        $variants = $this->_variantRepo->getAllVariantByArticleColor($colMember['articleid'], $colMember['articlecolorid'], $shop['shopid']);


        if($shopifyProductId == ''){
            // Create new product
            return 'new';
            //return $this->_collectionMemberRepo->createNewProduct($colMember, $variants, $shop);
        }
        else{
            // update product
            //return $variants;
            //return 'update';
            return $this->_collectionMemberRepo->updateProductTest($colMember, $variants, $shopifyProductId, $shop);
        }

        return true;
    }

    private function getShopifyShops($colId = 0){
        $conn = $this->_em->getConnection();
        if($colId === 0){
            $sql = sprintf(
                "
                    SELECT s.id shopid, s.Name as shopname, s.LanguageId as languageid, s.CurrencyId as currencyid
                    FROM (shopifyshops s) 
                    WHERE s.Active=1
                "
            );
        }
        else{
            $sql = sprintf(
                "
                    SELECT s.id shopid, s.Name as shopname, s.LanguageId as languageid, s.CurrencyId as currencyid
                    FROM (shopifyshops s, inshopifyshops iss) 
                    WHERE s.Active=1 AND iss.CollectionMemberId=%d AND iss.Active=1 AND iss.ShopifyShopId=s.Id
                ",
                (int)$colId
            );
        }

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $shops = $stmt->fetchAll();
    }


}
