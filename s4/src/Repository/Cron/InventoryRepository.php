<?php

namespace App\Repository\Cron;

use App\Entity\Cron\CronInventory;
use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Shopify\ShopifyvariantRepo;
use App\Repository\Stock\ItemRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CronInventory|null find($id, $lockMode = null, $lockVersion = null)
 * @method CronInventory|null findOneBy(array $criteria, array $orderBy = null)
 * @method CronInventory[]    findAll()
 * @method CronInventory[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class InventoryRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_shopifyUrl;
    private $_shopifyToken;
    private $_shopifyLocation;
    private $_sqlHelper;
    private $_shopifyvariantRepo;
    private $_itemRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        SqlhelperRepo $sqlHelper,
        ShopifyvariantRepo $shopifyvariantRepo,
        ItemRepository $itemRepository
    )
    {
        parent::__construct($registry, CronInventory::class);
        $this->_em = $em;
        $this->_sqlHelper = $sqlHelper;
        $this->_shopifyvariantRepo = $shopifyvariantRepo;
        $this->_itemRepository = $itemRepository;
    }

    public function transform(CronInventory $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
            'variantcodeid'     => (int) $item->getVariantcodeid(),
            'quantity'          => (int) $item->getQuantity(),
            'shopid'            => (int) $item->getShopifyShopId(),
        ];
    }

    private function setUrl($shop){

        if($shop['shopname'] == 'da'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_DA'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_DA'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_DA'];
            return true;
        }

        if($shop['shopname'] == 'eu'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_EU'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_EU'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_EU'];
            return true;
        }

        if($shop['shopname'] == 'no'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NO'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NO'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_NO'];
            return true;
        }

        if($shop['shopname'] == 'se'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_SE'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_SE'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_SE'];
            return true;
        }

        if($shop['shopname'] == 'am'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_AMBASSADOR'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_AMBASSADOR'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_AMBASSADOR'];
            return true;
        }

        if($shop['shopname'] == 'b2b'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_B2B'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_B2B'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_B2B'];
            return true;
        }

        if($shop['shopname'] == 'nl'){
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NL'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NL'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_NL'];
            return true;
        }

        return false;
    }

    public function getPickingContainer(){
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT *
                FROM parameter
                WHERE Mark='PickContainerId'
            "
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $parameter = $stmt->fetch();

        return (int)$parameter['Value'];
    }

    public function updateStockByChanged($variants, $userId, $comment = ''){

        $stockUpdated = $this->_itemRepository->updateStockByOffice($variants, $userId, $this->getPickingContainer(), $comment);
        foreach ($variants as $key => $variant) {
            $this->createInventoryLine($variant['variantcodeid']);
        }
        return $stockUpdated;
    }

    public function updateCountersForArticles($articles, $userId, $checkForZero){
        foreach ($articles as $key => $article) {
            $variantcodeid = $this->_sqlHelper->getVariantIdByArticle((int)$article['aId'], (int)$article['acId'], (int)$article['asId']);
            if((int)$variantcodeid === 0){
                continue;
            }
            if($checkForZero === true){
                $this->createInventoryLineAndCheckStock($variantcodeid, $article['qty']);
            }
            else{
                $this->createInventoryLine($variantcodeid);
            }
        }
        return true;
    }

    public function updateCountersForVariants($articles, $userId){
        foreach ($articles as $key => $article) {
            $variantcodeid = (int)$article['VariantCodeId'];
            if((int)$variantcodeid === 0){
                continue;
            }
            $this->createInventoryLine($variantcodeid);
        }
        return true;
    }

    public function getOrder($id){
        $order = $this->find($id);
        return $this->transform($order);
    }

    private function updateInventoryCount($variantcodeid, $quantity, $shopid = 0){
        foreach ($this->_sqlHelper->getShopifyShops($variantcodeid) as $key => $shop){

            $this->setUrl($shop);
            $shopifyVariant = $this->_shopifyvariantRepo->getShopifyVariant($variantcodeid, $shop['shopid']);
            if($shopifyVariant === false){
                continue;
            }

            $updateProduct = array(
                "inventory_item_id" => $shopifyVariant['inventoryitemid'],
                "location_id"       => $this->_shopifyLocation,
                "available"         => (int)$quantity
            );

            $ch = curl_init($this->_shopifyUrl.'inventory_levels/set.json');
            curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($updateProduct));
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: ".$this->_shopifyToken, "Content-Type: application/json"));
            curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

            $result = curl_exec($ch);
            if (curl_errno($ch)) {
                $error_msg = curl_error($ch);
                curl_close($ch);
                return $shop['shopname'].' '.$this->_shopifyUrl.' Update stock ('.$shopifyVariant['inventoryitemid'].') ' .$error_msg;
            }
            $info = curl_getinfo($ch);
            curl_close($ch);
            if((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201){
                return $shop['shopname'].' '.$this->_shopifyUrl.' Update stock ('.$shopifyVariant['inventoryitemid'].') ' .$result;
            }
        }
        return true;
    }

    public function testfunction(){

                $sql = sprintf("UPDATE cron_inventory SET Active=0, ModifyDate='%s' WHERE Id=1", date('Y-m-d H:i:s'));
                $conn = $this->_em->getConnection();
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            /*
            $variants = [];

            $sql = "SELECT group_concat(Id) as Ids, COUNT(Id) as count, MAX(Id) as MaxId, MIN(Id) as MinId
            FROM cron_inventory
            WHERE Active=1
            GROUP BY VariantCodeId
            ORDER BY VariantCodeId
            LIMIT 300";

            $conn = $this->_em->getConnection();

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $variantEntitys = $stmt->fetchAll();
            $lastEntries = array();
            foreach ($variantEntitys as $key => $value) {
                $entity = $this->find($value['MaxId']);
                $variant = $this->transform($entity);
                if(isset($lastEntries[$variant['variantcodeid']]) === false){
                    $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                    $lastEntries[$variant['variantcodeid']]['entity'] = $entity;
                    $lastEntries[$variant['variantcodeid']]['ids'] = $value['Ids'];
                }
                else{
                    if($lastEntries[$variant['variantcodeid']]['variant']['id'] < $variant['id']){
                        $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                        $lastEntries[$variant['variantcodeid']]['entity'] = $entity;
                        $lastEntries[$variant['variantcodeid']]['ids'] = $value['Ids'];
                    }
                }

            }

            foreach ($lastEntries as $key => $value) {
                $sql = sprintf("UPDATE cron_inventory SET Active=0, ModifyDate='%s' WHERE Id IN(%s)", $value['ids'],date('Y-m-d H:i:s'));
                $conn = $this->_em->getConnection();
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }
            */

            return 'test';




        $variants = [];
        $variantEntitys = $this->findBy(
            array(
                'active' => 1
            ),
            array(
                'variantcodeid' => 'ASC',
                'shopifyshopid' => 'ASC'
            )
        );

        $lastEntries = array();
        foreach ($variantEntitys as $key => $value) {
            $variant = $this->transform($value);
            if(isset($lastEntries[$variant['variantcodeid']]) === false){
                $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                $lastEntries[$variant['variantcodeid']]['entity'] = $value;
            }
            else{
                if($lastEntries[$variant['variantcodeid']]['variant']['id'] < $variant['id']){
                    $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                    $lastEntries[$variant['variantcodeid']]['entity'] = $value;
                }
            }

        }

        return $lastEntries;
    }

    private function getVariantsToTransfer(){
        $variants = [];

            $sql = "SELECT group_concat(Id) as Ids, COUNT(Id) as count, MAX(Id) as MaxId, MIN(Id) as MinId
            FROM cron_inventory
            WHERE Active=1
            GROUP BY VariantCodeId
            ORDER BY VariantCodeId
            LIMIT 300";

            $conn = $this->_em->getConnection();

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $variantEntitys = $stmt->fetchAll();
            $lastEntries = array();
            foreach ($variantEntitys as $key => $value) {
                $entity = $this->find($value['MaxId']);
                $variant = $this->transform($entity);
                if(isset($lastEntries[$variant['variantcodeid']]) === false){
                    $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                    $lastEntries[$variant['variantcodeid']]['entity'] = $entity;
                    $lastEntries[$variant['variantcodeid']]['ids'] = $value['Ids'];
                }
                else{
                    if($lastEntries[$variant['variantcodeid']]['variant']['id'] < $variant['id']){
                        $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                        $lastEntries[$variant['variantcodeid']]['entity'] = $entity;
                        $lastEntries[$variant['variantcodeid']]['ids'] = $value['Ids'];
                    }
                }

            }

            foreach ($lastEntries as $key => $value) {
                $sql = sprintf("UPDATE cron_inventory SET Active=0, ModifyDate='%s' WHERE Id IN(%s)", $value['ids'],date('Y-m-d H:i:s'));
                $conn = $this->_em->getConnection();
                $stmt = $conn->prepare($sql);
                $stmt->execute();
            }

            return $lastEntries;
    }

    public function getAllVariantsToExport(){
        $variants = [];
        $variantEntitys = $this->findBy(
            array(
                'active' => 1
            ),
            array(
                'variantcodeid' => 'ASC',
                'shopifyshopid' => 'ASC'
            ),
            300
        );

        $lastEntries = array();
        foreach ($variantEntitys as $key => $value) {
            $variant = $this->transform($value);
            if(isset($lastEntries[$variant['variantcodeid']]) === false){
                $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                $lastEntries[$variant['variantcodeid']]['entity'] = $value;
            }
            else{
                if($lastEntries[$variant['variantcodeid']]['variant']['id'] < $variant['id']){
                    $lastEntries[$variant['variantcodeid']]['variant'] = $variant;
                    $lastEntries[$variant['variantcodeid']]['entity'] = $value;
                }
            }

            $value->setActive(0);
            $value->setModifydate(new \DateTime());
            $this->_em->persist($value);
            $this->_em->flush();
        }

        //$lastEntries = $this->getVariantsToTransfer();

        foreach ($lastEntries as $key => $variant) {
            $return = $this->updateInventoryCount($variant['variant']['variantcodeid'], $variant['variant']['quantity'], $variant['variant']['shopid']);
            if($return !== true){
                $entity = $variant['entity'];
                $entity->setActive(1);
                $entity->setModifydate(new \DateTime());
                $entity->setError($return);
                $this->_em->persist($entity);
                $this->_em->flush();
            }
            else{
                $entity = $variant['entity'];
                $entity->setModifydate(new \DateTime());
                $entity->setError('done');
                $this->_em->persist($entity);
                $this->_em->flush();
            }
        }

        return $lastEntries;
    }

    public function getQtyByVariantcode($variantcodeId){
        return 4;
    }
    public function test($variantcodeId){
        return $this->_sqlHelper->getQtyOfVariant($variantcodeId);
    }

    private function createInventoryLineAndCheckStock($variantcodeid, $qty, $shopid = 0){
        if((int)$this->_sqlHelper->getQtyOfVariant($variantcodeId) - (int)$qty > 1){
            return true;
        }

        $inventory = new CronInventory;
        $inventory->setCreatedate(new \DateTime());
        $inventory->setCreateuserid(1);
        $inventory->setModifydate(new \DateTime());
        $inventory->setModifyuserid(1);
        $inventory->setActive(1);
        $inventory->setVariantcodeid($variantcodeId);
        $inventory->setQuantity($this->_sqlHelper->getQtyOfVariant($variantcodeId));
        $inventory->setShopifyshopid($shopid);

        $this->_em->persist($inventory);
        $this->_em->flush();

        return $inventory->getId();
    }

    public function createInventoryLine($variantcodeId, $shopid = 0){
        //$this->_sqlHelper->getQtyOfVariant($variantcodeId);
        // return $this->_sqlHelper->getQtyOfVariant($variantcodeId);
    	$inventory = new CronInventory;
        $inventory->setCreatedate(new \DateTime());
        $inventory->setCreateuserid(1);
		$inventory->setModifydate(new \DateTime());
        $inventory->setModifyuserid(1);
        $inventory->setActive(1);
        $inventory->setVariantcodeid($variantcodeId);
        $inventory->setQuantity($this->_sqlHelper->getQtyOfVariant($variantcodeId));
        $inventory->setShopifyshopid($shopid);

        $this->_em->persist($inventory);
        $this->_em->flush();

        return $inventory->getId();
    }


}
