<?php

namespace App\Repository\Cron;

use App\Entity\Cron\Cron;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronRepo extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Cron::class);
        $this->_em = $em;
    }

    public function transform(Cron $cron) {
        return [
            'id'            => (int) $cron->getId(),
            'start_date'	=> (string) ($cron->getStartDate() == null ? '' : $cron->getStartDate()->format('Y-m-d H:i:s')),
            'end_date'		=> (string) ($cron->getEndDate() == null ? '' : $cron->getEndDate()->format('Y-m-d H:i:s')),
            'done'     		=> (int) $cron->getDone(),
        ];
    }

    public function deleteOldStamps(){
        $cronJobsEntities = $this->findBy(array('Done' => 1), array('Id' => 'ASC'));
        foreach ($cronJobsEntities as $key => $value) {
            $cron = $this->transform($value);
            if((time() - strtotime($cron['end_date'])) > 5400){
                $this->_em->remove($value);
                $this->_em->flush();
            }
        }
        return true;
    }

    public function CreateCronTimeStamp(){

    	$cron = new Cron;
        $cron->setStartDate(new \DateTime());

        $this->_em->persist($cron);
        $this->_em->flush();

        return $cron->getId();
    }

    public function getCronTimeStamp(){
    	$cronJobsEntities = $this->findBy(array('Done' => 0));
    	return count($cronJobsEntities);
    }

    public function getCronTimeStampTime(){
        $cronJobsEntities = $this->findBy(array('Done' => 0));
        if(count($cronJobsEntities) > 0){
            foreach ($cronJobsEntities as $key => $value) {
                return strtotime($value->getStartDate()->format('Y-m-d H:i:s'));
            }
        }
        return 0;
    }

    public function updateTimeOverdue(){
        $cronJobsEntities = $this->findBy(array('Done' => 0));
        if(count($cronJobsEntities) > 0){
            foreach ($cronJobsEntities as $key => $value) {
                $value->setEndDate(new \DateTime());
                $value->setDone(1);

                $this->_em->persist($value);
                $this->_em->flush();
                return true;
            }
        }
        return true;
    }

    public function updateCronTimeStamp($id){
    	$cron = $this->find($id);
        $cron->setEndDate(new \DateTime());
        $cron->setDone(1);

        $this->_em->persist($cron);
        $this->_em->flush();

        return $cron->getId();
    }

}
