<?php

namespace App\Repository\Cron;

use App\Entity\Cron\CronInventoryLock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method CronInventoryLock|null find($id, $lockMode = null, $lockVersion = null)
 * @method CronInventoryLock|null findOneBy(array $criteria, array $orderBy = null)
 * @method CronInventoryLock[]    findAll()
 * @method CronInventoryLock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronInventoryLockRepo extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, CronInventoryLock::class);
        $this->_em = $em;
    }

    public function transform(CronInventoryLock $cron) {
        return [
            'id'            => (int) $cron->getId(),
            'start_date'	=> (string) ($cron->getStartDate() == null ? '' : $cron->getStartDate()->format('Y-m-d H:i:s')),
            'end_date'		=> (string) ($cron->getEndDate() == null ? '' : $cron->getEndDate()->format('Y-m-d H:i:s')),
            'running'       => (int) $cron->getRunning(),
        ];
    }

    public function CreateCronTimeStamp(){

    	$cron = $this->find(1);
        $cron->setStartDate(new \DateTime());
        $cron->setRunning(1);

        $this->_em->persist($cron);
        $this->_em->flush();

        return $cron->getId();
    }

    public function getCronTimeStamp(){
        $cron = $this->find(1);
        if((int)$cron->getRunning() === 1){
            $time = new \DateTime();

            if((strtotime($time->format('Y-m-d H:i:s')) - strtotime($cron->getStartDate()->format('Y-m-d H:i:s'))) > 1200){
                return false;
            }

            return true;
        }
        return false;
    }

    public function updateCronTimeStamp(){
    	$cron = $this->find(1);
        $cron->setEndDate(new \DateTime());
        $cron->setRunning(0);

        $this->_em->persist($cron);
        $this->_em->flush();

        return $cron->getId();
    }

}
