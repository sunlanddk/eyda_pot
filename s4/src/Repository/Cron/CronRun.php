<?php

namespace App\Repository\Cron;

use App\Entity\Cron\Cron;
use App\Repository\PimRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronRun extends ServiceEntityRepository
{
    protected $_em;
    private $_cronJobRepo;
    private $_cronRepo;
    private $_pimRepo;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        CronJobRepo $cronJobRepo,
        CronRepo $cronRepo,
        PimRepository $pimRepo
    )
    {
        parent::__construct($registry, Cron::class);
        $this->_em = $em;
        $this->_cronJobRepo = $cronJobRepo;
        $this->_cronRepo = $cronRepo;
        $this->_pimRepo = $pimRepo;
    }

    public function run() {
    	set_time_limit(600);
        if($this->_cronRepo->getCronTimeStamp() > 0){
            return false;
        }

        // Create stamp in db
        $cronTimeStamp = $this->_cronRepo->CreateCronTimeStamp();

        foreach ($this->_cronJobRepo->getCronJobs() as $key => $cronJob) {
            $return = $this->cronFunctions($cronJob);
        }

        $this->_cronRepo->updateCronTimeStamp($cronTimeStamp);
        $this->_cronRepo->deleteOldStamps();
        return true;
    }

    private function repoSwitcher($function){
        $repo = '';
        switch ($function) {
            case 'article':
            case 'getCronJobs':
                $repo = '_cronJobRepo';
                break;

            case 'publishProduct':
            case 'GetDescriptionFromShop':
                $repo = '_pimRepo';
                break;

            default:
                $repo = false;
                break;
        }

        return $repo;
    }

    private function cronFunctions($_cronJob){
        $repo = $this->repoSwitcher($_cronJob['cron_function']);
        if($repo === false){
            return false;
        }

        $function = $_cronJob['cron_function'];
        $value = explode(',', $_cronJob['cron_value']);


        $return = $this->$repo->$function($value);

        // return $return;
        if($return !== true){
            $this->_cronJobRepo->updateCronJob($_cronJob['id'], json_encode($return));
        }
        else{
            $this->_cronJobRepo->updateCronJob($_cronJob['id']);
        }

        return true;
    }
}
