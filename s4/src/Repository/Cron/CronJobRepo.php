<?php

namespace App\Repository\Cron;

use App\Entity\Cron\CronJobs;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CronJobRepo extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, CronJobs::class);
        $this->_em = $em;
    }

    public function transform(CronJobs $cronJob) {
        return [
            'id'                => (int) $cronJob->getId(),
            'type'              => (string) $cronJob->getType(),
            'internalid'        => (string) $cronJob->getInternalid(),
            'cron_name'        	=> (string) $cronJob->getCronName(),
            'cron_function'     => (string) $cronJob->getCronFunction(),
            'cron_value'     	=> (string) $cronJob->getCronValue(),
            'cron_scheduled_at'	=> (string) ($cronJob->getCronScheduledAt() == null ? '' : $cronJob->getCronScheduledAt()->format('Y-m-d H:i:s')),
            'cron_ran_at'      	=> (string) ($cronJob->getCronRanAt() == null ? '' : $cronJob->getCronRanAt()->format('Y-m-d H:i:s')),
            'error'      		=> (string) $cronJob->getError(),
            'done'      		=> (int) $cronJob->getDone(),
        ];
    }

    public function createCronJob($name, $function, $value, $type = 'default', $internalid = 0){

    	$cronJob = new CronJobs;
        $cronJob->setType($type);
        $cronJob->setInternalid($internalid);
    	$cronJob->setCronName($name);
    	$cronJob->setCronFunction($function);
    	$cronJob->setCronValue($value);
        $cronJob->setCronScheduledAt(new \DateTime());

        $this->_em->persist($cronJob);
        $this->_em->flush();

        return $cronJob->getId();
    }

    public function updateCronJob($id, $error = null){

    	$cronJob = $this->find($id);
    	$cronJob->setDone(1);
    	if($error !== null){
    		$cronJob->setError($error);
    	}
        $cronJob->setCronRanAt(new \DateTime());

        $this->_em->persist($cronJob);
        $this->_em->flush();

        return $cronJob->getId();
    }

    public function getCronJobs(){
    	$cronJobsEntities = $this->findBy(array('Done' => 0), null, 8);
    	$cronJobs = [];

    	foreach ($cronJobsEntities as $key => $entity) {
    		array_push($cronJobs, $this->transform($entity));
    	}

    	return $cronJobs;
    }

}
