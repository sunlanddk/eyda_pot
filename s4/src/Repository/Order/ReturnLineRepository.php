<?php

namespace App\Repository\Order;

use App\Entity\Order\ReturnLine;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReturnLine>
 *
 * @method ReturnLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnLine[]    findAll()
 * @method ReturnLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnLineRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnLine::class);
    }

    /**
     * @param  ReturnLine  $entity
     * @param  bool  $flush
     * @return void
     */
    public function save(ReturnLine $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
