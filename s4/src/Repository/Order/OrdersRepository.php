<?php

namespace App\Repository\Order;

use App\Entity\Order\Order;
use App\Repository\Article\VariantcodeRepository;
use App\Repository\Cron\InventoryRepository;
use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Stock\ContainerRepository;
use App\Repository\Stock\ItemRepository;
use App\Repository\Stock\ShipmentInvoiceRepository;
use App\Repository\Stock\TransactionRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrdersRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_orderRepository;
    private $_orderlineRepository;
    private $_pickorderRepository;
    private $_consolidatedorderRepository;
    private $_sqlHelper;
    private $_variantcodeRepository;
    private $_inventoryRepository;
    private $_itemRepository;
    private $_containerRepository;
    private $_shipmentInvoiceRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        OrderRepository $orderRepository,
        OrderlineRepository $orderlineRepository,
        PickorderRepository $pickorderRepository,
        ConsolidatedorderRepository $consolidatedorderRepository,
        VariantcodeRepository $variantcodeRepository,
        InventoryRepository $inventoryRepository,
        ItemRepository $itemRepository,
        TransactionRepository $transactionRepository,
        ContainerRepository $containerRepository,
        SqlhelperRepo $sqlHelper,
        ShipmentInvoiceRepository $shipmentInvoiceRepository
    ) {
        parent::__construct($registry, Order::class);
        $this->_em = $em;
        $this->_orderRepository = $orderRepository;
        $this->_pickorderRepository = $pickorderRepository;
        $this->_consolidatedorderRepository = $consolidatedorderRepository;
        $this->_sqlHelper = $sqlHelper;
        $this->_orderlineRepository = $orderlineRepository;
        $this->_variantcodeRepository = $variantcodeRepository;
        $this->_inventoryRepository = $inventoryRepository;
        $this->_itemRepository = $itemRepository;
        $this->_transactionRepository = $transactionRepository;
        $this->_containerRepository = $containerRepository;
        $this->_shipmentInvoiceRepository = $shipmentInvoiceRepository;
    }

    public function cancelOrder($post)
    {
        if ($this->_orderRepository->checkOrderReference($post['id']) === true) {
            return $this->_orderRepository->cancelOrder($post['id']);
        }

        return 'not in system';
    }

    public function refundSimpleItem($post)
    {
        $stockId = 0;
        $containerId = $this->getRefundContainer();
        $transactionId = 0;
        $totalQty = 1;
        $sku = $post['sku'];
        $variantcode = $this->_variantcodeRepository->getVariantCodeByGtin($sku);
        if ($variantcode !== false) {
            $transactionId = $this->_transactionRepository->createTransaction('refund', 0, $stockId, 14321, 0, 0);
            $this->_itemRepository->refundItem($variantcode, $transactionId, 1, $containerId, $stockId);
            $this->_inventoryRepository->createInventoryLine($variantcode['id']);
            $transactionId = $this->_transactionRepository->updateTransaction($transactionId, 0, 1);
        }

        return true;
    }

    public function getRefundContainer()
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT *
                FROM parameter
                WHERE Mark='ReturnContainer'
            "
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $parameter = $stmt->fetch();

        return (int)$parameter['Value'];
    }

    public function refundOrder($post, $new = false)
    {
        if ($new === false) {
            if ($this->findShipmentById($post['order_id']) !== false) {
                $stockId = $this->findShipmentById($post['order_id']);
            } else {
                $stockId = 0;
            }
            $containerId = $this->getRefundContainer();
            $transactionId = 0;
            $totalQty = 0;
            foreach ($post['refund_line_items'] as $rfKey => $refundLine) {
                if ($refundLine['restock_type'] !== 'no_restock') {
                    if (isset($refundLine['line_item']) === true) {
                        $sku = $refundLine['line_item']['sku'];
                        $variantcode = $this->_variantcodeRepository->getVariantCodeBySku($sku);
                        $collectionmember = $this->_variantcodeRepository->getCollectionmemberBySku($sku);
                        if ((int)$collectionmember['Cancel'] > 0) {
                            continue;
                        }
                        if ($variantcode !== false) {
                            if ($transactionId === 0) {
                                $transactionId = $this->_transactionRepository->createTransaction('refund', 0, $stockId, 14321, 0, 0);
                            }

                            $totalQty += (int)$refundLine['quantity'];
                            $this->_itemRepository->refundItem($variantcode, $transactionId, $refundLine['quantity'], $containerId, $stockId);
                            $this->_inventoryRepository->createInventoryLine($variantcode['id']);
                        }
                    }
                }
            }
            if ((int)$transactionId > 0) {
                $transactionId = $this->_transactionRepository->updateTransaction($transactionId, 0, $totalQty);
            }
        } else {
            if ($this->findShipmentById($post['orderid'], true) !== false) {
                $stockId = $this->findShipmentById($post['orderid'], true);
            } else {
                $stockId = 0;
            }

            $containerId = $this->getRefundContainer();
            $transactionId = 0;
            $totalQty = 0;
            foreach ($post['line_items'] as $rfKey => $refundLine) {
                if ((int)$refundLine['Restock'] === 1) {
                    $sku = $refundLine['VariantCode'];
                    $variantcode = $this->_variantcodeRepository->getVariantCodeByGtin($sku);
                    $collectionmember = $this->_variantcodeRepository->getCollectionmemberBySku($sku);
                    if ((int)$collectionmember['Cancel'] > 0) {
                        continue;
                    }
                    if ($variantcode !== false) {
                        if ($transactionId === 0) {
                            $transactionId = $this->_transactionRepository->createTransaction('refund', 0, $stockId, 14321, 0, 0);
                        }

                        $totalQty += (int)$refundLine['Scanned'];
                        $this->_itemRepository->refundItem($variantcode, $transactionId, $refundLine['Scanned'], $containerId, $stockId);
                        $this->_inventoryRepository->createInventoryLine($variantcode['id']);
                    }
                }
            }
            if ((int)$transactionId > 0) {
                $transactionId = $this->_transactionRepository->updateTransaction($transactionId, 0, $totalQty);
            }
        }

        return true;
    }

    private function findShipmentById($shopifyorderid, $new = false)
    {
        $conn = $this->_em->getConnection();
        if ($new === false) {
            $sql = sprintf(
                "
                    SELECT c.StockId
                    FROM (item i, container c, `order` o, `orderline` ol)
                    WHERE o.ShopifyId='%s' AND o.Active=1 AND 
                    ol.OrderId=o.Id AND ol.Active=1 AND 
                    i.OrderLineId=ol.Id AND i.Active=1 AND i.ContainerId > 0 AND 
                    c.Id=i.ContainerId
                ",
                $shopifyorderid
            );
        } else {
            $sql = sprintf(
                "
                    SELECT c.StockId
                    FROM (item i, container c, `order` o, `orderline` ol)
                    WHERE o.Id=%d AND o.Active=1 AND 
                    ol.OrderId=o.Id AND ol.Active=1 AND 
                    i.OrderLineId=ol.Id AND i.Active=1 AND i.ContainerId > 0 AND 
                    c.Id=i.ContainerId
                ",
                $shopifyorderid
            );
        }

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $orders = $stmt->fetchAll();

        foreach ($orders as $key => $value) {
            if ((int)$value['StockId'] > 0) {
                return (int)$value['StockId'];
            }
        }

        return false;
    }

    public function getOrder($id)
    {
        return $this->_orderRepository->getOrder($id);
    }

    public function updateOrder($post, $domain)
    {
        if ($this->_orderRepository->checkOrderReference($post['id']) === true) {
            return $this->_orderRepository->checkForFullfillmentOrder($post, $domain);
        }

        return 'not in system';
    }

    public function createOrderTest($post, $domain)
    {
        return $orderData = $this->convertPost($post, 1, $domain);
    }

    private function convertPost($post, $consolidatedId, $domain)
    {
        if ($this->checkPostedData($post) !== true) {
            return 'Data not accepted: ' . $this->checkPostedData($post);
        }
        $company = $this->_sqlHelper->getCompany(7779);
        $shop = $this->_sqlHelper->getShopId($domain);
        $order = array(
            'shopifyordernumber' => (string)$post['name'],
            'id' => $post['id'],
            'consolidatedid' => $consolidatedId,
            'companyid' => 7779, // webshopid
            'tocompanyid' => 787,
            'paymenttermid' => $company['PaymentTermId'],
            'deliverytermid' => $company['DeliveryTermId'],
            'carrierid' => $company['CarrierId'],
            'discount' => 0,
            'name' => $post['shipping_address']['first_name'] . ' ' . $post['shipping_address']['last_name'],
            'address1' => (string)$post['shipping_address']['address1'],
            'address2' => (string)$post['shipping_address']['address2'],
            'zip' => (string)$post['shipping_address']['zip'],
            'city' => (string)$post['shipping_address']['city'],
            'countryid' => 0,
            'packageshop' => '',
            'shopifytaxtotal' => $post['total_tax'],
            'shopifytotal' => $post['total_price'],
            'comment' => '',
            'email' => (string)$post['contact_email'],
            'shopifyshopid' => $shop['id'],
            'currencyid' => $shop['currencyid'],
            'languageid' => $shop['languageid'],
            'phone' => (string)$post['shipping_address']['phone'],
            'tax' => 0,
            'tags' => (string)$post['tags'],
            'billing' => array(
                'name' => $post['billing_address']['first_name'] . ' ' . $post['billing_address']['last_name'],
                'address1' => (string)$post['billing_address']['address1'],
                'address2' => (string)$post['billing_address']['address2'],
                'zip' => (string)$post['billing_address']['zip'],
                'city' => (string)$post['billing_address']['city'],
                'countryid' => 0,
                'packageshop' => '',
            ),
            'items' => []
        );

        foreach ($post['line_items'] as $key => $item) {
            if (strrpos(strtolower($item['sku']), 'gavekort') !== false || strrpos(strtolower($item['sku']), 'giftcard') !== false) {
                // $variant = $this->_sqlHelper->getVariantInformation($item['variant_id'] ,$shop['id']);
                $itemData = array(
                    'lineid' => $item['id'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                    'articleid' => (int)2,
                    'articlecolorid' => (int)0,
                    'articlesizeid' => (int)0,
                    'variantcode' => 'giftcard',
                    'variantcodeid' => 0,
                    'title' => $item['title']
                );
            }
            elseif ((string)$item['sku'] === '10181') {
                // $variant = $this->_sqlHelper->getVariantInformation($item['variant_id'] ,$shop['id']);
                $itemData = array(
                    'lineid' => $item['id'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                    'articleid' => (int)1080,
                    'articlecolorid' => (int)795,
                    'articlesizeid' => (int)1025,
                    'variantcode' => '5715232041060',
                    'variantcodeid' => 4107,
                    'title' => $item['title']
                );
            } 
            elseif ((string)$item['sku'] === '10220') {
                // $variant = $this->_sqlHelper->getVariantInformation($item['variant_id'] ,$shop['id']);
                $itemData = array(
                    'lineid' => $item['id'],
                    'price' => $item['price'],
                    'quantity' => $item['quantity'],
                    'articleid' => (int)1046,
                    'articlecolorid' => (int)660,
                    'articlesizeid' => (int)854,
                    'variantcode' => '5715232033782',
                    'variantcodeid' => 3379,
                    'title' => $item['title']
                );
            } 
            else {
                if ($item['variant_id'] === null) {
                    $discount = $this->getDiscounts($item);
                    $itemData = array(
                        'lineid' => $item['id'],
                        'OriginalPrice' => $item['price'],
                        'price' => $item['price'] - $discount,
                        'discount' => $discount,
                        'quantity' => $item['quantity'],
                        'articleid' => 4,
                        'articlecolorid' => 0,
                        'articlesizeid' => 0,
                        'variantcode' => 'customitem',
                        'variantcodeid' => 0,
                        'title' => $item['title']
                    );
                    if ($order['tax'] == 0) {
                        foreach ($item['tax_lines'] as $tkey => $taxline) {
                            $order['tax'] = (100 * $taxline['rate']);
                        }
                    }
                } else {
                    $variant = $this->_sqlHelper->getVariantInformation($item['variant_id'], $shop['id']);
                    if ($variant === false) {
                        return 'Data not accepted: Variant not found: ' . $item['variant_id'];
                    }
                    $discount = $this->getDiscounts($item);
                    $itemData = array(
                        'lineid' => $item['id'],
                        'OriginalPrice' => $item['price'],
                        'price' => $item['price'] - $discount,
                        'discount' => $discount,
                        'quantity' => $item['quantity'],
                        'articleid' => (int)$variant['ArticleId'],
                        'articlecolorid' => (int)$variant['ArticleColorId'],
                        'articlesizeid' => (int)$variant['ArticleSizeId'],
                        'variantcode' => $variant['VariantCode'],
                        'variantcodeid' => $variant['VariantCodeId'],
                        'title' => $item['title']
                    );
                    if ($order['tax'] == 0) {
                        foreach ($item['tax_lines'] as $tkey => $taxline) {
                            $order['tax'] = (100 * $taxline['rate']);
                        }
                    }
                }
            }

            array_push($order['items'], $itemData);
        }

        foreach ($post['shipping_lines'] as $key => $item) {
            $order['shipping'] = $item['price'];
            $order['shipping_title'] = utf8_decode($item['title']);
        }

        $order['countryid'] = (int)$this->_sqlHelper->getCountryId($post['shipping_address']['country_code'], $order['tax']);
        $order['billing']['countryid'] = (int)$this->_sqlHelper->getCountryId($post['billing_address']['country_code'], $order['tax']);

        return $order;
    }

    private function checkPostedData($post)
    {
        return true;
    }

    private function getDiscounts($item)
    {
        if (isset($item['discount_allocations']) === true) {
            $discount = 0;
            foreach ($item['discount_allocations'] as $key => $value) {
                $discount += $value['amount'];
            }
            return $discount;
        }
        return 0;
    }

    public function createOrder($post, $domain)
    {
        global $Pickorderlinecreated;
        //$Pickorderlinecreated = false;

        if ($this->_orderRepository->checkOrderReference($post['id']) === true) {
            return 'already in system';
        }

        $consolidatedId = $this->_consolidatedorderRepository->createConsolidatedorder();
        $orderData = $this->convertPost($post, $consolidatedId, $domain);

        if (getType($orderData) === 'string') {
            $path = 'C:\inetpub\wwwroot/eyda/webhook.txt';
            $myfile = fopen($path, "a");
            fwrite($myfile, json_encode($post) . "\n\r\n\r");
            fclose($myfile);
            return $orderData;
        }
        $orderId = $this->_orderRepository->createOrder($orderData);
        if (gettype($orderId) === 'integer') {
            $pickOrderId = $this->_pickorderRepository->createPickorder($orderData, $consolidatedId, $orderId, 'SalesOrder');
            $orderlines = $this->_orderlineRepository->createOrderlines($orderId, $pickOrderId, $orderData['items'], $orderData['tax']);
            if (isset($orderData['shipping']) === true) {
                $shipping = $this->_orderlineRepository->createShippingline(
                    $orderData['shipping'],
                    $orderData['shipping_title'],
                    (count($orderData['items']) + 1),
                    $orderId,
                    $orderData['tax']
                );
            }
            // if ($Pickorderlinecreated == true) {
            // $this->_pickorderRepository->setPickorderInActive($pickOrderId);
            // }
            return $orderId;
        }

        // Create error log
        return false;
        // return $pickOrderId = $this->_pickorderRepository->createPickorder(1,2,$orderId,$orderId,'SalesOrder',1);
    }

    public function checkForPickorders()
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT po.Id as poId, o.Id as oId
                FROM (pickorder po, `order` o)
                WHERE po.Active=0 AND o.WebshipperId=0 AND po.ReferenceId=o.Id AND o.Active=1 AND o.Done != 1
            "
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $orders = $stmt->fetchAll();
        $ids = [];
        foreach ($orders as $key => $order) {
            array_push($ids, $order['oId']);
        }
        if (count($ids) == 0) {
            return true;
        }

        $sql = sprintf(
            "UPDATE `order` SET WebshipperId=1 WHERE Id IN(%s)",
            implode(',', $ids)
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        foreach ($orders as $key => $order) {
            if ($this->_orderRepository->checkWebshipper($order['oId']) === true) {
                //$this->_pickorderRepository->setPickorderActive($order['poId']);
                $sqlpl = sprintf("SELECT COUNT(pl.Id) AS Test FROM pickorderline pl WHERE pl.pickOrderId = %d", (int)$order['poId']);
                $stmtpl = $conn->prepare($sqlpl);
                $stmtpl->execute();
                $test = $stmtpl->fetchAll();
                if ((int)$test[0]['Test'] > 0) {
                    $this->_pickorderRepository->setPickorderActive($order['poId']);
                }
            }
        }

        $this->checkOrders(implode(',', $ids));

        return $orders;
    }

    public function checkOrders($ids)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT *
                FROM (`order` o)
                WHERE o.Done != 1 AND o.Id IN(%s)
            ",
            $ids
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $orders = $stmt->fetchAll();

        foreach ($orders as $key => $value) {
            if ((int)$value['WebshipperId'] === 1) {
                $sql = sprintf(
                    "UPDATE `order` SET WebshipperId=0 WHERE Id=%d",
                    (int)$value['Id']
                );

                $single = $conn->prepare($sql);
                $single->execute();
            }
        }
    }

    //   public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
    //       $variant = $this->find($variantCodeId);
    //       $variant->setShopifyvariantid($shopifyVariantId);
    //       $variant->setShopifyproductid($shopifyProductId);
    //       $variant->setInventoryitemid($shopifyInventoryId);

    //       $this->_em->persist($variant);
    //       $this->_em->flush();

    //       return $variant->getId();
    //   }

}
