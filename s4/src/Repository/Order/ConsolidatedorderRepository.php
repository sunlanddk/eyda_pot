<?php

namespace App\Repository\Order;

use App\Entity\Order\Consolidatedorder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Consolidatedorder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Consolidatedorder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Consolidatedorder[]    findAll()
 * @method Consolidatedorder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ConsolidatedorderRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Consolidatedorder::class);
        $this->_em = $em;
    }

    public function transform(Consolidatedorder $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
        ];
    }

    public function getOrder($id){
        $order = $this->find($id);
        return $this->transform($order);
    }


    public function createConsolidatedorder(){
        $tz = new \DateTimeZone('Europe/London');
        $nowDate =  new \DateTime();
        $nowDate->setTimezone($tz);

    	$order = new Consolidatedorder;
        $order->setCreatedate($nowDate);
        $order->setCreateuserid(1);
		$order->setModifydate($nowDate);
        $order->setModifyuserid(1);
        $order->setActive(1);

        $this->_em->persist($order);
        $this->_em->flush();

        return $order->getId();

    }



}
