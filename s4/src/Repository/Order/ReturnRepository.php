<?php

namespace App\Repository\Order;

use App\Entity\Order\ReturnEntity;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<ReturnEntity>
 *
 * @method ReturnEntity|null find($id, $lockMode = null, $lockVersion = null)
 * @method ReturnEntity|null findOneBy(array $criteria, array $orderBy = null)
 * @method ReturnEntity[]    findAll()
 * @method ReturnEntity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ReturnRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, ReturnEntity::class);
    }

    /**
     * @param  ReturnEntity  $entity
     * @param  bool  $flush
     * @return void
     */
    public function save(ReturnEntity $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
