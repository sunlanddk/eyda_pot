<?php

namespace App\Repository\Order;

use App\Entity\Article\ArticleSize;
use App\Entity\Order\Orderline;
use App\Entity\Order\Orderquantity;
use App\Repository\Article\VariantcodeRepository;
use App\Repository\Cron\InventoryRepository;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Orderquantity|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orderquantity|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orderquantity[]    findAll()
 * @method Orderquantity[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderquantityRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_pickorderline;
    private $_inventoryRepository;
    private $_variantcodeRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        PickorderlineRepository $pickorderline,
        InventoryRepository $inventoryRepository,
        VariantcodeRepository $variantcodeRepository
    ) {
        parent::__construct($registry, Orderquantity::class);
        $this->_em = $em;
        $this->_pickorderline = $pickorderline;
        $this->_inventoryRepository = $inventoryRepository;
        $this->_variantcodeRepository = $variantcodeRepository;
    }

    public function transform(Orderquantity $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
        ];
    }

    public function createOrderquantity($lineId, $pickorderId, $item)
    {
        /** @var ArticleSize|null $articleSize */
        $articleSize = $this->_em->find(ArticleSize::class, $item['articlesizeid']);
        /** @var Orderline|null $orderLine */
        $orderLine = $this->_em->find(Orderline::class, $lineId);

        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);
        $oOrderQuantity = new Orderquantity();
        $oOrderQuantity->setOrderLine($orderLine);
        $oOrderQuantity->setCreatedate($nowDate);
        $oOrderQuantity->setModifydate($nowDate);
        $oOrderQuantity->setModifyuserid(1);
        $oOrderQuantity->setCreateuserid(1);
        $oOrderQuantity->setActive(true);
        $oOrderQuantity->setArticleSize($articleSize);
        $oOrderQuantity->setQuantity($item['quantity']);
        $oOrderQuantity->setShopifyId($item['lineid']);
        $oOrderQuantity->setWebshipperid(0);

        $this->_em->persist($oOrderQuantity);
        $this->_em->flush();

        $presale = false;
        $vc = $this->_variantcodeRepository->getVariantCodeByGtin($item['variantcode']);
        $presale = (bool)$vc['presale'];

        $conn = $this->_em->getConnection();
        $sql = sprintf("SELECT * FROM variantcode WHERE id = %d", $item['variantcodeid']);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $test = $stmt->fetchAll();
        if ((int)$test[0]['Presale'] == 0) {
            // || $presale !== true
            if ($item['variantcode'] !== 'giftcard') {
                $this->_pickorderline->createPickorderline($item, $pickorderId, $oOrderQuantity->getId());
                $this->_inventoryRepository->createInventoryLine($item['variantcodeid']);
            }
        }

        return true;
    }

    public function setWebshipperIdOnLines($lineIds, $webshipperLines)
    {
        foreach ($webshipperLines as $key => $webshipperline) {
            $orderquantity = $this->createQueryBuilder('o')
                ->andWhere('o.orderLine IN (:val)')
                ->andWhere('o.shopifyid = :val2')
                ->setParameter('val', $lineIds)
                ->setParameter('val2', $webshipperline['ext_ref'])
                ->getQuery()
                ->getOneOrNullResult();

            if ((bool)$orderquantity !== false) {
                $orderquantity->setWebshipperid($webshipperline['id']);
                $this->_em->persist($orderquantity);
                $this->_em->flush();
            }
        }

        return true;
    }

    public function createOrderquantityShipping($lineId)
    {
        /** @var Orderline|null $orderLine */
        $orderLine = $this->_em->find(Orderline::class, $lineId);
        /** @var ArticleSize $articleSize */
        $articleSize = $this->_em->find(ArticleSize::class, 0);

        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $oOrderQuantity = new Orderquantity();
        $oOrderQuantity->setOrderLine($orderLine);
        $oOrderQuantity->setCreatedate($nowDate);
        $oOrderQuantity->setModifydate($nowDate);
        $oOrderQuantity->setModifyuserid(1);
        $oOrderQuantity->setCreateuserid(1);
        $oOrderQuantity->setActive(true);
        $oOrderQuantity->setArticleSize($articleSize);
        $oOrderQuantity->setQuantity(1);
        $oOrderQuantity->setShopifyId(0);
        $oOrderQuantity->setWebshipperid(0);

        $this->_em->persist($oOrderQuantity);
        $this->_em->flush();
        return true;
    }
}
