<?php

namespace App\Repository\Order;

use App\Entity\Article\Article;
use App\Entity\Company\Company;
use App\Entity\Order\Order;
use App\Entity\Order\Orderline;
use App\Entity\Order\Pickorder;
use App\Repository\Stock\ShipmentInvoiceRepository;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_orderlineRepository;
    private $_orderaddressesRepository;
    private $_shipmentInvoiceRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        OrderlineRepository $orderlineRepository,
        OrderaddressesRepository $orderaddressesRepository,
        ShipmentInvoiceRepository $shipmentInvoiceRepository
    )
    {
        parent::__construct($registry, Order::class);
        $this->_em = $em;
        $this->_orderlineRepository = $orderlineRepository;
        $this->_orderaddressesRepository = $orderaddressesRepository;
        $this->_shipmentInvoiceRepository = $shipmentInvoiceRepository;
    }

    public function transform(Order $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
        ];
    }

    public function getOrder($id){
        $order = $this->find($id);
        return $this->transform($order);
    }

    public function tranformAll(Order $item){
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'            => (int) $item->getActive(),
            'done'            => (int) $item->getDone(),
        ];
    }

    public function checkOrderReference($shopifyId){
        $order = $this->findBy(array('shopifyid' => $shopifyId, 'active' => 1));
        if(count($order) === 0){
            return false;
        }

        return true;

    }

    private function getOrderByShopifyId($shopifyId){
        $order = $this->findOneBy(array('shopifyid' => $shopifyId, 'active' => 1));
        if($order === null){
            return false;
        }

        return $this->tranformAll($order);
    }

    private function deactivatePickorder($orderId){
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                UPDATE pickorder SET Active=0 WHERE ReferenceId=%d
            ",
            $orderId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return true;
    }

    public function cancelOrder($shopifyId){
        $order = $this->getOrderByShopifyId($shopifyId);
        if($order === false){
            return 'not found in system';
        }

        if((int)$order['done'] === 1){
            return 'Order already done.';
        }

        $tz = new \DateTimeZone('Europe/London');
        $nowDate =  new \DateTime();
        $nowDate->setTimezone($tz);

        $orderEntity = $this->find($order['id']);
        $orderEntity->setModifydate($nowDate)
            ->setModifyuserid(1)
            ->setDone(1)
            ->setOrderdoneid(9)
            ->setDonedate($nowDate)
            ->setDoneuserid(1);
        $this->_em->persist($orderEntity);
        $this->_em->flush();

        $this->deactivatePickorder($order['id']);

        return $this->_orderlineRepository->doneOrderLinesByIdCancel($order['id']);
    }

    private function checkFullFillmentForGiftcard($post, $order){
        $found = false;
        $count = 0;
        if(count($post['fulfillments']) > 1){
            return true;
        }
        else{
            if(isset($post['fulfillments'][0]['line_items']) === true){
                foreach ($post['fulfillments'][0]['line_items'] as $key => $item) {
                    if(strrpos(strtolower($item['sku']), 'gavekort') !== false || strrpos(strtolower($item['sku']), 'giftcard') !== false){
                        $found = true;
                        $this->_orderlineRepository->doneGiftcardLines($order['id']);
                    }
                }
            }
        }

        if($found === true){
            return false;
        }

        return true;
    }

    private function setCommentTags($orderId, $post){
        if(strrpos($post['tags'], 'reklamation') !== false){

            $orderEntity = $this->find($orderId);
            if(strrpos((string)$orderEntity->getComment(), 'reklamation') == false){
                $orderEntity->setComment('reklamation '.$orderEntity->getComment());
                $this->_em->persist($orderEntity);
                $this->_em->flush();
            }
        }
        if(strrpos($post['tags'], 'flashsale15') !== false){

            $orderEntity = $this->find($orderId);
            if(strrpos((string)$orderEntity->getComment(), 'flashsale15') == false){
                $orderEntity->setComment('flashsale15 '.$orderEntity->getComment());
                $this->_em->persist($orderEntity);
                $this->_em->flush();
            }
        }
        if(strrpos($post['tags'], 'claim') !== false){

            $orderEntity = $this->find($orderId);
            if(strrpos((string)$orderEntity->getComment(), 'claim') == false){
                $orderEntity->setComment('claim '.$orderEntity->getComment());
                $this->_em->persist($orderEntity);
                $this->_em->flush();
            }
        }
        if(strrpos($post['tags'], 'claims') !== false){

            $orderEntity = $this->find($orderId);
            if(strrpos((string)$orderEntity->getComment(), 'claims') == false){
                $orderEntity->setComment('claims '.$orderEntity->getComment());
                $this->_em->persist($orderEntity);
                $this->_em->flush();
            }
        }
        if(strrpos($post['tags'], 'manual refund') !== false){

            $orderEntity = $this->find($orderId);
            if(strrpos((string)$orderEntity->getComment(), 'manual refund') == false){
                $orderEntity->setComment('manual refund '.$orderEntity->getComment());
                $this->_em->persist($orderEntity);
                $this->_em->flush();
            }
        }
        if(strrpos($post['tags'], '30DAYS') !== false){

            $orderEntity = $this->find($orderId);
            if(strrpos((string)$orderEntity->getComment(), '30DAYS') == false){
                $orderEntity->setComment('30DAYS '.$orderEntity->getComment());
                $this->_em->persist($orderEntity);
                $this->_em->flush();
            }
        }
    }

    public function checkForFullfillmentOrder($post, $domain){
        $order = $this->getOrderByShopifyId($post['id']);

        if($order === false){
            return 'not found in system';
        }

        $this->setCommentTags($order['id'], $post);

        if((int)$order['done'] === 1){
            return 'Order already done.';
        }

        return 'Fullfillment are now done in PassOn -> Webshipper -> Shopify.';

        $tz = new \DateTimeZone('Europe/London');
        $nowDate =  new \DateTime();
        $nowDate->setTimezone($tz);

        if($post['fulfillment_status'] == 'partial'){
            // Fullfill order here
            if($this->checkFullFillmentForGiftcard($post, $order) === true){
                $orderEntity = $this->find($order['id']);
                $orderEntity->setModifydate($nowDate)
                    ->setModifyuserid(1)
                    ->setDone(1)
                    ->setOrderdoneid(5)
                    ->setDonedate($nowDate)
                    ->setDoneuserid(1);
                $this->_em->persist($orderEntity);
                $this->_em->flush();

                //return $this->createShipmentAndInvoice($order['id'], $post, $domain);
                $articles = $this->createShipmentAndInvoice($order['id'], $post, $domain);

                return $this->_orderlineRepository->doneOrderLinesById($order['id'], $articles);
            }
        }

        if($post['fulfillment_status'] == 'fulfilled'){
            // Fullfill order here
            $orderEntity = $this->find($order['id']);
            $orderEntity->setModifydate($nowDate)
                ->setModifyuserid(1)
                ->setDone(1)
                ->setOrderdoneid(5)
                ->setDonedate($nowDate)
                ->setDoneuserid(1);
            $this->_em->persist($orderEntity);
            $this->_em->flush();

            //return $this->createShipmentAndInvoice($order['id'], $post, $domain);
            $articles = $this->createShipmentAndInvoice($order['id'], $post, $domain);

            return $this->_orderlineRepository->doneOrderLinesById($order['id'], $articles);
        }

        return 'Not shipped yet.';
    }

    private function createShipmentAndInvoice($orderId, $post, $domain){
        return $this->_shipmentInvoiceRepository->createShipmentInvoiceFromPickorder($orderId, $post, $domain);
    }

    private function getCurrencyExchangeRate($currencyId){
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT c.Rate
                FROM (currency c) 
                WHERE c.Id=%d
            ",
            $currencyId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $return = $stmt->fetch();
        return (int)$return['Rate'];
    }

    public function checkWebshipper($id){
        $order = $this->find($id);
        $shopifyId = $order->getShopifyid();
        $webshipperId = 0;

        $ch = curl_init($_SERVER['WEBSHIPPER_URL'].'/orders?filter[ext_ref]='.$shopifyId);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer '.$_SERVER['WEBSHIPPER_TOKEN'] , 'Content-Type: application/vnd.api+json'));
        // curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($webshipper));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $result = curl_exec($ch);
        $resultObj = json_decode($result, true);

        if(isset($resultObj['data']) === true){
            foreach ($resultObj['data'] as $key => $webshipperrow) {
                if($webshipperrow['attributes']['ext_ref'] === $shopifyId){
                    $webshipperId = $webshipperrow['id'];
                    $order->setWebshipperid((int)$webshipperId);
                    /*if(isset($webshipperrow['attributes']['billing_address']['phone']) === true){
                        //$order->setPhone((string)$webshipperrow['attributes']['billing_address']['phone']);
                    }
                    if(isset($webshipperrow['attributes']['billing_address']['email']) === true){
                        //$order->setEmail((string)$webshipperrow['attributes']['billing_address']['email']);
                    }*/
                    if(isset($webshipperrow['attributes']['original_shipping']['shipping_name']) === true){
                        $order->setShippingshop(utf8_encode((string)$webshipperrow['attributes']['original_shipping']['shipping_name']));
                    }

                    $this->_em->persist($order);
                    $this->_em->flush();
                    return $this->_orderlineRepository->setWebshipperIdOnLines($id, $webshipperrow['attributes']['order_lines']);
                }
            }
        }

        return false;
    }

    public function createOrder($post){

        $langId = $post['languageid'];
        $currencyId = $post['currencyid'];
        $carrierId = $post['carrierid']; // skal sættes
        $deliveryTermId = $post['deliverytermid']; // skal sættes
        $paymentTermId = $post['paymenttermid']; // skal sættes
        $countryId = $post['countryid']; // skal sættes
        $seasonId = 1; // skal sættes

        $tz = new \DateTimeZone('Europe/London');
        $nowDate =  new \DateTime();
        $nowDate->setTimezone($tz);


        /** @var Company $company */
        $company = $this->_em->getRepository(Company::class)->find($post['companyid']);
        $order = new Order();
        $order->setConsolidatedid((isset($post['consolidatedid']) === true ? (int)$post['consolidatedid'] : 0))
            ->setCreatedate($nowDate)
            ->setCreateuserid(1)
            ->setModifydate($nowDate)
            ->setModifyuserid(1)
            ->setActive(true)
            ->setDescription('Shopify Order: '.$post['shopifyordernumber'])
            ->setCompany($company)
            ->setLanguageid((int)$langId)
            ->setCarrierid(0)
            ->setDeliverytermid((int)$deliveryTermId)
            ->setPaymenttermid((int)$paymentTermId)
            ->setCurrencyid((int)$currencyId)
            ->setSalesuserid(1)
            ->setDiscount((isset($post['discount']) === true ? (int)$post['discount'] : 0))
            ->setReference($post['shopifyordernumber'])
            ->setExchangerate($this->getCurrencyExchangeRate($currencyId))
            ->setAltcompanyname(utf8_decode($post['name']))
            ->setAddress1(utf8_decode($post['address1']))
            ->setAddress2(utf8_decode($post['address2']))
            ->setZip($post['zip'])
            ->setCity(utf8_decode($post['city']))
            ->setCountryid($countryId)
            ->setReady(1)
            ->setReadyuserid(1)
            ->setReadydate($nowDate)
            ->setDone(0)
            ->setDoneuserid(0)
            ->setDonedate($nowDate)
            ->setOrderdoneid(0)
            ->setDraft(false)
            ->setTocompanyid($post['tocompanyid'])
            ->setInstructions((isset($post['comment']) === true ? (string)$post['comment'] : ''))
            ->setEmail($post['email'])
            ->setShippingShop('') // ?? Hvad er dette ?
            ->setOrdertypeid(10)
            ->setReceiveddate($nowDate)
            ->setSeasonid($seasonId)
            ->setVariantcodes(0)
            ->setWebshopno((int)$post['shopifyshopid'])
            ->setProposal(0)
            ->setPhone($post['phone'])
            ->setWebshipperid(0)
            ->setShopifyid($post['id'])
            ->setShopifyTotal($post['shopifytotal'])
            ->setShopifyTaxTotal($post['shopifytaxtotal'])
        ;

        try {
            $this->_em->persist($order);
            $this->_em->flush();
        } catch (Exception $e) {
            return $e;
        }

        $this->_orderaddressesRepository->createOrderaddress($order->getId(), $post);
        $this->_orderaddressesRepository->createOrderaddress($order->getId(), $post['billing'], 'invoice');

        $this->setCommentTags($order->getId(), $post);

        return $order->getId();
    }

    //   public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
    //       $variant = $this->find($variantCodeId);
    //       $variant->setShopifyvariantid($shopifyVariantId);
    //       $variant->setShopifyproductid($shopifyProductId);
    //       $variant->setInventoryitemid($shopifyInventoryId);

    //       $this->_em->persist($variant);
    //       $this->_em->flush();

    //       return $variant->getId();
    //   }

    /**
     * @param string $reference
     * @param string $email
     * @param string|null $postcode
     * @param string|null $createdAfterDate
     * @return Order|null
     */
    public function findOneForReturn(
        string $reference,
        string $email,
        ?string $postcode = null,
        string $createdAfterDate = null
    ): ?Order {
        $postcode = !empty($postcode) ? $postcode : null;

        $queryBuilder = $this->createQueryBuilder('o')
            ->select(
                'o',
                'orderLines',
                'orderQuantity'
            )
            ->innerJoin(
                'o.orderLines',
                'orderLines',
                'WITH',
                'orderLines.active = :active AND orderLines.article != :exceptShippingLineOne AND orderLines.article != :exceptShippingLineTwo'
            )
            ->leftJoin(
                'orderLines.article',
                'article',
                'WITH',
                'article.active = :active AND article.articletypeid != :articleTypeId'
            )
            ->leftJoin(
                'orderLines.orderQuantity',
                'orderQuantity',
                'WITH',
                'orderQuantity.active = :active'
            )
            ->leftJoin(
                'orderQuantity.pickOrderLines',
                'pickOrderLines',
                'WITH',
                'pickOrderLines.active = :active AND pickOrderLines.packedquantity > 0'
            )
            ->leftJoin(
                Pickorder::class,
                'pickOrder',
                'WITH',
                'pickOrder.active = :active AND pickOrderLines.pickorderid = pickOrder.id'
            )
            ->where('o.reference = :reference')
            ->andWhere('o.email = :email')
            ->andWhere('o.active = :active')
            ->setParameters([
                'reference' => $reference,
                'email' => $email,
                'active' => true,
                'articleTypeId' => Article::TYPE_SERVICE,
                'exceptShippingLineOne' => Orderline::SHIPPING_LINE_ONE,
                'exceptShippingLineTwo' => Orderline::SHIPPING_LINE_TWO,
            ]);

        if ($postcode !== null) {
            $queryBuilder->andWhere('o.zip = :zip')
                ->setParameter('zip', $postcode);
        }

        if ($createdAfterDate !== null) {
            $queryBuilder->andWhere('pickOrder.packeddate > :createDate')
                ->setParameter('createDate', new DateTime($createdAfterDate));
        }

        return $queryBuilder->getQuery()->getOneOrNullResult();
    }
}
