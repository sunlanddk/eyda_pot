<?php

namespace App\Repository\Order;

use App\Entity\Order\Orderadresses;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Exception;

/**
 * @method Orderadresses|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orderadresses|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orderadresses[]    findAll()
 * @method Orderadresses[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderaddressesRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    ) {
        parent::__construct($registry, Orderadresses::class);
        $this->_em = $em;
    }

    public function transform(Orderadresses $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
        ];
    }

    /**
     * @param  int  $orderId
     * @param  array  $post
     * @param  string  $type
     * @return Exception|int|null
     */
    public function createOrderaddress(int $orderId, array $post, string $type = 'shipping')
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $order = new Orderadresses();
        $order->setCreatedate($nowDate)
            ->setCreateuserid(1)
            ->setModifydate($nowDate)
            ->setModifyuserid(1)
            ->setActive(true)
            ->setOrderid($orderId)
            ->setPackageshop($post['packageshop'])
            ->setType($type)
            ->setAltcompanyname(utf8_decode($post['name']))
            ->setAddress1(utf8_decode($post['address1']))
            ->setAddress2(utf8_decode($post['address2']))
            ->setZip($post['zip'])
            ->setCity(utf8_decode($post['city']))
            ->setCountryid($post['countryid']);

        try {
            $this->_em->persist($order);
            $this->_em->flush();
        } catch (Exception $e) {
            return $e;
        }

        return $order->getId();
    }

    /**
     * @param  int  $orderId
     * @param  string  $type
     * @return Orderadresses|null
     */
    public function findByOrderIdAndType(int $orderId, string $type): ?Orderadresses
    {
        return $this->createQueryBuilder('o')
            ->andWhere('o.orderid = :orderId')
            ->andWhere('o.type = :type')
            ->andWhere('o.active = 1')
            ->setParameter('orderId', $orderId)
            ->setParameter('type', $type)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
