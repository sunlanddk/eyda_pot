<?php

namespace App\Repository\Order;

use App\Entity\Order\RefundLine;
use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Stock\ItemRepository;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method RefundLine|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefundLine|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefundLine[]    findAll()
 * @method RefundLine[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefundLineRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_orderquantityRepository;
    private $_itemRepository;
    private $_sqlHelper;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        OrderquantityRepository $orderquantityRepository,
        ItemRepository $itemRepository,
        SqlhelperRepo $sqlhelperRepo
    ) {
        parent::__construct($registry, RefundLine::class);
        $this->_em = $em;
        $this->_orderquantityRepository = $orderquantityRepository;
        $this->_itemRepository = $itemRepository;
        $this->_sqlHelper = $sqlhelperRepo;
    }

    public function transform(RefundLine $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
            'refundid' => (int)$item->getRefundid(),
            'orderquantityid' => (int)$item->getOrderquantityid(),
            'reasonid' => (int)$item->getReasonid(),
            'quantity' => (int)$item->getQuantity(),
        ];
    }

    public function refundInShopify($id)
    {
        return $id;
    }


    public function createRefundLine($userid, $refundId, $post, $ws)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $refund = new RefundLine;
        $refund->setCreateuserid((int)$userid);
        $refund->setModifyuserid((int)$userid);
        $refund->setCreatedate($nowDate);
        $refund->setModifydate($nowDate);

        $refund->setOrderquantityid((int)$post['OrderQuantityId']);
        $refund->setQuantity((int)$post['Scanned']);
        $refund->setRefundId((int)$refundId);
        $refund->setReasonid((int)$post['ReasonId']);

        $this->_em->persist($refund);
        $this->_em->flush();

        return $this->_sqlHelper->createRefundPositionLabel($refundId, $refund->getId(), (int)$post['VariantCodeId'], (int)$post['Scanned'], $userid, $ws);
    }


}
