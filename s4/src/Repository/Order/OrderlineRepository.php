<?php

namespace App\Repository\Order;

use App\Entity\Article\Article;
use App\Entity\Article\ArticleColor;
use App\Entity\Order\Order;
use App\Entity\Order\Orderline;
use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Stock\ItemRepository;
use App\Repository\Stock\TransactionRepository;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Orderline|null find($id, $lockMode = null, $lockVersion = null)
 * @method Orderline|null findOneBy(array $criteria, array $orderBy = null)
 * @method Orderline[]    findAll()
 * @method Orderline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class OrderlineRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_orderquantityRepository;
    private $_itemRepository;
    private $_transactionRepository;
    private $_sqlHelper;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        OrderquantityRepository $orderquantityRepository,
        ItemRepository $itemRepository,
        TransactionRepository $transactionRepository,
        SqlhelperRepo $sqlhelperRepo
    ) {
        parent::__construct($registry, Orderline::class);
        $this->_em = $em;
        $this->_orderquantityRepository = $orderquantityRepository;
        $this->_itemRepository = $itemRepository;
        $this->_transactionRepository = $transactionRepository;
        $this->_sqlHelper = $sqlhelperRepo;
    }

    public function transform(Orderline $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
        ];
    }

    // public function getOrder($id){
    //     $order = $this->find($id);
    //     return $this->transform($order);
    // }

    public function doneOrderLinesByIdCancel($orderId)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);


        $orderlines = $this->findBy(array('active' => 1, 'order' => $orderId));
        foreach ($orderlines as $key => $orderline) {
            $orderline->setModifydate($nowDate)
                ->setModifyuserid(1)
                ->setDone(1)
                ->setDonedate($nowDate)
                ->setDoneuserid(1);

            $this->_em->persist($orderline);
            $this->_em->flush();
        }
        return true;
    }


    public function doneGiftcardLines($orderId)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $orderlines = $this->findBy(array('active' => 1, 'order' => $orderId, 'done' => 0));
        foreach ($orderlines as $key => $orderline) {
            if ($orderline->getArticle() && (int)$orderline->getArticle()->getId() === 2) {
                $orderline->setModifydate($nowDate)
                    ->setModifyuserid(1)
                    ->setDone(1)
                    ->setDonedate($nowDate)
                    ->setDoneuserid(1);

                $this->_em->persist($orderline);
                $this->_em->flush();
            }
        }

        return true;
    }

    public function doneOrderLinesById($orderId, $articles)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $orderlines = $this->findBy(array('active' => 1, 'order' => $orderId));
        // $transactionId = $this->_transactionRepository->createTransaction('Ship', $orderId, 14321, 0);
        $totalQty = 0;
        $totalValue = 0;
        foreach ($orderlines as $key => $orderline) {
            $found = false;
            $orderline->setModifydate($nowDate)
                ->setModifyuserid(1)
                ->setDone(1)
                ->setDonedate($nowDate)
                ->setDoneuserid(1);

            $this->_em->persist($orderline);
            $this->_em->flush();
            foreach ($articles as $key => $article) {
                if ((int)$orderline->getId() === (int)$article['orderlineid']) {
                    $found = (int)$article['qty'];
                }
            }

            $lineQuantities = $this->_sqlHelper->getOrderLineQuantities($orderline->getId());
            /*if($found !== false && (int)$orderline->getQuantity() > (int)$found){
                // inventory remaining on order
                foreach ($lineQuantities as $qkey => $orderquantity) {
                    $inventory = $this->_itemRepository->inventoryItem(
                        $orderline->getId(),
                        (int)$orderquantity['Id'],
                        $orderline->getArticleid(),
                        $orderline->getArticleColor()->getId(),
                        (int)$orderquantity['ArticleSizeId'],
                        (int)$orderquantity['Quantity'] - $found,
                        $transactionId
                    );
                    $totalQty += $inventory[0];
                    $totalValue += $inventory[1];
                }
            }
            else if($found === false){
                // inventory all on orderline
                foreach ($lineQuantities as $qkey => $orderquantity) {
                    $inventory = $this->_itemRepository->inventoryItem(
                        $orderline->getId(),
                        (int)$orderquantity['Id'],
                        $orderline->getArticleid(),
                        $orderline->getArticleColor()->getId(),
                        (int)$orderquantity['ArticleSizeId'],
                        (int)$orderquantity['Quantity'],
                        $transactionId
                    );
                    $totalQty += $inventory[0];
                    $totalValue += $inventory[1];
                }
            }
            else{
                continue;
            }*/
        }

        // $transactionId = $this->_transactionRepository->updateTransaction($transactionId, $totalValue, $totalQty);

        return true;
    }

    public function createShippingline($shipping, $type, $count, $orderId, $vat)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        /** @var Article $article */
        $article = $this->_em->find(Article::class, 1);
        /** @var Order $article */
        $order = $this->_em->find(Order::class, $orderId);
        /** @var ArticleColor $articleColor */
        $articleColor = $this->_em->find(ArticleColor::class, 0);

        $oOrderline = new Orderline();
        $oOrderline->setCreatedate($nowDate)
            ->setCreateuserid(1)
            ->setModifydate($nowDate)
            ->setModifyuserid(1)
            ->setDone(0)
            ->setDoneuserid(0)
            ->setDonedate(new DateTime('0000-00-00 00:00:00'))
            ->setActive(true)
            ->setNo($count)
            ->setDescription($type)
            ->setOrder($order)
            ->setArticle($article)
            ->setArticleColor($articleColor)
            ->setQuantity(1)
            ->setPricesale((float)($shipping * (100 / (100 + $vat))))
            ->setDeliverydate($nowDate);

        $this->_em->persist($oOrderline);
        $this->_em->flush();

        $this->_orderquantityRepository->createOrderquantityShipping($oOrderline->getId());
    }

    public function setWebshipperIdOnLines($orderId, $webshipperLines)
    {
        // return $webshipperLines;
        $orderlines = $this->findBy(array('active' => 1, 'order' => $orderId));
        $ids = [];
        foreach ($orderlines as $key => $value) {
            array_push($ids, $value->getId());
        }

        return $this->_orderquantityRepository->setWebshipperIdOnLines($ids, $webshipperLines);
    }


    public function createOrderlines($orderId, $pickorderId, $items, $vat = 20)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        /** @var Order|null $article */
        $order = $this->_em->find(Order::class, $orderId);

        foreach ($items as $key => $item) {
            /** @var Article|null $article */
            $article = $this->_em->find(Article::class, $item['articleid']);
            /** @var ArticleColor|null $articleColor */
            $articleColor = $this->_em->find(ArticleColor::class, $item['articlecolorid']);

            $oOrderline = new Orderline();
            $oOrderline->setCreatedate($nowDate)
                ->setCreateuserid(1)
                ->setModifydate($nowDate)
                ->setModifyuserid(1)
                ->setDone(0)
                ->setDoneuserid(0)
                ->setDonedate(new DateTime('0000-00-00 00:00:00'))
                ->setActive(true)
                ->setNo(($key + 1))
                ->setDescription($item['title'])
                ->setOrder($order)
                ->setArticle($article)
                ->setArticleColor($articleColor)
                ->setQuantity($item['quantity'])
                ->setPricesale((float)($item['price'] * (100 / (100 + $vat))))
                ->setDeliverydate($nowDate);

            $this->_em->persist($oOrderline);
            $this->_em->flush();

            $this->_orderquantityRepository->createOrderquantity($oOrderline->getId(), $pickorderId, $item);
        }
    }



    //   public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
    //       $variant = $this->find($variantCodeId);
    //       $variant->setShopifyvariantid($shopifyVariantId);
    //       $variant->setShopifyproductid($shopifyProductId);
    //       $variant->setInventoryitemid($shopifyInventoryId);

    //       $this->_em->persist($variant);
    //       $this->_em->flush();

    //       return $variant->getId();
    //   }

}
