<?php

namespace App\Repository\Order;

use App\Entity\Order\Refund;
use App\Repository\HelperFunctions\SqlhelperRepo;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Refund|null find($id, $lockMode = null, $lockVersion = null)
 * @method Refund|null findOneBy(array $criteria, array $orderBy = null)
 * @method Refund[]    findAll()
 * @method Refund[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefundRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_shopifyUrl;
    private $_shopifyToken;
    private $_shopifyLocation;
    private $_logger;
    private $_orderRepository;
    private $_refundlineRepository;
    private $_sqlHelper;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        OrdersRepository $orderRepository,
        RefundLineRepository $refundlineRepository,
        SqlhelperRepo $sqlhelperRepo
    ) {
        parent::__construct($registry, Refund::class);
        $this->_em = $em;
        $this->_refundlineRepository = $refundlineRepository;
        $this->_orderRepository = $orderRepository;
        $this->_sqlHelper = $sqlhelperRepo;
    }

    public function transform(Refund $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
            'orderid' => (int)$item->getOrderid(),
            'shopifyrefundid' => (string)$item->getShopifyrefundid(),
            'doneuserid' => (int)$item->getDone(),
            'donedate' => (string)$item->getDonedate()->format('Y-m-d H:i:s'),
            'doneuserid' => (int)$item->getDoneuserid(),
            'doneuserid' => (int)$item->getReady(),
            'readydate' => (string)$item->getReadydate()->format('Y-m-d H:i:s'),
            'readyuserid' => (int)$item->getReadyuserid(),
        ];
    }

    public function refundSpecificAmount($data, $userId)
    {
        $this->setUrl($data);
        $salesTransaction = $this->getSalesTransactionFromShopify($data['shopifyorderid']);

        if (isset($salesTransaction['id']) !== true) {
            return $salesTransaction;
        }

        $return = $this->createRefundOnAmount($data, $salesTransaction, $data['shopifyorderid'], $userId);

        if ($return !== true) {
            return $return;
        }

        return true;
    }

    private function setUrl($shop)
    {
        if ($shop['shopname'] == 'da') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_DA'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_DA'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_DA'];
            return true;
        }

        if ($shop['shopname'] == 'eu') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_EU'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_EU'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_EU'];
            return true;
        }

        if ($shop['shopname'] == 'no') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NO'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NO'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_NO'];
            return true;
        }

        if ($shop['shopname'] == 'se') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_SE'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_SE'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_SE'];
            return true;
        }

        if ($shop['shopname'] == 'am') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_AMBASSADOR'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_AMBASSADOR'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_AMBASSADOR'];
            return true;
        }

        if ($shop['shopname'] == 'b2b') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_B2B'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_B2B'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_B2B'];
            return true;
        }

        if ($shop['shopname'] == 'nl') {
            $this->_shopifyUrl = $_SERVER['SHOPIFY_URL_NL'];
            $this->_shopifyToken = $_SERVER['SHOPIFY_TOKEN_NL'];
            $this->_shopifyLocation = $_SERVER['SHOPIFY_LOCATION_NL'];
            return true;
        }

        return false;
    }

    private function getSalesTransactionFromShopify($_shopifyOrderId)
    {
        $ch = curl_init(sprintf($this->_shopifyUrl . "orders/%s/transactions.json", $_shopifyOrderId));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }

        $info = curl_getinfo($ch);
        curl_close($ch);
        if ((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201) {
            $resultObj = json_decode($result, true);
            if (isset($resultObj['errors']) === true) {
                return json_encode($resultObj['errors']);
            }
            return json_encode($result);
        }

        $result = json_decode($result, true);

        if (isset($result['transactions']) === true) {
            foreach ($result['transactions'] as $key => $value) {
                if ($value['kind'] === 'capture' && $value['status'] === 'success') {
                    return $value;
                }
            }
        }

        return 'Error no transaction found on order.';
    }

    private function createRefundOnAmount($_data, $_transaction, $_shopifyOrderId, $_userId)
    {
        $refundData = array(
            "refund" => array(
                "notify" => (bool)$_data['notify'],
                "note" => $_data['note'],
                "transactions" => [
                    array(
                        "parent_id" => $_transaction['id'],
                        "amount" => $_data['amount'],
                        "kind" => "refund",
                        "gateway" => $_transaction['gateway']
                    )
                ]
            )
        );

        $ch = curl_init(sprintf($this->_shopifyUrl . "orders/%s/refunds.json", $_shopifyOrderId));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($refundData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }

        $info = curl_getinfo($ch);
        curl_close($ch);
        if ((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201) {
            $resultObj = json_decode($result, true);
            if (isset($resultObj['errors']) === true) {
                return json_encode($resultObj['errors']);
            }
            return json_encode($result);
        }

        $result = json_decode($result, true);

        if (isset($result['refund']['id']) === true) {
            $this->addRefundAmountRecord($_data['orderid'], $_data['amount'], $_data['note'], $result['refund']['id'], $_userId);
            return true;
        }

        return $result;
    }

    private function addRefundAmountRecord($orderid, $amount, $note, $_shopifyRefundId, $_userId)
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $refund = new Refund;
        $refund->setCreateuserid((int)$_userId);
        $refund->setCreatedate($nowDate);
        $refund->setModifyuserid((int)$_userId);
        $refund->setModifydate($nowDate);
        $refund->setReadyuserid((int)$_userId);
        $refund->setReadydate($nowDate);
        $refund->setReady(1);
        $refund->setDone(1);
        $refund->setDonedate($nowDate);
        $refund->setDoneuserid((int)$_userId);
        $refund->setOrderid((int)$orderid);
        $refund->setShopifyrefundid($_shopifyRefundId);
        $refund->setType('amount');
        $refund->setNote($note);
        $refund->setAmount($amount);

        $this->_em->persist($refund);
        $this->_em->flush();
    }

    public function simpleReturnItem($post)
    {
        $variantCodeId = $this->getVariantCodeIdBySku($post['sku']);
        if ($variantCodeId === false) {
            return 'GTIN was not found in system.';
        }

        $value = array(
            'OrderQuantityId' => 0,
            'Scanned' => 1,
            'VariantCodeId' => $variantCodeId,
            'ReasonId' => 0,
        );

        // return $value;
        $this->_refundlineRepository->createRefundLine((int)$post['userid'], 0, $value, $post['ws']);

        return $this->_orderRepository->refundSimpleItem($post);
    }

    private function getVariantCodeIdBySku($sku)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
               SELECT * FROM variantcode WHERE VariantCode='%s' AND Active=1
            ",
            $sku
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $lineitems = $stmt->fetch();

        if (isset($lineitems['Id']) === true) {
            return (int)$lineitems['Id'];
        }
        return false;
    }

    /**
     * @param array $post
     * @param int $headerUserId
     * @return Refund
     */
    public function createPassonRefund(array $post, int $headerUserId): Refund
    {
        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $refund = new Refund;
        $refund->setCreateuserid((int)$post['userid']);
        $refund->setCreatedate($nowDate);
        $refund->setModifyuserid((int)$post['userid']);
        $refund->setModifydate($nowDate);
        $refund->setReadyuserid((int)$post['userid']);
        $refund->setReadydate($nowDate);
        $refund->setReady(1);
        $refund->setOrderid((int)$post['orderid']);
        $refund->setType('full');
        $refund->setAmount(0);
        $refund->setNote('');

        $this->_em->persist($refund);
        $this->_em->flush();

        foreach ($post['line_items'] as $key => $value) {
            $this->_refundlineRepository->createRefundLine((int)$post['userid'], $refund->getId(), $value, $post['ws']);
        }

        $this->_orderRepository->refundOrder($post, true);

        return $refund;
    }

    public function refundInShopifyCron($cronValue)
    {
        $id = (int)$cronValue[0];
        $userid = (int)$cronValue[1];

        return $this->refundInShopify($id, $userid);
    }

    public function refundInShopify($id, $userid)
    {
        $orderId = (int)$this->_sqlHelper->tableGetField('refund', 'OrderId', $id);
        $shopifyOrderId = (string)$this->_sqlHelper->tableGetField('`order`', 'ShopifyId', $orderId);
        $shop = $this->_sqlHelper->getShopInfo((int)$this->_sqlHelper->tableGetField('`order`', 'WebShopNo', $orderId));

        $this->setUrl(array('shopname' => $shop['name']));

        if ($orderId === 0) {
            return 'Order not found.';
        }
        if ($shopifyOrderId === '') {
            return 'Shopify order not found.';
        }
        if ((int)$this->_sqlHelper->tableGetField('refund', 'Done', $id) === 1) {
            return 'Refund already done';
        }

        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT SUM(rl.Quantity) as quantity, oq.ShopifyId as line_item_id, rr.Name as name, rr.Restock as restock, 'restock_type' as no_restock
				FROM (refundline rl, orderquantity oq, refundreason rr)
				WHERE rl.Active=1 AND rl.RefundId=%d AND oq.Id=rl.OrderQuantityId AND rr.Id=rl.ReasonId AND rl.Refund=1 GROUP BY oq.ShopifyId
            ",
            $id
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $lineitems = $stmt->fetchAll();

        $calculations = $this->createRefundCalculations($shopifyOrderId, $lineitems, $id);

        if (getType($calculations) !== 'array') {
            // An error have occured
            return $calculations;
        }


        $refund = $this->createRefund($calculations, $shopifyOrderId);

        if (getType($refund) !== 'array') {
            // An error have occured
            return $refund;
        }

        if (isset($refund['refund']['id']) !== true) {
            return $refund;
        }


        $shopifyRefundId = $refund['refund']['id'];

        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        $refund = $this->find($id);
        $refund->setShopifyrefundid($shopifyRefundId);
        $refund->setDone(1);
        $refund->setDonedate($nowDate);
        $refund->setDoneuserid((int)$userid);
        $refund->setModifyuserid((int)$userid);
        $refund->setModifydate($nowDate);

        $this->_em->persist($refund);
        $this->_em->flush();

        return true;
    }

    private function createRefundCalculations($_shopifyOrderId, $itemlines, $returnId = 0)
    {
        $calculations = array(
            "refund" => array(
                "refund_line_items" => $itemlines
            )
        );


        $ch = curl_init(sprintf($this->_shopifyUrl . "orders/%s/refunds/calculate.json", $_shopifyOrderId));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($calculations));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        $info = curl_getinfo($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }

        $resultObj = json_decode($result, true);

        $info = curl_getinfo($ch);
        curl_close($ch);
        if ((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201) {
            if (isset($resultObj['errors']) === true) {
                return json_encode($resultObj['errors']);
            }
            return json_encode($result);
        }


        if (count($resultObj['refund']['transactions']) == 0) {
            return 'Error in order no transactions.';
        }

        foreach ($resultObj['refund']['transactions'] as $key => $transaction) {
            $resultObj['refund']['transactions'][$key]['kind'] = 'refund';
        }

        $resultObj['refund']['note'] = 'No: ' . $returnId . ' - Returneret';
        $resultObj['refund']['notify'] = true;

        return $resultObj;
    }

    private function createRefund($refundData, $_shopifyOrderId)
    {
        $ch = curl_init(sprintf($this->_shopifyUrl . "orders/%s/refunds.json", $_shopifyOrderId));
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($refundData));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            $error_msg = curl_error($ch);
            curl_close($ch);
            return $error_msg;
        }

        $info = curl_getinfo($ch);
        curl_close($ch);
        if ((int)$info['http_code'] !== 200 && (int)$info['http_code'] !== 201) {
            $resultObj = json_decode($result, true);
            if (isset($resultObj['errors']) === true) {
                return json_encode($resultObj['errors']);
            }
            return json_encode($result);
        }

        $result = json_decode($result, true);
        return $result;
    }

    public function findShopifyOrder($id)
    {
        $conn = $this->_em->getConnection();
        $reference = strtoupper($id);
        $newId = false;
        $query = sprintf(
            "
			SELECT o.Id, o.Reference
			FROM (`order` o)
			WHERE 
			    o.Reference='%s' AND o.Active=1 
			LIMIT 1
			",
            strtoupper($id)
        );
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $order = $stmt->fetch();

        if ($order === false) {
            // Check if order exists by given return tracking code
            $query = sprintf(
                "
			SELECT o.Id, o.Reference, r.trackingCode
			FROM (`return` r)
			    INNER JOIN (`order` o) ON o.Id=r.orderId AND o.Active=1
			WHERE 
			    r.trackingCode='%s' AND r.Active=1 
			LIMIT 1
			",
                $id
            );
            $stmt = $conn->prepare($query);
            $stmt->execute();
            $order = $stmt->fetch();

            if ($order === false) {
                return 'Order not found with Shopify order or tracking number: ' . strtoupper($id);
            }

            $id = $order['Reference'];

//            $query = sprintf(
//                "
//                SELECT o.Id, o.Reference
//                FROM (`order` o)
//                WHERE
//                o.WebshipperId=%d AND o.Active=1
//                LIMIT 1
//                ",
//                (int)$id
//            );
//            $stmt = $conn->prepare($query);
//            $stmt->execute();
//            $order = $stmt->fetch();
//            $newId = true;
//            if($order === false){
//                return 'order not found with Shopify order number: ' . $id;
//            }
//            $reference = $order['Reference'];
        }

        if ($newId === false) {
            $query = sprintf(
                "
				SELECT s.Id
				FROM (`order` o, orderline ol,item i, container c, stock s)
				WHERE 
				o.Reference='%s' AND o.Active=1 AND ol.OrderId=o.Id AND ol.Active=1 AND i.Active=1 AND i.OrderLineId=ol.Id AND c.Id=i.ContainerId AND c.Active=1 AND s.Id=c.StockId AND s.Active=1
				LIMIT 1
				",
                strtoupper($id)
            );
        } else {
            $query = sprintf(
                "
				SELECT s.Id
				FROM (`order` o, orderline ol,item i, container c, stock s)
				WHERE 
				o.WebshipperId=%s AND o.Active=1 AND ol.OrderId=o.Id AND ol.Active=1 AND i.Active=1 AND i.OrderLineId=ol.Id AND c.Id=i.ContainerId AND c.Active=1 AND s.Id=c.StockId AND s.Active=1
				LIMIT 1
				",
                (int)$id
            );
        }
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $shipment = $stmt->fetch();

        if ($shipment === false) {
            return 'Order has not been shipped yet with shopify order number: ' . $id;
        }

        $query = sprintf(
            "
			SELECT o.Id as OrderId, ol.Description, v.Id as VariantCodeId, az.Name as Size, c.Description as Color, round((ol.PriceSale*(1+(cc.VATPercentage/100))),2) as PriceSale, ol.No, oq.Id as OrderQuantityId, oq.Quantity, oq.ShopifyId as lineItemId, SUM(IF(r.Active=1, rl.Quantity, 0)) as RefundQuantity, v.VariantCode, 'false' as active, 0 as Scanned, 0 as ReasonId, 0 as Restock, container.Position as Position
			FROM (`order` o, orderline ol, orderquantity oq, variantcode v, articlesize az, articlecolor ac, color c, country cc)
			LEFT JOIN container ON container.id=v.PickContainerId
			LEFT JOIN refundline rl ON rl.Active=1 AND rl.OrderQuantityId=oq.Id
			LEFT JOIN refund r ON r.Id=rl.RefundId
			WHERE 
			o.Reference='%s' AND o.Active=1 AND
			az.Id=oq.ArticleSizeId AND
			ac.Id=ol.ArticleColorId AND c.Id=ac.ColorId AND
			ol.OrderId=o.Id AND ol.Active=1 AND
			oq.Active=1 AND oq.OrderLineId=ol.Id AND ol.ArticleColorId > 0 AND 
			v.ArticleId=ol.ArticleId AND v.ArticleColorId=ol.ArticleColorId AND v.ArticleSizeId=oq.ArticleSizeId AND v.Active=1 AND
			cc.Id=o.CountryId
			GROUP BY oq.Id
			",
            strtoupper($id)
        );
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $lineitems = $stmt->fetchAll();

        $allDone = true;
        foreach ($lineitems as $key => $line) {
            if ((int)$line['Quantity'] > (int)$line['RefundQuantity']) {
                $allDone = false;
            }
            $lineitems[$key]['Quantity'] = (int)$line['Quantity'];
        }

        if ($allDone) {
            return 'All items have already been refunded on this order.';
        }


        $query = sprintf(
            "
			SELECT Name, Id, Restock FROM refundreason WHERE Active=1
			",
            strtoupper($id)
        );
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $reasons = $stmt->fetchAll();

        foreach ($reasons as $key => $value) {
            $reasons[$key]['Name'] = utf8_encode($value['Name']);
        }

        return [
            'shopifyorder' => $order['Reference'],
            'line_items' => $lineitems,
            'orderId' => $order['Id'],
            'order' => $order,
            'reasons' => $reasons
        ];
    }
}
