<?php

namespace App\Repository\Order;

use App\Entity\Order\Orderquantity;
use App\Entity\Order\Pickorderline;
use DateTime;
use DateTimeZone;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Pickorderline|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pickorderline|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pickorderline[]    findAll()
 * @method Pickorderline[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PickorderlineRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_PickStockId;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    ) {
        parent::__construct($registry, Pickorderline::class);
        $this->_em = $em;
        $this->_PickStockId = 14321;
    }

    public function getPickorderLines($pickorderid)
    {
        $lines = [];
        $dbLines = $this->findBy(array('active' => 1, 'pickorderid' => $pickorderid), array('done' => 'asc'));

        foreach ($dbLines as $key => $value) {
            array_push($lines, $this->transformLine($value));
        }

        return $lines;
    }

    public function transformLine(Pickorderline $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
            'scanned' => 0,
            'orderedquantity' => (int)$item->getOrderedquantity(),
            'packedquantity' => (int)$item->getPackedquantity(),
            'pickedquantity' => (int)$item->getPickedquantity(),
            'done' => (int)$item->getDone(),
            'variantcode' => (string)$this->getVariantCode($item->getVariantcodeid()),
            'description' => (string)$this->getDescription($item->getVariantcodeid(), $item->getVariantdescription()),
            'position' => (string)$this->getPosition($item->getVariantcodeid()),
        ];
    }

    private function getVariantCode($variantCodeId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                    VariantCode
                FROM variantcode
                WHERE 
                Id=%d AND Active=1
            ",
            (int)$variantCodeId
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        if ($variant === false) {
            return '';
        }

        return $variant['VariantCode'];
    }

    private function getDescription($variantCodeId, $desc)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                    CONCAT(a.Description, ' - ', c.Description, ' - ', az.Name) as description
                FROM (variantcode v, article a, articlesize az, articlecolor ac, color c)
                WHERE 
                v.Id=%d AND v.Active=1 AND a.Id=v.ArticleId AND ac.Id=v.ArticleColorId AND az.Id=v.ArticleSizeId AND c.Id=ac.ColorId
            ",
            (int)$variantCodeId
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        if ($variant === false) {
            return $desc;
        }

        return $variant['description'];
    }

    private function getPosition($variantCodeId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                    CONCAT(s.StockNo, c.Position) as position
                FROM (variantcode v, container c, stock s)
                WHERE 
                v.Id=%d AND v.Active=1 AND c.Id=v.PickContainerId AND c.Active=1 AND s.Id=c.StockId AND s.Active=1
            ",
            (int)$variantCodeId
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        if ($variant === false) {
            return 'No Pos';
        }

        return $variant['position'];
    }

    public function getOrder($id)
    {
        $order = $this->find($id);
        return $this->transform($order);
    }

    public function transform(Pickorderline $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
        ];
    }

    public function createPickorderline($item, $pickorderId, $qlId)
    {
        global $Pickorderlinecreated;
        $Pickorderlinecreated = true;

        $tz = new DateTimeZone('Europe/London');
        $nowDate = new DateTime();
        $nowDate->setTimezone($tz);

        /** @var Orderquantity|null $orderQuantity */
        $orderQuantity = $this->_em->getRepository(Orderquantity::class)->find($qlId);

        $order = new Pickorderline;
        $order->setCreatedate($nowDate);
        $order->setCreateuserid(1);
        $order->setModifydate($nowDate);
        $order->setModifyuserid(1);
        $order->setActive(1);
        $order->setVariantcodeid($item['variantcodeid']);
        $order->setVariantcode($item['variantcode']);
        $order->setOrderedquantity($item['quantity']);
        $order->setPickedquantity(0);
        $order->setPackedquantity(0);
        $order->setPickorderid($pickorderId);
        $order->setVariantcolor((string)$this->getArticleColorDesc($item['articlecolorid']));
        $order->setVariantsize((string)$this->getArticleSizeDesc($item['articlesizeid']));
        $order->setVariantdescription((string)$this->getArticleDesc($item['variantcodeid']));
        $order->setOrderQuantity($orderQuantity);

        $this->_em->persist($order);
        $this->_em->flush();

        return $order->getId();
    }

    public function getArticleColorDesc($articlecolorid)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                    c.Description
                FROM (`articlecolor` ac, `color` c)
                WHERE 
                ac.Id=%d AND c.Id=ac.ColorId
            ",
            (int)$articlecolorid
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $color = $stmt->fetch();

        return $color['Description'];
    }

    public function getArticleSizeDesc($articlesizeId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                    az.Name
                FROM (`articlesize` az)
                WHERE 
                az.Id=%d
            ",
            (int)$articlesizeId
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $color = $stmt->fetch();

        return $color['Name'];
    }

    public function getArticleDesc($variantcodeId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                    a.Description
                FROM (`variantcode` v, `article` a)
                WHERE 
                v.Id=%d AND a.Id=v.ArticleId
            ",
            (int)$variantcodeId
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $color = $stmt->fetch();

        return $color['Description'];
    }



    //   public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
    //       $variant = $this->find($variantCodeId);
    //       $variant->setShopifyvariantid($shopifyVariantId);
    //       $variant->setShopifyproductid($shopifyProductId);
    //       $variant->setInventoryitemid($shopifyInventoryId);

    //       $this->_em->persist($variant);
    //       $this->_em->flush();

    //       return $variant->getId();
    //   }

}
