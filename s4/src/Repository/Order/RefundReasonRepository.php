<?php

namespace App\Repository\Order;

use App\Entity\Order\RefundReason;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<RefundReason>
 *
 * @method RefundReason|null find($id, $lockMode = null, $lockVersion = null)
 * @method RefundReason|null findOneBy(array $criteria, array $orderBy = null)
 * @method RefundReason[]    findAll()
 * @method RefundReason[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class RefundReasonRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, RefundReason::class);
    }

    /**
     * @param  RefundReason  $entity
     * @param  bool  $flush
     * @return void
     */
    public function save(RefundReason $entity, bool $flush = false): void
    {
        $this->getEntityManager()->persist($entity);

        if ($flush) {
            $this->getEntityManager()->flush();
        }
    }
}
