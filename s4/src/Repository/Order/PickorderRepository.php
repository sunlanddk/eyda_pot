<?php

namespace App\Repository\Order;

use App\Entity\Order\Pickorder;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Pickorder|null find($id, $lockMode = null, $lockVersion = null)
 * @method Pickorder|null findOneBy(array $criteria, array $orderBy = null)
 * @method Pickorder[]    findAll()
 * @method Pickorder[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class PickorderRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_PickStockId;
    private $_userid;
    private $_pickorderlineRepo;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        PickorderlineRepository $pickorderlineRepository
    )
    {
        parent::__construct($registry, Pickorder::class);
        $this->_em = $em;
        $this->_PickStockId = 14321;
        $this->_pickorderlineRepo = $pickorderlineRepository;
    }

    public function transform(Pickorder $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
        ];
    }

    public function findPickorder($id, $userid){
        $this->_userid = (int)$userid;
        $pickorder = $this->findOneBy(
            array(
                'active'    => 1,
                'id'        => (int)$id
            )
        );

        if($pickorder === null){
            return 'Pickorder with id '.$id.' not found, try scanning again.';
        }

        if((int)$pickorder->getPacked() === 1){
           return 'Pickorder already packed.';
        }

        return array(
            'pickorder' => array(
                'id' => (int)$id,
                'orderid' => (int)$pickorder->getReferenceid(),
                'line_items' => $this->_pickorderlineRepo->getPickorderLines((int)$id)
            ),
            'stock'     => array(
                'id' => (int)$pickorder->getToid()
            )
        );
    }

    public function getOrder($id){
        $order = $this->find($id);
        return $this->transform($order);
    }

    public function setPickorderActive($id){
        $order = $this->find($id);
        $order->setActive(1);
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function setPickorderInActive($id){
        $order = $this->find($id);
        $order->setActive(0);
        $this->_em->persist($order);
        $this->_em->flush();
    }

    public function createPickorder($post, $consolidatedId, $orderId, $items, $type = 'SalesOrder'){

        $toCompanyId = $post['tocompanyid'];
        $companyId = $post['companyid'];
        $reference = $orderId;
        $referenceId = $orderId;

    	$order = new Pickorder;
        $order->setCreatedate(new \DateTime());
        $order->setCreateuserid(1);
		$order->setModifydate(new \DateTime());
        $order->setModifyuserid(1);
        $order->setDeliverydate(new \DateTime());
        $order->setExpecteddate(new \DateTime());
        $order->setPackeddate(new \DateTime('2001-01-01 00:00:00'));
        $order->setActive(0);
        $order->setType($type);
        $order->setFromid($this->_PickStockId);
        $order->setToid(0);
        $order->setReference((string)$reference);
        $order->setReferenceid((int)$referenceId);
        $order->setConsolidatedid($consolidatedId);
        $order->setOwnercompanyid($toCompanyId);
        $order->setHandlercompanyid($toCompanyId);
        $order->setCompanyid($companyId);
        $order->setInstructions('');
        $order->setPacked(0);
        $order->setUpdated(0);

        $this->_em->persist($order);
        $this->_em->flush();

        return $order->getId();
    }



  //   public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
  //       $variant = $this->find($variantCodeId);
  //       $variant->setShopifyvariantid($shopifyVariantId);
  //       $variant->setShopifyproductid($shopifyProductId);
  //       $variant->setInventoryitemid($shopifyInventoryId);

  //       $this->_em->persist($variant);
  //       $this->_em->flush();

  //       return $variant->getId();
  //   }

}
