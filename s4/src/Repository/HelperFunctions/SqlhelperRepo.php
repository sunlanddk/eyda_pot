<?php

namespace App\Repository\HelperFunctions;

use App\Entity\Sqlhelper;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;
use setasign\FPDF;

/**
 * @method Sqlhelper|null find($id, $lockMode = null, $lockVersion = null)
 * @method Sqlhelper|null findOneBy(array $criteria, array $orderBy = null)
 * @method Sqlhelper[]    findAll()
 * @method Sqlhelper[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class SqlhelperRepo extends ServiceEntityRepository
{
    protected $_em;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    ) {
        parent::__construct($registry, Sqlhelper::class);
        $this->_em = $em;
    }

    public function test()
    {
        $path = getcwd();
        $pattern = "/\[[^\]]*\]/";
        //$pattern = "/\[\d{4}-\d{2}-\d{2}\s\d{2}:\d{2}:\d{2}\]/";
        //return preg_split($pattern, file_get_contents('C:\inetpub\wwwroot\eyda\s4/var/log/prod.log'), -1,PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);

        return explode("\n", file_get_contents('C:\inetpub\wwwroot\eyda\s4/var/log/prod.log'));
    }

    public function printLabel($sku, $ws = 'none')
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "SELECT c.Position, v.VariantCode, a.Description
                    FROM (`variantcode` v, article a)
                    LEFT JOIN container c ON c.Active=1 AND c.Id=v.PickContainerId 
                    LEFT JOIN stock s ON s.Active=1 AND s.Id=c.StockId
                    WHERE v.Active=1 AND v.Id=%d and a.id=v.ArticleId",
            $sku
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        if (($variant === false) or is_null($variant['Position'])) {
            $variant['Position'] = 'NoPos';
        }

        if ($ws == 'none') {
            $_folder = 'PDF';
            $_subfolder = '/NOPOS';
        } else {
            $_folder = 'PDF/CU1';
            $_folder = 'NetSolution/CU1';
        }

        $pdf = new \FPDF('L', 'mm', [52, 25]);
        $pdf->SetMargins(2, 2);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 9);
        $pdf->Cell(58, 3, $variant['VariantCode'], 0, 0, 'C');
        $pdf->Ln();
        $pdf->Cell(58, 3, $variant['Description'], 0, 0, 'C');
        $pdf->Ln();
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 18);
        $pdf->Cell(58, 6, $variant['Position'], 0, 0, 'C');

        $pdf->Output('F', $_SERVER['DEVELSTORE'] . time() . '.pdf');

        $fileinfo['localePath'] = $_SERVER['DEVELSTORE'] . time() . '.pdf';
        $fileinfo['ftpPath'] = $_folder . '/' . $ws . '_Return' . substr($ws, 2, 2) . '_' . time() . '_1.pdf';

        $logininfo['host'] = 'vion.dk';
        $logininfo['usr'] = 'vion.dk04';
        $logininfo['pwd'] = 'Nutrinic1';

        $this->ftpUpload($logininfo, $fileinfo);

        return $variant['Position'];
    }

    private function ftpUpload($logininfo, $fileinfo)
    {
        $local_file = $fileinfo['localePath'];
        $ftp_file = $fileinfo['ftpPath'];

        // connect to FTP server (port 21)
        $conn_id = ftp_connect($logininfo['host'], 21) or die ("Cannot connect to host");

        // send access parameters
        ftp_login($conn_id, $logininfo['usr'], $logininfo['pwd']) or die("Cannot login " . $login['usr'] . ' - ' . $login['pwd']);

        // turn on passive mode transfers (some servers need this)
        ftp_pasv($conn_id, true);

        // perform file upload
//      $upload = ftp_put($conn_id, $ftp_file, $local_file, FTP_BINARY);
        $_timeOfDay = gettimeofday();
        $tmp_file = $ftp_file . '.' . $_timeOfDay['sec'] . $_timeOfDay['usec'];
        $upload = ftp_put($conn_id, $tmp_file, $local_file, FTP_BINARY);

        // check upload status:
        if (!$upload) {
            print('Cannot upload file ' . $local_file . ' to ftp ' . $ftp_file);
        } else {
            ftp_rename($conn_id, $tmp_file, $ftp_file);
        }

        /*
        ** Chmod the file (just as example)
        */

        if (!function_exists('ftp_chmod')) {
            function ftp_chmod($ftp_stream, $mode, $filename)
            {
                return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
            }
        }

        // close the FTP stream
        ftp_close($conn_id);

        return true;
    }

    public function createRefundPositionLabel($refundId, $refundLineId, $variantcodeId, $qty, $userid = 0, $ws = 'none')
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "SELECT c.Position, s.StockNo, v.VariantCode, a.Description, cm.Active, cm.Cancel, cm.Discontinued
                    FROM (`variantcode` v, article a)
                    LEFT JOIN collectionmember cm on cm.ArticleId=v.ArticleId AND cm.ArticleColorId=v.ArticleColorId
                    LEFT JOIN container c ON c.Active=1 AND c.Id=v.PickContainerId 
                    LEFT JOIN stock s ON s.Active=1 AND s.Id=c.StockId
                    WHERE v.Active=1 AND v.Id=%d and a.id=v.ArticleId",
            $variantcodeId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        if (($variant === false) or is_null($variant['Position'])) {
            // don't print any;
            $variant['Position'] = 'NoPos';
            $variant['StockNo'] = '00';
        }

        if (isset($variant['Active']) === true) {
            if ((int)$variant['Active'] === 0) {
                $variant['Position'] = 'Deleted';
                $variant['StockNo'] = '00';
            } elseif ((int)$variant['Cancel'] === 1) {
                $variant['Position'] = 'Cancelled';
                $variant['StockNo'] = '00';
            } elseif ((int)$variant['Discontinued'] === 1) {
                $variant['Position'] = 'Discontinued';
                $variant['StockNo'] = '00';
            }
        }

        if ($ws == 'none') {
            // don't print any if workstation not set
            $_folder = 'PDF';
            $_subfolder = '/NOPOS';
        } else {
            $_folder = 'PDF/CU1';
//            $_subfolder = $ws . '/DEL' ;
            $_folder = 'NetSolution/CU1';
        }
        $pdf = new \FPDF('L', 'mm', [40, 50]);
//        $pdf->AddPage() ;
        $pdf->SetMargins(2, 2);
        $pdf->SetAutoPageBreak(false);
        $pdf->AddPage();
        $pdf->SetFont('Arial', '', 9);
        $pdf->SetXY(2, 12);
        $pdf->Cell(45, 3, $variant['VariantCode'], 0, 0, 'C');
        $pdf->Ln();
        if (strlen($variant['Description']) < 30) {
            $pdf->Cell(45, 3, $variant['Description'], 0, 0, 'C');
            $pdf->Ln();
        } else {
            $pdf->MultiCell(45, 3, $variant['Description'], 0, 'C');
        }
        $pdf->Ln();
        $pdf->SetFont('Arial', 'B', 18);
        $pdf->Cell(45, 6, $variant['StockNo'] . $variant['Position'], 0, 0, 'C');

        $pdf->Output('F', $_SERVER['DEVELSTORE'] . $refundId . '_' . $refundLineId . '.pdf');

        $count = 1;
        while ($qty >= $count) {
//            $_folder = 'Test' ;
//            $_subfolder = 'alex';
            $fileinfo['localePath'] = $_SERVER['DEVELSTORE'] . $refundId . '_' . $refundLineId . '.pdf';

//            $fileinfo['ftpPath'] = $_folder.'/'. $_subfolder .'/' .  $refundId.'_'.$refundLineId .'_'.$count. '.pdf';
            $fileinfo['ftpPath'] = $_folder . '/' . $ws . '_Return' . substr($ws, 2, 2) . '_' . $refundId . '_' . $refundLineId . '_' . $count . '.pdf';

            $logininfo['host'] = 'vion.dk';
            $logininfo['usr'] = 'vion.dk04';
            $logininfo['pwd'] = 'Nutrinic1';

            $this->ftpUpload($logininfo, $fileinfo);
            $count++;
        }

        return $variant['StockNo'] . $variant['Position'];
    }

    public function tableGetField($table, $column, $id)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT %s
                FROM %s
                WHERE Id=%d
            ",
            $column,
            $table,
            $id
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();
        if ($variant === false) {
            return '';
        }

        return $variant[$column];
    }

    public function getCompany($id)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT *
                FROM company
                WHERE Id=%d
            ",
            $id
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        return $variant;
    }

    public function getNextSku()
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "SELECT * FROM `variantcode` ORDER BY ABS(`SKU`) DESC LIMIT 1"
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        return ((int)$variant['SKU'] + 1);
    }

    public function getShopifyShops($variantcodeId = 0)
    {
        $conn = $this->_em->getConnection();
        if ($variantcodeId === 0) {
            $sql = sprintf(
                "
                    SELECT s.id shopid, s.Name as shopname, s.LanguageId as languageid, s.CurrencyId as currencyid
                    FROM (shopifyshops s) 
                    WHERE s.Active=1
                "
            );
        } else {
            $sql = sprintf(
                "
                    SELECT s.id shopid, s.Name as shopname, s.LanguageId as languageid, s.CurrencyId as currencyid
                    FROM (shopifyshops s, inshopifyshops iss, variantcode v, collectionmember c) 
                    WHERE s.Active=1 AND iss.CollectionMemberId=c.Id AND c.Active=1 AND v.Id=%d AND iss.Active=1 AND iss.ShopifyShopId=s.Id AND c.ArticleId=v.ArticleId AND c.ArticleColorId=v.ArticleColorId
                ",
                $variantcodeId
            );
        }

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        return $shops = $stmt->fetchAll();
    }

    public function getVariantInformation($variantid, $shopId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT sv.ArticleId, v.ArticleColorId, v.ArticleSizeId, v.VariantCode, v.Id as VariantCodeId
                FROM (shopifyvariant sv, variantcode v) 
                WHERE sv.ShopifyVariantId='%d' AND v.Id=sv.VariantCodeId AND sv.ShopifyshopId=%d
                LIMIT 1
            ",
            $variantid,
            $shopId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        return $variant;
    }

    public function getVariantIdByArticle($articleId, $articleColorId, $articleSizeId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT v.Id
                FROM (variantcode v) 
                WHERE v.ArticleId=%d AND v.ArticleColorId=%d AND v.ArticleSizeId=%d AND Active=1
                LIMIT 1
            ",
            $articleId,
            $articleColorId,
            $articleSizeId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        return $variant['Id'];
    }

    public function getCountryId($countryCode, $vat)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT c.Id
                FROM (country c) 
                WHERE c.Active=1 AND c.IsoCode='%s' AND VATPercentage='%s'
                LIMIT 1
            ",
            $countryCode,
            $vat
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $country = $stmt->fetch();

        if ((int)$country['Id'] > 0) {
            return $country['Id'];
        }

        return 1;
    }

    public function getShopId($domain)
    {
        if ($domain == 'eyda-by-lyngholm.myshopify.com') {
            return $this->getShopInfo(1);
        }

        if ($domain == 'eydaintl.myshopify.com') {
            return $this->getShopInfo(2);
        }

        if ($domain == 'eyda-no.myshopify.com') {
            return $this->getShopInfo(3);
        }

        if ($domain == 'eyda-se.myshopify.com') {
            return $this->getShopInfo(4);
        }

        if ($domain == 'eyda-ambassadors.myshopify.com') {
            return $this->getShopInfo(5);
        }

        if ($domain == 'eyda-b2b.myshopify.com') {
            return $this->getShopInfo(6);
        }

        if ($domain == 'eyda-nl.myshopify.com') {
            return $this->getShopInfo(7);
        }

        if ($domain == 'eyda-development.myshopify.com') {
            return $this->getShopInfo(1);
        }

        if ($domain == 'eyda-development-eur.myshopify.com') {
            return $this->getShopInfo(2);
        }

        return false;
    }

    public function getShopInfo($id)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT c.Id as currencyid, l.Id as languageid, s.Id as id, s.Name as name
                FROM (shopifyshops s, currency c, language l) 
                WHERE s.Id=%d AND c.Id=s.CurrencyId AND l.Id=s.LanguageId
            ",
            $id
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $shops = $stmt->fetch();
    }

    public function getOrderLineQuantities($orderLineId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf("SELECT * FROM orderquantity WHERE Active=1 AND OrderLineId=%d", $orderLineId);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $quantities = $stmt->fetchAll();

        return $quantities;
    }

    public function get_article_qnty_from_reqcontainer($reqcontainer_id, $article_id, $color_id, $size_id)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "select sum(i.Quantity) as qnty from
                            container c
                            left join item i on i.ContainerId=c.Id
                            where c.Id=%d
                            and i.ArticleId=%d  and i.articlecolorId=%d  and i.ArticleSizeId=%d and i.Active=1",
            $reqcontainer_id,
            $article_id,
            $color_id,
            $size_id
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $qty = $stmt->fetch();

        return (int)$qty['qnty'];
    }

    public function getQtyOfVariant($variantcodeId)
    {
        $variant = $this->getVariantInformationById($variantcodeId);
        $qnty = $this->get_article_qnty_from_pickstock(1, $variant['articleid'], $variant['articlecolorid'], $variant['articlesizeid']);
        $qnty_reqcon = 0;
        /*
                if ((int)$variant['Presale']==0) {
                    $qnty_reqcon = 0;
                } else {
                    $qnty_reqcon = $this->get_article_qnty_from_reqcontainer(135400, $variant['articleid'], $variant['articlecolorid'], $variant['articlesizeid']);
                }
        */
        $qnty_in_order = $this->get_article_qnty_in_orders(1, $variant['articleid'], $variant['articlecolorid'], $variant['articlesizeid'], 787);
        $qnty = $qnty + $qnty_reqcon - $qnty_in_order;

        if ($qnty < 0) {
            $qnty = 0;
        }

        return $qnty;
    }

    public function getVariantInformationById($variantid)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT v.ArticleId as articleid, v.ArticleColorId as articlecolorid, v.ArticleSizeId as articlesizeid, v.VariantCode, v.Id as VariantCodeId,
                    v.Presale
                FROM (variantcode v) 
                WHERE v.Id='%d'
                LIMIT 1
            ",
            $variantid
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $variant = $stmt->fetch();

        return $variant;
    }

    public function get_article_qnty_from_pickstock($season_id, $article_id, $color_id, $size_id)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "select sum(i.Quantity) as qnty from
                        `season` s
                        left join `container` c on c.StockId=s.pickstockid
                        left join `item` i on i.ContainerId=c.Id
                        where s.Id=%d
                        and i.ArticleId=%d  and i.articlecolorId=%d  and i.ArticleSizeId=%d and i.Active=1",
            $season_id,
            $article_id,
            $color_id,
            $size_id
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $qty = $stmt->fetch();

        return (int)$qty['qnty'];
    }

    public function get_article_qnty_in_orders($season_id, $article_id, $color_id, $size_id, $owner_id)
    {
        $qnty = 0;
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "select sum(q.Quantity) as qnty, max(l.id) as orderlineid from
                        (`order` o, season s)
                        left join `orderline` l on l.OrderId=o.Id
                        left join `orderquantity` q on q.OrderLineId=l.Id  and q.active=1
                        inner join collection c on c.seasonid=o.seasonid and c.active=1
                        inner join collectionmember cm on cm.CollectionId=c.id and cm.ArticleId=l.ArticleId and cm.ArticleColorId=l.ArticleColorId and cm.active=1
                        inner join collectionlot cl on cl.id=cm.CollectionLOTId and cl.active=1
                        where o.Active=1 and l.active=1 and o.Done=0 and l.Done=0 and o.ToCompanyId = %d 
                        and s.id=o.seasonid and l.active=1 
                        and l.ArticleId=%d and l.ArticleColorId=%d and q.ArticleSizeId=%d
                        group by q.articlesizeid",
            $owner_id,
            $article_id,
            $color_id,
            $size_id
        );
 // Ken 2024.01.03
        $sql = sprintf(
            "select sum(q.Quantity) as qnty, max(l.id) as orderlineid from
                        (`order` o, season s)
                        left join `orderline` l on l.OrderId=o.Id
                        left join `orderquantity` q on q.OrderLineId=l.Id  and q.active=1
                        inner join collection c on c.seasonid=o.seasonid and c.active=1
                        inner join collectionmember cm on cm.CollectionId=c.id and cm.ArticleId=l.ArticleId and cm.ArticleColorId=l.ArticleColorId and cm.active=1
                        inner join collectionlot cl on cl.id=cm.CollectionLOTId and cl.active=1
                        where o.Active=1 and l.active=1 and o.Done=0 and l.Done=0 and o.ToCompanyId = %d 
                        and s.id=o.seasonid and l.active=1 
                        and l.ArticleColorId=%d and q.ArticleSizeId=%d
                        group by q.articlesizeid",
            $owner_id,
            $color_id,
            $size_id
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();
        if ($row === false) {
            return 0;
        }

        $qnty = $row['qnty'];

// Ken: Is this meaningful when more orders?
        $sql = sprintf("select sum(quantity) as qnty from `item` where orderlineid=%d and articlesizeid=%d and active=1", $row['orderlineid'], $size_id);
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $row = $stmt->fetch();

        $qnty = ((int)$row['qnty'] > $qnty) ? 0 : $qnty - $row['qnty'];

        return $qnty;
    }

    private function getCompanyId()
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT s.id shopid, s.Name as shopname, s.LanguageId as languageid, s.CurrencyId as currencyid
                FROM (shopifyshops s) 
                WHERE s.Active=1
            "
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();

        return $stmt->fetchAll();
    }
}
