<?php

namespace App\Repository\Stock;

use App\Entity\Stock\Stock;
use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Order\PickorderlineRepository;
use App\Repository\Order\PickorderRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

use Exception;

use const App\Repository\_LEGACY_LIB;

/**
 * @method Stock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stock[]    findAll()
 * @method Stock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ShipmentInvoiceRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_containerRepository;
    private $_itemRepository;
    private $_sqlHelper;
    private $_transactionRepository;
    private $_pickorderRepository;
    private $_pickorderlineRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        ContainerRepository $containerRepository,
        ItemRepository $itemRepository,
        SqlhelperRepo $sqlHelper,
        TransactionRepository $transactionRepository,
        PickorderRepository $pickorderRepository,
        PickorderlineRepository $pickorderlineRepository
    )
    {
        parent::__construct($registry, Stock::class);
        $this->_em = $em;
        $this->_containerRepository = $containerRepository;
        $this->_itemRepository = $itemRepository;
        $this->_sqlHelper = $sqlHelper;
        $this->_transactionRepository = $transactionRepository;
        $this->_pickorderRepository = $pickorderRepository;
        $this->_pickorderlineRepository = $pickorderlineRepository;
    }

    private function getOrderlineAndQuantityId($orderId, $shopifyVariantId, $shopifyLineId, $shop){
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT 
                ol.Articleid as articleid,
                ol.ArticleColorId as articlecolorid,
                ol.Id as orderlineid,
                oq.Id as orderquantityid,
                oq.ArticleSizeId as articlesizeid,
                v.variantcode as variantcode,
                v.PickContainerId as pickcontainerid,
                v.Id as variantcodeid
                FROM (`shopifyvariant` sv, `variantcode` v, `orderline` ol, `orderquantity` oq)
                WHERE 
                sv.ShopifyshopId=%d AND sv.ShopifyVariantId='%s' AND
                v.Id=sv.VariantCodeId AND
                ol.OrderId=%d AND ol.ArticleId=v.ArticleId AND ol.ArticleColorId=v.ArticleColorId AND ol.Active=1 AND 
                oq.OrderLineId=ol.Id AND oq.Active=1 AND oq.ArticleSizeId=v.ArticleSizeId AND oq.ShopifyId='%s'
            ",
            (int)$shop,
            $shopifyVariantId,
            (int)$orderId,
            $shopifyLineId
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetch();

        if($items === false){
            return array(
                        'variantcode' => '',
                        'variantcodeid' => 0,
                        'articleid' => 0,
                        'articlecolorid' => 0,
                        'articlesizeid' => 0,
                        'orderlineid' => 0,
                        'orderquantityid' => 0,
                        'pickcontainerid' => 0,
                    );
        }
        return $items;
    }

    public function packPickOrder($articles, $stockId, $orderId)
    {
        $pickorder = $this->_pickorderRepository->findOneBy(array('referenceid' => $orderId, 'active' => 1));
        if ($pickorder == null) {
            return false;
        }

        $pickorder->setModifydate(new \DateTime())
            ->setModifyuserid(1)
            ->setPackeddate(new \DateTime())
            ->setToid((int)$stockId)
            ->setPacked(1);
        $this->_em->persist($pickorder);
        $this->_em->flush();

        $pickorderLines = $this->_pickorderlineRepository->findBy(array('active' => 1, 'pickorderid' => $pickorder->getId()));
        foreach ($pickorderLines as $key => $pickorderline) {
            foreach ($articles as $key => $article) {
                if ((int)$pickorderline->getOrderQuantity()->getId() === (int)$article['orderquantityid'] && (int)$article['qty'] > 0) {
                    $pickorderline->setModifydate(new \DateTime())
                        ->setModifyuserid(1)
                        ->setPackedquantity((int)$article['qty']);
                    $this->_em->persist($pickorderline);
                    $this->_em->flush();
                    unset($articles[$key]);
                }
            }
        }

        return true;
    }

    public function createShipmentInvoiceFromPickorder($orderId, $postedLines, $domain){
        $shop = $this->_sqlHelper->getShopId($domain);

        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT *
                FROM orderadresses
                WHERE 
                OrderId=%d AND Type='shipping'
            ",
            $orderId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $address = $stmt->fetch();

        $sql = sprintf(
            "
                SELECT *
                FROM orderadresses
                WHERE 
                OrderId=%d AND Type='invoice'
            ",
            $orderId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $invoiceaddress = $stmt->fetch();

        $stock = new Stock();

        $stock->setCreatedate(new \DateTime())
            ->setCreateuserid(1)
            ->setModifydate(new \DateTime())
            ->setModifyuserid(1)
            ->setActive(true)
            ->setname('SalesOrder '.$orderId . ' Auto by API')
            ->setAltcompanyname($address['AltCompanyName'])
            ->setAddress1($address['Address1'])
            ->setAddress2($address['Address2'])
            ->setZip($address['ZIP'])
            ->setCity($address['City'])
            ->setCountryid($address['CountryId'])
            ->setCompanyid(7779)
            ->setType('shipment')
            ->setDeliverytermid(0)
            ->setCarrierid(0)
            ->setDeparturedate(new \DateTime())
            ->setReady(1)
            ->setReadydate(new \DateTime())
            ->setReadyuserid(1)
            ->setDeparted(1)
            ->setDeparteddate(new \DateTime())
            ->setDeparteduserid(1)
            ->setDone(1)
            ->setDonedate(new \DateTime())
            ->setDoneuserid(1)
            ->setInvoiced(1)
            ->setInvoiceddate(new \DateTime())
            ->setinvoiceduserid(1)
            ->setFromcompanyid(787)
            ->setInventorypct('100')
        ;

        try {
            $this->_em->persist($stock);
            $this->_em->flush();
        } catch (Exception $e) {
            return $e;
        }

        $containerId = $this->_containerRepository->createContainer($stock->getId(), 22);

        $articles = array();
        foreach ($postedLines['fulfillments'] as $key => $fullfillment) {
            foreach ($fullfillment['line_items'] as $key => $line) {
                if(isset($articles[$line['id']])){
                    $articles[$line['id']]['qty'] = (int)$articles[$line['id']]['qty'] + (int)$line['quantity'];
                }
                else{
                    $articles[$line['id']] = $this->getOrderlineAndQuantityId($orderId, $line['variant_id'], $line['id'], $shop['id']);
                    $articles[$line['id']]['qty'] = (int)$line['quantity'];
                }
            }
        }

        $transactionId = $this->_transactionRepository->createTransaction('Ship', $stock->getId(), 14321, $stock->getId());
        $totalQty = 0;
        $totalValue = 0;
        foreach ($articles as $key => $article) {
            if($article['orderlineid'] > 0){
                $inventory = $this->_itemRepository->moveItemsToShipment($article['orderlineid'], $article['orderquantityid'], $article['articleid'], $article['articlecolorid'], $article['articlesizeid'], $article['qty'], $transactionId, $containerId, (int)$article['pickcontainerid']);
                $totalQty += $inventory[0];
                $totalValue += $inventory[1];
            }
            else{
                // return 'error';
            }
        }
        $transactionId = $this->_transactionRepository->updateTransaction($transactionId, $totalValue, $totalQty);

        $this->packPickOrder($articles, $stock->getId(), $orderId);

        // $create

        return $articles;

        return $stock->getId();
    }

    private function createInvoice($articles, $invoiceaddress){

    }


    public function createShipmentInvoiceFromPickorderOld($Id){

        $conn = $this->_em->getConnection();

        $_POST = array(
            "id" => 8,
            "nid" => 1910,
            "VariantCode" => '',
            "GrossWeight" => '',
            "ContainerId" => '',
            "StockId" => 0,
            "ContainerTypeId" => 22,
            "Position" => '',
            "PickQty" => array(
                "5715232002054" => 1
            ),
            "OrderQty" => array(
                "5715232002054" => 1
            ),
            "PackQty" => array(
                "5715232002054" => 0
            ),

            "PickOrderLineId" => array(
                "5715232002054" => 0,
            ),
            "clicked" => 1
        );
        $Record = array();
        $User = array();
        $Navigation = array();
        global $User, $Navigation, $Record, $Size, $Id ;

        $PcsPerVariantUnit = array (
            'pcs'   =>  1,
            'PACK2' =>  2,
            'PACK3' =>  3,
            'PACK4' =>  4,
            'Pcs'   =>  1
        ) ;

        $_preinvoiceid = $this->_tableRepo->tableGetField('`order`','PreInvoiceId', $Record['ReferenceId']) ;
        $_reforderid = $Record['ReferenceId'] ;


        $_orderid = $Id;

        $TotalQuantity=0;
        foreach ($_POST['PickQty'] as $VariantCode => $qty) {
            $TotalQuantity += $qty ;
        }

        // Get Fields
        $fields = array (
            'ContainerId'       => array ('type' => 'integer'),
            'StockId'       => array ('type' => 'integer'),
            'ContainerTypeId'   => array ('type' => 'integer'),
            'Position'      => array (),
            'GrossWeight'       => array ('type' => 'decimal',  'format' => '12.3'),
        ) ;
        $res = $this->_saveRepo->saveFields (NULL, NULL, $fields) ;
        if ($res) return $res ;


        return $fields;
        set_time_limit (300) ;
        if ($TotalQuantity>0) { // Only do anything with items and containers if something is scanned.
            // Container creation and validation
            if ($fields['ContainerId']['value'] > 0) {
                // Existing Container !
                // Lookup information
                $query = sprintf ('SELECT Container.*, Stock.Name AS StockName, Stock.Ready AS StockReady FROM Container INNER JOIN Stock ON Stock.Id=Container.StockId WHERE Container.Id=%d AND Container.Active=1', $fields['ContainerId']['value']) ;
                $res = dbQuery ($query) ;
                $Container = dbFetch ($res) ;
                dbQueryFree ($res) ;
                if ((int)$Container['Id'] != $fields['ContainerId']['value']) return sprintf ('Container %d not found', $fields['ContainerId']['value']) ;
                if ($Container['StockReady']) return sprintf ('no operations allowed on Container %d', (int)$Container['Id']) ;
                // Any Gross Weight?
                if ($fields['GrossWeight']['value'] == 0) return 'please type new Gross weight for Container after adding items' ;
                $Container_update = array (
                    'GrossWeight' => $fields['GrossWeight']['value'],
                ) ;
                tableWrite ('Container', $Container_update, (int)$Container['Id']) ;
                //  $fields['StockId']['value'] = $Container['StockId'] ;

            } else {
                // New Container
                // Any Stock location ?
                // if ($fields['StockId']['value'] == 0) return 'please select new Location' ;
                if ($fields['StockId']['value'] == 0) {
                    // Make new shipment
                    if ($Record['Type']=='SalesOrder') {
                        $query = sprintf ('SELECT * FROM `Order` WHERE Id=%d AND Active=1', $Record['ReferenceId']) ;
                        $result = dbQuery ($query) ;
                        $Order = dbFetch ($result) ;
                        dbQueryFree ($result) ;
                        $shipment = array (
                            'Name'              => sprintf('%s SO: %s CO: %s', $Record['Type'], $Record['Reference'], $Record['ConsolidatedId']),
                            'Description'       => $Record['OrderTypeId']==10?'@':'',
                            'DepartureDate'     => $Record['DeliveryDate'],
                            'CompanyId'         => $Record['CompanyId'],
                            'FromCompanyId'     => $Record['OwnerCompanyId'],
                            'Type'              => 'shipment',
                            'DeliveryTermId'    => $Order['DeliveryTermId'],
                            'CarrierId'         => $Order['CarrierId'],
                            'AltCompanyName'    => $Order['AltCompanyName'],
                            'Address1'          => $Order['Address1'],
                            'Address2'          => $Order['Address2'],
                            'ZIP'               => $Order['ZIP'],
                            'City'              => $Order['City'],
                            'CountryId'         => $Order['CountryId']
                        ) ;
                    } else {
                        $shipment = array (
                            'Name'              => sprintf('%s SO: %s CO: %s', $Record['Type'], $Record['Reference'], $Record['ConsolidatedId']),
                            'Description'       => '',
                            'DepartureDate'     => $Record['DeliveryDate'],
                            'CompanyId'         => $Record['CompanyId'],
                            'FromCompanyId'     => $Record['OwnerCompanyId'],
                            'Type'              => 'shipment',
                            'DeliveryTermId'    => TableGetField('Company','DeliveryTermId',$Record['CompanyId']),
                            'CarrierId'         => TableGetField('Company','CarrierId',$Record['CompanyId']),
                            'Address1'          => TableGetField('Company','DeliveryAddress1',$Record['CompanyId']),
                            'Address2'          => TableGetField('Company','DeliveryAddress2',$Record['CompanyId']),
                            'ZIP'               => TableGetField('Company','DeliveryZip',$Record['CompanyId']),
                            'City'              => TableGetField('Company','DeliveryCity',$Record['CompanyId']),
                            'CountryId'         => TableGetField('Company','DeliveryCountryId',$Record['CompanyId'])
                        ) ;
                    }
                    $fields['StockId']['value']=tableWrite ('Stock', $shipment) ;
                }
                // Get Stock
                $query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $fields['StockId']['value']) ;
                $result = dbQuery ($query) ;
                $Stock = dbFetch ($result) ;
                dbQueryFree ($result) ;
                if ((int)$Stock['Id'] != $fields['StockId']['value']) return sprintf ('%s(%d) Stock not found, id %d', __FILE__, __LINE__, $fields['StockId']['value']) ;
                if ($Stock['Ready']) return sprintf ('%s(%d) no Item operations allowed on Stock for new Container, id %d', __FILE__, __LINE__, (int)$Stock['Id']) ;

                // Any ContainerType ?
                if ($fields['ContainerTypeId']['value'] == 0) return 'please select type for new Container' ;

                // Any Gross Weight?
                if ($fields['GrossWeight']['value'] == 0)
                    $fields['GrossWeight']['value'] = 1 ;
                    //return 'please type Gross weight for new Container' ;

                // Get ContainerType
                $query = sprintf ('SELECT * FROM ContainerType WHERE Id=%d AND Active=1', $fields['ContainerTypeId']['value']) ;
                $result = dbQuery ($query) ;
                $ContainerType = dbFetch ($result) ;
                dbQueryFree ($result) ;
                if ((int)$ContainerType['Id'] != $fields['ContainerTypeId']['value']) return sprintf ('%s(%d) ContainerType not found, id %d', __FILE__, __LINE__, $fields['ContainerTypeId']['value']) ;

                // Create new Container
                $Container = array (
                    'StockId' => (int)$Stock['Id'],
                    'ContainerTypeId' => (int)$ContainerType['Id'],
                    'Position' => $fields['Position']['value'],
                    'TaraWeight' => $ContainerType['TaraWeight'],
                    'GrossWeight' => $fields['GrossWeight']['value'],
                    'Volume' => $ContainerType['Volume']
                ) ;
                $Container['Id'] = tableWrite ('Container', $Container) ;
                $Container['StockName'] = $Stock['Name'] ;
            }
            // END CONTAINER VALIDATION AND CREATION

            // Create ItemOperation referance
            $ItemOperation = array (
                'Description' => sprintf ('Items picked to Container %d at %s', (int)$Container['Id'], $Container['StockName'])
            ) ;
            $ItemOperation['Id'] = tableWrite ('ItemOperation', $ItemOperation) ;

            $Error_txt=0 ;
            // Process scanned quantities for each pick line
            foreach ($_POST['PickQty'] as $VariantCode => $_consolidatedqty) {
                if ($_consolidatedqty==0) continue ; // Nothing picked: next line
                // multible consolidated picklines per variant
                $_consolidated=explode(',',$_POST['PickOrderLineId'][$VariantCode]) ;
                $_consolidatedlines = count($_consolidated) ;
                foreach ($_consolidated as $_pickorderlineid) {
                    if ($_consolidatedqty<=0) continue ; // No need on this line: next line

                    // Get Variant Information
                    $query = sprintf ('SELECT * FROM pickorderline WHERE Id=%d', $_pickorderlineid) ;
                    $res = dbQuery ($query) ;
                    $_pickorderline = dbFetch ($res) ;
                    dbQueryFree ($res) ;
                    $_pickorderlineqty = (int)$_pickorderline["OrderedQuantity"]-(int)$_pickorderline["PackedQuantity"] ; // -(int)$_pickorderline["PickedQuantity"] ;
                    if ($_pickorderlineqty<=0) continue ; // No need on this line: next line

                    if ($_consolidatedlines > 1) {
                        $qty = ($_consolidatedqty>$_pickorderlineqty)? $_pickorderlineqty : $_consolidatedqty ;
                        $_consolidatedqty -= $qty ;
                    } else {
                        $qty = $_consolidatedqty ; // Allow surplus on last line
                    }
                    $_consolidatedlines-- ;
                        // New code end
                    // process each consolidated pickline.
                    if ($VariantCode <= 0) return sprintf ('%s(%d) invalid index %d', __FILE__, __LINE__, $ol_id) ;

                    // Get Variant Information
                    $query = sprintf ('SELECT VariantCode.*, Container.StockId as PickStockId
                                    FROM VariantCode 
                                    LEFT JOIN Container ON VariantCode.PickContainerId=Container.Id AND Container.Active=1
                                    WHERE VariantCode.VariantCode=%s AND VariantCode.Active=1', $VariantCode) ;
                    $res = dbQuery ($query) ;
                    $VariantCodeInfo = dbFetch ($res) ;
                    dbQueryFree ($res) ;

                    // Is the PickStock the items main pick stock? If not use aleternative pickstock
                    if (!($VariantCodeInfo['PickStockId'] == $Record['FromId']) AND !($Record['FromId']==0))
                        $VariantCodeInfo['PickContainerId'] = 0;    // Future opt: Use containerid from pickcontainer table (variantid, container)

                    // decrease item qty in pick location (in pick container) or make new item.
                    if (($VariantCodeInfo['PickContainerId'] == 0) and ($Record['Type']=='SalesOrder'))
                        $query = sprintf ('SELECT Item.* FROM Item, Container WHERE Item.ContainerId=Container.Id And Container.StockId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Item.Active=1 order by createdate',
                                     $Record['FromId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
                    else
                        $query = sprintf ('SELECT * FROM Item WHERE ContainerId=%d And ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Active=1 order by createdate',
                                     $VariantCodeInfo['PickContainerId'],$VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId'] ) ;
                    $res = dbQuery ($query) ;

                    $PickedQuantity = 0;
                    if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
                        $RemScannedQty = $qty * $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']] ;
                    else
                        $RemScannedQty = $qty ;
                    $ReqQty=$RemScannedQty ;

                    // Allocate Items to sales orderlines for pickorders based on salesorders.
                    if ($Record['Type']=='SalesOrder') {
                        If ((int)$_pickorderline["QlId"]>0) {
                            $query = sprintf ('SELECT OrderQuantity.OrderLineId as Id 
                                                FROM OrderQuantity 
                                                WHERE OrderQuantity.Id=%d',(int)$_pickorderline["QlId"]) ;
                        } else {
                            $query = sprintf ('SELECT OrderLine.Id as Id 
                                                FROM OrderLine
                                                LEFT JOIN OrderQuantity ON OrderQuantity.OrderLineId=OrderLine.id And OrderQuantity.ArticleSizeId=%d AND OrderQuantity.Active=1 And OrderQuantity.Quantity>0
                                                WHERE   OrderLine.OrderId IN(%s) And OrderLine.ArticleId=%d AND 
                                                        OrderLine.ArticleColorId=%d AND OrderLine.Active=1  AND OrderLine.Done=0',
                                                        $VariantCodeInfo['ArticleSizeId'],$_orderid,
                                                        $VariantCodeInfo['ArticleId'],$VariantCodeInfo['ArticleColorId'] ) ;
                        }
                        $olres = dbQuery ($query) ;
                        $OrderLine = dbFetch ($olres) ;
                        dbQueryFree ($olres) ;
                        if ($OrderLine['Id']>0) {
                            $IOrderLineId = $OrderLine['Id'] ;
                        } else {
                            return sprintf('SKU %s in pickorder but not found in salesorder %d',
                                        $VariantCode, $Record['ReferenceId'],
                                        $VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleColorId'],$VariantCodeInfo['ArticleSizeId']) ;
                        }
                    }

                    // Find items on stock to fulfil registered qty.
                    while ($Item = dbFetch ($res)) {
                        if ($Item['Id'] > 0) {
                            if (($Item['Quantity'] > $RemScannedQty) or ($Item['Quantity'] == $RemScannedQty)) {
                                $PickedQuantity += $RemScannedQty ;

                                // Split Item and create new item with complete qty.
                                $Item['Quantity'] -= $RemScannedQty ;
                                tableWrite ('Item', $Item, $Item['Id']) ;
                                If ($Item['Quantity'] == 0)
                                    tableDelete ('Item', $Item['Id']) ;

                                $Item['PreviousContainerId']=$Item['ContainerId'] ;
                                $Item['ContainerId']=$Container['Id'] ;
                                $Item['ParentId']=$Item['Id'] ;
                                $Item['Quantity'] = $RemScannedQty ;
                                $Item['OrderLineId'] = $IOrderLineId ;

                                unset ($Item['Id']) ;
                                tableWrite ('Item', $Item) ;
                                break ;
                            } else {
                                $RemScannedQty -= $Item['Quantity'] ;
                                $PickedQuantity += $Item['Quantity'] ;

                                // delete Item on stock and create new item with item qty.
                                $ItemQty = $Item['Quantity'];
                                $Item['Quantity'] = 0 ;
                                tableWrite ('Item', $Item, $Item['Id']) ;
                                tableDelete ('Item', $Item['Id']) ;

                                $Item['PreviousContainerId']=$Item['ContainerId'] ;
                                $Item['ContainerId']=$Container['Id'] ;
                                $Item['ParentId']=$Item['Id'] ;
                                $Item['Quantity'] = $ItemQty ;
                                $Item['OrderLineId'] = $IOrderLineId ;

                                unset ($Item['Id']) ;
                                tableWrite ('Item', $Item) ;
                            }
                        } else {
                            return sprintf ('Invalid Item fetch') ;
                        }
                        $Clone_Item = $Item ;
                    }
                    // Check if enough items found on stock to fulfil registration
                    if ($PickedQuantity < $ReqQty) {
                        // Nothing found at all: Make item from scratch.
                        if ($PickedQuantity == 0) {
                            $Item = array (
                                'ArticleId'     => (int)$VariantCodeInfo['ArticleId'],
                                'ArticleColorId'=> (int)$VariantCodeInfo['ArticleColorId'],
                                'ArticleSizeId' => (int)$VariantCodeInfo['ArticleSizeId'],
                                'ContainerId'   => (int)$Container['Id'],
                                'OrderLineId'   => (int)$IOrderLineId ,
                                'BatchNumber'   => 'PICK' ,
                                'Sortation'     => 1,
                                'OwnerId'   => 787,
                                'Quantity'      => ($ReqQty - $PickedQuantity)
                            ) ;
                            unset ($Item['Id']) ;
                            tableWrite ('Item', $Item) ;
                        } else {
                            // something found but not enough: Clone last item.
                            $Clone_Item['Quantity']     = $ReqQty - $PickedQuantity ;
                            $Clone_Item['BatchNumber']  = 'PICK' ;
                            unset ($Clone_Item['Id']) ;
                            tableWrite ('Item', $Clone_Item) ;
                        }
                        if ($VariantCodeInfo['VariantUnit']) // Handle when items are picked in packs of more pcs's
                            $qty=(int)($ReqQty / $PcsPerVariantUnit[$VariantCodeInfo['VariantUnit']]) ;
                        else
                            $qty=$ReqQty;
                    }
                    dbQueryFree ($res) ;

                    // Update qty in PickOrderLine
                    $PickOrderLine['PackedQuantity'] = $_POST['PackQty'][$VariantCode] + $qty  ;
                    $PickOrderLine['PickedQuantity'] = 0  ;
                    tableWrite ('PickOrderLine', $PickOrderLine, $_pickorderlineid) ;
                } // END consolidated variants.
            } // END of each pickline.
        } // END TotalQuantity >0

        if ($TotalQuantity==0) {
            if ($Record['LastContainerId']==0)
                return 'Nothing to ship' ;
            else
                $Container['Id']=(int)$Record['LastContainerId'];
        }

        $PickOrder = array (
            'Packed'            => 1,
            'PackedUserId'  => $User['Id'],
            'PackedDate'        => dbDateEncode(time()),
            'LastContainerId' => (int)$Container['Id'],
            'ToId'          => (int)$fields['StockId']['value']
        ) ;
        if ($Navigation['Parameters'] == 'partship') {
            $PickOrder['Packed']=0 ;
            $PickOrder['Printed']=0 ;
        }
        tableWrite ('PickOrder', $PickOrder, $Id) ;

        if ($Record['Type']=='SalesOrder') {
            $shipment = array (
                'Ready'         => 1,
                'ReadyUserId'   => $User['Id'],
                'ReadyDate'     => dbDateEncode(time()),
                'Departed'          => 1,
                'DepartedUserId'    => $User['Id'],
                'DepartedDate'      => dbDateEncode(time()),
                'PartShip'          => 0
            ) ;
            if ($Navigation['Parameters'] == 'partship') $shipment['PartShip'] = 1 ;
        } else {
            $shipment = array (
                'Ready'         => 1,
                'ReadyUserId'   => $User['Id'],
                'ReadyDate'     => dbDateEncode(time()),
                'Departed'          => 0,
                'DepartedUserId'    => $User['Id'],
                'DepartedDate'      => dbDateEncode(time()),
                'Invoiced'          => 1,
                'InvoicedUserId'    => $User['Id'],
                'InvoicedDate'      => dbDateEncode(time()),
                'Done'          => 0,
                'DoneUserId'    => $User['Id'],
                'DoneDate'      => dbDateEncode(time())
            ) ;
        }
        $_shipmentid = tableWrite ('Stock', $shipment, (int)$fields['StockId']['value']) ;

        // Genrate invoice
        $_ordertypeid = $Record['OrderTypeId'] ;
        $query = sprintf ('SELECT * FROM Stock WHERE Id=%d AND Active=1', $_shipmentid) ;
        $res = dbQuery ($query) ;
        $Record = dbFetch ($res) ;
        dbQueryFree ($res) ;
        if ($Record['OrderTypeId']==10) {
            $Record['SetReady'] = 0 ;
        } else {
            $Record['SetReady'] = 0 ;
        }
        $Record['SetReady'] = 1 ;
        $Record['GoToShipment'] = $_shipmentid ;
        $Record['PreInvoiceId'] = $_preinvoiceid ;
        $Record['PreInvoiceOrderId'] = $_reforderid ;
        $Record['OrderTypeId']=$_ordertypeid ;
        require_once _LEGACY_LIB.'/module/invoice/generate-cmd.inc' ;

        return true;
    }

}
