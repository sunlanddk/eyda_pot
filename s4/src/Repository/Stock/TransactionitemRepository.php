<?php

namespace App\Repository\Stock;

use App\Entity\Stock\Transactionitem;
use App\Repository\HelperFunctions\SqlhelperRepo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Transactionitem|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transactionitem|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transactionitem[]    findAll()
 * @method Transactionitem[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionitemRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_sqlHelper;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        SqlhelperRepo $sqlhelperRepo
    )
    {
        parent::__construct($registry, Transactionitem::class);
        $this->_em = $em;
        $this->_sqlHelper = $sqlhelperRepo;

    }

    public function transform(Transactionitem $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
        ];
    }

    public function createTransactionItem($transactionId, $objectLineId, $objectQuantityId, $articleSizeId, $variantcodeId, $fromStock, $toStock, $auto, $quantity = 0, $value = 0, $userId = 1){
        $transaction = new Transactionitem();
        $transaction->setCreatedate(new \DateTime())
                    ->setCreateuserid($userId)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid($userId)
                    ->setTransactionid($transactionId)
                    ->setObjectlineid($objectLineId)
                    ->setObjectquantityid($objectQuantityId)
                    ->setArticlesizeid($articleSizeId)
                    ->setFromstockid($fromStock)
                    ->setTostockid($toStock)
                    ->setValue($value)
                    ->setQuantity($quantity)
                    ->setAuto($auto)
                    ->setVariantcodeid($variantcodeId)
        ;

    	$this->_em->persist($transaction);
    	$this->_em->flush();

    	return $transaction->getId();

    }

    public function updateTransactionitemAuto($auto, $transactionitemId){
        $item = $this->find($transactionitemId);
        $item->setAuto($auto);

        $this->_em->persist($item);
        $this->_em->flush();

        return $item->getId();
    }

    public function updateTransactionitemPrice($price, $transactionitemId){
        $item = $this->find($transactionitemId);
        $item->setValue($price);

        $this->_em->persist($item);
        $this->_em->flush();

        return $item->getId();
    }

    public function updateTransactionitemQty($qty, $transactionitemId){
        $item = $this->find($transactionitemId);
        $item->setQuantity($price);

        $this->_em->persist($item);
        $this->_em->flush();

        return $item->getId();
    }

}
