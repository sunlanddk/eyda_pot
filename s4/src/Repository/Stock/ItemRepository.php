<?php

namespace App\Repository\Stock;

use App\Entity\Order\Orderline;
use App\Entity\Stock\Container;
use App\Entity\Stock\Item;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ItemRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_itemOperationsRepo;
    private $_transactionitemRepository;
    private $_transactionRepository;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        TransactionitemRepository $transactionitemRepository,
        TransactionRepository $transactionRepository
    ) {
        parent::__construct($registry, Item::class);
        $this->_em = $em;
        $this->_transactionitemRepo = $transactionitemRepository;
        $this->_transactionRepository = $transactionRepository;
    }

    public function transform(Item $item)
    {
        return [
            'id' => (int)$item->getId(),
            'createdate' => (string)$item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid' => (int)$item->getCreateuserid(),
            'modifydate' => (string)$item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid' => (int)$item->getCreateuserid(),
            'active' => (int)$item->getActive(),
            'parentid' => (int)$item->getParent()->getId(),
            'articleid' => (int)$item->getArticleid(),
            'containerid' => (int)$item->getContainer()->getId(),
            'quantity' => (int)$item->getQuantity(),
            'sortation' => (int)$item->getSortation(),
            'batchnumber' => (string)$item->getBatchnumber(),
            'requisitionid' => (int)$item->getRequisitionid(),
            'requisitionlineid' => (int)$item->getRequisitionlineid(),
            'ownerid' => (int)$item->getOwnerid(),
            'transactionitemid' => (int)$item->getTransactionitemid(),
        ];
    }

    public function refundItem($variantcode, $transactionId, $qty, $containerId, $stockId = 0)
    {
        $transactionItemId = $this->_transactionitemRepo->createTransactionitem($transactionId, 0, 0, $variantcode['articlesizeid'], $variantcode['id'], $stockId, 14321, 0, $qty);

        $conn = $this->_em->getConnection();
        if ($stockId > 0) {
            $sql = sprintf(
                "
                    SELECT i.Id
                    FROM (item i, container c) 
                    WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND c.Id=i.ContainerId AND c.Id!=135400 AND c.StockId=%d AND c.Active=1
                ",
                $variantcode['articleid'],
                $variantcode['articlecolorid'],
                $variantcode['articlesizeid'],
                $stockId
            );
        } else {
            $sql = sprintf(
                "
                    SELECT i.Id
                    FROM (item i, container c) 
                    WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND c.Id=i.ContainerId AND c.Id!=135400 AND c.StockId=14321 AND c.Active=1
                ",
                $variantcode['articleid'],
                $variantcode['articlecolorid'],
                $variantcode['articlesizeid']
            );
        }

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll();

        $price = $this->getItemPrice($variantcode, $stockId);

        /** @var Container $container */
        $container = $this->_em->find(Container::class, $containerId);
        /** @var Orderline $orderLine */
        $orderLine = $this->_em->find(Orderline::class, 0);

        foreach ($items as $key => $value) {
            /** @var Item $item */
            $item = $this->find($value['Id']);
            // return $this->transform($item);
            $newItem = clone $item;
            $newItem->setCreatedate(new DateTime());
            $newItem->setCreateuserid(1);
            $newItem->setModifydate(new DateTime());
            $newItem->setModifyuserid(1);
            $newItem->setContainer($container);
            $newItem->setQuantity($qty);
            $newItem->setParent($item);
            $newItem->setOrderLine($orderLine);
            $newItem->setPreviouscontainerid(
                $item->getContainer() ? $item->getContainer()->getId() : 0
            );
            $newItem->setTransactionitemid($transactionItemId);

            if ((float)$item->getPrice() > 0) {
                $price = (float)$item->getPrice();
            } else {
                $newItem->setPrice((float)$price);
            }

            $this->_em->persist($newItem);
            $this->_em->flush();

            if ($stockId > 0) {
                if ((int)$item->getQuantity() == (int)$qty) {
                    $item->setActive(0);
                    $this->_em->persist($item);
                    $this->_em->flush();
                } else {
                    $item->setQuantity($item->getQuantity() - $qty);
                    $this->_em->persist($item);
                    $this->_em->flush();
                }
            }
            $this->_transactionitemRepo->updateTransactionitemPrice($price, $transactionItemId);

            return $newItem->getId();
        }


        if (count($items) == 0) {
            $this->_transactionitemRepo->updateTransactionitemAuto(1, $transactionItemId);
            return $this->createSimpleItem($transactionItemId, $qty, $containerId, $variantcode);
        }
    }

    public function getItemPrice($variantcode, $stockId = 0)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT MAX(i.Price) as Price
                FROM (item i, container c)
                WHERE 
                i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND i.ContainerId=c.Id AND c.StockId=%d
            ",
            $variantcode['articleid'],
            $variantcode['articlecolorid'],
            $variantcode['articlesizeid'],
            14321
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetch();
        if (isset($items['Price']) === true) {
            if ((float)$items['Price'] > 0) {
                return (float)$items['Price'];
            }
        }

        $sql = sprintf(
            "
                SELECT MAX(i.Price) as Price
                FROM (item i, container c)
                WHERE 
                i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=0 AND i.ContainerId=c.Id AND c.StockId=%d
            ",
            $variantcode['articleid'],
            $variantcode['articlecolorid'],
            $variantcode['articlesizeid'],
            14321
        );
        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetch();
        if (isset($items['Price']) === true) {
            return (float)$items['Price'];
        }

        return 0;
    }

    public function createSimpleItem($transactionitemId, $qty, $containerId, $variantcode, $orderlineId = 0)
    {
        $price = $this->getItemPrice($variantcode, 0);

        /** @var Orderline $orderLine */
        $orderLine = $this->_em->find(Orderline::class, $orderlineId);
        /** @var Container $container */
        $container = $this->_em->find(Container::class, $containerId);
        /** @var Item $parent */
        $parent = $this->_em->find(Item::class, 0);

        $item = new Item;
        $item->setModifydate(new DateTime())
            ->setModifyuserid(1)
            ->setCreatedate(new DateTime())
            ->setCreateuserid(1)
            ->setActive(1)
            ->setArticleid($variantcode['articleid'])
            ->setArticlecolorid($variantcode['articlecolorid'])
            ->setArticlesizeid($variantcode['articlesizeid'])
            ->setContainer($container)
            ->setPreviouscontainerid(0)
            ->setComment('')
            ->setPrice($price)
            ->setSortation(1)
            ->setBatchnumber('')
            ->setRequisitionid(0)
            ->setRequisitionlineid(0)
            ->setOwnerid(787)
            ->setQuantity($qty)
            ->setParent($parent)
            ->setOrderLine($orderLine)
            ->setTransactionitemid($transactionitemId);
        $this->_em->persist($item);
        $this->_em->flush();

        $this->_transactionitemRepo->updateTransactionitemPrice($price, $transactionitemId);

        return $item->getId();
    }

    public function updateStockByOffice($variants, $userId, $containerId = 140969, $comment = '')
    {
        if ($containerId === 140969) {
            $containerId = $this->getPickingContainer();
        }

        $InvLoss = $this->_transactionRepository->createTransaction('InvLoss', 0, 14321, 0, 0, 0, $userId, $comment, 1);
        $TotalInvLoss = 0;
        $TotalInvLossQty = 0;
        $InvPlus = $this->_transactionRepository->createTransaction('InvPlus', 0, 14321, 14321, 0, 0, $userId, $comment, 1);
        $TotalInvPlus = 0;
        $TotalInvPlusQty = 0;
        foreach ($variants as $key => $variant) {
            if ((int)$variant['changed'] < 0) {
                $qty = ((int)$variant['changed'] * -1);
                $negative = true;
            } else {
                $qty = (int)$variant['changed'];
                $negative = false;
            }

            $conn = $this->_em->getConnection();

            if ((int)$this->tableGetField('variantcode', 'PickContainerId', $variant['variantcodeid']) > 0) {
                $sql = sprintf(
                    "
                        SELECT i.Id, i.ContainerId
                        FROM (item i, container c) 
                        WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND i.ContainerId=%d AND c.Id=i.ContainerId AND c.StockId=14321 AND c.Active=1
                    ",
                    (int)$this->tableGetField('variantcode', 'ArticleId', $variant['variantcodeid']),
                    (int)$this->tableGetField('variantcode', 'ArticleColorId', $variant['variantcodeid']),
                    (int)$this->tableGetField('variantcode', 'ArticleSizeId', $variant['variantcodeid']),
                    (int)$this->tableGetField('variantcode', 'PickContainerId', $variant['variantcodeid'])
                );
            } else {
                $sql = sprintf(
                    "
                        SELECT i.Id, i.ContainerId
                        FROM (item i, container c) 
                        WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND c.Id=i.ContainerId AND c.Id!=135400 AND c.StockId=14321 AND c.Active=1
                    ",
                    $this->tableGetField('variantcode', 'ArticleId', $variant['variantcodeid']),
                    $this->tableGetField('variantcode', 'ArticleColorId', $variant['variantcodeid']),
                    $this->tableGetField('variantcode', 'ArticleSizeId', $variant['variantcodeid'])
                );
            }

            $stmt = $conn->prepare($sql);
            $stmt->execute();
            $items = $stmt->fetchAll();

            if ($negative === true) {
                $TotalInvLossQty += $qty;
                // remove/down adjust items
                $transactionItemId = $this->_transactionitemRepo->createTransactionitem(
                    $InvLoss,
                    0,
                    0,
                    $this->tableGetField('variantcode', 'ArticleSizeId', $variant['variantcodeid']),
                    $variant['variantcodeid'],
                    14321,
                    0,
                    0,
                    $qty,
                    0,
                    $userId
                );
                foreach ($items as $key => $itemArr) {
                    if ((int)$qty === 0) {
                        continue;
                    }

                    $item = $this->find($itemArr['Id']);
                    $this->_transactionitemRepo->updateTransactionitemPrice($item->getPrice(), $transactionItemId);
                    $TotalInvLoss += $item->getPrice();
                    $qty = $this->splitItem($item, $qty, 0, 0, 0);
                }
            } else {
                $TotalInvPlusQty += $qty;

                $transactionItemId = $this->_transactionitemRepo->createTransactionitem(
                    $InvPlus,
                    0,
                    0,
                    $this->tableGetField('variantcode', 'ArticleSizeId', $variant['variantcodeid']),
                    $variant['variantcodeid'],
                    14321,
                    14321,
                    0,
                    $qty,
                    0,
                    $userId
                );

                // add item clones or new item
                if (count($items) > 0) {
                    $first = 0;
                    foreach ($items as $key => $itemArr) {
                        if ($first > 0) {
                            continue;
                        }

                        $item = $this->find($itemArr['Id']);
                        $TotalInvPlus += $item->getPrice();
                        $this->_transactionitemRepo->updateTransactionitemPrice($item->getPrice(), $transactionItemId);
                        $this->cloneItem($item, $qty, $containerId, $userId, $transactionItemId);
                        $first = 100;
                    }
                } else {
                    $containerId = (int)$this->tableGetField('variantcode', 'PickContainerId', $variant['variantcodeid']);
                    if ((int)$containerId === 0) {
                        $containerId = $this->getPickingContainer();
                    }

                    $variantcode = array(
                        "articleid" => $this->tableGetField('variantcode', 'ArticleId', $variant['variantcodeid']),
                        "articlecolorid" => $this->tableGetField('variantcode', 'ArticleColorId', $variant['variantcodeid']),
                        "articlesizeid" => $this->tableGetField('variantcode', 'ArticleSizeId', $variant['variantcodeid'])
                    );
                    $this->_transactionitemRepo->updateTransactionitemAuto(1, $transactionItemId);
                    $this->createSimpleItem($transactionItemId, $qty, $containerId, $variantcode);
                }
            }
        }

        $this->_transactionRepository->updateTransaction($InvLoss, $TotalInvLoss, $TotalInvLossQty);
        $this->_transactionRepository->updateTransaction($InvPlus, $TotalInvPlus, $TotalInvPlusQty);
        return true;
    }

    public function getPickingContainer()
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT *
                FROM parameter
                WHERE Mark='PickContainerId'
            "
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $parameter = $stmt->fetch();

        return (int)$parameter['Value'];
    }

    private function tableGetField($table, $field, $id)
    {
        $query = sprintf("SELECT %s AS Value FROM %s WHERE Id=%d", $field, $table, $id);
        $conn = $this->_em->getConnection();
        $stmt = $conn->prepare($query);
        $stmt->execute();
        $row = $stmt->fetch();
        return $row["Value"];
    }

    //public function createSimpleItem($transactionitemId, $qty, $containerId, $variantcode, $userId = 1, $previousContainerId = 0){

    private function splitItem(Item $item, $qty, $orderlineId, $transactionitemId, $containerId = 0)
    {
        /** @var Orderline|null $orderLine */
        $orderLine = $this->_em->find(Orderline::class, $orderlineId);
        /** @var Container|null $container */
        $container = $this->_em->find(Container::class, $containerId);

        if ((int)$qty == (int)$item->getQuantity()) {
            //return 'equal';
            $item->setModifydate(new DateTime());
            $item->setModifyuserid(1);
            $item->setActive(0);
            $this->_em->persist($item);
            $this->_em->flush();

            $newItem = clone $item;
            $newItem->setCreatedate(new DateTime());
            $newItem->setCreateuserid(1);
            $newItem->setModifydate(new DateTime());
            $newItem->setModifyuserid(1);
            $newItem->setContainer($container);
            $newItem->setPreviouscontainerid(
                $item->getContainer() ? $item->getContainer()->getId() : 0
            );
            $newItem->setActive(1);
            $newItem->setParent($item);
            $newItem->setOrderLine($orderLine);
            $newItem->setTransactionitemid($transactionitemId);

            $this->_em->persist($newItem);
            $this->_em->flush();

            return 0;
        }

        if ((int)$qty < $item->getQuantity()) {
            //return 'qty less than on stock';
            $item->setQuantity((int)$item->getQuantity() - ($qty));
            $item->setModifydate(new DateTime());
            $item->setModifyuserid(1);
            $this->_em->persist($item);
            $this->_em->flush();

            /** @var Item $newItem */
            $newItem = clone $item;
            $newItem->setCreatedate(new DateTime());
            $newItem->setCreateuserid(1);
            $newItem->setModifydate(new DateTime());
            $newItem->setModifyuserid(1);
            $newItem->setContainer($container);
            $newItem->setPreviouscontainerid(
                $item->getContainer() ? $item->getContainer()->getId() : 0
            );
            $newItem->setQuantity($qty);
            $newItem->setParent($item);
            $newItem->setOrderLine($orderLine);
            $newItem->setTransactionitemid($transactionitemId);

            $this->_em->persist($newItem);
            $this->_em->flush();

            return 0;
        }

        $item->setActive(0);
        $item->setModifydate(new DateTime());
        $item->setModifyuserid(1);
        $this->_em->persist($item);
        $this->_em->flush();

        /** @var Item $newItem */
        $newItem = clone $item;
        $newItem->setCreatedate(new DateTime());
        $newItem->setCreateuserid(1);
        $newItem->setModifydate(new DateTime());
        $newItem->setModifyuserid(1);
        $newItem->setContainer($container);
        $newItem->setPreviouscontainerid(
            $item->getContainer() ? $item->getContainer()->getId() : 0
        );
        $newItem->setActive(1);
        $newItem->setParent($item);
        $newItem->setOrderLine($orderLine);
        $newItem->setTransactionitemid($transactionitemId);

        $this->_em->persist($newItem);
        $this->_em->flush();

        return $qty - $item->getQuantity();
    }

    private function cloneItem(Item $item, $qty, $containerId, $userId = 0, $transactionitemId = 0)
    {
        /** @var Orderline $orderLine */
        $orderLine = $this->_em->find(Orderline::class, 0);

        $newItem = clone $item;
        $newItem->setCreatedate(new DateTime());
        $newItem->setCreateuserid($userId);
        $newItem->setModifydate(new DateTime());
        $newItem->setModifyuserid($userId);
        // $newItem->setContainerId($containerId);
        $newItem->setActive(1);
        $newItem->setParent($item);
        $newItem->setOrderLine($orderLine);
        $newItem->setQuantity($qty);
        $newItem->setTransactionitemid($transactionitemId);

        $this->_em->persist($newItem);
        $this->_em->flush();

        return true;
    }

    public function moveItemsToShipment($orderlineId, $quantityId, $articleId, $articleColorId, $articleSizeId, $qty, $transactionId, $containerId, $pickcontainerid)
    {
        $origQty = $qty;

        $conn = $this->_em->getConnection();

        if ((int)$pickcontainerid > 0) {
            $sql = sprintf(
                "
                    SELECT i.Id, i.Quantity
                    FROM (item i, container c) 
                    WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND i.ContainerId=%d AND c.Id=i.ContainerId AND c.StockId=14321 AND c.Active=1
                ",
                $articleId,
                $articleColorId,
                $articleSizeId,
                $pickcontainerid
            );
        } else {
            $sql = sprintf(
                "
                    SELECT i.Id, i.Quantity
                    FROM (item i, container c) 
                    WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND c.Id=i.ContainerId AND c.Id!=135400 AND c.StockId=14321 AND c.Active=1
                ",
                $articleId,
                $articleColorId,
                $articleSizeId
            );
        }


        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll();

        if (count($items) > 0) {
            $lastPrice = 0;
            $transactionItemId = $this->_transactionitemRepo->createTransactionitem(
                $transactionId,
                $orderlineId,
                $quantityId,
                $articleSizeId,
                $this->getVariantCodeId($articleId, $articleColorId, $articleSizeId),
                14321,
                0,
                0,
                $qty
            );
            foreach ($items as $key => $itemArr) {
                $item = $this->find($itemArr['Id']);

                /*if((int)$qty === 0){
                    $this->_transactionitemRepo->updateTransactionitemPrice((string)$lastPrice, $transactionItemId);
                    return array($origQty, $item->getPrice());
                }*/
                $lastPrice = $item->getPrice();

                $qty = $this->splitItem($item, $qty, $orderlineId, $transactionItemId, $containerId);

                if ((int)$qty === 0) {
                    $this->_transactionitemRepo->updateTransactionitemPrice((string)$lastPrice, $transactionItemId);
                    return array($origQty, $item->getPrice());
                }
                //return $qty = $this->splitItem($item, $qty, $orderlineId, 0, 0);
            }

            $this->_transactionitemRepo->updateTransactionitemPrice((string)$lastPrice, $transactionItemId);
            return array($origQty, $lastPrice);
        } else {
            $transactionItemId = $this->_transactionitemRepo->createTransactionitem(
                $transactionId,
                $orderlineId,
                $quantityId,
                $articleSizeId,
                $this->getVariantCodeId($articleId, $articleColorId, $articleSizeId),
                14321,
                0,
                1,
                $qty
            );
            $this->createSimpleItem(
                $transactionItemId,
                $origQty,
                $containerId,
                array(
                    'articleid' => $articleId,
                    'articlecolorid' => $articleColorId,
                    'articlesizeid' => $articleSizeId
                ),
                $orderlineId
            );
            return array($origQty, 0);
        }
    }

    public function getVariantCodeId($articleId, $articleColorId, $articleSizeId)
    {
        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                    SELECT Id
                    FROM VariantCode 
                    WHERE ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Active=1
                ",
            $articleId,
            $articleColorId,
            $articleSizeId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetch();

        return (int)$items['Id'];
    }

    public function inventoryItem($orderlineId, $quantityId, $articleId, $articleColorId, $articleSizeId, $qty, $transactionId)
    {
        //$item = $this->findBy(array('articleid' => $articleId, 'articlecolorid' => $articleColorId, 'articlesizeid' => $articleSizeId));
        $origQty = $qty;

        // $qb = $this->createQueryBuilder('i');
        // $qb->where('i.articleid = :articleid','i.articlecolorid = :articlecolorid','i.articlesizeid = :articlesizeid','i.active = 1','i.containerid > 0')
        //    ->setParameter('articleid', $articleId)
        //    ->setParameter('articlecolorid', $articleColorId)
        //    ->setParameter('articlesizeid', $articleSizeId);

        // $items = $qb->getQuery()
        //       ->getResult();

        $conn = $this->_em->getConnection();
        $sql = sprintf(
            "
                SELECT i.Id
                FROM (item i, container c) 
                WHERE i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1 AND c.Id=i.ContainerId AND c.Id!=135400 AND c.StockId=14321 AND c.Active=1
            ",
            $articleId,
            $articleColorId,
            $articleSizeId
        );

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $items = $stmt->fetchAll();

        $lastPrice = 0;
        $transactionItemId = $this->_transactionitemRepo->createTransactionitem(
            $transactionId,
            $orderlineId,
            $quantityId,
            $articleSizeId,
            $this->getVariantCodeId($articleId, $articleColorId, $articleSizeId),
            14321,
            0,
            0,
            $qty
        );
        foreach ($items as $key => $itemArr) {
            $item = $this->find($itemArr['Id']);
            if ((int)$qty === 0) {
                $this->_transactionitemRepo->updateTransactionitemPrice($item->getPrice(), $transactionItemId);
                return array($origQty, $item->getPrice());
                return true;
            }
            $lastPrice = $item->getPrice();

            $qty = $this->splitItem($item, $qty, $orderlineId, $transactionItemId);
        }

        $this->_transactionitemRepo->updateTransactionitemPrice((string)$lastPrice, $transactionItemId);
        return array($origQty, $lastPrice);
        return true;
    }


}
