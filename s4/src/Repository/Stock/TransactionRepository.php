<?php

namespace App\Repository\Stock;

use App\Entity\Stock\Transaction;
use App\Repository\HelperFunctions\SqlhelperRepo;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Transaction|null find($id, $lockMode = null, $lockVersion = null)
 * @method Transaction|null findOneBy(array $criteria, array $orderBy = null)
 * @method Transaction[]    findAll()
 * @method Transaction[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TransactionRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;
    private $_sqlHelper;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em,
        SqlhelperRepo $sqlhelperRepo
    )
    {
        parent::__construct($registry, Transaction::class);
        $this->_em = $em;
        $this->_sqlHelper = $sqlhelperRepo;

    }

    public function transform(Transaction $item) {
        return [
            'id'                => (int) $item->getId(),
            'createdate'        => (string) $item->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $item->getCreateuserid(),
            'modifydate'        => (string) $item->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $item->getCreateuserid(),
            'active'      		=> (int) $item->getActive(),
        ];
    }

    public function createTransaction($type, $objectId, $fromStock, $toStock, $value = 0, $quantity = 0, $userId = 1, $comment = '', $online = 0){
        $transaction = new Transaction();
        $transaction->setCreatedate(new \DateTime())
                    ->setCreateuserid($userId)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid($userId)
                    ->setObjectid($objectId)
                    ->setType($type)
                    ->setFromstockid($fromStock)
                    ->setTostockid($toStock)
                    ->setValue($value)
                    ->setComment($comment)
                    ->setQuantity($quantity)
                    ->setOnline((int)$online)
        ;

    	$this->_em->persist($transaction);
    	$this->_em->flush();

    	return $transaction->getId();

    }

    public function updateTransaction($id, $value = 0, $quantity = 0, $userId = 1){
        $active=1;
        if((int)$quantity === 0){
            $active=0;
        }
        $transaction = $this->find($id);
        $transaction->setModifydate(new \DateTime())
                    ->setModifyuserid($userId)
                    ->setValue($value)
                    ->setQuantity($quantity)
                    ->setActive($active)
        ;

    	$this->_em->persist($transaction);
    	$this->_em->flush();

    	return $transaction->getId();
    }



  //   public function updateVariantcode($variantCodeId, $shopifyProductId, $shopifyVariantId, $shopifyInventoryId){
  //       $variant = $this->find($variantCodeId);
  //       $variant->setShopifyvariantid($shopifyVariantId);
  //       $variant->setShopifyproductid($shopifyProductId);
  //       $variant->setInventoryitemid($shopifyInventoryId);

  //       $this->_em->persist($variant);
  //       $this->_em->flush();

  //       return $variant->getId();
  //   }

}
