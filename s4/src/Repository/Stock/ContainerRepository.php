<?php

namespace App\Repository\Stock;

use App\Entity\Stock\Container;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use App\Entity\Stock\Stock;

/**
 * @method Item|null find($id, $lockMode = null, $lockVersion = null)
 * @method Item|null findOneBy(array $criteria, array $orderBy = null)
 * @method Item[]    findAll()
 * @method Item[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ContainerRepository extends ServiceEntityRepository
{
    protected $_em;
    private $_logger;

    public function __construct(
        ManagerRegistry $registry,
        EntityManagerInterface $em
    )
    {
        parent::__construct($registry, Container::class);
        $this->_em = $em;
    }

    public function transform(Container $container) {
        return [
            'id'                => (int) $container->getId(),
            'createdate'        => (string) $container->getCreatedate()->format('Y-m-d H:i:s'),
            'createuserid'      => (int) $container->getCreateuserid(),
            'modifydate'        => (string) $container->getModifydate()->format('Y-m-d H:i:s'),
            'modifyuserid'      => (int) $container->getCreateuserid(),
            'active'      		=> (int) $container->getActive(),
            'stockid'      	    => (int) $container->getStock()->getId(),
            'positon'      	    => (string) $container->getPosition(),
            'containertypeid'   => (int) $container->getContainertypeid(),
        ];
    }

    public function getContainerById($containerid){
        $container = $this->find($containerid);
        if($container !== null){
            return $this->transformSimple($container);
        }

        return array(
            'id'                => (int) 0,
            'stockid'           => (int) 14321,
            'position'           => '',
            'containertypeid'   => (int) 0,
        );
    }

    public function getContainerByPosition($position){
        $container = $this->findOneBy(array('active' => 1, 'position' => $position));
        if($container !== null){
            return $this->transformSimple($container);
        }

        return false;
    }

    public function transformSimple(Container $container) {
        return [
            'id'                => (int) $container->getId(),
            'stockid'           => (int) $container->getStock()->getId(),
            'position'           => (string) $container->getPosition(),
            'containertypeid'   => (int) $container->getContainertypeid(),
        ];
    }

    public function createContainer($stockId = 14321, $containertypeid = 33, $position = '')
    {
        /** @var Stock|null $stock */
        $stock = $this->_em->getRepository(Stock::class)->find($stockId);

        $container = new Container;
        $container->setCreatedate(new \DateTime());
        $container->setCreateuserid(1);
        $container->setModifydate(new \DateTime());
        $container->setModifyuserid(1);
        $container->setActive(1);
        $container->setStock($stock);
        $container->setContainertypeid((int)$containertypeid);
        $container->setposition($position);

        $this->_em->persist($container);
        $this->_em->flush();

        return $container->getId();
    }
}
