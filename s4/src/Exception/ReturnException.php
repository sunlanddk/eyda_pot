<?php

namespace App\Exception;

use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

class ReturnException extends RuntimeException
{
    /**
     * @param  string|null  $message
     * @param  int  $code
     */
    public function __construct(string $message = null, int $code = Response::HTTP_BAD_REQUEST)
    {
        parent::__construct($message ?? 'Return line already exists.', $code);
    }
}
