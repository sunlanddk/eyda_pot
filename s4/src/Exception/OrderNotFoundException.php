<?php

namespace App\Exception;

use RuntimeException;
use Symfony\Component\HttpFoundation\Response;

class OrderNotFoundException extends RuntimeException
{
    /**
     * @param  string|null  $message
     * @param  int  $code
     */
    public function __construct(string $message = null, int $code = Response::HTTP_NOT_FOUND)
    {
        parent::__construct($message ?? 'No order found.', $code);
    }
}
