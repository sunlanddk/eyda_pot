<?php 

// src/Command/CreateUserCommand.php
namespace App\Command;

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

use App\Repository\PimRepository;
use App\Repository\Order\RefundRepository;
use App\Repository\Cron\CronJobRepo;
use App\Repository\Cron\CronRepo;


class  CustomCron extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:custom-cron-test';

    private $_cronJobRepo;
    private $_cronRepo;
    private $_pimRepo;
    private $_refundRepo;

    public function __construct(CronJobRepo $cronJobRepo, CronRepo $cronRepo, PimRepository $pimRepo, RefundRepository $refundRepo)
    {
        parent::__construct();
        $this->_cronJobRepo = $cronJobRepo;
        $this->_cronRepo = $cronRepo;
        $this->_pimRepo = $pimRepo;
        $this->_refundRepo = $refundRepo;
    }

    protected function configure()
    {
        // ...
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Run cron job list');

        // Setup scheduled task som kører hvert 5 minut. 
        // lig cron fil og kig om den stadig er der.
        // hvis filen har ligget i mere end 30 min, slet og kør igen
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {   
        set_time_limit(600);
        if($this->_cronRepo->getCronTimeStamp() > 0){
            if((time() - $this->_cronRepo->getCronTimeStampTime()) > 1800){
                $this->_cronRepo->updateTimeOverdue();
            }
            /*echo PHP_EOL;
            echo 'Cron is already running.';
            echo PHP_EOL;
            */
            return 0;
        }

        $output->writeln('Cron started.');

        // Create stamp in db
        //$cronTimeStamp = $this->_cronRepo->CreateCronTimeStamp();
        if(count($this->_cronJobRepo->getCronJobs()) > 0){
            $cronTimeStamp = $this->_cronRepo->CreateCronTimeStamp();
        }
        else{
            return 0;
        }

        foreach ($this->_cronJobRepo->getCronJobs() as $key => $cronJob) {
            $return = $this->cronFunctions($cronJob);
            // print_r($return);
            $output->writeln('Job: '.$cronJob['cron_function'].' Ran.');
        }
        
        $this->_cronRepo->updateCronTimeStamp($cronTimeStamp);
        //$this->_cronRepo->deleteOldStamps();
        $output->writeln('Cron ended.');
        return 0;
    }

    private function repoSwitcher($function){
        $repo = '';
        switch ($function) {
            case 'refundInShopifyCron':
                $repo = '_refundRepo';
                break;
            case 'article':
            case 'getCronJobs':
                $repo = '_cronJobRepo';
                break;

            case 'deleteProduct':
            case 'publishProduct':
            case 'GetDescriptionFromShop':
                $repo = '_pimRepo';
                break;
            
            default:
                $repo = false;   
                break;
        }

        return $repo;
    }

    private function cronFunctions($_cronJob){
        $repo = $this->repoSwitcher($_cronJob['cron_function']); 
        if($repo === false){
            return false;
        }

        $function = $_cronJob['cron_function'];
        $value = explode(',', $_cronJob['cron_value']);

        
        $return = $this->$repo->$function($value);

        // return $return;
        if($return !== true){
            $this->_cronJobRepo->updateCronJob($_cronJob['id'], json_encode($return));
        }
        else{
            $this->_cronJobRepo->updateCronJob($_cronJob['id']);
        }
        
        return true;
    }
}

?>