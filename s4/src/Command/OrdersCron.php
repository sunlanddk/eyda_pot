<?php

// src/Command/CreateUserCommand.php
namespace App\Command;

use App\Repository\Order\OrdersRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class  OrdersCron extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:orders-cron';

    private $_ordersRepository;

    public function __construct(OrdersRepository $ordersRepository)
    {
        parent::__construct();
        $this->_ordersRepository = $ordersRepository;
    }

    protected function configure()
    {
        // ...
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Run cron job list');

        // Setup scheduled task som kører hvert 5 minut.
        // lig cron fil og kig om den stadig er der.
        // hvis filen har ligget i mere end 30 min, slet og kør igen
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        $output->writeln('Orders cron started.');
        $this->_ordersRepository->checkForPickorders();
        $output->writeln('Orders cron ended.');
        return 0;
    }

}

?>
