<?php

// src/Command/CreateUserCommand.php
namespace App\Command;

use App\Repository\Cron\CronInventoryLockRepo;
use App\Repository\Cron\InventoryRepository;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class  InventoryCron extends Command
{
    // the name of the command (the part after "bin/console")
    protected static $defaultName = 'app:inventory-cron';

    private $_inventoryRepository;
    private $_cronInventoryLockRepo;

    public function __construct(InventoryRepository $inventoryRepository, CronInventoryLockRepo $cronInventoryLockRepo)
    {
        parent::__construct();
        $this->_inventoryRepository = $inventoryRepository;
        $this->_cronInventoryLockRepo = $cronInventoryLockRepo;
    }

    protected function configure()
    {
        // ...
        $this
        // the short description shown while running "php bin/console list"
        ->setDescription('Run cron job list');

        // Setup scheduled task som kører hvert 5 minut.
        // lig cron fil og kig om den stadig er der.
        // hvis filen har ligget i mere end 30 min, slet og kør igen
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        set_time_limit(0);
        if($this->_cronInventoryLockRepo->getCronTimeStamp() === true){
            $output->writeln('Cron still running.');
            return 0;
        }

        $this->_cronInventoryLockRepo->CreateCronTimeStamp();

        $output->writeln('Cron started.');

        $this->_inventoryRepository->getAllVariantsToExport();

        $this->_cronInventoryLockRepo->updateCronTimeStamp();
        $output->writeln('Cron ended.');
        return 0;
    }

}

?>
