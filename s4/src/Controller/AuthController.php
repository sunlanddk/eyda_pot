<?php
namespace App\Controller;

use App\Repository\AuthRepository;
use App\Repository\TwoFactorRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class AuthController extends ApiController {


    /**
     * @Route("/user/login", methods="POST")
     */
    public function login(
        Request $request,
        AuthRepository $authRepo
    ) {

        $data = json_decode($request->getContent(), true);
        $loginname = $data['loginname'];
        $password = $data['password'];
        
        $user = $authRepo->getUserByLoginnameAndPassword($loginname, $password);

        if(gettype($user) == "string"){
            return $this->respond(
                array(
                    'error'             => true,
                    'errormessage'      => $user,
                    'data'              => ''
                )
            );
        }

        $isTwoFactorAuth = $user["2FA"] != "";
        
        $uid = $isTwoFactorAuth ? "" : $authRepo->createSession($loginname);

        return $this->respond(
            array(
                'error'             => false,
                'errormessage'      => '',
                'data'              => array("uid" => $uid, "isTwoFactor" => $isTwoFactorAuth)
            )
        );

    }


    /**
    * @Route("/user/check/cookie", methods="POST")
    */
    public function checkCookie(
        Request $request,
        AuthRepository $authRepo
    ) {
        $data = json_decode($request->getContent(), true);
        $clientIp = $request->getClientIp();
        $cookie = $data["cookie"];

        $isCookieValid =  $authRepo->checkCookie($clientIp, $cookie);        

        if ($isCookieValid) {
            return $this->respond(
                array(
                    'error'             => false,
                    'errormessage'      => '',
                    'data'              => "valid cookie"
                )
            );
        }
        return $this->respond(
            array(
                'error'             => true,
                'errormessage'      => 'invalid cookie',
                'data'              => ''
            )
        );
    }
    
    /**
    * @Route("/user/create/{token}", methods="GET")
    */
    public function create2FA(
        TwoFactorRepository $twoFactorRepo,
        $token
    ) {
        $qr = $twoFactorRepo->createQRCode($token);
        
        if ($qr) {
            return $this->respond(
                array(
                    'error'             => false,
                    'errormessage'      => '',
                    'data'              => $qr
                )
            );
        }
        return $this->respond(
            array(
                'error'             => true,
                'errormessage'      => 'Link expired',
                'data'              => ''
            )
        );
    }

    /**
    * @Route("/user/reset/{token}", methods="GET")
    */
    public function reset2FA(
        TwoFactorRepository $twoFactorRepo,
        $token
    ) {
        $qr = $twoFactorRepo->createQRCode($token, true);
        
        if ($qr) {
            return $this->respond(
                array(
                    'error'             => false,
                    'errormessage'      => '',
                    'data'              => $qr
                )
            );
        }
        return $this->respond(
            array(
                'error'             => true,
                'errormessage'      => 'Link expired',
                'data'              => ''
            )
        );
    }

    /**
    * @Route("/user/verify/reset", methods="POST")
    */
    public function verifyCodeForReset2FA(
        TwoFactorRepository $twoFactorRepo,
        Request $request
    ) {
        
        $data = json_decode($request->getContent(), true);

        $token = $data["token"];
        $loginname = $twoFactorRepo->getLoginnameByToken($data["token"]);
        $code = $data['code'];

        $qr = $twoFactorRepo->verifyCode($loginname, $code);

        if ($qr) {
            
            $qr = $twoFactorRepo->createQRCode($token);

            return $this->respond(
                array(
                    'error'             => false,
                    'errormessage'      => '',
                    'data'              => $qr
                )
            );
        }
        return $this->respond(
            array(
                'error'             => true,
                'errormessage'      => 'Wrong code',
                'data'              => ""
            )
        );
    }


    /**
    * @Route("/user/verify/code", methods="POST")
    */
    public function checkTwoFactor(
        TwoFactorRepository $twoFactorRepo,
        AuthRepository $authRepo,
        Request $request
    ) {
        
        $data = json_decode($request->getContent(), true);
        if(isset($data["token"])) {
            $loginname = $twoFactorRepo->getLoginnameByToken($data["token"]);
        } else {
            $loginname = $data['loginname'];
        }
        $code = $data['code'];

        $qr = $twoFactorRepo->verifyCode($loginname, $code);

        if ($qr) {
            $uid = isset($data["token"]) ? "" : $authRepo->createSession($loginname);

            return $this->respond(
                array(
                    'error'             => false,
                    'errormessage'      => '',
                    'data'              => $uid
                )
            );
        }
        return $this->respond(
            array(
                'error'             => true,
                'errormessage'      => 'Wrong code',
                'data'              => ""
            )
        );
    }


    /**
    * @Route("/user/password/reset", methods="POST")
    */
    public function recoverPassword(
        AuthRepository $authRepo,
        Request $request
    ) {
        
        $data = json_decode($request->getContent(), true);
        $email = $data["email"];
        $msg = $authRepo->sendPasswordRecoveryEmail($email);

        return $this->respond(
            array(
                'error'             => false,
                'errormessage'      => '',
                'data'              => $msg
            )
        );
    }


    /**
    * @Route("/user/password/reset/{token}",  methods={"GET","POST"})
    */
    public function passwordReset(
        AuthRepository $authRepo,
        Request $request,
        $token
    ) {

        $userId = $authRepo->decodePasswordToken($token);

        if(!$userId) {
            return $this->respond(
                array(
                    'error'             => true,
                    'errormessage'      => "Token expired",
                    'data'              => ""
                )
            );
        }

        if($request->getMethod() == "POST") {
            $data = json_decode($request->getContent(), true);
            $password = $data["password"];
            
            if($authRepo->resetPassword($userId, $password) == true) {
                return $this->respond(
                    array(
                        'error'             => false,
                        'errormessage'      => '',
                        'data'              => "Password has been changed"
                    )
                );
            }

            return $this->respond(
                array(
                    'error'             => true,
                    'errormessage'      => 'Password has been not changed',
                    'data'              => ""
                )
            );

        }

        return $this->respond(
            array(
                'error'             => false,
                'errormessage'      => '',
                'data'              => ""
            )
        );
    }

    /**
    * @Route("/user/password/hashall",  methods={"GET"})
    */
    public function hashAll(
        AuthRepository $authRepo,
        Request $request
    ) {
        //$authRepo->hashedAllPassword();

        return $this->respond(
                array(
                    'error'             => false,
                    'errormessage'      => '',
                    'data'              => ""
                ));
    }

}
