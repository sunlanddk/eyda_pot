<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\Order\PickorderRepository;

class PickController extends ApiController {

    /**
    * @Route("/pick/find/order/{id}", methods="GET")
    */
    public function findPickorder(
        EntityManagerInterface $em,
        PickorderRepository $pickRepo,
        Request $request,
        $id
    ) {
        $header = getallheaders();
        if(isset($header['X-Header-Auth']) !== true){
            return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => 'No headers',
                    'data' 			=> $header
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if($hmac_header !== 'ajdkaur783i8rhfak1o182hr'){
             return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => 'Not allowed'
                )
            );
        }

        $return = $pickRepo->findPickorder(urldecode($id), (int)$header['X-Header-Userid']);
        
        if(gettype($return) === 'string'){
            return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => $return,
                )
            );
        }

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $return,
            )
        );
    }
   
}