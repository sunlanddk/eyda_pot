<?php
namespace App\Controller;

use App\Repository\Cron\InventoryRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class InventoryController extends ApiController {

    /**
    * @Route("/inventory/update/counters", methods="POST")
    */
    public function Cron(
        EntityManagerInterface $em,
        InventoryRepository $inventoryRepository,
        Request $request
    ) {
    	set_time_limit(600);

        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }

        $request = json_decode($request, true);

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $inventoryRepository->updateCountersForArticles($request['articles'], $request['userId'], $request['checkIfStockWasZero'])
            )
        );
    }

    /**
    * @Route("/inventory/update/counters/variantcodeid/{id}", methods="GET")
    */
    public function createInventoryLineCron(
        EntityManagerInterface $em,
        InventoryRepository $inventoryRepository,
        Request $request,
        $id
    ) {
        set_time_limit(0);

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $inventoryRepository->createInventoryLine($id)
            )
        );
    }

    /**
    * @Route("/inventory/update/by/changed", methods="POST")
    */
    public function UpdateByChanged(
        EntityManagerInterface $em,
        InventoryRepository $inventoryRepository,
        Request $request
    ) {
        set_time_limit(600);

        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }

        $request = json_decode($request, true);

        $comment = '';
        if(isset($request['description']) === true){
            $comment = $request['description'];
        }

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $inventoryRepository->updateStockByChanged($request['variants'], $request['userId'], $comment)
            )
        );
    }

}
