<?php

namespace App\Controller;

use App\Repository\Order\RefundRepository;
use App\Service\Order\RefundService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class RefundController extends ApiController
{
    private RefundService $refundService;

    public function __construct(RefundService $refundService)
    {
        $this->refundService = $refundService;
    }

    /**
     * @Route("/refund/shopify/{id}", methods="GET")
     */
    public function createRefundInShopify(
        EntityManagerInterface $em,
        RefundRepository $refundRepo,
        Request $request,
        $id
    ) {
        $header = getallheaders();
        if (isset($header['X-Header-Auth']) !== true) {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'No headers'
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if ($hmac_header !== 'ajdkaur783i8rhfak1o182hr') {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'Not allowed'
                )
            );
        }

        $return = $refundRepo->refundInShopify($id, (int)$header['X-Header-Userid']);
        if ($return !== true) {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => $return,
                )
            );
        }

        return $this->respond(
            array(
                'status' => 'success',
                'response' => 'Order returned',
            )
        );
    }

    /**
     * @Route("/refund/simpleitem", methods="POST")
     */
    public function refundSimpleItem(
        EntityManagerInterface $em,
        RefundRepository $refundRepo,
        Request $request
    ) {
        $header = getallheaders();
        if (isset($header['X-Header-Auth']) !== true) {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'No headers'
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if ($hmac_header !== 'ajdkaur783i8rhfak1o182hr') {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'Not allowed'
                )
            );
        }

        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error' => true,
                    'errormessage' => 'Malformed post object 1',
                    'data' => '',
                )
            );
        }

        $requestConverted = json_decode($request, true);

        $return = $refundRepo->simpleReturnItem($requestConverted);

        if ($return === true) {
            return $this->respond(
                array(
                    'status' => 'success',
                    'order' => $return,
                    'response' => $requestConverted
                )
            );
        }

        return $this->respondError(array(
                'error' => true,
                'errormessage' => $return,
                'data' => '',
            )
        );
    }

    /**
     * @Route("/refund/shopify/specific/amount", methods="POST")
     */
    public function refundSpecificAmount(
        EntityManagerInterface $em,
        RefundRepository $refundRepo,
        Request $request
    ) {
        $header = getallheaders();
        if (isset($header['X-Header-Auth']) !== true) {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'No headers'
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if ($hmac_header !== 'ajdkaur783i8rhfak1o182hr') {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'Not allowed'
                )
            );
        }

        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error' => true,
                    'errormessage' => 'Malformed post object 1',
                    'data' => '',
                )
            );
        }

        $requestConverted = json_decode($request, true);

        $return = $refundRepo->refundSpecificAmount($requestConverted, (int)$header['X-Header-Userid']);

        if ($return === true) {
            return $this->respond(
                array(
                    'status' => 'success',
                    'order' => $return,
                    'response' => $requestConverted
                )
            );
        }

        return $this->respondError(array(
                'error' => true,
                'errormessage' => $return,
                'data' => '',
            )
        );
    }

    /**
     * @Route("/refund/create/passon/refund", methods="POST")
     */
    public function createPassonRefundRecord(
        EntityManagerInterface $em,
        RefundRepository $refundRepo,
        Request $request
    ) {
        $header = getallheaders();
        if (isset($header['X-Header-Auth']) !== true) {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'No headers',
                    'data' => $header
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if ($hmac_header !== 'ajdkaur783i8rhfak1o182hr') {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'Not allowed'
                )
            );
        }

        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error' => true,
                    'errormessage' => 'Malformed post object',
                    'data' => '',
                )
            );
        }
        $request = json_decode($request, true);

        $response = $this->refundService->createRefund($request, (int)$header['X-Header-Userid']);

        return $this->respond(
            array(
                'status' => 'success',
                'response' => $response
            )
        );
    }

    /**
     * @Route("/refund/find/order/{id}", methods="GET")
     */
    public function findOrderByShopifyReference(
        RefundRepository $refundRepo,
        $id
    ) {
        $header = getallheaders();

        $header['X-Header-Auth'] = 'ajdkaur783i8rhfak1o182hr';
        $header['X-Header-Userid'] = 1;

        if (isset($header['X-Header-Auth']) !== true) {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'No headers',
                    'data' => $header
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if ($hmac_header !== 'ajdkaur783i8rhfak1o182hr') {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => 'Not allowed'
                )
            );
        }

        $return = $refundRepo->findShopifyOrder(urldecode($id), (int)$header['X-Header-Userid']);
        if (gettype($return) === 'string') {
            return $this->respond(
                array(
                    'status' => 'error',
                    'response' => $return,
                )
            );
        }

        return $this->respond(
            array(
                'status' => 'success',
                'response' => $return,
            )
        );
    }
}
