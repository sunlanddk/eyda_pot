<?php

namespace App\Controller;

use App\Repository\AuthRepository;
use App\Repository\Cron\InventoryRepository;
use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Order\OrdersRepository;
use App\Repository\PimRepository;
use App\Repository\Stock\ItemRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

// use App\Repository\CompanyLogRepository;
// use App\Repository\OrdercontactlogRepository;
// use App\Repository\AuthRepository;
// use App\Repository\UserLoginRepository;
// use App\Repository\UserGuidRepository;
// use App\Repository\CompanyRepository;
// use App\Repository\SalesOrderRepository;

// use SendGrid\Mail\Mail;

class TestController extends ApiController
{

    /**
     * @Route("/test", methods="GET")
     */
    public function companylogtest(
        EntityManagerInterface $em,
        // VariantcodeRepository $vcr,
        // EntityManagerInterface $em,
        OrdersRepository $OrdersRepository,
        PimRepository $pimRepo,
        InventoryRepository $inventory,
        ItemRepository $item,
        Request $request
    ) {
        // $variant = $this->_sqlHelper->getVariantInformation($item['variant_id'] ,$this->_sqlHelper->getShopId($domain));
        //$val = $vcr->getVariantCodeByGtin('5715232026692');
        $val = 'teszt';
        return $this->respond(
            array(
                'status' => 'test',
                'response' => $val
            )
        );
        //


        $test = new DateTime('2001-01-01 00:00:00');
        return $this->respond(
            array(
                'status' => 'success',
                //'response' => $pimRepo->publishProductTest(array(381)) // 'test kørt'//$OrdersRepository->checkForPickorders()
                // 'response'      => $articleRepo->createArticleLang(1, 4, $description = 'test111', $webdescription = 'test333', $onwebshop = 1),
                // 'response'      => $item->inventoryItem(1, 881, 56, 78, 2),
                'response1' => $pimRepo->publishProductTest(array(504)),
            )
        );
    }

    /**
     * @Route("/tomek/test", methods="GET")
     */
    public function tomekTest(
        AuthRepository $authRepository
    ) {
        return $this->respond(
            array(
                'test' => '1',
                'tesst' => '2',
                "tesssst" => '3'
            )
        );

        $authRepository->hashedAllPassword();

        $salt = "wakhdkauhdfiasugfhg";

        $loginname = "Tomek";
        $timeStamp = time();

        $cipherMethod = openssl_get_cipher_methods();
        $cipherMethod = $cipherMethod[0];
        $ivlen = openssl_cipher_iv_length($cipher = "AES-128-CBC");
        $iv = openssl_random_pseudo_bytes($ivlen);

        $hashed = openssl_encrypt($loginname . '-' . $timeStamp, $cipherMethod, $salt, OPENSSL_RAW_DATA, $iv);
        $hashed = base64_encode($hashed);
        $salt = "dsadasdasdasd";
        $unhashed = openssl_decrypt(base64_decode($hashed), $cipherMethod, $salt, OPENSSL_RAW_DATA, $iv);

        $authRepository->hashedAllPassword();

        return $this->respond(
            array(
                'hashed' => $hashed,
                'unhashed' => $unhashed,
                "iv" => base64_encode($iv)
            )
        );
    }

    /**
     * @Route("/error/logs", methods="GET")
     */
    public function errorLogs(
        EntityManagerInterface $em,
        SqlhelperRepo $_sqlHelper,
        Request $request
    ) {
        return $this->respond(
            array(
                'status' => 'success',
                //'response' => $_sqlHelper->test()
            )
        );
    }

}
