<?php
namespace App\Controller;

use App\Repository\HelperFunctions\SqlhelperRepo;
use App\Repository\Order\OrdersRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class OrdersController extends ApiController {

    /**
    * @Route("/print/sku/label", methods="POST")
    */
    public function printLabel(
        EntityManagerInterface $em,
        SqlHelperRepo $SqlhelperRepo,
        Request $request
    ) {
        $header = getallheaders();
        return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => 'No headers'
                )
            );
        if(isset($header['X-Header-Auth']) !== true){
            return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => 'No headers'
                )
            );
        }

        $hmac_header = $header['X-Header-Auth'];

        if($hmac_header !== 'ajdkaur783i8rhfak1o182hr'){
             return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => 'Not allowed'
                )
            );
        }

        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }
        $request = json_decode($request, true);

        $return = $SqlhelperRepo->printLabel($request['sku'], $request['ws']);

        if($return !== true){
            return $this->respond(
                array(
                    'status'        => 'error',
                    'response'      => $return,
                )
            );
        }

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => 'Label printed',
            )
        );
    }

    /**
    * @Route("/orders/shopify/new", methods="POST")
    */
    public function webhookCreateNewOrder(
        EntityManagerInterface $em,
        OrdersRepository $ordersRepo,
        Request $request
    ) {
        $header = getallheaders();
        $hmac_header = $header['X-Shopify-Hmac-Sha256'];
        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }
        $requestConverted = json_decode($request, true);
        $verified = $this->verify_webhook($request, $hmac_header);
        // if($verified === false){
        //     return $this->respond(
        //         array(
        //             'status'        => 'error',
        //             'verified'      => $verified
        //         )
        //     );
        // }
        if($requestConverted['id'] === '3944433090640'){
             return $this->respond(
                array(
                    'status'        => 'success',
                    //'response'      => $requestConverted
                )
            );
        }
        /*
        $path = 'C:\inetpub\wwwroot/eyda/webhook.txt';
        $myfile = fopen($path, "a");
        fwrite($myfile, $request. "\n\r\n\r");
        fclose($myfile);
        */

        return $this->respond(
            array(
                'status'        => 'success',
                'verified'      => $verified,
                'order'          => $ordersRepo->createOrder($requestConverted, $header['X-Shopify-Shop-Domain']),
                //'response'      => $requestConverted
            )
        );
    }

    /**
    * @Route("/orders/shopify/new/multiple", methods="POST")
    */
    public function webhookCreateNewOrdermultiple(
        EntityManagerInterface $em,
        OrdersRepository $ordersRepo,
        Request $request
    ) {
        $header = getallheaders();
        $hmac_header = $header['X-Shopify-Hmac-Sha256'];
        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object', 
                    'data'          => '',
                )
            );
        }
        $requestConverted = json_decode($request, true);
        $verified = $this->verify_webhook($request, $hmac_header);
       

        $orders = [];

        foreach ($requestConverted['orders'] as $key => $value) {
            $orders[] = $ordersRepo->createOrder($value, $header['X-Shopify-Shop-Domain']);
        }

        return $this->respond(
            array(
                'status'        => 'success',
                'verified'      => $verified,
                'order'          => $orders,
                //'response'      => $requestConverted
            )
        );
    }

    /**
    * @Route("/orders/shopify/testorder", methods="POST")
    */
    public function webhookCreateNewOrderTest(
        EntityManagerInterface $em,
        OrdersRepository $ordersRepo,
        Request $request
    ) {
        $header = getallheaders();
        $hmac_header = $header['X-Shopify-Hmac-Sha256'];
        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }
        $requestConverted = json_decode($request, true);
        $verified = $this->verify_webhook($request, $hmac_header);
        // if($verified === false){
        //     return $this->respond(
        //         array(
        //             'status'        => 'error',
        //             'verified'      => $verified
        //         )
        //     );
        // }

        return $this->respond(
            array(
                'status'        => 'success',
                'verified'      => $verified,
                'order'          => $ordersRepo->createOrderTest($requestConverted, $header['X-Shopify-Shop-Domain']),
                //'response'      => $requestConverted
            )
        );
    }

    /**
    * @Route("/orders/shopify/cancel", methods="POST")
    */
    public function webhookOrderCancel(
        EntityManagerInterface $em,
        OrdersRepository $ordersRepo,
        Request $request
    ) {
        $header = getallheaders();
        $hmac_header = $header['X-Shopify-Hmac-Sha256'];
        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }
        $requestConverted = json_decode($request, true);
        $verified = $this->verify_webhook($request, $hmac_header);
        // if($verified === false){
        //     return $this->respond(
        //         array(
        //             'status'        => 'error',
        //             'verified'      => $verified
        //         )
        //     );
        // }

        return $this->respond(
            array(
                'status'        => 'success',
                'verified'      => $verified,
                'order'          => $ordersRepo->cancelOrder($requestConverted),
                'response'      => $requestConverted
            )
        );
    }

    /**
    * @Route("/orders/shopify/refund", methods="POST")
    */
    public function webhookOrderRefund(
        EntityManagerInterface $em,
        OrdersRepository $ordersRepo,
        Request $request
    ) {

        // Inserted to disable webhook when adding PassON return handling

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => true
            )
        );

        // Inserted to disable webhook when adding PassON return handling


        $header = getallheaders();
        $hmac_header = $header['X-Shopify-Hmac-Sha256'];
        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }
        $requestConverted = json_decode($request, true);
        $verified = $this->verify_webhook($request, $hmac_header);
        // if($verified === false){
        //     return $this->respond(
        //         array(
        //             'status'        => 'error',
        //             'verified'      => $verified
        //         )
        //     );
        // }

        return $this->respond(
            array(
                'status'        => 'success',
                'verified'      => $verified,
                'order'          => $ordersRepo->refundOrder($requestConverted),
                'response'      => $requestConverted
            )
        );
    }

    /**
    * @Route("/orders/shopify/update", methods="POST")
    */
    public function webhookCreateUpdateOrder(
        EntityManagerInterface $em,
        OrdersRepository $ordersRepo,
        Request $request
    ) {
        $header = getallheaders();
        $hmac_header = $header['X-Shopify-Hmac-Sha256'];
        $request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
                    'error'         => true,
                    'errormessage'  => 'Malformed post object',
                    'data'          => '',
                )
            );
        }
        $requestConverted = json_decode($request, true);
        $verified = $this->verify_webhook($request, $hmac_header);
        // if($verified === false){
        //     return $this->respond(
        //         array(
        //             'status'        => 'error',
        //             'verified'      => $verified
        //         )
        //     );
        // }
        return $this->respond(
            array(
                'status'        => 'success',
                'verified'      => $verified,
                'test'          => $ordersRepo->updateOrder($requestConverted, $header['X-Shopify-Shop-Domain']),
                'response'      => $requestConverted
            )
        );
    }

    private function verify_webhook($data, $hmac_header)
    {
        $token = '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';//$_SERVER['SHOPIFY_TOKEN_DA'];
        $calculated_hmac = base64_encode(hash_hmac('sha256', $data, $token, true));
        return hash_equals($hmac_header, $calculated_hmac);
    }

    // /**
    // * @Route("/pim/shopify/get/description", methods="GET")
    // */
    // public function companylogtest(
    //     EntityManagerInterface $em,
    //     PimRepository $pimRepo,
    //     Request $request
    // ) {

    //     return $this->respond(
    //         array(
    //             'status'        => 'success',
    //             // 'response'      => $articleRepo->createArticleLang(1, 4, $description = 'test111', $webdescription = 'test333', $onwebshop = 1),
    //             'response'      => $pimRepo->getDescriptionFromShop([4, 879])
    //         )
    //     );
    // }

}
