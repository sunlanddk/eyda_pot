<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\Cron\CronRun;
// use SendGrid\Mail\Mail;

class ApiGetKpiController extends ApiController {

    /**
    * @Route("/getkpi", methods="GET")
    */
    public function GetKpi(
        EntityManagerInterface $em
    ) {

        $conn = $em->getConnection();

        $_datefilter = date("Y m d", strtotime("-1 day")) ;

        $sql = "
            SELECT ShippedPcs, Shipments, producedDate FROM kpiday 
            WHERE Active=1 
            AND date_format(producedDate, '%Y %m %d')= 
            '" . $_datefilter . "';";

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $result = $stmt->fetchAll();





        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $result
            )
        );
    }

     /**
    * @Route("/getcurrentkpi", methods="GET")
    */
    public function GetCurrentKpi(
        EntityManagerInterface $em
    ) {

        $conn = $em->getConnection();


        $sql = "
           SELECT count(Shipments) as OrderQty FROM (
                            SELECT date_format(Date_add(s.createdate, interval 1 hour),'%H') as packed, COUNT(s.Id) AS Shipments, sum(pl.PackedQuantity) as ShipmentPcs 
                            FROM (stock s, pickorder p, pickorderline pl) 
                                    WHERE s.CreateDate >= date_format(CURRENT_DATE(), '%Y-%m-%d')
                                    AND s.Type !='fixed' AND s.Departed=1 
                                    AND s.active=1
                                    AND s.Id=p.toId
                                    AND p.active=1
                                    AND pl.pickorderId=p.Id
                                    AND pl.active=1
                                    AND p.fromid=14321
                                    AND p.ownercompanyid=787
                                    AND p.handlercompanyid=787
                                    GROUP BY s.Id) dayupdate
                                    GROUP BY dayupdate.packed 
                                    ORDER BY dayupdate.packed ASC";



        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_orderQtyToday = $stmt->fetch();
        $result['_orderQtyToday'] = (isset($_orderQtyToday['OrderQty']) === true ? (int)$_orderQtyToday['OrderQty'] : 0);

        $sql = "
           SELECT sum(ShipmentPcs) as PackedQty FROM (
                            SELECT date_format(Date_add(s.createdate, interval 1 hour),'%H') as packed, COUNT(s.Id) AS Shipments, sum(pl.PackedQuantity) as ShipmentPcs 
                            FROM (stock s, pickorder p, pickorderline pl) 
                                    WHERE s.CreateDate >= date_format(CURRENT_DATE(), '%Y-%m-%d')
                                    AND s.Type !='fixed' AND s.Departed=1 
                                    AND s.active=1
                                    AND s.Id=p.toId
                                    AND p.active=1
                                    AND pl.pickorderId=p.Id
                                    AND pl.active=1
                                    AND p.fromid=14321
                                    AND p.ownercompanyid=787
                                    AND p.handlercompanyid=787
                                    GROUP BY s.Id) dayupdate
                                    GROUP BY dayupdate.packed 
                                    ORDER BY dayupdate.packed ASC";



        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_packedQtyToday = $stmt->fetch();
        $result['_packedQtyToday'] = (isset($_packedQtyToday['PackedQty']) === true ? (int)$_packedQtyToday['PackedQty'] : 0);



        $sql ="select sum(pl.orderedquantity) as cnt from (pickorder p, pickorderline pl) 
                  where p.createdate>CURRENT_DATE() and p.active=1  and p.ownercompanyid=787 and p.handlercompanyid=787 
                        and pl.pickorderid=p.id and pl.active=1" ;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_newOrderPcs = $stmt->fetch();
        $result['_newOrderPcs'] = (isset($_newOrderPcs['cnt']) === true ? (int)$_newOrderPcs['cnt'] : 0);


         $sql ="select count(id) as cnt 
                from pickorder 
                where packed=0 and active=1" ;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_openOrder = $stmt->fetch();
        $result['_openOrder'] = (isset($_openOrder['cnt']) === true ? (int)$_openOrder['cnt'] : 0);

         $sql = "select sum(pl.orderedquantity-pl.packedquantity) as cnt from (pickorder p, pickorderline pl) 
                  where p.packed=0 and p.active=1 and p.ownercompanyid=787 and p.handlercompanyid=787 
                        and pl.pickorderid=p.id and pl.active=1" ;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_openOrderPcs = $stmt->fetch();
        $result['_openOrderPcs'] = (isset($_openOrderPcs['cnt']) === true ? (int)$_openOrderPcs['cnt'] : 0);

         $sql = "select count(id) as cnt from pickorder where createdate>CURRENT_DATE() and active=1" ;

        $stmt = $conn->prepare($sql);
        $stmt->execute();
        $_newOrders = $stmt->fetch();
        $result['_newOrders'] = (isset($_newOrders['cnt']) === true ? (int)$_newOrders['cnt'] : 0);

        




        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $result
            )
        );
    }
   
}