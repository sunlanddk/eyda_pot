<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\JsonResponse;

// use App\Repository\UserLoginRepository;

class ApiController
{

    /**
     * @var integer HTTP status code - 200 (OK) by default
     */
    protected $statusCode = 200;

    /**
     * Returns a 401 Unauthorized http response
     *
     * @param string $message
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respondUnauthorized($message = 'Not authorized!')
    {
        return $this->setStatusCode(401)->respondWithErrors($message);
    }

    /**
     * Sets an error message and returns a JSON response
     *
     * @param string|array $errors
     *
     * @return JsonResponse
     */
    public function respondWithErrors($errors, $headers = [])
    {
        $data = [
            'errors' => $errors,
        ];

        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Gets the value of statusCode.
     *
     * @return integer
     */
    public function getStatusCode()
    {
        return $this->statusCode;
    }

    /**
     * Sets the value of statusCode.
     *
     * @param integer $statusCode the status code
     *
     * @return self
     */
    protected function setStatusCode($statusCode)
    {
        $this->statusCode = $statusCode;

        return $this;
    }

    /**
     * Returns a 202
     *
     * @param string $message
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function responseAccepted($data, $headers = [])
    {
        return new JsonResponse($data, 202, $headers);
    }

    /**
     * Returns a 422 Unprocessable Entity
     *
     * @param string $message
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respondValidationError($message = 'Validation errors')
    {
        return $this->setStatusCode(422)->respondWithErrors($message);
    }

    /**
     * Returns a 404 Not Found
     *
     * @param string $message
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respondNotFound($message = 'Not found!')
    {
        return $this->setStatusCode(404)->respondWithErrors($message);
    }

    /**
     * Returns a 201 Created
     *
     * @param array $data
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respondCreated($data = [])
    {
        return $this->setStatusCode(201)->respond($data);
    }

    /**
     * Returns a JSON response
     *
     * @param array $data
     * @param array $headers
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respond($data, $headers = [])
    {
        return new JsonResponse($data, $this->getStatusCode(), $headers);
    }

    /**
     * Returns a 401 Logout
     *
     * @param array $data
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respondLogout($message)
    {
        return $this->setStatusCode(401)->respondWithErrors($message);
    }

    /**
     * Returns a 406 Logout
     *
     * @param array $data
     *
     * @return Symfony\Component\HttpFoundation\JsonResponse
     */
    public function respondError($message)
    {
        return new JsonResponse($message, 406, []);
        // return $this->setStatusCode(405)->respondWithErrors($message);
    }

    // public function validateToken(AuthRepository $authRepository, UserLoginRepository $userLoginRepository, Request $request) {
    // public function validateToken(AuthRepository $authRepository, UserLoginRepository $userLoginRepository, $request) {
    //     // $authorizationHeader = $request->headers->get('authorization');
    //     // $decryptedTokenArray = $authRepository->checkToken($authorizationHeader);
    //     $decryptedTokenArray = $authRepository->checkToken($request);
    //     $userId = (int)$decryptedTokenArray[1];
    //     $tokenCounter = (int)$decryptedTokenArray[2];

    //     $user = $userLoginRepository->getUser($userId);
    //     if(gettype($user) == 'object'){
    //         if($tokenCounter < $user->getTokenCounter()) {
    //             return 'Du skal logge ind igen.';
    //         }
    //         else {
    //             // return $userLoginRepository->getToken($user);
    //             return $user;
    //         }
    //     }
    //     else{
    //         return 'No Account';
    //     }
    // }

    // public function validateTokenTest(AuthRepository $authRepository, UserLoginRepository $userLoginRepository, $request) {
    //     // $authorizationHeader = $request->headers->get('authorization');
    //     // $decryptedTokenArray = $authRepository->checkToken($authorizationHeader);
    //     $decryptedTokenArray = $authRepository->checkToken($request);
    //     $userId = (int)$decryptedTokenArray[1];
    //     $tokenCounter = (int)$decryptedTokenArray[2];
    //     return $decryptedTokenArray;
    //     $user = $userLoginRepository->getUser($userId);
    //     if(gettype($user) == 'object'){
    //         if($tokenCounter < $user->getTokenCounter()) {
    //             return 'Du skal logge ind igen.';
    //         }
    //         else {
    //             // return $userLoginRepository->getToken($user);
    //             return $user;
    //         }
    //     }
    //     else{
    //         return 'No Account';
    //     }
    // }
}
