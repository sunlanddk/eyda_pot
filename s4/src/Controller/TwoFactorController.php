<?php
namespace App\Controller;

use App\Repository\TwoFactorRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Sonata\GoogleAuthenticator\GoogleAuthenticator;

class TwoFactorController extends ApiController {

    /**
    * @Route("/auth/generate", methods="GET")
    */
    public function qrGenerator(TwoFactorRepository $tfRepo) {
        
        $qrCode = $tfRepo->createQRCode("Tomek");

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $qrCode
            )
        );
    }

    /**
    * @Route("/auth/verify", methods="POST")
    */
    public function errorLogs(TwoFactorRepository $tfRepo, Request $request) {

        $code = $_POST['code'];

        if($tfRepo->verifyCode("Tomek", $code)){
            echo 'Success!';
            die();
        }
        echo ':(';
        die();
    }
}
