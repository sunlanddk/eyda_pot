<?php

namespace App\Controller\Api;

use App\Controller\ApiController;
use App\Entity\Language;
use App\Normalizer\RefundReasonNormalizer;
use App\Service\Order\RefundReasonService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class RefundController extends ApiController
{
    private RefundReasonService $refundReasonService;

    public function __construct(RefundReasonService $refundReasonService)
    {
        $this->refundReasonService = $refundReasonService;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/refund/reasons", name="api_refund_reasons", methods="GET")
     */
    public function getRefundReasons(Request $request): JsonResponse
    {
        $reasons = $this->refundReasonService->getRefundReasons();

        /** @var Language|null $language */
        $language = $this->refundReasonService
            ->getEntityManager()
            ->getRepository(Language::class)
            ->findOneBy(['isoCode' => $request->getLocale()]);
        $serializer = new Serializer([new RefundReasonNormalizer($language)]);
        $normalized = $serializer->normalize($reasons, null, [
            'groups' => ['api', 'refund', 'reasons']
        ]);

        return $this->respond([
            'data' => $normalized,
        ]);
    }
}
