<?php

namespace App\Controller\Api;

use App\Controller\ApiController;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Routing\Annotation\Route;

class TestController extends ApiController
{
    private MailerInterface $mailer;
    private ParameterBagInterface $parameter;
    private EntityManagerInterface $entityManager;

    public function __construct(
        MailerInterface $mailer,
        ParameterBagInterface $parameter,
        EntityManagerInterface $entityManager
    ) {
        $this->mailer = $mailer;
        $this->parameter = $parameter;
        $this->entityManager = $entityManager;
    }

    /**
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/test/email", name="api_test_email", methods="GET")
     */
    public function testEmailSend(Request $request): JsonResponse
    {
        $recipient = $request->query->get('recipient');
        if ($recipient === null) {
            return $this->respond([
                'success' => false,
                'message' => '`recipient` parameter is required.'
            ]);
        }

        try {
            $email = (new TemplatedEmail())
                ->from(
                    new Address(
                        $this->parameter->get('app.mailer.from_address'),
                        $this->parameter->get('app.mailer.from_name')
                    )
                )
                ->to(new Address($recipient))
                ->subject('Test email');

            $email->htmlTemplate('emails/test.html.twig')
                ->context([
                    // pass variables (name => value) to the template
                ]);

            $this->mailer->send($email);

            return $this->respond([
                'success' => true,
                'message' => 'Email sent to ' . $recipient . '.'
            ]);
        } catch (Exception $exception) {
            return $this->respond([
                'success' => false,
                'message' => $exception->getMessage()
            ]);
        }
    }
}
