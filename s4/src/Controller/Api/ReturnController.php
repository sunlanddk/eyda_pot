<?php

namespace App\Controller\Api;

use App\Controller\ApiController;
use App\Event\EmailEvent;
use App\Exception\OrderNotFoundException;
use App\Exception\ReturnException;
use App\Normalizer\ReturnNormalizer;
use App\Request\Order\ShowRequest;
use App\Request\Returns\SendEmailRequest;
use App\Request\Returns\StoreRequest;
use App\Service\Order\OrderService;
use App\Service\Order\ReturnService;
use App\Support\Integrations\GLSService;
use Exception;
use RuntimeException;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class ReturnController extends ApiController
{
    private EventDispatcherInterface $eventDispatcher;
    private ReturnService $returnService;
    private OrderService $orderService;

    public function __construct(
        EventDispatcherInterface $eventDispatcher,
        ReturnService $returnService,
        OrderService $orderService
    ) {
        $this->eventDispatcher = $eventDispatcher;
        $this->orderService = $orderService;
        $this->returnService = $returnService;
    }

    /**
     * @param ShowRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route("/api/return/order", name="api_return_order_find", methods="POST")
     */
    public function getReturnByOrder(ShowRequest $request, SerializerInterface $serializer): JsonResponse
    {
        try {
            $order = $this->orderService->findOrderByParametersOrFail(
                $request->orderNumber,
                $request->orderEmail,
                $request->orderPostcode
            );
        } catch (OrderNotFoundException $e) {
            $this->setStatusCode($e->getCode());

            return $this->respondWithErrors([
                [
                    'message' => $e->getMessage(),
                ],
            ]);
        }

        $returns = $this->returnService->getReturnsByOrder($order);
        if (!$returns) {
            return $this->respond([
                'data' => [],
            ]);
        }

        /** @var ReturnNormalizer $normalized */
        $normalized = $serializer->normalize($returns, null, [
            'groups' => ['api', 'return', 'order'],
            'normalizer' => new ReturnNormalizer(),
        ]);

        return $this->respond([
            'data' => $normalized,
        ]);
    }

    /**
     * @param StoreRequest $request
     * @param SerializerInterface $serializer
     * @return JsonResponse
     *
     * @Route("/api/return/order/store", name="api_return_order_store", methods="POST")
     */
    public function storeReturnByOrder(StoreRequest $request, SerializerInterface $serializer): JsonResponse
    {
     try {
            $order = $this->orderService->findOrderByParametersOrFail(
                $request->orderNumber,
                $request->orderEmail,
                $request->orderPostcode
            );

            $result = $this->returnService->storeReturnByOrder(
                $order,
                $request->orderReturn,
                $request->orderReturnItems
            );

            /** @var ReturnNormalizer $normalized */
            $normalized = $serializer->normalize($result, null, [
                'groups' => ['api', 'return', 'order'],
                'normalizer' => new ReturnNormalizer(),
            ]);

            $this->setStatusCode(Response::HTTP_CREATED);

            return $this->respond([
                'data' => $normalized,
            ]);
        } catch (ReturnException|OrderNotFoundException $e) {
            $this->setStatusCode($e->getCode());

            return $this->respondWithErrors([
                [
                    'message' => $e->getMessage(),
                ],
            ]);
        }
    }

    /**
     * @param SendEmailRequest $storeRequest
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/return/label/send-email", name="api_return_label_send_email", methods="POST")
     */
    public function sendEmailReturnLabel(SendEmailRequest $storeRequest, Request $request): JsonResponse
    {
        $returnEntity = $this->returnService->getReturnRepository()->findOneBy([
            'id' => $storeRequest->returnId,
            'active' => true,
        ]);

        if (!$returnEntity) {
            $this->setStatusCode(Response::HTTP_NOT_FOUND);

            return $this->respondWithErrors([
                [
                    'message' => 'Return not found.',
                ],
            ]);
        }

        $locale = $returnEntity->getEmailLanguage();
        if (empty($locale)) {
            $request->setLocale('da');
            $locale = $request->getLocale();
        }

        $event = new EmailEvent(
            $returnEntity->getOrder()->getEmail(),
            [
                'service' => GLSService::SERVICE_NAME,
                'returnEmail' => $storeRequest->returnEmail,
                'returnEntity' => $returnEntity,
            ],
            $locale
        );

        try {
            $this->eventDispatcher->dispatch($event, 'email.send.' . EmailEvent::TYPE_RETURN_LABEL);
        } catch (Exception $e) {
            $this->setStatusCode(Response::HTTP_BAD_REQUEST);

            if (str_contains($e->getMessage(), sprintf(GLSService::FILE_PATH . GLSService::FILE_NAME_PNG, ''))) {
                return $this->respondWithErrors([
                    [
                        'message' => 'Failed to make GLS request for generate QR code.',
                    ],
                ]);
            }

            return $this->respondWithErrors([
                [
                    'message' => $e->getMessage(),
                ],
            ]);
        }

        return $this->respond([
            'data' => [
                'message' => 'Email sent successfully.',
            ],
        ]);
    }

    /**
     * @param Request $request
     * @return BinaryFileResponse|JsonResponse
     *
     * @Route("/api/return/label/generate", name="api_return_label_generate", methods="GET")
     */
    public function generateReturnLabel(Request $request)
    {
        $orderId = (int)$request->query->get('order-id');
        $returnId = (int)$request->query->get('return-id');
        $labelType = (string)$request->query->get('label-type');
        $labelType = trim(str_replace(['\'', '"'], '', $labelType));

        try {
            $result = $this->returnService->generateReturnLabel(
                $request,
                $orderId,
                $returnId,
                $labelType
            );
        } catch (ReturnException|RuntimeException $e) {
            $this->setStatusCode(
                (Response::HTTP_BAD_REQUEST <= $e->getCode() && $e->getCode() < Response::HTTP_INTERNAL_SERVER_ERROR)
                    ? $e->getCode()
                    : Response::HTTP_BAD_REQUEST
            );

            return $this->respondWithErrors([
                [
                    'message' => $e->getMessage(),
                ],
            ]);
        }

        return $result;
    }
}
