<?php

namespace App\Controller\Api;

use App\Controller\ApiController;
use App\Entity\Language;
use App\Exception\OrderNotFoundException;
use App\Normalizer\OrderNormalizer;
use App\Request\Order\ShowRequest;
use App\Service\Order\OrderService;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class OrderController extends ApiController
{
    private OrderService $orderService;

    public function __construct(OrderService $orderService)
    {
        $this->orderService = $orderService;
    }

    /**
     * @param ShowRequest $showRequest
     * @param Request $request
     * @return JsonResponse
     *
     * @Route("/api/order/return", name="api_order_return_find", methods="POST")
     */
    public function getOrderForReturn(ShowRequest $showRequest, Request $request): JsonResponse
    {
        try {
            $order = $this->orderService->findOneForReturn(
                $showRequest->orderNumber,
                $showRequest->orderEmail,
                $showRequest->orderPostcode
            );
        } catch (OrderNotFoundException $e) {
            $this->setStatusCode($e->getCode());

            return $this->respondWithErrors([
                [
                    'message' => $e->getMessage(),
                ],
            ]);
        }

        /** @var Language|null $language */
        $language = $this->orderService
            ->getEntityManager()
            ->getRepository(Language::class)
            ->findOneBy(['isoCode' => $request->getLocale()]);
        $serializer = new Serializer([new OrderNormalizer($language)]);
        $normalized = $serializer->normalize($order, null, [
            'groups' => ['api', 'order']
        ]);

        return $this->respond([
            'data' => $normalized,
        ]);
    }
}
