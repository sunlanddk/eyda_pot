<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
// use App\Repository\CompanyLogRepository;
// use App\Repository\OrdercontactlogRepository;
// use App\Repository\AuthRepository;
// use App\Repository\UserLoginRepository;
// use App\Repository\UserGuidRepository;
// use App\Repository\CompanyRepository;
// use App\Repository\SalesOrderRepository;
use App\Repository\PimRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;
// use SendGrid\Mail\Mail;

class PimController extends ApiController {

    /**
    * @Route("/pim/shopify/get/description", methods="POST")
    */
    public function getShopifyDescription(
        EntityManagerInterface $em,
        PimRepository $pimRepo,
        Request $request
    ) {

    	$request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
	                'error'        	=> true,
	                'errormessage' 	=> 'Malformed post object', 
	                'data' 			=> '',
            	)
            );
        }
        $request = json_decode($request, true);

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $pimRepo->getDescriptionFromShop([$request['languageid'], $request['articleid']])
            )
        );
    }

    /**
    * @Route("/pim/shopify/set/description", methods="POST")
    */
    public function setShopifyDescription(
        EntityManagerInterface $em,
        PimRepository $pimRepo,
        Request $request
    ) {

    	$request = file_get_contents('php://input');
        if (!$request) {
            return $this->respondError(array(
	                'error'        	=> true,
	                'errormessage' 	=> 'Malformed post object', 
	                'data' 			=> '',
            	)
            );
        }
        $request = json_decode($request, true);

        return $this->respond(
            array(
                'status'        => 'success',
                'response'      => $pimRepo->setDescriptionOnShop([$request['languageid'], $request['articleid']])
            )
        );
    }

    /**
    * @Route("/pim/shopify/get/description", methods="GET")
    */
    public function companylogtest(
        EntityManagerInterface $em,
        PimRepository $pimRepo,
        Request $request
    ) {

        return $this->respond(
            array(
                'status'        => 'success',
                // 'response'      => $articleRepo->createArticleLang(1, 4, $description = 'test111', $webdescription = 'test333', $onwebshop = 1), 
                'response'      => $pimRepo->getDescriptionFromShop([4, 879])
            )
        );
    }
   
}