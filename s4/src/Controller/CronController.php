<?php
namespace App\Controller;

use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;
use Symfony\Component\Routing\Annotation\Route;

use App\Repository\Cron\CronRun;
// use SendGrid\Mail\Mail;

class CronController extends ApiController {

    /**
    * @Route("/cron", methods="GET")
    */
    public function Cron(
        EntityManagerInterface $em,
        CronRun $CronRun,
        Request $request
    ) {
    	set_time_limit(600);
        return $this->respond(
            array(
                'status'        => 'success',
                //'response'      => $CronRun->run()
            )
        );
    }
   
}