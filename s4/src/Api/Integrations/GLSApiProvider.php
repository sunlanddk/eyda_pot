<?php

namespace App\Api\Integrations;

use App\Actions\Integrations\GLS\CreateShipmentResponseAction;
use App\Dto\Api\GLS\CreateShipmentResponse\CreateShipmentDto;
use Exception;
use JsonException;
use RuntimeException;
use Symfony\Component\HttpClient\Response\CurlResponse;
use Symfony\Component\HttpFoundation\Response;

class GLSApiProvider extends AbstractApiProvider
{
    public const API_URL = 'https://api.gls.dk/ws';
    public const API_LANGUAGE_CODE = 'DK';
    public const API_VERSION = 'V1';

    /**
     * @return string
     */
    public function getApiUrl(): string
    {
        return self::API_URL . '/' . self::API_LANGUAGE_CODE . '/' . self::API_VERSION;
    }

    /**
     * @param  array  $requestData
     * @return CreateShipmentDto
     * @throws RuntimeException
     */
    public function createShipmentRequest(array $requestData): CreateShipmentDto
    {
        $url = $this->getApiUrl() . '/CreateShipment';

        try {
            /** @var CurlResponse $response */
            $response = $this->getHttpClient()->request('POST', $url, [
                'headers' => [
                    'Accept' => 'application/json',
                    'Content-Type' => 'application/json',
                ],
                'body' => json_encode($requestData, JSON_THROW_ON_ERROR),
            ]);
        } catch (\Exception $e) {
            throw new RuntimeException('Error calling. ' . $url . 'Reply:' .$e->getMessage());
        }


        if ($response->getStatusCode() !== Response::HTTP_OK) {
            $errorMessage = 'Unknown error. Please contact support.';

            try {
                $responseDecoded = json_decode($response->getContent(false), true, 512, JSON_THROW_ON_ERROR);
            } catch (\JsonException $e) {
                $errorMessage = $e->getMessage();
            }

            if (!empty($responseDecoded['Message'])) {
                $errorMessage = $responseDecoded['Message'];
            }

            throw new RuntimeException($errorMessage, $response->getStatusCode());
        }
        return (new CreateShipmentResponseAction($response->toArray()))->execute();
    }
}
