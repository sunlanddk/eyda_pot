<?php

namespace App\Normalizer;

use App\Entity\Article\ArticleLang;
use App\Entity\Language;
use App\Entity\Order\Order;
use App\Entity\Order\OrderLine;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class OrderNormalizer implements NormalizerInterface
{
    private ?Language $language = null;

    public function __construct(?Language $language = null)
    {
        $this->language = $language;
    }

    /**
     * @param $data
     * @param $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof Order;
    }

    /**
     * @param $object
     * @param $format
     * @param array $context
     * @return array
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        /* @var Order $object */
        return [
            'id' => $object->getId(),
            'number' => $object->getReference(),
            'email' => $object->getEmail(),
            'postcode' => $object->getZip(),
            'description' => utf8_encode($object->getDescription()),
            'orderLines' => $this->normalizeOrderLines($object->getOrderLines()),
        ];
    }

    /**
     * @param Collection $objects
     * @return array
     */
    private function normalizeOrderLines(Collection $objects): array
    {
        $normalized = [];

        /** @var OrderLine $object */
        foreach ($objects as $object) {
            $packedQuantity = 0;
            foreach ($object->getOrderQuantity()->getPickOrderLines() as $pickOrderLine) {
                $packedQuantity += $pickOrderLine->getPackedQuantity();
            }

            $article = $object->getArticle();
            $articleColor = $object->getArticleColor();
            $articleSize = $object->getOrderQuantity()->getArticleSize();

            $data = [
                'id' => $object->getId(),
                'description' => utf8_encode($article->getDescription()),
                'color' => $articleColor ? utf8_encode($articleColor->getColor()->getDescription()) : '',
                'size' => $articleSize ? $articleSize->getName() : '',
                'quantity' => $packedQuantity,
                'priceCost' => $object->getPricesale(),
            ];

            if ($this->language) {
                $translations = $article->getArticleLangs();
                if (!empty($translations)) {
                    /** @var ArticleLang $translation */
                    foreach ($translations as $translation) {
                        if (!empty($translation->getDescription()) && ($this->language->getId() === $translation->getLanguageid())) {
                            $data['description'] = utf8_encode($translation->getDescription());
                            break;
                        }
                    }
                }
            }

            $normalized[] = $data;
        }

        return $normalized;
    }
}
