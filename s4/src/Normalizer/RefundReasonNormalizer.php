<?php

namespace App\Normalizer;

use App\Entity\Language;
use App\Entity\Order\RefundReason;
use App\Entity\Order\RefundReasonLang;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class RefundReasonNormalizer implements NormalizerInterface
{
    private ?Language $language = null;

    public function __construct(?Language $language = null)
    {
        $this->language = $language;
    }

    /**
     * @param $data
     * @param $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return is_array($data) && !empty($data) && $data[0] instanceof RefundReason;
    }

    /**
     * @param $objects
     * @param $format
     * @param array $context
     * @return array
     */
    public function normalize($objects, $format = null, array $context = []): array
    {
        $normalized = [];

        /** @var RefundReason $object */
        foreach ($objects as $object) {
            $data = [
                'id' => $object->getId(),
                'name' => utf8_encode($object->getName()),
                'description' => utf8_encode($object->getDescription()),
            ];

            if ($this->language) {
                $translations = $object->getRefundReasonLangs();
                if ($translations->count() > 0) {
                    /** @var RefundReasonLang $translation */
                    foreach ($translations as $translation) {
                        if (
                            !empty($translation->getName())
                            && ($this->language->getIsoCode() === $translation->getLanguage()->getIsoCode())
                        ) {
                            $data['name'] = utf8_encode($translation->getName());
                        }

                        if (
                            !empty($translation->getDescription())
                            && ($this->language->getIsoCode() === $translation->getLanguage()->getIsoCode())
                        ) {
                            $data['description'] = utf8_encode($translation->getDescription());
                            break;
                        }
                    }
                }
            }

            $normalized[] = $data;
        }

        return $normalized;
    }
}
