<?php

namespace App\Normalizer;

use App\Entity\Order\ReturnEntity;
use App\Entity\Order\ReturnLine;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Serializer\Normalizer\NormalizerInterface;

class ReturnNormalizer implements NormalizerInterface
{
    /**
     * @param $data
     * @param $format
     * @return bool
     */
    public function supportsNormalization($data, $format = null): bool
    {
        return $data instanceof ReturnEntity;
    }

    /**
     * @param $object
     * @param $format
     * @param  array  $context
     * @return array
     */
    public function normalize($object, $format = null, array $context = []): array
    {
        /* @var ReturnEntity $object */
        return [
            'id' => $object->getId(),
            'orderId' => $object->getOrder()->getId(),
            'fundsType' => $object->getFundsType(),
            'labelType' => $object->getLabelType(),
            'createDate' => $object->getCreatedate()->format('Y-m-d H:i:s'),
            'returnLines' => $this->normalizeReturnLines($object->getReturnLines()),
        ];
    }

    /**
     * @param  Collection  $objects
     * @return array
     */
    private function normalizeReturnLines(Collection $objects): array
    {
        $normalized = [];

        /** @var ReturnLine $object */
        foreach ($objects as $object) {
            if ($object->getActive()) {
                $normalized[] = [
                    'id' => $object->getId(),
                    'orderLineId' => $object->getOrderLine()->getId(),
                    'refundReasonId' => $object->getRefundReason()->getId(),
                    'quantity' => $object->getQuantity(),
                    'comment' => utf8_encode($object->getComment()),
                ];
            }
        }

        return $normalized;
    }
}
