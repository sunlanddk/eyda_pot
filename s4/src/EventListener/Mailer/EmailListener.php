<?php

namespace App\EventListener\Mailer;

use App\Event\EmailEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\MailerInterface;

class EmailListener implements EventSubscriberInterface
{
    private MailerInterface $mailer;

    public function __construct(MailerInterface $mailer)
    {
        $this->mailer = $mailer;
    }

    /**
     * @param EmailEvent $event
     * @return void
     */
    public function onEmailSend(EmailEvent $event): void
    {
        // Example:
        // $recipient = $event->getRecipient();
        // $message = $event->getMessage();

        // $email = (new Mailer())
        //     ->from('example@mail.com')
        //     ->to(new Address($recipient))
        //     ->subject('Return Label')
        //     ->htmlTemplate('emails/example.html.twig')
        //     // pass variables (name => value) to the template
        //     ->context([
        //         'expiration_date' => new \DateTime('+7 days'),
        //         'username' => 'foo',
        //     ]);

        // $this->mailer->send($email);
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'email.send' => 'onEmailSend',
        ];
    }
}
