<?php

namespace App\EventListener\Mailer;

use App\Entity\Order\ReturnEntity;
use App\Entity\Order\ReturnLine;
use App\Event\EmailEvent;
use App\Repository\Order\ReturnLineRepository;
use App\Support\Integrations\DHLService;
use App\Support\Integrations\GLSService;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Mailer\Exception\TransportExceptionInterface;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Contracts\Translation\TranslatorInterface;

class EmailReturnLabelListener implements EventSubscriberInterface
{
    private MailerInterface $mailer;
    private ParameterBagInterface $parameter;
    private EntityManagerInterface $entityManager;
    private TranslatorInterface $translator;

    public function __construct(
        MailerInterface $mailer,
        ParameterBagInterface $parameter,
        EntityManagerInterface $entityManager,
        TranslatorInterface $translator
    ) {
        $this->mailer = $mailer;
        $this->parameter = $parameter;
        $this->entityManager = $entityManager;
        $this->translator = $translator;
    }

    /**
     * @return ReturnLineRepository
     */
    public function getReturnLineRepository(): ReturnLineRepository
    {
        return $this->entityManager->getRepository(ReturnLine::class);
    }

    /**
     * @param EmailEvent $event
     * @return void
     * @throws TransportExceptionInterface
     */
    public function onEmailSend(EmailEvent $event): void
    {
        $recipient = $event->getRecipient();
        $parameters = $event->getParameters();
        $userLocale = $event->getUserLocale();

        $this->translator->setLocale($userLocale);

        $service = $parameters['service'];
        $returnEmail = $parameters['returnEmail'];
        /** @var ReturnEntity $returnEntity */
        $returnEntity = $parameters['returnEntity'];

        if ($recipient !== $returnEmail || $returnEntity->getEmail() !== $returnEmail) {
            $recipient = $returnEmail;
        }

        $email = (new TemplatedEmail())
            ->from(
                new Address(
                    $this->parameter->get('app.mailer.from_address'),
                    $this->parameter->get('app.mailer.from_name')
                )
            )
            ->to(new Address($recipient))
            ->subject(
                $this->translator->trans('email.return_label_template.subject.' . $returnEntity->getLabelType(), [], 'emails')
            );

        $filePath = $this->getFilePath($service, $returnEntity);
        if ($filePath) {
            $email->attachFromPath($filePath);
        }

        $email->htmlTemplate('emails/return_label_template.html.twig')
            // pass variables (name => value) to the template
            ->context([
                'user_locale' => $userLocale,
                'return' => $returnEntity,
                'return_lines' => $this->generateReturnLinesTableData($returnEntity),
            ]);

        $this->mailer->send($email);

        $returnEntity->setEmail($recipient);
        $returnEntity->setEmailSendDate(new \DateTime());
        $returnEntity->setEmailLanguage($userLocale);

        $this->entityManager->persist($returnEntity);
        $this->entityManager->flush();
    }

    /**
     * @param string $service
     * @param ReturnEntity $returnEntity
     * @return string
     */
    private function getFilePath(string $service, ReturnEntity $returnEntity): string
    {
        $filePath = null;

        switch ($service) {
            case GLSService::SERVICE_NAME:
                switch ($returnEntity->getLabelType()) {
                    case ReturnEntity::LABEL_TYPE_PDF:
                        $filePath = sprintf(GLSService::FILE_PATH . GLSService::FILE_NAME_PDF, $returnEntity->getTrackingCode());
                        break;
                    case ReturnEntity::LABEL_TYPE_QR:
                        $filePath = sprintf(GLSService::FILE_PATH . GLSService::FILE_NAME_PNG, $returnEntity->getTrackingCode());
                        break;
                }
                break;
            case DHLService::SERVICE_NAME:
                break;
            default:
                throw new RuntimeException('Service not found');
        }

        return $filePath;
    }

    /**
     * @param ReturnEntity $returnEntity
     * @return array
     */
    private function generateReturnLinesTableData(ReturnEntity $returnEntity): array
    {
        $data = [];

        foreach ($returnEntity->getReturnLines() as $returnLine) {
            $data[] = [
                'product_name' => $returnLine->getOrderLine()->getDescription(),
                'returned_quantity' => $this->getReturnedQuantity($returnEntity, $returnLine),
                'returning_quantity' => $returnLine->getQuantity(),
                'refund_reason' => $returnLine->getRefundReason()->getName(),
                'comment' => $returnLine->getComment(),
            ];
        }

        return $data;
    }

    /**
     * @param ReturnEntity $returnEntity
     * @param ReturnLine $returnLine
     * @return int
     */
    private function getReturnedQuantity(ReturnEntity $returnEntity, ReturnLine $returnLine): int
    {
        $returnLines = $this->getReturnLineRepository()->findBy([
            'return' => $returnEntity,
            'orderLine' => $returnLine->getOrderLine(),
            'active' => true,
        ]);

        if (count($returnLines) > 0) {
            return array_reduce($returnLines, static function ($accumulator, $returnLine) {
                return $accumulator + $returnLine->getQuantity();
            }, 0);
        }

        return 0;
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            'email.send.' . EmailEvent::TYPE_RETURN_LABEL => 'onEmailSend',
        ];
    }
}
