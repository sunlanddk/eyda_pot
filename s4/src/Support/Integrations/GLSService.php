<?php

namespace App\Support\Integrations;

use App\Actions\Integrations\GLS\CreateShipmentDtoAction;
use App\Api\Integrations\GLSApiProvider;
use App\Dto\Api\GLS\CreateShipmentResponse\CreateShipmentDto as CreateShipmentDtoResponse;
use App\Entity\Order\ReturnEntity;
use App\Event\EmailEvent;
use Exception;
use RuntimeException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\ResponseHeaderBag;

class GLSService extends IntegrationService implements IntegrationInterface
{
    public const SERVICE_NAME = 'GLS';

    public const FILE_PATH = 'labels/gls/';
    public const FILE_NAME_PDF = 'gls_%s_return_label.pdf';
    public const FILE_NAME_PNG = 'gls_%s_return_qr_code.png';

    private GLSApiProvider $apiProvider;

    public function __construct()
    {
        $this->apiProvider = new GLSApiProvider();
    }

    /**
     * @return GLSApiProvider
     */
    public function getApiProvider(): GLSApiProvider
    {
        return $this->apiProvider;
    }

    /**
     * @param Request $request
     * @param ReturnEntity $returnEntity
     * @param string $labelType
     * @return BinaryFileResponse|null
     */
    public function resolveReturnLabel(Request $request, ReturnEntity $returnEntity, string $labelType): ?BinaryFileResponse
    {
        if ($returnEntity->getTrackingCode()) {
            return $this->getReturnLabel($request, $returnEntity, $labelType);
        }

        return $this->generateReturnLabel($request, $returnEntity, $labelType);
    }

    /**
     * @param ReturnEntity $returnEntity
     * @param string $labelType
     * @return BinaryFileResponse
     */
    private function getReturnLabel(Request $request, ReturnEntity $returnEntity, string $labelType): BinaryFileResponse
    {
        switch ($labelType) {
            case ReturnEntity::LABEL_TYPE_QR:
                $filePath = sprintf(self::FILE_PATH . self::FILE_NAME_PNG, $returnEntity->getTrackingCode());
                break;
            default:
                $filePath = sprintf(self::FILE_PATH . self::FILE_NAME_PDF, $returnEntity->getTrackingCode());
                break;
        }
        $filesystem = new Filesystem();
        if (!$filesystem->exists($filePath)) {
            return $this->generateReturnLabel($request, $returnEntity, $labelType);
        }

        $response = new BinaryFileResponse($filePath);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/' . strtolower($labelType));
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE
        );

        return $response;
    }

    /**
     * @param Request $request
     * @param ReturnEntity $returnEntity
     * @param string $labelType
     * @return BinaryFileResponse
     */
    private function generateReturnLabel(Request $request, ReturnEntity $returnEntity, string $labelType): BinaryFileResponse
    {
        $createShipmentDto = new CreateShipmentDtoAction(
            $this->getCountryRepository(),
            $this->getOrderAddressesRepository(),
            $this->getVariantcodeRepository()
        );
        $createShipmentDto->setLabelType($labelType);
        $requestData = $createShipmentDto->execute($returnEntity);

        $responseApi = null;
        try {
            $responseApi = $this->getApiProvider()->createShipmentRequest($requestData->toArray());
            $parcelNumber = $responseApi->getParcels()[0]->getParcelNumber();

        } catch (\Exception $exception) {
            $errorMessage = $exception->getMessage();
            $returnEntity->setEmailLog($errorMessage);
            if ($responseApi && !empty($responseApi->getResponse()) && !empty(json_encode($responseApi->getResponse()))) {
                $returnEntity->setEmailLog(json_encode($responseApi->getResponse()));
            }
            $this->getReturnRepository()->save($returnEntity, true);

            if ($exception->getCode() === Response::HTTP_UNAUTHORIZED) {
                $errorMessage = 'Return request created, but GLS request failed. Please contact the support team.';
            }
            throw new RuntimeException($errorMessage, Response::HTTP_BAD_REQUEST);
        }

        if (empty($parcelNumber)) {
            $returnEntity->setEmailLog(json_encode($responseApi->getResponse()));
            $this->getReturnRepository()->save($returnEntity, true);

            throw new RuntimeException('The parcel number is empty. Please contact the support team.', Response::HTTP_BAD_REQUEST);
        }

        $requestLabelFormat = strtolower($requestData->getLabelFormat());


        switch ($requestLabelFormat) {
            case ReturnEntity::LABEL_TYPE_QR:
                $response = $this->handleQR($responseApi, $parcelNumber);
                break;
            case ReturnEntity::LABEL_TYPE_PDF:
                $response = $this->handlePDF($responseApi, $parcelNumber);
                break;
            default:
                throw new RuntimeException('GLS New Returns: The label type is not supported.', Response::HTTP_BAD_REQUEST);
        }

        $returnEntity->setEmailLog();
        $returnEntity->setTrackingCode($parcelNumber);
        $this->getReturnRepository()->save($returnEntity, true);

        $event = new EmailEvent(
            $returnEntity->getOrder()->getEmail(),
            [
                'service' => self::SERVICE_NAME,
                'returnEmail' => $returnEntity->getOrder()->getEmail(),
                'returnEntity' => $returnEntity,
            ],
            $request->getLocale()
        );
        $this->getEventDispatcher()->dispatch($event, 'email.send.' . EmailEvent::TYPE_RETURN_LABEL);

        return $response;
    }

    /**
     * @param CreateShipmentDtoResponse $responseApi
     * @param string $parcelNumber
     * @return BinaryFileResponse
     */
    private function handlePDF(CreateShipmentDtoResponse $responseApi, string $parcelNumber): BinaryFileResponse
    {
        if (!$responseApi->getPDF()) {
            throw new RuntimeException('GLS New Returns: The PDF file could not be generated.', Response::HTTP_BAD_REQUEST);
        }

        $decoded = base64_decode($responseApi->getPDF());
        $filePath = sprintf(self::FILE_PATH . self::FILE_NAME_PDF, $parcelNumber);

        $filesystem = new Filesystem();
        $filesystem->dumpFile($filePath, $decoded);
        if (!$filesystem->exists($filePath)) {
            throw new RuntimeException('GLS New Returns: The PDF file could not be saved.', Response::HTTP_BAD_REQUEST);
        }

        $response = new BinaryFileResponse($filePath);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/pdf');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf(self::FILE_NAME_PDF, $parcelNumber)
        );

        return $response;
    }

    /**
     * @param CreateShipmentDtoResponse $responseApi
     * @param string $parcelNumber
     * @return BinaryFileResponse
     */
    private function handleQR(CreateShipmentDtoResponse $responseApi, string $parcelNumber): BinaryFileResponse
    {
        if (!$responseApi->getPNG()) {
            throw new RuntimeException('GLS New Returns: The QR code file could not be generated.', Response::HTTP_BAD_REQUEST);
        }

        $decoded = base64_decode($responseApi->getPNG());
        $filePath = sprintf(self::FILE_PATH . self::FILE_NAME_PNG, $parcelNumber);

        $filesystem = new Filesystem();
        $filesystem->dumpFile($filePath, $decoded);
        if (!$filesystem->exists($filePath)) {
            throw new RuntimeException('GLS New Returns: The QR code file could not be saved.', Response::HTTP_BAD_REQUEST);
        }

        $response = new BinaryFileResponse($filePath);
        $response->headers->set('Access-Control-Allow-Origin', '*');
        $response->headers->set('Content-Type', 'application/png');
        $response->setContentDisposition(
            ResponseHeaderBag::DISPOSITION_INLINE,
            sprintf(self::FILE_NAME_PNG, $parcelNumber)
        );

        return $response;
    }
}
