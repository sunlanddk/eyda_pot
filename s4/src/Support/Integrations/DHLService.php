<?php

namespace App\Support\Integrations;

use App\Entity\Order\ReturnEntity;
use Symfony\Component\HttpFoundation\BinaryFileResponse;
use Symfony\Component\HttpFoundation\Request;

class DHLService extends IntegrationService implements IntegrationInterface
{
    public const SERVICE_NAME = 'DHL';

    /**
     * @param Request $request
     * @param ReturnEntity $returnEntity
     * @param string $labelType
     * @return BinaryFileResponse|null
     */
    public function resolveReturnLabel(Request $request, ReturnEntity $returnEntity, string $labelType): ?BinaryFileResponse
    {
        return null;
    }
}
