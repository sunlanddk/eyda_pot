<?php

namespace App\Support\Integrations;

use App\Repository\Article\VariantcodeRepository;
use App\Repository\CountryRepository;
use App\Repository\Order\OrderaddressesRepository;
use App\Repository\Order\ReturnRepository;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Contracts\Translation\TranslatorInterface;

class IntegrationService
{
    /**
     * @var array|string[] $services
     */
    private array $services = [
        GLSService::SERVICE_NAME => GLSService::class,
        DHLService::SERVICE_NAME => DHLService::class,
    ];

    private OrderaddressesRepository $orderAddressesRepository;
    private CountryRepository $countryRepository;
    private ReturnRepository $returnRepository;
    private VariantcodeRepository $variantcodeRepository;
    private EventDispatcherInterface $eventDispatcher;
    private TranslatorInterface $translator;

    /**
     * @param  string  $service
     * @return GLSService|DHLService
     */
    public function resolveService(string $service)
    {
        $service = new $this->services[$service];
        $service->setOrderAddressesRepository($this->getOrderAddressesRepository());
        $service->setCountryRepository($this->getCountryRepository());
        $service->setReturnRepository($this->getReturnRepository());
        $service->setVariantcodeRepository($this->getVariantcodeRepository());
        $service->setEventDispatcher($this->getEventDispatcher());
        $service->setTranslatorInterface($this->getTranslatorInterface());

        return $service;
    }

    /**
     * @return OrderaddressesRepository
     */
    public function getOrderAddressesRepository(): OrderaddressesRepository
    {
        return $this->orderAddressesRepository;
    }

    /**
     * @param  OrderaddressesRepository  $orderAddressesRepository
     * @return void
     * @required
     */
    public function setOrderAddressesRepository(OrderaddressesRepository $orderAddressesRepository): void
    {
        $this->orderAddressesRepository = $orderAddressesRepository;
    }

    /**
     * @return CountryRepository
     */
    public function getCountryRepository(): CountryRepository
    {
        return $this->countryRepository;
    }

    /**
     * @param  CountryRepository  $countryRepository
     * @return void
     * @required
     */
    public function setCountryRepository(CountryRepository $countryRepository): void
    {
        $this->countryRepository = $countryRepository;
    }

    /**
     * @return ReturnRepository
     */
    public function getReturnRepository(): ReturnRepository
    {
        return $this->returnRepository;
    }

    /**
     * @param  ReturnRepository  $returnRepository
     * @return void
     * @required
     */
    public function setReturnRepository(ReturnRepository $returnRepository): void
    {
        $this->returnRepository = $returnRepository;
    }

    /**
     * @return VariantcodeRepository
     */
    public function getVariantcodeRepository(): VariantcodeRepository
    {
        return $this->variantcodeRepository;
    }

    /**
     * @param  VariantcodeRepository  $variantcodeRepository
     * @return void
     * @required
     */
    public function setVariantcodeRepository(VariantcodeRepository $variantcodeRepository): void
    {
        $this->variantcodeRepository = $variantcodeRepository;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getEventDispatcher(): EventDispatcherInterface
    {
        return $this->eventDispatcher;
    }

    /**
     * @param EventDispatcherInterface $eventDispatcher
     * @return void
     * @required
     */
    public function setEventDispatcher(EventDispatcherInterface $eventDispatcher): void
    {
        $this->eventDispatcher = $eventDispatcher;
    }

    /**
     * @return TranslatorInterface
     */
    public function getTranslatorInterface(): TranslatorInterface
    {
        return $this->translator;
    }

    /**
     * @param TranslatorInterface $translator
     * @return void
     * @required
     */
    public function setTranslatorInterface(TranslatorInterface $translator): void
    {
        $this->translator = $translator;
    }
}
