<?php

namespace App\Request;

use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Exception\ValidatorException;
use Symfony\Component\Validator\Validator\ValidatorInterface;

abstract class AbstractRequest
{
    protected ValidatorInterface $validator;

    /**
     * @param  ValidatorInterface  $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
        $this->populate();

        if ($this->autoValidateRequest()) {
            $this->validate();
        }
    }

    /**
     * @return void
     */
    protected function populate(): void
    {
        $requestPost = json_decode($this->getRequest()->getContent(), true) ?? [];
        $requestQuery = $this->getRequest()->query->all() ?? [];

        foreach (array_merge($requestPost, $requestQuery) as $property => $value) {
            if (property_exists($this, $property)) {
                $this->{$property} = $value;
            }
        }
    }

    /**
     * @return Request
     */
    public function getRequest(): Request
    {
        return Request::createFromGlobals();
    }

    /**
     * @return bool
     */
    protected function autoValidateRequest(): bool
    {
        return true;
    }

    /**
     * @return void
     */
    public function validate(): void
    {
        $errors = $this->validator->validate($this);

        $messages = ['message' => 'Validation failed', 'errors' => []];

        /** @var ConstraintViolation $message */
        foreach ($errors as $message) {
            $messages['errors'][] = [
                'property' => $message->getPropertyPath(),
                'value' => $message->getInvalidValue(),
                'message' => $message->getMessage(),
            ];
        }

        if (count($messages['errors']) > 0) {
            $response = new JsonResponse($messages, Response::HTTP_UNPROCESSABLE_ENTITY);
            $response->send();

            throw new ValidatorException((string)$errors);
        }
    }
}
