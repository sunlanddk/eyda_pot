<?php

namespace App\Request\Returns;

use App\Request\AbstractRequest;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class SendEmailRequest extends AbstractRequest
{
    /**
     * @var integer
     * @Type("int")
     * @NotBlank()
     */
    public int $returnId;

    /**
     * @var string
     * @Type("string")
     * @NotBlank()
     */
    public string $returnEmail;
}
