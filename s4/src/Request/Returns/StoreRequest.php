<?php

namespace App\Request\Returns;

use App\Request\AbstractRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class StoreRequest extends AbstractRequest
{
    /**
     * @var string
     * @Type("string")
     * @NotBlank()
     * @Assert\Length(min=1, max=40)
     */
    public string $orderNumber;

    /**
     * @var string
     * @Type("string")
     * @NotBlank()
     * @Assert\Length(min=5, max=100)
     */
    public string $orderEmail;

    /**
     * @var string
     * @Type("string")
     * @NotBlank()
     * @Assert\Length(min=1, max=20)
     */
    public string $orderPostcode;

    /**
     * @var array
     * @Type("array")
     */
    public array $orderReturn;

    /**
     * @var array
     * @Type("array")
     * @Assert\Count(min=1)
     */
    public array $orderReturnItems;
}
