<?php

namespace App\Request\Order;

use App\Request\AbstractRequest;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Type;

class ShowRequest extends AbstractRequest
{
    /**
     * @var string
     * @Type("string")
     * @NotBlank()
     * @Assert\Length(min=1, max=40)
     */
    public string $orderNumber;

    /**
     * @var string
     * @Type("string")
     * @NotBlank()
     * @Assert\Length(min=5, max=100)
     */
    public string $orderEmail;

    /**
     * @var string|null
     * @Type("string")
     * @Assert\Length(min=1, max=20)
     */
    public ?string $orderPostcode = null;
}
