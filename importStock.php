<?php 
require_once('lib/config.inc');
require_once('lib/db.inc');
require_once('lib/table.inc');

// $csv = file_get_contents('valuation.csv');
die();

// Sku = [0]
// Quantity = [3]
// Cost Price = [4]

				$post = array(
					'Type' => 'Receive',
					'ObjectId' => 0,
					'FromStockId' => 0,
					'ToStockId' => 0,
				);
				$transactionId = tableWrite('transaction', $post);
				$totalPrice = 0;
				$totalQty = 0;

				$post = array(
					'StockId' => 14321,
				);
				$containerId = tableWrite('contianer', $post);

$file = fopen('valuation.csv', 'r');
while (($lines = fgetcsv($file, 0, "\r")) !== FALSE) {
  	//$line is an array of the csv elements
	foreach ($lines as $key => $line) {
		$line = str_replace('"', '', $line);
		$values = explode(';', $line);
		if($values[0] === 'Number'){
			continue;
		}
		// print_r($line);

	  	$variantCodeId = tableGetFieldWhere('variantcode', 'Id', 'Active=1 AND SKU="'.$values[0].'"');
		$articleId = tableGetFieldWhere('variantcode', 'ArticleId', 'Active=1 AND SKU="'.$values[0].'"');
		$articleColorId = tableGetFieldWhere('variantcode', 'ArticleColorId', 'Active=1 AND SKU="'.$values[0].'"');
		$articleSizeId = tableGetFieldWhere('variantcode', 'ArticleSizeId', 'Active=1 AND SKU="'.$values[0].'"');
		if((int)$variantCodeId > 0){
			$item = tableGetFieldWhere('item', 'Id', sprintf("Active=1 AND ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d", $articleId, $articleColorId, $articleSizeId));
			if($item > 0){
				// $totalPrice += ((float)$values[4] * (int)$values[3]);
			}
			else{

				$post = array(
					'TransactionId' => $transactionId,
					'ArticleSizeId' => $articleSizeId,
					'Quantity' => (int)$values[3],
					'Value' => (float)$values[4],
				);

				$transactionitemId = tableWrite('transactionitem', $post);
				$totalPrice += ((float)$values[4] * (int)$values[3]);
				$totalQty += (int)$values[3];

				$post = array(
					'ArticleId' => $articleId,
					'ArticleColorId' => $articleColorId,
					'ArticleSizeId' => $articleSizeId,
					'Quantity' => (int)$values[3],
					'Price' => (float)$values[4],
					'TransactionItemId' => $transactionitemId
					'ContainerId' => $containerId,
					'OwnerId' => 787,
					'Sortation' => 1
				);
				tableWrite('item', $post);
			}
		}
	}
}

echo '<br />';
echo '<br />';
echo '<br />';

echo 'Total Price ' .$totalPrice;
echo '<br />';
echo 'Total Qty ' .$totalQty;
echo '<br />';


fclose($file);