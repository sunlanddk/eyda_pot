<?php
   require       "lib/config.inc" ;
   require       "lib/table.inc" ;
   require       "lib/db.inc" ;
   require       "lib/url.inc" ;
   require_once  "lib/log.inc" ;

   require_once  'lib/navigation.inc' ;

   
   
   // $location = _LEGACY_URI."/login";
   $location = _LEGACY_URI."/login";

   if (function_exists('mb_internal_encoding')) {
      mb_internal_encoding('ISO-8859-1');
   }
   // define('SFMODULE_PATH', '/pot_fub/sfmodule/sw');
   define('SMARTY_RESOURCE_CHAR_SET', 'ISO-8859-1');
   require_once  "lib/smarty/Smarty.class.php";


   ini_set('default_charset', 'ISO-8859-1');
   ini_set('include_path', ini_get('include_path') . PATH_SEPARATOR . 'PEAR');

   // Initialization
   $Error = false ;
   list($usec, $sec) = explode(" ",microtime()) ;
   $startexec = (float)$usec + (float)$sec ;
   $PermMap         = array ("r"=>"PermRead",       "w"=>"PermWrite",       "d"=>"PermDelete",      "c"=>"PermCreate") ;
   $PermMapOther    = array ("r"=>"PermReadOther",  "w"=>"PermWriteOther",  "d"=>"PermDeleteOther", "c"=>"PermCreate") ;
   $PermFields      = array ("PermRead", "PermWrite", "PermCreate", "PermDelete", "PermReadOther", "PermWriteOther", "PermDeleteOther") ;
   $NavigationParam = array () ;

   //smarty initialization

   $smarty = new Smarty();
   $smarty
      ->setTemplateDir('smarty/templates/')
      ->setCompileDir('smarty/templates_c/')
      ->setConfigDir('smarty/configs/')
      ->setCacheDir('smarty/cache/')
      ->addPluginsDir('smarty/plugins/');

   $isAjax = isset($_SERVER['HTTP_X_REQUESTED_WITH'])
      AND strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest';

   /*
    * ==================
    * Session check
    * =================
    */
   if (empty($_COOKIE[COOKIE_NAME])) {
     // Establish new session
      //print_r(sprintf("Location: %s", $location));
      //die();
      header(sprintf("Location: %s", $location));
      die();
      include "module/main/session.inc" ;
   }
   $uid = $_COOKIE[COOKIE_NAME] ;
   $query     = sprintf ("SELECT Id, CreateDate, IP FROM Session WHERE UID='%s'", $uid) ;
   $result    = dbQuery ($query) ;
   $Session   = dbFetch ($result) ;
   dbQueryFree ($result);
   if (!$Session) {
     logPrintf (logWARNING, "session not found, IP %s, uid %s", $_SERVER["REMOTE_ADDR"], $uid) ;
     header(sprintf("Location: %s", $location));
     die();
     include "module/main/session.inc" ;

   }
   if ($Session["IP"] != $_SERVER["REMOTE_ADDR"]) {
      logPrintf (logWARNING, "session mismatch, IP %s, uid %s", $_SERVER["REMOTE_ADDR"], $uid) ;
      header(sprintf("Location: %s", $location));
      die();
      include "module/main/session.inc" ;

   }

   /*
    * ==================
    * Login check
    * =================
    */
   $query  = sprintf ("SELECT login.* FROM (login) WHERE SessionId=%d AND Active=1", $Session["Id"]) ;
   $result = dbQuery ($query) ;
   $Login  = dbFetch($result) ;
   dbQueryFree ($result) ;

   if ($Login && dbDateDecode($Login["AccessDate"]) < (time()-60*600)) {
     include "module/main/logout.inc" ;
     header(sprintf("Location: %s", $location));
     die();
     unset ($Login) ;
   }

   // Do Login ?
   if (!$Login) {
      header(sprintf("Location: %s", $location));
      die();
      include "module/main/login.inc" ;
      if ($Config['debugDump']) {
         logDump () ;
      }
      exit ;
   }

   /*
    * ==================
   * User Information
   * =================
   */
   $query = sprintf (
         "SELECT User.*, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, Company.Name AS CompanyName, Company.Internal AS Internal, Company.Surplus AS CompanySurplus, Company.CurrencyId, TimeZone.Name AS TimeZone, Role.Name AS RoleName ".
         "FROM User ".
         "LEFT JOIN Company ON User.CompanyId=Company.Id ".
         "LEFT JOIN TimeZone ON User.TimeZoneId=TimeZone.Id ".
         "LEFT JOIN Role ON Role.Id=User.RoleId AND Role.Active=1 ".
         "WHERE User.Id=%d AND User.Active=1 AND User.Login=1", $Login["UserId"]
   );
   $result = dbQuery ($query) ;
   $User   = dbFetch($result) ;
   dbQueryFree ($result) ;
   if (!$User) {
      logPrintf (logFATAL, "user not found, id %d", $Login["UserId"]) ;
      if ($Config['debugDump']) {
         logDump () ;
      }
      exit ;
   }

   // Timezone
   if ($User["TimeZone"]) {
      putenv (sprintf("TZ=%s", $User["TimeZone"])) ;
      $_ENV["TZ"] = $User["TimeZone"] ;
   }


   if ($_POST["nid"] > 0) {
      $nid = (int)$_POST["nid"] ;
   } else if ($_GET["nid"] > 0) {
      $nid = (int)$_GET["nid"] ;
   } else {
      $nid = 2244 ;
   }


   /*
    * ==================
    * Navigation reference
    * =================
    */
   $q = 'SELECT '.
         'Navigation.*, '.
         'NavigationType.Mark AS Type, NavigationType.LayoutRaw, NavigationType.LayoutMain, NavigationType.LayoutPopup, NavigationType.LayoutSubMenu, '.
         'Function.PermRecord, Function.PermPublic, Function.PermAdminSystem, Function.PermAdminProject, Function.PermProjectRole, Function.Mark AS FunctionMark '.
      'FROM (Navigation, NavigationType, Function) '.
      'WHERE %s AND Navigation.FunctionId=Function.Id AND Function.Active=1 AND Navigation.Active=1 AND Navigation.NavigationTypeId=NavigationType.Id' ;
   $query      = sprintf ($q, 'Navigation.Id='.$nid) ;
   $result     = dbQuery ($query) ;
   $Navigation = dbFetch ($result) ;
   dbQueryFree ($result);

   if ($Navigation['Type'] == 'link') {
      $p          = $Navigation ;
      $query      = sprintf ($q, 'Navigation.Mark="'.$Navigation['Function'].'"') ;
      $result     = dbQuery ($query) ;
      $Navigation = dbFetch ($result) ;
      dbQueryFree ($result);
     if ($Navigation) {
        if ($p['Header'] != '') {
           $Navigation['Header'] = $p['Header'] ;
        }
        if ($p['Parameters'] != '') {
           $Navigation['Parameters'] = $p['Parameters'] ;
        }
        $Navigation['MustLoad']     = $p['MustLoad'] ;
        $Navigation['RefIdInclude'] = $p['RefIdInclude'] ;
     }
   }
   if (!$Navigation) {
      logPrintf (logFATAL, "no module 1, nid %d\n", $nid) ;
      if ($Config['debugDump']) {
         logDump () ;
      }
      exit ;
   }
   if (strlen($Navigation["Module"]) == 0) {
      //search for the lowest default toolbar
      $def = searchDefaultToolbar($Navigation["Id"], (int)$_GET['DefaultNID']);
      if (isset($def)) {
         $Navigation["Module"]            = $def["Module"];
         $Navigation["Function"]          = $def["Function"];
         $Navigation["DefaultNID"]        = $def["Id"];
         $Navigation["DefaultLayoutRaw"]  = $def["LayoutRaw"];
         $Navigation["Header"]            = $def['Header'];
      }

      if (strlen($Navigation["Module"]) == 0) {
         logPrintf (logFATAL, "no module 2, nid %d\n", $nid) ;
         if ($Config['debugDump']) {
            logDump () ;
         }
         exit ;
      }
   }
   // Id
   if ($Navigation["Type"] == "command") {
      $Id = (int)$_POST["id"] ;
   } else if (isset($_GET["id"])) {
      $Id = (int)$_GET["id"] ;
   } else {
      $Id = -1 ;
   }
   if ($Navigation['Function']=='reportview_xml') {
        $q = 'SELECT '.
             'Navigation.* '.
             'FROM (Navigation) '.
             'WHERE %s';
        $query      = sprintf ($q, 'Navigation.Id='.$nid) ;
        $result     = dbQuery ($query) ;
        $xml_param = dbFetch ($result) ;
        dbQueryFree ($result);

        $navParameters = explode(',',$xml_param['Parameters']);
        if((int)$navParameters[0]) {
            $Id = (int)$navParameters[0];
            unset($navParameters[0]);
        }
   }
    
   /*
    * ==================
   * Load Permissions
   * =================
   */
   $roleid = $User['RoleId'] ;
   if ($roleid == 0) {
      $Error = "No Role selecetd for user" ;
   }
   $query = sprintf (
         "SELECT Permission.* ".
         "FROM Permission ".
         "WHERE Permission.RoleId=%d AND Permission.Active=1 AND Permission.FunctionId=%d",
         $roleid, $Navigation['FunctionId']
   ) ;
   $result     = dbQuery ($query) ;
   $Permission = dbFetch ($result) ;
   dbQueryFree ($result) ;
   if ($Permission['RoleId'] != $roleid) {
      $Permission['RoleId'] = $roleid ;
   }

   /*
    * ==================
    * Load application record
    * =================
    */
    if (!$Error) {
      $name = sprintf("module/%s.inc", $Navigation['Module']); //the new way
      if (file_exists ($name)) {
         $Error = include $name ;
         if (!$Error){
            $_className = explode('/', $Navigation['Module']);
            $_count = count($_className);
            $Application = new $_className[$_count - 1];
         }
      } else {
         //the old way
         $name = sprintf ("module/%s/load.inc", $Navigation["Module"]) ;
         if (file_exists ($name)) {
            $Error = include $name ;
            if (!$Error) {
               if (is_array($Record)) {
                  $Application = new stdClass();
                  foreach ($Record as $key => $val) {
                     $Application->$key = $val;
                  }
               }
            }
         }
      }
   }

   // Verify that record has been loaded
   if (!$Error && ($Navigation['MustLoad'] || $Navigation['PermInternal'] || $Navigation['PermOwner'] || $Navigation['PermRecord'])) {
      if (!($Id > 0) || $Id != $Application->Id) {
         $Error = sprintf ("record not found, nid %d, id %d, appId %d, rid %d", $nid, $Id, $Application->Id, $Record['Id']) ;
      }
   }

   /*
    * ==================
    * Check Permissions
    * =================
    */
   if (!$Error) {
      if (!$User["AdminSystem"]) {
         if ($Navigation["PermAdminSystem"]) {
            $Error = "You do not have access to this function (index, step 0)" ;
          } else {
             if (!$User["AdminProject"]) {
                if ($Navigation["PermAdminProject"]) {
                   $Error = "You do not have access to this function (index, step 1)" ;
                } else {
                  if (!$Navigation["PermPublic"]) {
                     if ($Navigation["PermInternal"] and !$User["Internal"] and !$Application->External) {
                        $Error = "You do not have permission to access this function (index, step 2)" ;
                     } else {
                        if ($Navigation["PermOwner"] and $Application->CreateUserId != $User["Id"]) {
                           if (!$Permission[$PermMapOther[$Navigation["Operation"]]]) {
                              $Error = "You do not have permission to access this function (index, step 3)" ;
                           }
                        } else {
                           if (!$Permission[$PermMap[$Navigation["Operation"]]]) {
//                              $Error = "You do not have permission to access this function (index, step 4) " . $User["Id"] ;
                           }
                        }
                     }
                  }
               }
            }
         }
      }
   }

   // Update status
   if (!$Error) {
      // Access information
      $query = sprintf ("UPDATE Login SET AccessDate='%s' WHERE Id=%d AND Active=1", dbDateEncode(time()), $Login["Id"]) ;
     dbRawQuery ($query) ;
   }

   // make function name
   if (!$Error) {
      $funcname = $Navigation['Function'];//'F' . preg_replace('/\-c/', 'C', $Navigation['Function']);
   }

   /*
    * ==================
    * Command execution
    * =================
    */
   if (!$Error && $Navigation["Type"]=="command") {
      $Id = (int)$_POST["id"] ;
      if (method_exists($Application, $funcname)) {
         $Error = $Application->$funcname();
      } else {
         $Error = include sprintf ("module/%s/%s.inc", $Navigation["Module"], $Navigation["Function"]) ;
      }
      if (!is_string($Error)) {
         switch ($Error) {
            case 0 :
               if (headers_sent()) {
                  $Error = 'Header allready send, stopping execution' ;
                  break ;
               }

               // On success, use browser history to step back
               printf ("<script type='text/javascript' src='lib/app.js'></script>\n") ;
               printf ("<script type='text/javascript'>\nappCommandEnd(%d)\n</script>\n", $Navigation["Back"]) ;

            case -1 :
               // next step handled by application (ex. navigationCommandMark)
               if ($Config['debugDump']) {
                  logDump () ;
               }
               exit ;

            default :
               $Error = 'code ' . (int)$Error ;
               break ;
         }
      }
   }

   ob_start();
   /*
    * ==================
    * Handle raw layout
    * =================
    */
   if (!$Error && $Navigation["LayoutRaw"]) {
      if (method_exists($Application,$funcname) ) {
         $Error = $Application->$funcname();
      } else {
          $Error = include sprintf ("module/%s/%s.inc", $Navigation["Module"], $Navigation["Function"]) ;
      }
   }

   // Display Errors properly for raw pages
   if ($Error and !headers_sent() and $Navigation["LayoutRaw"]) {
      $Navigation["LayoutRaw"] = 0 ;
      if (!$Navigation['LayoutPopup']) {
         $Navigation["LayoutMain"] = 1 ;
      }
   }

   /*
    * ==================
    * Menus
    * =================
    */
   if (!$Navigation["LayoutRaw"]) {

     // HTTP Header
     if (!headers_sent()) {
        switch ($_SERVER["SERVER_PROTOCOL"]) {
           case "HTTP/1.1":
              //header ("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
              //header ("Last-Modified: " . gmdate("D, d M Y H:i:s", time() + 12*60*60) . " GMT");
              header ("Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0");
              break ;

           case "HTTP/1.0":
              header ("Pragma: no-cache") ;
              break ;
        }
     }


      $_menu      = '';
      $menu = array () ;
      $child = array () ;
      if ($Navigation["LayoutMain"]) {
         // Menu Bar, line 2
         // Generate menu structure

         navigationItems ($menu, -1, "m") ;

         foreach ($menu as $row) {
            $child[((int)$row["ParentId"])][] = (int)$row["Id"] ;
         }
         $_menu = $Config['newLayout'] ? prepareMainMenuNew (0,0) : prepareMainMenu (0,0) ;
      } 

      $_isClaim = stripos($Navigation['Module'], 'claim') !== false;
      if ($_isClaim) {
         if (!class_exists('ClaimCart')) {

            require_once  "module/sales/claimcart.inc" ;
         }
         $cart = new ClaimCart();
      } else {
         if (!class_exists('OrderCart')) {
            require_once  "module/sales/ordercart.inc" ;
         }
         $cart = new OrderCart();
      }

      $User['CompanyName'] = tableGetField("Company", "Name", $User["CompanyId"]);
	  $_cartCustomerid = (int)$_COOKIE['customerId'] ;
	  $_cartCustomerid = isset($_POST['companyId']) ? (int)$_POST['companyId'] : (int)$_COOKIE['customerId'];
      $_cartCustomer = tableGetField("Company", "Name", $_cartCustomerid);
      $_toolmenu = ($Config['newLayout'] ? prepareToolMenuNew() : prepareToolMenu());
      $smarty->assign(array(
           'Id'          => $Id,
           'nid'         => $nid,
           'Config'      => $Config,
           'Navigation'  => $Navigation,
           'User'        => $User,
           'Error'       => $Error,
           'htmlHeader'  => sprintf($Config["HTMLHeader"], $Navigation["Header"], $User["Loginname"], $User['CompanyName']),
           'menu'        => $_menu,
           'toolmenu'    => $_toolmenu,
           'cartCustomer'    => $_cartCustomer,
           'hasToolMenu' => strlen($_toolmenu) > 0,
           'cartInfo'    => $cart->get_cart_info(),
           'cartMark'    => $cart->get_mark()
      ));
      if ($Config['newLayout']) {
         $smarty->display('layout/index_new.tpl');
      } else {
         $smarty->display('layout/index.tpl');
      }
      $smarty->clearAllAssign();
      unset ($child) ;
      unset ($menu) ;

     // Sub navigation
     if ($Navigation["LayoutSubMenu"]) {
       include "module/".$Navigation["Module"]."/submenu.inc" ;
     }

     // App Window page
     if (!$Config['newLayout']) {
        printf ("<td class='main' height=100%% valign='top'>\n") ;
     }

     // Function
     if (!$Error) {
        if (method_exists($Application,$funcname) ) {
           $Error = $Application->$funcname();
        } else {
           $Error = include sprintf ("./module/%s/%s.inc", $Navigation["Module"], $Navigation["Function"]) ;
        }
     }
   }

   if ($Error) {
      logPrintf (logERROR, $Error) ;
      printf ("<br><p><b>Error: %s</b></p>\n", $Error) ;
   }

   $smarty->assign(array(
      'Navigation'      => $Navigation,
      'NavigationParam' => $NavigationParam,
      'Error'           => $Error
   ));
   if ($Config['newLayout']) {
      $smarty->display('layout/footer_new.tpl');
   } else {
      $smarty->display('layout/footer.tpl');
   }
   $smarty->clearAllAssign();

   ob_end_flush();

   if ($Config['debugDump']) {
      logDump () ;
   }
?>
