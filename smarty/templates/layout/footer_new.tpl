{assign var=items_count value=$NavigationParam|@count}
{if $items_count > 0}
   {foreach $NavigationParam as $mark => $param}
      {if $param.Pasive == 'true'}
         <input type="hidden" name="navigationpassive[]" class="navigationpassive" value="{navigation_ids_by_mark mark=$mark}">
      {/if}
   {/foreach}
   {literal}
       <script type="text/javascript">
          $(document).ready(function() {
             menuRenderer.setEnable();
             $(".navigationpassive").each(function() {
                var ids = $(this).val().split(",");
                for (var j = 0 ; j < ids.length ; j++) {
                   menuRenderer.setDisable(ids[j]);
                }
             });
          });
       </script>
   {/literal}
 {/if}

{if !$Navigation["LayoutRaw"]}
   </div>


   {if !$Error}
      {if $Navigation['Focus'] != ''}
         {literal}
         <script type='text/javascript'>
            appFocus ('{/literal}{$Navigation['Focus']}{literal}') ;
         </script>
         {/literal}
      {/if}

      {if $Navigation['ProjectSelect'] == 0}
         <script type='text/javascript'>
            appRestore();
         </script>
      {/if}

   {/if}

   </body>
</html>
{/if}