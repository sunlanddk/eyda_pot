<div class="ui-widget">
   <div class="ui-state-error ui-corner-all messages">
   <p>
      <span class="ui-icon ui-icon-alert"></span>
      <strong>Error:</strong>
      <font>{$error}</font>
   </p>
   </div>
</div>