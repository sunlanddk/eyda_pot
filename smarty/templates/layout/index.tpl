 <!DOCTYPE html>
  <html>
      <head>
         <title>PassOnTex - {$Navigation["Header"]} [{$Config['Instance']}]</title>
         <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
         <link href='layout/default/styles.css?v=2' rel='stylesheet' type='text/css'>
         <link rel='stylesheet' type='text/css' href='layout/default/menu.css'>
         <link rel='stylesheet' type='text/css' href='layout/default/newMenu.css'>
         <link rel='stylesheet' type='text/css' href='layout/default/list.css'>
         <link rel="stylesheet" type="text/css" media="print" href="layout/default/print.css">
         <link rel="stylesheet" type="text/css" href="layout/jquery/css/ui-lightness/jquery-ui-1.10.2.min.css">


         <script type='text/javascript' src='js/jquery/jquery-1.9.1.min.js'></script>
         <script type='text/javascript' src='js/jquery/jquery-ui-1.10.2.min.js'></script>
         <script type='text/javascript' src='js/jquery/jquery.cookie.js'></script>
         <script type='text/javascript' src='js/validation.js'></script>
         <script type='text/javascript' src='lib/app.js'></script>
         <!--script type='text/javascript' src='lib/menu.js'></script-->
         <script type='text/javascript' src='lib/kmenu.js'></script>
         <script type='text/javascript' src='js/toolBar.js'></script>
         {literal}
            <script type='text/javascript'>
              var MainMenu =[{/literal}{$menu}{literal}];
              var ToolMenu =[{/literal}{$toolmenu}{literal}];


              $(document).ready(function() {
            	  kmDraw ('mainmenu', MainMenu, 'k');
              });
            </script>
         {/literal}
      </head>
   <body onload='appLoaded()'>
   <!--
      id: {$Id}, nid: {$nid}
      ApId: {$App->Id},
      NavId: {$Navigation['Id']}
   -->
   <table height="100%" width="100%">
      <tr>
         <td width="100%">
            {$htmlHeader}
         </td>
      </tr>
      <tr>
         <td>
            <div class=bar id=mainmenu>
            </div>
         </td>
      </tr>
      <tr>
         <td>
            <div class=bar>
               <table>
                  <tr>
                  {if !$Error}
                     {if $hasToolMenu}
                        <td style="height:29px;vertical-align:middle;">
                           <div id=toolmenu>
                           </div>
                        </td>
                        <script type='text/javascript'>
                           kmDraw ('toolmenu', ToolMenu, 'm');
                        </script>

                     {/if}
                  {/if}

                  {if ($Error or !$hasToolMenu)}
                     {if $Navigation["LayoutPopup"]}
                        <td class=barbutton>
                           <table onclick='appBack({$Navigation["Back"]})'>
                              <tr>
                                 <td class=barlabel>
                                    <img src='image/toolbar/close.gif'>
                                 </td>
                                 <td class=barlabel>Close</td>
                              </tr>
                           </table>
                        </td>\n", () ;
                     {/if}
                     <td class=barbutton>
                        <table onclick='history.back()'>
                           <tr>
                              <td class=barlabel><img src='image/toolbar/back.gif'></td>
                              <td class=barlabel>Back</td>
                           </tr>
                        </table>
                     </td>
                  {/if}

                 </tr>
              </table>
           </div>
           <div class="shadowline"></div>
        </td>
     </tr>
     <tr>
        <td height=100%>
           <table height=100% width=100%>
              <tr>
