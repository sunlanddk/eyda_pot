{if !$Navigation["LayoutRaw"]}
            </td>
         </tr>
      </table>

   {if $Navigation["LayoutMain"]}
            </td>
         </tr>
      </table>
   {/if}

   {if !$Error}
      {if $Navigation['Focus'] != ''}
         {literal}
         <script type='text/javascript'>
            appFocus ('{/literal}{$Navigation['Focus']}{literal}') ;
         </script>
         {/literal}
      {/if}

      {if $Navigation['ProjectSelect'] == 0}
         <script type='text/javascript'>
            appRestore();
         </script>
      {/if}

   {/if}

   </body>
</html>
{/if}