<br>
<table class="{$table_class}">
	<thead>
		<tr>
			<th>{$title}</th>
		</tr>
	</thead>
	<tbody>
		{foreach $error_messages as $i => $message}
			<tr>
				<td>{$message}</td>
			</tr>
		{/foreach}
	</tbody>
</table>