<div class="main">
	<form method="POST" name="appform">
		<input type="hidden" name="id" value="{$claim.Id}"/>
		<input type="hidden" name="nid" value="{$nid}"/>
		<table class="item">
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemlabel">Claim</td>
				<td class="itemfield">{$claim.Id}</td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemheader" style="width:80px">Field</td>
				<td class="itemheader">Content</td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemlabel">Handler</td>
				<td class="itemfield">
					<select name="HandlerId" style="width:250px; border:1px solid">
						{html_options options=$companies}
					</select>
				</td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemlabel">Pick Stock</td>
				<td class="itemfield">
					<select name="PickStockId" style="width:250px; border:1px solid">
						{html_options options=$pick_stocks}
					</select>
				</td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemlabel">Instructions</td>
				<td class="itemfield">
					<textarea style="width:90%;height:100px" name="Instructions"></textarea>
				</td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
		</table>
		<table class="list claimlist">
			<tr>
				<td class="listhead" style="width:80px"><p class="listhead">LOT</p></td>
				<td class="listhead" style="width:80px"><p class="listhead">Planned Date</p></td>
				<td class="listhead" style="width:80px"><p class="listhead">Generated until</p></td>
			</tr>
			{foreach from=$season_lots item=season_lot}
				<tr>
					<td class="list"><p class="list">{$season_lot.Name}</p></td>
					<td class="list"><p class="list">{$season_lot.LOTDate}</p></td>
					<td class="list"><p class="list">{$season_lot.GeneratedDate}</p></td>
				</tr>
			{/foreach}
		</table>
		<table class="item">
			<tr>
				<td class="itemspace"></td>
			</tr>
			<tr>
				<td class="itemspace"></td>
			</tr>
		</table>
	</form>
</div>