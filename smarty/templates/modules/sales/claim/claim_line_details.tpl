<table class="item">
	<tbody>
		<tr>
			<td class="itemheader" style="width:80px">Field</td>
			<td class="itemheader">Content</td>
		</tr>
		<tr>
			<td class="itemspace"></td>
		</tr>
		<tr>
			<td class="itemlabel">Claim</td>
			<td class="itemfield">
				{$claim_line.Claim}
				<img class="list" style="cursor:pointer" src="./image/toolbar/order.gif"
					 {navigation_on_click_link_by_mark mark="sales.claim.view" id={$claim_line.Claim}}>
				</img>
			</td>
		</tr>
		<tr>
			<td class="itemlabel">Case</td>
			<td class="itemfield">{$claim_line.Case}</td>
		</tr>
		<tr>
			<td class="itemlabel">Article</td>
			<td class="itemfield">{$claim_line.Article}</td>
		</tr>
		<tr>
			<td class="itemlabel">Colour</td>
			<td class="itemfield">{$claim_line.Color}</td>
		</tr>
		<tr>
			<td class="itemlabel">Description</td>
			<td class="itemfield">{$claim_line.Description}</td>
		</tr>
		<tr>
			<td class="itemspace"></td>
		</tr>
		<tr>
			<td class="itemlabel">Quantity</td>
			<td class="itemfield">
				<table>
					<tr>
						{for $i=1 to $claim_sizes|@count}
						<td style="width:60px">{$claim_sizes[$i-1].Name}</td>
						{/for}
					</tr>
					<tr>
						{for $i=1 to $claim_sizes|@count}
						<td style="width:60px">{floor($claim_sizes[$i-1].Quantity)}</td>
						{/for}
						<td>Pcs</td>
					</tr>
				</table>
			</td>
		</tr>
		<tr>
			<td class="itemspace"></td>
		</tr>
		<tr>
			<td class="itemlabel">Quantity</td>
			<td class="itemfield">{floor($claim_line.Quantity)} Pcs</td>
		</tr>
		<tr>
			<td class="itemlabel">Price Sale</td>
			<td class="itemfield">{number_format($claim_line.Price, 2, ',', ' ')}</td>
		</tr>
		<tr>
			<td class="itemspace"></td>
		</tr>
	</tbody>
</table>