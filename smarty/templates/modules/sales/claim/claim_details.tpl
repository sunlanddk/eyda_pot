<table class="item">
	<tbody>
		<tr>
			<td class="itemheader" style="width:80px">Field</td>
			<td class="itemheader">Content</td>
		</tr>
		<tr>
			<td class="itemspace"></td>
		</tr>
		<tr>
			<td class="itemlabel">Claim</td>
			<td class="itemfield">{$claim.Id}</td>
		</tr>
		<tr>
			<td class="itemlabel">Description</td>
			<td class="itemfield">{$claim.Description}</td>
		</tr>
		<tr>
			<td class="itemlabel">State</td>
			<td class="itemfield">{$claim.State}</td>
		</tr>
		<tr>
			<td class="itemlabel">Created</td>
			<td class="itemfield">{$claim.Created}, {$claim.CreateUser}</td>
		</tr>
		{if $claim.CreditNoteId > 0}
			<tr>
				<td class="itemlabel">Credit Note</td>
				<td class="itemfield">
					{$claim.CreditNoteId}
					<img class="list" style="cursor:pointer" src="./image/toolbar/invoice.gif"
					 {navigation_on_click_link_by_mark mark="invoiceview" id={$claim.CreditNoteId}}>
					</img>
				</td>
			</tr>
		{/if}
		{if $claim.PickOrderId > 0}
			<tr>
				<td class="itemlabel">Pick Order</td>
				<td class="itemfield">
					{$claim.PickOrderId}
					<img class="list" style="cursor:pointer" src="./image/toolbar/invoice.gif"
					 {navigation_on_click_link_by_mark mark="picklist" id={$claim.PickOrderId}}>
					</img>
				</td>
			</tr>
		{/if}
		<tr>
			<td class="itemspace"></td>
		</tr>
	</tbody>
</table>

<table class="item">
	<tbody>
		<tr>
			<td>
				<table class="item">
					<tr><td class="itemheader super">Sales</td></tr>
				</table>
				<table class="item">
					<tbody>
						<tr>
							<td class="itemheader" style="width:80px">Field</td>
							<td class="itemheader">Content</td>
						</tr>
						<tr>
							<td class="itemspace"></td>
						</tr>
						<tr>
							<td class="itemlabel">Created</td>
							<td class="itemfield">{$claim.Created}, {$claim.CreateUser}</td>
						</tr>
						<tr>
							<td class="itemspace"></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="border-left: 1px solid #cdcabb;">
				<table class="item">
					<tr><td class="itemheader super">Confirmation/Invoice</td></tr>
				</table>
				<table class="item">
					<tbody>
						<tr>
							<td class="itemheader" style="width:80px">Field</td>
							<td class="itemheader">Content</td>
						</tr>
						<tr>
							<td class="itemspace"></td>
						</tr>
						<tr>
							<td class="itemlabel">Created</td>
							<td class="itemfield">{$claim.Created}, {$claim.CreateUser}</td>
						</tr>
						<tr>
							<td class="itemspace"></td>
						</tr>
					</tbody>
				</table>
			</td>
			<td style="border-left: 1px solid #cdcabb;">
				<table class="item">
					<tr><td class="itemheader super">Delivery</td></tr>
				</table>
				<table class="item">
					<tbody>
						<tr>
							<td class="itemheader" style="width:80px">Field</td>
							<td class="itemheader">Content</td>
						</tr>
						<tr>
							<td class="itemspace"></td>
						</tr>
						<tr>
							<td class="itemlabel">Created</td>
							<td class="itemfield">{$claim.Created}, {$claim.CreateUser}</td>
						</tr>
						<tr>
							<td class="itemspace"></td>
						</tr>
					</tbody>
				</table>
			</td>
		</tr>
	</tbody>
</table>

<table class="list">
	<tbody>
		<tr>
			<td class="itemheader super">Lines</td>
		</tr>
	</tbody>
</table>

<table class="list claimlist">
	{assign var=total_quantity value=0}
	{assign var=total_price value=0}
	<tbody>
		<tr>
			<td class="listhead" style="width:50px"></td>
			<td class="listhead" style="width:30px">Pos</td>
			<td class="listhead" style="width:70px">Case</td>
			<td class="listhead" style="width:100px">Article</td>
			<td class="listhead" style="width:100px">Colour</td>
			<td class="listhead" style="width:100%">Description</td>
			<td class="listhead" style="width:100px; text-align: right;">Quantity</td>
			<td class="listhead" style="width:100px; text-align: right;">Price</td>
			<td class="listhead" style="width:100px; text-align: right;">Total</td>
		</tr>

		{foreach $claim_lines as $i => $claim_line}
		<tr>

			{assign var="url" value="{$imageStore}/thumbnails/{$claim_line.Article}_{$claim_line.Colour}.jpg"}
			{if !file_exists($url)}
				{assign var="url" value="image/no_image.png"}
	      	{/if}
			<td><a {navigation_on_click_link_by_mark mark='sales.claim.line.view' id={$claim_line.Id}}>
				<img src="{$url}" style="width:50px; height:50px"></img>
			</a></td>

			<td>{$i}</td>
			<td>{$claim_line.CaseId}</td>
			<td>{$claim_line.Article}</td>
			<td>{$claim_line.Colour}</td>
			<td>{$claim_line.Description}</td>
			<td style="text-align: right;">{floor($claim_line.Quantity)} Pcs</td>
			<td style="text-align: right;">{number_format($claim_line.Price, 2, ',', ' ')} {$claim.Currency}</td>
			<td style="text-align: right;">{number_format($claim_line.Quantity * $claim_line.Price, 2, ',', ' ')} {$claim.Currency}</td>

			{assign var=total_quantity value=$total_quantity + $claim_line.Quantity}
			{assign var=total_price value=$total_price + $claim_line.Quantity*$claim_line.Price}
		</tr>
		{/foreach}

		<tr>
			<td class="list" colspan="6"><p class="list"></p></td>
			<td class="list" style="text-align: right;">{floor($total_quantity)} Pcs</td>
			<td class="list"><p class="list"></p></td>
			<td class="list" style="text-align: right;">{number_format($total_price, 2, ',', ' ')} {$claim.Currency}</td>
		</tr>
	</tbody>
</table>