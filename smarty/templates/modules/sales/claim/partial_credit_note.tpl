<div class="main">
	<form method="Post" name="appform">
		<input type="hidden" name="nid" value="{$nid}">
		<input type="hidden" name="id" value="{$id}">
		<table class="list listStyling">
			<tr>
				<th>Pos</th><th>Article</th><th>Description</th><th>Color</th><th>Name Color</th>
				<th>Size</th><th>Credit Qty</th><th>InvoiceQty</th><th>Unit</th>
			</tr>
			{foreach from=$claim_lines key=key item=claim_line}
				{assign var="claim_sizes" value=$claim_line.Sizes}
				<tr>
					<td>{$key + 1}</td><td>{$claim_line.Article}</td><td>{$claim_line.Description}</td><td>{$claim_line.Color}</td>
					<td>{$claim_line['Color Name']}</td><td></td><td></td><td></td><td>Pcs</td>
				</tr>

				{foreach from=$claim_sizes item=claim_size}
					<tr>
						<td colspan="5"></td>
						<td>{$claim_size.Size}</td>
						<td>
							{if $claim_size.Quantity > 0}
							<input type="text" style="width:70%" name="SizeQuantity[{$claim_size.Id}]" value="0" />
							{/if}
						</td>
						<td>{round($claim_size.Quantity)}</td>
						<td></td>
					</tr>
				{/foreach}
				
			{/foreach}
		</table>
	</form>
</div>