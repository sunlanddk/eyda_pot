{assign var=count value=$sizes|@count}
{literal}
<script type='text/javascript' src='js/style.js'></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#addtocartbtn').attr("disabled", "disabled");

		$('.user-qnty').each(function() {
			maskElement(this, isInteger);
		});

		$('.user-qnty').focus(function() {
			var val =  parseInt($(this).attr('act'));
			if(val==0){
				$(this).removeClass('inactive');
				$(this).select();
			}
		});

		$('.user-qnty').blur(function() {
			$(this).addClass('inactive');
		});

		$('.user-qnty').change(function() {
			userQuantityChanged($(this));
		});

		$('.size-incr-btn').button({
           icons: {
            primary: "ui-icon-plus"
           },
		   text : false
		}).click(function( event ) {
			var sizeid = '#sizeid_' + $(this).attr('sizeid');
			var _assort =  parseInt($(this).attr('act'));
			var tqnty = 0;
			var lotid = $(this).attr('stylelotid');
			$('.user-qnty').each(function (i) {
				if(lotid==$(this).attr('lotid')) {
					tqnty += parseInt(checkIsNumberOrReturnZero($(this).val()));
				}
			});
			var val =  parseInt(checkIsNumberOrReturnZero($(sizeid).val()));
			var mval =  parseInt($(sizeid).attr('maxvalue'));
			if(val<mval || mval<0){
				if(tqnty>0 || _assort==0) {
					$(sizeid).val(val+1);
					userQuantityChanged(null);
				}
			}
		});

		$('.incr-assort-btn').button().click(function( event ) {
			var lineassortid = $(this).attr('assortmentid');
			var linelotid = $(this).attr('lotid');
			$('.assort-qnty').each(function (i) {
				var sizeid = '#sizeid_' + linelotid + '_' + $(this).attr('assortid');
				
                if (lineassortid == $(this).attr('assortmentid')) {
					var val =  parseInt(checkIsNumberOrReturnZero($(sizeid).val()));
					var mval =  parseInt($(sizeid).attr('maxvalue'));
					var ival =  parseInt($(this).attr('value'));
					if(val<mval || mval<0){
						$(sizeid).val(val+ival);
						userQuantityChanged(null);
					}
				}
		    });
			userQuantityChanged(null);
		});

		$('.clear-qnty-btn').button().click(function( event ) {
			var lotid = $(this).attr('lotid');
			$('.user-qnty').each(function (i) {
				if(lotid==$(this).attr('lotid')) {
					$(this).val(0);
				}
		    });
			userQuantityChanged(null);
		});

		$('.incr-qnty-btn').button().click(function( event ) {
			var lotid = $(this).attr('lotid');
			$('.user-qnty').each(function (i) {
				if(lotid==$(this).attr('lotid')) {
					var val =  parseInt(checkIsNumberOrReturnZero($(this).val()));
					var mval =  parseInt($(this).attr('maxvalue'));
					if(val<mval || mval<0){
						$(this).val(val+1);
					}
				}
		    });
			userQuantityChanged(null);
		});

		$('#styleColorsId').change(function() {
			var currColId = $('#articleColorId').val();
			var newColId  = $(this).val();
			if(newColId!='' && newColId!=currColId){
				var articleId = $('#articleId').val();
				var seasonId = $('#seasonId').val();
				var nid = $('#nid').val();
				var vnid = $('#vnid').val();
				showArticle(articleId, newColId, seasonId, nid, vnid);
			}
		});

		$('#addtocartbtn').click(function( event ) {
			var arts =  {};
			$('.style-size-totalqnty').each(function (i) {
				//alert($(this).attr('lotid')) ;
				var _totalqnty = parseInt(checkIsNumberOrReturnZero($(this).val()));
				if (_totalqnty>0) {
					lotid = $(this).attr('lotid');
					arts[lotid] = {} ;
					arts[lotid]["ArticleId"] 		  = $('#articleId').val();
					arts[lotid]["ArticleColorId"] 	  = $('#articleColorId').val();
					arts[lotid]["ArticleTypeId"] 	  = $('#articleTypeId').val();
					arts[lotid]["Price"] 			  = $('#articlePrice').val();
					arts[lotid]["Desc"] 			  = $('#articleDesc').val();
					arts[lotid]["Number"] 			  = $('#articleNumber').val();
					arts[lotid]["SeasonId"] 		  = $('#seasonId').val();
					arts[lotid]["SeasonName"] 		  = $('#seasonName').val();
					arts[lotid]["ColorNumber"] 		  = $('#colorNumber').val();
					arts[lotid]["ColorDesc"] 		  = $('#colorDesc').val();
					arts[lotid]["TopBottom"] 		  = $('#topBottom').val();
					arts[lotid]["CurrencyId"] 	      = $('#currencyId').val();
					arts[lotid]["CategoryGroupId"] 	  = $('#categoryGroupId').val();
					arts[lotid]["CollectionMemberId"] = $('#collectionMemberId').val();
					arts[lotid]["LOTName"] 	          = $(this).attr('lotname'); //$('#lotName').val();
					arts[lotid]["LotDate"] 	          = $(this).attr('lotdate'); //$('#lotDate').val();
					arts[lotid]["LOTId"] 			  = $(this).attr('lotid');
					arts[lotid]["Sizes"] 			  = {} ;

					$('.user-qnty').each(function (i) {
						if(lotid==$(this).attr('lotid')) {
							var sizeqnty = parseInt(checkIsNumberOrReturnZero($(this).val()));
							if(sizeqnty > 0){
								var sizeid 		= $(this).attr('sizeid');
								var sizename 	= $(this).attr('sizename');
								arts[lotid]["Sizes"][sizeid] 	= {qty:sizeqnty, name: sizename};
							}
						}
					});
				}
			});

			jQuery.ui.Mask.show("Adding to cart ...");
			$("#zoom_02").hide() ;
         	var post = $.post(
	             baseUrl + "index.php",
	             {
	                nid            : $(this).attr("nid"),
	                articleInfo    : $.toJSON(arts)
	             },
	             function(data, textStatus, jqXHR) {
	                if ($("#reload").val() === 'true') {
	                   var href = location.href ;
	                   location.replace(href);
	                } else {
                 	    var id = '#' + $('#articleId').val() + '_' + $('#articleColorId').val() + '_' + $('#seasonId').val();
           	        	$(id).addClass('in-cart');
           	            $(".zoomContainer").hide().html("");
           	            $(".item-panel").hide().html("");
    	                getCartInfo();
	                }
	             },
	             'json'
	        );

		});

		userQuantityChanged(null);
	});

	function userQuantityChanged(el){
		if(el){
			checkMaxvalue(el);
		}
		var tqnty = 0;
		var enableadd = 0;

		$('.style-size-totalqnty').each(function (i) {
			tqnty = 0;
			var lotid = $(this).attr('lotid');
			$('.user-qnty').each(function (i) {
				if(lotid==$(this).attr('lotid')) {
					tqnty += parseInt(checkIsNumberOrReturnZero($(this).val()));
				}
			});
			$(this).val(tqnty);
			var trice = parseFloat($('#articlePrice').val()) * tqnty;
			var totalprice_lotid = '#totalprice_' + lotid ;

			$(totalprice_lotid).val(formatPrice(trice, 2, ",", " "));
			if(tqnty>0){
				enableadd = 1;
			} 
	    });
		if(enableadd>0){
			$('#addtocartbtn').removeAttr("disabled", "disabled");
		} else {
			$('#addtocartbtn').attr("disabled", "disabled");
		}
	}

	function checkMaxvalue(el){
		var val = parseInt(checkIsNumberOrReturnZero($(el).val()));
		var maxval = parseInt($(el).attr('maxvalue'));
		if(val > maxval && maxval>=0){
			$(el).val(maxval);
		}
	}

	function formatPrice(number, decimals, dec_sep, thousands_sep) {
		dec_sep = typeof dec_sep !== 'undefined' ? dec_sep : ',';
		thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : ' ';
		var parts = number.toFixed(decimals).toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);
		return parts[0] + dec_sep + parts[1];
	}
	
	function checkIsNumberOrReturnZero(val){
		return (isNaN(val) || val=="") ? "0" : val;
	}
	function backtolist() {
                 	    var id = '#' + $('#articleId').val() + '_' + $('#articleColorId').val() + '_' + $('#seasonId').val();
//           	        	$(id).addClass('in-cart');
						$("#zoom_02").elevateZoom({zoomEnabled:false,
													zoomWindowWidth: 0,
													zoomWindowHeight: 0,
													zoomWindowOffetx: 0,
													zoomWindowOffety: 0,
													zoomWindowPosition: 1,
													constrainType: 'height',  //width or height
													constrainSize: 1, 
													});
           	            $(".zoomContainer").hide().html("");
           	            $(".item-panel").hide().html("");
    	                getCartInfo();
	}
	
</script>
<script src="js/zoom/jquery.elevatezoom.js" type="text/javascript"></script> 

<script type="text/javascript">
$("#zoom_02").elevateZoom({tint:true, tintColour:'#F000000', tintOpacity:0.0});
</script>
{/literal}

<div class="style-view">
	<input type="hidden" id="nid" name="nid" value="{$nid}">
	<input type="hidden" id="tablenid" name="tablenid" value="{$sizesnid}">
	<input type="hidden" id="articleId" name="articleId" value="{$style.articleId}">
	<input type="hidden" id="articleNumber" name="articleNumber" value="{$style.Number}">
	<input type="hidden" id="articleColorId" name="articleColorId" value="{$style.colorId}">
    <input type="hidden" id="articleTypeId" name="articleTypeId" value="{$style.ArticleTypeId}">
	<input type="hidden" id="articlePrice" name="articlePrice" value="{$style.WholesalePrice}">
	<input type="hidden" id="articleDesc" name="articleDesc" value="{$style.name}">
	<input type="hidden" id="colorNumber" name="colorNumber" value="{$style.colorNumber}">
	<input type="hidden" id="colorDesc" name="colorDesc" value="{$style.color}">
	<input type="hidden" id="seasonId" name="seasonId" value="{$style.seasonId}">
	<input type="hidden" id="seasonName" name="seasonName" value="{$style.seasonName}">
	<input type="hidden" id="topBottom" name="topBottom" value="{$style.TopBottom}">
	<input type="hidden" id="currencyId" name="currencyId" value="{$currency.id}">
	<input type="hidden" id="categoryGroupId" name="categoryGroupId" value="{$style.categoryGroupId}">
    <input type="hidden" id="collectionMemberId" name="collectionMemberId" value="{$style.collectionMemberId}">
    <input type="hidden" id="lotName" name="lotName" value="{$style.lotName}">
    <input type="hidden" id="lotId" name="lotId" value="{$style.lotId}">
    <input type="hidden" id="lotDate" name="lotDate" value="{$style.lotDate}">

	<input type="hidden" id="nid" name="nid" value="{$nid}">
	<input type="hidden" id="vnid" name="vnid" value="{$vnid}">
	<input type="hidden" id="reload" name="reload" value="{$reload}">
	<table>
	<colgroup>
		<col style="width: 310px">
		<col style="width: 100%">
	</colgroup>
	<tr>
	<td>
		<div class="style-img">
			{assign var="url" value="{$imageStore}/thumbnails/{$style.Number}_{$style.colorNumber}.jpg"}
			{assign var="url2" value="{$imageStore}/thumbnails/{$style.Number}_{$style.colorNumber}_upl.jpg"}
			{if !file_exists($url)}
				{assign var="url" value="image/no_image.png"}
			{/if}
			{if !file_exists($url2)}
				{assign var="url2" value="image/no_image.png"}
			{/if}
			<img id="zoom_02" class="image" alt="" src="{$url}" data-zoom-image="{$url2}">
		</div>
		<div class="style-thumbnails">
		</div>
	</td>
	<td>

	<div class="style-data">
		<h1 class="name">{$style.name} ({$style.Number} {$style.colorNumber})</h1>
		<div class="color">
			<span>Color:</span> {$style.color}
		</div>
		<ul class="params">
			{if isset($style.sex)}
			<li>
			<div>Sex:</div>
			{$style.sex}
			</li>
			{/if}

			{if isset($style.brand)}
			<li>
			<div>Brand:</div>
			{$style.brand}
			</li>
			{/if}

			{if isset($style.seasonName)}
			<li>
			<div>Season:</div>
			{$style.seasonName}
			</li>
			{/if}

			<li>
			<div>Colors:</div>
				<select id="styleColorsId" name="style-colors" class="style-colors">
					{html_options options=$colors  selected=$style.colorId}
				</select>
			</li>
			{if $assortments eq true}
			<li>
			<div></div>
			{$assorttxt}
			</li>
			{/if}

		</ul>


			<div id="styleSizes" class="style-sizes" style="width: 100%;">
				<table class="sizes">
					<colgroup>
						<col style="width: 50px">

						{for $ii=1 to $count}
						<col style="width: 40px">
						{/for}
						<col style="width: 45px">
						<col style="width: 45px">
						<col style="width: 25px">
					 </colgroup>
					<tr>
						<th class="leftTh">&nbsp;</th>
						{foreach $sizes as $i => $size}
							<th>
								{$size.Value}
								<center><table class="noborder"><tr><td>
								Max
								</td><td>
								Order
								</td><td>
								Add
								</td></tr></table></center>
								
							</th>
						{/foreach}
						<th>Total Qty</th>
						<th>Total Price {$currency.symbol}</th>
						<th></th>
					</tr>
{foreach $lots as $j => $lot}
					<tr>
						<td class="leftTh">{$lot.lotName}</td>
						{assign var=tavl value=0}
						{foreach $lot.sizes as $i => $size}
						<td>
							<center><table class="noborder"><tr><td>
							{if $lot.unlimited ne true}
								{$size.available}
								{assign var=tavl value=$tavl+$size.available}
							{else}
								No limit
							{/if}
							</td><td>
							{if $size.available gt 0 or $size.available lt 0}
								<input id="sizeid_{$j}_{$size.Id}" type="text" lotid="{$j}" sizeid="{$size.Id}" sizename="{$size.Value}" maxvalue="{$size.available}" value="{$size.qnty}"  act="{$assortments}" class="user-qnty style-size-input inactive">
							{else}
								0
							{/if}
							</td><td>
							{if $size.available gt 0 or $size.available lt 0}
								<button tabindex="-1" sizeid="{$j}_{$size.Id}" act="{$assortments}" lotid="{$j}" stylelotid="{$style.lotId}" class="size-incr-btn button-small">&nbsp;</button>
							{else}
								-
							{/if}
							</td></tr></table></center>
						</td>
						{/foreach}
						<td>
							<center><table class="noborder"><tr><td>
							{if $lot.unlimited ne true}
								{$tavl}
							{else}
								No limit
							{/if}
							</td><td>
								<input id="totalqnty_{$j}" name="totalqnty" lotid="{$j}" stylelotid="{$style.lotId}" lotname="{$lot.lotName}" lotdate="{$lot.lotDate}" type="text" readonly="readonly" tabindex="-1" value="" class="style-size-totalqnty inactive">
							</td><td>
							</td></tr></table></center>
						</td>
						<td class="total">
							<center><table class="noborder"><tr><td>
							{if $lot.unlimited ne true}
								<input type="text" readonly="readonly" tabindex="-1" class="style-size-input inactive total" value="{number_format($tavl*$style.WholesalePrice, 2, ',', ' ')}">
							{else}
								++
							{/if}
							</td><td>
								<input id="totalprice_{$j}" name="totalprice" lotid="{$j}" type="text" readonly="readonly" tabindex="-1" value="" class="style-size-total inactive total">
							</td><td>
							</td></tr></table></center>
						</td>
						<td>
								{if $assortments eq false}
									<input id="addidbtn" type="button" lotid="{$j}" value="+ all" class="incr-qnty-btn flatButton">
								{/if}
								<input id="clearidbtn" type="button" lotid="{$j}" value="Clear" class="clear-qnty-btn flatButton">
						</td>
					</tr>
{/foreach}
					{if $assortments eq true}
					{assign var=letter value='Q'}
					{foreach $assortsets as $i => $assortset}
					{if $letter eq 'D'} {assign var=letter value='E'} {/if}
					{if $letter eq 'C'} {assign var=letter value='D'} {/if}
					{if $letter eq 'B'} {assign var=letter value='C'} {/if}
					{if $letter eq 'A'} {assign var=letter value='B'} {/if}
					{if $letter eq 'Q'} {assign var=letter value='A'} {/if}
					<tr>
						<td class="leftTh">Assortment {$letter}</td>
						{assign var=tavla value=0}
						{foreach $assortset as $j => $assort}
						<td>
							<input id="assortid_{$assort.Id}" lotid="{$style.lotId}" type="text" assortid="{$assort.Id}" assortmentid="{$assort.AssortmentId}" assortname="{$assort.Name}" maxvalue="{$size.available}" value="{$assort.quantity}" class="assort-qnty style-size-input inactive">
							{assign var=tavla value=$tavla+$assort.quantity}
						</td>
						{/foreach}
						<td>{$tavla}</td>
						<td>
							<input id="addassbtn" assortmentid="{$i}" lotid="{$style.lotId}" type="button" value="Add" class="incr-assort-btn flatButton">
						</td>
					</tr>
					{/foreach}
					{/if}

				</table>

			</div>

			{if isset($style.WholesalePrice) && $style.WholesalePrice>=0 && $currency.id>0}
				<div class="price">
					<table width="100%"><tr>
					<td align="left">
						<div class="price">{number_format($style.WholesalePrice, 2, ',', ' ')} {$currency.symbol}</div>
						{if isset($style.RetailPrice) && $style.RetailPrice>=0}
							<div class="price">RRP: {number_format($style.RetailPrice, 2, ',', ' ')} {$currency.symbol}</div>
						{/if}

					</td>
					<td align="right">
					{if isset($claim)}
						<input type="button" value="Add To Claim" id="addtocartbtn" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.claimcart.add'}">
					{else}
						<input type="button" value="Add To Cart" id="addtocartbtn" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.ordercart.add'}">
					{/if}
					</td>
					</tr></table>
				</div>
			{else}
				<div class="price">
					<div class="price">Cannot retrieve price for this style in {$currency.symbol}.</div>
				</div>
			{/if}

	</div>

	</td>
	</tr>
	</table>





</div>

