{assign var=count value=$sizes|@count}
{literal}
<script type='text/javascript' src='js/style.js'></script>
<script type="text/javascript">
	$(document).ready(function() {
		$('#addtocartbtn').attr("disabled", "disabled");

		$('.user-qnty').each(function() {
			maskElement(this, isInteger);
		});

		$('.user-qnty').focus(function() {
			$(this).removeClass('inactive');
			$(this).select();
		});

		$('.user-qnty').blur(function() {
			$(this).addClass('inactive');
		});

		$('.user-qnty').change(function() {
			userQuantityChanged($(this));
		});

		$('.size-incr-btn').button({
           icons: {
            primary: "ui-icon-plus"
           },
		   text : false
		}).click(function( event ) {
			var sizeid = '#sizeid_' + $(this).attr('sizeid');

			var val =  parseInt(checkIsNumberOrReturnZero($(sizeid).val()));
			var mval =  parseInt($(sizeid).attr('maxvalue'));
			if(val<mval || mval<0){
				$(sizeid).val(val+1);
				userQuantityChanged(null);
			}
		});

		$('#clearidbtn').button().click(function( event ) {
			$('.user-qnty').val(0);
			userQuantityChanged(null);
		});

		$('#addidbtn').button().click(function( event ) {
			$('.user-qnty').each(function (i) {
				var val =  parseInt(checkIsNumberOrReturnZero($(this).val()));
				var mval =  parseInt($(this).attr('maxvalue'));
				if(val<mval || mval<0){
					$(this).val(val+1);
				}
		    });
			userQuantityChanged(null);
		});

		$('#styleColorsId').change(function() {
			var currColId = $('#articleColorId').val();
			var newColId  = $(this).val();
			if(newColId!='' && newColId!=currColId){
				var articleId = $('#articleId').val();
				var seasonId = $('#seasonId').val();
				var nid = $('#nid').val();
				var vnid = $('#vnid').val();
				showArticle(articleId, newColId, seasonId, nid, vnid);
			}
		});

		$('#addtocartbtn').click(function( event ) {

			var art =  {};
			art["ArticleId"] 		  = $('#articleId').val();
			art["ArticleColorId"] 	  = $('#articleColorId').val();
			art["ArticleTypeId"] 	  = $('#articleTypeId').val();
			art["Price"] 			  = $('#articlePrice').val();
			art["Desc"] 			  = $('#articleDesc').val();
			art["Number"] 			  = $('#articleNumber').val();
			art["SeasonId"] 		  = $('#seasonId').val();
			art["SeasonName"] 		  = $('#seasonName').val();
			art["ColorNumber"] 		  = $('#colorNumber').val();
			art["ColorDesc"] 		  = $('#colorDesc').val();
			art["TopBottom"] 		  = $('#topBottom').val();
			art["CurrencyId"] 	      = $('#currencyId').val();
			art["CategoryGroupId"] 	  = $('#categoryGroupId').val();
			art["CollectionMemberId"] = $('#collectionMemberId').val();
			art["LOTId"] 	          = $('#lotId').val();
			art["LOTName"] 	          = $('#lotName').val();
			art["LotDate"] 	          = $('#lotDate').val();

			var sizes =  {};
			$('.user-qnty').each(function (i) {
				var sizeqnty = parseInt(checkIsNumberOrReturnZero($(this).val()));
				if(sizeqnty > 0){
					var sizeid = $(this).attr('sizeid');
					var sizename = $(this).attr('sizename');
					var sizeprice = $(this).attr('sizeprice');
					sizes[sizeid] = {qty:sizeqnty, name: sizename, sizeprice: sizeprice};
				}
		    });

			art["Sizes"] 		= sizes;

			jQuery.ui.Mask.show("Adding to cart1 ..");
          	var post = $.post(
	             baseUrl + "index.php",
	             {
	                nid            : $(this).attr("nid"),
/*!	                articleInfo    : JSON.stringify(art)
	                articleInfo    : "gryf" 
*/
	                articleInfo    : $.toJSON(art) 
	             },
	             function(data, textStatus, jqXHR) {
	                if ($("#reload").val() === 'true') {
	                   var href = location.href ;
	                   location.replace(href);
	                } else {
                 	    var id = '#' + $('#articleId').val() + '_' + $('#articleColorId').val() + '_' + $('#seasonId').val();
           	        	$(id).addClass('in-cart');
           	            $(".zoomContainer").hide().html("");
           	            $(".item-panel").hide().html("");
    	                getCartInfo();
	                }
	             },
	             'json'
	        );

			jQuery.ui.Mask.show("Adding to cart2 ..");
someText = $.toJSON(art).replace(/(\r\n|\n|\r)/gm,"");
			//alert(someText) ;

		});

		userQuantityChanged(null);
	});

	function userQuantityChanged(el){
		if(el){
			checkMaxvalue(el);
		}
		var tqnty = 0;
		var tprice = 0;
		$('.user-qnty').each(function (i) {
			tqnty += parseInt(checkIsNumberOrReturnZero($(this).val()));
			var _price =  parseFloat($(this).attr('sizeprice'));
			tprice += parseFloat(checkIsNumberOrReturnZero($(this).val())) * _price;
	    });
	    $('#totalqnty').val(tqnty);
	    var trice = parseFloat($('#articlePrice').val()) * tqnty;
	    $('#totalprice').val(formatPrice(tprice, 2, ",", " "));
	    if(tqnty>0){
	    	$('#addtocartbtn').removeAttr("disabled", "disabled");
	    } else {
	    	$('#addtocartbtn').attr("disabled", "disabled");
		}
	}

	function checkMaxvalue(el){
		var val = parseInt(checkIsNumberOrReturnZero($(el).val()));
		var maxval = parseInt($(el).attr('maxvalue'));
		if(val > maxval && maxval>=0){
			$(el).val(maxval);
		}
	}

	function formatPrice(number, decimals, dec_sep, thousands_sep) {
		dec_sep = typeof dec_sep !== 'undefined' ? dec_sep : ',';
		thousands_sep = typeof thousands_sep !== 'undefined' ? thousands_sep : ' ';
		var parts = number.toFixed(decimals).toString().split(".");
		parts[0] = parts[0].replace(/\B(?=(\d{3})+(?!\d))/g, thousands_sep);
		return parts[0] + dec_sep + parts[1];
	}
	
	function checkIsNumberOrReturnZero(val){
		return (isNaN(val) || val=="") ? "0" : val;
	}
	function test() {
                 	    var id = '#' + $('#articleId').val() + '_' + $('#articleColorId').val() + '_' + $('#seasonId').val();
//           	        	$(id).addClass('in-cart');
						$("#zoom_02").elevateZoom({zoomEnabled:false,
													zoomWindowWidth: 0,
													zoomWindowHeight: 0,
													zoomWindowOffetx: 0,
													zoomWindowOffety: 0,
													zoomWindowPosition: 1,
													constrainType: 'height',  //width or height
													constrainSize: 1, 
													});
           	            $(".zoomContainer").hide().html("");
           	            $(".item-panel").hide().html("");
    	                getCartInfo();
	}
</script>
<script src="js/zoom/jquery.elevatezoom.js" type="text/javascript"></script> 

<script type="text/javascript">
$("#zoom_02").elevateZoom({tint:true, tintColour:'#F90', tintOpacity:0.0, zoomEnabled:true});

</script>
{/literal}

<div class="style-view">
	<input type="hidden" id="nid" name="nid" value="{$nid}">
	<input type="hidden" id="tablenid" name="tablenid" value="{$sizesnid}">
	<input type="hidden" id="articleId" name="articleId" value="{$style.articleId}">
	<input type="hidden" id="articleNumber" name="articleNumber" value="{$style.Number}">
	<input type="hidden" id="articleColorId" name="articleColorId" value="{$style.colorId}">
    <input type="hidden" id="articleTypeId" name="articleTypeId" value="{$style.ArticleTypeId}">
	<input type="hidden" id="articlePrice" name="articlePrice" value="{$style.WholesalePrice}">
	<input type="hidden" id="articleDesc" name="articleDesc" value="{$style.name}">
	<input type="hidden" id="colorNumber" name="colorNumber" value="{$style.colorNumber}">
	<input type="hidden" id="colorDesc" name="colorDesc" value="{$style.color}">
	<input type="hidden" id="seasonId" name="seasonId" value="{$style.seasonId}">
	<input type="hidden" id="seasonName" name="seasonName" value="{$style.seasonName}">
	<input type="hidden" id="topBottom" name="topBottom" value="{$style.TopBottom}">
	<input type="hidden" id="currencyId" name="currencyId" value="{$currency.id}">
	<input type="hidden" id="categoryGroupId" name="categoryGroupId" value="{$style.categoryGroupId}">
    <input type="hidden" id="collectionMemberId" name="collectionMemberId" value="{$style.collectionMemberId}">
    <input type="hidden" id="lotName" name="lotName" value="{$style.lotName}">
    <input type="hidden" id="lotId" name="lotId" value="{$style.lotId}">
    <input type="hidden" id="lotDate" name="lotDate" value="{$style.lotDate}">

	<input type="hidden" id="nid" name="nid" value="{$nid}">
	<input type="hidden" id="vnid" name="vnid" value="{$vnid}">
	<input type="hidden" id="reload" name="reload" value="{$reload}">
	<table>
	<colgroup>
		<col style="width: 310px">
		<col style="width: 100%">
	</colgroup>
	<tr>
	<td>
		<div class="style-img">
			{assign var="url" value="{$imageStore}/thumbnails/{$style.Number}_{$style.colorNumber}.jpg"}
			{assign var="url2" value="{$imageStore}/thumbnails/{$style.Number}_{$style.colorNumber}_upl.jpg"}
			{if !file_exists($url)}
				{assign var="url" value="image/no_image.png"}
			{/if}
			<img id="zoom_02" class="image" alt="" src="{$url}" data-zoom-image="{$url2}">
		</div>
		<div class="style-thumbnails">
		</div>
	</td>
	<td>

	<div class="style-data">
		<h1 class="name">{$style.name} ({$style.Number} {$style.colorNumber})</h1>
		<div class="color">
			<span>Color:</span> {$style.color}
		</div>
		<ul class="params">
			{if isset($style.sex)}
			<li>
			<div>Sex:</div>
			{$style.sex}
			</li>
			{/if}

			{if isset($style.brand)}
			<li>
			<div>Brand:</div>
			{$style.brand}
			</li>
			{/if}

			{if isset($style.seasonName)}
			<li>
			<div>Season:</div>
			{$style.seasonName}
			</li>
			{/if}

			<li>
			<table>
			<tr>
			<td>
			<div>Select Color:</div>
				<select id="styleColorsId" name="style-colors" class="style-colors">
					{html_options options=$colors  selected=$style.colorId}
				</select>
			</td>
			<td>
			... or order for all colors:
			</td>
			<td class=list onclick="{$matrixedit}" style='cursor:pointer;'><img class=list src="./image/toolbar/pen.gif"></td>	
			</tr>
			</table>
			</li>

		</ul>


			<div id="styleSizes" class="style-sizes" style="width: 100%;">
				<table class="sizes">
					<colgroup>
						<col style="width: 50px">

						{for $ii=1 to $count}
						<col style="width: 40px">
						{/for}
						<col style="width: 45px">
						<col style="width: 45px">
					 </colgroup>
					<tr>
						<th class="leftTh">&nbsp;</th>
						{foreach $sizes as $i => $size}
						<th>{$size.Value}</th>
						{/foreach}
						<th>Total Qty</th>
						<th class="total">Total Price {$currency.symbol}</th>
					</tr>
					<tr>
						<td class="leftTh">Available</td>
						{assign var=tavl value=0}
						{foreach $sizes as $i => $size}
						<td>
						{if $unlimited ne true}
							{$size.available}
							{assign var=tavl value=$tavl+$size.available}
						{else}
							++
						{/if}
						</td>
						{/foreach}
						<td>{$tavl}</td>
						<td class="total">
							
						</td>
					</tr>
					<tr>
						<td class="leftTh">Quantity</td>
						{foreach $sizes as $i => $size}
						<td>
							{if $size.available gt 0 or $size.available lt 0}
								<input id="sizeid_{$size.Id}" type="text" sizeid="{$size.Id}" sizename="{$size.Value}" sizeprice="{$size.WholeSalePrice}" maxvalue="{$size.available}" value="{$size.qnty}" class="user-qnty style-size-input inactive">
							{else}
								0
							{/if}
						</td>
						{/foreach}
						<td>
							<input id="totalqnty" name="totalqnty" type="text" readonly="readonly" tabindex="-1" value="" class="style-size-input inactive">
						</td>
						<td class="total">
							<input id="totalprice" name="totalprice" type="text" readonly="readonly" tabindex="-1" value="" class="style-size-input inactive total">
						</td>
					</tr>
					<tr>
						<td class="leftTh">Price</td>
						{foreach $sizes as $i => $size}
						<td>
							{if $size.WholeSalePrice gt 0 or $size.WholeSalePrice lt 0}
								{$size.WholeSalePrice}
							{else}
								0
							{/if}
						</td>
						{/foreach}
						<td>
						</td>
						<td class="total">
						</td>
					</tr>
					<tr>
						<td class="leftTh"></td>
						{foreach $sizes as $i => $size}
						<td>
							{if $size.available gt 0 or $size.available lt 0}
								<button sizeid="{$size.Id}" class="size-incr-btn button-small">&nbsp;</button>
							{else}
								0
							{/if}
						</td>
						{/foreach}
						<td>
							<input id="clearidbtn" type="button" value="Clear" class="clear-qnty-btn flatButton">
						</td>
						<td>
							<input id="addidbtn" type="button" value="Add" class="incr-qnty-btn flatButton">
						</td>
					</tr>
				</table>

			</div>
			{if isset($style.MinMax.MaxWholeSalePrice) && $style.MinMax.MaxWholeSalePrice>0}
				<div class="price">
					<table width="100%"><tr>
					<td align="left">
					{if $style.MinMax.MaxWholeSalePrice==$style.MinMax.MinWholeSalePrice}
						<div class="price">{number_format($style.MinMax.MinWholeSalePrice, 2, ',', ' ')} {$currency.symbol}</div>
					{else}
						<div class="price">{number_format($style.MinMax.MinWholeSalePrice, 2, ',', ' ')} - {number_format($style.MinMax.MaxWholeSalePrice, 2, ',', ' ')} {$currency.symbol}</div>
					{/if}
					</td>
					<td align="right">
					{if isset($claim)}
						<input type="button" value="Add To Claim" id="addtocartbtn" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.claimcart.add'}">
					{else}
						<input type="button" value="Add To Cart" id="addtocartbtn" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.ordercart.add'}">
					{/if}
					</td>
					</tr></table>
				</div>
			{elseif isset($style.WholesalePrice) && $style.WholesalePrice>=0 && $currency.id>0}
				<div class="price">
					<table width="100%"><tr>
					<td align="left">
						<div class="price">{number_format($style.WholesalePrice, 2, ',', ' ')} {$currency.symbol}</div>
					</td>
					<td align="right">
					{if isset($claim)}
						<input type="button" value="Add To Claim" id="addtocartbtn" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.claimcart.add'}">
					{else}
						<input type="button" value="Add To Cart" id="addtocartbtn" class="form" style="cursor: pointer;" nid="{navigation_mark mark='sales.ordercart.add'}">
					{/if}
					</td>
					</tr></table>
				</div>
			{else}
				<div class="price">
					<div class="price">Cannot retrieve price for this style in {$currency.symbol}.</div>
				</div>
			{/if}

	</div>

	</td>
	</tr>
	</table>





</div>

