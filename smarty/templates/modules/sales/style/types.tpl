<div>

<div class="list">
<div class="mainlist card">

{foreach $listItems as $i => $item}
	<div class="type-title">{$item.Name}</div>
	<div class="container clearfix">
		{foreach $item.articles as $j => $article}
		<div class="inner-item {if $article.inCart eq 1}in-cart{/if}" id = "{$article.Id}_{$article.ColorId}_{$article.seasonId}"
			onclick="showArticle({$article.Id}, {$article.ColorId}, {$article.seasonId}, {$nid}, {$vnid})">
			<div>
				{assign var="url" value="{$imageStore}/thumbnails/{$article.Number}_{$article.ColorNumber}.jpg"}
				{if !file_exists($url)}
					{assign var="url" value="image/no_image.png"}
				{/if}
				<img class="item-img" src="{$url}">
			</div>
			<div class="label">{$article.Name}</div>
			<div class="label">{$article.Number}</div>
			<div class="label">{$article.colorName}</div>
			{if isset($article.WholesalePrice) && $article.WholesalePrice>=0 && $currency.id>0}
			<div class="item-price">
				<div class="price">{number_format($article.WholesalePrice, 2, ',', ' ')} {$currency.symbol}</div>
			</div>
			{/if}

		</div>
		{/foreach}

	</div>
{/foreach}
</div>

</div>
</div>





