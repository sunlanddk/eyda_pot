{literal}
   <script type="text/javascript">
      $(document).ready(function() {
          $("#CompanyId").change(function(){
         	var currencyId = $("#CompanyId option:selected").attr('currency');
         	if(currencyId)
         		$('#CurrencyId').val(currencyId);
         	else
         		$('#CurrencyId').val('');
          });

          $( "#dialog-modal" ).dialog({
             appendTo : ".main",
             height   : 200,
             width    : 400,
             modal    : true,
             autoOpen : true,
             buttons  : {
                'Ok': function() {
                   if (save()) {
                      $( "#dialog-modal" ).remove();
                   }
                 },
                 'Cancel': function() {
                    $( "#dialog-modal" ).remove();
                 }
              }
           });
      });

 	  function save () {
 	  	 if($('#CurrencyId').val() != ''){
 	  	 	 if ($('#CurrencyId').val() != $('#selCurrency').val() || $('#CompanyId').val() != $('#selComp').val()){
  	  	 	 	$.address.history(false);
    	  	     appLoadAjaxBody(
    	  	         $("#cartCurr").attr("changeCurrency"),
    	  	         null,
    	  	         null,
    	  	         null,
    	  	         {
    	  	            currencyId : $("#CurrencyId").val(),
    	  	            companyId  : $("#CompanyId").val()
       	  	         },
    	  	         "POST",
    	  	         getCartInfo
    	  	     );
		         $.address.history(true);
 	  	 	 }

	         return true;
 	  	 }else{
 	  	 	$( "tr.warn" ).show();
 	  	 }
    }

	</script>
{/literal}


<div id="dialog-modal" title="Change Currency" style="display: none;">
<input type="hidden" id="selCurrency" value="{$selectedCurrency}" />
<input type="hidden" id="selComp" value="{$selectedCompany}" />
     <div class="tblCurrency" title="Select Currency or Company">
  	   <table>
  	   <tbody>
  	        <tr class="warn" style="display:none">
  	           <td colspan="2" >
                  <span class="ui-icon ui-icon-alert" style="float: left;"></span>
                  <strong>Error:</strong>
                  <font>You should select Currency.&nbsp;</font>
  	           </td>
  	        </tr>
            <tr>
              <td>Currency:</td>
              <td>
                 <select name="CurrencyId"  id ="CurrencyId" style="width: 95%;" required>
                    {html_options options=$currencies selected=$selectedCurrency}
                 </select>
              </td>
            </tr>
            <tr>
              <td>Company:</td>
              <td>
                 <select name="CompanyId" id="CompanyId" style="width: 95%;">
	               {foreach $userCompanies as $k =>  $company}
	               <option currency="{$company['currency']}" value="{$company['value']}" {if $company['value'] == $selectedCompany}selected{/if}>{$company['text']}</option>
	               {/foreach}
	            </select>
              </td>
            </tr>
         </tbody>
  	   </table>
  	</div>
</div>