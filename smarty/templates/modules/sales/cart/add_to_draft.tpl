{assign var=items_count value=$cart|@count}

{if $items_count > 0}
{literal}
   <script type='text/javascript'>
      function checkSelection(data) {
         var tables = $(".tblView").length,
             selected = $(".tblView tr.selected").length,
             result = (tables == selected);

         if (result) {
            data.orders = [];
            $(".tblView tr.selected").each(function() {
               var seasonId = $(this).attr("seasonId");;
               var orderId  = $(this).attr("id");
               var currencyId  = $(this).attr("currencyId");
               data.orders.push({'seasonId' : seasonId, 'orderId' : orderId, 'currencyId': currencyId});
            });
         } else {
            $( "#dialog-modal" ).dialog({
               height: 140,
               modal: true,
               buttons: {
                  Ok: function() {
                    $( this ).dialog( "close" );
                  }
                }
             });
         }

         return result;
      }

      $(document).ready(function(){
         $("table.tblView tr").click(function() {
            var tbody = $(this).closest("tbody");
            $("tr", tbody).removeClass("selected");
            $(this).addClass("selected");
         });
      });
   </script>
{/literal}

{foreach $cart as $seasonId => $season}
   <h2 class="cartInfo">{$season.seasonName}</h2>
   {if count($items[$seasonId]) > 0}
      <table class="tblData tblView" id="view-cart">
   		<thead>
   			{foreach $headers as $header}
      			   <th class="{$styles[$header]}">{$header}</th>
   			{/foreach}
   		</thead>
   		<tbody>
   			{foreach $items[$seasonId] as $i => $item}
   			<tr id={$item.Number} seasonId={$seasonId} currencyId={$item.CurrencyId}>
   				{foreach $headers as $header}
   				<td class="{$styles[$header]}">{$item[$header]}</td>
   				{/foreach}
   			</tr>
   			{/foreach}
   		</tbody>
      </table>
   {else}
      <b>This season doesn't have draft orders. You should create a new one.</b>
   {/if}
{/foreach}

<div id="dialog-modal" title="Please select draft orders." style="display: none;">
  <p><span class="ui-icon ui-icon-alert" style="float: left; margin: 0 7px 20px 0;"></span>You should select one draft order for each seasons.</p>
</div>
{else}
   <div class="ui-widget">
      <div class="ui-state-highlight ui-corner-all messages">
         <p>
            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            <strong>Info:</strong>
            Cart is empty
         </p>
      </div>
   </div>
{/if}