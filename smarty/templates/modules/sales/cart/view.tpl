{assign var=items_count value=$cart|@count}

{if $items_count > 0}
    {literal}
       <script type="text/javascript">
       	  $(document).ready(function() {
             $("#view-cart tbody tr").click(function(){
                showArticle($(this).attr('ArticleId'),  $(this).attr('ArticleColorId'), $(this).attr('SeasonId'), null, $(this).attr('nid'), true);
/*
                appLoadAjaxBody(
                   $(this).attr('nid'),
                   null,
                   null,
                   ".item-panel",
                   {
                       seasonId       : $(this).attr('SeasonId'),
                       colorId        : $(this).attr('ArticleColorId'),
                       articleId      : $(this).attr('ArticleId')
                   }
                );
*/
              });

              $("#view-cart input[value='Delete']").click(function( e ) {
                 $( "div.tblSurvey" ).remove();
                 $( "div.tblLOTs" ).remove();

                 e.stopPropagation();
                 var tr = $(this).closest('tr');
                 $.address.history(false);
                 appLoadAjaxBody(
                    $(this).attr('nid'),
                    null,
                    null,
                    null,
                    {
                        SeasonId       : $(tr).attr('SeasonId'),
                        ArticleColorId : $(tr).attr('ArticleColorId'),
                        ArticleId      : $(tr).attr('ArticleId')
                    },
                    "GET",
                    getCartInfo
                 );
                 $.address.history(true);
              });
              $(".datepicker").each(function() {
                 var val = $(this).attr('lotDate');
                 $(this).datepicker('option', {
                    dateFormat : 'yy-mm-dd',
                    minDate    : val,
                 }).datepicker("refresh");;
              });
              $( "div.tblSurvey" ).dialog({
                 appendTo: ".main",
                 autoOpen: false,
                 height: 300,
                 width: 350,
                 modal: true,
                 buttons: {
                    Close : function() {
                       $( this ).dialog( "close" );
                    }
                 }
              });
              $( "div.tblLOTs" ).dialog({
                 appendTo: ".main",
                 autoOpen: false,
                 height: 400,
                 width: 350,
                 modal: true,
                 open: function( event, ui ) {
                 },
                 buttons: {
                    Save : function() {
                       var deliveryDate = $(".datepicker", this).val(),
                           seasonId     = $(".datepicker", this).attr('seasonId'),
                           data         = null;
                       if (deliveryDate.length > 0) {
                          $("td[season="+seasonId+"]").text(deliveryDate);
                          data = {
                             DeliveryDate : deliveryDate,
                             SeasonId     : seasonId
                          };
                       } else {
                          $(".delivery", this).each(function() {
                             var date     = $(this).val(),
                                 seasonId = $(this).attr('seasonId'),
                                 lotId    = $(this).attr('lotid');
                             $("td[season="+seasonId+"][lotid="+lotId+"]").text(date);
                          });
                          data = $(".delivery", this).serializeObject();
                       }

                       $.address.history(false);
                       appLoadAjaxBody(
                             $("#updatelots").attr('nid'),
                             null,
                             null,
                             ".item-panel",
                             data
                       );
                       $.address.history(true);

                       $( this ).dialog( "close" );
                    },
                    Close : function() {
                       $( this ).dialog( "close" );
                    }
                 }
              });
              $("button.btnClear").button({
                 icons: {
                    primary: "ui-icon-close"
                  },
                  text: false
              }).click(function () {
                 var seasonId = $(this).attr("seasonId");
                 $(".datepicker[seasonId="+seasonId+"]").val('');
              });
              $("button.btnSurvey").button({
                 icons: {
                    primary: "ui-icon-script"
                  },
                  text: false
              }).click(function () {
                 var season = $(this).attr("season");
                 $("div.tblSurvey[season="+season+"]").dialog( "open" );
              });
              $("button.btnDeliveryLot").button({
                 icons: {
                    primary: "ui-icon-calendar"
                  },
                  text: false
              }).click(function () {
                 var season = $(this).attr("season");
                 $("div.tblLOTs[season="+season+"]").dialog( "open" );
              });

          });
       </script>
    {/literal}

    <div class="item-panel"></div>
    <input type="hidden" id="updatelots" nid="{navigation_mark mark='sales.ordercart.updatelots'}" />

    {foreach $cart as $seasonId => $season}
	   <h2 class="cartInfo">{$season.seasonName}
		  {if !$cartCustomer==NULL and 1==0} to {$cartCustomer} {/if}
	      <button season="{$seasonId}" class="btnSurvey button-small">Season survey</button>
      	  {if $showLots == 1}
      	      <button season="{$seasonId}" class="btnDeliveryLot button-small">Delivery Lot</button>
          {/if}
       </h2>

	<div style="display: none;" class="tblSurvey" season="{$seasonId}" title="Season survey">
	   <table class="tblData tblView">
	   <thead>
	     <tr>
		  <th class="left">Name</th>
		  <th class="right">qty</th>
		  <th class="right">Styles</th>
		 </tr>
	   </thead>
	   <tbody>
           {foreach $categories[$seasonId] as $category}
           {if $category.pcs > 0}
              <tr>
                 <td>{$category.Name}</td>
                 <td class="right">{$category.qty} pcs.</td>
                 <td class="right">{$category.pcs} styles</td>
              </tr>
           {/if}
           {/foreach}
       </tbody>
	   </table>
	   <br/>
	</div>
    {if $showLots}
      	<div style="display: none;" class="tblLOTs" season="{$seasonId}" title="Delivery LOTs">
      	   <table class="tblData tblView">
      	   <input type="hidden" name="autofocus" autofocus="autofocus" />
      	   <tbody>
      	   	   <thead>
                    <tr>
      	   	        <th>LOTS</th>
                    <th>Delivery</th>
                    <th>Later Delivery</th>
                    </tr>
                 </thead>
                 {assign var="lastDate" value="" }
                 {foreach $lots[$seasonId] as $lot}
                    {if $lot.Date != null}
                    {assign var="lastDate" value=$lot.Date }
                    <tr>
                       <td>{$lot.Name}</td>
                       <td>{$lot.Date}</td>
                       <td>
                          {assign var="lotIndex" value=$lot@index }
                          <select name="LaterDelivery[{$seasonId}][{$lot.Id}]" class="delivery" lotId="{$lot.Id}" seasonId="{$seasonId}">
                            {foreach $lots[$seasonId] as $delivery}
                               {if $delivery@index >= $lot@index}
                                  <option value="{$delivery.Date}">{$delivery.Name} </option>
                               {/if}
                            {/foreach}
                          </select>
                       </td>
                    </tr>
                    {/if}
                 {/foreach}
                 <tr><td colspan="3">&nbsp;</td></tr>
                 <tr>
                    <td >Delivery Date</td>
                    <td colspan="2">
                       <input type="text" name="DeliveryDate" class="datepicker" lotDate="{$lastDate}" seasonId="{$seasonId}" readonly="readonly"/>
                       <button class="btnClear button-small" seasonId="{$seasonId}">Clear</button>
                    </td>
                 </tr>
             </tbody>
      	   </table>
      	   <br/>
      	</div>
   	{/if}

	<table class="tblData tblView" id="view-cart">
		<thead>
           <tr>
           <th width="10%">&nbsp;</th>
           <th width="15%">Style No</th>
           <th width="15%">Color</th>
           <th width="15%">Sizes</th>
           <th width="8%">Delivery Date</th>
           <th width="5%"  class="right">Discount</th>
           <th width="7%" class="right">Price</th>
           <th width="5%"  class="right">Q-ty</th>
           <th width="10%" class="right">Subtotal</th>
           <th width="10%">&nbsp;</th>
           </tr>
		</thead>
		<tbody>
           {foreach $season.items as $article}
              <tr nid='{navigation_mark mark="$detailsMark"}'  SeasonId="{$seasonId}" ArticleColorId="{$article.ArticleColorId}" ArticleId="{$article.ArticleId}" style="cursor:pointer;">
                 <td>
      				{assign var="url" value="{$imageStore}/thumbnails/{$article.Number}_{$article.ColorNumber}.jpg"}
      				{if !file_exists($url)}
      					{assign var="url" value="image/no_image.png"}
      				{/if}
      				<img  src="{$url}" height="80px" width="80px">
                 </td>
                 <td>{$article.Number}<br/>{$article.Desc}</td>
                 <td>{$article.ColorNumber}<br>{$article.ColorDesc}</td>
				 <td>
				{foreach $article.Sizes as $_size}
					{$_size.name},
				{/foreach}
				 </td>
                 <td lotid="{$article.LOTId}" season="{$seasonId}">{$article.DeliveryDate|default: {$article.LotDate}}</td>
                 <td class="right">0 %</td>
                 <td class="right">{number_format($article.Price, 2, ',', ' ')} {$season.currency.Name}</td>
                 <td class="right">{$article.qty}</td>
                 <td class="right">{number_format($article.subTotal, 2, ',', ' ')} {$season.currency.Name}</td>
                 <td><input type="button" value="Delete" class="form" nid='{navigation_mark mark="sales.`$cartMark`.delete"}'/></td>
              </tr>
           {/foreach}
		</tbody>
		<tfoot>
		   <tr>
		      <th colspan="6">&nbsp;</th>
		      <th class="right">SubTotal</th>
		      <th class="right">{$season.qty}</th>
		      <th class="right">{number_format($season.subTotal, 2, ',', ' ')} {$season.currency.Name}</th>
		      <th>&nbsp;</th>
		   </tr>
		</tfoot>
	</table>
	{/foreach}
{else}
   <div class="ui-widget">
      <div class="ui-state-highlight ui-corner-all messages">
         <p>
            <span class="ui-icon ui-icon-info" style="float: left; margin-right: .3em;"></span>
            <strong>Info:</strong>
            Cart is empty
         </p>
      </div>
   </div>
   <div class="item-panel"></div>
{/if}