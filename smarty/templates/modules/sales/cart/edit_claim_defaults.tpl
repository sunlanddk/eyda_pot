{literal}
   <script type='text/javascript'>
      function saveForm(data) {
         $("#alert").hide();

         var msg = [];
         if ($("#CompanyId").val() == "") {
            $("#alert font").html("You should select Customer Company first.");
            $("#alert").show();
            return;
         }

         $("[required]").each(function() {
            $(this).removeClass("ui-state-error");
            if ($(this).val() == "") {
               $(this).addClass("ui-state-error");
               msg.push($(this).attr("name"));
            }
         });

         $("#ClaimTypeId").removeClass("ui-state-error");
         var stateId = $("#ClaimStateId").val();
         var typeId = $("#ClaimTypeId").length > 0 ? $("#ClaimTypeId").val() : null;
         if (stateId > 1 && typeId == "") {
            $("#ClaimTypeId").addClass("ui-state-error");
            msg.push("ClaimTypeId");
         }

         if (msg == "") {
            $( "#dialog-modal" ).dialog({
               height: 220,
               width : 400,
               modal: true,
               buttons: {
                  'Confirm & Mail': function() {
                    save(true);
                    $( this ).dialog( "close" );
                  },
                  'Only Confirm': function() {
                     save(false);
                     $( this ).dialog( "close" );
                  },
                  'Cancel': function() {
                     $( this ).dialog( "close" );
                  }
                }
             });
            return true;
         } else {
            $("#alert font").html("Next fields: <b>" + msg.join() + "</b> are mandatory.");
            $("#alert").show();
            return false;
         }
      }

      function save (withMail) {
         var data =  $("#defaults").serializeObject();

         if (withMail) {
            data['sendMail'] = 1;
            data['Email'] = $("#Email").val();
            data['EmailUpdate'] = $("#EmailUpdate").is(':checked');
         }
         $.address.history(false);
         appLoadAjaxBody(
               $("#savenew").attr('nid'),
               null,
               null,
               "#alert",
               data,
               "POST"
         );
         $.address.history(true);
      }

      $(document).ready(function() {
   		$("#CompanyId").change(function() {
   		   appLoadLegacy(true, $("#editdef").attr('nid'), 0, "CompanyId="+$(this).val());
   		});
      });
   </script>
{/literal}

<div class="ui-widget" style="display: none;" id="alert">
   <div class="ui-state-error ui-corner-all messages">
   <p>
      <span class="ui-icon ui-icon-alert"></span>
      <strong>Error:</strong>
      <font>&nbsp;</font>
   </p>
   </div>
</div>

<form class="saveform" id="defaults">
   <input type="hidden" id="editdef" nid="{$nid}" />
   <input type="hidden" id="savenew" nid="{navigation_mark mark='sales.claimcart.savenew'}" />
   <table class="saveform">
      {if $user['Extern']}
      <input type="hidden" name="CompanyId" value="{$record['CompanyId']}" />
      <tr>
         <td class="header" colspan="4">{$header}</td>
      </tr>
      {else}
      <tr>
         <td class="label">Company:</td>
         <td class="field">
            <select name="CompanyId" id="CompanyId" required>
               {html_options options=$userCompanies selected=(int)$record['CompanyId']}
            </select>
      </tr>
      {/if}
      <tr>
         <td class="label">Description:</td>
         <td class="field"><input name="Description" value="{$record['Description']}" /></td>
         <td class="label">State:</td>
         <td class="field">
            <select name="ClaimStateId" id="ClaimStateId" required {if $user['Extern']}disabled="disabled"{/if}>
               {html_options options=$states selected="1"}
            </select>
         </td>
      </tr>
      <tr>
         <td class="label">Reference:</td>
         <td class="field"><input name="Reference" value="{$record['Reference']}" /></td>
           {if !$user['Extern']}
               <td class="label">Type:</td>
               <td class="field">
                  <select name="ClaimTypeId" id="ClaimTypeId" >
                     {html_options options=$types selected=(int)$record['ClaimTypeId']}
                  </select>
               </td>
            {/if}
         </td>
      </tr>
      <tr>
         <td class="label">Owner Company:</td>
         <td class="field">
            <input type="hidden" name="ToCompanyId"  value="{$record['ToCompanyId']}" />
            <select name="ToCompanyId" disabled="disabled" required>
               {html_options options=$companies selected=(int)$record['ToCompanyId']}
            </select>
         </td>
         <td class="label">Sales Ref:</td>
         <td class="field">
            <select name="SalesUserId">
               {html_options options=$sales_ref selected=(int)$record['SalesUserId']}
            </select>
         </td>
      </tr>
      <tr>
         <td class="label">Currency:</td>
         <td class="field">
            <!--  input type="hidden" name="CurrencyId" value="{$record['CurrencyId']}" /-->
            <select name="CurrencyId" required>
               {html_options options=$currencies selected=(int)$record['CurrencyId']}
            </select>
         </td>
         <td class="label">Invoice:</td>
         <td class="field">
            <select name="InvoiceId">
               {html_options options=$invoices selected=(int){$selected}}
            </select>
         </td>
      </tr>
      {if !$user['Extern']}
         <tr>
            <td class="label">Internal Comment:</td>
            <td class="field" colspan="3">
               <textarea name="InternalComment">{$record['InternalComment']}</textarea>
            </td>
         </tr>
         <tr>
            <td class="label">External Comment:</td>
            <td class="field" colspan="3">
               <textarea name="ExternalComment">{$record['ExternalComment']}</textarea>
            </td>
         </tr>
         {/if}
   </table>
</form>

<div id="dialog-modal" title="Send Confirmation To mails" style="display: none;">
  <p>Customer <b>{$record.Name}</b> is registered to use the email address <font color="red">{$record.Mail}</font>. Id e-mail is incorrect please change it in the textbox below and finally confirm by clicking confirm button</p>
  <br>
  <form class="saveform">
     <table class="saveform">
        <tr>
           <td class="label left small">Email:</td>
           <td class="field">
              <input name="Email" id="Email" value="{$record.Mail}"/>
           </td>
        </tr>
        <tr>
           <td class="label  left small">
              <input type=checkbox id="EmailUpdate" name="EmailUpdate" value="1">
           </td>
          <td class="label  left large">
          Update e-mail address in customer account.
          </td>
        </tr>
     </table>
  </form>
</div>