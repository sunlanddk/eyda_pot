{literal}
	<script type='text/javascript' src='js/fileupload.js'></script>
{/literal}

<form id="uploadform" class="form" enctype="multipart/form-data" action="index.php" method="post">
	<table class="tblUpload">
		<tr>
			<td>File to Upload:</td>
			<td>
				<input name="file" id="file" type="file" value=""/>
			</td
		</tr>
		<tr>
			<td>Skip Errors:</td>
			<td>
				<input name="skipErrors" id="skipErrors" type="checkbox" value="1"/>
			</td
		</tr>
		<tr>
			<td colspan="2">
				<input type="button" value="Upload" onclick="uploadFile();"/>
				<input type="button" value="Print" onclick="printResult();"/>
			</td>
		</tr>
	</table>
	<input id="nid" name="nid" type="hidden" value="{$nid}" >
</form>
<div id="uploadresult"></div>