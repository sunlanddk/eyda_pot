{literal}
	<script type='text/javascript' src='js/budget.js'></script>
{/literal}

<table class="tblBudget">
	<thead>
		<th>Customer/Sales Responsible</th>
		{foreach $headers as $header}
			<th>{$header}</th>
		{/foreach}
	</thead>
	<tfoot>
		<td>Total</td>
		{foreach $headers as $i => $header}
			<td><span>Budget:</span>{number_format($total[$i]['Budget'], 2, ',', ' ')}{" "}{$currencyName}
			<br><span>Sold:</span>{number_format($total[$i]['Sold'], 2, ',', ' ')}{" "}{$currencyName}
			<br><span>Invoiced:</span>{number_format($total[$i]['Invoiced'], 2, ',', ' ')}{" "}{$currencyName}</td>
		{/foreach}
	</tfoot>
	<tbody>
		{foreach $companies as $i => $item}
		<tr>
			<td>{$item['cname']}
			<br>{$item['uname']}
			<br>{$item['aname']}</td>
			{foreach $headers as $j => $header}
  			   <td onclick="editBudget({$item['currencyid']}, {$i}, null, {$j}, {$nid})">
  			   <span>Budget:</span> {if (isset($item[$j]['Budget']) && $item[$j]['Budget']>0)}{number_format($item[$j]['Budget'], 2, ',', ' ')}{" "}{$item['currencyname']}{/if} 
  			   <br><span>Sold:</span> {if isset($item[$j]['Sold'])}{number_format($item[$j]['Sold'], 2, ',', ' ')}{" "}{$item['currencyname']}{/if}
  			   <br><span>Invoiced:</span> {if isset($item[$j]['Invoiced'])}{number_format($item[$j]['Invoiced'], 2, ',', ' ')}{" "}{$item['currencyname']}{/if}</td>
			{/foreach}
		</tr>
		{/foreach}
	</tbody>
</table>