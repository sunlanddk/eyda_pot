{literal}
<script type="text/javascript" src="jquery.js"></script> 
{/literal}

<div>

<div class="list">
<div class="mainlist card">

	<div class="container clearfix">
		{foreach $listItems as $i => $item}
		<div class="inner-item {if $item.inCart eq 1}in-cart{else}{if $item.soldOut}sold-out{/if}{/if}" id = "{$item.Id}_{$item.ColorId}_{$item.seasonId}"
			onclick="showArticle({$item.Id}, {$item.ColorId}, {$item.seasonId}, {$nid}, {$vnid})">
			<div>
				{assign var="url" value="{$imageStore}/thumbnails/{$item.Number}_{$item.ColorNumber}.jpg"}
				{if !file_exists($url)}
					{assign var="url" value="image/no_image.png"}
				{/if}
				<img class="item-img" src="{$url}">
			</div>
			<center><div class="{if $item.soldOut}soldout{else}label{/if}">{$item.Number} - {$item.Name}</div></center>
			<div class="{if $item.soldOut}soldout{else}label{/if}">{$item.colorName}</div>
			{if isset($item.MinMax.MaxWholeSalePrice) && $item.MinMax.MaxWholeSalePrice>0}
					{if $item.MinMax.MaxWholeSalePrice==$item.MinMax.MinWholeSalePrice}
						<div class="{if $item.soldOut}soldout{else}label{/if}">{number_format($item.MinMax.MinWholeSalePrice, 2, ',', ' ')} {$currency.symbol}</div>
						{if $item.soldOut}<div class="soldout">Sold out</div>{/if}
					{else}
						<div class="{if $item.soldOut}soldout{else}label{/if}">{number_format($item.MinMax.MinWholeSalePrice, 2, ',', ' ')} - {number_format($item.MinMax.MaxWholeSalePrice, 2, ',', ' ')} {$currency.symbol}</div>
						{if $item.soldOut}<div class="soldout">Sold out</div>{/if}
					{/if}
			{elseif isset($item.WholesalePrice) && $item.WholesalePrice>=0 && $currency.id>0}
					<div class="{if $item.soldOut}soldout{else}label{/if}">{number_format($item.WholesalePrice, 2, ',', ' ')} {$currency.symbol}</div>
					{if $item.soldOut}<div class="soldout">Sold out</div>{/if}
			{/if}
		</div>
		{/foreach}
	</div>
</div>

</div>
</div>





