{literal}
<script type="text/javascript">

$(document).ready(function() {
	$('#filterform select, #filterform input[type!="radio"], #filterform input[type!="checkbox"]').change(function() {
		startFiltering();
	});

	$('#filterform input[type!="button"][type!="radio"][type!="checkbox"][type!="hidden"]').keypress(function(event) {
		if(event.which == 13) {
			event.preventDefault();
			startFiltering();
		}
	});
});

function startFiltering(){
	if($('#formSubmit').length){
		$('#filterform').submit();
	} else {
	   appLoadAjaxBody(
	      $('#filterform input[name="nid"]').val(),
	      null,
	      null,
	      null,
	      {
	         filter : $("#filterform").serializeObject()
          },
	      $('#filterform').attr('method')
	   );
	}
}

</script>
{/literal}

<div class="filter container clearfix {if $verticallayout}filter-left{/if}">
	<form method=GET name="filterleftform" id="filterform" class="form filter left">
		<input type=hidden name=nid value={$nid}>
		{if $Id > 0}
			<input type=hidden name=id value={$Id}>
		{/if}

		{if $listSort != 0}
			<input type=hidden name=sort value={$listSort}>
		{/if}

		{if $Navigation["LayoutRaw"]}
			<input type=hidden id=formSubmit name=formSubmit value={$formSubmit}>
		{/if}

		<ul>

               {if $n > 1}
                  {foreach $listField as $i => $field}
                     {if !$field || $field.nofilter }
                        {continue}
                     {/if}
                     <li class="inner">
                        <label class="label">{$field.name|escape}</label>
                        {if !isset($field.type)}
                        	{assign var="field.type" value="0"}
                        {/if}

                        {if $field.type == 1}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="datepicker">
                        {elseif $field.type == 2}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="integer">
                        {elseif $field.type == 3}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="float">
                        {elseif $field.type == 4}
                        	<select name="{$field.name}" class="">
                        	<option></option>
                        	{if isset($field.options)}
                        		{html_options options=$field.options selected=$filterValues.$i}
                        	{else}
                        		<option value="Yes">Yes</option>
                        		<option value="No">No</option>
                        	{/if}
                        	</select>
                        {elseif $field.type == 5 && isset($field.options)}
                        	<select name="{$field.name}" class="">
                        		{if isset($field.options)}
                        			{html_options options=$field.options selected=$filterValues.$i}
                        		{/if}
                        	</select>
                        {else}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="">
                        {/if}
                     </li>
                  {/foreach}
               {else}
                  <input type=hidden name=field value={$fid}>
               {/if}
		</ul>

	</form>
</div>

