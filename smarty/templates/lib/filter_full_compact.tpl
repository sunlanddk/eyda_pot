<div class="filter container clearfix">
   <form method=GET name="filterform" id="filterform" class="form filter">
      <input type=hidden name=nid value={$Navigation["DefaultNID"]|default: {$nid}}>
      {if $Id > 0}
         <input type=hidden name=id value={$Id}>
      {/if}

      {if $listSort != 0}
         <input type=hidden name=sort value={$listSort}>
      {/if}
      <ul style="float: left; width: 90%;" class="">

               {if $n > 1}
                  {foreach $listField as $i => $field}
                     {if !$field || $field.nofilter }
                        {continue}
                     {/if}
                     <li class="inner">
                        <label class="label">{$field.name|escape}</label>
                        {if !isset($field.type)}
                        	{assign var="field.type" value="0"}
                        {/if}

                        {if $field.type == 1}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="datepicker">
                        {elseif $field.type == 2}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="integer">
                        {elseif $field.type == 3}
                        	<input type=text name="{$field.name}" value="{$filterValues.$i}" class="float">
                        {elseif $field.type == 4}
                        	<select name="{$field.name}" class="">
                        	<option></option>
                        	{if isset($field.options)}
                        		{html_options options=$field.options selected=$filterValues.$i}
                        	{else}
                        		<option value="Yes">Yes</option>
                        		<option value="No">No</option>
                        	{/if}
                        	</select>
                        {elseif $field.type == 5 && isset($field.options)}
                        	<select name="{$field.name}" class="">
                        		{if isset($field.options)}
                        			{html_options options=$field.options selected=$filterValues.$i}
                        		{/if}
                        	</select>
                        {else}
                        	<input type=text name="{$field.name|escape}" value="{$filterValues.$i}" class="">
                        {/if}
                     </li>
                  {/foreach}
               {else}
                  <input type=hidden name=field value={$fid}>
               {/if}
				<li class="inner">
                    <label class="label">&nbsp;</label>
					{if $Navigation["LayoutRaw"] ||  $Navigation["DefaultLayoutRaw"]}
						<input type="button" id="submit" value="Search"/>
					{else}
						<input type="submit" value="Search"/>
					{/if}
				</li>
				<li class="inner">
                    <label class="label">&nbsp;</label>
					<input type="hidden" id="clearFilter" name="clearFilter" value=""/>
					<input type="button" id="reset" value="Clear"/>
               {if $ShowNorwayOption == 'true'}
                  {if $Norway == 'true'}
                     <input type="button" id="hidenorway" value="Hide Norway" style="width:104px" />
                  {else}
                     <input type="button" id="shownorway" value="Show Norway" style="width:110px" />
                  {/if}
               {/if}
				</li>
			   
      </ul>
      <div style="clear: both"></div>
   </form>
</div>

