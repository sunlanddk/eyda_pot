<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {prepare_paging_link} plugin
 *
 * Type:     function
 * Name:     prepare_paging_link
 * Purpose:  prepares onclick attribute string with appLoadAjaxBody function call
 */
function smarty_function_prepare_paging_link($params, &$smarty) {
	return sprintf("onclick=\"appAjaxPaging(%d);return false;\"", $params['offset']);
}
?>