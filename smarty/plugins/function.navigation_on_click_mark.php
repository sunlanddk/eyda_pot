<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_on_click_mark} plugin
 *
 * Type:     function
 * Name:     navigation_on_click_mark
 * Purpose:  outputs result of navigationOnClickMark function call
 */
function smarty_function_navigation_on_click_mark($params, &$smarty) {
	return navigationOnClickMark($params['mark'], $params['id'], $params['param']);
}
?>