<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_onclick_link} plugin
 *
 * Type:     function
 * Name:     navigation_onclick_link
 * Purpose:  outputs result of navigationOnClickLink call and temporary sets $Navigation['Id'] as $Navigation['ParentId'] 
 *           if $params['switch_id'] is set to true
 */
function smarty_function_navigation_onclick_link($params, &$smarty) {
	if($params['switch_id'] === true) {
		global $Navigation;
		$nid = $Navigation['Id'];
		$Navigation['Id'] = $Navigation['ParentId'];
		$link = navigationOnClickLink($params['mark'], $params['id'], $params['param']);
		$Navigation['Id'] = $nid;
		return $link;
	} else {
		return navigationOnClickLink($params['mark'], $params['id'], $params['param']);
	}
}
?>