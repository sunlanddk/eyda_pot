<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */

/**
 * Smarty {navigation_on_click_link_by_mark} plugin
 *
 * Type:     function
 * Name:     navigation_on_click_link_by_mark
 * Purpose:  outputs result of navigationOnClickLinkByMark function call
 */
function smarty_function_navigation_on_click_link_by_mark($params, &$smarty) {
	return navigationOnClickByMark($params['mark'], $params['id'], $params['param'], $params['type']);
}
?>