<?php
/**
 * Smarty plugin
 * @package Smarty
 * @subpackage plugins
 */
   require_once 'lib/navigation.inc' ;
/**
 * Smarty {navigation_mark} plugin
 *
 * Type:     function
 * Name:     navigation_mark
 * Purpose:  outputs result of navigationMark function call
 */
function smarty_function_navigation_mark($params, &$smarty) {
	return navigationMark($params['mark']);
}
?>