<?php 
//die();
// $json = '{"data":"eyJhbGciOiJIUzI1NiJ9.IntcImFjdGlvblwiOlwiY29ubmVjdGlvbl90ZXN0XCIsXCJkYXRhXCI6e319Ig.PDr4nTFMgC2538hp-N_GxsumxvJ2kALYjVi-Fn8DLCY"}';
// $obj = json_decode($json, true);
// $splittet = explode('.', $obj['data']);
// $data = decodeDataFromShipmondo($splittet[1]);
// print_r($data);

// function addPadding($base64String){
//     if (strlen($base64String) % 4 !== 0) {
//         return addPadding($base64String . '=');
//     }

//     return $base64String;
// }

// function urlDecodeNew($toDecode)
// {
//     return (string) base64_decode(
//         addPadding(toBase64($toDecode)),
//         true
//     );
// }

// function toBase64($urlString)
// {
//     return str_replace(['-', '_'], ['+', '/'], $urlString);
// }

// function decodeDataFromShipmondo($toDecode)
// {
//     return json_decode(urlDecodeNew($toDecode), true);
// }



// die();
require_once('lib/config.inc');
require_once('lib/db.inc');
require_once('lib/table.inc');
// DK
$ch = curl_init('https://f0c1dc1fcc573851a741f993dc31ab42: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@eyda-development.myshopify.com/admin/api/2021-01/products.json?limit=250');

// EU 
//$ch = curl_init('https://bf1f28a61dbc759b0442f45f68aafa7f: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@eyda-development-eur.myshopify.com/admin/api/2021-04/products.json?limit=250');
curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// DK
curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: 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", "Content-Type: application/json"));
// EU
//curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: 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", "Content-Type: application/json"));
curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
$result = curl_exec($ch);
curl_close($ch);


$currencyId = 1; // DKK
$languageid = 4; // DKK
$first = 0;

//$currencyId = 2; // EUR
//$languageid = 6; // UK
//$first = 0;

// $currencyId = 4; // NOK
// $languageid = 1; // NOK
// $first = 0;

// $currencyId = 5; // SEK
// $languageid = 1; // SEK
// $first = 0;

// die();

// $file = file_get_contents('products-dk.json');
//die($result) ;
$json = json_decode($result, true);

?>
<table>
	<thead>
		<tr>
			<th style="text-align:left">Title</th>
			<th style="text-align:left">Id</th>
			<th style="text-align:left">Variant Id</th>
			<th style="text-align:left">SKU</th>
			<th style="text-align:left">Barcode</th>
			<th style="text-align:left">Message</th>
		</tr>
	</thead>
	<tbody>
		<?php
		$products = [];
		$errors = [];
		foreach ($json['products'] as $key => $product) {
			$shopifyProductId = $product['id'];
			$count = 1;
			foreach ($product['variants'] as $vkey => $variant) {
				if($variant['sku'] == '' || $variant['sku'] == ' '){
					echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>Sku er tomt</td></tr>';
					// echo $variant['title'].' '.$variant['id'].' '.$variant['sku'];
					// echo '<br/>';
					continue;
				}

				$shopifyVariantId = $variant['id'];
				$iteminventoryId = $variant['inventory_item_id'];
				$variantCodeId = tableGetFieldWhere('variantcode', 'Id', 'Active=1 AND SKU="'.$variant['sku'].'"');
				$articleId = tableGetField('variantcode', 'ArticleId', (int)$variantCodeId);
				if((int)$variantCodeId > 0){
					if((int)tableGetFieldWhere('shopifyvariant', 'Id', sprintf("ShopifyshopId=%d AND VariantCodeId=%d AND ArticleId=%d", 1, $variantCodeId, $articleId)) > 0){
						$articleColorId = tableGetField('variantcode', 'ArticleColorId', (int)$variantCodeId);
						$articleSizeId = tableGetField('variantcode', 'ArticleSizeId', (int)$variantCodeId);
						
						if($count === 1){
							updateName($articleId, $product, $languageid);
							
							if($first === 1){
								// echo $variantCodeId;
								// echo PHP_EOL;
								$found = updateCluster($articleId, 5, $product['product_type']);
								if($found !== true){
									array_push($errors, $variantCodeId. ' ' .$found);
									echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>'.$found.'</td></tr>';
								}
								
								$tags = explode(',', $product['tags']);

								$found = updateTagsCluster($articleId, $tags);
								if($found !== true){
									array_push($errors, $variantCodeId. ' ' .$found);
									echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>'.$found.'</td></tr>';
								}
								$found = updateCluster($articleId, 9, $product['status']);
								if($found !== true){
									array_push($errors, $variantCodeId. ' ' .$found);
									echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>'.$found.'</td></tr>';
								}

								// updateCountryAndHs($variant, $articleId);
								
							}

						}

						setWeight($variantCodeId, $variant['weight']);
						$return = setPrices($variantCodeId, $variant, $currencyId);
						if($return !== true){
							array_push($errors, $variantCodeId. ' ' .$return);
							echo '<tr><td>'.$variant['title'].'</td><td>'.$product['id'].'</td><td>'.$variant['id'].'</td><td>'.$variant['sku'].'</td><td>'.$variant['barcode'].'</td><td>'.$return.'</td></tr>';
						}

						// echo $variantCodeId.' '.$articleId.' '.$articleColorId.' '.$articleSizeId;
						// echo '<br />';

						$count ++;
					}
					
				}
				else{
				}
			}
		}

		

		echo '<br />';
		echo '<br />';
		echo '<br />';
		echo '<br />';
		echo $count;

		print_r($errors);

		?>
	</tbody>
</table>
<?php

function updateCountryAndHs($variant, $articleid){
	return true;
	$ch = curl_init('https://f0c1dc1fcc573851a741f993dc31ab42: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@eyda-development.myshopify.com/admin/api/2021-01/inventory_items/'.$variant['inventory_item_id'].'.json');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_HTTPHEADER, array("X-Shopify-Access-Token: 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", "Content-Type: application/json"));
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);
	curl_close($ch);
	$result = json_decode($result, true);
	
	$post = array(
		'WorkCountryId' => (int)tableGetFieldWhere('country', 'Id', sprintf("Active=1 AND VATPercentage='25.0' AND IsoCode='%s'",$result['inventory_item']['country_code_of_origin']))
	);

	tableWrite('article', $post, (int)$articleid);
	return true;
}

function createBasePrices($collectionmemberid){
	$_currencyids = array(0,1,2,4,5) ;

	foreach ($_currencyids as $key => $id) {
		$priceid = tableGetFieldWhere('collectionmemberprice', 'id', sprintf("Active=1 AND CollectionMemberId=%d AND CurrencyId=%d", $collectionmemberid, $id));
		$CollectionMemberPrice = array (
			'CollectionMemberId'=>	$collectionmemberid,
			'CurrencyId' 		=>	$id,
			'RetailPrice' 	 	=>	0
		) ;
		$_collectionmemberpriceid=tableWrite('collectionmemberprice', $CollectionMemberPrice, (int)$priceid) ;
	}
	return true;
}

function setPrices($variantcodeid, $variant, $currencyId = 1){

	$articleId = tableGetField('variantcode', 'ArticleId', $variantcodeid);
	$articleColorId = tableGetField('variantcode', 'ArticleColorId', $variantcodeid);
	$articleSizeId = tableGetField('variantcode', 'ArticleSizeId', $variantcodeid);
	$collectionmemberid = tableGetFieldWhere('collectionmember', 'Id', sprintf("Active=1 AND ArticleId=%d AND ArticleColorId=%d", $articleId, $articleColorId));
	if((int)$collectionmemberid === 0){
		return 'Collectionmember not found.'. $variantcodeid;
	}
	createBasePrices($collectionmemberid);

	$CollectionMemberPriceId = tableGetFieldWhere('collectionmemberprice', 'Id', sprintf("Active=1 AND CollectionMemberId=%d AND CurrencyId=%d", $collectionmemberid, $currencyId));
	if((int)$CollectionMemberPriceId === 0){
		return 'Default CollectionmemberPrice not found.'. $variantcodeid;
	}

	$collectionSizeId = tableGetFieldWhere('collectionmemberpricesize', 'Id', sprintf("Active=1 AND CollectionMemberPriceId=%d AND ArticleSizeId=%d", $CollectionMemberPriceId, $articleSizeId));

	$CollectionMemberPriceSize = array (
		'CollectionMemberPriceId'=>	$CollectionMemberPriceId,
		'ArticleSizeId'			=>	$articleSizeId,
		'RetailPrice' 	 		=>	(float)str_replace(',','.',$variant['price']),
		'CampaignPrice' 	 	=>	(float)str_replace(',','.',$variant['compare_at_price']),
		'PurchasePrice' 	 	=>	(float)str_replace(',','.',0),
	) ;


	$_collectionmemberpricesize=tableWrite('collectionmemberpricesize', $CollectionMemberPriceSize, (int)$collectionSizeId) ;

	return true;
}

function setWeight($variantcodeid, $weight){
	$post = array(
		'Weight' => $weight,
	);

	tableWriteCron('variantcode', $post, (int)$variantcodeid);

	return true;
}

function updateTagsCluster($articleid, $tags){
	foreach ($tags as $key => $tag) {
		// if (strpos($tag, 'size-scale') !== false) {
		// 	updateCluster($articleid,8,$tag);
		// 	continue;
		// }

		$query = sprintf ("SELECT * FROM clustertype WHERE ClusterGroupId=%d AND Active=1", 1) ;
	    $res = dbQuery ($query) ;
	    $typeIds = [];
	    while ($row = dbFetch ($res) ) {
			array_push($typeIds, $row['Id']);
	    }
	    dbQueryFree ($res) ;

		// echo $clustertypeIds = tableGetFieldWhere();
		$clusterId = tableGetFieldWhere('cluster', 'Id', sprintf("Active=1 AND Name='%s' AND TypeId IN(%s)", str_replace(' ', '', $tag), implode(',', $typeIds)));
		$clusterTypeId = tableGetField('cluster', 'TypeId', (int)$clusterId);
		if((int)$clusterId === 0 || (int)$clusterTypeId === 0 ){
			return 'Error tag not found: '.$tag;
		}

		$clusterarticleId = tableGetFieldWhere('clusterarticle', 'Id', sprintf("ClusterId=%d AND ArticleId=%d", $clusterId, $articleid));	

		$post = array(
			'ArticleId' => $articleid,
			'TypeId'	=> $clusterTypeId,
			'ClusterId' => $clusterId
		);
		
		tableWrite('clusterarticle', $post, (int)$clusterarticleId);
	}
	return true;
}

function updateCluster($articleid, $clustertypeid, $value){
	if($value == ''){
		return true;
	}
	// if((int)$clustertypeid === 8){
	// 	$value = str_replace(' ', '', $value);
	// 	$value = str_replace(' ', '', $value);
	// 	$value = str_replace(' ', '', $value);
	// 	if($value == 'size-scale-1'){
	// 		$value = 1;	
	// 	}
	// 	if($value == 'size-scale-2'){
	// 		$value = 2;	
	// 	}
	// 	if($value == 'size-scale-3'){
	// 		$value = 3;	
	// 	}
	// 	if($value == 'size-scale-4'){
	// 		$value = 4;	
	// 	}
	// 	if($value == 'size-scale-5'){
	// 		$value = 5;	
	// 	}
	// }
	$clusterId = tableGetFieldWhere('cluster', 'Id', sprintf("Name='%s' AND TypeId=%d", ucfirst($value), $clustertypeid));	
	if((int)$clusterId == 0){
		return 'No cluster found '.$clustertypeid .' - '.$value;
	}

	$clusterarticleId = tableGetFieldWhere('clusterarticle', 'Id', sprintf("TypeId=%d AND ArticleId=%d", $clustertypeid, $articleid));	

	$post = array(
		'ArticleId' => $articleid,
		'TypeId'	=> $clustertypeid,
		'ClusterId' => $clusterId
	);

	tableWrite('clusterarticle', $post, (int)$clusterarticleId);

	return true;
}

function updateName($articleid, $value, $languageid = 4){
	$originalName = tableGetField('article', 'Description', $articleid);

	$articleLangId = tableGetFieldWhere('article_lang', 'Id', sprintf("ArticleId=%d AND LanguageId=%d", $articleid, $languageid));
	
	$post = array(
		'ArticleId'		=> $articleid,
		'LanguageId'	=> $languageid,
		'WebDescription' => base64_encode(utf8_decode($value['body_html'])),
		'Description'	=> '',
	);

	if($originalName !== $value['title']){
		$post['Description'] = $value['title'];
	}

	tableWriteCron('article_lang', $post, (int)$articleLangId);
	return true;
}

function updateWeight($variantcodeid, $value){
	
}



