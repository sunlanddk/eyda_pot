<?php 

require_once('lib/config.inc');
require_once('lib/db.inc');
require_once('lib/table.inc');


$file = file_get_contents('products-eu.json');
$json = json_decode($file, true);


foreach ($json['products'] as $key => $product) {
	$shopifyProductId = $product['id'];
	foreach ($product['variants'] as $vkey => $variant) {
		$shopifyVariantId = $variant['id'];
		$iteminventoryId = $variant['inventory_item_id'];
		$variantCodeId = tableGetFieldWhere('variantcode', 'Id', 'Active=1 AND SKU="'.$variant['sku'].'"');
		$articleId = tableGetFieldWhere('variantcode', 'ArticleId', 'Active=1 AND SKU="'.$variant['sku'].'"');
		if((int)$variantCodeId > 0){
			
			$post = array(
				"ShopifyshopId" => 2,
				"VariantCodeId" => $variantCodeId,
				"ArticleId" => $articleId,
				"ShopifyVariantId" => $shopifyVariantId,
				"ShopifyProductId" => $shopifyProductId,
				"InventoryItemId" => $iteminventoryId,
			);
			tableWriteCron('shopifyvariant', $post);
		}
		else{
			echo $variant['id'].' '.$variant['sku'];
			echo '<br/>';
		}
	}
}
