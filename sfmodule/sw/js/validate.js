
function validate(form) {
    var reqClass = /(^|\s)required(\s|$)/;
    var reqValue = /^\s*$/;
    var elements = form.elements;
    var el;

    for (var i=0, iLen=elements.length; i<iLen; i++) {
        el = elements[i];

        if (reqClass.test(el.className) && reqValue.test(el.value)) {
            alert('Please fill required fields: ' + el.name);

            return false;
        }
    }

    return true;
}

