<?php
if (!ini_get('display_errors')) {
//    ini_set('display_errors', '1');
}

require       	"../../lib/config.inc" ;
require_once       _LEGACY_LIB."/lib/table.inc" ;
require_once       _LEGACY_LIB."/lib/db.inc" ;
require_once       _LEGACY_LIB."/lib/navigation.inc" ;
require_once      _LEGACY_LIB.'/lib/log.inc' ;

// Initialization
$Error = false ;
//list($usec, $sec) = explode(" ",microtime()) ;
//$startexec = (float)$usec + (float)$sec ;
$PermMap         = array ("r"=>"PermRead",       "w"=>"PermWrite",       "d"=>"PermDelete",      "c"=>"PermCreate") ;
$PermMapOther    = array ("r"=>"PermReadOther",  "w"=>"PermWriteOther",  "d"=>"PermDeleteOther", "c"=>"PermCreate") ;
$PermFields      = array ("PermRead", "PermWrite", "PermCreate", "PermDelete", "PermReadOther", "PermWriteOther", "PermDeleteOther") ;
$NavigationParam = array () ;


/*
  * ==================
  * Session check
  * =================
  */
$uid = filter_input(INPUT_COOKIE,COOKIE_NAME) ;
if (empty($uid)) {
    header("Location: "._LEGACY_URI);
    exit;
}

$query     = sprintf ("SELECT Id, CreateDate, IP FROM Session WHERE UID='%s'", $uid) ;
$result    = dbQuery ($query) ;
$Session   = dbFetch ($result) ;
dbQueryFree ($result);
if ((!$Session) || ($Session["IP"] != $_SERVER["REMOTE_ADDR"])) {
   echo 'You do not have permission to this route 1';
      die();
    header("Location: "._LEGACY_URI);
    exit;
}

/*
 * ==================
 * Login check
 * =================
 */
$query  = sprintf ("SELECT Login.* FROM (Login) WHERE SessionId=%d AND Active=1", $Session["Id"]) ;
$result = dbQuery ($query) ;
$Login  = dbFetch($result) ;
dbQueryFree ($result) ;
if ((!$Login) || ($Login && dbDateDecode($Login["AccessDate"]) < (time()-60*120))) {
   echo 'You do not have permission to this route 2';
      die();
    header("Location: "._LEGACY_URI);
    exit;
}


   /*
    * ==================
   * User Information
   * =================
   */
   $query = sprintf (
         "SELECT User.*, CONCAT(User.FirstName, ' ', User.LastName) AS FullName, Company.Name AS CompanyName, Company.Internal AS Internal, Company.Surplus AS CompanySurplus, Company.CurrencyId, TimeZone.Name AS TimeZone, Role.Name AS RoleName ".
         "FROM User ".
         "LEFT JOIN Company ON User.CompanyId=Company.Id ".
         "LEFT JOIN TimeZone ON User.TimeZoneId=TimeZone.Id ".
         "LEFT JOIN Role ON Role.Id=User.RoleId AND Role.Active=1 ".
         "WHERE User.Id=%d AND User.Active=1 AND User.Login=1", $Login["UserId"]
   );
   $result = dbQuery ($query) ;
   $User   = dbFetch($result) ;
   dbQueryFree ($result) ;
   if (!$User) {
      echo 'You do not have permission to this route 3';
      die();
        header("Location: "._LEGACY_URI);
        exit;
   }

// Timezone
if ($User["TimeZone"]) {
    putenv (sprintf("TZ=%s", $User["TimeZone"])) ;
    $_ENV["TZ"] = $User["TimeZone"] ;
}

   if(!$nid){
       $nid = filter_input(INPUT_POST,'nid');
   }
   if(!$nid || $nid < 0){
       $nid = filter_input(INPUT_GET,'nid');
   }
   if(!$nid || $nid < 0){
       $nid = 2244;
   }

 /*
    * ==================
    * Navigation reference
    * =================
    */
   $q = 'SELECT '.
         'Navigation.*, '.
         'NavigationType.Mark AS Type, NavigationType.LayoutRaw, NavigationType.LayoutMain, NavigationType.LayoutPopup, NavigationType.LayoutSubMenu, '.
         'Function.PermRecord, Function.PermPublic, Function.PermAdminSystem, Function.PermAdminProject, Function.PermProjectRole, Function.Mark AS FunctionMark '.
      'FROM (Navigation, NavigationType, Function) '.
      'WHERE %s AND Navigation.FunctionId=Function.Id AND Function.Active=1 AND Navigation.Active=1 AND Navigation.NavigationTypeId=NavigationType.Id' ;
   $query      = sprintf ($q, 'Navigation.Id='.$nid) ;
   $result     = dbQuery ($query) ;
   $Navigation = dbFetch ($result) ;
   dbQueryFree ($result);

   if ($Navigation['Type'] == 'link') {
      $p          = $Navigation ;
      $query      = sprintf ($q, 'Navigation.Mark="'.$Navigation['Function'].'"') ;
      $result     = dbQuery ($query) ;
      $Navigation = dbFetch ($result) ;
      dbQueryFree ($result);
     if ($Navigation) {
        if ($p['Header'] != '') {
           $Navigation['Header'] = $p['Header'] ;
        }
        if ($p['Parameters'] != '') {
           $Navigation['Parameters'] = $p['Parameters'] ;
        }
        $Navigation['MustLoad']     = $p['MustLoad'] ;
        $Navigation['RefIdInclude'] = $p['RefIdInclude'] ;
     }
   }

   if (!$Navigation) {
      logPrintf (logFATAL, "no module 1, nid %d\n", $nid) ;
      if ($Config['debugDump']) {
         logDump () ;
      }
      exit ;
   }
   
   if (strlen($Navigation["Module"]) == 0) {
      //search for the lowest default toolbar
      $def = searchDefaultToolbar($Navigation["Id"], (int)$_GET['DefaultNID']);
      if (isset($def)) {
         $Navigation["Module"]            = $def["Module"];
         $Navigation["Function"]          = $def["Function"];
         $Navigation["DefaultNID"]        = $def["Id"];
         $Navigation["DefaultLayoutRaw"]  = $def["LayoutRaw"];
         $Navigation["Header"]            = $def['Header'];
      }

      if (strlen($Navigation["Module"]) == 0) {
         logPrintf (logFATAL, "no module 2, nid %d\n", $nid) ;
         if ($Config['debugDump']) {
            logDump () ;
         }
         exit ;
      }
   }

   // Id
   $Id = (int)filter_input(INPUT_GET,"id");
   if (!$Id && ($Navigation["Type"] == "command")) {
      $Id = (int)filter_input(INPUT_POST,"id") ;
   } else {
      //$Id = -1 ;
   }

   /*
    * ==================
   * Load Permissions
   * =================
   */
   $roleid = $User['RoleId'] ;
   if ($roleid == 0) {
      $Error = "No Role selecetd for user" ;
   }
   $query = sprintf (
         "SELECT Permission.* ".
         "FROM Permission ".
         "WHERE Permission.RoleId=%d AND Permission.Active=1 AND Permission.FunctionId=%d",
         $roleid, $Navigation['FunctionId']
   ) ;
   $result     = dbQuery ($query) ;
   $Permission = dbFetch ($result) ;
   dbQueryFree ($result) ;
   if ($Permission['RoleId'] != $roleid) {
      $Permission['RoleId'] = $roleid ;
   }

   /*
    * ==================
   * Menu prepare
   * =================
   */
   
$_menu      = '';
$menu = array () ;
$child = array () ;
if ($Navigation["LayoutMain"]) {

   navigationItems ($menu, -1, "m") ;

   foreach ($menu as $row) {
      $child[((int)$row["ParentId"])][] = (int)$row["Id"] ;
   }

   $_menu = $Config['newLayout'] ? prepareMainMenuNew (0,0) : prepareMainMenu (0,0) ;
}

$User['CompanyName'] = tableGetField("Company", "Name", $User["CompanyId"]);
$_toolmenu = ($Config['newLayout'] ? prepareToolMenuNew() : prepareToolMenu());
unset ($child) ;
unset ($menu) ;

// Sub navigation
if ($Navigation["LayoutSubMenu"]) {
  include "module/".$Navigation["Module"]."/submenu.inc" ;
}

// Logout
if ($Navigation["Module"]=="main" && $Navigation["Function"]=='Logout-cmd') {

        // Terminate any active logins at this session
        $query = sprintf ("SELECT Id FROM Login WHERE SessionId=%d AND Active=1", $Session["Id"]) ;
        $result = dbQuery ($query) ;

        while ($row = dbFetch ($result)) {
            $query = sprintf ("UPDATE Login SET Active=0, LogoutDate='%s' WHERE Id=%d", dbDateEncode(time()), $row["Id"]) ;
            dbQuery ($query) ;
            logPrintf (logINFO, "logout, LoginId %d last access: %s, %s", $row["Id"], $Login["AccessDate"],$Login["Id"]) ; 
        }
        dbQueryFree ($result) ;
echo 'You do not have permission to this route 4';
      die();
        // Go to main page
        header("Location: "._LEGACY_URI);
        exit;
}
