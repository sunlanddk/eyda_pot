<?php

// list of ips allowed to use POT api
$allowedIpApi = array('94.143.10.156', '127.0.0.1', 'localhost', '::1',
'94.153.240.30', // Lvivtex
 '87.116.21.34', // PassOn, Ikast
 '87.48.50.111', // Turn Digital
 '185.17.218.26', // Vion
 '195.210.18.22', // Beumer
 '195.184.116.74', // ??
 '194.255.246.185', '62.198.134.31', '188.177.223.226', '62.198.132.254', // '212.97.250.144', // Self
 '13.93.112.97', '13.80.73.48'); // Webshop

// '212.130.116.86', '130.185.140.42', '176.20.225.242', Skybrud adresser
// '13.93.105.114','13.93.112.97','13.80.73.48', MS hosting (Skybrud)

$url = $_SERVER['REQUEST_URI'];

if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
    $ip = $_SERVER['HTTP_CLIENT_IP'];
} elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
    $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
} else {
    $ip = $_SERVER['REMOTE_ADDR'];
}
//if (strpos($url, '/api/')){var_dump($ip);exit;}

if (strpos($url, '/api/') === false OR strpos($url, '/shipping/') === false) {
	include 'legacyCheck.php';
}
else{
	if (!in_array($ip, $allowedIpApi)) {
        include 'legacyCheck.php';
		//echo 'You do not have permission to this route';
		//die();
	}
}



use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../app/bootstrap.php.cache';

// Enable APC for autoloading to improve performance.
// You should change the ApcClassLoader first argument to a unique prefix
// in order to prevent cache key conflicts with other applications
// also using APC.
/*
$apcLoader = new Symfony\Component\ClassLoader\ApcClassLoader(sha1(__FILE__), $loader);
$loader->unregister();
$apcLoader->register(true);
*/


$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);

/*
include 'legacyCheck.php';

use Symfony\Component\HttpFoundation\Request;

/**
 * @var Composer\Autoload\ClassLoader
 */
/*$loader = require __DIR__.'/../app/autoload.php';
include_once __DIR__.'/../app/bootstrap.php.cache';

// Enable APC for autoloading to improve performance.
// You should change the ApcClassLoader first argument to a unique prefix
// in order to prevent cache key conflicts with other applications
// also using APC.
/*
$apcLoader = new Symfony\Component\ClassLoader\ApcClassLoader(sha1(__FILE__), $loader);
$loader->unregister();
$apcLoader->register(true);
*/

/*$kernel = new AppKernel('prod', false);
$kernel->loadClassCache();
//$kernel = new AppCache($kernel);

// When using the HttpCache, you need to call the method in your front controller instead of relying on the configuration parameter
//Request::enableHttpMethodParameterOverride();
$request = Request::createFromGlobals();
$response = $kernel->handle($request);
$response->send();
$kernel->terminate($request, $response);
*/