<?php

namespace AppBundle\Model;

/**
 * Class Category
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Model
 */
class Category
{
    public function getCategoryByIdQuery($idsCat, $langs)
    {
        $ids = '';

        if (is_array($idsCat)) {
            foreach ($idsCat as $id) {
                $ids .= $id . ', ';
            }

            $ids = substr($ids, 0, -2);
        }

        return "SELECT acg.Id as ID, acgl.Name, acgl.Description, acg.Name as oldName, l.Culture as Lang, mc.id_magento as ID_Magento 
                FROM articlecategorygroup acg
                LEFT JOIN articlecategorygroup_lang acgl ON acgl.ArticleCategoryGroupId = acg.Id
                LEFT JOIN magento_category mc ON mc.id_pot = acg.Id
                LEFT JOIN language l ON l.id = acgl.LanguageId 
                WHERE acgl.ArticleCategoryGroupId=acg.id and (acgl.LanguageId IN ($langs) OR acgl.LanguageId is null) and acg.id IN($ids)";
    }

    public function mapCategory($idPot, $idMagento)
    {
        return "INSERT INTO magento_category(id_pot, id_magento) VALUES($idPot, $idMagento); ";
    }

    public function clearCategoryMap()
    {
        return "TRUNCATE TABLE magento_category";
    }

    public function getMaps()
    {
        return "SELECT id_pot, id_magento FROM magento_category GROUP BY id_pot";
    }

}
