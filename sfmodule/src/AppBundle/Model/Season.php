<?php

namespace AppBundle\Model;

/**
 * Class Season
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Model
 */
class Season
{
    public function getWebshopSeasonsQuery($webshop)
    {
        return "select id from season where webshopid= $webshop and done=0 AND active=1";
    }

    public function getWebshopStocksQuery($webshop)
    {
        return "select pickstockid, fromstockid from season where webshopid= $webshop and done=0 AND active=1 GROUP BY pickstockid, fromstockid";
    }
}
