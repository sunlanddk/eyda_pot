<?php

namespace AppBundle\Model;

/**
 * Class Product
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Model
 */
class Product
{
    /**
     * Method return string query for getting product using in POT system.
     *
     * @return string
     */
    public function getWebProductsQuery($languages, $currencies, $webshop, $season = null)
    {
        $query = "SELECT `cm`.`id` AS `ID`, a.id AS id_product,
					concat(`a`.`Number`,'_',co.Number) AS `PRODUCTID`,
					(SELECT acg.id as ID FROM articlecategory ac, articlecategorygroup acg where ac.articlecategorygroupid=acg.id and Category=substring(a.number,2,3))  as `CATID`,
					if((isnull(`a`.`DeliveryComment`) or (`a`.`DeliveryComment` = _utf8'')),`a`.`Description`,`a`.`DeliveryComment`) AS `DESCRIPTION`,
					`cmp`.`RetailPrice` AS `PRICE`,
					0 AS `STOCK`,0 AS `FRONTPAGE`,0 AS `NEW`,

					ac.id as ArticleColorId, co.Description as ColorName, sl.Name as SexName, `cmp`.`CampaignPrice` AS `CAMPAIGN_PRICE`, size.id as SIZE, vc.VariantCode, cm.WebFrontpage as front_page,
					cm.WebFitGroup, cm.WebCampaign, size.Name AS SIZE_NAME, se.id AS SEASON_ID, co.id as COLOR_ID, co.Number AS COLOR_NUMBER, `a`.`Number` AS PRODUCT_NUMBER,
					cmp.CurrencyId AS CurrencyId, cur.Name as CurrencyName, sl.SexId AS SexId,
					artlang.Description as article_title_lang, artlang.WebDescription as article_desc_lang, l.Culture AS article_lang, b.Name as brand_name, bl.Name as brand_lang_name, b.id AS brand_id
					FROM `collection` `c`
                        JOIN `collectionmember` `cm` ON `c`.`id` = `cm`.`CollectionId`
                        JOIN `collectionmemberprice` `cmp` ON `cmp`.`CollectionMemberId` = `cm`.`id`
                        JOIN `article` `a` ON `cm`.`ArticleId` = `a`.`Id`
                        JOIN `articlecolor` `ac` ON ac.id = cm.articlecolorid
                        JOIN `articlesize` `size` ON size.ArticleId = a.Id
                        JOIN `sex` `s` ON ac.sexid = s.id
                        JOIN `sex_lang` `sl` ON sl.SexId = s.id
                        JOIN `color` `co` ON co.id = ac.colorid
                        JOIN variantcode vc ON (vc.ArticleId = a.id AND vc.ArticleColorId = ac.id AND vc.ArticleSizeId = size.id)
                        JOIN season se ON c.SeasonId = se.id
                        JOIN currency cur ON cur.id = cmp.CurrencyId
                        LEFT JOIN article_lang artlang ON artlang.ArticleId = a.id
                        LEFT JOIN language l ON artlang.LanguageId = l.id
                        LEFT JOIN brand b ON c.brandid = b.id
                        LEFT JOIN brand_lang bl ON b.id = bl.brandId
                        
				    WHERE `c`.`Active` = 1 
                        AND `cm`.`notonwebshop` = 0
                        AND se.WebshopId = $webshop
                        AND `cm`.`Active` = 1 
                        AND `cmp`.`Active` = 1 
                        AND `cmp`.`CurrencyId` IN ($currencies) 
						AND `cmp`.`RetailPrice` > 0
                        AND sl.LanguageId IN ($languages)
                        AND vc.active = 1
                        AND `cm`.`Cancel` = 0 ";

        if (null !== $season) {
            $query .= ' AND se.id = ' . $season;
        }

        $query .= ' ORDER BY a.Id ';

        return $query;
    }

    /**
     * Method return string query for getting product using in POT system.
     *
     * @return string
     */
    public function getAllProductsQuery($languages, $currencies, $webshop, $season = null)
    {
        $query = "SELECT 
                    a.Number as ArticleNumber,
                    a.Description as ArticleName,
                    a.Id as ArticleId,
                    co.Number as ColorNumber,
                    artlang.WebDescription as article_desc_lang, 
                    artlang.Description as article_name_lang,
                    artlang.LanguageId as lang_id,
                    size.name as SizeName,
                    vc. variantcode as ean,
                    cm.id as CollectionMemberId,
                    cm.WebFrontPage as WebFrontPage,
                    ac.Id as ArticleColorId,
                    size.id as ArticleSizeId,
                    b.Name as brand_name,
                    c.SeasonId as season_id,
                    c.id as collection_id,
                    c.Name as collectionName,
                    co.Description as color_name,
                    co.Number AS colorNumber,
                    co.Id AS colorId,
                    s.Id as genderId

                    FROM season se
                        JOIN `collection` `c` On c.SeasonId = se.id AND `c`.`Active` = 1
                        JOIN `collectionmember` `cm` ON `c`.`id` = `cm`.`CollectionId` AND `cm`.`Cancel` = 0 AND `cm`.`notonwebshop` = 0 AND `cm`.`Active` = 1 
                        JOIN `article` `a` ON  `a`.`Id` = `cm`.`ArticleId`
                        JOIN `articlecolor` `ac` ON ac.id = cm.articlecolorid
                        JOIN `articlesize` `size` ON size.ArticleId = a.Id and size.active=1
                        JOIN `color` `co` ON co.id = ac.colorid
                        JOIN variantcode vc ON (vc.ArticleId = a.id AND vc.ArticleColorId = ac.id AND vc.ArticleSizeId = size.id) AND vc.active = 1
                        LEFT JOIN article_lang artlang ON artlang.ArticleId = a.id
                        LEFT JOIN brand b ON c.brandid = b.id
                        LEFT JOIN sex s ON ac.SexId = se.Id
                        

                    WHERE se.WebshopId = $webshop 
                    ";

        if (null !== $season) {
            $query .= ' AND se.id = ' . $season;
        }

        $query .= ' ORDER BY a.Number, co.Number, size.displayorder ';

        return $query;
    }

    /**
     * Method return string query for getting a single product using in POT system.
     *
     * @return string
     */
    public function getSingleProductsQuery($languages, $currencies, $webshop, $season = null, $collectionMemberId)
    {
        $query = "SELECT 
                    article.Number as ArticleNumber,
                    article.Description as ArticleName,
                    article.Id as ArticleId,
                    articlecolor.Id as ArticleColorId,
                    color.Number as ColorNumber,
                    color.Description as color_name,
                    color.Id AS colorId,
					colorgroup.ValueRed AS ValueRed,
					colorgroup.ValueGreen AS ValueGreen,
					colorgroup.ValueBlue AS ValueBlue,
                    articlesize.name as SizeName,
                    articlesize.id as ArticleSizeId,
                    variantcode.variantcode as ean,
                    collectionmember.id as CollectionMemberId,
                    collectionmember.WebFrontPage as WebFrontPage,
                    collectionmember.WebFitGroup as WebFitGroup,
                    brand.Name as brand_name,
                    collection.SeasonId as season_id,
                    collection.Id as collection_id,
                    collection.Name as collectionName,
                    collection.RetailDiscount as RetailDiscount,
                    company.Id as store_code,
                    company.Name as store_partner,
                    country.Name as country_of_manufacture,
                    sex.Id as genderId,
                    article_lang.LanguageId as langId,
                    article_lang.Description as langName,
                    article_lang.WebDescription as langDesc,
                    article_lang.onWebshop as onWebshop,
                    language.Culture as langCode
                        
                FROM article
                    JOIN collectionmember ON collectionmember.ArticleId=article.Id 
                    JOIN collection ON collectionmember.CollectionId=collection.Id
                    JOIN articlecolor ON articlecolor.id = collectionmember.ArticleColorId
                    JOIN color ON color.id=articlecolor.ColorId
                    JOIN articlesize ON articlesize.ArticleId = article.Id and articlesize.active=1
                    JOIN variantcode ON (variantcode.ArticleId = article.id AND variantcode.ArticleColorId = articlecolor.id AND variantcode.ArticleSizeId = articlesize.id) AND variantcode.active = 1
					LEFT JOIN colorgroup ON colorgroup.id=color.colorgroupid
                    LEFT JOIN sex ON articlecolor.SexId = sex.Id
                    LEFT JOIN article_lang ON article_lang.ArticleId = article.id AND article_lang.LanguageId IN ($languages)
                    LEFT JOIN brand ON collection.brandid = brand.id
                    LEFT JOIN company ON company.Id = article.SupplierCompanyId
                    LEFT JOIN country ON country.ID = company.CountryId
                    LEFT JOIN language ON language.Id = article_lang.LanguageId

                    WHERE collectionmember.id = $collectionMemberId AND collectionmember.notonwebshop = 0

                    ";
                    // AND sex.Id = articlecolor.SexId
                        // AND collectionmember.notonwebshop = 0

        if (null !== $season) {
            // $query .= ' AND se.id = ' . $season;
        }

        // $query .= ' ORDER BY a.Number, co.Number, size.displayorder ';

        return $query;
    }

    /**
     * Method return string query for getting crosssell products using in POT system.
     *
     * @return string
     */
    public function getCrosssellProductsQuery($languages, $currencies, $webshop, $season = null, $crosssell, $collectionmemberid)
    {
        $query = "SELECT 
                    article.Number as articleId, 
                    color.Number as colorNumber
                    
                FROM article
                    INNER JOIN collectionmember ON collectionmember.ArticleId=article.Id  and collectionmember.Active=1 
                    INNER JOIN articlecolor ON articlecolor.id=collectionmember.ArticleColorId
                    INNER JOIN color ON color.id=articlecolor.ColorId
                WHERE collectionmember.WebFitGroup = $crosssell and collectionmember.id<>$collectionmemberid 
                ";


        return $query;
    }

    /**
     * Method return string query for getting products by season using in POT system.
     *
     * @return string
     */
    public function getProductsBySeasonQuery($languages, $seasonid)
    {
        $query = "
            SELECT 
                article.Number as ArticleNumber,
                article.Description as ArticleName,
                article.Id as ArticleId,
                articlecolor.Id as ArticleColorId,
                color.Number as ColorNumber,
                color.Description as color_name,
                color.Id AS colorId,
                colorgroup.ValueRed AS ValueRed,
                colorgroup.ValueGreen AS ValueGreen,
                colorgroup.ValueBlue AS ValueBlue,
                articlesize.name as SizeName,
                articlesize.id as ArticleSizeId,
                variantcode.variantcode as ean,
                collectionmember.id as CollectionMemberId,
                collectionmember.WebFrontPage as WebFrontPage,
                collectionmember.WebFitGroup as WebFitGroup,
                brand.Name as brand_name,
                collection.SeasonId as season_id,
                collection.Id as collection_id,
                collection.Name as collectionName,
                collection.RetailDiscount as RetailDiscount,
                company.Id as store_code,
                company.Name as store_partner,
                country.Name as country_of_manufacture,
                sex.Id as genderId,
                article_lang.LanguageId as langId,
                article_lang.Description as langName,
                article_lang.WebDescription as langDesc,
                article_lang.onWebshop as onWebshop,
                language.Culture as langCode
                    
            FROM article
                JOIN collectionmember ON collectionmember.ArticleId=article.Id  and collectionmember.Active=1 
                JOIN collection ON collectionmember.CollectionId=collection.Id
                JOIN articlecolor ON articlecolor.id = collectionmember.ArticleColorId
                JOIN color ON color.id=articlecolor.ColorId
                JOIN articlesize ON articlesize.ArticleId = article.Id and articlesize.active=1
                JOIN variantcode ON (variantcode.ArticleId = article.id AND variantcode.ArticleColorId = articlecolor.id AND variantcode.ArticleSizeId = articlesize.id) AND variantcode.active = 1
                LEFT JOIN colorgroup ON colorgroup.id=color.colorgroupid
                LEFT JOIN sex ON articlecolor.SexId = sex.Id
                LEFT JOIN article_lang ON article_lang.ArticleId = article.id AND article_lang.LanguageId IN ($languages)
                LEFT JOIN brand ON collection.brandid = brand.id
                LEFT JOIN company ON company.Id = article.SupplierCompanyId
                LEFT JOIN country ON country.ID = company.CountryId
                LEFT JOIN language ON language.Id = article_lang.LanguageId
                        
            WHERE collection.SeasonId = $seasonid AND collectionmember.notonwebshop = 0
        ";

        return $query;
    }

    /**
     * Method return string query for getting color name in all languages in POT system.
     *
     * @return string
     */
    public function getColorNameQuery($languages,$variantCode)
    {
        $query = "SELECT 
                    cl.description,
                    cl.LanguageId,
					language.Culture as langCode
    
                FROM 
                    (variantcode vc, 
                    articlecolor ac, 
                    color c, 
                    color_lang cl) 

				LEFT JOIN language ON language.Id = cl.LanguageId
    
                WHERE vc.variantcode = '". $variantCode . "'
                    AND vc.active = 1 
                    AND ac.id = vc.articlecolorid 
                    AND c.id = ac.colorid 
                    AND cl.colorid = c.id
					 AND cl.LanguageId IN ($languages)
                ";


        return $query;
    }

    /**
     * Method return string query for getting color RGB codes for collectionmember.
     *
     * @return string
     */
    public function getColorRGBQuery($collectionmemberid)
    {
        $query = "SELECT 
                    cg.ValueRed,
                    cg.ValueGreen,
                    cg.ValueBlue
    
                FROM 
                    (collectionmember cm,
					articlecolor ac, 
                    color c, 
                    colorgroup cg) 
    
                WHERE cm.id = $collectionmemberid
					AND ac.articleid = cm.articlecolorid 
                    AND c.id = ac.colorid 
                    AND cg.id = c.colorgroupid
                ";


        return $query;
    }
    /**
     * Method return string query for getting gender names in all languages in POT system.
     *
     * @return string
     */
    public function getGenderNameQuery($languages, $genderId)
    {
        $query = "SELECT 
                    sl.Name,
                    sl.LanguageId,
					language.Culture as langCode
    
                FROM 
                    (sex_lang sl) 

				LEFT JOIN language ON language.Id = sl.LanguageId
    
                WHERE sl.sexid = $genderId AND sl.LanguageId IN ($languages)

                ";

        return $query;
    }
    /**
     * Method return string query for getting category names in all languages in POT system.
     *
     * @return string
     */
    public function getCategoryNameQuery($languages, $articleNumber, $categoryProcedure)
    {
		if (1==$categoryProcedure) { // CategoryGroup is property on article
			$query = sprintf("Select aacg.id as cat_Id, 
								acg.Name as Name,
								acgl.Name as lang_name, 
								acgl.Description as lang_description, 
								acgl.LanguageId,
								language.Culture as langCode
							From (article a, articlearticlecategorygroup aacg, articlecategorygroup acg, articlecategorygroup_lang acgl) 
							LEFT JOIN language ON language.Id = acgl.LanguageId
							Where a.number='%s' and aacg.ArticleId=a.id AND aacg.Active=1 AND acg.id=aacg.articlecategorygroupid AND acgl.ArticleCategoryGroupId=acg.id and acgl.LanguageId IN (%s)
							Order By acg.DisplayOrder", $articleNumber,$languages) ;
		} else { // Category is part off articlenumber
			$query = "SELECT 
						acg.id as cat_Id, 
						acg.Name as Name, 
						acgl.Name as lang_name, 
						acgl.LanguageId
					FROM 
						articlecategory ac, 
						articlecategorygroup acg,
						articlecategorygroup_lang acgl
					where 
						ac.articlecategorygroupid = acg.id 
						AND acg.id = acgl.ArticleCategoryGroupId
						AND ac.Category=substring($articleNumber,2,3)
					";
		}

        return $query;
    }

    /**
     * Method return string query for getting products in a season using in POT system.
     *
     * @return string
     */
    public function getSeasonProductsQuery($languages, $currencies, $webshop, $season = null)
    {
        $query = "SELECT 
                    article.Number as articleId, 
                    color.Number as colorNumber
                    
                FROM article
                    INNER JOIN collectionmember ON collectionmember.ArticleId=article.Id
                    INNER JOIN articlecolor ON articlecolor.id=collectionmember.ArticleColorId
                    INNER JOIN color ON color.id=articlecolor.ColorId
                    LEFT JOIN sex ON articlecolor.SexId = sex.Id
                WHERE collectionmember.WebFitGroup = $crosssell
                    AND sex.Id = articlecolor.SexId
                ";


        return $query;
    }

    /**
     * Method return string query for getting product prices using in POT system.
     *
     * @return string
     */
    public function getPriceSizeQuery($currency_ids, $collectionMemberId, $articleSizeId)
    {
        // $query = "SELECT c.name as CurrencyName, cmp.WholesalePrice  as WholesalePrice, cmp.RetailPrice  as RetailPrice
        //             from (collectionmember cm, collectionmemberprice cmp, currency c)
        //             where cm.id=$collectionmemberid 
        //             and cmp.collectionmemberid=cm.id 
        //             and cmp.currencyid in ($currency_ids) 
        //             and c.id=cmp.currencyid
        //             and cmp.active=1" ;

        $query = "SELECT c.name as CurrencyName, cmps.WholesalePrice  as WholesalePrice, cmps.RetailPrice  as RetailPrice,  cmps.CampaignPrice  as CampaignPrice
                    from (collectionmember cm, collectionmemberprice cmp,collectionmemberpricesize cmps, currency c)
                    where cm.id=$collectionMemberId and cmp.collectionmemberid=cm.id and cmp.currencyid in ($currency_ids) 
                    and cmps.collectionmemberpriceid=cmp.id and cmps.articlesizeid=$articleSizeId
                    and c.id=cmp.currencyid
                    and cmp.active=1 and cmps.active=1";

            /*$query = "SELECT cm.id as collectionMemberId, c.name as CurrencyName, cmp.WholesalePrice as WholesalePrice, cmp.RetailPrice as RetailPrice 
            from (collectionmember cm, collectionmemberprice cmp, currency c)
             where cmp.collectionmemberid=cm.id 
             and cmp.currencyid in ($currency_ids)  
             and c.id=cmp.currencyid 
             and cmp.active=1";*/

        return $query;
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getProductInventoryQuantityQuery($articleId, $articleColorId, $articleSizeId, $webshop, $stockIds)
    {
        $ids = $this->transformArrayToIds($stockIds, 'pickstockid|fromstockid');
		
		if($ids == ''){
			return '';
		}
		else{
			return "SELECT  cm.articleid, cm.articlecolorid, i.articlesizeid, sum(quantity) as quantity
					FROM (Season se, Collection co, collectionmember cm)
					INNER JOIN item i ON i.articleid=cm.articleid and  i.articlecolorid=cm.articlecolorid and i.active=1
					INNER JOIN container c ON i.containerid=c.id and c.stockid in ($ids) and c.active=1
					INNER JOIN articlesize az ON az.id=i.articlesizeid
					WHERE se.WebshopId= $webshop and se.done=0 and se.active=1
						and cm.articleid = $articleId and cm.articlecolorid = $articleColorId
						and i.articlesizeid = $articleSizeId
						and co.seasonid=se.id and co.active=1
						and cm.collectionid=co.id and cm.notonwebshop=0 and cm.active=1
						GROUP BY cm.articleid, cm.articlecolorid, i.articlesizeid
					 ";
		}
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getProductOrderQuantityQuery($articleId, $articleColorId, $articleSizeId)
    {
        /*return "SELECT ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 left join collectionlot cl ON cl.Active=1 and cl.id=ol.LotId and ol.LotId>0
                 where o.active=1 and o.ready=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
                       and (isnull(cl.id) or (cl.id>0 and cl.presale=0 and (select count(id) from requisitionline rl where rl.Active=1 and rl.Done=0 and rl.LotId=cl.id)>0) )
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid"  ;*/
		// Also counting draft orders. removed "and o.ready=1" from whwere clause
        return "select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 left join collection c on c.seasonid=o.seasonid
                 left join collectionmember cm on cm.collectionid=c.id and cm.articleid=ol.articleid and cm.articlecolorid=ol.articlecolorid
                 left join collectionlot cl ON cl.Active=1 and cl.id=cm.CollectionLOTId
                 where o.active=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
					   and cm.notonwebshop = 0
                       and (isnull(cl.id) or cl.id=0 or (cl.id>0 and cl.presale=0 and (select count(id) from requisitionline rl where rl.Active=1 and rl.Done=0 and rl.LotId=cl.id)=0) )
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid";
/*
        return "select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 where o.active=1 and o.ready=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid
                 ";
*/
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getAvailableUrlShops($articleId, $articleColorId, $webshop)
    {
        return "select ol.articlecolorid, c.Name, c.webshop, c.web, co.Name as Country 
                from (season se, orderline ol, `order` o, company c, country co) where se.WebshopId=$webshop and o.seasonid=se.id and  ol.orderid=o.id and o.companyid=c.id
         and o.active=1 and ol.active=1 and ol.articleid=$articleId and ol.articlecolorid=$articleColorId and co.id=c.countryid
         and c.active=1 and c.ListHidden=0 and c.webshop=1 group by c.id, ol.articlecolorid";
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getVariantOrdersQuery(array $variants)
    {
        $vids = '';

        foreach ($variants as $id) {
            $vids .= $id . ', ';
        }

        $vids = substr($vids, 0, -2);

        $query = "SELECT vc.VariantCode, ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
        FROM variantcode vc 
        JOIN orderline ol ON ol.ArticleId = vc.ArticleId AND ol.ArticleColorId = vc.ArticleColorId 
        JOIN `order` o ON ol.orderId = o.id and o.active=1 and o.done=0
        JOIN orderquantity oq ON oq.OrderLineId = ol.id and oq.articlesizeid=vc.articlesizeid
        WHERE ol.active=1 and ol.done=0 and oq.active=1 and vc.VariantCode IN ($vids) GROUP BY vc.VariantCode; ";

        return $query;


        return "select ol.articleid, ol.articlecolorid, oq.articlesizeid, sum(oq.quantity) as quantity 
                 from (`order` o, orderline ol, orderquantity oq)
                 where o.active=1 and o.ready=1 and o.done=0  
                       and ol.orderid=o.id and ol.active=1 and ol.done=0
                       and oq.orderlineid=ol.id and oq.active=1
                       and ol.articleid = $articleId and ol.articlecolorid = $articleColorId
                       and oq.articlesizeid = $articleSizeId
                       GROUP BY ol.articleid, ol.articlecolorid, oq.articlesizeid
                 ";
    }

    /**
     * {@inheritdoc}
     * @see at AppBundle\Service\Product
     */
    public function getVariantsQuery(array $variants, $webshop, $stockIds, $season = null) {

        $ids = $this->transformArrayToIds($stockIds, 'pickstockid|fromstockid');

        $vids = '';

        foreach ($variants as $id) {
            $vids .= $id . ', ';
        }

        $vids = substr($vids, 0, -2);

        $query = "SELECT vc.VariantCode, cm.articlecolorid, i.articlesizeid, sum(i.quantity) as quantity, mc.id_magento as colorMagento, ms.id_magento as sexMagento, col.id as colorid, msi.id_magento as sizeMagento, mb.id_magento as brandMagento FROM variantcode vc
                JOIN collectionmember cm ON cm.ArticleId = vc.ArticleId AND cm.ArticleColorId = vc.ArticleColorId
                JOIN collection c ON cm.CollectionId = c.id
                JOIN season se ON se.id = c.SeasonId
                JOIN articlecolor ac ON ac.id = vc.ArticleColorId
                JOIN articlesize asize ON asize.id = vc.ArticleSizeId
                JOIN color col ON col.id = ac.ColorId
                LEFT JOIN magento_brand mb ON mb.id_pot = c.brandId
                LEFT JOIN magento_color mc ON mc.id_pot = col.id
                LEFT JOIN magento_sex ms ON ms.id_pot = ac.sexId
                LEFT JOIN magento_size msi ON msi.id_pot = asize.id
                LEFT JOIN item i ON i.articleId = vc.ArticleId and i.articlecolorid = vc.ArticleColorId and i.articlesizeid = vc.ArticleSizeId
                LEFT JOIN container co ON i.containerid = co.id and co.stockid in ($ids) and co.active = 1
                WHERE se.WebshopId = $webshop and se.done = 0 and se.active = 1 and c.active = 1 and cm.notonwebshop = 0 and cm.active = 1
                and vc.VariantCode IN ($vids) GROUP BY vc.VariantCode ";

        if ($season) {
            $query .= " and se.id = $season ";
        }

        $query .= ";"; //end query

        return $query;
    }

    private function transformArrayToIds(array $stocks, $fieldName)
    {
        $ids = '';
        $names = explode('|', $fieldName);

        foreach ($stocks as $stock) {
            $ids .= $stock[$names[0]] . ', ';

            if (isset($names[1])) {
                $ids .= $stock[$names[1]] . ', ';
            }
        }

        return substr($ids, 0, -2);
    }
}
