<?php

namespace AppBundle\Model;

/**
 * Class Language
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Model
 */
class Language
{
    public function getSexLangsQuery($id)
    {
        return "SELECT sl.Name, l.Culture, l.Description FROM sex_lang sl JOIN language l ON sl.LanguageId = l.id where SexId = $id";
    }

    public function getLanguages($ids)
    {
        return "SELECT l.Culture FROM language l WHERE l.id IN ($ids); ";
    }
}
