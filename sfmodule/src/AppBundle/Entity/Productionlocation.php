<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Productionlocation
 */
class Productionlocation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var boolean
     */
    private $typesew;

    /**
     * @var integer
     */
    private $indexstart;

    /**
     * @var boolean
     */
    private $typeproduction;

    /**
     * @var boolean
     */
    private $typesample;

    /**
     * @var boolean
     */
    private $dobundles;

    /**
     * @var boolean
     */
    private $typeconstruction;

    /**
     * @var string
     */
    private $percentage;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Productionlocation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Productionlocation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Productionlocation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Productionlocation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Productionlocation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Productionlocation
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Productionlocation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Productionlocation
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set typesew
     *
     * @param boolean $typesew
     * @return Productionlocation
     */
    public function setTypesew($typesew)
    {
        $this->typesew = $typesew;

        return $this;
    }

    /**
     * Get typesew
     *
     * @return boolean 
     */
    public function getTypesew()
    {
        return $this->typesew;
    }

    /**
     * Set indexstart
     *
     * @param integer $indexstart
     * @return Productionlocation
     */
    public function setIndexstart($indexstart)
    {
        $this->indexstart = $indexstart;

        return $this;
    }

    /**
     * Get indexstart
     *
     * @return integer 
     */
    public function getIndexstart()
    {
        return $this->indexstart;
    }

    /**
     * Set typeproduction
     *
     * @param boolean $typeproduction
     * @return Productionlocation
     */
    public function setTypeproduction($typeproduction)
    {
        $this->typeproduction = $typeproduction;

        return $this;
    }

    /**
     * Get typeproduction
     *
     * @return boolean 
     */
    public function getTypeproduction()
    {
        return $this->typeproduction;
    }

    /**
     * Set typesample
     *
     * @param boolean $typesample
     * @return Productionlocation
     */
    public function setTypesample($typesample)
    {
        $this->typesample = $typesample;

        return $this;
    }

    /**
     * Get typesample
     *
     * @return boolean 
     */
    public function getTypesample()
    {
        return $this->typesample;
    }

    /**
     * Set dobundles
     *
     * @param boolean $dobundles
     * @return Productionlocation
     */
    public function setDobundles($dobundles)
    {
        $this->dobundles = $dobundles;

        return $this;
    }

    /**
     * Get dobundles
     *
     * @return boolean 
     */
    public function getDobundles()
    {
        return $this->dobundles;
    }

    /**
     * Set typeconstruction
     *
     * @param boolean $typeconstruction
     * @return Productionlocation
     */
    public function setTypeconstruction($typeconstruction)
    {
        $this->typeconstruction = $typeconstruction;

        return $this;
    }

    /**
     * Get typeconstruction
     *
     * @return boolean 
     */
    public function getTypeconstruction()
    {
        return $this->typeconstruction;
    }

    /**
     * Set percentage
     *
     * @param string $percentage
     * @return Productionlocation
     */
    public function setPercentage($percentage)
    {
        $this->percentage = $percentage;

        return $this;
    }

    /**
     * Get percentage
     *
     * @return string 
     */
    public function getPercentage()
    {
        return $this->percentage;
    }
}
