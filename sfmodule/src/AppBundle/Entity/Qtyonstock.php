<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Qtyonstock
 */
class Qtyonstock
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $quantity;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Qtyonstock
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Qtyonstock
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Qtyonstock
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
