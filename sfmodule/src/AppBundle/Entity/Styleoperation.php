<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Styleoperation
 */
class Styleoperation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $no;

    /**
     * @var string
     */
    private $productionminutes;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var integer
     */
    private $operationid;

    /**
     * @var integer
     */
    private $machinegroupid;

    /**
     * @var string
     */
    private $bundletext;

    /**
     * @var integer
     */
    private $styleoperationsversionid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Styleoperation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Styleoperation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Styleoperation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Styleoperation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Styleoperation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Styleoperation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set no
     *
     * @param integer $no
     * @return Styleoperation
     */
    public function setNo($no)
    {
        $this->no = $no;

        return $this;
    }

    /**
     * Get no
     *
     * @return integer 
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * Set productionminutes
     *
     * @param string $productionminutes
     * @return Styleoperation
     */
    public function setProductionminutes($productionminutes)
    {
        $this->productionminutes = $productionminutes;

        return $this;
    }

    /**
     * Get productionminutes
     *
     * @return string 
     */
    public function getProductionminutes()
    {
        return $this->productionminutes;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Styleoperation
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set operationid
     *
     * @param integer $operationid
     * @return Styleoperation
     */
    public function setOperationid($operationid)
    {
        $this->operationid = $operationid;

        return $this;
    }

    /**
     * Get operationid
     *
     * @return integer 
     */
    public function getOperationid()
    {
        return $this->operationid;
    }

    /**
     * Set machinegroupid
     *
     * @param integer $machinegroupid
     * @return Styleoperation
     */
    public function setMachinegroupid($machinegroupid)
    {
        $this->machinegroupid = $machinegroupid;

        return $this;
    }

    /**
     * Get machinegroupid
     *
     * @return integer 
     */
    public function getMachinegroupid()
    {
        return $this->machinegroupid;
    }

    /**
     * Set bundletext
     *
     * @param string $bundletext
     * @return Styleoperation
     */
    public function setBundletext($bundletext)
    {
        $this->bundletext = $bundletext;

        return $this;
    }

    /**
     * Get bundletext
     *
     * @return string 
     */
    public function getBundletext()
    {
        return $this->bundletext;
    }

    /**
     * Set styleoperationsversionid
     *
     * @param integer $styleoperationsversionid
     * @return Styleoperation
     */
    public function setStyleoperationsversionid($styleoperationsversionid)
    {
        $this->styleoperationsversionid = $styleoperationsversionid;

        return $this;
    }

    /**
     * Get styleoperationsversionid
     *
     * @return integer 
     */
    public function getStyleoperationsversionid()
    {
        return $this->styleoperationsversionid;
    }
}
