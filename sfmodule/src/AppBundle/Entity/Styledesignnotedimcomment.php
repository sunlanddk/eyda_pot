<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Styledesignnotedimcomment
 */
class Styledesignnotedimcomment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $styledesignnoteid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Styledesignnotedimcomment
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Styledesignnotedimcomment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set styledesignnoteid
     *
     * @param integer $styledesignnoteid
     * @return Styledesignnotedimcomment
     */
    public function setStyledesignnoteid($styledesignnoteid)
    {
        $this->styledesignnoteid = $styledesignnoteid;

        return $this;
    }

    /**
     * Get styledesignnoteid
     *
     * @return integer 
     */
    public function getStyledesignnoteid()
    {
        return $this->styledesignnoteid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Styledesignnotedimcomment
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Styledesignnotedimcomment
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Styledesignnotedimcomment
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Styledesignnotedimcomment
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
