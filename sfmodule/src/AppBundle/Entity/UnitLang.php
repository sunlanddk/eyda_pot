<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * UnitLang
 */
class UnitLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $unitid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unitid
     *
     * @param integer $unitid
     * @return UnitLang
     */
    public function setUnitid($unitid)
    {
        $this->unitid = $unitid;

        return $this;
    }

    /**
     * Get unitid
     *
     * @return integer 
     */
    public function getUnitid()
    {
        return $this->unitid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return UnitLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return UnitLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return UnitLang
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }
}
