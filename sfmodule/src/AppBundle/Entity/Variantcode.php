<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Variantcode
 */
class Variantcode
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $variantcode;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $reference;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $articlecertificateid;

    /**
     * @var string
     */
    private $variantmodelref;

    /**
     * @var string
     */
    private $variantdescription;

    /**
     * @var string
     */
    private $variantcolordesc;

    /**
     * @var string
     */
    private $variantcolorcode;

    /**
     * @var string
     */
    private $variantsize;

    /**
     * @var string
     */
    private $variantunit;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var integer
     */
    private $pickcontainerid;

    /**
     * @var integer
     */
    private $replenishcontainerid;

    public function __construct()
    {
        $this->replenishcontainerid = 0;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set variantcode
     *
     * @param string $variantcode
     * @return Variantcode
     */
    public function setVariantcode($variantcode)
    {
        $this->variantcode = $variantcode;

        return $this;
    }

    /**
     * Get variantcode
     *
     * @return string 
     */
    public function getVariantcode()
    {
        return $this->variantcode;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Variantcode
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set reference
     *
     * @param integer $reference
     * @return Variantcode
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return integer 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Variantcode
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Variantcode
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Variantcode
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set articlecertificateid
     *
     * @param integer $articlecertificateid
     * @return Variantcode
     */
    public function setArticlecertificateid($articlecertificateid)
    {
        $this->articlecertificateid = $articlecertificateid;

        return $this;
    }

    /**
     * Get articlecertificateid
     *
     * @return integer 
     */
    public function getArticlecertificateid()
    {
        return $this->articlecertificateid;
    }

    /**
     * Set variantmodelref
     *
     * @param string $variantmodelref
     * @return Variantcode
     */
    public function setVariantmodelref($variantmodelref)
    {
        $this->variantmodelref = $variantmodelref;

        return $this;
    }

    /**
     * Get variantmodelref
     *
     * @return string 
     */
    public function getVariantmodelref()
    {
        return $this->variantmodelref;
    }

    /**
     * Set variantdescription
     *
     * @param string $variantdescription
     * @return Variantcode
     */
    public function setVariantdescription($variantdescription)
    {
        $this->variantdescription = $variantdescription;

        return $this;
    }

    /**
     * Get variantdescription
     *
     * @return string 
     */
    public function getVariantdescription()
    {
        return $this->variantdescription;
    }

    /**
     * Set variantcolordesc
     *
     * @param string $variantcolordesc
     * @return Variantcode
     */
    public function setVariantcolordesc($variantcolordesc)
    {
        $this->variantcolordesc = $variantcolordesc;

        return $this;
    }

    /**
     * Get variantcolordesc
     *
     * @return string 
     */
    public function getVariantcolordesc()
    {
        return $this->variantcolordesc;
    }

    /**
     * Set variantcolorcode
     *
     * @param string $variantcolorcode
     * @return Variantcode
     */
    public function setVariantcolorcode($variantcolorcode)
    {
        $this->variantcolorcode = $variantcolorcode;

        return $this;
    }

    /**
     * Get variantcolorcode
     *
     * @return string 
     */
    public function getVariantcolorcode()
    {
        return $this->variantcolorcode;
    }

    /**
     * Set variantsize
     *
     * @param string $variantsize
     * @return Variantcode
     */
    public function setVariantsize($variantsize)
    {
        $this->variantsize = $variantsize;

        return $this;
    }

    /**
     * Get variantsize
     *
     * @return string 
     */
    public function getVariantsize()
    {
        return $this->variantsize;
    }

    /**
     * Set variantunit
     *
     * @param string $variantunit
     * @return Variantcode
     */
    public function setVariantunit($variantunit)
    {
        $this->variantunit = $variantunit;

        return $this;
    }

    /**
     * Get variantunit
     *
     * @return string 
     */
    public function getVariantunit()
    {
        return $this->variantunit;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Variantcode
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Variantcode
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Variantcode
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Variantcode
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Variantcode
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set pickcontainerid
     *
     * @param integer $pickcontainerid
     * @return Variantcode
     */
    public function setPickcontainerid($pickcontainerid)
    {
        $this->pickcontainerid = $pickcontainerid;

        return $this;
    }

    /**
     * Get pickcontainerid
     *
     * @return integer 
     */
    public function getPickcontainerid()
    {
        return $this->pickcontainerid;
    }

    /**
     * Set replenishcontainerid
     *
     * @param integer $replenishcontainerid
     * @return Variantcode
     */
    public function setReplenishcontainerid($replenishcontainerid)
    {
        $this->replenishcontainerid = $replenishcontainerid;

        return $this;
    }

    /**
     * Get replenishcontainerid
     *
     * @return integer 
     */
    public function getReplenishcontainerid()
    {
        return $this->replenishcontainerid;
    }
}
