<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timelogginguserregistration
 */
class Timelogginguserregistration
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $timeloggingactivityid;

    /**
     * @var string
     */
    private $hours;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $typeid;

    /**
     * @var integer
     */
    private $reportingid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Timelogginguserregistration
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set timeloggingactivityid
     *
     * @param integer $timeloggingactivityid
     * @return Timelogginguserregistration
     */
    public function setTimeloggingactivityid($timeloggingactivityid)
    {
        $this->timeloggingactivityid = $timeloggingactivityid;

        return $this;
    }

    /**
     * Get timeloggingactivityid
     *
     * @return integer 
     */
    public function getTimeloggingactivityid()
    {
        return $this->timeloggingactivityid;
    }

    /**
     * Set hours
     *
     * @param string $hours
     * @return Timelogginguserregistration
     */
    public function setHours($hours)
    {
        $this->hours = $hours;

        return $this;
    }

    /**
     * Get hours
     *
     * @return string 
     */
    public function getHours()
    {
        return $this->hours;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Timelogginguserregistration
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set typeid
     *
     * @param integer $typeid
     * @return Timelogginguserregistration
     */
    public function setTypeid($typeid)
    {
        $this->typeid = $typeid;

        return $this;
    }

    /**
     * Get typeid
     *
     * @return integer 
     */
    public function getTypeid()
    {
        return $this->typeid;
    }

    /**
     * Set reportingid
     *
     * @param integer $reportingid
     * @return Timelogginguserregistration
     */
    public function setReportingid($reportingid)
    {
        $this->reportingid = $reportingid;

        return $this;
    }

    /**
     * Get reportingid
     *
     * @return integer 
     */
    public function getReportingid()
    {
        return $this->reportingid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Timelogginguserregistration
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Timelogginguserregistration
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Timelogginguserregistration
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Timelogginguserregistration
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
