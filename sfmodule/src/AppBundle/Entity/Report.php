<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Report
 */
class Report
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $executemodule;

    /**
     * @var integer
     */
    private $reportgroupid;

    /**
     * @var integer
     */
    private $limit;

    /**
     * @var integer
     */
    private $freezecolumn;

    /**
     * @var integer
     */
    private $fitwidth;

    /**
     * @var integer
     */
    private $fitheight;

    /**
     * @var boolean
     */
    private $landscape;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Report
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Report
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Report
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Report
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Report
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Report
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Report
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set executemodule
     *
     * @param boolean $executemodule
     * @return Report
     */
    public function setExecutemodule($executemodule)
    {
        $this->executemodule = $executemodule;

        return $this;
    }

    /**
     * Get executemodule
     *
     * @return boolean 
     */
    public function getExecutemodule()
    {
        return $this->executemodule;
    }

    /**
     * Set reportgroupid
     *
     * @param integer $reportgroupid
     * @return Report
     */
    public function setReportgroupid($reportgroupid)
    {
        $this->reportgroupid = $reportgroupid;

        return $this;
    }

    /**
     * Get reportgroupid
     *
     * @return integer 
     */
    public function getReportgroupid()
    {
        return $this->reportgroupid;
    }

    /**
     * Set limit
     *
     * @param integer $limit
     * @return Report
     */
    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    /**
     * Get limit
     *
     * @return integer 
     */
    public function getLimit()
    {
        return $this->limit;
    }

    /**
     * Set freezecolumn
     *
     * @param integer $freezecolumn
     * @return Report
     */
    public function setFreezecolumn($freezecolumn)
    {
        $this->freezecolumn = $freezecolumn;

        return $this;
    }

    /**
     * Get freezecolumn
     *
     * @return integer 
     */
    public function getFreezecolumn()
    {
        return $this->freezecolumn;
    }

    /**
     * Set fitwidth
     *
     * @param integer $fitwidth
     * @return Report
     */
    public function setFitwidth($fitwidth)
    {
        $this->fitwidth = $fitwidth;

        return $this;
    }

    /**
     * Get fitwidth
     *
     * @return integer 
     */
    public function getFitwidth()
    {
        return $this->fitwidth;
    }

    /**
     * Set fitheight
     *
     * @param integer $fitheight
     * @return Report
     */
    public function setFitheight($fitheight)
    {
        $this->fitheight = $fitheight;

        return $this;
    }

    /**
     * Get fitheight
     *
     * @return integer 
     */
    public function getFitheight()
    {
        return $this->fitheight;
    }

    /**
     * Set landscape
     *
     * @param boolean $landscape
     * @return Report
     */
    public function setLandscape($landscape)
    {
        $this->landscape = $landscape;

        return $this;
    }

    /**
     * Get landscape
     *
     * @return boolean 
     */
    public function getLandscape()
    {
        return $this->landscape;
    }
}
