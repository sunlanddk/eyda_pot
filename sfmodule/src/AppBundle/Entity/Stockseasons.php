<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stockseasons
 */
class Stockseasons
{
    /**
     * @var integer
     */
    private $stockid;

    /**
     * @var integer
     */
    private $seasonid;


    /**
     * Set stockid
     *
     * @param integer $stockid
     * @return Stockseasons
     */
    public function setStockid($stockid)
    {
        $this->stockid = $stockid;

        return $this;
    }

    /**
     * Get stockid
     *
     * @return integer 
     */
    public function getStockid()
    {
        return $this->stockid;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Stockseasons
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer 
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }
}
