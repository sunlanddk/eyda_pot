<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * MagentoColor
 *
 * @ORM\Table(name="magento_color", indexes={@ORM\Index(name="id_pot", columns={"id_pot", "id_magento"})})
 * @ORM\Entity
 */
class MagentoColor
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer", nullable=false)
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_pot", type="integer", nullable=false)
     */
    private $idPot;

    /**
     * @var integer
     *
     * @ORM\Column(name="id_magento", type="integer", nullable=false)
     */
    private $idMagento;



    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set idPot
     *
     * @param integer $idPot
     *
     * @return MagentoColor
     */
    public function setIdPot($idPot)
    {
        $this->idPot = $idPot;

        return $this;
    }

    /**
     * Get idPot
     *
     * @return integer
     */
    public function getIdPot()
    {
        return $this->idPot;
    }

    /**
     * Set idMagento
     *
     * @param integer $idMagento
     *
     * @return MagentoColor
     */
    public function setIdMagento($idMagento)
    {
        $this->idMagento = $idMagento;

        return $this;
    }

    /**
     * Get idMagento
     *
     * @return integer
     */
    public function getIdMagento()
    {
        return $this->idMagento;
    }
}
