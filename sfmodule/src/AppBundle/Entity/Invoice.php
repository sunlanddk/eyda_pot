<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoice
 */
class Invoice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var boolean
     */
    private $credit;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var integer
     */
    private $stockid;

    /**
     * @var integer
     */
    private $paymenttermid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var integer
     */
    private $salesuserid;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var string
     */
    private $invoiceheader;

    /**
     * @var string
     */
    private $invoicefooter;

    /**
     * @var \DateTime
     */
    private $duedate;

    /**
     * @var boolean
     */
    private $ready;

    /**
     * @var integer
     */
    private $readyuserid;

    /**
     * @var \DateTime
     */
    private $readydate;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var \DateTime
     */
    private $invoicedate;

    /**
     * @var \DateTime
     */
    private $duebasedate;

    /**
     * @var integer
     */
    private $accountingno;

    /**
     * @var string
     */
    private $companyfooter;

    /**
     * @var string
     */
    private $email;

    /**
     * @var integer
     */
    private $fromcompanyid;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var string
     */
    private $paidincash;

    /**
     * @var string
     */
    private $paidincard;

    /**
     * @var integer
     */
    private $invoicesummaryid;

    /**
     * @var string
     */
    private $currencyrate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Invoice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Invoice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Invoice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Invoice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Invoice
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Invoice
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set credit
     *
     * @param boolean $credit
     * @return Invoice
     */
    public function setCredit($credit)
    {
        $this->credit = $credit;

        return $this;
    }

    /**
     * Get credit
     *
     * @return boolean 
     */
    public function getCredit()
    {
        return $this->credit;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Invoice
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Invoice
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set stockid
     *
     * @param integer $stockid
     * @return Invoice
     */
    public function setStockid($stockid)
    {
        $this->stockid = $stockid;

        return $this;
    }

    /**
     * Get stockid
     *
     * @return integer 
     */
    public function getStockid()
    {
        return $this->stockid;
    }

    /**
     * Set paymenttermid
     *
     * @param integer $paymenttermid
     * @return Invoice
     */
    public function setPaymenttermid($paymenttermid)
    {
        $this->paymenttermid = $paymenttermid;

        return $this;
    }

    /**
     * Get paymenttermid
     *
     * @return integer 
     */
    public function getPaymenttermid()
    {
        return $this->paymenttermid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Invoice
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set salesuserid
     *
     * @param integer $salesuserid
     * @return Invoice
     */
    public function setSalesuserid($salesuserid)
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    /**
     * Get salesuserid
     *
     * @return integer 
     */
    public function getSalesuserid()
    {
        return $this->salesuserid;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Invoice
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set invoiceheader
     *
     * @param string $invoiceheader
     * @return Invoice
     */
    public function setInvoiceheader($invoiceheader)
    {
        $this->invoiceheader = $invoiceheader;

        return $this;
    }

    /**
     * Get invoiceheader
     *
     * @return string 
     */
    public function getInvoiceheader()
    {
        return $this->invoiceheader;
    }

    /**
     * Set invoicefooter
     *
     * @param string $invoicefooter
     * @return Invoice
     */
    public function setInvoicefooter($invoicefooter)
    {
        $this->invoicefooter = $invoicefooter;

        return $this;
    }

    /**
     * Get invoicefooter
     *
     * @return string 
     */
    public function getInvoicefooter()
    {
        return $this->invoicefooter;
    }

    /**
     * Set duedate
     *
     * @param \DateTime $duedate
     * @return Invoice
     */
    public function setDuedate($duedate)
    {
        $this->duedate = $duedate;

        return $this;
    }

    /**
     * Get duedate
     *
     * @return \DateTime 
     */
    public function getDuedate()
    {
        return $this->duedate;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     * @return Invoice
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean 
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set readyuserid
     *
     * @param integer $readyuserid
     * @return Invoice
     */
    public function setReadyuserid($readyuserid)
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    /**
     * Get readyuserid
     *
     * @return integer 
     */
    public function getReadyuserid()
    {
        return $this->readyuserid;
    }

    /**
     * Set readydate
     *
     * @param \DateTime $readydate
     * @return Invoice
     */
    public function setReadydate($readydate)
    {
        $this->readydate = $readydate;

        return $this;
    }

    /**
     * Get readydate
     *
     * @return \DateTime 
     */
    public function getReadydate()
    {
        return $this->readydate;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Invoice
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Invoice
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Invoice
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set invoicedate
     *
     * @param \DateTime $invoicedate
     * @return Invoice
     */
    public function setInvoicedate($invoicedate)
    {
        $this->invoicedate = $invoicedate;

        return $this;
    }

    /**
     * Get invoicedate
     *
     * @return \DateTime 
     */
    public function getInvoicedate()
    {
        return $this->invoicedate;
    }

    /**
     * Set duebasedate
     *
     * @param \DateTime $duebasedate
     * @return Invoice
     */
    public function setDuebasedate($duebasedate)
    {
        $this->duebasedate = $duebasedate;

        return $this;
    }

    /**
     * Get duebasedate
     *
     * @return \DateTime 
     */
    public function getDuebasedate()
    {
        return $this->duebasedate;
    }

    /**
     * Set accountingno
     *
     * @param integer $accountingno
     * @return Invoice
     */
    public function setAccountingno($accountingno)
    {
        $this->accountingno = $accountingno;

        return $this;
    }

    /**
     * Get accountingno
     *
     * @return integer 
     */
    public function getAccountingno()
    {
        return $this->accountingno;
    }

    /**
     * Set companyfooter
     *
     * @param string $companyfooter
     * @return Invoice
     */
    public function setCompanyfooter($companyfooter)
    {
        $this->companyfooter = $companyfooter;

        return $this;
    }

    /**
     * Get companyfooter
     *
     * @return string 
     */
    public function getCompanyfooter()
    {
        return $this->companyfooter;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return Invoice
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set fromcompanyid
     *
     * @param integer $fromcompanyid
     * @return Invoice
     */
    public function setFromcompanyid($fromcompanyid)
    {
        $this->fromcompanyid = $fromcompanyid;

        return $this;
    }

    /**
     * Get fromcompanyid
     *
     * @return integer 
     */
    public function getFromcompanyid()
    {
        return $this->fromcompanyid;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Invoice
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set paidincash
     *
     * @param string $paidincash
     * @return Invoice
     */
    public function setPaidincash($paidincash)
    {
        $this->paidincash = $paidincash;

        return $this;
    }

    /**
     * Get paidincash
     *
     * @return string 
     */
    public function getPaidincash()
    {
        return $this->paidincash;
    }

    /**
     * Set paidincard
     *
     * @param string $paidincard
     * @return Invoice
     */
    public function setPaidincard($paidincard)
    {
        $this->paidincard = $paidincard;

        return $this;
    }

    /**
     * Get paidincard
     *
     * @return string 
     */
    public function getPaidincard()
    {
        return $this->paidincard;
    }

    /**
     * Set invoicesummaryid
     *
     * @param integer $invoicesummaryid
     * @return Invoice
     */
    public function setInvoicesummaryid($invoicesummaryid)
    {
        $this->invoicesummaryid = $invoicesummaryid;

        return $this;
    }

    /**
     * Get invoicesummaryid
     *
     * @return integer 
     */
    public function getInvoicesummaryid()
    {
        return $this->invoicesummaryid;
    }

    /**
     * Set currencyrate
     *
     * @param string $currencyrate
     * @return Invoice
     */
    public function setCurrencyrate($currencyrate)
    {
        $this->currencyrate = $currencyrate;

        return $this;
    }

    /**
     * Get currencyrate
     *
     * @return string 
     */
    public function getCurrencyrate()
    {
        return $this->currencyrate;
    }
}
