<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;

/**
 * Carrier
 *
 * @@ExclusionPolicy("None")
 */
class Carrier
{
    /**
     * @var integer
     *
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Exclude
     */
    private $createdate;

    /**
     * @var integer
     *
     * @Exclude
     */
    private $createuserid;

    /**
     * @var \DateTime
     *
     * @Exclude
     */
    private $modifydate;

    /**
     * @var integer
     *
     * @Exclude
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     *
     * @Exclude
     */
    private $companyid;

    /**
     * @var string
     */
    private $transportborder;

    /**
     * @var string
     */
    private $transportlocal;

    /**
     * @var string
     */
    private $customslocation;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Carrier
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Carrier
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Carrier
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Carrier
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Carrier
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Carrier
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Carrier
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Carrier
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set transportborder
     *
     * @param string $transportborder
     * @return Carrier
     */
    public function setTransportborder($transportborder)
    {
        $this->transportborder = $transportborder;

        return $this;
    }

    /**
     * Get transportborder
     *
     * @return string 
     */
    public function getTransportborder()
    {
        return $this->transportborder;
    }

    /**
     * Set transportlocal
     *
     * @param string $transportlocal
     * @return Carrier
     */
    public function setTransportlocal($transportlocal)
    {
        $this->transportlocal = $transportlocal;

        return $this;
    }

    /**
     * Get transportlocal
     *
     * @return string 
     */
    public function getTransportlocal()
    {
        return $this->transportlocal;
    }

    /**
     * Set customslocation
     *
     * @param string $customslocation
     * @return Carrier
     */
    public function setCustomslocation($customslocation)
    {
        $this->customslocation = $customslocation;

        return $this;
    }

    /**
     * Get customslocation
     *
     * @return string 
     */
    public function getCustomslocation()
    {
        return $this->customslocation;
    }
}
