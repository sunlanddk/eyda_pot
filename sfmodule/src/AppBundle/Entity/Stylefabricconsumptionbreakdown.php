<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylefabricconsumptionbreakdown
 */
class Stylefabricconsumptionbreakdown
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stylefabricconsumptionid;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stylefabricconsumptionid
     *
     * @param integer $stylefabricconsumptionid
     * @return Stylefabricconsumptionbreakdown
     */
    public function setStylefabricconsumptionid($stylefabricconsumptionid)
    {
        $this->stylefabricconsumptionid = $stylefabricconsumptionid;

        return $this;
    }

    /**
     * Get stylefabricconsumptionid
     *
     * @return integer 
     */
    public function getStylefabricconsumptionid()
    {
        return $this->stylefabricconsumptionid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Stylefabricconsumptionbreakdown
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Stylefabricconsumptionbreakdown
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylefabricconsumptionbreakdown
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylefabricconsumptionbreakdown
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylefabricconsumptionbreakdown
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylefabricconsumptionbreakdown
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
