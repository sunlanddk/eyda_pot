<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ArticleLang
 */
class ArticleLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $webdescription;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return ArticleLang
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return ArticleLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return ArticleLang
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set webdescription
     *
     * @param string $webdescription
     * @return ArticleLang
     */
    public function setWebdescription($webdescription)
    {
        $this->webdescription = $webdescription;

        return $this;
    }

    /**
     * Get webdescription
     *
     * @return string 
     */
    public function getWebdescription()
    {
        return $this->webdescription;
    }
}
