<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pickorderline
 */
class Pickorderline
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $pickorderid;

    /**
     * @var integer
     */
    private $variantcodeid;

    /**
     * @var string
     */
    private $variantcode;

    /**
     * @var string
     */
    private $variantdescription;

    /**
     * @var string
     */
    private $variantcolor;

    /**
     * @var string
     */
    private $variantsize;

    /**
     * @var integer
     */
    private $orderedquantity;

    /**
     * @var integer
     */
    private $pickedquantity;

    /**
     * @var integer
     */
    private $packedquantity;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var integer
     */
    private $qlid;

    /**
     * @var integer
     */
    private $done;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set pickorderid
     *
     * @param integer $pickorderid
     * @return Pickorderline
     */
    public function setPickorderid($pickorderid)
    {
        $this->pickorderid = $pickorderid;

        return $this;
    }

    /**
     * Get pickorderid
     *
     * @return integer 
     */
    public function getPickorderid()
    {
        return $this->pickorderid;
    }

    /**
     * Set variantcodeid
     *
     * @param integer $variantcodeid
     * @return Pickorderline
     */
    public function setVariantcodeid($variantcodeid)
    {
        $this->variantcodeid = $variantcodeid;

        return $this;
    }

    /**
     * Get variantcodeid
     *
     * @return integer 
     */
    public function getVariantcodeid()
    {
        return $this->variantcodeid;
    }

    /**
     * Set variantcode
     *
     * @param string $variantcode
     * @return Pickorderline
     */
    public function setVariantcode($variantcode)
    {
        $this->variantcode = $variantcode;

        return $this;
    }

    /**
     * Get variantcode
     *
     * @return string 
     */
    public function getVariantcode()
    {
        return $this->variantcode;
    }

    /**
     * Set variantdescription
     *
     * @param string $variantdescription
     * @return Pickorderline
     */
    public function setVariantdescription($variantdescription)
    {
        $this->variantdescription = $variantdescription;

        return $this;
    }

    /**
     * Get variantdescription
     *
     * @return string 
     */
    public function getVariantdescription()
    {
        return $this->variantdescription;
    }

    /**
     * Set variantcolor
     *
     * @param string $variantcolor
     * @return Pickorderline
     */
    public function setVariantcolor($variantcolor)
    {
        $this->variantcolor = $variantcolor;

        return $this;
    }

    /**
     * Get variantcolor
     *
     * @return string 
     */
    public function getVariantcolor()
    {
        return $this->variantcolor;
    }

    /**
     * Set variantsize
     *
     * @param string $variantsize
     * @return Pickorderline
     */
    public function setVariantsize($variantsize)
    {
        $this->variantsize = $variantsize;

        return $this;
    }

    /**
     * Get variantsize
     *
     * @return string 
     */
    public function getVariantsize()
    {
        return $this->variantsize;
    }

    /**
     * Set orderedquantity
     *
     * @param integer $orderedquantity
     * @return Pickorderline
     */
    public function setOrderedquantity($orderedquantity)
    {
        $this->orderedquantity = $orderedquantity;

        return $this;
    }

    /**
     * Get orderedquantity
     *
     * @return integer 
     */
    public function getOrderedquantity()
    {
        return $this->orderedquantity;
    }

    /**
     * Set pickedquantity
     *
     * @param integer $pickedquantity
     * @return Pickorderline
     */
    public function setPickedquantity($pickedquantity)
    {
        $this->pickedquantity = $pickedquantity;

        return $this;
    }

    /**
     * Get pickedquantity
     *
     * @return integer 
     */
    public function getPickedquantity()
    {
        return $this->pickedquantity;
    }

    /**
     * Set packedquantity
     *
     * @param integer $packedquantity
     * @return Pickorderline
     */
    public function setPackedquantity($packedquantity)
    {
        $this->packedquantity = $packedquantity;

        return $this;
    }

    /**
     * Get packedquantity
     *
     * @return integer 
     */
    public function getPackedquantity()
    {
        return $this->packedquantity;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Pickorderline
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set qlid
     *
     * @param integer $qlid
     * @return Pickorderline
     */
    public function setQlid($qlid)
    {
        $this->qlid = $qlid;

        return $this;
    }

    /**
     * Get qlid
     *
     * @return integer 
     */
    public function getQlid()
    {
        return $this->qlid;
    }

    /**
     * Set done
     *
     * @param integer $done
     * @return Pickorderline
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return integer 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Pickorderline
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Pickorderline
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Pickorderline
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Pickorderline
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
