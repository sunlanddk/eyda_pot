<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pickstocks
 */
class Pickstocks
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stockid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stockid
     *
     * @param integer $stockid
     * @return Pickstocks
     */
    public function setStockid($stockid)
    {
        $this->stockid = $stockid;

        return $this;
    }

    /**
     * Get stockid
     *
     * @return integer 
     */
    public function getStockid()
    {
        return $this->stockid;
    }
}
