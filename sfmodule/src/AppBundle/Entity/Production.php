<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Production
 */
class Production
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $number;

    /**
     * @var integer
     */
    private $quantity;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var string
     */
    private $productionminutes;

    /**
     * @var boolean
     */
    private $approved;

    /**
     * @var integer
     */
    private $approveduserid;

    /**
     * @var \DateTime
     */
    private $approveddate;

    /**
     * @var \DateTime
     */
    private $productionstartdate;

    /**
     * @var \DateTime
     */
    private $productionenddate;

    /**
     * @var \DateTime
     */
    private $materialdate;

    /**
     * @var \DateTime
     */
    private $subdeliverydate;

    /**
     * @var \DateTime
     */
    private $deliverydate;

    /**
     * @var boolean
     */
    private $cut;

    /**
     * @var integer
     */
    private $cutuserid;

    /**
     * @var \DateTime
     */
    private $cutdate;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var boolean
     */
    private $ready;

    /**
     * @var integer
     */
    private $readyuserid;

    /**
     * @var \DateTime
     */
    private $readydate;

    /**
     * @var boolean
     */
    private $sample;

    /**
     * @var string
     */
    private $sampletext;

    /**
     * @var \DateTime
     */
    private $sampledate;

    /**
     * @var string
     */
    private $plantext;

    /**
     * @var string
     */
    private $missingtext;

    /**
     * @var string
     */
    private $bundletext;

    /**
     * @var boolean
     */
    private $packed;

    /**
     * @var integer
     */
    private $packeduserid;

    /**
     * @var \DateTime
     */
    private $packeddate;

    /**
     * @var boolean
     */
    private $allocated;

    /**
     * @var integer
     */
    private $allocateduserid;

    /**
     * @var \DateTime
     */
    private $allocateddate;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var integer
     */
    private $productionlocationid;

    /**
     * @var integer
     */
    private $sourcefromno;

    /**
     * @var integer
     */
    private $sourcetono;

    /**
     * @var integer
     */
    private $sourcelocationid;

    /**
     * @var boolean
     */
    private $sourceout;

    /**
     * @var integer
     */
    private $sourceoutuserid;

    /**
     * @var \DateTime
     */
    private $sourceoutdate;

    /**
     * @var boolean
     */
    private $sourcein;

    /**
     * @var integer
     */
    private $sourceinuserid;

    /**
     * @var \DateTime
     */
    private $sourceindate;

    /**
     * @var boolean
     */
    private $fabrics;

    /**
     * @var integer
     */
    private $fabricsuserid;

    /**
     * @var \DateTime
     */
    private $fabricsdate;

    /**
     * @var boolean
     */
    private $accessories;

    /**
     * @var integer
     */
    private $accessoriesuserid;

    /**
     * @var \DateTime
     */
    private $accessoriesdate;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var boolean
     */
    private $materialsent;

    /**
     * @var integer
     */
    private $materialsentuserid;

    /**
     * @var \DateTime
     */
    private $materialsentdate;

    /**
     * @var integer
     */
    private $efficiency;

    /**
     * @var integer
     */
    private $accsent;

    /**
     * @var integer
     */
    private $accsentuserid;

    /**
     * @var \DateTime
     */
    private $accsentdate;

    /**
     * @var \DateTime
     */
    private $productionstartdateAct;

    /**
     * @var \DateTime
     */
    private $productionenddateAct;

    /**
     * @var \DateTime
     */
    private $materialdateAct;

    /**
     * @var \DateTime
     */
    private $subdeliverydateAct;

    /**
     * @var \DateTime
     */
    private $deliverydateAct;

    /**
     * @var integer
     */
    private $request;

    /**
     * @var integer
     */
    private $requestuserid;

    /**
     * @var \DateTime
     */
    private $requestdate;

    /**
     * @var integer
     */
    private $seasonid;

    /**
     * @var boolean
     */
    private $proddocrec;

    /**
     * @var integer
     */
    private $proddocrecuserid;

    /**
     * @var \DateTime
     */
    private $proddocrecdate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Production
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Production
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Production
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Production
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Production
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set number
     *
     * @param integer $number
     * @return Production
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set quantity
     *
     * @param integer $quantity
     * @return Production
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return integer 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Production
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Production
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set productionminutes
     *
     * @param string $productionminutes
     * @return Production
     */
    public function setProductionminutes($productionminutes)
    {
        $this->productionminutes = $productionminutes;

        return $this;
    }

    /**
     * Get productionminutes
     *
     * @return string 
     */
    public function getProductionminutes()
    {
        return $this->productionminutes;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return Production
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set approveduserid
     *
     * @param integer $approveduserid
     * @return Production
     */
    public function setApproveduserid($approveduserid)
    {
        $this->approveduserid = $approveduserid;

        return $this;
    }

    /**
     * Get approveduserid
     *
     * @return integer 
     */
    public function getApproveduserid()
    {
        return $this->approveduserid;
    }

    /**
     * Set approveddate
     *
     * @param \DateTime $approveddate
     * @return Production
     */
    public function setApproveddate($approveddate)
    {
        $this->approveddate = $approveddate;

        return $this;
    }

    /**
     * Get approveddate
     *
     * @return \DateTime 
     */
    public function getApproveddate()
    {
        return $this->approveddate;
    }

    /**
     * Set productionstartdate
     *
     * @param \DateTime $productionstartdate
     * @return Production
     */
    public function setProductionstartdate($productionstartdate)
    {
        $this->productionstartdate = $productionstartdate;

        return $this;
    }

    /**
     * Get productionstartdate
     *
     * @return \DateTime 
     */
    public function getProductionstartdate()
    {
        return $this->productionstartdate;
    }

    /**
     * Set productionenddate
     *
     * @param \DateTime $productionenddate
     * @return Production
     */
    public function setProductionenddate($productionenddate)
    {
        $this->productionenddate = $productionenddate;

        return $this;
    }

    /**
     * Get productionenddate
     *
     * @return \DateTime 
     */
    public function getProductionenddate()
    {
        return $this->productionenddate;
    }

    /**
     * Set materialdate
     *
     * @param \DateTime $materialdate
     * @return Production
     */
    public function setMaterialdate($materialdate)
    {
        $this->materialdate = $materialdate;

        return $this;
    }

    /**
     * Get materialdate
     *
     * @return \DateTime 
     */
    public function getMaterialdate()
    {
        return $this->materialdate;
    }

    /**
     * Set subdeliverydate
     *
     * @param \DateTime $subdeliverydate
     * @return Production
     */
    public function setSubdeliverydate($subdeliverydate)
    {
        $this->subdeliverydate = $subdeliverydate;

        return $this;
    }

    /**
     * Get subdeliverydate
     *
     * @return \DateTime 
     */
    public function getSubdeliverydate()
    {
        return $this->subdeliverydate;
    }

    /**
     * Set deliverydate
     *
     * @param \DateTime $deliverydate
     * @return Production
     */
    public function setDeliverydate($deliverydate)
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    /**
     * Get deliverydate
     *
     * @return \DateTime 
     */
    public function getDeliverydate()
    {
        return $this->deliverydate;
    }

    /**
     * Set cut
     *
     * @param boolean $cut
     * @return Production
     */
    public function setCut($cut)
    {
        $this->cut = $cut;

        return $this;
    }

    /**
     * Get cut
     *
     * @return boolean 
     */
    public function getCut()
    {
        return $this->cut;
    }

    /**
     * Set cutuserid
     *
     * @param integer $cutuserid
     * @return Production
     */
    public function setCutuserid($cutuserid)
    {
        $this->cutuserid = $cutuserid;

        return $this;
    }

    /**
     * Get cutuserid
     *
     * @return integer 
     */
    public function getCutuserid()
    {
        return $this->cutuserid;
    }

    /**
     * Set cutdate
     *
     * @param \DateTime $cutdate
     * @return Production
     */
    public function setCutdate($cutdate)
    {
        $this->cutdate = $cutdate;

        return $this;
    }

    /**
     * Get cutdate
     *
     * @return \DateTime 
     */
    public function getCutdate()
    {
        return $this->cutdate;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Production
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Production
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Production
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     * @return Production
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean 
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set readyuserid
     *
     * @param integer $readyuserid
     * @return Production
     */
    public function setReadyuserid($readyuserid)
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    /**
     * Get readyuserid
     *
     * @return integer 
     */
    public function getReadyuserid()
    {
        return $this->readyuserid;
    }

    /**
     * Set readydate
     *
     * @param \DateTime $readydate
     * @return Production
     */
    public function setReadydate($readydate)
    {
        $this->readydate = $readydate;

        return $this;
    }

    /**
     * Get readydate
     *
     * @return \DateTime 
     */
    public function getReadydate()
    {
        return $this->readydate;
    }

    /**
     * Set sample
     *
     * @param boolean $sample
     * @return Production
     */
    public function setSample($sample)
    {
        $this->sample = $sample;

        return $this;
    }

    /**
     * Get sample
     *
     * @return boolean 
     */
    public function getSample()
    {
        return $this->sample;
    }

    /**
     * Set sampletext
     *
     * @param string $sampletext
     * @return Production
     */
    public function setSampletext($sampletext)
    {
        $this->sampletext = $sampletext;

        return $this;
    }

    /**
     * Get sampletext
     *
     * @return string 
     */
    public function getSampletext()
    {
        return $this->sampletext;
    }

    /**
     * Set sampledate
     *
     * @param \DateTime $sampledate
     * @return Production
     */
    public function setSampledate($sampledate)
    {
        $this->sampledate = $sampledate;

        return $this;
    }

    /**
     * Get sampledate
     *
     * @return \DateTime 
     */
    public function getSampledate()
    {
        return $this->sampledate;
    }

    /**
     * Set plantext
     *
     * @param string $plantext
     * @return Production
     */
    public function setPlantext($plantext)
    {
        $this->plantext = $plantext;

        return $this;
    }

    /**
     * Get plantext
     *
     * @return string 
     */
    public function getPlantext()
    {
        return $this->plantext;
    }

    /**
     * Set missingtext
     *
     * @param string $missingtext
     * @return Production
     */
    public function setMissingtext($missingtext)
    {
        $this->missingtext = $missingtext;

        return $this;
    }

    /**
     * Get missingtext
     *
     * @return string 
     */
    public function getMissingtext()
    {
        return $this->missingtext;
    }

    /**
     * Set bundletext
     *
     * @param string $bundletext
     * @return Production
     */
    public function setBundletext($bundletext)
    {
        $this->bundletext = $bundletext;

        return $this;
    }

    /**
     * Get bundletext
     *
     * @return string 
     */
    public function getBundletext()
    {
        return $this->bundletext;
    }

    /**
     * Set packed
     *
     * @param boolean $packed
     * @return Production
     */
    public function setPacked($packed)
    {
        $this->packed = $packed;

        return $this;
    }

    /**
     * Get packed
     *
     * @return boolean 
     */
    public function getPacked()
    {
        return $this->packed;
    }

    /**
     * Set packeduserid
     *
     * @param integer $packeduserid
     * @return Production
     */
    public function setPackeduserid($packeduserid)
    {
        $this->packeduserid = $packeduserid;

        return $this;
    }

    /**
     * Get packeduserid
     *
     * @return integer 
     */
    public function getPackeduserid()
    {
        return $this->packeduserid;
    }

    /**
     * Set packeddate
     *
     * @param \DateTime $packeddate
     * @return Production
     */
    public function setPackeddate($packeddate)
    {
        $this->packeddate = $packeddate;

        return $this;
    }

    /**
     * Get packeddate
     *
     * @return \DateTime 
     */
    public function getPackeddate()
    {
        return $this->packeddate;
    }

    /**
     * Set allocated
     *
     * @param boolean $allocated
     * @return Production
     */
    public function setAllocated($allocated)
    {
        $this->allocated = $allocated;

        return $this;
    }

    /**
     * Get allocated
     *
     * @return boolean 
     */
    public function getAllocated()
    {
        return $this->allocated;
    }

    /**
     * Set allocateduserid
     *
     * @param integer $allocateduserid
     * @return Production
     */
    public function setAllocateduserid($allocateduserid)
    {
        $this->allocateduserid = $allocateduserid;

        return $this;
    }

    /**
     * Get allocateduserid
     *
     * @return integer 
     */
    public function getAllocateduserid()
    {
        return $this->allocateduserid;
    }

    /**
     * Set allocateddate
     *
     * @param \DateTime $allocateddate
     * @return Production
     */
    public function setAllocateddate($allocateddate)
    {
        $this->allocateddate = $allocateddate;

        return $this;
    }

    /**
     * Get allocateddate
     *
     * @return \DateTime 
     */
    public function getAllocateddate()
    {
        return $this->allocateddate;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Production
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set productionlocationid
     *
     * @param integer $productionlocationid
     * @return Production
     */
    public function setProductionlocationid($productionlocationid)
    {
        $this->productionlocationid = $productionlocationid;

        return $this;
    }

    /**
     * Get productionlocationid
     *
     * @return integer 
     */
    public function getProductionlocationid()
    {
        return $this->productionlocationid;
    }

    /**
     * Set sourcefromno
     *
     * @param integer $sourcefromno
     * @return Production
     */
    public function setSourcefromno($sourcefromno)
    {
        $this->sourcefromno = $sourcefromno;

        return $this;
    }

    /**
     * Get sourcefromno
     *
     * @return integer 
     */
    public function getSourcefromno()
    {
        return $this->sourcefromno;
    }

    /**
     * Set sourcetono
     *
     * @param integer $sourcetono
     * @return Production
     */
    public function setSourcetono($sourcetono)
    {
        $this->sourcetono = $sourcetono;

        return $this;
    }

    /**
     * Get sourcetono
     *
     * @return integer 
     */
    public function getSourcetono()
    {
        return $this->sourcetono;
    }

    /**
     * Set sourcelocationid
     *
     * @param integer $sourcelocationid
     * @return Production
     */
    public function setSourcelocationid($sourcelocationid)
    {
        $this->sourcelocationid = $sourcelocationid;

        return $this;
    }

    /**
     * Get sourcelocationid
     *
     * @return integer 
     */
    public function getSourcelocationid()
    {
        return $this->sourcelocationid;
    }

    /**
     * Set sourceout
     *
     * @param boolean $sourceout
     * @return Production
     */
    public function setSourceout($sourceout)
    {
        $this->sourceout = $sourceout;

        return $this;
    }

    /**
     * Get sourceout
     *
     * @return boolean 
     */
    public function getSourceout()
    {
        return $this->sourceout;
    }

    /**
     * Set sourceoutuserid
     *
     * @param integer $sourceoutuserid
     * @return Production
     */
    public function setSourceoutuserid($sourceoutuserid)
    {
        $this->sourceoutuserid = $sourceoutuserid;

        return $this;
    }

    /**
     * Get sourceoutuserid
     *
     * @return integer 
     */
    public function getSourceoutuserid()
    {
        return $this->sourceoutuserid;
    }

    /**
     * Set sourceoutdate
     *
     * @param \DateTime $sourceoutdate
     * @return Production
     */
    public function setSourceoutdate($sourceoutdate)
    {
        $this->sourceoutdate = $sourceoutdate;

        return $this;
    }

    /**
     * Get sourceoutdate
     *
     * @return \DateTime 
     */
    public function getSourceoutdate()
    {
        return $this->sourceoutdate;
    }

    /**
     * Set sourcein
     *
     * @param boolean $sourcein
     * @return Production
     */
    public function setSourcein($sourcein)
    {
        $this->sourcein = $sourcein;

        return $this;
    }

    /**
     * Get sourcein
     *
     * @return boolean 
     */
    public function getSourcein()
    {
        return $this->sourcein;
    }

    /**
     * Set sourceinuserid
     *
     * @param integer $sourceinuserid
     * @return Production
     */
    public function setSourceinuserid($sourceinuserid)
    {
        $this->sourceinuserid = $sourceinuserid;

        return $this;
    }

    /**
     * Get sourceinuserid
     *
     * @return integer 
     */
    public function getSourceinuserid()
    {
        return $this->sourceinuserid;
    }

    /**
     * Set sourceindate
     *
     * @param \DateTime $sourceindate
     * @return Production
     */
    public function setSourceindate($sourceindate)
    {
        $this->sourceindate = $sourceindate;

        return $this;
    }

    /**
     * Get sourceindate
     *
     * @return \DateTime 
     */
    public function getSourceindate()
    {
        return $this->sourceindate;
    }

    /**
     * Set fabrics
     *
     * @param boolean $fabrics
     * @return Production
     */
    public function setFabrics($fabrics)
    {
        $this->fabrics = $fabrics;

        return $this;
    }

    /**
     * Get fabrics
     *
     * @return boolean 
     */
    public function getFabrics()
    {
        return $this->fabrics;
    }

    /**
     * Set fabricsuserid
     *
     * @param integer $fabricsuserid
     * @return Production
     */
    public function setFabricsuserid($fabricsuserid)
    {
        $this->fabricsuserid = $fabricsuserid;

        return $this;
    }

    /**
     * Get fabricsuserid
     *
     * @return integer 
     */
    public function getFabricsuserid()
    {
        return $this->fabricsuserid;
    }

    /**
     * Set fabricsdate
     *
     * @param \DateTime $fabricsdate
     * @return Production
     */
    public function setFabricsdate($fabricsdate)
    {
        $this->fabricsdate = $fabricsdate;

        return $this;
    }

    /**
     * Get fabricsdate
     *
     * @return \DateTime 
     */
    public function getFabricsdate()
    {
        return $this->fabricsdate;
    }

    /**
     * Set accessories
     *
     * @param boolean $accessories
     * @return Production
     */
    public function setAccessories($accessories)
    {
        $this->accessories = $accessories;

        return $this;
    }

    /**
     * Get accessories
     *
     * @return boolean 
     */
    public function getAccessories()
    {
        return $this->accessories;
    }

    /**
     * Set accessoriesuserid
     *
     * @param integer $accessoriesuserid
     * @return Production
     */
    public function setAccessoriesuserid($accessoriesuserid)
    {
        $this->accessoriesuserid = $accessoriesuserid;

        return $this;
    }

    /**
     * Get accessoriesuserid
     *
     * @return integer 
     */
    public function getAccessoriesuserid()
    {
        return $this->accessoriesuserid;
    }

    /**
     * Set accessoriesdate
     *
     * @param \DateTime $accessoriesdate
     * @return Production
     */
    public function setAccessoriesdate($accessoriesdate)
    {
        $this->accessoriesdate = $accessoriesdate;

        return $this;
    }

    /**
     * Get accessoriesdate
     *
     * @return \DateTime 
     */
    public function getAccessoriesdate()
    {
        return $this->accessoriesdate;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Production
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set materialsent
     *
     * @param boolean $materialsent
     * @return Production
     */
    public function setMaterialsent($materialsent)
    {
        $this->materialsent = $materialsent;

        return $this;
    }

    /**
     * Get materialsent
     *
     * @return boolean 
     */
    public function getMaterialsent()
    {
        return $this->materialsent;
    }

    /**
     * Set materialsentuserid
     *
     * @param integer $materialsentuserid
     * @return Production
     */
    public function setMaterialsentuserid($materialsentuserid)
    {
        $this->materialsentuserid = $materialsentuserid;

        return $this;
    }

    /**
     * Get materialsentuserid
     *
     * @return integer 
     */
    public function getMaterialsentuserid()
    {
        return $this->materialsentuserid;
    }

    /**
     * Set materialsentdate
     *
     * @param \DateTime $materialsentdate
     * @return Production
     */
    public function setMaterialsentdate($materialsentdate)
    {
        $this->materialsentdate = $materialsentdate;

        return $this;
    }

    /**
     * Get materialsentdate
     *
     * @return \DateTime 
     */
    public function getMaterialsentdate()
    {
        return $this->materialsentdate;
    }

    /**
     * Set efficiency
     *
     * @param integer $efficiency
     * @return Production
     */
    public function setEfficiency($efficiency)
    {
        $this->efficiency = $efficiency;

        return $this;
    }

    /**
     * Get efficiency
     *
     * @return integer 
     */
    public function getEfficiency()
    {
        return $this->efficiency;
    }

    /**
     * Set accsent
     *
     * @param integer $accsent
     * @return Production
     */
    public function setAccsent($accsent)
    {
        $this->accsent = $accsent;

        return $this;
    }

    /**
     * Get accsent
     *
     * @return integer 
     */
    public function getAccsent()
    {
        return $this->accsent;
    }

    /**
     * Set accsentuserid
     *
     * @param integer $accsentuserid
     * @return Production
     */
    public function setAccsentuserid($accsentuserid)
    {
        $this->accsentuserid = $accsentuserid;

        return $this;
    }

    /**
     * Get accsentuserid
     *
     * @return integer 
     */
    public function getAccsentuserid()
    {
        return $this->accsentuserid;
    }

    /**
     * Set accsentdate
     *
     * @param \DateTime $accsentdate
     * @return Production
     */
    public function setAccsentdate($accsentdate)
    {
        $this->accsentdate = $accsentdate;

        return $this;
    }

    /**
     * Get accsentdate
     *
     * @return \DateTime 
     */
    public function getAccsentdate()
    {
        return $this->accsentdate;
    }

    /**
     * Set productionstartdateAct
     *
     * @param \DateTime $productionstartdateAct
     * @return Production
     */
    public function setProductionstartdateAct($productionstartdateAct)
    {
        $this->productionstartdateAct = $productionstartdateAct;

        return $this;
    }

    /**
     * Get productionstartdateAct
     *
     * @return \DateTime 
     */
    public function getProductionstartdateAct()
    {
        return $this->productionstartdateAct;
    }

    /**
     * Set productionenddateAct
     *
     * @param \DateTime $productionenddateAct
     * @return Production
     */
    public function setProductionenddateAct($productionenddateAct)
    {
        $this->productionenddateAct = $productionenddateAct;

        return $this;
    }

    /**
     * Get productionenddateAct
     *
     * @return \DateTime 
     */
    public function getProductionenddateAct()
    {
        return $this->productionenddateAct;
    }

    /**
     * Set materialdateAct
     *
     * @param \DateTime $materialdateAct
     * @return Production
     */
    public function setMaterialdateAct($materialdateAct)
    {
        $this->materialdateAct = $materialdateAct;

        return $this;
    }

    /**
     * Get materialdateAct
     *
     * @return \DateTime 
     */
    public function getMaterialdateAct()
    {
        return $this->materialdateAct;
    }

    /**
     * Set subdeliverydateAct
     *
     * @param \DateTime $subdeliverydateAct
     * @return Production
     */
    public function setSubdeliverydateAct($subdeliverydateAct)
    {
        $this->subdeliverydateAct = $subdeliverydateAct;

        return $this;
    }

    /**
     * Get subdeliverydateAct
     *
     * @return \DateTime 
     */
    public function getSubdeliverydateAct()
    {
        return $this->subdeliverydateAct;
    }

    /**
     * Set deliverydateAct
     *
     * @param \DateTime $deliverydateAct
     * @return Production
     */
    public function setDeliverydateAct($deliverydateAct)
    {
        $this->deliverydateAct = $deliverydateAct;

        return $this;
    }

    /**
     * Get deliverydateAct
     *
     * @return \DateTime 
     */
    public function getDeliverydateAct()
    {
        return $this->deliverydateAct;
    }

    /**
     * Set request
     *
     * @param integer $request
     * @return Production
     */
    public function setRequest($request)
    {
        $this->request = $request;

        return $this;
    }

    /**
     * Get request
     *
     * @return integer 
     */
    public function getRequest()
    {
        return $this->request;
    }

    /**
     * Set requestuserid
     *
     * @param integer $requestuserid
     * @return Production
     */
    public function setRequestuserid($requestuserid)
    {
        $this->requestuserid = $requestuserid;

        return $this;
    }

    /**
     * Get requestuserid
     *
     * @return integer 
     */
    public function getRequestuserid()
    {
        return $this->requestuserid;
    }

    /**
     * Set requestdate
     *
     * @param \DateTime $requestdate
     * @return Production
     */
    public function setRequestdate($requestdate)
    {
        $this->requestdate = $requestdate;

        return $this;
    }

    /**
     * Get requestdate
     *
     * @return \DateTime 
     */
    public function getRequestdate()
    {
        return $this->requestdate;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Production
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer 
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }

    /**
     * Set proddocrec
     *
     * @param boolean $proddocrec
     * @return Production
     */
    public function setProddocrec($proddocrec)
    {
        $this->proddocrec = $proddocrec;

        return $this;
    }

    /**
     * Get proddocrec
     *
     * @return boolean 
     */
    public function getProddocrec()
    {
        return $this->proddocrec;
    }

    /**
     * Set proddocrecuserid
     *
     * @param integer $proddocrecuserid
     * @return Production
     */
    public function setProddocrecuserid($proddocrecuserid)
    {
        $this->proddocrecuserid = $proddocrecuserid;

        return $this;
    }

    /**
     * Get proddocrecuserid
     *
     * @return integer 
     */
    public function getProddocrecuserid()
    {
        return $this->proddocrecuserid;
    }

    /**
     * Set proddocrecdate
     *
     * @param \DateTime $proddocrecdate
     * @return Production
     */
    public function setProddocrecdate($proddocrecdate)
    {
        $this->proddocrecdate = $proddocrecdate;

        return $this;
    }

    /**
     * Get proddocrecdate
     *
     * @return \DateTime 
     */
    public function getProddocrecdate()
    {
        return $this->proddocrecdate;
    }
}
