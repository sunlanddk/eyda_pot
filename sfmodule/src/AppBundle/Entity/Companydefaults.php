<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Companydefaults
 */
class Companydefaults
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $countryid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var integer
     */
    private $paymenttermid;

    /**
     * @var integer
     */
    private $deliverytermid;

    /**
     * @var integer
     */
    private $carrierid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Companydefaults
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Companydefaults
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set paymenttermid
     *
     * @param integer $paymenttermid
     * @return Companydefaults
     */
    public function setPaymenttermid($paymenttermid)
    {
        $this->paymenttermid = $paymenttermid;

        return $this;
    }

    /**
     * Get paymenttermid
     *
     * @return integer 
     */
    public function getPaymenttermid()
    {
        return $this->paymenttermid;
    }

    /**
     * Set deliverytermid
     *
     * @param integer $deliverytermid
     * @return Companydefaults
     */
    public function setDeliverytermid($deliverytermid)
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    /**
     * Get deliverytermid
     *
     * @return integer 
     */
    public function getDeliverytermid()
    {
        return $this->deliverytermid;
    }

    /**
     * Set carrierid
     *
     * @param integer $carrierid
     * @return Companydefaults
     */
    public function setCarrierid($carrierid)
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    /**
     * Get carrierid
     *
     * @return integer 
     */
    public function getCarrierid()
    {
        return $this->carrierid;
    }
}
