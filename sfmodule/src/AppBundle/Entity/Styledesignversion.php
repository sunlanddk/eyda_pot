<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Styledesignversion
 */
class Styledesignversion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var string
     */
    private $details;

    /**
     * @var string
     */
    private $remarks;

    /**
     * @var string
     */
    private $labelplacement;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Styledesignversion
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set details
     *
     * @param string $details
     * @return Styledesignversion
     */
    public function setDetails($details)
    {
        $this->details = $details;

        return $this;
    }

    /**
     * Get details
     *
     * @return string 
     */
    public function getDetails()
    {
        return $this->details;
    }

    /**
     * Set remarks
     *
     * @param string $remarks
     * @return Styledesignversion
     */
    public function setRemarks($remarks)
    {
        $this->remarks = $remarks;

        return $this;
    }

    /**
     * Get remarks
     *
     * @return string 
     */
    public function getRemarks()
    {
        return $this->remarks;
    }

    /**
     * Set labelplacement
     *
     * @param string $labelplacement
     * @return Styledesignversion
     */
    public function setLabelplacement($labelplacement)
    {
        $this->labelplacement = $labelplacement;

        return $this;
    }

    /**
     * Get labelplacement
     *
     * @return string 
     */
    public function getLabelplacement()
    {
        return $this->labelplacement;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Styledesignversion
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Styledesignversion
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Styledesignversion
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Styledesignversion
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Styledesignversion
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Styledesignversion
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
