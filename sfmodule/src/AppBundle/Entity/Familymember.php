<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Familymember
 */
class Familymember
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $familyid;

    /**
     * @var integer
     */
    private $companyid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set familyid
     *
     * @param integer $familyid
     * @return Familymember
     */
    public function setFamilyid($familyid)
    {
        $this->familyid = $familyid;

        return $this;
    }

    /**
     * Get familyid
     *
     * @return integer 
     */
    public function getFamilyid()
    {
        return $this->familyid;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Familymember
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }
}
