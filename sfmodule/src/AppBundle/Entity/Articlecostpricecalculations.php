<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articlecostpricecalculations
 */
class Articlecostpricecalculations
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var string
     */
    private $surplusmaterials;

    /**
     * @var string
     */
    private $surplusoperations;

    /**
     * @var string
     */
    private $logisticcost;

    /**
     * @var string
     */
    private $othermaterialcost;

    /**
     * @var string
     */
    private $cat1;

    /**
     * @var string
     */
    private $cat2;

    /**
     * @var string
     */
    private $cat3;

    /**
     * @var string
     */
    private $cat4;

    /**
     * @var string
     */
    private $cat5;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Articlecostpricecalculations
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set surplusmaterials
     *
     * @param string $surplusmaterials
     * @return Articlecostpricecalculations
     */
    public function setSurplusmaterials($surplusmaterials)
    {
        $this->surplusmaterials = $surplusmaterials;

        return $this;
    }

    /**
     * Get surplusmaterials
     *
     * @return string 
     */
    public function getSurplusmaterials()
    {
        return $this->surplusmaterials;
    }

    /**
     * Set surplusoperations
     *
     * @param string $surplusoperations
     * @return Articlecostpricecalculations
     */
    public function setSurplusoperations($surplusoperations)
    {
        $this->surplusoperations = $surplusoperations;

        return $this;
    }

    /**
     * Get surplusoperations
     *
     * @return string 
     */
    public function getSurplusoperations()
    {
        return $this->surplusoperations;
    }

    /**
     * Set logisticcost
     *
     * @param string $logisticcost
     * @return Articlecostpricecalculations
     */
    public function setLogisticcost($logisticcost)
    {
        $this->logisticcost = $logisticcost;

        return $this;
    }

    /**
     * Get logisticcost
     *
     * @return string 
     */
    public function getLogisticcost()
    {
        return $this->logisticcost;
    }

    /**
     * Set othermaterialcost
     *
     * @param string $othermaterialcost
     * @return Articlecostpricecalculations
     */
    public function setOthermaterialcost($othermaterialcost)
    {
        $this->othermaterialcost = $othermaterialcost;

        return $this;
    }

    /**
     * Get othermaterialcost
     *
     * @return string 
     */
    public function getOthermaterialcost()
    {
        return $this->othermaterialcost;
    }

    /**
     * Set cat1
     *
     * @param string $cat1
     * @return Articlecostpricecalculations
     */
    public function setCat1($cat1)
    {
        $this->cat1 = $cat1;

        return $this;
    }

    /**
     * Get cat1
     *
     * @return string 
     */
    public function getCat1()
    {
        return $this->cat1;
    }

    /**
     * Set cat2
     *
     * @param string $cat2
     * @return Articlecostpricecalculations
     */
    public function setCat2($cat2)
    {
        $this->cat2 = $cat2;

        return $this;
    }

    /**
     * Get cat2
     *
     * @return string 
     */
    public function getCat2()
    {
        return $this->cat2;
    }

    /**
     * Set cat3
     *
     * @param string $cat3
     * @return Articlecostpricecalculations
     */
    public function setCat3($cat3)
    {
        $this->cat3 = $cat3;

        return $this;
    }

    /**
     * Get cat3
     *
     * @return string 
     */
    public function getCat3()
    {
        return $this->cat3;
    }

    /**
     * Set cat4
     *
     * @param string $cat4
     * @return Articlecostpricecalculations
     */
    public function setCat4($cat4)
    {
        $this->cat4 = $cat4;

        return $this;
    }

    /**
     * Get cat4
     *
     * @return string 
     */
    public function getCat4()
    {
        return $this->cat4;
    }

    /**
     * Set cat5
     *
     * @param string $cat5
     * @return Articlecostpricecalculations
     */
    public function setCat5($cat5)
    {
        $this->cat5 = $cat5;

        return $this;
    }

    /**
     * Get cat5
     *
     * @return string 
     */
    public function getCat5()
    {
        return $this->cat5;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Articlecostpricecalculations
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Articlecostpricecalculations
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Articlecostpricecalculations
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Articlecostpricecalculations
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
