<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accountmap
 */
class Accountmap
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $countrygrp;

    /**
     * @var string
     */
    private $leaddigit;

    /**
     * @var string
     */
    private $account;

    /**
     * @var integer
     */
    private $ownercompanyid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set countrygrp
     *
     * @param integer $countrygrp
     * @return Accountmap
     */
    public function setCountrygrp($countrygrp)
    {
        $this->countrygrp = $countrygrp;

        return $this;
    }

    /**
     * Get countrygrp
     *
     * @return integer 
     */
    public function getCountrygrp()
    {
        return $this->countrygrp;
    }

    /**
     * Set leaddigit
     *
     * @param string $leaddigit
     * @return Accountmap
     */
    public function setLeaddigit($leaddigit)
    {
        $this->leaddigit = $leaddigit;

        return $this;
    }

    /**
     * Get leaddigit
     *
     * @return string 
     */
    public function getLeaddigit()
    {
        return $this->leaddigit;
    }

    /**
     * Set account
     *
     * @param string $account
     * @return Accountmap
     */
    public function setAccount($account)
    {
        $this->account = $account;

        return $this;
    }

    /**
     * Get account
     *
     * @return string 
     */
    public function getAccount()
    {
        return $this->account;
    }

    /**
     * Set ownercompanyid
     *
     * @param integer $ownercompanyid
     * @return Accountmap
     */
    public function setOwnercompanyid($ownercompanyid)
    {
        $this->ownercompanyid = $ownercompanyid;

        return $this;
    }

    /**
     * Get ownercompanyid
     *
     * @return integer 
     */
    public function getOwnercompanyid()
    {
        return $this->ownercompanyid;
    }
}
