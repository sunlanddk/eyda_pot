<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articlecolor
 */
class Articlecolor
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $colorid;

    /**
     * @var string
     */
    private $priceadjustpercentage;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var boolean
     */
    private $heavymetals;

    /**
     * @var string
     */
    private $line00description;

    /**
     * @var integer
     */
    private $line00colorid;

    /**
     * @var string
     */
    private $line01description;

    /**
     * @var integer
     */
    private $line01colorid;

    /**
     * @var string
     */
    private $line02description;

    /**
     * @var integer
     */
    private $line02colorid;

    /**
     * @var string
     */
    private $line03description;

    /**
     * @var integer
     */
    private $line03colorid;

    /**
     * @var string
     */
    private $line04description;

    /**
     * @var integer
     */
    private $line04colorid;

    /**
     * @var string
     */
    private $line05description;

    /**
     * @var integer
     */
    private $line05colorid;

    /**
     * @var string
     */
    private $line06description;

    /**
     * @var integer
     */
    private $line06colorid;

    /**
     * @var string
     */
    private $line07description;

    /**
     * @var integer
     */
    private $line07colorid;

    /**
     * @var string
     */
    private $line08description;

    /**
     * @var integer
     */
    private $line08colorid;

    /**
     * @var string
     */
    private $line09description;

    /**
     * @var integer
     */
    private $line09colorid;

    /**
     * @var string
     */
    private $line10description;

    /**
     * @var integer
     */
    private $line10colorid;

    /**
     * @var string
     */
    private $line11description;

    /**
     * @var integer
     */
    private $line11colorid;

    /**
     * @var string
     */
    private $line12description;

    /**
     * @var integer
     */
    private $line12colorid;

    /**
     * @var \DateTime
     */
    private $ordereddate;

    /**
     * @var \DateTime
     */
    private $receiveddate;

    /**
     * @var \DateTime
     */
    private $rejecteddate;

    /**
     * @var \DateTime
     */
    private $approveddate;

    /**
     * @var integer
     */
    private $customercompanyid;

    /**
     * @var integer
     */
    private $salesuserid;

    /**
     * @var integer
     */
    private $projectid;

    /**
     * @var string
     */
    private $labdyecomment;

    /**
     * @var integer
     */
    private $approveduserid;

    /**
     * @var \DateTime
     */
    private $approveduserdate;

    /**
     * @var integer
     */
    private $receiveduserid;

    /**
     * @var \DateTime
     */
    private $receiveduserdate;

    /**
     * @var integer
     */
    private $ordereduserid;

    /**
     * @var \DateTime
     */
    private $ordereduserdate;

    /**
     * @var integer
     */
    private $receivecompanyid;

    /**
     * @var boolean
     */
    private $archived;

    /**
     * @var \DateTime
     */
    private $archiveddate;

    /**
     * @var integer
     */
    private $archiveduserid;

    /**
     * @var string
     */
    private $orderedqty;

    /**
     * @var string
     */
    private $receivedqty;

    /**
     * @var integer
     */
    private $sexid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Articlecolor
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Articlecolor
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Articlecolor
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Articlecolor
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Articlecolor
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Articlecolor
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set colorid
     *
     * @param integer $colorid
     * @return Articlecolor
     */
    public function setColorid($colorid)
    {
        $this->colorid = $colorid;

        return $this;
    }

    /**
     * Get colorid
     *
     * @return integer 
     */
    public function getColorid()
    {
        return $this->colorid;
    }

    /**
     * Set priceadjustpercentage
     *
     * @param string $priceadjustpercentage
     * @return Articlecolor
     */
    public function setPriceadjustpercentage($priceadjustpercentage)
    {
        $this->priceadjustpercentage = $priceadjustpercentage;

        return $this;
    }

    /**
     * Get priceadjustpercentage
     *
     * @return string 
     */
    public function getPriceadjustpercentage()
    {
        return $this->priceadjustpercentage;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Articlecolor
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Articlecolor
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set heavymetals
     *
     * @param boolean $heavymetals
     * @return Articlecolor
     */
    public function setHeavymetals($heavymetals)
    {
        $this->heavymetals = $heavymetals;

        return $this;
    }

    /**
     * Get heavymetals
     *
     * @return boolean 
     */
    public function getHeavymetals()
    {
        return $this->heavymetals;
    }

    /**
     * Set line00description
     *
     * @param string $line00description
     * @return Articlecolor
     */
    public function setLine00description($line00description)
    {
        $this->line00description = $line00description;

        return $this;
    }

    /**
     * Get line00description
     *
     * @return string 
     */
    public function getLine00description()
    {
        return $this->line00description;
    }

    /**
     * Set line00colorid
     *
     * @param integer $line00colorid
     * @return Articlecolor
     */
    public function setLine00colorid($line00colorid)
    {
        $this->line00colorid = $line00colorid;

        return $this;
    }

    /**
     * Get line00colorid
     *
     * @return integer 
     */
    public function getLine00colorid()
    {
        return $this->line00colorid;
    }

    /**
     * Set line01description
     *
     * @param string $line01description
     * @return Articlecolor
     */
    public function setLine01description($line01description)
    {
        $this->line01description = $line01description;

        return $this;
    }

    /**
     * Get line01description
     *
     * @return string 
     */
    public function getLine01description()
    {
        return $this->line01description;
    }

    /**
     * Set line01colorid
     *
     * @param integer $line01colorid
     * @return Articlecolor
     */
    public function setLine01colorid($line01colorid)
    {
        $this->line01colorid = $line01colorid;

        return $this;
    }

    /**
     * Get line01colorid
     *
     * @return integer 
     */
    public function getLine01colorid()
    {
        return $this->line01colorid;
    }

    /**
     * Set line02description
     *
     * @param string $line02description
     * @return Articlecolor
     */
    public function setLine02description($line02description)
    {
        $this->line02description = $line02description;

        return $this;
    }

    /**
     * Get line02description
     *
     * @return string 
     */
    public function getLine02description()
    {
        return $this->line02description;
    }

    /**
     * Set line02colorid
     *
     * @param integer $line02colorid
     * @return Articlecolor
     */
    public function setLine02colorid($line02colorid)
    {
        $this->line02colorid = $line02colorid;

        return $this;
    }

    /**
     * Get line02colorid
     *
     * @return integer 
     */
    public function getLine02colorid()
    {
        return $this->line02colorid;
    }

    /**
     * Set line03description
     *
     * @param string $line03description
     * @return Articlecolor
     */
    public function setLine03description($line03description)
    {
        $this->line03description = $line03description;

        return $this;
    }

    /**
     * Get line03description
     *
     * @return string 
     */
    public function getLine03description()
    {
        return $this->line03description;
    }

    /**
     * Set line03colorid
     *
     * @param integer $line03colorid
     * @return Articlecolor
     */
    public function setLine03colorid($line03colorid)
    {
        $this->line03colorid = $line03colorid;

        return $this;
    }

    /**
     * Get line03colorid
     *
     * @return integer 
     */
    public function getLine03colorid()
    {
        return $this->line03colorid;
    }

    /**
     * Set line04description
     *
     * @param string $line04description
     * @return Articlecolor
     */
    public function setLine04description($line04description)
    {
        $this->line04description = $line04description;

        return $this;
    }

    /**
     * Get line04description
     *
     * @return string 
     */
    public function getLine04description()
    {
        return $this->line04description;
    }

    /**
     * Set line04colorid
     *
     * @param integer $line04colorid
     * @return Articlecolor
     */
    public function setLine04colorid($line04colorid)
    {
        $this->line04colorid = $line04colorid;

        return $this;
    }

    /**
     * Get line04colorid
     *
     * @return integer 
     */
    public function getLine04colorid()
    {
        return $this->line04colorid;
    }

    /**
     * Set line05description
     *
     * @param string $line05description
     * @return Articlecolor
     */
    public function setLine05description($line05description)
    {
        $this->line05description = $line05description;

        return $this;
    }

    /**
     * Get line05description
     *
     * @return string 
     */
    public function getLine05description()
    {
        return $this->line05description;
    }

    /**
     * Set line05colorid
     *
     * @param integer $line05colorid
     * @return Articlecolor
     */
    public function setLine05colorid($line05colorid)
    {
        $this->line05colorid = $line05colorid;

        return $this;
    }

    /**
     * Get line05colorid
     *
     * @return integer 
     */
    public function getLine05colorid()
    {
        return $this->line05colorid;
    }

    /**
     * Set line06description
     *
     * @param string $line06description
     * @return Articlecolor
     */
    public function setLine06description($line06description)
    {
        $this->line06description = $line06description;

        return $this;
    }

    /**
     * Get line06description
     *
     * @return string 
     */
    public function getLine06description()
    {
        return $this->line06description;
    }

    /**
     * Set line06colorid
     *
     * @param integer $line06colorid
     * @return Articlecolor
     */
    public function setLine06colorid($line06colorid)
    {
        $this->line06colorid = $line06colorid;

        return $this;
    }

    /**
     * Get line06colorid
     *
     * @return integer 
     */
    public function getLine06colorid()
    {
        return $this->line06colorid;
    }

    /**
     * Set line07description
     *
     * @param string $line07description
     * @return Articlecolor
     */
    public function setLine07description($line07description)
    {
        $this->line07description = $line07description;

        return $this;
    }

    /**
     * Get line07description
     *
     * @return string 
     */
    public function getLine07description()
    {
        return $this->line07description;
    }

    /**
     * Set line07colorid
     *
     * @param integer $line07colorid
     * @return Articlecolor
     */
    public function setLine07colorid($line07colorid)
    {
        $this->line07colorid = $line07colorid;

        return $this;
    }

    /**
     * Get line07colorid
     *
     * @return integer 
     */
    public function getLine07colorid()
    {
        return $this->line07colorid;
    }

    /**
     * Set line08description
     *
     * @param string $line08description
     * @return Articlecolor
     */
    public function setLine08description($line08description)
    {
        $this->line08description = $line08description;

        return $this;
    }

    /**
     * Get line08description
     *
     * @return string 
     */
    public function getLine08description()
    {
        return $this->line08description;
    }

    /**
     * Set line08colorid
     *
     * @param integer $line08colorid
     * @return Articlecolor
     */
    public function setLine08colorid($line08colorid)
    {
        $this->line08colorid = $line08colorid;

        return $this;
    }

    /**
     * Get line08colorid
     *
     * @return integer 
     */
    public function getLine08colorid()
    {
        return $this->line08colorid;
    }

    /**
     * Set line09description
     *
     * @param string $line09description
     * @return Articlecolor
     */
    public function setLine09description($line09description)
    {
        $this->line09description = $line09description;

        return $this;
    }

    /**
     * Get line09description
     *
     * @return string 
     */
    public function getLine09description()
    {
        return $this->line09description;
    }

    /**
     * Set line09colorid
     *
     * @param integer $line09colorid
     * @return Articlecolor
     */
    public function setLine09colorid($line09colorid)
    {
        $this->line09colorid = $line09colorid;

        return $this;
    }

    /**
     * Get line09colorid
     *
     * @return integer 
     */
    public function getLine09colorid()
    {
        return $this->line09colorid;
    }

    /**
     * Set line10description
     *
     * @param string $line10description
     * @return Articlecolor
     */
    public function setLine10description($line10description)
    {
        $this->line10description = $line10description;

        return $this;
    }

    /**
     * Get line10description
     *
     * @return string 
     */
    public function getLine10description()
    {
        return $this->line10description;
    }

    /**
     * Set line10colorid
     *
     * @param integer $line10colorid
     * @return Articlecolor
     */
    public function setLine10colorid($line10colorid)
    {
        $this->line10colorid = $line10colorid;

        return $this;
    }

    /**
     * Get line10colorid
     *
     * @return integer 
     */
    public function getLine10colorid()
    {
        return $this->line10colorid;
    }

    /**
     * Set line11description
     *
     * @param string $line11description
     * @return Articlecolor
     */
    public function setLine11description($line11description)
    {
        $this->line11description = $line11description;

        return $this;
    }

    /**
     * Get line11description
     *
     * @return string 
     */
    public function getLine11description()
    {
        return $this->line11description;
    }

    /**
     * Set line11colorid
     *
     * @param integer $line11colorid
     * @return Articlecolor
     */
    public function setLine11colorid($line11colorid)
    {
        $this->line11colorid = $line11colorid;

        return $this;
    }

    /**
     * Get line11colorid
     *
     * @return integer 
     */
    public function getLine11colorid()
    {
        return $this->line11colorid;
    }

    /**
     * Set line12description
     *
     * @param string $line12description
     * @return Articlecolor
     */
    public function setLine12description($line12description)
    {
        $this->line12description = $line12description;

        return $this;
    }

    /**
     * Get line12description
     *
     * @return string 
     */
    public function getLine12description()
    {
        return $this->line12description;
    }

    /**
     * Set line12colorid
     *
     * @param integer $line12colorid
     * @return Articlecolor
     */
    public function setLine12colorid($line12colorid)
    {
        $this->line12colorid = $line12colorid;

        return $this;
    }

    /**
     * Get line12colorid
     *
     * @return integer 
     */
    public function getLine12colorid()
    {
        return $this->line12colorid;
    }

    /**
     * Set ordereddate
     *
     * @param \DateTime $ordereddate
     * @return Articlecolor
     */
    public function setOrdereddate($ordereddate)
    {
        $this->ordereddate = $ordereddate;

        return $this;
    }

    /**
     * Get ordereddate
     *
     * @return \DateTime 
     */
    public function getOrdereddate()
    {
        return $this->ordereddate;
    }

    /**
     * Set receiveddate
     *
     * @param \DateTime $receiveddate
     * @return Articlecolor
     */
    public function setReceiveddate($receiveddate)
    {
        $this->receiveddate = $receiveddate;

        return $this;
    }

    /**
     * Get receiveddate
     *
     * @return \DateTime 
     */
    public function getReceiveddate()
    {
        return $this->receiveddate;
    }

    /**
     * Set rejecteddate
     *
     * @param \DateTime $rejecteddate
     * @return Articlecolor
     */
    public function setRejecteddate($rejecteddate)
    {
        $this->rejecteddate = $rejecteddate;

        return $this;
    }

    /**
     * Get rejecteddate
     *
     * @return \DateTime 
     */
    public function getRejecteddate()
    {
        return $this->rejecteddate;
    }

    /**
     * Set approveddate
     *
     * @param \DateTime $approveddate
     * @return Articlecolor
     */
    public function setApproveddate($approveddate)
    {
        $this->approveddate = $approveddate;

        return $this;
    }

    /**
     * Get approveddate
     *
     * @return \DateTime 
     */
    public function getApproveddate()
    {
        return $this->approveddate;
    }

    /**
     * Set customercompanyid
     *
     * @param integer $customercompanyid
     * @return Articlecolor
     */
    public function setCustomercompanyid($customercompanyid)
    {
        $this->customercompanyid = $customercompanyid;

        return $this;
    }

    /**
     * Get customercompanyid
     *
     * @return integer 
     */
    public function getCustomercompanyid()
    {
        return $this->customercompanyid;
    }

    /**
     * Set salesuserid
     *
     * @param integer $salesuserid
     * @return Articlecolor
     */
    public function setSalesuserid($salesuserid)
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    /**
     * Get salesuserid
     *
     * @return integer 
     */
    public function getSalesuserid()
    {
        return $this->salesuserid;
    }

    /**
     * Set projectid
     *
     * @param integer $projectid
     * @return Articlecolor
     */
    public function setProjectid($projectid)
    {
        $this->projectid = $projectid;

        return $this;
    }

    /**
     * Get projectid
     *
     * @return integer 
     */
    public function getProjectid()
    {
        return $this->projectid;
    }

    /**
     * Set labdyecomment
     *
     * @param string $labdyecomment
     * @return Articlecolor
     */
    public function setLabdyecomment($labdyecomment)
    {
        $this->labdyecomment = $labdyecomment;

        return $this;
    }

    /**
     * Get labdyecomment
     *
     * @return string 
     */
    public function getLabdyecomment()
    {
        return $this->labdyecomment;
    }

    /**
     * Set approveduserid
     *
     * @param integer $approveduserid
     * @return Articlecolor
     */
    public function setApproveduserid($approveduserid)
    {
        $this->approveduserid = $approveduserid;

        return $this;
    }

    /**
     * Get approveduserid
     *
     * @return integer 
     */
    public function getApproveduserid()
    {
        return $this->approveduserid;
    }

    /**
     * Set approveduserdate
     *
     * @param \DateTime $approveduserdate
     * @return Articlecolor
     */
    public function setApproveduserdate($approveduserdate)
    {
        $this->approveduserdate = $approveduserdate;

        return $this;
    }

    /**
     * Get approveduserdate
     *
     * @return \DateTime 
     */
    public function getApproveduserdate()
    {
        return $this->approveduserdate;
    }

    /**
     * Set receiveduserid
     *
     * @param integer $receiveduserid
     * @return Articlecolor
     */
    public function setReceiveduserid($receiveduserid)
    {
        $this->receiveduserid = $receiveduserid;

        return $this;
    }

    /**
     * Get receiveduserid
     *
     * @return integer 
     */
    public function getReceiveduserid()
    {
        return $this->receiveduserid;
    }

    /**
     * Set receiveduserdate
     *
     * @param \DateTime $receiveduserdate
     * @return Articlecolor
     */
    public function setReceiveduserdate($receiveduserdate)
    {
        $this->receiveduserdate = $receiveduserdate;

        return $this;
    }

    /**
     * Get receiveduserdate
     *
     * @return \DateTime 
     */
    public function getReceiveduserdate()
    {
        return $this->receiveduserdate;
    }

    /**
     * Set ordereduserid
     *
     * @param integer $ordereduserid
     * @return Articlecolor
     */
    public function setOrdereduserid($ordereduserid)
    {
        $this->ordereduserid = $ordereduserid;

        return $this;
    }

    /**
     * Get ordereduserid
     *
     * @return integer 
     */
    public function getOrdereduserid()
    {
        return $this->ordereduserid;
    }

    /**
     * Set ordereduserdate
     *
     * @param \DateTime $ordereduserdate
     * @return Articlecolor
     */
    public function setOrdereduserdate($ordereduserdate)
    {
        $this->ordereduserdate = $ordereduserdate;

        return $this;
    }

    /**
     * Get ordereduserdate
     *
     * @return \DateTime 
     */
    public function getOrdereduserdate()
    {
        return $this->ordereduserdate;
    }

    /**
     * Set receivecompanyid
     *
     * @param integer $receivecompanyid
     * @return Articlecolor
     */
    public function setReceivecompanyid($receivecompanyid)
    {
        $this->receivecompanyid = $receivecompanyid;

        return $this;
    }

    /**
     * Get receivecompanyid
     *
     * @return integer 
     */
    public function getReceivecompanyid()
    {
        return $this->receivecompanyid;
    }

    /**
     * Set archived
     *
     * @param boolean $archived
     * @return Articlecolor
     */
    public function setArchived($archived)
    {
        $this->archived = $archived;

        return $this;
    }

    /**
     * Get archived
     *
     * @return boolean 
     */
    public function getArchived()
    {
        return $this->archived;
    }

    /**
     * Set archiveddate
     *
     * @param \DateTime $archiveddate
     * @return Articlecolor
     */
    public function setArchiveddate($archiveddate)
    {
        $this->archiveddate = $archiveddate;

        return $this;
    }

    /**
     * Get archiveddate
     *
     * @return \DateTime 
     */
    public function getArchiveddate()
    {
        return $this->archiveddate;
    }

    /**
     * Set archiveduserid
     *
     * @param integer $archiveduserid
     * @return Articlecolor
     */
    public function setArchiveduserid($archiveduserid)
    {
        $this->archiveduserid = $archiveduserid;

        return $this;
    }

    /**
     * Get archiveduserid
     *
     * @return integer 
     */
    public function getArchiveduserid()
    {
        return $this->archiveduserid;
    }

    /**
     * Set orderedqty
     *
     * @param string $orderedqty
     * @return Articlecolor
     */
    public function setOrderedqty($orderedqty)
    {
        $this->orderedqty = $orderedqty;

        return $this;
    }

    /**
     * Get orderedqty
     *
     * @return string 
     */
    public function getOrderedqty()
    {
        return $this->orderedqty;
    }

    /**
     * Set receivedqty
     *
     * @param string $receivedqty
     * @return Articlecolor
     */
    public function setReceivedqty($receivedqty)
    {
        $this->receivedqty = $receivedqty;

        return $this;
    }

    /**
     * Get receivedqty
     *
     * @return string 
     */
    public function getReceivedqty()
    {
        return $this->receivedqty;
    }

    /**
     * Set sexid
     *
     * @param integer $sexid
     * @return Articlecolor
     */
    public function setSexid($sexid)
    {
        $this->sexid = $sexid;

        return $this;
    }

    /**
     * Get sexid
     *
     * @return integer 
     */
    public function getSexid()
    {
        return $this->sexid;
    }
}
