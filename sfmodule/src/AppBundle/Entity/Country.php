<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation\ExclusionPolicy;
use JMS\Serializer\Annotation\Exclude;

/**
 * Country
 *
 * @@ExclusionPolicy("None")
 */
class Country
{
    /**
     * @var integer
     *
     */
    private $id;

    /**
     * @var \DateTime
     *
     * @Exclude
     */
    private $createdate;

    /**
     * @var integer
     *
     * @Exclude
     */
    private $createuserid;

    /**
     * @var \DateTime
     *
     * @Exclude
     */
    private $modifydate;

    /**
     * @var integer
     *
     * @Exclude
     */
    private $modifyuserid;

    /**
     * @var boolean
     *
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var boolean
     */
    private $eumember;

    /**
     * @var boolean
     */
    private $eftamember;

    /**
     * @var string
     */
    private $countrygroup;

    /**
     * @var string
     */
    private $vatpercentage;

    /**
     * @var boolean
     *
     * @Exclude
     */
    private $invoicecustomsinfo;

    /**
     * @var string
     *
      * @Exclude
    */
    private $invoicefooter;

    /**
     * @var string
     */
    private $isocode;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Country
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Country
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Country
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Country
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Country
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Country
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Country
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set eumember
     *
     * @param boolean $eumember
     * @return Country
     */
    public function setEumember($eumember)
    {
        $this->eumember = $eumember;

        return $this;
    }

    /**
     * Get eumember
     *
     * @return boolean 
     */
    public function getEumember()
    {
        return $this->eumember;
    }

    /**
     * Set eftamember
     *
     * @param boolean $eftamember
     * @return Country
     */
    public function setEftamember($eftamember)
    {
        $this->eftamember = $eftamember;

        return $this;
    }

    /**
     * Get eftamember
     *
     * @return boolean 
     */
    public function getEftamember()
    {
        return $this->eftamember;
    }

    /**
     * Set countrygroup
     *
     * @param string $countrygroup
     * @return Country
     */
    public function setCountrygroup($countrygroup)
    {
        $this->countrygroup = $countrygroup;

        return $this;
    }

    /**
     * Get countrygroup
     *
     * @return string 
     */
    public function getCountrygroup()
    {
        return $this->countrygroup;
    }

    /**
     * Set vatpercentage
     *
     * @param string $vatpercentage
     * @return Country
     */
    public function setVatpercentage($vatpercentage)
    {
        $this->vatpercentage = $vatpercentage;

        return $this;
    }

    /**
     * Get vatpercentage
     *
     * @return string 
     */
    public function getVatpercentage()
    {
        return $this->vatpercentage;
    }

    /**
     * Set invoicecustomsinfo
     *
     * @param boolean $invoicecustomsinfo
     * @return Country
     */
    public function setInvoicecustomsinfo($invoicecustomsinfo)
    {
        $this->invoicecustomsinfo = $invoicecustomsinfo;

        return $this;
    }

    /**
     * Get invoicecustomsinfo
     *
     * @return boolean 
     */
    public function getInvoicecustomsinfo()
    {
        return $this->invoicecustomsinfo;
    }

    /**
     * Set invoicefooter
     *
     * @param string $invoicefooter
     * @return Country
     */
    public function setInvoicefooter($invoicefooter)
    {
        $this->invoicefooter = $invoicefooter;

        return $this;
    }

    /**
     * Get invoicefooter
     *
     * @return string 
     */
    public function getInvoicefooter()
    {
        return $this->invoicefooter;
    }

	    /**
     * Set isocode
     *
     * @param string $isocode
     * @return Country
     */
    public function setIsocode($isocode)
    {
        $this->isocode = $isocode;

        return $this;
    }

    /**
     * Get isocode
     *
     * @return string 
     */
    public function getIsocode()
    {
        return $this->isocode;
    }

}
