<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylesketchversion
 */
class Stylesketchversion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var boolean
     */
    private $constructiondone;

    /**
     * @var boolean
     */
    private $technologydone;

    /**
     * @var \DateTime
     */
    private $constructiondonedate;

    /**
     * @var integer
     */
    private $constructiondoneuserid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set constructiondone
     *
     * @param boolean $constructiondone
     * @return Stylesketchversion
     */
    public function setConstructiondone($constructiondone)
    {
        $this->constructiondone = $constructiondone;

        return $this;
    }

    /**
     * Get constructiondone
     *
     * @return boolean 
     */
    public function getConstructiondone()
    {
        return $this->constructiondone;
    }

    /**
     * Set technologydone
     *
     * @param boolean $technologydone
     * @return Stylesketchversion
     */
    public function setTechnologydone($technologydone)
    {
        $this->technologydone = $technologydone;

        return $this;
    }

    /**
     * Get technologydone
     *
     * @return boolean 
     */
    public function getTechnologydone()
    {
        return $this->technologydone;
    }

    /**
     * Set constructiondonedate
     *
     * @param \DateTime $constructiondonedate
     * @return Stylesketchversion
     */
    public function setConstructiondonedate($constructiondonedate)
    {
        $this->constructiondonedate = $constructiondonedate;

        return $this;
    }

    /**
     * Get constructiondonedate
     *
     * @return \DateTime 
     */
    public function getConstructiondonedate()
    {
        return $this->constructiondonedate;
    }

    /**
     * Set constructiondoneuserid
     *
     * @param integer $constructiondoneuserid
     * @return Stylesketchversion
     */
    public function setConstructiondoneuserid($constructiondoneuserid)
    {
        $this->constructiondoneuserid = $constructiondoneuserid;

        return $this;
    }

    /**
     * Get constructiondoneuserid
     *
     * @return integer 
     */
    public function getConstructiondoneuserid()
    {
        return $this->constructiondoneuserid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylesketchversion
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylesketchversion
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylesketchversion
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylesketchversion
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
