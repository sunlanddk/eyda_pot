<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Seasonallowuser
 */
class Seasonallowuser
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $seasonid;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $agentfee;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Seasonallowuser
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer 
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Seasonallowuser
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Seasonallowuser
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Seasonallowuser
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Seasonallowuser
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Seasonallowuser
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Seasonallowuser
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set agentfee
     *
     * @param integer $agentfee
     * @return Seasonallowuser
     */
    public function setAgentfee($agentfee)
    {
        $this->agentfee = $agentfee;

        return $this;
    }

    /**
     * Get agentfee
     *
     * @return integer 
     */
    public function getAgentfee()
    {
        return $this->agentfee;
    }
}
