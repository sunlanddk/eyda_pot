<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Company
 */
class Company
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $phonemain;

    /**
     * @var string
     */
    private $address1;

    /**
     * @var string
     */
    private $address2;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var boolean
     */
    private $internal;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var string
     */
    private $number;

    /**
     * @var integer
     */
    private $countryid;

    /**
     * @var boolean
     */
    private $typecustomer;

    /**
     * @var boolean
     */
    private $typesupplier;

    /**
     * @var string
     */
    private $phonefax;

    /**
     * @var string
     */
    private $mail;

    /**
     * @var string
     */
    private $regnumber;

    /**
     * @var boolean
     */
    private $typecarrier;

    /**
     * @var string
     */
    private $invoiceaddress1;

    /**
     * @var string
     */
    private $invoiceaddress2;

    /**
     * @var string
     */
    private $invoicezip;

    /**
     * @var string
     */
    private $invoicecity;

    /**
     * @var integer
     */
    private $invoicecountryid;

    /**
     * @var string
     */
    private $deliveryaddress1;

    /**
     * @var string
     */
    private $deliveryaddress2;

    /**
     * @var string
     */
    private $deliveryzip;

    /**
     * @var string
     */
    private $deliverycity;

    /**
     * @var integer
     */
    private $deliverycountryid;

    /**
     * @var integer
     */
    private $carrierid;

    /**
     * @var integer
     */
    private $deliverytermid;

    /**
     * @var integer
     */
    private $paymenttermid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var string
     */
    private $vatnumber;

    /**
     * @var integer
     */
    private $salesuserid;

    /**
     * @var boolean
     */
    private $invoicesplit;

    /**
     * @var boolean
     */
    private $customsdeclaration;

    /**
     * @var boolean
     */
    private $invoiceorigin;

    /**
     * @var boolean
     */
    private $invoicecomposition;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $companyfooter;

    /**
     * @var integer
     */
    private $invoiceheader;

    /**
     * @var integer
     */
    private $listhidden;

    /**
     * @var integer
     */
    private $orderlinefooter;

    /**
     * @var integer
     */
    private $surplus;

    /**
     * @var integer
     */
    private $headconstructoruserid;

    /**
     * @var boolean
     */
    private $typeagent;

    /**
     * @var integer
     */
    private $agentid;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var string
     */
    private $fee;

    /**
     * @var string
     */
    private $cashdiscount;

    /**
     * @var string
     */
    private $bonus;

    /**
     * @var string
     */
    private $targetprofit;

    /**
     * @var integer
     */
    private $purchaserefid;

    /**
     * @var integer
     */
    private $purchasecarrierid;

    /**
     * @var integer
     */
    private $purchasedeliverytermid;

    /**
     * @var string
     */
    private $purchaseagent;

    /**
     * @var integer
     */
    private $purchasecurrencyid;

    /**
     * @var integer
     */
    private $purchasepaymenttermid;

    /**
     * @var string
     */
    private $purchasesurplus;

    /**
     * @var string
     */
    private $gpkey;

    /**
     * @var integer
     */
    private $customerclassid;

    /**
     * @var integer
     */
    private $supplierclassid;

    /**
     * @var integer
     */
    private $tocompanyid;

    /**
     * @var boolean
     */
    private $prospect;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Company
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Company
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set phonemain
     *
     * @param string $phonemain
     * @return Company
     */
    public function setPhonemain($phonemain)
    {
        $this->phonemain = $phonemain;

        return $this;
    }

    /**
     * Get phonemain
     *
     * @return string 
     */
    public function getPhonemain()
    {
        return $this->phonemain;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return Company
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return Company
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return Company
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return Company
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Company
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Company
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set internal
     *
     * @param boolean $internal
     * @return Company
     */
    public function setInternal($internal)
    {
        $this->internal = $internal;

        return $this;
    }

    /**
     * Get internal
     *
     * @return boolean 
     */
    public function getInternal()
    {
        return $this->internal;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Company
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Company
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Company
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Company
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set typecustomer
     *
     * @param boolean $typecustomer
     * @return Company
     */
    public function setTypecustomer($typecustomer)
    {
        $this->typecustomer = $typecustomer;

        return $this;
    }

    /**
     * Get typecustomer
     *
     * @return boolean 
     */
    public function getTypecustomer()
    {
        return $this->typecustomer;
    }

    /**
     * Set typesupplier
     *
     * @param boolean $typesupplier
     * @return Company
     */
    public function setTypesupplier($typesupplier)
    {
        $this->typesupplier = $typesupplier;

        return $this;
    }

    /**
     * Get typesupplier
     *
     * @return boolean 
     */
    public function getTypesupplier()
    {
        return $this->typesupplier;
    }

    /**
     * Set phonefax
     *
     * @param string $phonefax
     * @return Company
     */
    public function setPhonefax($phonefax)
    {
        $this->phonefax = $phonefax;

        return $this;
    }

    /**
     * Get phonefax
     *
     * @return string 
     */
    public function getPhonefax()
    {
        return $this->phonefax;
    }

    /**
     * Set mail
     *
     * @param string $mail
     * @return Company
     */
    public function setMail($mail)
    {
        $this->mail = $mail;

        return $this;
    }

    /**
     * Get mail
     *
     * @return string 
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * Set regnumber
     *
     * @param string $regnumber
     * @return Company
     */
    public function setRegnumber($regnumber)
    {
        $this->regnumber = $regnumber;

        return $this;
    }

    /**
     * Get regnumber
     *
     * @return string 
     */
    public function getRegnumber()
    {
        return $this->regnumber;
    }

    /**
     * Set typecarrier
     *
     * @param boolean $typecarrier
     * @return Company
     */
    public function setTypecarrier($typecarrier)
    {
        $this->typecarrier = $typecarrier;

        return $this;
    }

    /**
     * Get typecarrier
     *
     * @return boolean 
     */
    public function getTypecarrier()
    {
        return $this->typecarrier;
    }

    /**
     * Set invoiceaddress1
     *
     * @param string $invoiceaddress1
     * @return Company
     */
    public function setInvoiceaddress1($invoiceaddress1)
    {
        $this->invoiceaddress1 = $invoiceaddress1;

        return $this;
    }

    /**
     * Get invoiceaddress1
     *
     * @return string 
     */
    public function getInvoiceaddress1()
    {
        return $this->invoiceaddress1;
    }

    /**
     * Set invoiceaddress2
     *
     * @param string $invoiceaddress2
     * @return Company
     */
    public function setInvoiceaddress2($invoiceaddress2)
    {
        $this->invoiceaddress2 = $invoiceaddress2;

        return $this;
    }

    /**
     * Get invoiceaddress2
     *
     * @return string 
     */
    public function getInvoiceaddress2()
    {
        return $this->invoiceaddress2;
    }

    /**
     * Set invoicezip
     *
     * @param string $invoicezip
     * @return Company
     */
    public function setInvoicezip($invoicezip)
    {
        $this->invoicezip = $invoicezip;

        return $this;
    }

    /**
     * Get invoicezip
     *
     * @return string 
     */
    public function getInvoicezip()
    {
        return $this->invoicezip;
    }

    /**
     * Set invoicecity
     *
     * @param string $invoicecity
     * @return Company
     */
    public function setInvoicecity($invoicecity)
    {
        $this->invoicecity = $invoicecity;

        return $this;
    }

    /**
     * Get invoicecity
     *
     * @return string 
     */
    public function getInvoicecity()
    {
        return $this->invoicecity;
    }

    /**
     * Set invoicecountryid
     *
     * @param integer $invoicecountryid
     * @return Company
     */
    public function setInvoicecountryid($invoicecountryid)
    {
        $this->invoicecountryid = $invoicecountryid;

        return $this;
    }

    /**
     * Get invoicecountryid
     *
     * @return integer 
     */
    public function getInvoicecountryid()
    {
        return $this->invoicecountryid;
    }

    /**
     * Set deliveryaddress1
     *
     * @param string $deliveryaddress1
     * @return Company
     */
    public function setDeliveryaddress1($deliveryaddress1)
    {
        $this->deliveryaddress1 = $deliveryaddress1;

        return $this;
    }

    /**
     * Get deliveryaddress1
     *
     * @return string 
     */
    public function getDeliveryaddress1()
    {
        return $this->deliveryaddress1;
    }

    /**
     * Set deliveryaddress2
     *
     * @param string $deliveryaddress2
     * @return Company
     */
    public function setDeliveryaddress2($deliveryaddress2)
    {
        $this->deliveryaddress2 = $deliveryaddress2;

        return $this;
    }

    /**
     * Get deliveryaddress2
     *
     * @return string 
     */
    public function getDeliveryaddress2()
    {
        return $this->deliveryaddress2;
    }

    /**
     * Set deliveryzip
     *
     * @param string $deliveryzip
     * @return Company
     */
    public function setDeliveryzip($deliveryzip)
    {
        $this->deliveryzip = $deliveryzip;

        return $this;
    }

    /**
     * Get deliveryzip
     *
     * @return string 
     */
    public function getDeliveryzip()
    {
        return $this->deliveryzip;
    }

    /**
     * Set deliverycity
     *
     * @param string $deliverycity
     * @return Company
     */
    public function setDeliverycity($deliverycity)
    {
        $this->deliverycity = $deliverycity;

        return $this;
    }

    /**
     * Get deliverycity
     *
     * @return string 
     */
    public function getDeliverycity()
    {
        return $this->deliverycity;
    }

    /**
     * Set deliverycountryid
     *
     * @param integer $deliverycountryid
     * @return Company
     */
    public function setDeliverycountryid($deliverycountryid)
    {
        $this->deliverycountryid = $deliverycountryid;

        return $this;
    }

    /**
     * Get deliverycountryid
     *
     * @return integer 
     */
    public function getDeliverycountryid()
    {
        return $this->deliverycountryid;
    }

    /**
     * Set carrierid
     *
     * @param integer $carrierid
     * @return Company
     */
    public function setCarrierid($carrierid)
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    /**
     * Get carrierid
     *
     * @return integer 
     */
    public function getCarrierid()
    {
        return $this->carrierid;
    }

    /**
     * Set deliverytermid
     *
     * @param integer $deliverytermid
     * @return Company
     */
    public function setDeliverytermid($deliverytermid)
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    /**
     * Get deliverytermid
     *
     * @return integer 
     */
    public function getDeliverytermid()
    {
        return $this->deliverytermid;
    }

    /**
     * Set paymenttermid
     *
     * @param integer $paymenttermid
     * @return Company
     */
    public function setPaymenttermid($paymenttermid)
    {
        $this->paymenttermid = $paymenttermid;

        return $this;
    }

    /**
     * Get paymenttermid
     *
     * @return integer 
     */
    public function getPaymenttermid()
    {
        return $this->paymenttermid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Company
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set vatnumber
     *
     * @param string $vatnumber
     * @return Company
     */
    public function setVatnumber($vatnumber)
    {
        $this->vatnumber = $vatnumber;

        return $this;
    }

    /**
     * Get vatnumber
     *
     * @return string 
     */
    public function getVatnumber()
    {
        return $this->vatnumber;
    }

    /**
     * Set salesuserid
     *
     * @param integer $salesuserid
     * @return Company
     */
    public function setSalesuserid($salesuserid)
    {
        $this->salesuserid = $salesuserid;

        return $this;
    }

    /**
     * Get salesuserid
     *
     * @return integer 
     */
    public function getSalesuserid()
    {
        return $this->salesuserid;
    }

    /**
     * Set invoicesplit
     *
     * @param boolean $invoicesplit
     * @return Company
     */
    public function setInvoicesplit($invoicesplit)
    {
        $this->invoicesplit = $invoicesplit;

        return $this;
    }

    /**
     * Get invoicesplit
     *
     * @return boolean 
     */
    public function getInvoicesplit()
    {
        return $this->invoicesplit;
    }

    /**
     * Set customsdeclaration
     *
     * @param boolean $customsdeclaration
     * @return Company
     */
    public function setCustomsdeclaration($customsdeclaration)
    {
        $this->customsdeclaration = $customsdeclaration;

        return $this;
    }

    /**
     * Get customsdeclaration
     *
     * @return boolean 
     */
    public function getCustomsdeclaration()
    {
        return $this->customsdeclaration;
    }

    /**
     * Set invoiceorigin
     *
     * @param boolean $invoiceorigin
     * @return Company
     */
    public function setInvoiceorigin($invoiceorigin)
    {
        $this->invoiceorigin = $invoiceorigin;

        return $this;
    }

    /**
     * Get invoiceorigin
     *
     * @return boolean 
     */
    public function getInvoiceorigin()
    {
        return $this->invoiceorigin;
    }

    /**
     * Set invoicecomposition
     *
     * @param boolean $invoicecomposition
     * @return Company
     */
    public function setInvoicecomposition($invoicecomposition)
    {
        $this->invoicecomposition = $invoicecomposition;

        return $this;
    }

    /**
     * Get invoicecomposition
     *
     * @return boolean 
     */
    public function getInvoicecomposition()
    {
        return $this->invoicecomposition;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Company
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set companyfooter
     *
     * @param string $companyfooter
     * @return Company
     */
    public function setCompanyfooter($companyfooter)
    {
        $this->companyfooter = $companyfooter;

        return $this;
    }

    /**
     * Get companyfooter
     *
     * @return string 
     */
    public function getCompanyfooter()
    {
        return $this->companyfooter;
    }

    /**
     * Set invoiceheader
     *
     * @param integer $invoiceheader
     * @return Company
     */
    public function setInvoiceheader($invoiceheader)
    {
        $this->invoiceheader = $invoiceheader;

        return $this;
    }

    /**
     * Get invoiceheader
     *
     * @return integer 
     */
    public function getInvoiceheader()
    {
        return $this->invoiceheader;
    }

    /**
     * Set listhidden
     *
     * @param integer $listhidden
     * @return Company
     */
    public function setListhidden($listhidden)
    {
        $this->listhidden = $listhidden;

        return $this;
    }

    /**
     * Get listhidden
     *
     * @return integer 
     */
    public function getListhidden()
    {
        return $this->listhidden;
    }

    /**
     * Set orderlinefooter
     *
     * @param integer $orderlinefooter
     * @return Company
     */
    public function setOrderlinefooter($orderlinefooter)
    {
        $this->orderlinefooter = $orderlinefooter;

        return $this;
    }

    /**
     * Get orderlinefooter
     *
     * @return integer 
     */
    public function getOrderlinefooter()
    {
        return $this->orderlinefooter;
    }

    /**
     * Set surplus
     *
     * @param integer $surplus
     * @return Company
     */
    public function setSurplus($surplus)
    {
        $this->surplus = $surplus;

        return $this;
    }

    /**
     * Get surplus
     *
     * @return integer 
     */
    public function getSurplus()
    {
        return $this->surplus;
    }

    /**
     * Set headconstructoruserid
     *
     * @param integer $headconstructoruserid
     * @return Company
     */
    public function setHeadconstructoruserid($headconstructoruserid)
    {
        $this->headconstructoruserid = $headconstructoruserid;

        return $this;
    }

    /**
     * Get headconstructoruserid
     *
     * @return integer 
     */
    public function getHeadconstructoruserid()
    {
        return $this->headconstructoruserid;
    }

    /**
     * Set typeagent
     *
     * @param boolean $typeagent
     * @return Company
     */
    public function setTypeagent($typeagent)
    {
        $this->typeagent = $typeagent;

        return $this;
    }

    /**
     * Get typeagent
     *
     * @return boolean 
     */
    public function getTypeagent()
    {
        return $this->typeagent;
    }

    /**
     * Set agentid
     *
     * @param integer $agentid
     * @return Company
     */
    public function setAgentid($agentid)
    {
        $this->agentid = $agentid;

        return $this;
    }

    /**
     * Get agentid
     *
     * @return integer 
     */
    public function getAgentid()
    {
        return $this->agentid;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Company
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set fee
     *
     * @param string $fee
     * @return Company
     */
    public function setFee($fee)
    {
        $this->fee = $fee;

        return $this;
    }

    /**
     * Get fee
     *
     * @return string 
     */
    public function getFee()
    {
        return $this->fee;
    }

    /**
     * Set cashdiscount
     *
     * @param string $cashdiscount
     * @return Company
     */
    public function setCashdiscount($cashdiscount)
    {
        $this->cashdiscount = $cashdiscount;

        return $this;
    }

    /**
     * Get cashdiscount
     *
     * @return string 
     */
    public function getCashdiscount()
    {
        return $this->cashdiscount;
    }

    /**
     * Set bonus
     *
     * @param string $bonus
     * @return Company
     */
    public function setBonus($bonus)
    {
        $this->bonus = $bonus;

        return $this;
    }

    /**
     * Get bonus
     *
     * @return string 
     */
    public function getBonus()
    {
        return $this->bonus;
    }

    /**
     * Set targetprofit
     *
     * @param string $targetprofit
     * @return Company
     */
    public function setTargetprofit($targetprofit)
    {
        $this->targetprofit = $targetprofit;

        return $this;
    }

    /**
     * Get targetprofit
     *
     * @return string 
     */
    public function getTargetprofit()
    {
        return $this->targetprofit;
    }

    /**
     * Set purchaserefid
     *
     * @param integer $purchaserefid
     * @return Company
     */
    public function setPurchaserefid($purchaserefid)
    {
        $this->purchaserefid = $purchaserefid;

        return $this;
    }

    /**
     * Get purchaserefid
     *
     * @return integer 
     */
    public function getPurchaserefid()
    {
        return $this->purchaserefid;
    }

    /**
     * Set purchasecarrierid
     *
     * @param integer $purchasecarrierid
     * @return Company
     */
    public function setPurchasecarrierid($purchasecarrierid)
    {
        $this->purchasecarrierid = $purchasecarrierid;

        return $this;
    }

    /**
     * Get purchasecarrierid
     *
     * @return integer 
     */
    public function getPurchasecarrierid()
    {
        return $this->purchasecarrierid;
    }

    /**
     * Set purchasedeliverytermid
     *
     * @param integer $purchasedeliverytermid
     * @return Company
     */
    public function setPurchasedeliverytermid($purchasedeliverytermid)
    {
        $this->purchasedeliverytermid = $purchasedeliverytermid;

        return $this;
    }

    /**
     * Get purchasedeliverytermid
     *
     * @return integer 
     */
    public function getPurchasedeliverytermid()
    {
        return $this->purchasedeliverytermid;
    }

    /**
     * Set purchaseagent
     *
     * @param string $purchaseagent
     * @return Company
     */
    public function setPurchaseagent($purchaseagent)
    {
        $this->purchaseagent = $purchaseagent;

        return $this;
    }

    /**
     * Get purchaseagent
     *
     * @return string 
     */
    public function getPurchaseagent()
    {
        return $this->purchaseagent;
    }

    /**
     * Set purchasecurrencyid
     *
     * @param integer $purchasecurrencyid
     * @return Company
     */
    public function setPurchasecurrencyid($purchasecurrencyid)
    {
        $this->purchasecurrencyid = $purchasecurrencyid;

        return $this;
    }

    /**
     * Get purchasecurrencyid
     *
     * @return integer 
     */
    public function getPurchasecurrencyid()
    {
        return $this->purchasecurrencyid;
    }

    /**
     * Set purchasepaymenttermid
     *
     * @param integer $purchasepaymenttermid
     * @return Company
     */
    public function setPurchasepaymenttermid($purchasepaymenttermid)
    {
        $this->purchasepaymenttermid = $purchasepaymenttermid;

        return $this;
    }

    /**
     * Get purchasepaymenttermid
     *
     * @return integer 
     */
    public function getPurchasepaymenttermid()
    {
        return $this->purchasepaymenttermid;
    }

    /**
     * Set purchasesurplus
     *
     * @param string $purchasesurplus
     * @return Company
     */
    public function setPurchasesurplus($purchasesurplus)
    {
        $this->purchasesurplus = $purchasesurplus;

        return $this;
    }

    /**
     * Get purchasesurplus
     *
     * @return string 
     */
    public function getPurchasesurplus()
    {
        return $this->purchasesurplus;
    }

    /**
     * Set gpkey
     *
     * @param string $gpkey
     * @return Company
     */
    public function setGpkey($gpkey)
    {
        $this->gpkey = $gpkey;

        return $this;
    }

    /**
     * Get gpkey
     *
     * @return string 
     */
    public function getGpkey()
    {
        return $this->gpkey;
    }

    /**
     * Set customerclassid
     *
     * @param integer $customerclassid
     * @return Company
     */
    public function setCustomerclassid($customerclassid)
    {
        $this->customerclassid = $customerclassid;

        return $this;
    }

    /**
     * Get customerclassid
     *
     * @return integer 
     */
    public function getCustomerclassid()
    {
        return $this->customerclassid;
    }

    /**
     * Set supplierclassid
     *
     * @param integer $supplierclassid
     * @return Company
     */
    public function setSupplierclassid($supplierclassid)
    {
        $this->supplierclassid = $supplierclassid;

        return $this;
    }

    /**
     * Get supplierclassid
     *
     * @return integer 
     */
    public function getSupplierclassid()
    {
        return $this->supplierclassid;
    }

    /**
     * Set tocompanyid
     *
     * @param integer $tocompanyid
     * @return Company
     */
    public function setTocompanyid($tocompanyid)
    {
        $this->tocompanyid = $tocompanyid;

        return $this;
    }

    /**
     * Get tocompanyid
     *
     * @return integer 
     */
    public function getTocompanyid()
    {
        return $this->tocompanyid;
    }

    /**
     * Set prospect
     *
     * @param boolean $prospect
     * @return Company
     */
    public function setProspect($prospect)
    {
        $this->prospect = $prospect;

        return $this;
    }

    /**
     * Get prospect
     *
     * @return boolean 
     */
    public function getProspect()
    {
        return $this->prospect;
    }
}
