<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * User
 */
class User
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $firstname;

    /**
     * @var string
     */
    private $lastname;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var string
     */
    private $loginname;

    /**
     * @var string
     */
    private $password;

    /**
     * @var string
     */
    private $title;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $projectid;

    /**
     * @var boolean
     */
    private $adminsystem;

    /**
     * @var string
     */
    private $phonebusiness;

    /**
     * @var string
     */
    private $phonemobile;

    /**
     * @var string
     */
    private $phoneprivate;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $address1;

    /**
     * @var string
     */
    private $address2;

    /**
     * @var string
     */
    private $zip;

    /**
     * @var string
     */
    private $city;

    /**
     * @var string
     */
    private $country;

    /**
     * @var string
     */
    private $phonedirect;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var boolean
     */
    private $projectselect;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $timezoneid;

    /**
     * @var integer
     */
    private $roleid;

    /**
     * @var boolean
     */
    private $login;

    /**
     * @var \DateTime
     */
    private $lastnewsdate;

    /**
     * @var boolean
     */
    private $extern;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var boolean
     */
    private $restricted;

    /**
     * @var string
     */
    private $companylist;

    /**
     * @var string
     */
    private $userlist;

    /**
     * @var integer
     */
    private $salesref;

    /**
     * @var integer
     */
    private $agentcompanyid;

    /**
     * @var integer
     */
    private $agentmanager;

    /**
     * @var integer
     */
    private $avatarid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set firstname
     *
     * @param string $firstname
     * @return User
     */
    public function setFirstname($firstname)
    {
        $this->firstname = $firstname;

        return $this;
    }

    /**
     * Get firstname
     *
     * @return string 
     */
    public function getFirstname()
    {
        return $this->firstname;
    }

    /**
     * Set lastname
     *
     * @param string $lastname
     * @return User
     */
    public function setLastname($lastname)
    {
        $this->lastname = $lastname;

        return $this;
    }

    /**
     * Get lastname
     *
     * @return string 
     */
    public function getLastname()
    {
        return $this->lastname;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return User
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set loginname
     *
     * @param string $loginname
     * @return User
     */
    public function setLoginname($loginname)
    {
        $this->loginname = $loginname;

        return $this;
    }

    /**
     * Get loginname
     *
     * @return string 
     */
    public function getLoginname()
    {
        return $this->loginname;
    }

    /**
     * Set password
     *
     * @param string $password
     * @return User
     */
    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    /**
     * Get password
     *
     * @return string 
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return User
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return User
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set projectid
     *
     * @param integer $projectid
     * @return User
     */
    public function setProjectid($projectid)
    {
        $this->projectid = $projectid;

        return $this;
    }

    /**
     * Get projectid
     *
     * @return integer 
     */
    public function getProjectid()
    {
        return $this->projectid;
    }

    /**
     * Set adminsystem
     *
     * @param boolean $adminsystem
     * @return User
     */
    public function setAdminsystem($adminsystem)
    {
        $this->adminsystem = $adminsystem;

        return $this;
    }

    /**
     * Get adminsystem
     *
     * @return boolean 
     */
    public function getAdminsystem()
    {
        return $this->adminsystem;
    }

    /**
     * Set phonebusiness
     *
     * @param string $phonebusiness
     * @return User
     */
    public function setPhonebusiness($phonebusiness)
    {
        $this->phonebusiness = $phonebusiness;

        return $this;
    }

    /**
     * Get phonebusiness
     *
     * @return string 
     */
    public function getPhonebusiness()
    {
        return $this->phonebusiness;
    }

    /**
     * Set phonemobile
     *
     * @param string $phonemobile
     * @return User
     */
    public function setPhonemobile($phonemobile)
    {
        $this->phonemobile = $phonemobile;

        return $this;
    }

    /**
     * Get phonemobile
     *
     * @return string 
     */
    public function getPhonemobile()
    {
        return $this->phonemobile;
    }

    /**
     * Set phoneprivate
     *
     * @param string $phoneprivate
     * @return User
     */
    public function setPhoneprivate($phoneprivate)
    {
        $this->phoneprivate = $phoneprivate;

        return $this;
    }

    /**
     * Get phoneprivate
     *
     * @return string 
     */
    public function getPhoneprivate()
    {
        return $this->phoneprivate;
    }

    /**
     * Set email
     *
     * @param string $email
     * @return User
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string 
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set address1
     *
     * @param string $address1
     * @return User
     */
    public function setAddress1($address1)
    {
        $this->address1 = $address1;

        return $this;
    }

    /**
     * Get address1
     *
     * @return string 
     */
    public function getAddress1()
    {
        return $this->address1;
    }

    /**
     * Set address2
     *
     * @param string $address2
     * @return User
     */
    public function setAddress2($address2)
    {
        $this->address2 = $address2;

        return $this;
    }

    /**
     * Get address2
     *
     * @return string 
     */
    public function getAddress2()
    {
        return $this->address2;
    }

    /**
     * Set zip
     *
     * @param string $zip
     * @return User
     */
    public function setZip($zip)
    {
        $this->zip = $zip;

        return $this;
    }

    /**
     * Get zip
     *
     * @return string 
     */
    public function getZip()
    {
        return $this->zip;
    }

    /**
     * Set city
     *
     * @param string $city
     * @return User
     */
    public function setCity($city)
    {
        $this->city = $city;

        return $this;
    }

    /**
     * Get city
     *
     * @return string 
     */
    public function getCity()
    {
        return $this->city;
    }

    /**
     * Set country
     *
     * @param string $country
     * @return User
     */
    public function setCountry($country)
    {
        $this->country = $country;

        return $this;
    }

    /**
     * Get country
     *
     * @return string 
     */
    public function getCountry()
    {
        return $this->country;
    }

    /**
     * Set phonedirect
     *
     * @param string $phonedirect
     * @return User
     */
    public function setPhonedirect($phonedirect)
    {
        $this->phonedirect = $phonedirect;

        return $this;
    }

    /**
     * Get phonedirect
     *
     * @return string 
     */
    public function getPhonedirect()
    {
        return $this->phonedirect;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return User
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return User
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set projectselect
     *
     * @param boolean $projectselect
     * @return User
     */
    public function setProjectselect($projectselect)
    {
        $this->projectselect = $projectselect;

        return $this;
    }

    /**
     * Get projectselect
     *
     * @return boolean 
     */
    public function getProjectselect()
    {
        return $this->projectselect;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return User
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return User
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set timezoneid
     *
     * @param integer $timezoneid
     * @return User
     */
    public function setTimezoneid($timezoneid)
    {
        $this->timezoneid = $timezoneid;

        return $this;
    }

    /**
     * Get timezoneid
     *
     * @return integer 
     */
    public function getTimezoneid()
    {
        return $this->timezoneid;
    }

    /**
     * Set roleid
     *
     * @param integer $roleid
     * @return User
     */
    public function setRoleid($roleid)
    {
        $this->roleid = $roleid;

        return $this;
    }

    /**
     * Get roleid
     *
     * @return integer 
     */
    public function getRoleid()
    {
        return $this->roleid;
    }

    /**
     * Set login
     *
     * @param boolean $login
     * @return User
     */
    public function setLogin($login)
    {
        $this->login = $login;

        return $this;
    }

    /**
     * Get login
     *
     * @return boolean 
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Set lastnewsdate
     *
     * @param \DateTime $lastnewsdate
     * @return User
     */
    public function setLastnewsdate($lastnewsdate)
    {
        $this->lastnewsdate = $lastnewsdate;

        return $this;
    }

    /**
     * Get lastnewsdate
     *
     * @return \DateTime 
     */
    public function getLastnewsdate()
    {
        return $this->lastnewsdate;
    }

    /**
     * Set extern
     *
     * @param boolean $extern
     * @return User
     */
    public function setExtern($extern)
    {
        $this->extern = $extern;

        return $this;
    }

    /**
     * Get extern
     *
     * @return boolean 
     */
    public function getExtern()
    {
        return $this->extern;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return User
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set restricted
     *
     * @param boolean $restricted
     * @return User
     */
    public function setRestricted($restricted)
    {
        $this->restricted = $restricted;

        return $this;
    }

    /**
     * Get restricted
     *
     * @return boolean 
     */
    public function getRestricted()
    {
        return $this->restricted;
    }

    /**
     * Set companylist
     *
     * @param string $companylist
     * @return User
     */
    public function setCompanylist($companylist)
    {
        $this->companylist = $companylist;

        return $this;
    }

    /**
     * Get companylist
     *
     * @return string 
     */
    public function getCompanylist()
    {
        return $this->companylist;
    }

    /**
     * Set userlist
     *
     * @param string $userlist
     * @return User
     */
    public function setUserlist($userlist)
    {
        $this->userlist = $userlist;

        return $this;
    }

    /**
     * Get userlist
     *
     * @return string 
     */
    public function getUserlist()
    {
        return $this->userlist;
    }

    /**
     * Set salesref
     *
     * @param integer $salesref
     * @return User
     */
    public function setSalesref($salesref)
    {
        $this->salesref = $salesref;

        return $this;
    }

    /**
     * Get salesref
     *
     * @return integer 
     */
    public function getSalesref()
    {
        return $this->salesref;
    }

    /**
     * Set agentcompanyid
     *
     * @param integer $agentcompanyid
     * @return User
     */
    public function setAgentcompanyid($agentcompanyid)
    {
        $this->agentcompanyid = $agentcompanyid;

        return $this;
    }

    /**
     * Get agentcompanyid
     *
     * @return integer 
     */
    public function getAgentcompanyid()
    {
        return $this->agentcompanyid;
    }

    /**
     * Set agentmanager
     *
     * @param integer $agentmanager
     * @return User
     */
    public function setAgentmanager($agentmanager)
    {
        $this->agentmanager = $agentmanager;

        return $this;
    }

    /**
     * Get agentmanager
     *
     * @return integer 
     */
    public function getAgentmanager()
    {
        return $this->agentmanager;
    }

    /**
     * Set avatarid
     *
     * @param integer $avatarid
     * @return User
     */
    public function setAvatarid($avatarid)
    {
        $this->avatarid = $avatarid;

        return $this;
    }

    /**
     * Get avatarid
     *
     * @return integer 
     */
    public function getAvatarid()
    {
        return $this->avatarid;
    }
}
