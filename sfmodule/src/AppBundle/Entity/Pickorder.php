<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Pickorder
 */
class Pickorder
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $consolidatedid;

    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $referenceid;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var integer
     */
    private $ownercompanyid;

    /**
     * @var integer
     */
    private $fromid;

    /**
     * @var integer
     */
    private $toid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var integer
     */
    private $packed;

    /**
     * @var \DateTime
     */
    private $deliverydate;

    /**
     * @var \DateTime
     */
    private $expecteddate;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $lastcontainerid;

    /**
     * @var integer
     */
    private $pickeduserid;

    /**
     * @var \DateTime
     */
    private $pickeddate;

    /**
     * @var integer
     */
    private $picked;

    /**
     * @var integer
     */
    private $packeduserid;

    /**
     * @var \DateTime
     */
    private $packeddate;

    /**
     * @var integer
     */
    private $pickuserid;

    /**
     * @var integer
     */
    private $printed;

    /**
     * @var integer
     */
    private $handlercompanyid;

    /**
     * @var integer
     */
    private $updated;

    /**
     * @var string
     */
    private $instructions;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Pickorder
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set referenceid
     *
     * @param integer $referenceid
     * @return Pickorder
     */
    public function setReferenceid($referenceid)
    {
        $this->referenceid = $referenceid;

        return $this;
    }

    /**
     * Get referenceid
     *
     * @return integer 
     */
    public function getReferenceid()
    {
        return $this->referenceid;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Pickorder
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Pickorder
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set consolidatedid
     *
     * @param integer $consolidatedid
     * @return Pickorder
     */
    public function setConsolidatedid($consolidatedid)
    {
        $this->consolidatedid = $consolidatedid;

        return $this;
    }

    /**
     * Get consolidatedid
     *
     * @return integer 
     */
    public function getConsolidatedid()
    {
        return $this->consolidatedid;
    }

    /**
     * Set ownercompanyid
     *
     * @param integer $ownercompanyid
     * @return Pickorder
     */
    public function setOwnercompanyid($ownercompanyid)
    {
        $this->ownercompanyid = $ownercompanyid;

        return $this;
    }

    /**
     * Get ownercompanyid
     *
     * @return integer 
     */
    public function getOwnercompanyid()
    {
        return $this->ownercompanyid;
    }

    /**
     * Set fromid
     *
     * @param integer $fromid
     * @return Pickorder
     */
    public function setFromid($fromid)
    {
        $this->fromid = $fromid;

        return $this;
    }

    /**
     * Get fromid
     *
     * @return integer 
     */
    public function getFromid()
    {
        return $this->fromid;
    }

    /**
     * Set toid
     *
     * @param integer $toid
     * @return Pickorder
     */
    public function setToid($toid)
    {
        $this->toid = $toid;

        return $this;
    }

    /**
     * Get toid
     *
     * @return integer 
     */
    public function getToid()
    {
        return $this->toid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Pickorder
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set packed
     *
     * @param integer $packed
     * @return Pickorder
     */
    public function setPacked($packed)
    {
        $this->packed = $packed;

        return $this;
    }

    /**
     * Get packed
     *
     * @return integer 
     */
    public function getPacked()
    {
        return $this->packed;
    }

    /**
     * Set deliverydate
     *
     * @param \DateTime $deliverydate
     * @return Pickorder
     */
    public function setDeliverydate($deliverydate)
    {
        $this->deliverydate = $deliverydate;

        return $this;
    }

    /**
     * Get deliverydate
     *
     * @return \DateTime 
     */
    public function getDeliverydate()
    {
        return $this->deliverydate;
    }

    /**
     * Set expecteddate
     *
     * @param \DateTime $expecteddate
     * @return Pickorder
     */
    public function setExpecteddate($expecteddate)
    {
        $this->expecteddate = $expecteddate;

        return $this;
    }

    /**
     * Get expecteddate
     *
     * @return \DateTime 
     */
    public function getExpecteddate()
    {
        return $this->expecteddate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Pickorder
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Pickorder
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Pickorder
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Pickorder
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set lastcontainerid
     *
     * @param integer $lastcontainerid
     * @return Pickorder
     */
    public function setLastcontainerid($lastcontainerid)
    {
        $this->lastcontainerid = $lastcontainerid;

        return $this;
    }

    /**
     * Get lastcontainerid
     *
     * @return integer 
     */
    public function getLastcontainerid()
    {
        return $this->lastcontainerid;
    }

    /**
     * Set pickeduserid
     *
     * @param integer $pickeduserid
     * @return Pickorder
     */
    public function setPickeduserid($pickeduserid)
    {
        $this->pickeduserid = $pickeduserid;

        return $this;
    }

    /**
     * Get pickeduserid
     *
     * @return integer 
     */
    public function getPickeduserid()
    {
        return $this->pickeduserid;
    }

    /**
     * Set pickeddate
     *
     * @param \DateTime $pickeddate
     * @return Pickorder
     */
    public function setPickeddate($pickeddate)
    {
        $this->pickeddate = $pickeddate;

        return $this;
    }

    /**
     * Get pickeddate
     *
     * @return \DateTime 
     */
    public function getPickeddate()
    {
        return $this->pickeddate;
    }

    /**
     * Set picked
     *
     * @param integer $picked
     * @return Pickorder
     */
    public function setPicked($picked)
    {
        $this->picked = $picked;

        return $this;
    }

    /**
     * Get picked
     *
     * @return integer 
     */
    public function getPicked()
    {
        return $this->picked;
    }

    /**
     * Set packeduserid
     *
     * @param integer $packeduserid
     * @return Pickorder
     */
    public function setPackeduserid($packeduserid)
    {
        $this->packeduserid = $packeduserid;

        return $this;
    }

    /**
     * Get packeduserid
     *
     * @return integer 
     */
    public function getPackeduserid()
    {
        return $this->packeduserid;
    }

    /**
     * Set packeddate
     *
     * @param \DateTime $packeddate
     * @return Pickorder
     */
    public function setPackeddate($packeddate)
    {
        $this->packeddate = $packeddate;

        return $this;
    }

    /**
     * Get packeddate
     *
     * @return \DateTime 
     */
    public function getPackeddate()
    {
        return $this->packeddate;
    }

    /**
     * Set pickuserid
     *
     * @param integer $pickuserid
     * @return Pickorder
     */
    public function setPickuserid($pickuserid)
    {
        $this->pickuserid = $pickuserid;

        return $this;
    }

    /**
     * Get pickuserid
     *
     * @return integer 
     */
    public function getPickuserid()
    {
        return $this->pickuserid;
    }

    /**
     * Set printed
     *
     * @param integer $printed
     * @return Pickorder
     */
    public function setPrinted($printed)
    {
        $this->printed = $printed;

        return $this;
    }

    /**
     * Get printed
     *
     * @return integer 
     */
    public function getPrinted()
    {
        return $this->printed;
    }

    /**
     * Set handlercompanyid
     *
     * @param integer $handlercompanyid
     * @return Pickorder
     */
    public function setHandlercompanyid($handlercompanyid)
    {
        $this->handlercompanyid = $handlercompanyid;

        return $this;
    }

    /**
     * Get handlercompanyid
     *
     * @return integer 
     */
    public function getHandlercompanyid()
    {
        return $this->handlercompanyid;
    }

    /**
     * Set updated
     *
     * @param integer $updated
     * @return Pickorder
     */
    public function setUpdated($updated)
    {
        $this->updated = $updated;

        return $this;
    }

    /**
     * Get updated
     *
     * @return integer 
     */
    public function getUpdated()
    {
        return $this->updated;
    }

    /**
     * Set instructions
     *
     * @param string $instructions
     * @return Pickorder
     */
    public function setInstructions($instructions)
    {
        $this->instructions = $instructions;

        return $this;
    }

    /**
     * Get instructions
     *
     * @return string 
     */
    public function getInstructions()
    {
        return $this->instructions;
    }
}
