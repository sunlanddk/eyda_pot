<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timelogginuseractivity
 */
class Timelogginuseractivity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $userid;

    /**
     * @var integer
     */
    private $timeloggingactivityid;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userid
     *
     * @param integer $userid
     * @return Timelogginuseractivity
     */
    public function setUserid($userid)
    {
        $this->userid = $userid;

        return $this;
    }

    /**
     * Get userid
     *
     * @return integer 
     */
    public function getUserid()
    {
        return $this->userid;
    }

    /**
     * Set timeloggingactivityid
     *
     * @param integer $timeloggingactivityid
     * @return Timelogginuseractivity
     */
    public function setTimeloggingactivityid($timeloggingactivityid)
    {
        $this->timeloggingactivityid = $timeloggingactivityid;

        return $this;
    }

    /**
     * Get timeloggingactivityid
     *
     * @return integer 
     */
    public function getTimeloggingactivityid()
    {
        return $this->timeloggingactivityid;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Timelogginuseractivity
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Timelogginuseractivity
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Timelogginuseractivity
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Timelogginuseractivity
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
