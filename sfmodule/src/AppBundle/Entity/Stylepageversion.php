<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylepageversion
 */
class Stylepageversion
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var integer
     */
    private $pagetypeid;

    /**
     * @var integer
     */
    private $pageid;

    /**
     * @var integer
     */
    private $pageversion;

    /**
     * @var boolean
     */
    private $current;

    /**
     * @var boolean
     */
    private $closed;

    /**
     * @var boolean
     */
    private $copyapproval;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $originalstylepageversionid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Stylepageversion
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set pagetypeid
     *
     * @param integer $pagetypeid
     * @return Stylepageversion
     */
    public function setPagetypeid($pagetypeid)
    {
        $this->pagetypeid = $pagetypeid;

        return $this;
    }

    /**
     * Get pagetypeid
     *
     * @return integer 
     */
    public function getPagetypeid()
    {
        return $this->pagetypeid;
    }

    /**
     * Set pageid
     *
     * @param integer $pageid
     * @return Stylepageversion
     */
    public function setPageid($pageid)
    {
        $this->pageid = $pageid;

        return $this;
    }

    /**
     * Get pageid
     *
     * @return integer 
     */
    public function getPageid()
    {
        return $this->pageid;
    }

    /**
     * Set pageversion
     *
     * @param integer $pageversion
     * @return Stylepageversion
     */
    public function setPageversion($pageversion)
    {
        $this->pageversion = $pageversion;

        return $this;
    }

    /**
     * Get pageversion
     *
     * @return integer 
     */
    public function getPageversion()
    {
        return $this->pageversion;
    }

    /**
     * Set current
     *
     * @param boolean $current
     * @return Stylepageversion
     */
    public function setCurrent($current)
    {
        $this->current = $current;

        return $this;
    }

    /**
     * Get current
     *
     * @return boolean 
     */
    public function getCurrent()
    {
        return $this->current;
    }

    /**
     * Set closed
     *
     * @param boolean $closed
     * @return Stylepageversion
     */
    public function setClosed($closed)
    {
        $this->closed = $closed;

        return $this;
    }

    /**
     * Get closed
     *
     * @return boolean 
     */
    public function getClosed()
    {
        return $this->closed;
    }

    /**
     * Set copyapproval
     *
     * @param boolean $copyapproval
     * @return Stylepageversion
     */
    public function setCopyapproval($copyapproval)
    {
        $this->copyapproval = $copyapproval;

        return $this;
    }

    /**
     * Get copyapproval
     *
     * @return boolean 
     */
    public function getCopyapproval()
    {
        return $this->copyapproval;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylepageversion
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylepageversion
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylepageversion
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylepageversion
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set originalstylepageversionid
     *
     * @param integer $originalstylepageversionid
     * @return Stylepageversion
     */
    public function setOriginalstylepageversionid($originalstylepageversionid)
    {
        $this->originalstylepageversionid = $originalstylepageversionid;

        return $this;
    }

    /**
     * Get originalstylepageversionid
     *
     * @return integer 
     */
    public function getOriginalstylepageversionid()
    {
        return $this->originalstylepageversionid;
    }
}
