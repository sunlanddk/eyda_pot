<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customsposition
 */
class Customsposition
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $customscategoryid;

    /**
     * @var integer
     */
    private $unitid;

    /**
     * @var string
     */
    private $weight;

    /**
     * @var string
     */
    private $yearquantity;

    /**
     * @var string
     */
    private $yearvalue;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Customsposition
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Customsposition
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Customsposition
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Customsposition
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Customsposition
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Customsposition
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Customsposition
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set customscategoryid
     *
     * @param integer $customscategoryid
     * @return Customsposition
     */
    public function setCustomscategoryid($customscategoryid)
    {
        $this->customscategoryid = $customscategoryid;

        return $this;
    }

    /**
     * Get customscategoryid
     *
     * @return integer 
     */
    public function getCustomscategoryid()
    {
        return $this->customscategoryid;
    }

    /**
     * Set unitid
     *
     * @param integer $unitid
     * @return Customsposition
     */
    public function setUnitid($unitid)
    {
        $this->unitid = $unitid;

        return $this;
    }

    /**
     * Get unitid
     *
     * @return integer 
     */
    public function getUnitid()
    {
        return $this->unitid;
    }

    /**
     * Set weight
     *
     * @param string $weight
     * @return Customsposition
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return string 
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set yearquantity
     *
     * @param string $yearquantity
     * @return Customsposition
     */
    public function setYearquantity($yearquantity)
    {
        $this->yearquantity = $yearquantity;

        return $this;
    }

    /**
     * Get yearquantity
     *
     * @return string 
     */
    public function getYearquantity()
    {
        return $this->yearquantity;
    }

    /**
     * Set yearvalue
     *
     * @param string $yearvalue
     * @return Customsposition
     */
    public function setYearvalue($yearvalue)
    {
        $this->yearvalue = $yearvalue;

        return $this;
    }

    /**
     * Get yearvalue
     *
     * @return string 
     */
    public function getYearvalue()
    {
        return $this->yearvalue;
    }
}
