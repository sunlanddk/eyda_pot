<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * NavigationLang
 */
class NavigationLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $navigationid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $header;

    /**
     * @var string
     */
    private $icon;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set navigationid
     *
     * @param integer $navigationid
     * @return NavigationLang
     */
    public function setNavigationid($navigationid)
    {
        $this->navigationid = $navigationid;

        return $this;
    }

    /**
     * Get navigationid
     *
     * @return integer 
     */
    public function getNavigationid()
    {
        return $this->navigationid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return NavigationLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return NavigationLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set header
     *
     * @param string $header
     * @return NavigationLang
     */
    public function setHeader($header)
    {
        $this->header = $header;

        return $this;
    }

    /**
     * Get header
     *
     * @return string 
     */
    public function getHeader()
    {
        return $this->header;
    }

    /**
     * Set icon
     *
     * @param string $icon
     * @return NavigationLang
     */
    public function setIcon($icon)
    {
        $this->icon = $icon;

        return $this;
    }

    /**
     * Get icon
     *
     * @return string 
     */
    public function getIcon()
    {
        return $this->icon;
    }
}
