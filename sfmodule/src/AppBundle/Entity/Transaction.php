<?php

namespace AppBundle\Entity;

use DateTime;

/**
 * Transaction
 */
class Transaction
{
    /**
     * @var integer
     */
    private $id;
    // TODO: change typ to enum
    /**
     * @var string
     */
    private $type;

    /**
     * @var integer
     */
    private $objectid;

    /**
     * @var integer
     */
    private $fromstockid;

    /**
     * @var integer
     */
    private $tostockid;
    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $value;

    /**
     * @var DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $currencyrate;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set type
     *
     * @param string $type
     * @return Transaction
     */
    public function setType($type)
    {
        $this->type = $type;

        return $this;
    }

    /**
     * Get type
     *
     * @return string 
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Set objectid
     *
     * @param integer $objectid
     * @return Transaction
     */
    public function setObjectid($objectid)
    {
        $this->objectid = $objectid;

        return $this;
    }
    /**
     * Get objectid
     *
     * @param integer $objectid
     * @return Transaction
     */
    public function getObjectid()
    {
        return $this->objectid;
    }

    /**
     * Set fromstockid
     *
     * @param integer $fromstockid
     * @return Transaction
     */
    public function setFromstockid($fromstockid)
    {
        $this->fromstockid = $fromstockid;

        return $this;
    }
    /**
     * Get fromstockid$
     *
     * @param integer $fromstockid
     * @return Transaction
     */
    public function getFromstockid()
    {
        return $this->fromstockid;
    }

    /**
     * Set tostockid
     *
     * @param integer $tostockid
     * @return Transaction
     */
    public function setTostockid($tostockid)
    {
        $this->tostockid = $tostockid;

        return $this;
    }
    /**
     * Get tostockid$
     *
     * @param integer $tostockid
     * @return Transaction
     */
    public function getTostockid()
    {
        return $this->tostockid;
    }


    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Transaction
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }
    /**
     * Get quantity$
     *
     * @param string $quantity
     * @return Transaction
     */
    public function getQuantity()
    {
        return $this->quantity;
    }


    /**
     * Set value
     *
     * @param string $value
     * @return Transaction
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }
    /**
     * Get value$
     *
     * @param string $value
     * @return Transaction
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdate
     *
     * @param DateTime $createdate
     * @return Transaction
     */
    public function setcreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }
    /**
     * Get createdate$
     *
     * @param DateTime $createdate
     * @return Transaction
     */
    public function getcreatedate()
    {
        return $this->createdate;
    }


    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Transaction
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }
    /**
     * Get createuserid$
     *
     * @param integer $createuserid
     * @return Transaction
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }



    /**
     * Set modifydate
     *
     * @param DateTime $modifydate
     * @return Transaction
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }
    /**
     * Get modifydate$
     *
     * @param DateTime $modifydate
     * @return Transaction
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }


    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Transaction
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }
    /**
     * Get modifyuserid$
     *
     * @param integer $modifyuserid
     * @return Transaction
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Transaction
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Transaction
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set currencyrate
     *
     * @param string $comment
     * @return Transaction
     */
    public function setCurrencyrate($currencyrate)
    {
        $this->currencyrate = $currencyrate;

        return $this;
    }

    /**
     * Get currencyrate
     *
     * @return string 
     */
    public function getCurrencyrate()
    {
        return $this->currencyrate;
    }
}
