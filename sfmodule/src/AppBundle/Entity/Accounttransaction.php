<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accounttransaction
 */
class Accounttransaction
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $fromaccountid;

    /**
     * @var integer
     */
    private $toaccountid;

    /**
     * @var string
     */
    private $costvalue;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fromaccountid
     *
     * @param integer $fromaccountid
     * @return Accounttransaction
     */
    public function setFromaccountid($fromaccountid)
    {
        $this->fromaccountid = $fromaccountid;

        return $this;
    }

    /**
     * Get fromaccountid
     *
     * @return integer 
     */
    public function getFromaccountid()
    {
        return $this->fromaccountid;
    }

    /**
     * Set toaccountid
     *
     * @param integer $toaccountid
     * @return Accounttransaction
     */
    public function setToaccountid($toaccountid)
    {
        $this->toaccountid = $toaccountid;

        return $this;
    }

    /**
     * Get toaccountid
     *
     * @return integer 
     */
    public function getToaccountid()
    {
        return $this->toaccountid;
    }

    /**
     * Set costvalue
     *
     * @param string $costvalue
     * @return Accounttransaction
     */
    public function setCostvalue($costvalue)
    {
        $this->costvalue = $costvalue;

        return $this;
    }

    /**
     * Get costvalue
     *
     * @return string 
     */
    public function getCostvalue()
    {
        return $this->costvalue;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Accounttransaction
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Accounttransaction
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Accounttransaction
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Accounttransaction
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
