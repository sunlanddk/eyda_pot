<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylefabricconsumption
 */
class Stylefabricconsumption
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $breakdownnumber;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var string
     */
    private $usagepercentage;

    /**
     * @var string
     */
    private $usage;

    /**
     * @var integer
     */
    private $stylematerialsversionid;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set breakdownnumber
     *
     * @param integer $breakdownnumber
     * @return Stylefabricconsumption
     */
    public function setBreakdownnumber($breakdownnumber)
    {
        $this->breakdownnumber = $breakdownnumber;

        return $this;
    }

    /**
     * Get breakdownnumber
     *
     * @return integer 
     */
    public function getBreakdownnumber()
    {
        return $this->breakdownnumber;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Stylefabricconsumption
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Stylefabricconsumption
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set usagepercentage
     *
     * @param string $usagepercentage
     * @return Stylefabricconsumption
     */
    public function setUsagepercentage($usagepercentage)
    {
        $this->usagepercentage = $usagepercentage;

        return $this;
    }

    /**
     * Get usagepercentage
     *
     * @return string 
     */
    public function getUsagepercentage()
    {
        return $this->usagepercentage;
    }

    /**
     * Set usage
     *
     * @param string $usage
     * @return Stylefabricconsumption
     */
    public function setUsage($usage)
    {
        $this->usage = $usage;

        return $this;
    }

    /**
     * Get usage
     *
     * @return string 
     */
    public function getUsage()
    {
        return $this->usage;
    }

    /**
     * Set stylematerialsversionid
     *
     * @param integer $stylematerialsversionid
     * @return Stylefabricconsumption
     */
    public function setStylematerialsversionid($stylematerialsversionid)
    {
        $this->stylematerialsversionid = $stylematerialsversionid;

        return $this;
    }

    /**
     * Get stylematerialsversionid
     *
     * @return integer 
     */
    public function getStylematerialsversionid()
    {
        return $this->stylematerialsversionid;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Stylefabricconsumption
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Stylefabricconsumption
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylefabricconsumption
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylefabricconsumption
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylefabricconsumption
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylefabricconsumption
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
