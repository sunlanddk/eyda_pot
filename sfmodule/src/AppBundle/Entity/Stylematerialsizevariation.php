<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Stylematerialsizevariation
 */
class Stylematerialsizevariation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $stylematerialid;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var string
     */
    private $quantityvariant;

    /**
     * @var integer
     */
    private $variantarticlesizeid;

    /**
     * @var string
     */
    private $variantarticledimension;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set stylematerialid
     *
     * @param integer $stylematerialid
     * @return Stylematerialsizevariation
     */
    public function setStylematerialid($stylematerialid)
    {
        $this->stylematerialid = $stylematerialid;

        return $this;
    }

    /**
     * Get stylematerialid
     *
     * @return integer 
     */
    public function getStylematerialid()
    {
        return $this->stylematerialid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Stylematerialsizevariation
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set quantityvariant
     *
     * @param string $quantityvariant
     * @return Stylematerialsizevariation
     */
    public function setQuantityvariant($quantityvariant)
    {
        $this->quantityvariant = $quantityvariant;

        return $this;
    }

    /**
     * Get quantityvariant
     *
     * @return string 
     */
    public function getQuantityvariant()
    {
        return $this->quantityvariant;
    }

    /**
     * Set variantarticlesizeid
     *
     * @param integer $variantarticlesizeid
     * @return Stylematerialsizevariation
     */
    public function setVariantarticlesizeid($variantarticlesizeid)
    {
        $this->variantarticlesizeid = $variantarticlesizeid;

        return $this;
    }

    /**
     * Get variantarticlesizeid
     *
     * @return integer 
     */
    public function getVariantarticlesizeid()
    {
        return $this->variantarticlesizeid;
    }

    /**
     * Set variantarticledimension
     *
     * @param string $variantarticledimension
     * @return Stylematerialsizevariation
     */
    public function setVariantarticledimension($variantarticledimension)
    {
        $this->variantarticledimension = $variantarticledimension;

        return $this;
    }

    /**
     * Get variantarticledimension
     *
     * @return string 
     */
    public function getVariantarticledimension()
    {
        return $this->variantarticledimension;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Stylematerialsizevariation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Stylematerialsizevariation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Stylematerialsizevariation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Stylematerialsizevariation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
