<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Supplierdefaults
 */
class Supplierdefaults
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var integer
     */
    private $certificateid;

    /**
     * @var integer
     */
    private $componentid;

    /**
     * @var integer
     */
    private $materialid;

    /**
     * @var integer
     */
    private $knitid;

    /**
     * @var integer
     */
    private $workid;

    /**
     * @var integer
     */
    private $logisticcurrencyid;

    /**
     * @var integer
     */
    private $unitid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Supplierdefaults
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set certificateid
     *
     * @param integer $certificateid
     * @return Supplierdefaults
     */
    public function setCertificateid($certificateid)
    {
        $this->certificateid = $certificateid;

        return $this;
    }

    /**
     * Get certificateid
     *
     * @return integer 
     */
    public function getCertificateid()
    {
        return $this->certificateid;
    }

    /**
     * Set componentid
     *
     * @param integer $componentid
     * @return Supplierdefaults
     */
    public function setComponentid($componentid)
    {
        $this->componentid = $componentid;

        return $this;
    }

    /**
     * Get componentid
     *
     * @return integer 
     */
    public function getComponentid()
    {
        return $this->componentid;
    }

    /**
     * Set materialid
     *
     * @param integer $materialid
     * @return Supplierdefaults
     */
    public function setMaterialid($materialid)
    {
        $this->materialid = $materialid;

        return $this;
    }

    /**
     * Get materialid
     *
     * @return integer 
     */
    public function getMaterialid()
    {
        return $this->materialid;
    }

    /**
     * Set knitid
     *
     * @param integer $knitid
     * @return Supplierdefaults
     */
    public function setKnitid($knitid)
    {
        $this->knitid = $knitid;

        return $this;
    }

    /**
     * Get knitid
     *
     * @return integer 
     */
    public function getKnitid()
    {
        return $this->knitid;
    }

    /**
     * Set workid
     *
     * @param integer $workid
     * @return Supplierdefaults
     */
    public function setWorkid($workid)
    {
        $this->workid = $workid;

        return $this;
    }

    /**
     * Get workid
     *
     * @return integer 
     */
    public function getWorkid()
    {
        return $this->workid;
    }

    /**
     * Set logisticcurrencyid
     *
     * @param integer $logisticcurrencyid
     * @return Supplierdefaults
     */
    public function setLogisticcurrencyid($logisticcurrencyid)
    {
        $this->logisticcurrencyid = $logisticcurrencyid;

        return $this;
    }

    /**
     * Get logisticcurrencyid
     *
     * @return integer 
     */
    public function getLogisticcurrencyid()
    {
        return $this->logisticcurrencyid;
    }

    /**
     * Set unitid
     *
     * @param integer $unitid
     * @return Supplierdefaults
     */
    public function setUnitid($unitid)
    {
        $this->unitid = $unitid;

        return $this;
    }

    /**
     * Get unitid
     *
     * @return integer 
     */
    public function getUnitid()
    {
        return $this->unitid;
    }
}
