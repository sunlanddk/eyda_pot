<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Item
 */
class Item
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $containerid;

    /**
     * @var integer
     */
    private $previouscontainerid;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var integer
     */
    private $purchaseid;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var integer
     */
    private $fromproductionid;

    /**
     * @var integer
     */
    private $consumeproductionid;

    /**
     * @var integer
     */
    private $parentid;

    /**
     * @var integer
     */
    private $orderlineid;

    /**
     * @var integer
     */
    private $transportid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var string
     */
    private $price;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $articlecertificateid;

    /**
     * @var integer
     */
    private $dimension;

    /**
     * @var integer
     */
    private $sortation;

    /**
     * @var integer
     */
    private $testitemid;

    /**
     * @var boolean
     */
    private $testdone;

    /**
     * @var integer
     */
    private $testdoneuserid;

    /**
     * @var \DateTime
     */
    private $testdonedate;

    /**
     * @var integer
     */
    private $widthusable;

    /**
     * @var integer
     */
    private $widthfull;

    /**
     * @var integer
     */
    private $m2weight;

    /**
     * @var string
     */
    private $shrinkagewashlength;

    /**
     * @var string
     */
    private $shrinkagewashwidth;

    /**
     * @var string
     */
    private $shrinkageworklength;

    /**
     * @var string
     */
    private $shrinkageworkwidth;

    /**
     * @var string
     */
    private $washinginstruction;

    /**
     * @var string
     */
    private $colorfastnesswater;

    /**
     * @var string
     */
    private $colorfastnesswash;

    /**
     * @var string
     */
    private $colorfastnesslight;

    /**
     * @var string
     */
    private $colorfastnessperspiration;

    /**
     * @var string
     */
    private $colorfastnesschlor;

    /**
     * @var string
     */
    private $colorfastnesssea;

    /**
     * @var string
     */
    private $colorfastnessdurability;

    /**
     * @var string
     */
    private $rubbingdry;

    /**
     * @var string
     */
    private $rubbingwet;

    /**
     * @var string
     */
    private $batchnumber;

    /**
     * @var string
     */
    private $receivalnumber;

    /**
     * @var integer
     */
    private $itemoperationid;

    /**
     * @var integer
     */
    private $previousid;

    /**
     * @var integer
     */
    private $requisitionid;

    /**
     * @var integer
     */
    private $requisitionlineid;

    /**
     * @var integer
     */
    private $productionid;

    /**
     * @var integer
     */
    private $reservationid;

    /**
     * @var integer
     */
    private $ownerid;

    /**
     * @var integer
     */
    private $inventorypct;

    /**
     * @var \DateTime
     */
    private $reservationdate;

    /**
     * @var integer
     */
    private $reservationuserid;

    /**
     * @var integer
     */
    private $purchasedforid;

    /**
     * @var integer
     */
    private $altstockid;

    /**
     * @var string
     */
    private $twisting;


    /**
     * Set id
     *
     * @param integer $id
     * @return Item
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Item
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Item
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Item
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Item
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Item
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Item
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set containerid
     *
     * @param integer $containerid
     * @return Item
     */
    public function setContainerid($containerid)
    {
        $this->containerid = $containerid;

        return $this;
    }

    /**
     * Get containerid
     *
     * @return integer 
     */
    public function getContainerid()
    {
        return $this->containerid;
    }

    /**
     * Set previouscontainerid
     *
     * @param integer $previouscontainerid
     * @return Item
     */
    public function setPreviouscontainerid($previouscontainerid)
    {
        $this->previouscontainerid = $previouscontainerid;

        return $this;
    }

    /**
     * Get previouscontainerid
     *
     * @return integer 
     */
    public function getPreviouscontainerid()
    {
        return $this->previouscontainerid;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Item
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set purchaseid
     *
     * @param integer $purchaseid
     * @return Item
     */
    public function setPurchaseid($purchaseid)
    {
        $this->purchaseid = $purchaseid;

        return $this;
    }

    /**
     * Get purchaseid
     *
     * @return integer 
     */
    public function getPurchaseid()
    {
        return $this->purchaseid;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Item
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set fromproductionid
     *
     * @param integer $fromproductionid
     * @return Item
     */
    public function setFromproductionid($fromproductionid)
    {
        $this->fromproductionid = $fromproductionid;

        return $this;
    }

    /**
     * Get fromproductionid
     *
     * @return integer 
     */
    public function getFromproductionid()
    {
        return $this->fromproductionid;
    }

    /**
     * Set consumeproductionid
     *
     * @param integer $consumeproductionid
     * @return Item
     */
    public function setConsumeproductionid($consumeproductionid)
    {
        $this->consumeproductionid = $consumeproductionid;

        return $this;
    }

    /**
     * Get consumeproductionid
     *
     * @return integer 
     */
    public function getConsumeproductionid()
    {
        return $this->consumeproductionid;
    }

    /**
     * Set parentid
     *
     * @param integer $parentid
     * @return Item
     */
    public function setParentid($parentid)
    {
        $this->parentid = $parentid;

        return $this;
    }

    /**
     * Get parentid
     *
     * @return integer 
     */
    public function getParentid()
    {
        return $this->parentid;
    }

    /**
     * Set orderlineid
     *
     * @param integer $orderlineid
     * @return Item
     */
    public function setOrderlineid($orderlineid)
    {
        $this->orderlineid = $orderlineid;

        return $this;
    }

    /**
     * Get orderlineid
     *
     * @return integer 
     */
    public function getOrderlineid()
    {
        return $this->orderlineid;
    }

    /**
     * Set transportid
     *
     * @param integer $transportid
     * @return Item
     */
    public function setTransportid($transportid)
    {
        $this->transportid = $transportid;

        return $this;
    }

    /**
     * Get transportid
     *
     * @return integer 
     */
    public function getTransportid()
    {
        return $this->transportid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Item
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Item
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set price
     *
     * @param string $price
     * @return Item
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return string 
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Item
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Item
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set articlecertificateid
     *
     * @param integer $articlecertificateid
     * @return Item
     */
    public function setArticlecertificateid($articlecertificateid)
    {
        $this->articlecertificateid = $articlecertificateid;

        return $this;
    }

    /**
     * Get articlecertificateid
     *
     * @return integer 
     */
    public function getArticlecertificateid()
    {
        return $this->articlecertificateid;
    }

    /**
     * Set dimension
     *
     * @param integer $dimension
     * @return Item
     */
    public function setDimension($dimension)
    {
        $this->dimension = $dimension;

        return $this;
    }

    /**
     * Get dimension
     *
     * @return integer 
     */
    public function getDimension()
    {
        return $this->dimension;
    }

    /**
     * Set sortation
     *
     * @param integer $sortation
     * @return Item
     */
    public function setSortation($sortation)
    {
        $this->sortation = $sortation;

        return $this;
    }

    /**
     * Get sortation
     *
     * @return integer 
     */
    public function getSortation()
    {
        return $this->sortation;
    }

    /**
     * Set testitemid
     *
     * @param integer $testitemid
     * @return Item
     */
    public function setTestitemid($testitemid)
    {
        $this->testitemid = $testitemid;

        return $this;
    }

    /**
     * Get testitemid
     *
     * @return integer 
     */
    public function getTestitemid()
    {
        return $this->testitemid;
    }

    /**
     * Set testdone
     *
     * @param boolean $testdone
     * @return Item
     */
    public function setTestdone($testdone)
    {
        $this->testdone = $testdone;

        return $this;
    }

    /**
     * Get testdone
     *
     * @return boolean 
     */
    public function getTestdone()
    {
        return $this->testdone;
    }

    /**
     * Set testdoneuserid
     *
     * @param integer $testdoneuserid
     * @return Item
     */
    public function setTestdoneuserid($testdoneuserid)
    {
        $this->testdoneuserid = $testdoneuserid;

        return $this;
    }

    /**
     * Get testdoneuserid
     *
     * @return integer 
     */
    public function getTestdoneuserid()
    {
        return $this->testdoneuserid;
    }

    /**
     * Set testdonedate
     *
     * @param \DateTime $testdonedate
     * @return Item
     */
    public function setTestdonedate($testdonedate)
    {
        $this->testdonedate = $testdonedate;

        return $this;
    }

    /**
     * Get testdonedate
     *
     * @return \DateTime 
     */
    public function getTestdonedate()
    {
        return $this->testdonedate;
    }

    /**
     * Set widthusable
     *
     * @param integer $widthusable
     * @return Item
     */
    public function setWidthusable($widthusable)
    {
        $this->widthusable = $widthusable;

        return $this;
    }

    /**
     * Get widthusable
     *
     * @return integer 
     */
    public function getWidthusable()
    {
        return $this->widthusable;
    }

    /**
     * Set widthfull
     *
     * @param integer $widthfull
     * @return Item
     */
    public function setWidthfull($widthfull)
    {
        $this->widthfull = $widthfull;

        return $this;
    }

    /**
     * Get widthfull
     *
     * @return integer 
     */
    public function getWidthfull()
    {
        return $this->widthfull;
    }

    /**
     * Set m2weight
     *
     * @param integer $m2weight
     * @return Item
     */
    public function setM2weight($m2weight)
    {
        $this->m2weight = $m2weight;

        return $this;
    }

    /**
     * Get m2weight
     *
     * @return integer 
     */
    public function getM2weight()
    {
        return $this->m2weight;
    }

    /**
     * Set shrinkagewashlength
     *
     * @param string $shrinkagewashlength
     * @return Item
     */
    public function setShrinkagewashlength($shrinkagewashlength)
    {
        $this->shrinkagewashlength = $shrinkagewashlength;

        return $this;
    }

    /**
     * Get shrinkagewashlength
     *
     * @return string 
     */
    public function getShrinkagewashlength()
    {
        return $this->shrinkagewashlength;
    }

    /**
     * Set shrinkagewashwidth
     *
     * @param string $shrinkagewashwidth
     * @return Item
     */
    public function setShrinkagewashwidth($shrinkagewashwidth)
    {
        $this->shrinkagewashwidth = $shrinkagewashwidth;

        return $this;
    }

    /**
     * Get shrinkagewashwidth
     *
     * @return string 
     */
    public function getShrinkagewashwidth()
    {
        return $this->shrinkagewashwidth;
    }

    /**
     * Set shrinkageworklength
     *
     * @param string $shrinkageworklength
     * @return Item
     */
    public function setShrinkageworklength($shrinkageworklength)
    {
        $this->shrinkageworklength = $shrinkageworklength;

        return $this;
    }

    /**
     * Get shrinkageworklength
     *
     * @return string 
     */
    public function getShrinkageworklength()
    {
        return $this->shrinkageworklength;
    }

    /**
     * Set shrinkageworkwidth
     *
     * @param string $shrinkageworkwidth
     * @return Item
     */
    public function setShrinkageworkwidth($shrinkageworkwidth)
    {
        $this->shrinkageworkwidth = $shrinkageworkwidth;

        return $this;
    }

    /**
     * Get shrinkageworkwidth
     *
     * @return string 
     */
    public function getShrinkageworkwidth()
    {
        return $this->shrinkageworkwidth;
    }

    /**
     * Set washinginstruction
     *
     * @param string $washinginstruction
     * @return Item
     */
    public function setWashinginstruction($washinginstruction)
    {
        $this->washinginstruction = $washinginstruction;

        return $this;
    }

    /**
     * Get washinginstruction
     *
     * @return string 
     */
    public function getWashinginstruction()
    {
        return $this->washinginstruction;
    }

    /**
     * Set colorfastnesswater
     *
     * @param string $colorfastnesswater
     * @return Item
     */
    public function setColorfastnesswater($colorfastnesswater)
    {
        $this->colorfastnesswater = $colorfastnesswater;

        return $this;
    }

    /**
     * Get colorfastnesswater
     *
     * @return string 
     */
    public function getColorfastnesswater()
    {
        return $this->colorfastnesswater;
    }

    /**
     * Set colorfastnesswash
     *
     * @param string $colorfastnesswash
     * @return Item
     */
    public function setColorfastnesswash($colorfastnesswash)
    {
        $this->colorfastnesswash = $colorfastnesswash;

        return $this;
    }

    /**
     * Get colorfastnesswash
     *
     * @return string 
     */
    public function getColorfastnesswash()
    {
        return $this->colorfastnesswash;
    }

    /**
     * Set colorfastnesslight
     *
     * @param string $colorfastnesslight
     * @return Item
     */
    public function setColorfastnesslight($colorfastnesslight)
    {
        $this->colorfastnesslight = $colorfastnesslight;

        return $this;
    }

    /**
     * Get colorfastnesslight
     *
     * @return string 
     */
    public function getColorfastnesslight()
    {
        return $this->colorfastnesslight;
    }

    /**
     * Set colorfastnessperspiration
     *
     * @param string $colorfastnessperspiration
     * @return Item
     */
    public function setColorfastnessperspiration($colorfastnessperspiration)
    {
        $this->colorfastnessperspiration = $colorfastnessperspiration;

        return $this;
    }

    /**
     * Get colorfastnessperspiration
     *
     * @return string 
     */
    public function getColorfastnessperspiration()
    {
        return $this->colorfastnessperspiration;
    }

    /**
     * Set colorfastnesschlor
     *
     * @param string $colorfastnesschlor
     * @return Item
     */
    public function setColorfastnesschlor($colorfastnesschlor)
    {
        $this->colorfastnesschlor = $colorfastnesschlor;

        return $this;
    }

    /**
     * Get colorfastnesschlor
     *
     * @return string 
     */
    public function getColorfastnesschlor()
    {
        return $this->colorfastnesschlor;
    }

    /**
     * Set colorfastnesssea
     *
     * @param string $colorfastnesssea
     * @return Item
     */
    public function setColorfastnesssea($colorfastnesssea)
    {
        $this->colorfastnesssea = $colorfastnesssea;

        return $this;
    }

    /**
     * Get colorfastnesssea
     *
     * @return string 
     */
    public function getColorfastnesssea()
    {
        return $this->colorfastnesssea;
    }

    /**
     * Set colorfastnessdurability
     *
     * @param string $colorfastnessdurability
     * @return Item
     */
    public function setColorfastnessdurability($colorfastnessdurability)
    {
        $this->colorfastnessdurability = $colorfastnessdurability;

        return $this;
    }

    /**
     * Get colorfastnessdurability
     *
     * @return string 
     */
    public function getColorfastnessdurability()
    {
        return $this->colorfastnessdurability;
    }

    /**
     * Set rubbingdry
     *
     * @param string $rubbingdry
     * @return Item
     */
    public function setRubbingdry($rubbingdry)
    {
        $this->rubbingdry = $rubbingdry;

        return $this;
    }

    /**
     * Get rubbingdry
     *
     * @return string 
     */
    public function getRubbingdry()
    {
        return $this->rubbingdry;
    }

    /**
     * Set rubbingwet
     *
     * @param string $rubbingwet
     * @return Item
     */
    public function setRubbingwet($rubbingwet)
    {
        $this->rubbingwet = $rubbingwet;

        return $this;
    }

    /**
     * Get rubbingwet
     *
     * @return string 
     */
    public function getRubbingwet()
    {
        return $this->rubbingwet;
    }

    /**
     * Set batchnumber
     *
     * @param string $batchnumber
     * @return Item
     */
    public function setBatchnumber($batchnumber)
    {
        $this->batchnumber = $batchnumber;

        return $this;
    }

    /**
     * Get batchnumber
     *
     * @return string 
     */
    public function getBatchnumber()
    {
        return $this->batchnumber;
    }

    /**
     * Set receivalnumber
     *
     * @param string $receivalnumber
     * @return Item
     */
    public function setReceivalnumber($receivalnumber)
    {
        $this->receivalnumber = $receivalnumber;

        return $this;
    }

    /**
     * Get receivalnumber
     *
     * @return string 
     */
    public function getReceivalnumber()
    {
        return $this->receivalnumber;
    }

    /**
     * Set itemoperationid
     *
     * @param integer $itemoperationid
     * @return Item
     */
    public function setItemoperationid($itemoperationid)
    {
        $this->itemoperationid = $itemoperationid;

        return $this;
    }

    /**
     * Get itemoperationid
     *
     * @return integer 
     */
    public function getItemoperationid()
    {
        return $this->itemoperationid;
    }

    /**
     * Set previousid
     *
     * @param integer $previousid
     * @return Item
     */
    public function setPreviousid($previousid)
    {
        $this->previousid = $previousid;

        return $this;
    }

    /**
     * Get previousid
     *
     * @return integer 
     */
    public function getPreviousid()
    {
        return $this->previousid;
    }

    /**
     * Set requisitionid
     *
     * @param integer $requisitionid
     * @return Item
     */
    public function setRequisitionid($requisitionid)
    {
        $this->requisitionid = $requisitionid;

        return $this;
    }

    /**
     * Get requisitionid
     *
     * @return integer 
     */
    public function getRequisitionid()
    {
        return $this->requisitionid;
    }

    /**
     * Set requisitionlineid
     *
     * @param integer $requisitionlineid
     * @return Item
     */
    public function setRequisitionlineid($requisitionlineid)
    {
        $this->requisitionlineid = $requisitionlineid;

        return $this;
    }

    /**
     * Get requisitionlineid
     *
     * @return integer 
     */
    public function getRequisitionlineid()
    {
        return $this->requisitionlineid;
    }

    /**
     * Set productionid
     *
     * @param integer $productionid
     * @return Item
     */
    public function setProductionid($productionid)
    {
        $this->productionid = $productionid;

        return $this;
    }

    /**
     * Get productionid
     *
     * @return integer 
     */
    public function getProductionid()
    {
        return $this->productionid;
    }

    /**
     * Set reservationid
     *
     * @param integer $reservationid
     * @return Item
     */
    public function setReservationid($reservationid)
    {
        $this->reservationid = $reservationid;

        return $this;
    }

    /**
     * Get reservationid
     *
     * @return integer 
     */
    public function getReservationid()
    {
        return $this->reservationid;
    }

    /**
     * Set ownerid
     *
     * @param integer $ownerid
     * @return Item
     */
    public function setOwnerid($ownerid)
    {
        $this->ownerid = $ownerid;

        return $this;
    }

    /**
     * Get ownerid
     *
     * @return integer 
     */
    public function getOwnerid()
    {
        return $this->ownerid;
    }

    /**
     * Set inventorypct
     *
     * @param integer $inventorypct
     * @return Item
     */
    public function setInventorypct($inventorypct)
    {
        $this->inventorypct = $inventorypct;

        return $this;
    }

    /**
     * Get inventorypct
     *
     * @return integer 
     */
    public function getInventorypct()
    {
        return $this->inventorypct;
    }

    /**
     * Set reservationdate
     *
     * @param \DateTime $reservationdate
     * @return Item
     */
    public function setReservationdate($reservationdate)
    {
        $this->reservationdate = $reservationdate;

        return $this;
    }

    /**
     * Get reservationdate
     *
     * @return \DateTime 
     */
    public function getReservationdate()
    {
        return $this->reservationdate;
    }

    /**
     * Set reservationuserid
     *
     * @param integer $reservationuserid
     * @return Item
     */
    public function setReservationuserid($reservationuserid)
    {
        $this->reservationuserid = $reservationuserid;

        return $this;
    }

    /**
     * Get reservationuserid
     *
     * @return integer 
     */
    public function getReservationuserid()
    {
        return $this->reservationuserid;
    }

    /**
     * Set purchasedforid
     *
     * @param integer $purchasedforid
     * @return Item
     */
    public function setPurchasedforid($purchasedforid)
    {
        $this->purchasedforid = $purchasedforid;

        return $this;
    }

    /**
     * Get purchasedforid
     *
     * @return integer 
     */
    public function getPurchasedforid()
    {
        return $this->purchasedforid;
    }

    /**
     * Set altstockid
     *
     * @param integer $altstockid
     * @return Item
     */
    public function setAltstockid($altstockid)
    {
        $this->altstockid = $altstockid;

        return $this;
    }

    /**
     * Get altstockid
     *
     * @return integer 
     */
    public function getAltstockid()
    {
        return $this->altstockid;
    }

    /**
     * Set twisting
     *
     * @param string $twisting
     * @return Item
     */
    public function setTwisting($twisting)
    {
        $this->twisting = $twisting;

        return $this;
    }

    /**
     * Get twisting
     *
     * @return string 
     */
    public function getTwisting()
    {
        return $this->twisting;
    }
}
