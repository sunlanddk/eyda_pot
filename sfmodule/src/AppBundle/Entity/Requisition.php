<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requisition
 */
class Requisition
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $companyid;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $reference;

    /**
     * @var integer
     */
    private $purchaseuserid;

    /**
     * @var integer
     */
    private $currencyid;

    /**
     * @var integer
     */
    private $paymentid;

    /**
     * @var integer
     */
    private $deliverytermid;

    /**
     * @var integer
     */
    private $carrierid;

    /**
     * @var integer
     */
    private $deliveryid;

    /**
     * @var string
     */
    private $requisitionheader;

    /**
     * @var string
     */
    private $requisitionfooter;

    /**
     * @var boolean
     */
    private $ready;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var integer
     */
    private $doneconditionid;

    /**
     * @var integer
     */
    private $readyuserid;

    /**
     * @var \DateTime
     */
    private $readydate;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $fromcompanyid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $ordertypeid;

    /**
     * @var integer
     */
    private $seasonid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Requisition
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Requisition
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set reference
     *
     * @param string $reference
     * @return Requisition
     */
    public function setReference($reference)
    {
        $this->reference = $reference;

        return $this;
    }

    /**
     * Get reference
     *
     * @return string 
     */
    public function getReference()
    {
        return $this->reference;
    }

    /**
     * Set purchaseuserid
     *
     * @param integer $purchaseuserid
     * @return Requisition
     */
    public function setPurchaseuserid($purchaseuserid)
    {
        $this->purchaseuserid = $purchaseuserid;

        return $this;
    }

    /**
     * Get purchaseuserid
     *
     * @return integer 
     */
    public function getPurchaseuserid()
    {
        return $this->purchaseuserid;
    }

    /**
     * Set currencyid
     *
     * @param integer $currencyid
     * @return Requisition
     */
    public function setCurrencyid($currencyid)
    {
        $this->currencyid = $currencyid;

        return $this;
    }

    /**
     * Get currencyid
     *
     * @return integer 
     */
    public function getCurrencyid()
    {
        return $this->currencyid;
    }

    /**
     * Set paymentid
     *
     * @param integer $paymentid
     * @return Requisition
     */
    public function setPaymentid($paymentid)
    {
        $this->paymentid = $paymentid;

        return $this;
    }

    /**
     * Get paymentid
     *
     * @return integer 
     */
    public function getPaymentid()
    {
        return $this->paymentid;
    }

    /**
     * Set deliverytermid
     *
     * @param integer $deliverytermid
     * @return Requisition
     */
    public function setDeliverytermid($deliverytermid)
    {
        $this->deliverytermid = $deliverytermid;

        return $this;
    }

    /**
     * Get deliverytermid
     *
     * @return integer 
     */
    public function getDeliverytermid()
    {
        return $this->deliverytermid;
    }

    /**
     * Set carrierid
     *
     * @param integer $carrierid
     * @return Requisition
     */
    public function setCarrierid($carrierid)
    {
        $this->carrierid = $carrierid;

        return $this;
    }

    /**
     * Get carrierid
     *
     * @return integer 
     */
    public function getCarrierid()
    {
        return $this->carrierid;
    }

    /**
     * Set deliveryid
     *
     * @param integer $deliveryid
     * @return Requisition
     */
    public function setDeliveryid($deliveryid)
    {
        $this->deliveryid = $deliveryid;

        return $this;
    }

    /**
     * Get deliveryid
     *
     * @return integer 
     */
    public function getDeliveryid()
    {
        return $this->deliveryid;
    }

    /**
     * Set requisitionheader
     *
     * @param string $requisitionheader
     * @return Requisition
     */
    public function setRequisitionheader($requisitionheader)
    {
        $this->requisitionheader = $requisitionheader;

        return $this;
    }

    /**
     * Get requisitionheader
     *
     * @return string 
     */
    public function getRequisitionheader()
    {
        return $this->requisitionheader;
    }

    /**
     * Set requisitionfooter
     *
     * @param string $requisitionfooter
     * @return Requisition
     */
    public function setRequisitionfooter($requisitionfooter)
    {
        $this->requisitionfooter = $requisitionfooter;

        return $this;
    }

    /**
     * Get requisitionfooter
     *
     * @return string 
     */
    public function getRequisitionfooter()
    {
        return $this->requisitionfooter;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     * @return Requisition
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean 
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Requisition
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set doneconditionid
     *
     * @param integer $doneconditionid
     * @return Requisition
     */
    public function setDoneconditionid($doneconditionid)
    {
        $this->doneconditionid = $doneconditionid;

        return $this;
    }

    /**
     * Get doneconditionid
     *
     * @return integer 
     */
    public function getDoneconditionid()
    {
        return $this->doneconditionid;
    }

    /**
     * Set readyuserid
     *
     * @param integer $readyuserid
     * @return Requisition
     */
    public function setReadyuserid($readyuserid)
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    /**
     * Get readyuserid
     *
     * @return integer 
     */
    public function getReadyuserid()
    {
        return $this->readyuserid;
    }

    /**
     * Set readydate
     *
     * @param \DateTime $readydate
     * @return Requisition
     */
    public function setReadydate($readydate)
    {
        $this->readydate = $readydate;

        return $this;
    }

    /**
     * Get readydate
     *
     * @return \DateTime 
     */
    public function getReadydate()
    {
        return $this->readydate;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Requisition
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Requisition
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Requisition
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Requisition
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Requisition
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Requisition
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set fromcompanyid
     *
     * @param integer $fromcompanyid
     * @return Requisition
     */
    public function setFromcompanyid($fromcompanyid)
    {
        $this->fromcompanyid = $fromcompanyid;

        return $this;
    }

    /**
     * Get fromcompanyid
     *
     * @return integer 
     */
    public function getFromcompanyid()
    {
        return $this->fromcompanyid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Requisition
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set ordertypeid
     *
     * @param integer $ordertypeid
     * @return Requisition
     */
    public function setOrdertypeid($ordertypeid)
    {
        $this->ordertypeid = $ordertypeid;

        return $this;
    }

    /**
     * Get ordertypeid
     *
     * @return integer 
     */
    public function getOrdertypeid()
    {
        return $this->ordertypeid;
    }

    /**
     * Set seasonid
     *
     * @param integer $seasonid
     * @return Requisition
     */
    public function setSeasonid($seasonid)
    {
        $this->seasonid = $seasonid;

        return $this;
    }

    /**
     * Get seasonid
     *
     * @return integer 
     */
    public function getSeasonid()
    {
        return $this->seasonid;
    }
}
