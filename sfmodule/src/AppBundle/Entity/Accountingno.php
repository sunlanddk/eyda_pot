<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accountingno
 */
class Accountingno
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $accountingdate;

    /**
     * @var integer
     */
    private $companyid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set accountingdate
     *
     * @param \DateTime $accountingdate
     * @return Accountingno
     */
    public function setAccountingdate($accountingdate)
    {
        $this->accountingdate = $accountingdate;

        return $this;
    }

    /**
     * Get accountingdate
     *
     * @return \DateTime 
     */
    public function getAccountingdate()
    {
        return $this->accountingdate;
    }

    /**
     * Set companyid
     *
     * @param integer $companyid
     * @return Accountingno
     */
    public function setCompanyid($companyid)
    {
        $this->companyid = $companyid;

        return $this;
    }

    /**
     * Get companyid
     *
     * @return integer 
     */
    public function getCompanyid()
    {
        return $this->companyid;
    }
}
