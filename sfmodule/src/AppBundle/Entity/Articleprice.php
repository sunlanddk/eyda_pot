<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articleprice
 */
class Articleprice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var string
     */
    private $quantitystart;

    /**
     * @var string
     */
    private $quantityend;

    /**
     * @var string
     */
    private $pricestart;

    /**
     * @var string
     */
    private $priceunit;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var integer
     */
    private $supplierid;

    /**
     * @var integer
     */
    private $ownerid;

    /**
     * @var string
     */
    private $purchase;

    /**
     * @var string
     */
    private $logistic;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Articleprice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Articleprice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Articleprice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Articleprice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Articleprice
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Articleprice
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set quantitystart
     *
     * @param string $quantitystart
     * @return Articleprice
     */
    public function setQuantitystart($quantitystart)
    {
        $this->quantitystart = $quantitystart;

        return $this;
    }

    /**
     * Get quantitystart
     *
     * @return string 
     */
    public function getQuantitystart()
    {
        return $this->quantitystart;
    }

    /**
     * Set quantityend
     *
     * @param string $quantityend
     * @return Articleprice
     */
    public function setQuantityend($quantityend)
    {
        $this->quantityend = $quantityend;

        return $this;
    }

    /**
     * Get quantityend
     *
     * @return string 
     */
    public function getQuantityend()
    {
        return $this->quantityend;
    }

    /**
     * Set pricestart
     *
     * @param string $pricestart
     * @return Articleprice
     */
    public function setPricestart($pricestart)
    {
        $this->pricestart = $pricestart;

        return $this;
    }

    /**
     * Get pricestart
     *
     * @return string 
     */
    public function getPricestart()
    {
        return $this->pricestart;
    }

    /**
     * Set priceunit
     *
     * @param string $priceunit
     * @return Articleprice
     */
    public function setPriceunit($priceunit)
    {
        $this->priceunit = $priceunit;

        return $this;
    }

    /**
     * Get priceunit
     *
     * @return string 
     */
    public function getPriceunit()
    {
        return $this->priceunit;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Articleprice
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set supplierid
     *
     * @param integer $supplierid
     * @return Articleprice
     */
    public function setSupplierid($supplierid)
    {
        $this->supplierid = $supplierid;

        return $this;
    }

    /**
     * Get supplierid
     *
     * @return integer 
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }

    /**
     * Set ownerid
     *
     * @param integer $ownerid
     * @return Articleprice
     */
    public function setOwnerid($ownerid)
    {
        $this->ownerid = $ownerid;

        return $this;
    }

    /**
     * Get ownerid
     *
     * @return integer 
     */
    public function getOwnerid()
    {
        return $this->ownerid;
    }

    /**
     * Set purchase
     *
     * @param string $purchase
     * @return Articleprice
     */
    public function setPurchase($purchase)
    {
        $this->purchase = $purchase;

        return $this;
    }

    /**
     * Get purchase
     *
     * @return string 
     */
    public function getPurchase()
    {
        return $this->purchase;
    }

    /**
     * Set logistic
     *
     * @param string $logistic
     * @return Articleprice
     */
    public function setLogistic($logistic)
    {
        $this->logistic = $logistic;

        return $this;
    }

    /**
     * Get logistic
     *
     * @return string 
     */
    public function getLogistic()
    {
        return $this->logistic;
    }
}
