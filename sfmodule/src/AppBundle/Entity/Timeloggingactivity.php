<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Timeloggingactivity
 */
class Timeloggingactivity
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $level1;

    /**
     * @var string
     */
    private $level2;

    /**
     * @var string
     */
    private $level3;

    /**
     * @var string
     */
    private $level4;

    /**
     * @var string
     */
    private $level5;

    /**
     * @var \DateTime
     */
    private $plannedstartdate;

    /**
     * @var \DateTime
     */
    private $plannedenddate;

    /**
     * @var string
     */
    private $plannedhours;

    /**
     * @var \DateTime
     */
    private $expectedstartdate;

    /**
     * @var \DateTime
     */
    private $expectedenddate;

    /**
     * @var string
     */
    private $expectehours;

    /**
     * @var boolean
     */
    private $done;

    /**
     * @var \DateTime
     */
    private $donedate;

    /**
     * @var integer
     */
    private $doneuserid;

    /**
     * @var integer
     */
    private $projectid;

    /**
     * @var string
     */
    private $comment;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set level1
     *
     * @param string $level1
     * @return Timeloggingactivity
     */
    public function setLevel1($level1)
    {
        $this->level1 = $level1;

        return $this;
    }

    /**
     * Get level1
     *
     * @return string 
     */
    public function getLevel1()
    {
        return $this->level1;
    }

    /**
     * Set level2
     *
     * @param string $level2
     * @return Timeloggingactivity
     */
    public function setLevel2($level2)
    {
        $this->level2 = $level2;

        return $this;
    }

    /**
     * Get level2
     *
     * @return string 
     */
    public function getLevel2()
    {
        return $this->level2;
    }

    /**
     * Set level3
     *
     * @param string $level3
     * @return Timeloggingactivity
     */
    public function setLevel3($level3)
    {
        $this->level3 = $level3;

        return $this;
    }

    /**
     * Get level3
     *
     * @return string 
     */
    public function getLevel3()
    {
        return $this->level3;
    }

    /**
     * Set level4
     *
     * @param string $level4
     * @return Timeloggingactivity
     */
    public function setLevel4($level4)
    {
        $this->level4 = $level4;

        return $this;
    }

    /**
     * Get level4
     *
     * @return string 
     */
    public function getLevel4()
    {
        return $this->level4;
    }

    /**
     * Set level5
     *
     * @param string $level5
     * @return Timeloggingactivity
     */
    public function setLevel5($level5)
    {
        $this->level5 = $level5;

        return $this;
    }

    /**
     * Get level5
     *
     * @return string 
     */
    public function getLevel5()
    {
        return $this->level5;
    }

    /**
     * Set plannedstartdate
     *
     * @param \DateTime $plannedstartdate
     * @return Timeloggingactivity
     */
    public function setPlannedstartdate($plannedstartdate)
    {
        $this->plannedstartdate = $plannedstartdate;

        return $this;
    }

    /**
     * Get plannedstartdate
     *
     * @return \DateTime 
     */
    public function getPlannedstartdate()
    {
        return $this->plannedstartdate;
    }

    /**
     * Set plannedenddate
     *
     * @param \DateTime $plannedenddate
     * @return Timeloggingactivity
     */
    public function setPlannedenddate($plannedenddate)
    {
        $this->plannedenddate = $plannedenddate;

        return $this;
    }

    /**
     * Get plannedenddate
     *
     * @return \DateTime 
     */
    public function getPlannedenddate()
    {
        return $this->plannedenddate;
    }

    /**
     * Set plannedhours
     *
     * @param string $plannedhours
     * @return Timeloggingactivity
     */
    public function setPlannedhours($plannedhours)
    {
        $this->plannedhours = $plannedhours;

        return $this;
    }

    /**
     * Get plannedhours
     *
     * @return string 
     */
    public function getPlannedhours()
    {
        return $this->plannedhours;
    }

    /**
     * Set expectedstartdate
     *
     * @param \DateTime $expectedstartdate
     * @return Timeloggingactivity
     */
    public function setExpectedstartdate($expectedstartdate)
    {
        $this->expectedstartdate = $expectedstartdate;

        return $this;
    }

    /**
     * Get expectedstartdate
     *
     * @return \DateTime 
     */
    public function getExpectedstartdate()
    {
        return $this->expectedstartdate;
    }

    /**
     * Set expectedenddate
     *
     * @param \DateTime $expectedenddate
     * @return Timeloggingactivity
     */
    public function setExpectedenddate($expectedenddate)
    {
        $this->expectedenddate = $expectedenddate;

        return $this;
    }

    /**
     * Get expectedenddate
     *
     * @return \DateTime 
     */
    public function getExpectedenddate()
    {
        return $this->expectedenddate;
    }

    /**
     * Set expectehours
     *
     * @param string $expectehours
     * @return Timeloggingactivity
     */
    public function setExpectehours($expectehours)
    {
        $this->expectehours = $expectehours;

        return $this;
    }

    /**
     * Get expectehours
     *
     * @return string 
     */
    public function getExpectehours()
    {
        return $this->expectehours;
    }

    /**
     * Set done
     *
     * @param boolean $done
     * @return Timeloggingactivity
     */
    public function setDone($done)
    {
        $this->done = $done;

        return $this;
    }

    /**
     * Get done
     *
     * @return boolean 
     */
    public function getDone()
    {
        return $this->done;
    }

    /**
     * Set donedate
     *
     * @param \DateTime $donedate
     * @return Timeloggingactivity
     */
    public function setDonedate($donedate)
    {
        $this->donedate = $donedate;

        return $this;
    }

    /**
     * Get donedate
     *
     * @return \DateTime 
     */
    public function getDonedate()
    {
        return $this->donedate;
    }

    /**
     * Set doneuserid
     *
     * @param integer $doneuserid
     * @return Timeloggingactivity
     */
    public function setDoneuserid($doneuserid)
    {
        $this->doneuserid = $doneuserid;

        return $this;
    }

    /**
     * Get doneuserid
     *
     * @return integer 
     */
    public function getDoneuserid()
    {
        return $this->doneuserid;
    }

    /**
     * Set projectid
     *
     * @param integer $projectid
     * @return Timeloggingactivity
     */
    public function setProjectid($projectid)
    {
        $this->projectid = $projectid;

        return $this;
    }

    /**
     * Get projectid
     *
     * @return integer 
     */
    public function getProjectid()
    {
        return $this->projectid;
    }

    /**
     * Set comment
     *
     * @param string $comment
     * @return Timeloggingactivity
     */
    public function setComment($comment)
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * Get comment
     *
     * @return string 
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Timeloggingactivity
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Timeloggingactivity
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Timeloggingactivity
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Timeloggingactivity
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
