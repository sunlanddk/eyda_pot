<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Invoiceline
 */
class Invoiceline
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $no;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $invoicefooter;

    /**
     * @var integer
     */
    private $invoiceid;

    /**
     * @var integer
     */
    private $orderlineid;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $articlecolorid;

    /**
     * @var integer
     */
    private $articlecertificateid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $pricesale;

    /**
     * @var string
     */
    private $pricecost;

    /**
     * @var integer
     */
    private $discount;

    /**
     * @var string
     */
    private $custartref;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Invoiceline
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Invoiceline
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Invoiceline
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Invoiceline
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Invoiceline
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set no
     *
     * @param integer $no
     * @return Invoiceline
     */
    public function setNo($no)
    {
        $this->no = $no;

        return $this;
    }

    /**
     * Get no
     *
     * @return integer 
     */
    public function getNo()
    {
        return $this->no;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Invoiceline
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set invoicefooter
     *
     * @param string $invoicefooter
     * @return Invoiceline
     */
    public function setInvoicefooter($invoicefooter)
    {
        $this->invoicefooter = $invoicefooter;

        return $this;
    }

    /**
     * Get invoicefooter
     *
     * @return string 
     */
    public function getInvoicefooter()
    {
        return $this->invoicefooter;
    }

    /**
     * Set invoiceid
     *
     * @param integer $invoiceid
     * @return Invoiceline
     */
    public function setInvoiceid($invoiceid)
    {
        $this->invoiceid = $invoiceid;

        return $this;
    }

    /**
     * Get invoiceid
     *
     * @return integer 
     */
    public function getInvoiceid()
    {
        return $this->invoiceid;
    }

    /**
     * Set orderlineid
     *
     * @param integer $orderlineid
     * @return Invoiceline
     */
    public function setOrderlineid($orderlineid)
    {
        $this->orderlineid = $orderlineid;

        return $this;
    }

    /**
     * Get orderlineid
     *
     * @return integer 
     */
    public function getOrderlineid()
    {
        return $this->orderlineid;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Invoiceline
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set articlecolorid
     *
     * @param integer $articlecolorid
     * @return Invoiceline
     */
    public function setArticlecolorid($articlecolorid)
    {
        $this->articlecolorid = $articlecolorid;

        return $this;
    }

    /**
     * Get articlecolorid
     *
     * @return integer 
     */
    public function getArticlecolorid()
    {
        return $this->articlecolorid;
    }

    /**
     * Set articlecertificateid
     *
     * @param integer $articlecertificateid
     * @return Invoiceline
     */
    public function setArticlecertificateid($articlecertificateid)
    {
        $this->articlecertificateid = $articlecertificateid;

        return $this;
    }

    /**
     * Get articlecertificateid
     *
     * @return integer 
     */
    public function getArticlecertificateid()
    {
        return $this->articlecertificateid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Invoiceline
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set pricesale
     *
     * @param string $pricesale
     * @return Invoiceline
     */
    public function setPricesale($pricesale)
    {
        $this->pricesale = $pricesale;

        return $this;
    }

    /**
     * Get pricesale
     *
     * @return string 
     */
    public function getPricesale()
    {
        return $this->pricesale;
    }

    /**
     * Set pricecost
     *
     * @param string $pricecost
     * @return Invoiceline
     */
    public function setPricecost($pricecost)
    {
        $this->pricecost = $pricecost;

        return $this;
    }

    /**
     * Get pricecost
     *
     * @return string 
     */
    public function getPricecost()
    {
        return $this->pricecost;
    }

    /**
     * Set discount
     *
     * @param integer $discount
     * @return Invoiceline
     */
    public function setDiscount($discount)
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * Get discount
     *
     * @return integer 
     */
    public function getDiscount()
    {
        return $this->discount;
    }

    /**
     * Set custartref
     *
     * @param string $custartref
     * @return Invoiceline
     */
    public function setCustartref($custartref)
    {
        $this->custartref = $custartref;

        return $this;
    }

    /**
     * Get custartref
     *
     * @return string 
     */
    public function getCustartref()
    {
        return $this->custartref;
    }
}
