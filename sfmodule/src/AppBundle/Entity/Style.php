<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Style
 */
class Style
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $splitoperationno;

    /**
     * @var integer
     */
    private $version;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var \DateTime
     */
    private $sketchdate;

    /**
     * @var integer
     */
    private $sketchuserid;

    /**
     * @var string
     */
    private $sketchtype;

    /**
     * @var boolean
     */
    private $ready;

    /**
     * @var integer
     */
    private $readyuserid;

    /**
     * @var \DateTime
     */
    private $readydate;

    /**
     * @var boolean
     */
    private $approved;

    /**
     * @var integer
     */
    private $approveduserid;

    /**
     * @var \DateTime
     */
    private $approveddate;

    /**
     * @var boolean
     */
    private $patternmade;

    /**
     * @var integer
     */
    private $patternmadeuserid;

    /**
     * @var \DateTime
     */
    private $patternmadedate;

    /**
     * @var integer
     */
    private $constructionlocationid;

    /**
     * @var integer
     */
    private $salesrefid;

    /**
     * @var \DateTime
     */
    private $requesteddate;

    /**
     * @var \DateTime
     */
    private $actualdate;

    /**
     * @var integer
     */
    private $headconstructoruserid;

    /**
     * @var string
     */
    private $pattern;

    /**
     * @var boolean
     */
    private $readyforsample;

    /**
     * @var \DateTime
     */
    private $allocateddate;

    /**
     * @var integer
     */
    private $originalarticleid;

    /**
     * @var integer
     */
    private $caseid;

    /**
     * @var boolean
     */
    private $canceled;

    /**
     * @var boolean
     */
    private $costrequest;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Style
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Style
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Style
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Style
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Style
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set splitoperationno
     *
     * @param integer $splitoperationno
     * @return Style
     */
    public function setSplitoperationno($splitoperationno)
    {
        $this->splitoperationno = $splitoperationno;

        return $this;
    }

    /**
     * Get splitoperationno
     *
     * @return integer 
     */
    public function getSplitoperationno()
    {
        return $this->splitoperationno;
    }

    /**
     * Set version
     *
     * @param integer $version
     * @return Style
     */
    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    /**
     * Get version
     *
     * @return integer 
     */
    public function getVersion()
    {
        return $this->version;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Style
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set sketchdate
     *
     * @param \DateTime $sketchdate
     * @return Style
     */
    public function setSketchdate($sketchdate)
    {
        $this->sketchdate = $sketchdate;

        return $this;
    }

    /**
     * Get sketchdate
     *
     * @return \DateTime 
     */
    public function getSketchdate()
    {
        return $this->sketchdate;
    }

    /**
     * Set sketchuserid
     *
     * @param integer $sketchuserid
     * @return Style
     */
    public function setSketchuserid($sketchuserid)
    {
        $this->sketchuserid = $sketchuserid;

        return $this;
    }

    /**
     * Get sketchuserid
     *
     * @return integer 
     */
    public function getSketchuserid()
    {
        return $this->sketchuserid;
    }

    /**
     * Set sketchtype
     *
     * @param string $sketchtype
     * @return Style
     */
    public function setSketchtype($sketchtype)
    {
        $this->sketchtype = $sketchtype;

        return $this;
    }

    /**
     * Get sketchtype
     *
     * @return string 
     */
    public function getSketchtype()
    {
        return $this->sketchtype;
    }

    /**
     * Set ready
     *
     * @param boolean $ready
     * @return Style
     */
    public function setReady($ready)
    {
        $this->ready = $ready;

        return $this;
    }

    /**
     * Get ready
     *
     * @return boolean 
     */
    public function getReady()
    {
        return $this->ready;
    }

    /**
     * Set readyuserid
     *
     * @param integer $readyuserid
     * @return Style
     */
    public function setReadyuserid($readyuserid)
    {
        $this->readyuserid = $readyuserid;

        return $this;
    }

    /**
     * Get readyuserid
     *
     * @return integer 
     */
    public function getReadyuserid()
    {
        return $this->readyuserid;
    }

    /**
     * Set readydate
     *
     * @param \DateTime $readydate
     * @return Style
     */
    public function setReadydate($readydate)
    {
        $this->readydate = $readydate;

        return $this;
    }

    /**
     * Get readydate
     *
     * @return \DateTime 
     */
    public function getReadydate()
    {
        return $this->readydate;
    }

    /**
     * Set approved
     *
     * @param boolean $approved
     * @return Style
     */
    public function setApproved($approved)
    {
        $this->approved = $approved;

        return $this;
    }

    /**
     * Get approved
     *
     * @return boolean 
     */
    public function getApproved()
    {
        return $this->approved;
    }

    /**
     * Set approveduserid
     *
     * @param integer $approveduserid
     * @return Style
     */
    public function setApproveduserid($approveduserid)
    {
        $this->approveduserid = $approveduserid;

        return $this;
    }

    /**
     * Get approveduserid
     *
     * @return integer 
     */
    public function getApproveduserid()
    {
        return $this->approveduserid;
    }

    /**
     * Set approveddate
     *
     * @param \DateTime $approveddate
     * @return Style
     */
    public function setApproveddate($approveddate)
    {
        $this->approveddate = $approveddate;

        return $this;
    }

    /**
     * Get approveddate
     *
     * @return \DateTime 
     */
    public function getApproveddate()
    {
        return $this->approveddate;
    }

    /**
     * Set patternmade
     *
     * @param boolean $patternmade
     * @return Style
     */
    public function setPatternmade($patternmade)
    {
        $this->patternmade = $patternmade;

        return $this;
    }

    /**
     * Get patternmade
     *
     * @return boolean 
     */
    public function getPatternmade()
    {
        return $this->patternmade;
    }

    /**
     * Set patternmadeuserid
     *
     * @param integer $patternmadeuserid
     * @return Style
     */
    public function setPatternmadeuserid($patternmadeuserid)
    {
        $this->patternmadeuserid = $patternmadeuserid;

        return $this;
    }

    /**
     * Get patternmadeuserid
     *
     * @return integer 
     */
    public function getPatternmadeuserid()
    {
        return $this->patternmadeuserid;
    }

    /**
     * Set patternmadedate
     *
     * @param \DateTime $patternmadedate
     * @return Style
     */
    public function setPatternmadedate($patternmadedate)
    {
        $this->patternmadedate = $patternmadedate;

        return $this;
    }

    /**
     * Get patternmadedate
     *
     * @return \DateTime 
     */
    public function getPatternmadedate()
    {
        return $this->patternmadedate;
    }

    /**
     * Set constructionlocationid
     *
     * @param integer $constructionlocationid
     * @return Style
     */
    public function setConstructionlocationid($constructionlocationid)
    {
        $this->constructionlocationid = $constructionlocationid;

        return $this;
    }

    /**
     * Get constructionlocationid
     *
     * @return integer 
     */
    public function getConstructionlocationid()
    {
        return $this->constructionlocationid;
    }

    /**
     * Set salesrefid
     *
     * @param integer $salesrefid
     * @return Style
     */
    public function setSalesrefid($salesrefid)
    {
        $this->salesrefid = $salesrefid;

        return $this;
    }

    /**
     * Get salesrefid
     *
     * @return integer 
     */
    public function getSalesrefid()
    {
        return $this->salesrefid;
    }

    /**
     * Set requesteddate
     *
     * @param \DateTime $requesteddate
     * @return Style
     */
    public function setRequesteddate($requesteddate)
    {
        $this->requesteddate = $requesteddate;

        return $this;
    }

    /**
     * Get requesteddate
     *
     * @return \DateTime 
     */
    public function getRequesteddate()
    {
        return $this->requesteddate;
    }

    /**
     * Set actualdate
     *
     * @param \DateTime $actualdate
     * @return Style
     */
    public function setActualdate($actualdate)
    {
        $this->actualdate = $actualdate;

        return $this;
    }

    /**
     * Get actualdate
     *
     * @return \DateTime 
     */
    public function getActualdate()
    {
        return $this->actualdate;
    }

    /**
     * Set headconstructoruserid
     *
     * @param integer $headconstructoruserid
     * @return Style
     */
    public function setHeadconstructoruserid($headconstructoruserid)
    {
        $this->headconstructoruserid = $headconstructoruserid;

        return $this;
    }

    /**
     * Get headconstructoruserid
     *
     * @return integer 
     */
    public function getHeadconstructoruserid()
    {
        return $this->headconstructoruserid;
    }

    /**
     * Set pattern
     *
     * @param string $pattern
     * @return Style
     */
    public function setPattern($pattern)
    {
        $this->pattern = $pattern;

        return $this;
    }

    /**
     * Get pattern
     *
     * @return string 
     */
    public function getPattern()
    {
        return $this->pattern;
    }

    /**
     * Set readyforsample
     *
     * @param boolean $readyforsample
     * @return Style
     */
    public function setReadyforsample($readyforsample)
    {
        $this->readyforsample = $readyforsample;

        return $this;
    }

    /**
     * Get readyforsample
     *
     * @return boolean 
     */
    public function getReadyforsample()
    {
        return $this->readyforsample;
    }

    /**
     * Set allocateddate
     *
     * @param \DateTime $allocateddate
     * @return Style
     */
    public function setAllocateddate($allocateddate)
    {
        $this->allocateddate = $allocateddate;

        return $this;
    }

    /**
     * Get allocateddate
     *
     * @return \DateTime 
     */
    public function getAllocateddate()
    {
        return $this->allocateddate;
    }

    /**
     * Set originalarticleid
     *
     * @param integer $originalarticleid
     * @return Style
     */
    public function setOriginalarticleid($originalarticleid)
    {
        $this->originalarticleid = $originalarticleid;

        return $this;
    }

    /**
     * Get originalarticleid
     *
     * @return integer 
     */
    public function getOriginalarticleid()
    {
        return $this->originalarticleid;
    }

    /**
     * Set caseid
     *
     * @param integer $caseid
     * @return Style
     */
    public function setCaseid($caseid)
    {
        $this->caseid = $caseid;

        return $this;
    }

    /**
     * Get caseid
     *
     * @return integer 
     */
    public function getCaseid()
    {
        return $this->caseid;
    }

    /**
     * Set canceled
     *
     * @param boolean $canceled
     * @return Style
     */
    public function setCanceled($canceled)
    {
        $this->canceled = $canceled;

        return $this;
    }

    /**
     * Get canceled
     *
     * @return boolean 
     */
    public function getCanceled()
    {
        return $this->canceled;
    }

    /**
     * Set costrequest
     *
     * @param boolean $costrequest
     * @return Style
     */
    public function setCostrequest($costrequest)
    {
        $this->costrequest = $costrequest;

        return $this;
    }

    /**
     * Get costrequest
     *
     * @return boolean 
     */
    public function getCostrequest()
    {
        return $this->costrequest;
    }
}
