<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requisitionitemoperation
 */
class Requisitionitemoperation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $requisitionitemoperationlistid;

    /**
     * @var integer
     */
    private $fromstockid;

    /**
     * @var integer
     */
    private $fromcontainerid;

    /**
     * @var integer
     */
    private $fromitemid;

    /**
     * @var integer
     */
    private $tostockid;

    /**
     * @var integer
     */
    private $tocontainerid;

    /**
     * @var integer
     */
    private $toitemid;

    /**
     * @var integer
     */
    private $unitid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set requisitionitemoperationlistid
     *
     * @param integer $requisitionitemoperationlistid
     * @return Requisitionitemoperation
     */
    public function setRequisitionitemoperationlistid($requisitionitemoperationlistid)
    {
        $this->requisitionitemoperationlistid = $requisitionitemoperationlistid;

        return $this;
    }

    /**
     * Get requisitionitemoperationlistid
     *
     * @return integer 
     */
    public function getRequisitionitemoperationlistid()
    {
        return $this->requisitionitemoperationlistid;
    }

    /**
     * Set fromstockid
     *
     * @param integer $fromstockid
     * @return Requisitionitemoperation
     */
    public function setFromstockid($fromstockid)
    {
        $this->fromstockid = $fromstockid;

        return $this;
    }

    /**
     * Get fromstockid
     *
     * @return integer 
     */
    public function getFromstockid()
    {
        return $this->fromstockid;
    }

    /**
     * Set fromcontainerid
     *
     * @param integer $fromcontainerid
     * @return Requisitionitemoperation
     */
    public function setFromcontainerid($fromcontainerid)
    {
        $this->fromcontainerid = $fromcontainerid;

        return $this;
    }

    /**
     * Get fromcontainerid
     *
     * @return integer 
     */
    public function getFromcontainerid()
    {
        return $this->fromcontainerid;
    }

    /**
     * Set fromitemid
     *
     * @param integer $fromitemid
     * @return Requisitionitemoperation
     */
    public function setFromitemid($fromitemid)
    {
        $this->fromitemid = $fromitemid;

        return $this;
    }

    /**
     * Get fromitemid
     *
     * @return integer 
     */
    public function getFromitemid()
    {
        return $this->fromitemid;
    }

    /**
     * Set tostockid
     *
     * @param integer $tostockid
     * @return Requisitionitemoperation
     */
    public function setTostockid($tostockid)
    {
        $this->tostockid = $tostockid;

        return $this;
    }

    /**
     * Get tostockid
     *
     * @return integer 
     */
    public function getTostockid()
    {
        return $this->tostockid;
    }

    /**
     * Set tocontainerid
     *
     * @param integer $tocontainerid
     * @return Requisitionitemoperation
     */
    public function setTocontainerid($tocontainerid)
    {
        $this->tocontainerid = $tocontainerid;

        return $this;
    }

    /**
     * Get tocontainerid
     *
     * @return integer 
     */
    public function getTocontainerid()
    {
        return $this->tocontainerid;
    }

    /**
     * Set toitemid
     *
     * @param integer $toitemid
     * @return Requisitionitemoperation
     */
    public function setToitemid($toitemid)
    {
        $this->toitemid = $toitemid;

        return $this;
    }

    /**
     * Get toitemid
     *
     * @return integer 
     */
    public function getToitemid()
    {
        return $this->toitemid;
    }

    /**
     * Set unitid
     *
     * @param integer $unitid
     * @return Requisitionitemoperation
     */
    public function setUnitid($unitid)
    {
        $this->unitid = $unitid;

        return $this;
    }

    /**
     * Get unitid
     *
     * @return integer 
     */
    public function getUnitid()
    {
        return $this->unitid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Requisitionitemoperation
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Requisitionitemoperation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Requisitionitemoperation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Requisitionitemoperation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Requisitionitemoperation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
