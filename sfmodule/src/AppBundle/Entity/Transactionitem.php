<?php

namespace AppBundle\Entity;

use DateTime;

/**
 * Transactionitem
 */
class Transactionitem
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $transactionid;

    /**
     * @var integer
     */
    private $objectlineid;

    /**
     * @var integer
     */
    private $objectquantityid;
   
    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var integer
     */
    private $variantcodeid;

    /**
     * @var integer
     */
    private $auto;
   
    /**
     * @var integer
     */
    private $fromstockid;
   
    /**
     * @var integer
     */
    private $tostockid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $value;

    /**
     * @var DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var integer
     */
    private $active;

    /**
     * @var string
     */
    private $logisticcost;

    /**
     * @var string
     */
    private $actualprice;

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set transactionid
     *
     * @param integer $transactionid
     * @return Transactionitem
     */
    public function setTransactionid($transactionid)
    {
        $this->transactionid = $transactionid;

        return $this;
    }

    /**
     * Get transactionid
     *
     * @return integer 
     */
    public function getTransactionid()
    {
        return $this->transactionid;
    }

    /**
     * Set objectlineid
     *
     * @param integer $objectlineid
     * @return Transactionitem
     */
    public function setObjectlineid($objectlineid)
    {
        $this->objectlineid = $objectlineid;

        return $this;
    }

    /**
     * Get objectlineid
     *
     * @return integer 
     */
    public function getObjectlineid()
    {
        return $this->objectlineid;
    }

    /**
     * Set objectquantityid
     *
     * @param integer $objectquantityid
     * @return Transactionitem
     */
    public function setObjectquantityid($objectquantityid)
    {
        $this->objectquantityid = $objectquantityid;

        return $this;
    }

    /**
     * Get objectquantityid
     *
     * @return integer 
     */
    public function getObjectquantityid()
    {
        return $this->objectquantityid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Transactionitem
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set fromstockid
     *
     * @param integer $fromstockid
     * @return Transactionitem
     */
    public function setFromstockid($fromstockid)
    {
        $this->fromstockid = $fromstockid;

        return $this;
    }

    /**
     * Get fromstockid
     *
     * @return integer 
     */
    public function getFromstockid()
    {
        return $this->fromstockid;
    }

    /**
     * Set tostockid
     *
     * @param integer $tostockid
     * @return Transactionitem
     */
    public function setTostockid($tostockid)
    {
        $this->tostockid = $tostockid;

        return $this;
    }

    /**
     * Get tostockid$
     *
     * @param integer $tostockid
     * @return Transactionitem
     */
    public function getTostockid()
    {
        return $this->tostockid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Transactionitem
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity$
     *
     * @param string $quantity
     * @return Transactionitem
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set value
     *
     * @param string $value
     * @return Transactionitem
     */
    public function setValue($value)
    {
        $this->value = $value;

        return $this;
    }

    /**
     * Get value$
     *
     * @param string $value
     * @return Transactionitem
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * Set createdate
     *
     * @param DateTime $createdate
     * @return Transactionitem
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate$
     *
     * @param DateTime $createdate
     * @return Transactionitem
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Transactionitem
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid$
     *
     * @param integer $createuserid
     * @return Transactionitem
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param DateTime $modifydate
     * @return Transactionitem
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }
    
    /**
     * Get modifydate$
     *
     * @param DateTime $modifydate
     * @return Transactionitem
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Transactionitem
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid$
     *
     * @param integer $modifyuserid
     * @return Transactionitem
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Transactionitem
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set variantcodeid
     *
     * @param integer $variantcodeid
     * @return Transactionitem
     */
    public function setVariantcodeid($variantcodeid)
    {
        $this->variantcodeid = $variantcodeid;

        return $this;
    }

    /**
     * Get variantcodeid
     *
     * @return integer 
     */
    public function getVariantcodeid()
    {
        return $this->variantcodeid;
    }

    /**
     * Set auto
     *
     * @param integer $auto
     * @return Transactionitem
     */
    public function setAuto($auto)
    {
        $this->auto = $auto;

        return $this;
    }

    /**
     * Get auto
     *
     * @return integer 
     */
    public function getAuto()
    {
        return $this->auto;
    }

    /**
     * Set logisticcost
     *
     * @param string $logisticcost
     * @return Transactionitem
     */
    public function setLogisticcost($logisticcost)
    {
        $this->logisticcost = $logisticcost;

        return $this;
    }

    /**
     * Get logisticcost
     *
     * @return string 
     */
    public function getLogisticcost()
    {
        return $this->logisticcost;
    }

    /**
     * Set actualprice
     *
     * @param string $actualprice
     * @return Transactionitem
     */
    public function setActualprice($actualprice)
    {
        $this->actualprice = $actualprice;

        return $this;
    }

    /**
     * Get actualprice
     *
     * @return string 
     */
    public function getActualprice()
    {
        return $this->actualprice;
    }
}
