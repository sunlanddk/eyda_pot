<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Budgetgoal
 */
class Budgetgoal
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $fromdate;

    /**
     * @var string
     */
    private $description;

    /**
     * @var integer
     */
    private $budgetgoal;

    /**
     * @var integer
     */
    private $budgetgoalext;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Budgetgoal
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Budgetgoal
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Budgetgoal
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Budgetgoal
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Budgetgoal
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set fromdate
     *
     * @param \DateTime $fromdate
     * @return Budgetgoal
     */
    public function setFromdate($fromdate)
    {
        $this->fromdate = $fromdate;

        return $this;
    }

    /**
     * Get fromdate
     *
     * @return \DateTime 
     */
    public function getFromdate()
    {
        return $this->fromdate;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Budgetgoal
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set budgetgoal
     *
     * @param integer $budgetgoal
     * @return Budgetgoal
     */
    public function setBudgetgoal($budgetgoal)
    {
        $this->budgetgoal = $budgetgoal;

        return $this;
    }

    /**
     * Get budgetgoal
     *
     * @return integer 
     */
    public function getBudgetgoal()
    {
        return $this->budgetgoal;
    }

    /**
     * Set budgetgoalext
     *
     * @param integer $budgetgoalext
     * @return Budgetgoal
     */
    public function setBudgetgoalext($budgetgoalext)
    {
        $this->budgetgoalext = $budgetgoalext;

        return $this;
    }

    /**
     * Get budgetgoalext
     *
     * @return integer 
     */
    public function getBudgetgoalext()
    {
        return $this->budgetgoalext;
    }
}
