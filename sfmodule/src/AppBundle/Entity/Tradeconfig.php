<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Tradeconfig
 */
class Tradeconfig
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $customerid;

    /**
     * @var integer
     */
    private $supplierid;

    /**
     * @var integer
     */
    private $gppct;

    /**
     * @var string
     */
    private $logisticfixed;

    /**
     * @var integer
     */
    private $logisticpct;

    /**
     * @var integer
     */
    private $active;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Tradeconfig
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set customerid
     *
     * @param integer $customerid
     * @return Tradeconfig
     */
    public function setCustomerid($customerid)
    {
        $this->customerid = $customerid;

        return $this;
    }

    /**
     * Get customerid
     *
     * @return integer 
     */
    public function getCustomerid()
    {
        return $this->customerid;
    }

    /**
     * Set supplierid
     *
     * @param integer $supplierid
     * @return Tradeconfig
     */
    public function setSupplierid($supplierid)
    {
        $this->supplierid = $supplierid;

        return $this;
    }

    /**
     * Get supplierid
     *
     * @return integer 
     */
    public function getSupplierid()
    {
        return $this->supplierid;
    }

    /**
     * Set gppct
     *
     * @param integer $gppct
     * @return Tradeconfig
     */
    public function setGppct($gppct)
    {
        $this->gppct = $gppct;

        return $this;
    }

    /**
     * Get gppct
     *
     * @return integer 
     */
    public function getGppct()
    {
        return $this->gppct;
    }

    /**
     * Set logisticfixed
     *
     * @param string $logisticfixed
     * @return Tradeconfig
     */
    public function setLogisticfixed($logisticfixed)
    {
        $this->logisticfixed = $logisticfixed;

        return $this;
    }

    /**
     * Get logisticfixed
     *
     * @return string 
     */
    public function getLogisticfixed()
    {
        return $this->logisticfixed;
    }

    /**
     * Set logisticpct
     *
     * @param integer $logisticpct
     * @return Tradeconfig
     */
    public function setLogisticpct($logisticpct)
    {
        $this->logisticpct = $logisticpct;

        return $this;
    }

    /**
     * Get logisticpct
     *
     * @return integer 
     */
    public function getLogisticpct()
    {
        return $this->logisticpct;
    }

    /**
     * Set active
     *
     * @param integer $active
     * @return Tradeconfig
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return integer 
     */
    public function getActive()
    {
        return $this->active;
    }
}
