<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Navigationtype
 */
class Navigationtype
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $name;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $mark;

    /**
     * @var boolean
     */
    private $layoutraw;

    /**
     * @var boolean
     */
    private $layoutpopup;

    /**
     * @var boolean
     */
    private $layoutmain;

    /**
     * @var boolean
     */
    private $layoutsubmenu;

    /**
     * @var integer
     */
    private $projectid;

    /**
     * @var boolean
     */
    private $defaultrecord;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Navigationtype
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Navigationtype
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Navigationtype
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Navigationtype
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Navigationtype
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Navigationtype
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Navigationtype
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set mark
     *
     * @param string $mark
     * @return Navigationtype
     */
    public function setMark($mark)
    {
        $this->mark = $mark;

        return $this;
    }

    /**
     * Get mark
     *
     * @return string 
     */
    public function getMark()
    {
        return $this->mark;
    }

    /**
     * Set layoutraw
     *
     * @param boolean $layoutraw
     * @return Navigationtype
     */
    public function setLayoutraw($layoutraw)
    {
        $this->layoutraw = $layoutraw;

        return $this;
    }

    /**
     * Get layoutraw
     *
     * @return boolean 
     */
    public function getLayoutraw()
    {
        return $this->layoutraw;
    }

    /**
     * Set layoutpopup
     *
     * @param boolean $layoutpopup
     * @return Navigationtype
     */
    public function setLayoutpopup($layoutpopup)
    {
        $this->layoutpopup = $layoutpopup;

        return $this;
    }

    /**
     * Get layoutpopup
     *
     * @return boolean 
     */
    public function getLayoutpopup()
    {
        return $this->layoutpopup;
    }

    /**
     * Set layoutmain
     *
     * @param boolean $layoutmain
     * @return Navigationtype
     */
    public function setLayoutmain($layoutmain)
    {
        $this->layoutmain = $layoutmain;

        return $this;
    }

    /**
     * Get layoutmain
     *
     * @return boolean 
     */
    public function getLayoutmain()
    {
        return $this->layoutmain;
    }

    /**
     * Set layoutsubmenu
     *
     * @param boolean $layoutsubmenu
     * @return Navigationtype
     */
    public function setLayoutsubmenu($layoutsubmenu)
    {
        $this->layoutsubmenu = $layoutsubmenu;

        return $this;
    }

    /**
     * Get layoutsubmenu
     *
     * @return boolean 
     */
    public function getLayoutsubmenu()
    {
        return $this->layoutsubmenu;
    }

    /**
     * Set projectid
     *
     * @param integer $projectid
     * @return Navigationtype
     */
    public function setProjectid($projectid)
    {
        $this->projectid = $projectid;

        return $this;
    }

    /**
     * Get projectid
     *
     * @return integer 
     */
    public function getProjectid()
    {
        return $this->projectid;
    }

    /**
     * Set defaultrecord
     *
     * @param boolean $defaultrecord
     * @return Navigationtype
     */
    public function setDefaultrecord($defaultrecord)
    {
        $this->defaultrecord = $defaultrecord;

        return $this;
    }

    /**
     * Get defaultrecord
     *
     * @return boolean 
     */
    public function getDefaultrecord()
    {
        return $this->defaultrecord;
    }
}
