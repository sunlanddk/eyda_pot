<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Customsforma12
 */
class Customsforma12
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $invoiceid;

    /**
     * @var integer
     */
    private $customspositionid;

    /**
     * @var integer
     */
    private $countryid;

    /**
     * @var integer
     */
    private $field35;

    /**
     * @var string
     */
    private $field37;

    /**
     * @var integer
     */
    private $field38;

    /**
     * @var string
     */
    private $field41;

    /**
     * @var string
     */
    private $field44;

    /**
     * @var string
     */
    private $field46;

    /**
     * @var string
     */
    private $field49;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Customsforma12
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Customsforma12
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Customsforma12
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Customsforma12
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Customsforma12
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set invoiceid
     *
     * @param integer $invoiceid
     * @return Customsforma12
     */
    public function setInvoiceid($invoiceid)
    {
        $this->invoiceid = $invoiceid;

        return $this;
    }

    /**
     * Get invoiceid
     *
     * @return integer 
     */
    public function getInvoiceid()
    {
        return $this->invoiceid;
    }

    /**
     * Set customspositionid
     *
     * @param integer $customspositionid
     * @return Customsforma12
     */
    public function setCustomspositionid($customspositionid)
    {
        $this->customspositionid = $customspositionid;

        return $this;
    }

    /**
     * Get customspositionid
     *
     * @return integer 
     */
    public function getCustomspositionid()
    {
        return $this->customspositionid;
    }

    /**
     * Set countryid
     *
     * @param integer $countryid
     * @return Customsforma12
     */
    public function setCountryid($countryid)
    {
        $this->countryid = $countryid;

        return $this;
    }

    /**
     * Get countryid
     *
     * @return integer 
     */
    public function getCountryid()
    {
        return $this->countryid;
    }

    /**
     * Set field35
     *
     * @param integer $field35
     * @return Customsforma12
     */
    public function setField35($field35)
    {
        $this->field35 = $field35;

        return $this;
    }

    /**
     * Get field35
     *
     * @return integer 
     */
    public function getField35()
    {
        return $this->field35;
    }

    /**
     * Set field37
     *
     * @param string $field37
     * @return Customsforma12
     */
    public function setField37($field37)
    {
        $this->field37 = $field37;

        return $this;
    }

    /**
     * Get field37
     *
     * @return string 
     */
    public function getField37()
    {
        return $this->field37;
    }

    /**
     * Set field38
     *
     * @param integer $field38
     * @return Customsforma12
     */
    public function setField38($field38)
    {
        $this->field38 = $field38;

        return $this;
    }

    /**
     * Get field38
     *
     * @return integer 
     */
    public function getField38()
    {
        return $this->field38;
    }

    /**
     * Set field41
     *
     * @param string $field41
     * @return Customsforma12
     */
    public function setField41($field41)
    {
        $this->field41 = $field41;

        return $this;
    }

    /**
     * Get field41
     *
     * @return string 
     */
    public function getField41()
    {
        return $this->field41;
    }

    /**
     * Set field44
     *
     * @param string $field44
     * @return Customsforma12
     */
    public function setField44($field44)
    {
        $this->field44 = $field44;

        return $this;
    }

    /**
     * Get field44
     *
     * @return string 
     */
    public function getField44()
    {
        return $this->field44;
    }

    /**
     * Set field46
     *
     * @param string $field46
     * @return Customsforma12
     */
    public function setField46($field46)
    {
        $this->field46 = $field46;

        return $this;
    }

    /**
     * Get field46
     *
     * @return string 
     */
    public function getField46()
    {
        return $this->field46;
    }

    /**
     * Set field49
     *
     * @param string $field49
     * @return Customsforma12
     */
    public function setField49($field49)
    {
        $this->field49 = $field49;

        return $this;
    }

    /**
     * Get field49
     *
     * @return string 
     */
    public function getField49()
    {
        return $this->field49;
    }
}
