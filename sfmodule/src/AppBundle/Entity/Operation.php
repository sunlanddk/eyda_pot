<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Operation
 */
class Operation
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var string
     */
    private $number;

    /**
     * @var integer
     */
    private $machinegroupid;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $bundletext;

    /**
     * @var string
     */
    private $productionminutes;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Operation
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Operation
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Operation
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Operation
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Operation
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set number
     *
     * @param string $number
     * @return Operation
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return string 
     */
    public function getNumber()
    {
        return $this->number;
    }

    /**
     * Set machinegroupid
     *
     * @param integer $machinegroupid
     * @return Operation
     */
    public function setMachinegroupid($machinegroupid)
    {
        $this->machinegroupid = $machinegroupid;

        return $this;
    }

    /**
     * Get machinegroupid
     *
     * @return integer 
     */
    public function getMachinegroupid()
    {
        return $this->machinegroupid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Operation
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set bundletext
     *
     * @param string $bundletext
     * @return Operation
     */
    public function setBundletext($bundletext)
    {
        $this->bundletext = $bundletext;

        return $this;
    }

    /**
     * Get bundletext
     *
     * @return string 
     */
    public function getBundletext()
    {
        return $this->bundletext;
    }

    /**
     * Set productionminutes
     *
     * @param string $productionminutes
     * @return Operation
     */
    public function setProductionminutes($productionminutes)
    {
        $this->productionminutes = $productionminutes;

        return $this;
    }

    /**
     * Get productionminutes
     *
     * @return string 
     */
    public function getProductionminutes()
    {
        return $this->productionminutes;
    }
}
