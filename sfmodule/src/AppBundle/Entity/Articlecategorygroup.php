<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articlecategorygroup
 */
class Articlecategorygroup
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $name;

    /**
     * @var integer
     */
    private $articlegroupid;

    /**
     * @var integer
     */
    private $displayorder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Articlecategorygroup
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set articlegroupid
     *
     * @param integer $articlegroupid
     * @return Articlecategorygroup
     */
    public function setArticlegroupid($articlegroupid)
    {
        $this->articlegroupid = $articlegroupid;

        return $this;
    }

    /**
     * Get articlegroupid
     *
     * @return integer 
     */
    public function getArticlegroupid()
    {
        return $this->articlegroupid;
    }

    /**
     * Set displayorder
     *
     * @param integer $displayorder
     * @return Articlecategorygroup
     */
    public function setDisplayorder($displayorder)
    {
        $this->displayorder = $displayorder;

        return $this;
    }

    /**
     * Get displayorder
     *
     * @return integer 
     */
    public function getDisplayorder()
    {
        return $this->displayorder;
    }
}
