<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Collectionmemberassortment
 */
class Collectionmemberassortment
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var integer
     */
    private $collectionmemberid;

    /**
     * @var integer
     */
    private $assortmentid;

    /**
     * @var integer
     */
    private $articlesizeid;

    /**
     * @var string
     */
    private $quantity;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Collectionmemberassortment
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Collectionmemberassortment
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Collectionmemberassortment
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Collectionmemberassortment
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set active
     *
     * @param boolean $active
     * @return Collectionmemberassortment
     */
    public function setActive($active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean 
     */
    public function getActive()
    {
        return $this->active;
    }

    /**
     * Set collectionmemberid
     *
     * @param integer $collectionmemberid
     * @return Collectionmemberassortment
     */
    public function setCollectionmemberid($collectionmemberid)
    {
        $this->collectionmemberid = $collectionmemberid;

        return $this;
    }

    /**
     * Get collectionmemberid
     *
     * @return integer 
     */
    public function getCollectionmemberid()
    {
        return $this->collectionmemberid;
    }

    /**
     * Set assortmentid
     *
     * @param integer $assortmentid
     * @return Collectionmemberassortment
     */
    public function setAssortmentid($assortmentid)
    {
        $this->assortmentid = $assortmentid;

        return $this;
    }

    /**
     * Get assortmentid
     *
     * @return integer 
     */
    public function getAssortmentid()
    {
        return $this->assortmentid;
    }

    /**
     * Set articlesizeid
     *
     * @param integer $articlesizeid
     * @return Collectionmemberassortment
     */
    public function setArticlesizeid($articlesizeid)
    {
        $this->articlesizeid = $articlesizeid;

        return $this;
    }

    /**
     * Get articlesizeid
     *
     * @return integer 
     */
    public function getArticlesizeid()
    {
        return $this->articlesizeid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Collectionmemberassortment
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }
}
