<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SexLang
 */
class SexLang
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $sexid;

    /**
     * @var integer
     */
    private $languageid;

    /**
     * @var string
     */
    private $name;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set sexid
     *
     * @param integer $sexid
     * @return SexLang
     */
    public function setSexid($sexid)
    {
        $this->sexid = $sexid;

        return $this;
    }

    /**
     * Get sexid
     *
     * @return integer 
     */
    public function getSexid()
    {
        return $this->sexid;
    }

    /**
     * Set languageid
     *
     * @param integer $languageid
     * @return SexLang
     */
    public function setLanguageid($languageid)
    {
        $this->languageid = $languageid;

        return $this;
    }

    /**
     * Get languageid
     *
     * @return integer 
     */
    public function getLanguageid()
    {
        return $this->languageid;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return SexLang
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string 
     */
    public function getName()
    {
        return $this->name;
    }
}
