<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Requisitionitemoperationlist
 */
class Requisitionitemoperationlist
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $objectid;

    /**
     * @var integer
     */
    private $sourceid;

    /**
     * @var string
     */
    private $description;

    /**
     * @var string
     */
    private $costvalue;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set objectid
     *
     * @param integer $objectid
     * @return Requisitionitemoperationlist
     */
    public function setObjectid($objectid)
    {
        $this->objectid = $objectid;

        return $this;
    }

    /**
     * Get objectid
     *
     * @return integer 
     */
    public function getObjectid()
    {
        return $this->objectid;
    }

    /**
     * Set sourceid
     *
     * @param integer $sourceid
     * @return Requisitionitemoperationlist
     */
    public function setSourceid($sourceid)
    {
        $this->sourceid = $sourceid;

        return $this;
    }

    /**
     * Get sourceid
     *
     * @return integer 
     */
    public function getSourceid()
    {
        return $this->sourceid;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Requisitionitemoperationlist
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set costvalue
     *
     * @param string $costvalue
     * @return Requisitionitemoperationlist
     */
    public function setCostvalue($costvalue)
    {
        $this->costvalue = $costvalue;

        return $this;
    }

    /**
     * Get costvalue
     *
     * @return string 
     */
    public function getCostvalue()
    {
        return $this->costvalue;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Requisitionitemoperationlist
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Requisitionitemoperationlist
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Requisitionitemoperationlist
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Requisitionitemoperationlist
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }
}
