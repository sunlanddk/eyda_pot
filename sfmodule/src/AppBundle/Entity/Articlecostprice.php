<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Articlecostprice
 */
class Articlecostprice
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var integer
     */
    private $articleid;

    /**
     * @var integer
     */
    private $styleid;

    /**
     * @var string
     */
    private $quantity;

    /**
     * @var string
     */
    private $standard;

    /**
     * @var string
     */
    private $minimum;

    /**
     * @var string
     */
    private $standardpurchase;

    /**
     * @var string
     */
    private $standardlogistic;

    /**
     * @var string
     */
    private $minimumpurchase;

    /**
     * @var string
     */
    private $maximum;

    /**
     * @var string
     */
    private $maxpurchase;

    /**
     * @var string
     */
    private $maxlogistic;

    /**
     * @var string
     */
    private $minimumlogistic;

    /**
     * @var string
     */
    private $wastage;

    /**
     * @var boolean
     */
    private $confirm;

    /**
     * @var \DateTime
     */
    private $createdate;

    /**
     * @var integer
     */
    private $createuserid;

    /**
     * @var \DateTime
     */
    private $modifydate;

    /**
     * @var integer
     */
    private $modifyuserid;

    /**
     * @var \DateTime
     */
    private $confirmdate;

    /**
     * @var integer
     */
    private $confirmuserid;

    /**
     * @var integer
     */
    private $articlepurchaseunitid;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set articleid
     *
     * @param integer $articleid
     * @return Articlecostprice
     */
    public function setArticleid($articleid)
    {
        $this->articleid = $articleid;

        return $this;
    }

    /**
     * Get articleid
     *
     * @return integer 
     */
    public function getArticleid()
    {
        return $this->articleid;
    }

    /**
     * Set styleid
     *
     * @param integer $styleid
     * @return Articlecostprice
     */
    public function setStyleid($styleid)
    {
        $this->styleid = $styleid;

        return $this;
    }

    /**
     * Get styleid
     *
     * @return integer 
     */
    public function getStyleid()
    {
        return $this->styleid;
    }

    /**
     * Set quantity
     *
     * @param string $quantity
     * @return Articlecostprice
     */
    public function setQuantity($quantity)
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * Get quantity
     *
     * @return string 
     */
    public function getQuantity()
    {
        return $this->quantity;
    }

    /**
     * Set standard
     *
     * @param string $standard
     * @return Articlecostprice
     */
    public function setStandard($standard)
    {
        $this->standard = $standard;

        return $this;
    }

    /**
     * Get standard
     *
     * @return string 
     */
    public function getStandard()
    {
        return $this->standard;
    }

    /**
     * Set minimum
     *
     * @param string $minimum
     * @return Articlecostprice
     */
    public function setMinimum($minimum)
    {
        $this->minimum = $minimum;

        return $this;
    }

    /**
     * Get minimum
     *
     * @return string 
     */
    public function getMinimum()
    {
        return $this->minimum;
    }

    /**
     * Set standardpurchase
     *
     * @param string $standardpurchase
     * @return Articlecostprice
     */
    public function setStandardpurchase($standardpurchase)
    {
        $this->standardpurchase = $standardpurchase;

        return $this;
    }

    /**
     * Get standardpurchase
     *
     * @return string 
     */
    public function getStandardpurchase()
    {
        return $this->standardpurchase;
    }

    /**
     * Set standardlogistic
     *
     * @param string $standardlogistic
     * @return Articlecostprice
     */
    public function setStandardlogistic($standardlogistic)
    {
        $this->standardlogistic = $standardlogistic;

        return $this;
    }

    /**
     * Get standardlogistic
     *
     * @return string 
     */
    public function getStandardlogistic()
    {
        return $this->standardlogistic;
    }

    /**
     * Set minimumpurchase
     *
     * @param string $minimumpurchase
     * @return Articlecostprice
     */
    public function setMinimumpurchase($minimumpurchase)
    {
        $this->minimumpurchase = $minimumpurchase;

        return $this;
    }

    /**
     * Get minimumpurchase
     *
     * @return string 
     */
    public function getMinimumpurchase()
    {
        return $this->minimumpurchase;
    }

    /**
     * Set maximum
     *
     * @param string $maximum
     * @return Articlecostprice
     */
    public function setMaximum($maximum)
    {
        $this->maximum = $maximum;

        return $this;
    }

    /**
     * Get maximum
     *
     * @return string 
     */
    public function getMaximum()
    {
        return $this->maximum;
    }

    /**
     * Set maxpurchase
     *
     * @param string $maxpurchase
     * @return Articlecostprice
     */
    public function setMaxpurchase($maxpurchase)
    {
        $this->maxpurchase = $maxpurchase;

        return $this;
    }

    /**
     * Get maxpurchase
     *
     * @return string 
     */
    public function getMaxpurchase()
    {
        return $this->maxpurchase;
    }

    /**
     * Set maxlogistic
     *
     * @param string $maxlogistic
     * @return Articlecostprice
     */
    public function setMaxlogistic($maxlogistic)
    {
        $this->maxlogistic = $maxlogistic;

        return $this;
    }

    /**
     * Get maxlogistic
     *
     * @return string 
     */
    public function getMaxlogistic()
    {
        return $this->maxlogistic;
    }

    /**
     * Set minimumlogistic
     *
     * @param string $minimumlogistic
     * @return Articlecostprice
     */
    public function setMinimumlogistic($minimumlogistic)
    {
        $this->minimumlogistic = $minimumlogistic;

        return $this;
    }

    /**
     * Get minimumlogistic
     *
     * @return string 
     */
    public function getMinimumlogistic()
    {
        return $this->minimumlogistic;
    }

    /**
     * Set wastage
     *
     * @param string $wastage
     * @return Articlecostprice
     */
    public function setWastage($wastage)
    {
        $this->wastage = $wastage;

        return $this;
    }

    /**
     * Get wastage
     *
     * @return string 
     */
    public function getWastage()
    {
        return $this->wastage;
    }

    /**
     * Set confirm
     *
     * @param boolean $confirm
     * @return Articlecostprice
     */
    public function setConfirm($confirm)
    {
        $this->confirm = $confirm;

        return $this;
    }

    /**
     * Get confirm
     *
     * @return boolean 
     */
    public function getConfirm()
    {
        return $this->confirm;
    }

    /**
     * Set createdate
     *
     * @param \DateTime $createdate
     * @return Articlecostprice
     */
    public function setCreatedate($createdate)
    {
        $this->createdate = $createdate;

        return $this;
    }

    /**
     * Get createdate
     *
     * @return \DateTime 
     */
    public function getCreatedate()
    {
        return $this->createdate;
    }

    /**
     * Set createuserid
     *
     * @param integer $createuserid
     * @return Articlecostprice
     */
    public function setCreateuserid($createuserid)
    {
        $this->createuserid = $createuserid;

        return $this;
    }

    /**
     * Get createuserid
     *
     * @return integer 
     */
    public function getCreateuserid()
    {
        return $this->createuserid;
    }

    /**
     * Set modifydate
     *
     * @param \DateTime $modifydate
     * @return Articlecostprice
     */
    public function setModifydate($modifydate)
    {
        $this->modifydate = $modifydate;

        return $this;
    }

    /**
     * Get modifydate
     *
     * @return \DateTime 
     */
    public function getModifydate()
    {
        return $this->modifydate;
    }

    /**
     * Set modifyuserid
     *
     * @param integer $modifyuserid
     * @return Articlecostprice
     */
    public function setModifyuserid($modifyuserid)
    {
        $this->modifyuserid = $modifyuserid;

        return $this;
    }

    /**
     * Get modifyuserid
     *
     * @return integer 
     */
    public function getModifyuserid()
    {
        return $this->modifyuserid;
    }

    /**
     * Set confirmdate
     *
     * @param \DateTime $confirmdate
     * @return Articlecostprice
     */
    public function setConfirmdate($confirmdate)
    {
        $this->confirmdate = $confirmdate;

        return $this;
    }

    /**
     * Get confirmdate
     *
     * @return \DateTime 
     */
    public function getConfirmdate()
    {
        return $this->confirmdate;
    }

    /**
     * Set confirmuserid
     *
     * @param integer $confirmuserid
     * @return Articlecostprice
     */
    public function setConfirmuserid($confirmuserid)
    {
        $this->confirmuserid = $confirmuserid;

        return $this;
    }

    /**
     * Get confirmuserid
     *
     * @return integer 
     */
    public function getConfirmuserid()
    {
        return $this->confirmuserid;
    }

    /**
     * Set articlepurchaseunitid
     *
     * @param integer $articlepurchaseunitid
     * @return Articlecostprice
     */
    public function setArticlepurchaseunitid($articlepurchaseunitid)
    {
        $this->articlepurchaseunitid = $articlepurchaseunitid;

        return $this;
    }

    /**
     * Get articlepurchaseunitid
     *
     * @return integer 
     */
    public function getArticlepurchaseunitid()
    {
        return $this->articlepurchaseunitid;
    }
}
