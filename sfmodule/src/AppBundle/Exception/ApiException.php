<?php

namespace AppBundle\Exception;

/**
 * Class ApiException
 * @author Mariusz Angielski <mariusz.angielski@jcommerce.pl>
 * @package App\Exception
 */
class ApiException extends \Exception{

    const CODE_NOT_FOUND                = 104;
    const CODE_PARAMETER_IS_REQUIRED    = 100;
    const CODE_PARAMETER_BAD_FORMAT     = 101;

    /**
     * ApiException constructor.
     * @param string $message
     * @param int $code
     * @param Exception $previous
     */
    public function __construct($message, $code, \Exception $previous = null)
    {
        parent::__construct($message, $code, $previous);
    }

}