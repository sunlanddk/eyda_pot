<?php 

	namespace AppBundle\Controller;

	class ApiControllerProjectSpecific
	{
		
		public function productallDetailsPostProcessor($productArray)
    	{
			require_once __DIR__.'\..\..\..\..\lib\config.inc' ;
			
			foreach ($productArray as $key => $prod) {
				unset($rp) ; 

				$rp['productNumber'] = $key ;	
				foreach ($prod['name'] as $nkey => $name) {
					$rp['productDescriptions'][] = array(
						'productLanguage' 		=> $nkey,
						'shortDesc' 	=> $name,
						'longDesc'	 	=> $prod['description'][$nkey]
					);
				}
				unset($prod['categories']) ; 		// move after foreach loop if categories wanted. in API
				unset($prod['categoriesdesc']) ; 	// move after foreach loop if categories wanted. in API
	            if(isset($prod['categories'])){
	    			foreach ($prod['categories'] as $cankey => $caname) {
	    				$rp['categories'][] = array(
	    					'categoryLanguage' 		=> $cankey,
	    					'categoryNames' 		=> $caname,
	    					'categoryDescriptions'	=> isset($prod['categoriesdesc'][$cankey])? $prod['categoriesdesc'][$cankey] : ''
	    				);
	    			}
	            }
				foreach ($prod['colors'] as $ckey => $color) {
					unset($rc) ;unset($rcs) ;
					$rc['colorNumber'] = $ckey ;	
					foreach ( $color['colorName'] as $cnkey => $cname) {
						$rc['colorDescriptions'][] = array(
							'colorLanguage' 	=> $cnkey,
							'colorName' 		=> $cname,
							'gender'			=> isset($color['gender'][$cnkey])? $color['gender'][$cnkey] : 'None',
							'OnWebShop'			=> $color['webshop'][$cnkey] 
						);
					}
					unset($color['colorName']) ;
					unset($color['gender']) ;
					unset($color['webshop']) ;

					$_arr = explode(".", $color['image']) ;
					$_file =$_arr[0] . '_upl.' . $_arr[1] ;
					$_size = $this->sizeX($Config['photoLib'].$_file) ;
					$color['images'][] = array(
							'fileName' 	=> $_file,
							'width' 	=> $_size['x'],
							'height'	=> $_size['y']
						);
					$_arr = explode(".", $color['image']) ;
					$_file =$_arr[0] . '_upl_d1.' . $_arr[1] ;
					if (file_exists($Config['photoLib'].$_file)) {
						$_size = $this->sizeX($Config['photoLib'].$_file) ;
						$color['images'][] = array(
								'fileName' 	=> $_file,
								'width' 	=> $_size['x'],
								'height'	=> $_size['y']
							);
					}
					$_file =$_arr[0] . '_upl_d2.' . $_arr[1] ;
					if (file_exists($Config['photoLib'].$_file)) {
						$_size = $this->sizeX($Config['photoLib'].$_file) ;
						$color['images'][] = array(
								'fileName' 	=> $_file,
								'width' 	=> $_size['x'],
								'height'	=> $_size['y']
							);
					}
					unset($color['image']) ;

					$_file =$_arr[0] . '_icon.' . $_arr[1] ;

					if (file_exists($Config['photoLib'].$_file)) {
						$_allgood = 1 ;
					} else {
						$file_thump = $Config['photoLib'] . $ArticleNumber . '_' . $ColorNumber . '.jpg' ;
						$this->createcrop($file_thump,$_file,80,80, -75, -30);
						chmod ($_file, 0777) ;
					}

					$_size = $this->sizeX($Config['photoLib'].$_file) ;
					$color['styleIcon'][] = array(
							'fileName' 	=> $_file,
							'width' 	=> $_size['x'],
							'height'	=> $_size['y']
						);

//					$_iconfile['fileName'] =  $color['ColorRGB']['valueRed'] . '_' . $color['ColorRGB']['valueGreen'] . '_' . $color['ColorRGB']['valueBlue'] . '.png' ;
//					$color['styleIcon'][] = array_merge($_iconfile,$color['ColorRGB']) ;
					unset($color['ColorRGB']) ;

					foreach ($color['sizes'] as $skey => $size) {
						unset($rs) ; 
						$rs['sizeName'] = $skey ;	
						$rcs['sizes'][] = array_merge($rs, $size) ;
					}
					unset($color['sizes']) ;
					$rp['colors'][] = array_merge($rc, $color, $rcs) ; // $rc ;
				}
				$NewproductArray['products'][] = $rp ;
			}
			$productArray = $NewproductArray ;
			return $productArray;
    	}
		
		public function createcrop($name,$filename,$new_w,$new_h, $adj_x, $adj_y) {
			$system=explode('.',$name);
			if (preg_match('/jpg|jpeg/',$system[1])){
				$src_img=imagecreatefromjpeg($name);
			}
			if (preg_match('/png/',$system[1])){
				$src_img=imagecreatefrompng($name);
			}

			// Check dimensions
			$x=(imageSX($src_img)/2) - ($old_x/2) + $adj_x ;
			$y=(imageSY($src_img)/2) - ($old_y/2) + $adj_y ;
			
			$_arr = array (
				'x' 	=> $x,
				'y' 	=> $y,
				'width' 	=> $new_w,
				'height' 	=> $new_h
			) ;
			$dst_img=imagecrop($src_img,$_arr) ;

			if (preg_match("/png/",$system[1]))
			{
				imagepng($dst_img,$filename); 
			} else {
				imagejpeg($dst_img,$filename); 
			}
			imagedestroy($dst_img); 
			imagedestroy($src_img); 
		}
		
		public function sizeX($name) {
			$system=explode('.',$name);
			if (preg_match('/jpg|jpeg/',$system[1])){
				$src_img=imagecreatefromjpeg($name);
			}
			if (preg_match('/png/',$system[1])){
				$src_img=imagecreatefrompng($name);
			}

			// Check dimensions
			$x=imageSX($src_img) ;
			$y=imageSY($src_img) ;
			
			$_arr = array (
				'x' 	=> $x,
				'y' 	=> $y
			) ;
			imagedestroy($src_img); 
			
			return $_arr ;
		}
	}

