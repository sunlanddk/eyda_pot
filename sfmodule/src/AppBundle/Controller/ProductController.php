<?php

namespace AppBundle\Controller;

use AppBundle\Module\Export\Collection\ProductCollection;
use Doctrine\Common\Collections\ArrayCollection;
use Monolog\Logger;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Config\Definition\Exception\InvalidConfigurationException;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Product controller class
 *
 * @Route("/products")
 */
class ProductController extends Controller
{
    /**
     * This method export all products from POT system into Magento webshop
     *
     * @Route("/export/magento", name="export_magento")
     */
    public function exportMagentoAction(Request $request)
    {
        $db = $this->get('database_connection');
        $seasonId = $request->get('season');
        $productManager = $this->get('app.product.manager');
        $transporter = $this->get('app.transporter')->getProductTransporter();
        $productsCollection = $productManager->getWebProducts($seasonId);
        $isBreak = false;
        $onlyProduct = false;
        $error = '';
        $time_start = microtime(true);

        if ($productsCollection->isEmpty()) {
            $response = sprintf("<h1>Magento export</h1><p>No products found for export in season %s.</p>", $seasonId);

            return new Response($response);
        }
        $token = $transporter->authenticate();

        try {

            $db->beginTransaction();

//            $this->addSex($token);
            $this->addBrands($productsCollection, $token);
            $this->addColors($productsCollection, $token);
//            $this->addCategories($productsCollection, $token);
            $this->addSizes($productsCollection, $token);

            $db->commit();

        } catch (\Exception $e) {
            $db->rollBack();
            $isBreak = true;
            $error = $e->getMessage();
        }

        try {
            $responseJson = json_decode($this->addProducts($productsCollection, $seasonId, $token));
        } catch (\Exception $e) {
            $isBreak = true;
            $responseJson = false;
            $onlyProduct = true;
            $class = 'error';
            $error = $e->getMessage() . ' ' . $e->getLine();
        }

        $time_end = microtime(true);
        $time = $time_end - $time_start;

        /*if ($isBreak && $onlyProduct) {
            $response = sprintf("<h1>Magento export</h1><p>We have some error with export protudct only from season %s. Other data like: categories, colors, sizes was exported without any problem. What do you want to do:<br><a href=''>Star whole export process again</a><br><a href=''>Try export only files</a> <br><br><b>More info</b>: %s</p>", $seasonId, $error);
        }*/

        if ($isBreak) {
            $response = sprintf("<h1>Magento export</h1><p>We have some error with export in season %s. <br><b>More info</b>: %s</p>", $seasonId, $error);

            return new Response($response);
        }

        if (is_object($responseJson) && $responseJson->success) {
            $class = 'success';
        }

        if (is_object($responseJson)) {
            $msg = $responseJson->message;
        } else {
            $msg = sprintf('Export products from season %s was complete without any problems. You can now close this window. Time executed: %s', $seasonId, $time);
        }

        $response = "<h1>Magento export</h1><p style=\"\">$msg</p>";

        return new Response($response);
    }

    /**
     * This method clears all products on Magento webshop
     *
     * @Route("/clear/magento", name="clear_magento")
     */
    public function clearMagentoAction(Request $request)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        $token = $transporter->authenticate();
        
        $response = $transporter->cleanProduct($token);
        
        return new Response($response);
    }
    
    private function addCategories(ProductCollection $productsCollection, $token = null)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        if (!$token) {
            $token = $transporter->authenticate();
        }

        $catManager = $this->get('app.category.service');
        $categories = $catManager->getCategoriesByIds($productsCollection->getCategories());
        $mapSql = '';

        foreach($categories as $category) {
            $idPot = $category['id'];
            $idMagento = $transporter->sendCategory($token, $category);
            if(!is_numeric($idMagento)){
                $magCatObj = json_decode($idMagento);
                $idMagento = $magCatObj->value;
            }
            $mapSql .= $catManager->addCategoryMap($idPot, $idMagento);
        }

        $catManager->saveCategoryMapDB($mapSql);
    }

    private function addColors(ProductCollection $productCollection, $token = null)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        if (!$token) {
            $token = $transporter->authenticate();
        }

        $attrManager = $this->get('app.attribute.service');
        $colors = $attrManager->getColorsByIds($productCollection->getColors());
        $colorsForMap = $attrManager->getColorsByIds($productCollection->getColors(), true);
        $mapSql = '';

        $transporter->sendColors($token, $colors);
        $magentoColors = new ArrayCollection($transporter->getColors($token, false));

        foreach ($colorsForMap as $colMap) {
            $filter = $magentoColors->filter(function($s) use ($colMap){
                return strtoupper($s->label) == strtoupper($colMap['default_label']);
            })->first();

            if ($filter) {
                $idPot = $colMap['id'];
                $idMagento = $filter->value;

                $mapSql .= $attrManager->addColorMap($idPot, $idMagento);
            }
        }

        $attrManager->saveColorMapDB($mapSql);
    }

    private function addSizes(ProductCollection $productCollection, $token = null)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        if (!$token) {
            $token = $transporter->authenticate();
        }

        $attrManager = $this->get('app.attribute.service');
        $sizesForSend = $productCollection->getSizes();
        $sizesForMap = $productCollection->getSizes(true);
        $mapSql = '';

        $transporter->sendSizes($token, $sizesForSend);
        $magentoSizes = new ArrayCollection($transporter->getSizes($token));

        foreach ($sizesForMap as $size) {
            $filter = $magentoSizes->filter(function($s) use ($size){
                return $s->label == $size['name'];
            })->first();

            if ($filter) {
                $idPot = $size['id'];
                $idMagento = $filter->value;

                $mapSql .= $attrManager->addSizeMap($idPot, $idMagento);
            }
        }

        $attrManager->saveSizesMapDB($mapSql);
    }

    private function addSex($token = null)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        if (!$token) {
            $token = $transporter->authenticate();
        }

        $attrManager = $this->get('app.attribute.service');
        $sex = $attrManager->getSex();

        $mapSql = '';

        foreach($sex as $item) {
            $idPot = $item['id'];
            $idMagento = $transporter->sendSex($token, $item);
            if(!is_numeric($idMagento)){
                $magSexObj = json_decode($idMagento);
                $idMagento = $magSexObj->value;
            }
            $mapSql .= $attrManager->addSexMap($idPot, $idMagento);
        }

        $attrManager->saveSexMapDB($mapSql);
    }

    private function addBrands(ProductCollection $productCollection, $token = null)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        if (!$token) {
            $token = $transporter->authenticate();
        }

        $attrManager = $this->get('app.attribute.service');
        $brands = $attrManager->getBrands($productCollection->getBrands());
        $mapSql = '';

        foreach($brands as $brand) {
            $idPot = $brand['id'];
            $idMagento = $transporter->sendBrand($token, $brand);
            if(!is_numeric($idMagento)){
                $magBrandObj = json_decode($idMagento);
                $idMagento = $magBrandObj->value;
            }
            $mapSql .= $attrManager->addBrandMap($idPot, $idMagento);
        }

        $attrManager->saveBrandMapDB($mapSql);
    }

    private function addProducts(ProductCollection $productCollection, $season = null, $token = null)
    {
        $transporter = $this->get('app.transporter')->getProductTransporter();
        if (!$token) {
            $token = $transporter->authenticate();
        }

        $mapper = $this->get('app.mapper.factory')->createVariantCodeMapper();
        $options = $this->container->getParameter('pot_export');
        $productService = $this->get('app.product.manager');
        $catService = $this->get('app.category.service');
        $langService = $this->get('app.language.service');
        $langs = $langService->getLanguages();
        $variants = $productService->getVariants($productCollection->getVariants(), $season);
        //variant quantities
        foreach($variants as $vc => $variant){
            $productMapped = $mapper->map($vc);
            $productArray = $productMapped->toArray();
            $quantityArray = $productService->getProductQuantity($productArray['article_id'], $productArray['color_id'], $productArray['size_id']);
            if (false === isset($quantityArray[0])) {
                $quantityArray[0]['quantity'] = 0;
                $quantityArray[0]['shops'] = '';
            }
            if ($quantityArray[0]['quantity'] > 0) {
                $orderQuantity = $productService->getOrderQuantity($productArray['article_id'], $productArray['color_id'], $productArray['size_id']);

                if (true === isset($orderQuantity[0]['quantity']) && $orderQuantity[0]['quantity'] > 0) {
                    $quantityArray[0]['quantity'] -= $orderQuantity[0]['quantity'];
                }
                $quantityArray[0]['shops'] = '';
            } else {
                $urls = $productService->getShopUrls($productArray['article_id'], $productArray['color_id']);
                if (isset($urls[0])) {
                    $shops = array();
                    foreach ($urls as $key => $shop) {
                        $s['url'] = $shop['web'];
                        array_push($shops, $s);
                    }
                    $quantityArray[0]['shops'] = $shops;
                }
            }
            $variants[$vc]['quantity'] = $quantityArray[0]['quantity'];
            $variants[$vc]['shops'] = $quantityArray[0]['shops'];
        }

        $productsDivCollection = $productCollection->getProducts($variants, $catService->getMap(), $langs);

        //check currency config
        foreach ($langs as $l) {
            if (false === isset($options['currency'][$l['Culture']])) {
                throw new InvalidConfigurationException('Invalid currency configuration in parameters.yml');
            }
        }

        $sizes = $transporter->getSizes($token, false);
        $colors = $transporter->getColorsDef($token, false);

        return $transporter->sendProduct($token, $productsDivCollection, $langs, $colors->attribute_id, $sizes->attribute_id, $options['image_domain']);
    }
}
