<?php

namespace AppBundle\Controller;

use AppBundle\Exception\ApiException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Range;
use AppBundle\Controller\ApiControllerProjectSpecific;

/**
 * shipping controller class
 * @author Alex Roland <alexander.roland.a@gmail.com>
 * 
 * @Route("/api/shipping")
 */
class ShippingController extends Controller
{   

    /**
     * Return alex test
     *
     * @Route("/testalex", name="test_alex")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function testAlex(Request $request)
    {
        //$manager = $this->get('app.product.manager');
        return new JsonResponse(array('success' => false, 'message' => 'No products found'));
    }

    /**
     * Get Nearest available stock
     *
     * @Route("/stocklayout/available/position/{id}", name="get_available_position")
     * @Method({"GET"})
     * @param Request $request
     * @return JsonResponse
     */
    public function stocklayoutAvailablePosition(Request $request, $id)
    {
        //$manager = $this->get('app.product.manager');
        $shippingmanager = $this->get('app.shipping.handler');
        if($id != ''){
            // $position = $shippingmanager->getNextAvailablePosition($id);
            $position = $shippingmanager->getNextAvailablePosition();
        }
        else{
            $position = $shippingmanager->getNextAvailablePosition();
        }
        
        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => array('next' => $position, 'old' => '') ) );
    }

    /**
     * move GTIN
     *
     * @Route("/post/movegtin", name="post_move_gtin")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function moveGTIN(Request $request) {
        $shippingmanager = $this->get('app.shipping.handler');

        $aData = (array)json_decode($request->getContent(), true);

        $msg = $shippingmanager->moveGTIN($aData);

        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $msg));

    }

    /**
     * get stock by position
     *
     * @Route("/get/stockbyposition", name="get_stock_by_postion")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getStockByPostion(Request $request) {
        $shippingmanager = $this->get('app.shipping.handler');
        $aData = (array)json_decode($request->getContent(), true);
        
        $stock = $shippingmanager->getStockByPosition($aData['variantcode'], $aData['position']);
        
        if(getType($stock) == 'string') {
            return new JsonResponse(array('error' => true, 'errorcode' => '', 'data' => $stock));
        }

        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $stock));
    }


    /**
     * update quantity of item
     *  
     * @Route("/post/update/quantity/item", name="post_update_quantity_item")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function updateQuantityOfItem(Request $request) {

        $shippingmanager = $this->get('app.shipping.handler');
        $aData = (array)json_decode($request->getContent(), true);

        $response = $shippingmanager->updateItemQuantityOnPosition($aData['position'], $aData['variantcode'], $aData['oldQuantity'], $aData['updatedQuantity']);
        
        if(getType($response) == 'string') {
            return new JsonResponse(array('error' => true, 'errorcode' => '', 'data' => $response));
        }

        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $response));
    }


    /**
     * get sscc container ai information
     *
     * @Route("/get/sscc/ai/info", name="get_sscc_ai_info")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getSsccAiInfo(Request $request)
    {
        $aData = (array)json_decode($request->getContent(), true);
        $shippingmanager = $this->get('app.shipping.handler');


        $ais = $shippingmanager->getSsccAiInformation($aData['sscc']);
        if(getType($ais) == 'string' ){
            return new JsonResponse(array('error' => true, 'errorcode' => $ais, 'data' => $ais));            
        }

        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $ais));
    }

    /**
     * Receive shipment
     *
     * @Route("/get/sscc/available", name="get_sscc_available")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getFullSSCC(Request $request)
    {
        $aData = (array)json_decode($request->getContent(), true);
        $shippingmanager = $this->get('app.shipping.handler');


        // $position = $shippingmanager->getSsccFromStockByItemOrPosition($aData['type'], $aData['scanned']);
        $positions = $shippingmanager->getVariantReplenishPositions($aData['position']);
        if(getType($positions) == 'string' ){
            return new JsonResponse(array('error' => true, 'errorcode' => $positions, 'data' => $positions));            
        }
        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $positions));
    }

    /**
     * Replenish variant on position
     *
     * @Route("/replenish/from/pick", name="replenish_from_pick")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function replenishFromPickContainer(Request $request)
    {
        $aData = (array)json_decode($request->getContent(), true);
        $shippingmanager = $this->get('app.shipping.handler');

        $positions = $shippingmanager->replenishFromPickContainer($aData['position'], $aData['variantcode'], $aData['qty'], $aData['userid']);

        if($positions === true){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $positions));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $positions, 'data' => $positions));
        }
    }

    /**
     * Replenish variant on position
     *
     * @Route("/replenish/variant", name="replenish_varitant")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function replenishVariantAtPosition(Request $request)
    {
        $aData = (array)json_decode($request->getContent(), true);
        $shippingmanager = $this->get('app.shipping.handler');

        $positions = $shippingmanager->replenishVariantBySscc($aData['position'], $aData['sscc']);

        if($positions !== true ){
            return new JsonResponse(array('error' => true, 'errorcode' => $positions, 'data' => $positions));            
        }
        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $positions));
    }



    /**
     * Receive shipment
     *
     * @Route("/stocklayout/available/position/by/article", name="available_position_article")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function availablePositionByArticle(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $sscc = $aData['sscc'];

        $shippingmanager = $this->get('app.shipping.handler');

        $position = $shippingmanager->getNextAvailablePositionByArticle($sscc);
        if(getType($position) == 'string'){
            return new JsonResponse(array('error' => true, 'errorcode' => $position, 'data' => $position));            
        }

        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $position));
    }

    /*public function getSsccAiInformation($ssccFull){
        $sscc = $this->convertSingleCode($ssccFull);
        $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
           'sscc' => $sscc,
           'active' => 1
        ]);

        if(gettype($container) == 'object'){
            $item = $this->em->getRepository('AppBundle\Entity\Item')->findOneBy([
                'containerid' => $container->getId(),
                'active' => 1
            ]);
            if(gettype($item) != 'object'){
                return array('ais' => false, 'ailist' => $this->getAiDescription());                
            }

            $aisRows = $this->getAiFromItem($item->getId());
            $ais = $this->convertAiRowsToArray($aisRows);
            return array('ais' => $ais, 'ailist' => $this->getAiDescription());
        }

        return array('ais' => false, 'ailist' => $this->getAiDescription());
    }

    public function getAiDescription(){
        $query = sprintf("SELECT * FROM ai");
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            return false;
        } 
        $list = array();
        foreach ($fetch as $key => $ai) {
            $temp = array(
                'ai'            => $ai['Name'],
                'description'   => $ai['Description'],
                'ailength'        => $ai['Length'],
            );
            array_push($list, $temp);
        }

        return $list;
    }

    public function convertAiRowsToArray($rows){
        $ais = array();
        foreach ($rows as $key => $ai) {
            if($ai['Name'] === 'RDT'){
                continue;
            }
            $tmp = array(
                'ai'            => $ai['Name'],
                'value'         => $ai['Value'],
                'description'   => $ai['Description'],
            );
            array_push($ais, $tmp);
        }

        return $ais;
    }

    public function getAiFromItem($itemId){
        $query = sprintf("SELECT * FROM (variant_ai va, ai) WHERE va.ItemId=%d AND ai.Id=va.AiId", $itemId);
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch;  
        }
    } 
    */

    /**
     * Receive shipment
     *
     * @Route("/stocklayout/update/position", name="update_position")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function updatePosition(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        if((int)$aData['delete'] === 1 ){
            $position = $shippingmanager->deletePosition($aData);
        }
        else{
            $position = $shippingmanager->updatePosition($aData);
        }
        

        if($position !== true){
            return new JsonResponse(array('error' => true, 'errorcode' => $position, 'data' => $position));            
        }

        return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $position));
    }


    /**
     * Receive shipment
     *
     * @Route("/inbound", name="inbound_shipment")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function inboundShipment(Request $request)
    {
        $aData = (array)json_decode($request->getContent());

        $shippingmanager = $this->get('app.shipping.handler');

        $containers = $shippingmanager->covertCodes($aData);
        
        if(gettype($containers) == 'string'){
            return new JsonResponse(array('error' => true, 'errorcode' => $containers, 'data' => $containers));
        }

        $containerCheck = $shippingmanager->checkInbouncContainers($containers);

        if($containerCheck[0] === false){
            return new JsonResponse(array('error' => true, 'errorcode' => 'One of the SSCC already exist in stock. ('.$containerCheck[1].')', 'data' => $containers));
        }

        $shipmentId = $shippingmanager->createInboundShipment();
        $containers = $shippingmanager->createInboundContainers($containers, $shipmentId);

        if($containers === true){
            include_once('../../lib/config.inc');
            include_once(_LEGACY_LIB.'/lib/db.inc');
            $config = $Config;
            global $Record, $DontOpen, $Config, $Page, $MyCompany, $Company, $Total;

            $Page = 'item';
            $Config = $config;
            $Record = $shippingmanager->createPrintNoteRecord($shipmentId);
            $DontOpen = true;
            include_once(_LEGACY_LIB.'/module/shipment/printnote.inc');
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $shipmentId));    
        }

        
        
        // $return = $shippingmanager->testAlex($aData);
        return new JsonResponse(array('error' => true, 'errorcode' => $containers, 'data' => $containers));
    }

    


    /**
     * Store sku
     *
     * @Route("/store/sku", name="store_sku")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function storeSku(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        $return = $shippingmanager->storeSku($aData['scanned'], $aData['position'], $aData['type'], $aData['quantity'], $aData['stock']);
        $returnData = $return;
        if(isset($return['return']) === true){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $return['position']));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $return, 'data' => $return));
        }
    }

    /**
     * Store sku
     *
     * @Route("/sku/get/stock", name="sku_get_stock")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function stockGetSku(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        $return = $shippingmanager->getStockCount($aData['variantcode'], $aData['stock'], (isset($aData['replenish']) === true ? true : false), (isset($aData['pick']) === true ? true : false));

        if(getType($return) === 'array'){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $return));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $return, 'data' => $return));
        }
    }


    /**
     * Store container
     *
     * @Route("/store/container", name="store_container")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function storeContainer(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        $container = $shippingmanager->storeContainer($aData['sscc'], $aData['position']);

        if(gettype($container) == 'array'){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $container));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $container, 'data' => $container));
        }
    }

    /**
     * Create and finalize outbound shipment
     *
     * @Route("/outbound/new", name="new_outbound")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function newOutbound(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        // return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $aData));
        $shipmentId = $shippingmanager->createNewOutboundWithSscc($aData['items'], (int)$aData['stockid'], (int)$aData['id'], true);

        $shippingmanager->createNewOutboundWithoutSscc($aData['notscanable'], (int)$shipmentId, (int)$aData['id']);

        
        if((int)$aData['id'] > 0){
            $shippingmanager->updatePickorder($aData['id'], $shipmentId);
        }

        if($shipmentId > 0){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $shipmentId));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $shipmentId, 'data' => $shipmentId));
        }
    }

    /**
     * Save outbound shipment
     *
     * @Route("/outbound/save", name="save_outbound")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function saveOutbound(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        if((int)$aData['totalpending'] === 0){
            // $doneShipment = true;
            $doneShipment = false;
        }
        else{
            $doneShipment = false;   
        }

        $shipmentId = $shippingmanager->createNewOutboundWithSscc($aData['items'], (int)$aData['stockid'], (int)$aData['id'], $doneShipment);

        $shippingmanager->createNewOutboundWithoutSscc($aData['notscanable'], (int)$shipmentId, (int)$aData['id']);

        $shippingmanager->updatePickorderToId($aData['id'], $shipmentId);   
        
        if((int)$aData['totalpending'] === 0){
            $shippingmanager->updatePickorder($aData['id'], $shipmentId);
        }

        if($shipmentId > 0){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $shipmentId));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $shipmentId, 'data' => $shipmentId));
        }
    }

    /**
     * get container data
     *
     * @Route("/container/data", name="get_container_data")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getContainerData(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        // return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $aData));
        $container = $shippingmanager->getContainersData($aData['sscc']);

        if(getType($container) == 'array'){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $container));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $container, 'data' => $container));
        }
    }


    /**
     * pickorderlines
     *
     * @Route("/pickorder/lines", name="get_pickorderline")
     * @Method({"POST"})
     * @param Request $request
     * @return JsonResponse
     */
    public function getPickorderLines(Request $request)
    {
        $aData = (array)json_decode($request->getContent());
        $shippingmanager = $this->get('app.shipping.handler');

        // return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $aData));
        $lines = $shippingmanager->getPickorderline($aData['id']);

        if(getType($lines) == 'array'){
            return new JsonResponse(array('error' => false, 'errorcode' => '', 'data' => $lines));
        }
        else{
            return new JsonResponse(array('error' => true, 'errorcode' => $lines, 'data' => $lines));
        }
    }


}
