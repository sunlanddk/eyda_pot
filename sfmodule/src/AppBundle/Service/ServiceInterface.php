<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Interface Service
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
interface ServiceInterface
{
    /**
     * @return EntityManager
     */
    public function getEntityManager();

    /**
     * @return Container
     */
    public function getContainer();

}
