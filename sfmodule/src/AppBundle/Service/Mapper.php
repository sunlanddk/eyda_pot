<?php

namespace AppBundle\Service;

use AppBundle\Module\Mapper\MapperInterface;
use AppBundle\Module\Mapper\VariantCodeMapper;
use Doctrine\Common\Proxy\Exception\InvalidArgumentException;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Module\Mapper\Mapper as MapperFactory;

/**
 * Class Mapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
class Mapper extends Service
{
    /**
     * @var Mapper $factory
     */
    private $factory;

    /**
     * @var EntityManager $entityManager
     */
    private $entityManager;

    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->factory = new MapperFactory();
        $this->entityManager = $em;
    }

    /**
     * @return VariantCodeMapper
     */
    public function createVariantCodeMapper()
    {
        return $this->factory->create('variantcode', [$this->entityManager]);
    }

    /**
     * @param string $type Type of magento mappers. Allows [category, color, sex, size, variant]
     *
     * @return MapperInterface
     */
    public function createMagentoMapper($type)
    {
        $allowTypes = ['category', 'color', 'sex', 'size', 'variant'];

        if (false === in_array($type, $allowTypes)) {
            throw new InvalidArgumentException(sprintf('Mapper type `%s` is not allowed.', $type));
        }

        return $this->factory->create($type, [$this->entityManager]);
    }
}
