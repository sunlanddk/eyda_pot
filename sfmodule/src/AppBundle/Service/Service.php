<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;

/**
 * Class Service
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
abstract class Service implements ServiceInterface
{
    /**
     * @var EntityManager $em
     */
    protected $em;

    /**
     * @var Container $container
     */
    protected $container;

    /**
     * @var array $options
     */
    protected $options;

    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        $this->em = $em;
        $this->container = $container;
        $this->options = $this->container->getParameter('pot_export');
    }

    /**
     * @return EntityManager
     */
    public function getEntityManager()
    {
        return $this->em;
    }

    /**
     * @return Container
     */
    public function getContainer()
    {
        return $this->container;
    }
}
