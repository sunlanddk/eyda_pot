<?php

namespace AppBundle\Service;

use AppBundle\Module\Export\Collection\ProductCollection;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Product as ProductModel;
use AppBundle\Model\Season;

/**
 * Class Product
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
class Product extends Service
{
    /**
     * @var ProductModel $productModel
     */
    private $productModel;

    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->productModel = new ProductModel();
    }

    /**
     * Get web products from POT system.
     *
     * @param null|int $season season id
     *
     * @return ProductCollection
     */
    public function getWebProducts($season = null)
    {
        $statement = $this->em->getConnection()
            ->prepare($this->productModel->getWebProductsQuery($this->options['language_ids'], $this->options['currency_ids'], $this->options['webshop_id'], $season));
        $statement->execute();

        return new ProductCollection($statement->fetchAll(), $this->container);
    }

    public function getOrderQuantity($article, $color, $size)
    {
        $query = $this->productModel->getProductOrderQuantityQuery($article, $color, $size);
        $quantity = $this->em->getConnection()->prepare($query);
        $quantity->execute();

        return $quantity->fetchAll();
    }

    /**
     * Return total quantity of specific variant (article, color, size).
     *
     * @param int $article article id from POT DB
     * @param int $color   color id from POT DB
     * @param int $size    size id from POT DB
     *
     * @return array
     */
    public function getProductQuantity($article, $color, $size)
    {
        $seasonModel = new Season();
        $stocks = $this->em->getConnection()->prepare($seasonModel->getWebshopStocksQuery($this->options['webshop_id']));
        $stocks->execute();

        $query = $this->productModel->getProductInventoryQuantityQuery($article, $color, $size, $this->options['webshop_id'], $stocks->fetchAll());
        $quantity = $this->em->getConnection()->prepare($query);
        $quantity->execute();

        return $quantity->fetchAll();
    }

    /**
     * Return list of shops where specific product is available for buy
     *
     * @param int $article article id from POT db
     * @param int $color   article id from POT db
     *
     * @return array
     */
    public function getShopUrls($article, $color)
    {
        $query = $this->productModel->getAvailableUrlShops($article, $color, $this->options['webshop_id']);
        $urls = $this->em->getConnection()->prepare($query);
        $urls->execute();

        return $urls->fetchAll();
    }

    /**
     * Return all data for variants
     *
     * @param array    $variants simple array with variant_codes only
     * @param null|int $season Season id
     *
     * @return array
     */
    public function getVariants($variants, $season = null)
    {
        $seasonModel = new Season();
        $stocks = $this->em->getConnection()->prepare($seasonModel->getWebshopStocksQuery($this->options['webshop_id']));
        $stocks->execute();

        $query = $this->productModel->getVariantsQuery($variants, $this->options['webshop_id'], $stocks->fetchAll());
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();

        return $variants->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);
    }

    /**
     * Method return from POT every order for each variant passed in param
     *
     * @param array $variants all variants collection for getting orders of each
     *
     * @return array
     */
    public function getVariantsOrder($variants)
    {
        $query = $this->productModel->getVariantOrdersQuery($variants);
        $orderVariants = $this->em->getConnection()->prepare($query);
        $orderVariants->execute();

        return $orderVariants->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);
    }
}
