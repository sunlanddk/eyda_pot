<?php

namespace AppBundle\Service;

use AppBundle\Entity\Order;
use AppBundle\Entity\Orderline;
use AppBundle\Entity\Orderquantity;
use AppBundle\Entity\Consolidatedorder;
use AppBundle\Entity\Pickorder;
use AppBundle\Entity\Pickorderline;
use AppBundle\Entity\Variantcode;
use AppBundle\Exception\ApiException;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Company;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\Collection;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Component\Validator\Constraints\DateTime;
use Symfony\Component\Validator\Constraints\DateTimeZone;
use Symfony\Component\Validator\Validator\RecursiveValidator;
use Symfony\Component\Validator\Constraints\NotBlank;
use Symfony\Component\Validator\Constraints\Range;
use Symfony\Component\Validator\Constraints\Type;

/**
 * Class OrderService
 * @author Mariusz Angielski <mariusz.angielski@jcommerce.pl>
 * @package AppBundle\Service
 */
class OrderService extends Service
{
    const ORDER_TYPE_ID     = 10;       // Order generated from webshop
    const MAGENTO_USER      = 1;        // Magento system user
    /*const DeliveryTermId    = 52;       // KEN
    const PaymentTermId     = 9;
            // Ken*/
    const DeliveryTermId    = 1; // Ex Worx ?
    const PaymentTermId     = 155; // Prepaid
    const PickStockId     	=  14321;
    const ShippingArticleId = 1; // 5712199059930
    const RabatArticleId    = 376; // 5712199059920

    const DEFAULT_DESC      = 'Generated from Webshop Order: ';

    /**
     * @var Mapper
     */
    protected $oMapper;

    /**
     * @var RecursiveValidator
     */
    protected $oValidator;


    private $lineIndex=1;
    /**
     * OrderService constructor.
     *
     * @param EntityManager      $em
     * @param Container          $container
     * @param Mapper             $oMapper
     * @param RecursiveValidator $oValidator
     */
    public function __construct(EntityManager $em, Container $container, Mapper $oMapper, RecursiveValidator $oValidator)
    {
        $this->oMapper      = $oMapper;
        $this->oValidator   = $oValidator;
        parent::__construct($em, $container);
    }

    /**
     * Get currency rate by currency id
     * @param $iCurrencyId
     * @return int
     */
    protected function getCurrencyExchangeRate($iCurrencyId)
    {
        $oCurrency = $this->em->getRepository('AppBundle\Entity\Currency')->find($iCurrencyId);

        return $oCurrency->getRate();
    }


    /**
     * @param array $aData
     * @param Collection $oConstraints
     */
    protected function validateData(array $aData, Collection $oConstraints)
    {
        $oViolations = $this->oValidator->validateValue($aData, $oConstraints);

        if(!empty($oViolations)) {
            foreach ($oViolations as $oViolation) {
                if (Collection::NO_SUCH_FIELD_ERROR == $oViolation->getCode()) {
                    continue;
                }
                throw new ApiException($oViolation->getMessage() . ' ' . $oViolation->getPropertyPath(), ApiException::CODE_PARAMETER_BAD_FORMAT);
            }
        }
    }

    /**
     * Validate order data
     * @param array $aData
     * @throws ApiException
     */
    protected function validateOrderData(array $aData)
    {
        $oConstraints = new Collection([
            'Name'          => new NotBlank(),
            'Address1'      => new NotBlank(),
            'ZIP'           => new NotBlank(),
            'CountryId'     => new Type(['type' => 'numeric']),
            'CarrierId'     => new Type(['type' => 'numeric']),
            'Phone'         => new NotBlank(),
            'OrderId'       => new NotBlank(), // Type(['type' => 'numeric']),
            'Season'        => new NotBlank(),
            'WebshopNo'     => new NotBlank(),
            'Articles'      => new NotBlank()
        ]);

        $oArticleConstraints = new Collection([
            'VariantCode'   => new NotBlank(),
            'Price'         => new Type(['type' => 'numeric']),
            'Quantity'      => new Type(['type' => 'numeric']),
            //'Discount'      => new Type(['type' => 'numeric']),
        ]);

        $this->validateData($aData, $oConstraints);
        $aArticles = $aData['Articles'];

        // Check parameter format JSON array
        if(!is_array($aArticles)) {
            throw new ApiException('Bad "Articles" parameter format. Need array.', ApiException::CODE_PARAMETER_BAD_FORMAT);
        }

        // Validating articles
        foreach ($aArticles as $aArticle) {
            $this->validateData($aArticle, $oArticleConstraints);
        }
    }

    /**
     * Create new order
     * @param array $aOrder
     * @return int
     */
    public function newOrder(array $aOrder)
    {
        if (empty($aOrder)) {
            die('empty order!!!');
        } else {
 //           print_r($aOrder);
 //           die(' -------------------- order OK');
        }

        // Create Consolidated order
        $cOrder = new Consolidatedorder();
        $cOrder->setCreatedate(new \DateTime())
                ->setCreateuserid(self::MAGENTO_USER)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(self::MAGENTO_USER)
                ->setActive(true)
        ;
        $this->em->persist($cOrder);
        $this->em->flush();



        $options = $this->container->getParameter('pot_export');
        $aOrder['WebshopNo'] = $options['webshop_id'];
        $companyId = $options['company_id'];
        $tocompanyId = $options['to_company_id'];
        $this->validateOrderData($aOrder);

//        $langId = $this->getLanguageId($aOrder['StoreCode']);
        $vat = (int)$this->getVAT($aOrder['CountryId']);
		if (!$aOrder['DeliveryName']=='' and isset($aOrder['DeliveryName'])) {
			$aOrder['Address1'] = $aOrder['DeliveryName'] ;
			$aOrder['Address2'] = $aOrder['DeliveryAddress1'] ;
			$aOrder['ZIP'] = $aOrder['DeliveryZIP'] ;
			$aOrder['City'] = $aOrder['DeliveryCity'] ;
		} else {
			$aOrder['Address2'] = $aOrder['Address1'] ;
			$aOrder['Address1'] = $aOrder['Name'] ;
		}

		$_datetime = new \DateTime() ;
		$_datetime->setTimezone(new \DateTimeZone('GMT'));

        $oNullDate      = \DateTime::createFromFormat('Y-m-d H:i:s', '0000-00-00 00:00:00');
        $seasonOrders = array();

        foreach ($aOrder['Articles'] as $key => $value) {
            $temporder = $aOrder;
            unset($temporder['Articles']);
			if ($_seasonvalue>0) {
				$value['Season'] = $_seasonvalue ;
			} else {
				$value['Season'] = $this->getSeasonIdByCollectionMemberId((int)$value['CollectionMemberId']) ;
				$_seasonvalue = $value['Season'] ;
			}
/* Later when disount, freigth, consolidated order etc is resolved.
			$value['Season'] = $this->getSeasonIdByCollectionMemberId((int)$value['CollectionMemberId']) ;
			if ($value['Season']) {
				$_seasonvalue = $value['Season'] ;
			} else {
				$value['Season'] = $_seasonvalue ;
			}
*/
            $seasonOrders[$value['Season']]['order'] = $temporder;
            $seasonOrders[$value['Season']]['Articles'][] = $value;
            $seasonOrders[$value['Season']]['order']['Season'] =  $value['Season'];
        }

        // Create one pickorder for all items
        $pOrder = new Pickorder();
        $pOrder->setCreatedate($_datetime)
                ->setCreateuserid(self::MAGENTO_USER)
                ->setModifydate($_datetime)
                ->setModifyuserid(self::MAGENTO_USER)
                ->setActive(true)
                ->setType("SalesOrder")
                ->setReference(0) 
                ->setReferenceid(0)
//                ->setReference($oOrder->getId()) 
//                ->setReferenceid((int)$oOrder->getId())
                ->setConsolidatedid((int)$cOrder->getId())
                //->setFromid($companyId)
                ->setFromid(self::PickStockId)
                ->setOwnercompanyid($tocompanyId)
                ->setHandlercompanyid($tocompanyId)
                ->setInstructions("")
                ->setPacked(0)
                ->setDeliverydate($_datetime)
                ->setExpecteddate($_datetime)
                ->setPickeddate($oNullDate)
                ->setPackeddate($oNullDate)
                ->setCompanyid($companyId)
                ->setUpdated(0);
        $this->em->persist($pOrder);
        $this->em->flush();

        // Go Through seasons to create orders
        foreach ($seasonOrders as $seasonId => $seasonOrder) {

    		
            $oOrder = new Order();
            $oOrder->setConsolidatedid($cOrder->getId())
                    ->setCreatedate($_datetime)
                    ->setCreateuserid(self::MAGENTO_USER)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(self::MAGENTO_USER)
                    ->setActive(true)
                    ->setNumber(0)
                    ->setDescription(self::DEFAULT_DESC . ' ' . $seasonOrder['order']['OrderId'])
                    ->setCompanyid($companyId)
                    ->setLanguageid($langId)
                    ->setCarrierid($seasonOrder['order']['CarrierId'])
                    ->setDeliverytermid(self::DeliveryTermId)
                    ->setPaymenttermid(self::PaymentTermId)
                    ->setCurrencyid($seasonOrder['order']['CurrencyId'])
                    ->setSalesuserid(self::MAGENTO_USER)
                    ->setDiscount((int)$seasonOrder['order']['Discount'])
                    ->setReference($seasonOrder['order']['OrderId'])
                    ->setExchangerate($this->getCurrencyExchangeRate($seasonOrder['order']['CurrencyId']))
                    ->setInvoiceheader('')
                    ->setInvoicefooter('')
                    ->setAltcompanyname($seasonOrder['order']['Name'])
                    ->setAddress1($seasonOrder['order']['Address1'])
                    ->setAddress2($seasonOrder['order']['Address2'])
                    ->setZip($seasonOrder['order']['ZIP'])
                    ->setCity($seasonOrder['order']['City'])
                    ->setCountryid($seasonOrder['order']['CountryId'])
                    ->setReady(1)
                    ->setReadyuserid(self::MAGENTO_USER)
                    ->setReadydate($_datetime)
                    ->setDone(0)
                    ->setDoneuserid(0)
                    ->setDonedate($_datetime)
                    ->setOrderdoneid(0)
                    ->setDraft(false)
                    ->setTocompanyid($tocompanyId)
                    ->setInstructions($seasonOrder['order']['Comment'])
                    ->setEmail($aOrder['Email'])
                    ->setShippingShop($aOrder['ShippingShop'])
                    ->setOrdertypeid(self::ORDER_TYPE_ID)
                    ->setReceiveddate(\DateTime::createFromFormat('Y-m-d H:i:s', '0001-01-01 12:00:00'))
                    ->setSeasonid($seasonId)
                    ->setVariantcodes(0)
                    ->setPurchaseorderid(0)
                    ->setTopurchaseorderid(0)
                    ->setWebshopno($seasonOrder['order']['WebshopNo'])
                    ->setProposal(0)
                    ->setPhone($seasonOrder['order']['Phone'])
            ;

    		error_log(__LINE__ . ' ' . var_export($seasonOrder['order'], 1));

            $aShipping['shippingLabel'] = $seasonOrder['order']['shippingLabel'];
            $aShipping['shippingCost'] = $seasonOrder['order']['shippingCost'];
            $db = $this->container->get('database_connection');
            $db->beginTransaction();
            try {

                //$this->em->persist($oOrder);
                //$this->em->flush();
    //           print_r($aOrder);
    //           die(' -------------------- order OK');
                $this->em->persist($oOrder);
                $this->em->flush();

                $tempPorder = $this->em->getRepository('AppBundle\Entity\Pickorder')->find($pOrder->getId());
                $tempPorder->setReference($oOrder->getId()) 
                        ->setReferenceid($oOrder->getId())
                ;
                $this->em->persist($tempPorder);
                $this->em->flush();

                $this->createOrderlines($oOrder, $pOrder, $seasonOrder['Articles'], $aShipping, $langId, $vat);
                //$this->createOrderlines($oOrder, $aOrder['Articles'], $aShipping, $langId, $vat);
                $db->commit();
            } catch (\Exception $e) {
                $db->rollBack();
    			error_log(__LINE__ . ' ' . $e->getMessage());
                throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);
            }

    		// Transfer pick order to Alpi.
    		global $Config, $Id, $Record, $Navigation, $User, $test;
    		require_once __DIR__.'\..\..\..\..\lib\config.inc' ;
    		require_once __DIR__.'\..\..\..\..\lib\db.inc' ;
    		require_once __DIR__.'\..\..\..\..\lib\file.inc' ;
    		require_once __DIR__.'\..\..\..\..\lib\table.inc' ;
    		require_once __DIR__.'\..\..\..\..\module\pick\packcsv-cmd.inc' ;
    		CreatePickCSV($oOrder->getId()) ;

    		$_pdffile=$this->generatePdfFile($oOrder->getId()) ;
    		
    		//preparing pdf attachment for the mail
    	   $_attachmentArray = array(
    			array(
    				'path'     => $_pdffile,
    				'mimeType' => 'application/pdf',
    				'name'     => sprintf('Order #%d.pdf', $oOrder->getId())
    			)
    		);

    		// Send mail
    		require_once __DIR__.'\..\..\..\..\lib\mail.inc' ;
    		$_bodytxt = tableGetFieldWhere('maillayout','Body_default','Mark="weborderconf"')  ;
    //		$_param['body'] = nl2br(htmlentities($_bodytxt)) . '<br>' ;
    		$_param['body'] = nl2br($_bodytxt) . '<br>' ;
    		$_mark="weborderconf" ;
    		if ($Config['Instance'] == 'Test')
    //			sendmail($_mark, self::MAGENTO_USER, NULL, $_param, NULL, false, $aOrder['Email'], false, null, sprintf(' to %s Order #%d', $aOrder['Name'], $oOrder->getId()));
    			sendmail($_mark, $companyId, NULL, $_param, NULL, false, $aOrder['Email'], false, $_attachmentArray, sprintf(' to %s Order #%d', iconv("UTF-8", "ISO-8859-1//TRANSLIT", $aOrder['Name']), $oOrder->getId()));
//    			sendmail($_mark, $companyId, NULL, $_param, NULL, false, $aOrder['Email'], false, $_attachmentArray, sprintf(' to %s Order #%d', utf8_decode($aOrder['Name']), $oOrder->getId()));
    //			sendmail($_mark, $companyId, NULL, $_param, NULL, false, $aOrder['Email'], false, $_attachmentArray, sprintf(' to %s Order #%d', $aOrder['Name'], $oOrder->getId()));
    		else {
    			sendmail($_mark, $companyId, NULL, $_param, NULL, false, $aOrder['Email'] . ', sales@eyda.dk', false, $_attachmentArray, sprintf(' to %s Order #%d', $aOrder['Name'], $oOrder->getId()));
				// Update webshop available quantities
				require_once __DIR__.'\..\..\..\..\module\order\stockExportToWeb.php' ;
				sendStockDataToWebshop($oOrder->getId()) ;
    		}
    		

        } // ENd of seasons array

        //return $cOrder->getId();
        return $oOrder->getId()  ;
    }

     protected function generatePdfFile($_oid) {
        global $User, $Id, $Config, $Record, $ProformaInvoice, $ProformaCustoms;

//        $User = $this->_user;
        $Id   = $_oid;
//        $Navigation['Function'] = 'printorder';
//		require_once __DIR__.'\..\..\..\..\module\order\load.inc' ;
		
        $Config['savePdfToFile'] = true;
		$ProformaInvoice=0; $ProformaCustoms=0;$Record['ToCompanyId']=787; $User['CompanyId']=787 ;
		require_once __DIR__.'\..\..\..\..\module\order\printorder.inc' ;
        $Config['savePdfToFile'] = false;

         return fileName('order', (int)$Id);
    }


    /**
     * Create orderlines
     * @param Order $oOrder
     * @param array $aArticles
     */
    protected function createOrderlines(Order $oOrder, Pickorder $pOrder, array $aArticles, array $aShipping, $langId = 4, $vat)
    {
        $oVariantCodeMapper = $this->oMapper->createVariantCodeMapper();
        $options = $this->container->getParameter('pot_export');
        $companyId = $options['company_id'];
        $tocompanyId = $options['to_company_id'];
        $oNullDate      = \DateTime::createFromFormat('Y-m-d H:i:s', '0000-00-00 00:00:00');

		$_datetime = new \DateTime() ;
		$_datetime->setTimezone(new \DateTimeZone('GMT'));
		
/*
        $pOrder = new Pickorder();

        $pOrder->setCreatedate($_datetime)
                ->setCreateuserid(self::MAGENTO_USER)
                ->setModifydate($_datetime)
                ->setModifyuserid(self::MAGENTO_USER)
                ->setActive(true)
                ->setType("SalesOrder")
                ->setReference($oOrder->getId()) 
                ->setReferenceid((int)$oOrder->getId())
                //->setFromid($companyId)
                ->setFromid(self::PickStockId)
                ->setOwnercompanyid($tocompanyId)
                ->setHandlercompanyid($tocompanyId)
                ->setInstructions("")
                ->setPacked(0)
                ->setDeliverydate($_datetime)
                ->setExpecteddate($_datetime)
                ->setPickeddate($oNullDate)
                ->setPackeddate($oNullDate)
                ->setCompanyid($companyId)
                ->setUpdated(0)
        ;
            $this->em->persist($pOrder);
            $this->em->flush();
            */
		// Preprocess possible discount amount and recalcute to percentage.
		// Special FUB function since webshop cant handle percentage
		$_discountamount=0; $_stylesamount=0; $_discountpercentage=0;
        foreach ($aArticles as $key => $aArticle) {
			switch ($aArticle['VariantCode']){
				case '5712199059920': 	// Discount amount line.
					$_discountamount -= (float)$aArticle['Price'] ;
					unset($aArticles[$key]) ;
					break ;
				case '5712199059930': 	// Freigth line (no discount)
					break;
				default:				// Styles line
					$_stylesamount += (float)$aArticle['Price']*(float)$aArticle['Quantity'] ;
					break;
			}
		}
		if ($_stylesamount>0)
			$_discountpercentage = (int)((100*$_discountamount)/$_stylesamount) ;
//			$_discountpercentage = (int)round((100*$_discountamount)/$_stylesamount,0) ;

		// Create Orderlines
        foreach ($aArticles as $iIndex => $aArticle) {
            //error_log(__LINE__ . ' iIndex ' . var_export($iIndex, 1));
			if ($_discountpercentage>0) { // Fub specific overwrite if a discount amount is representing percentage
				switch ($aArticle['VariantCode']){
					case '5712199059930': 	// Freigth line (no discount)
						break;
					default:				// Styles line
						$aArticle['Discount'] = $_discountpercentage ;
						break;
				}
			}
            $mapper = $oVariantCodeMapper->map($aArticle['VariantCode']);

            if (!$mapper) {
                throw new ApiException('VariantCode ' . $aArticle['VariantCode'] . ' not found', ApiException::CODE_NOT_FOUND);
            }
            // Order lines
            $aMappedArticle = $mapper->toArray();
            $oOrderline     = new Orderline();

            $oOrderline->setCreatedate($_datetime)
                        ->setCreateuserid(self::MAGENTO_USER)
                        ->setModifydate($_datetime)
                        ->setModifyuserid(self::MAGENTO_USER)
                        ->setActive(true)
                        //->setNo($iIndex)
                        ->setNo($this->lineIndex)
                        ->setInvoicefooter('')
                        ->setOrderid($oOrder->getId())
                        ->setCaseid(0)
                        ->setArticleid($aMappedArticle['article_id'])
                        ->setArticlecolorid($aMappedArticle['color_id'])
                        ->setQuantity($aArticle['Quantity'])
                        ->setSurplus(0)
                        ->setPricesale((float)($aArticle['Price']*(100/(100+$vat))))
                        ->setPricecost(0)
                        ->setDeliverydate($_datetime)
                        ->setArticlecertificateid(0)
                        ->setPrefdeliverydate($oNullDate)
                        ->setRequestcomment('')
                        ->setCustartref('')
                        ->setProductionid(null)
                        ->setDone(0)
                        ->setDoneuserid(self::MAGENTO_USER)
                        ->setDonedate($_datetime)
                        ->setConfdeliverydate($oNullDate)
                        ->setLinefooter('')
                        ->setAltcolorname(null)
                        ->setQuantity($aArticle['Quantity'])
                        ->setCollectionmemberid($aArticle['CollectionMemberId'])
                        ->setSeasonid($this->getSeasonIdByCollectionMemberId((int)$aArticle['CollectionMemberId']))
            ;
            if (isset($aArticle['Description'])) { 				
                $oOrderline->setDescription($aArticle['Description']);
            } else { // Use POT description if not set by Webshop
	            $description = $this->getVariantDescription($aMappedArticle['article_id'], $langId);
                $oOrderline->setDescription($description);
			}
            if (isset($aArticle['Discount'])) { // percentage values
                $oOrderline->setDiscount($aArticle['Discount']);
            }

            $this->em->persist($oOrderline);
            $this->em->flush();

            // Order quantities
            $oOrderQuantity = new Orderquantity();
            $oOrderQuantity->setOrderlineid($oOrderline->getId());
            $oOrderQuantity->setCreatedate($_datetime);
            $oOrderQuantity->setModifydate($_datetime);
            $oOrderQuantity->setModifyuserid(self::MAGENTO_USER);
            $oOrderQuantity->setCreateuserid(self::MAGENTO_USER);
            $oOrderQuantity->setActive(true);
            $oOrderQuantity->setArticlesizeid($aMappedArticle['size_id']);
            $oOrderQuantity->setQuantity($aArticle['Quantity']);
            $oOrderQuantity->setDimension(0);

            $this->em->persist($oOrderQuantity);
            $this->em->flush();
            
			error_log(__LINE__ . ' ' . var_export($aArticle, 1));

            error_log('aMappedArticle ' . var_export($aMappedArticle, 1));

            $this->lineIndex++;

			if ($aMappedArticle['article_id']==self::ShippingArticleId or $aMappedArticle['article_id']==self::RabatArticleId) continue ; // should be check on articletype == None inventory.
			
            // PickOrderLine
            $pOrderline = new Pickorderline();
            $variantCodeId = $this->getVariantIdByVariantCode($aArticle['VariantCode']);
            error_log(__LINE__ . ' ' . var_export($variantCodeId, 1));
            $pOrderline->setCreatedate($_datetime)
                    ->setCreateuserid(self::MAGENTO_USER)
                    ->setModifydate($_datetime)
                    ->setModifyuserid(self::MAGENTO_USER)
                    ->setActive(true)
                    ->setVariantcodeid($variantCodeId[0]['id']) // VariantCodeId
                    ->setVariantcode($aArticle['VariantCode'])
                    ->setOrderedquantity($aArticle['Quantity'])
                    ->setPickedquantity(0)
                    ->setPackedquantity(0)
                    ->setPickorderid($pOrder->getId())
                    ->setVariantdescription($description)
                    //->setVariantcolor($this->getVariantColor($variantCodeId['articlecolorid']))
                    ->setVariantcolor($this->getVariantColor( $aMappedArticle['color_id'] ))
                    //->setVariantsize($this->getVariantSize($aMappedArticle['article_id']))
                    ->setVariantsize($this->getVariantSize($variantCodeId[0]['articlesizeid']))
                    ->setDone(0)
                    ->setQlid($oOrderQuantity->getId())
                ;

			$this->em->persist($pOrderline);
			$this->em->flush();

        }

    }

    /**
     * Get POT order data by reference
     * @param $iReferenceId
     */
    public function getOrderByMagentoId($iMagentoOrderId)
    {
        return  $this->em->getRepository('AppBundle\Entity\Order')->createQueryBuilder('o')
                                                                    ->select('o')
                                                                    ->where('o.reference = :reference')
                                                                    ->setParameter('reference', $iMagentoOrderId)
                                                                    ->getQuery()
                                                                    ->getResult(Query::HYDRATE_ARRAY);
    }


    /**
     * Get POT variantcode id by varinatcode
     * @param $variantCode
     */
    public function getVariantIdByVariantCode($variantCode)
    {
        return  $this->em->getRepository('AppBundle\Entity\Variantcode')->createQueryBuilder('v')
                                                                    ->select('v')
                                                                    ->where('v.variantcode = :variantcode')
                                                                    ->setParameter('variantcode', $variantCode)
                                                                    ->getQuery()
                                                                    ->getResult(Query::HYDRATE_ARRAY);
    }

    public function getVariantColor($colorId) {

        $query = 'SELECT color.Description FROM articlecolor
                  LEFT JOIN color ON color.id=articlecolor.ColorId 
                  WHERE articlecolor.id = '.$colorId . ' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();

        $fetch  =$variants->fetch();
        if (!empty($fetch)) {
            return $fetch['Description'];
        } else {
            return '';
        }
    }

    public function getVariantSize($articlesizeid) {

        $query = 'SELECT Name FROM articlesize 
                  WHERE id='.$articlesizeid.' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();
        if (!empty($fetch)) {
            return $fetch['Name'];
        } else {
            return '';
        }
    }


    public function getVariantDescription($articleid, $langId=4) {

        $query = 'SELECT * FROM article_lang 
                  WHERE ArticleId='.intval($articleid).' AND LanguageId = '.$langId.' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();
        if (!empty($fetch)) {
            return $fetch['Description'];
        } else {
            return '';
        }
    }

    public function getLanguageId($culture='da') {

        $lang = $this->em->getRepository('AppBundle\Entity\Language')->findOneBy(array('culture' => $culture));
        if(isset($lang) && $lang->getId()) {
            return  $lang->getId();
        }else{
            return 4; // danish
        }
    }

    public function getSeason($id) {

        $query = 'SELECT  Name FROM season 
                  WHERE id = '.$id.' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch['Name'];
        }

    }

    public function getSeasonIdByCollectionMemberId($collectionmemberid) {

        $query = 'select c.seasonid as SeasonId 
					From (collection c, collectionmember cm)
					where cm.id='.$collectionmemberid.' and c.id=cm.collectionid LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch['SeasonId'];
        }

    }

    public function getNumber($id) {

        $query = 'SELECT Number FROM article
                  WHERE id = '.$id.' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch['Number'];
        }
    }
	
    public function getVAT($id) {
        /*$query = 'SELECT countryVATPercentage FROM country
                  WHERE id = '.$id.' LIMIT 1';*/
        $query = 'SELECT VATPercentage FROM country
                  WHERE id = '.$id.' LIMIT 1';
        $variants = $this->em->getConnection()->prepare($query);
        $variants->execute();
        $fetch  =$variants->fetch();

//           die(' -------------------- order OK ' . $id. ' ' . $query . ' ' . $fetch['VATPercentage']);
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch['VATPercentage'];
        }
    }
	


}
