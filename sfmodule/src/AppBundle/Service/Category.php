<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Category as CategoryModel;

/**
 * Class Language
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Service
 */
class Category extends Service
{
    /**
     * @var CategoryModel $category
     */
    private $category;

    /**
     * Language constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->category = new CategoryModel();
    }

    /**
     * Get all categories info from POT db
     *
     * @param $ids
     *
     * @return array
     */
    public function getCategoriesByIds($ids)
    {
        $statement = $this->em->getConnection()->prepare($this->category->getCategoryByIdQuery($ids, $this->options['language_ids']));
        $statement->execute();

        $cat = array();
        $categories = $statement->fetchAll();

        foreach ($categories as $key => $category) {
            $name = $category['Name'] != null ? $category['Name'] : $category['oldName'];
            $lang = $category['Lang'] != null ? $category['Lang'] : 'default';

            if (isset($cat[$category['ID']])) {
                $cat[$category['ID']][$lang . '_label'] = $name;
                $cat[$category['ID']]['description_' .$lang] = $category['Description'];
            } else {
                $c = array(
                    'id' => $category['ID'],
                    $lang . '_label' => $name,
                    'description_' . $lang => $category['Description'],
                );

                if (is_int($category['ID_Magento'])) {
                    $c['magento_id'] = $category['ID_Magento'];
                }

                $cat[$category['ID']] = $c;
            }

            if (false === isset($cat[$category['ID']]['default_label'])) {
                $cat[$category['ID']]['default_label'] = $category['oldName']; //put a fallback name from articlecategorygroup name not from _lang
            }

            if (false === isset($cat[$category['ID']]['description_default'])) {
                $cat[$category['ID']]['description_default'] = ''; //we don't have fallback description in DB
            }
        }

        return $cat;
    }

    /**
     * Create query mapped category POT <--> Magento
     *
     * @param int $idPot     Id category from POT
     * @param int $idMagento Id category to Magento
     *
     * @return string
     */
    public function addCategoryMap($idPot, $idMagento)
    {
        return $this->category->mapCategory($idPot, $idMagento);
    }

    /**
     * Save mapped query in DB
     *
     * @param string $query Query of adding all mapped categories
     */
    public function saveCategoryMapDB($query)
    {
        $statement1 = $this->em->getConnection()->prepare($this->category->clearCategoryMap());
        $statement1->execute();

        $statement2 = $this->em->getConnection()->prepare($query);
        $statement2->execute();
    }

    /**
     * Get category mapper data
     *
     * @return array
     */
    public function getMap()
    {
        $query = $this->category->getMaps();
        $st = $this->em->getConnection()->prepare($query);
        $st->execute();

        return $st->fetchAll(\PDO::FETCH_GROUP|\PDO::FETCH_UNIQUE);
    }
}
