<?php

namespace AppBundle\Service;

use AppBundle\Entity\Order;
use AppBundle\Entity\Orderline;
use AppBundle\Entity\Orderquantity;
use Doctrine\ORM\EntityManager;
use AppBundle\Entity\Company;
use Doctrine\ORM\Query;
use Symfony\Component\DependencyInjection\Container;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class OrderService
 * @author Mariusz Angielski <mariusz.angielski@jcommerce.pl>
 * @package AppBundle\Service
 */
class ListService extends Service
{

    /**
     * Get list
     * @param $sType
     * @return array
     */
    public function getList($sType)
    {
        $aAllowed = ['country', 'currency', 'carrier', 'language'];
        if(!in_array($sType, $aAllowed)) {
            return [];
        }

        $sType = ucfirst($sType);
        $aList = $this->em->getRepository('AppBundle\Entity\\' . $sType)->findAll(Query::HYDRATE_ARRAY);

        return $aList;
    }
}
