<?php

namespace AppBundle\Service;

use AppBundle\Entity\Ai;
use AppBundle\Entity\VariantAi;
use AppBundle\Entity\Variantcode;
use AppBundle\Entity\Transaction;
use AppBundle\Entity\Transactionitem;
use AppBundle\Entity\Stocklayout;
use AppBundle\Entity\Article;
use AppBundle\Entity\Stock;
use AppBundle\Entity\Pickorderline;
use AppBundle\Entity\Item;
use AppBundle\Entity\Containers;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query;
use AppBundle\Exception\ApiException;

use Symfony\Component\DependencyInjection\Container;
use AppBundle\Model\Import as ShippingModel;

/**
 * Class Shipping
 *
 * @author Alex roland <aro@turndigital.dk>
 * @package AppBundle\Service
 */
class Shipping extends Service
{
    const PickStockId       = 14321;
    const API_USER          = 214;
    /**
     * @var ShippingModel $shippingModel
     */
    private $shippingModel;

    /**
     * Product constructor.
     *
     * @param EntityManager $em
     * @param Container $container
     */
    public function __construct(EntityManager $em, Container $container)
    {
        parent::__construct($em, $container);
        $this->shippingModel = new ShippingModel();
    }


    public function testAlex($data){
        return $data;
    }

    public function convertSingleCode($data){
        $sscc = array();
        $donestr = $data;
        // while ($donestr !== '') {                    
            $firstAiStr = substr($donestr, 0, 2);
            $ai = $this->em->getRepository('AppBundle\Entity\Ai')->findOneBy([
               'name' => $firstAiStr,
            ]);
            $value = substr($donestr, 2, ($ai->getLength()));
            $sscc[$firstAiStr] = $value;
            
            return $sscc['00'];
            
            $donestr = str_replace(substr($donestr, 0, (2 + $ai->getLength())), '', $donestr);
        // }
        return $sscc['00'];
    }

    public function occupieStockPosition($_newposition, $_oldposition = '', $_stockId = 14321, $_oldstockid = 14321){
        if($_newposition !== ''){
            $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
               'position' => $_newposition,
               'stockid' => $_stockId
            ]);

            if(getType($stocklayout) == 'object'){
                $stocklayout->setOccupied(1);
                $this->em->persist($stocklayout);
                $this->em->flush();
            }
            else{
                return 'Stock position not found';
            }
        }

        if($_oldposition !== ''){
            $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
               'position' => $_oldposition,
               'stockid' => $_oldstockid
            ]);

            if(getType($stocklayout) == 'object'){
                $stocklayout->setOccupied(0);
                $this->em->persist($stocklayout);
                $this->em->flush();
            }            
        }
    }

    public function updateItemQuantityOnPosition($_position, $_variantcode, $oldQuantity, $updatedQuantity) {


        $stockNo = substr($_position, 0, 2);
        $position = substr($_position, 2);

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'Stock not found.';
        }

        $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
            'variantcode'    => $_variantcode,
            'active'         => 1
         ]);

        if(getType($variantcode) !== 'object'){
            return 'GTIN not found in system.';
        }


        $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
            'position'      => $position,
            'stockid'       => $stock->getId(),
            'active'        => 1
         ]);

        if(getType($container) !== 'object'){
            return 'Position not found in system.';
        }

        $query = sprintf ("
            SELECT SUM(i.quantity) as qty
            FROM item i
            WHERE i.ContainerId=%d AND i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1", 
            $container->getId(),
            $variantcode->getArticleid(),
            $variantcode->getArticlecolorid(),
            $variantcode->getArticlesizeid()
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if($fetch === false){
            return "Not found.";
        }

        $qty = (int)$fetch['qty'];

        if($qty == 0){
            return "Variantcode not found in this position";
        }

        $items = $this->em->getRepository("AppBundle\Entity\Item")->findBy(
            array(
                "containerid"       => $container->getId(),
                "articleid"         => $variantcode->getArticleid(),
                "articlecolorid"    => $variantcode->getArticlecolorid(),
                "articlesizeid"     => $variantcode->getArticlesizeid(),
                "active"            => 1
            ),
            array("createdate" => 'ASC')
        );

        
        if($updatedQuantity == $oldQuantity){
            return ;
        }

        
        $transaction = new Transaction();
        $transaction->setCreateuserid(1);
        $transaction->setModifyuserid(1);
        $transaction->setFromstockid($stock->getId());
        $transaction->setTostockid(0);
        $transaction->setObjectid(0);
        $transaction->setCreatedate(new \DateTime($datatime = 'now'));
        $transaction->setModifydate(new \DateTime($datatime = 'now'));
        $transaction->setComment('Stock adjustment at warehouse');
        $transaction->setActive(1);
        $this->em->persist($transaction);
        $this->em->flush();

        $value = 0;
            
        if($updatedQuantity > $oldQuantity){

            $qty = (int)($updatedQuantity - $oldQuantity);

            $transaction->setType('InvPlus');
            $transaction->setQuantity($qty); 

            $count = 0;
            foreach($items as $key => $item){
                if($count > 0){
                    continue;
                }

                $value = $value + ($item->getPrice() * $qty);

                $newItem = clone $item;
                $newItem->setCreatedate(new \DateTime());
                $newItem->setCreateuserid(1);
                $newItem->setModifydate(new \DateTime());
                $newItem->setModifyuserid(1);
                $newItem->setParentid($item->getId());
                $newItem->setBatchnumber('Surplus');
                $newItem->setQuantity($qty);
                $this->em->persist($newItem);
                $this->em->flush();


                $transactionitem = new Transactionitem();
                $transactionitem->setTransactionid($transaction->getId());
                $transactionitem->setFromstockid($stock->getId());
                $transactionitem->setTostockid(0);
                $transactionitem->setCreatedate(new \DateTime($datatime = 'now'));
                $transactionitem->setModifydate(new \DateTime($datatime = 'now'));
                $transactionitem->setCreateuserid(1);
                $transactionitem->setModifyuserid(1);
                $transactionitem->setQuantity($qty);
                $transactionitem->setArticlesizeid($newItem->getArticlesizeid());
                $transactionitem->setValue($value);
                $transactionitem->setObjectlineid(0);
                $transactionitem->setObjectquantityid(0);
                $transactionitem->setActive(1);
                $transactionitem->setAuto(0);
                $transactionitem->setVariantcodeid($this->getVariantCodeIdByArt($newItem->getArticleid(),$newItem->getArticlecolorid(),$newItem->getArticlesizeid()));
                $this->em->persist($transactionitem);
                $this->em->flush();

                $query = sprintf("UPDATE item SET transactionitemid = %d WHERE id = %d", $transactionitem->getId(), $newItem->getId());
                $this->em->getConnection()->prepare($query)->execute();

                $transaction->setValue($value);
              
                $count ++;
            }

        }else if($updatedQuantity < $oldQuantity){

            $qty = (int)($oldQuantity - $updatedQuantity);

            $transaction->setQuantity($qty);
            $transaction->setType('InvLoss');

            
            foreach($items as $key => $item){
                if((int)$qty === 0){
                    continue;
                }

                if((int)$item->getQuantity() > (int)$qty){
                    // transactionitem
                    $item->setQuantity($item->getQuantity() - $qty);
                    $item->setModifydate(new \DateTime());
                    $item->setModifyuserid(1);
                    $this->em->persist($item);
                    $this->em->flush();

                    $value =  $value + ($item->getPrice() * $qty);

                    $newItem = clone $item;
                    $newItem->setQuantity($qty);
                    $newItem->setCreatedate(new \DateTime());
                    $newItem->setCreateuserid(1);
                    $newItem->setModifydate(new \DateTime());
                    $newItem->setModifyuserid(1);
                    $newItem->setParentid($item->getId());
                    $newItem->setContainerid(0);
                    $newItem->setPreviouscontainerid($item->getContainerid());
                    $newItem->setBatchnumber('InvConsume');
                    $newItem->setQuantity($qty);
                    $this->em->persist($newItem);
                    $this->em->flush();


                    $transactionitem = new Transactionitem();
                    $transactionitem->setTransactionid($transaction->getId());
                    $transactionitem->setFromstockid($stock->getId());
                    $transactionitem->setTostockid(0);
                    $transactionitem->setCreatedate(new \DateTime($datatime = 'now'));
                    $transactionitem->setModifydate(new \DateTime($datatime = 'now'));
                    $transactionitem->setCreateuserid(1);
                    $transactionitem->setModifyuserid(1);
                    $transactionitem->setQuantity($qty);
                    $transactionitem->setArticlesizeid($newItem->getArticlesizeid());
                    $transactionitem->setValue($qty * $item->getPrice());
                    $transactionitem->setObjectlineid(0);
                    $transactionitem->setObjectquantityid(0);
                    $transactionitem->setActive(1);
                    $transactionitem->setAuto(0);
                    $transactionitem->setVariantcodeid($this->getVariantCodeIdByArt($newItem->getArticleid(),$newItem->getArticlecolorid(),$newItem->getArticlesizeid()));
                    $this->em->persist($transactionitem);
                    $this->em->flush();


                    $query = sprintf("UPDATE item SET transactionitemid = %d WHERE id = %d", $transactionitem->getId(), $newItem->getId());
                    $this->em->getConnection()->prepare($query)->execute();


                    $qty = 0;
                }
                elseif((int)$item->getQuantity() < (int)$qty){
  
                  
                    $item->setPreviouscontainerid($item->getContainerid());
                    $item->setContainerId(0);
                    $item->setBatchnumber('InvConsume');
                    $item->setModifydate(new \DateTime());
                    $item->setModifyuserid(1);
                    // TODO should add transactionitem id
                    $this->em->persist($item);
                    $this->em->flush();

                    $value =  $value + ($item->getPrice() * $qty);

                    $transactionitem = new Transactionitem();
                    $transactionitem->setTransactionid($transaction->getId());
                    $transactionitem->setFromstockid($stock->getId());
                    $transactionitem->setTostockid(0);
                    $transactionitem->setCreatedate(new \DateTime($datatime = 'now'));
                    $transactionitem->setModifydate(new \DateTime($datatime = 'now'));
                    $transactionitem->setCreateuserid(1);
                    $transactionitem->setModifyuserid(1);
                    $transactionitem->setQuantity((int)$item->getQuantity());
                    $transactionitem->setArticlesizeid($item->getArticlesizeid());
                    $transactionitem->setValue((int)$item->getQuantity() * $item->getPrice());
                    $transactionitem->setObjectlineid(0);
                    $transactionitem->setObjectquantityid(0);
                    $transactionitem->setActive(1);
                    $transactionitem->setAuto(0);
                    $transactionitem->setVariantcodeid($this->getVariantCodeIdByArt($item->getArticleid(),$item->getArticlecolorid(),$item->getArticlesizeid()));
                    $this->em->persist($transactionitem);
                    $this->em->flush();


                    $qty = $qty - (int)$item->getQuantity();
                }
                else{

                    $item->setPreviouscontainerid($item->getContainerid());
                    $item->setContainerId(0);
                    $item->setBatchnumber('InvConsume');
                    $item->setModifydate(new \DateTime());
                    $item->setModifyuserid(1);
                    $this->em->persist($item);
                    $this->em->flush();
                

                    $value =  $value + ($item->getPrice() * $qty);

                    $transactionitem = new Transactionitem();
                    $transactionitem->setTransactionid($transaction->getId());
                    $transactionitem->setFromstockid($stock->getId());
                    $transactionitem->setTostockid(0);
                    $transactionitem->setCreatedate(new \DateTime($datatime = 'now'));
                    $transactionitem->setModifydate(new \DateTime($datatime = 'now'));
                    $transactionitem->setCreateuserid(1);
                    $transactionitem->setModifyuserid(1);
                    $transactionitem->setQuantity((int)$item->getQuantity());
                    $transactionitem->setArticlesizeid($item->getArticlesizeid());
                    $transactionitem->setValue($item->getPrice() * $qty);
                    $transactionitem->setObjectlineid(0);
                    $transactionitem->setObjectquantityid(0);
                    $transactionitem->setActive(1);
                    $transactionitem->setAuto(0);
                    $transactionitem->setVariantcodeid($this->getVariantCodeIdByArt($item->getArticleid(),$item->getArticlecolorid(),$item->getArticlesizeid()));
                    $this->em->persist($transactionitem);
                    $this->em->flush();

                    $qty = 0;
                }
            }
        }
        $transaction->setValue($value);
        $this->em->persist($transaction);
        $this->em->flush();

        return true;
    }

    public function getVariantCodeIdByArt($articleId, $articleColorId, $articleSizeId){
        $query = sprintf('SELECT * FROM variantcode WHERE ArticleId=%d AND ArticleColorId=%d AND ArticleSizeId=%d AND Active=1
            LIMIT 1', $articleId, $articleColorId, $articleSizeId);

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();

        if(isset($fetch['Id']) === true){
            return (int)$fetch['Id'];    
        }
        return 0;
    }

    public function getSsccAndPositionByPosition($_position){
        $stockNo = substr($_position, 0, 2);
        $pos = strpos($_position, $stockNo);
        $position = substr_replace($_position, $replace, $pos, strlen($stockNo));

        if(getType($stock) !== 'object'){
            return 'This stock: '.$stockNo.' does not exist';
        }
        
        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        $query = sprintf('SELECT c.Id, i.ArticleId
            FROM (`container` c, `item` i)
            WHERE c.Position="%s" AND c.StockId=%d AND i.ContainerId=c.Id AND i.Active=1
            LIMIT 1', $position, $stock->getId());

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return 0;
        } else {
            
        } 

        $query = sprintf("SELECT i.ArticleId, c.*
            FROM (`item` i, `container` c, `stocklayout` s)
            WHERE s.Type='sscc' AND s.Position=c.Position AND c.Active=1 AND i.Active=1 AND s.Active=1 AND i.ContainerId=c.Id AND s.Stockid=%d AND i.ArticleId=%s
            ORDER BY c.CreateDate DESC
            LIMIT 1", $stock->getId(), $fetch['ArticleId']);

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return 0;
        } else {
            return array('position' => $fetch['Position'], 'sscc' => '00'.$fetch['Sscc']);  
        } 

    }



    public function getVariantsByPosition($_position){
        $stockNo = substr($_position, 0, 2);
        $pos = strpos($_position, $stockNo);
        $position = substr_replace($_position, $replace, $pos, strlen($stockNo));

        // return $position;
        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        $query = sprintf ('
            SELECT DISTINCT v.VariantCode, v.*, c.StockId
            FROM (container c, variantcode v, item i)
            WHERE 
            c.StockId=%d AND c.Position="%s" AND c.Active=1 AND
            i.ContainerId=c.Id AND i.Active=1 AND
            v.Id=i.VariantCodeId AND v.Active=1', 
            $stock->getId(), 
            $position
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            return array();
        } else {
            return $fetch;
        } 
    }

    public function getVariantcodesBySscc($_sscc, $_stockid){
        $query = sprintf ("
            SELECT v.* 
            FROM (variantcode v, item i, container c) 
            WHERE 
            v.Active=1 AND
            i.Active=1 AND i.VariantCodeId=v.Id AND i.ContainerId=c.Id AND
            c.Active=1 AND c.StockId=%d AND c.Sscc='%s'", 
            $_stockid,
            $_sscc
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            return array();
        }
        else{
            return $fetch;
        }
    }

    public function checkIfVariantsMatch($_haystack1, $_haystack2){
        $check = false;
        foreach ($_haystack1 as $key => $needle1) {
            foreach ($_haystack2 as $key2 => $needle2) {
                if((string)$needle1['VariantCode'] === (string)$needle2['VariantCode']){
                    $check = true;
                }
            }
        }
        return $check;
    }

    public function getContainerByPosition($_position,$_stockid){
        $query = sprintf ("
            SELECT Id
            FROM container 
            WHERE 
            container.StockId=%d AND container.Position='%s'", 
            $_stockid,
            $_position
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return array();
        }
        else{
            return $fetch['Id'];
        }
    }

    public function replenishVariantBySscc($_position, $_sscc){
        $stockNo = substr($_position, 0, 2);
        $pos = strpos($_position, $stockNo);
        $position = substr_replace($_position, $replace, $pos, strlen($stockNo));
        $presscc = substr($_sscc, 2, 0);
        $posshort = strpos($_sscc, $presscc);
        $sscc = substr($_sscc, 2, strlen($_sscc));



        $dbcontainer = $this->getContainerToStore($sscc);

        if($dbcontainer === false){
            return 'No container with this SSCC exists. '.$sscc;
        }
        $oldcontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find($dbcontainer['Id']);

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        $replenishContainerId = $this->getContainerByPosition($position, $stock->getId());

        $variantsOnPosition = $this->getVariantReplenishPositions($_position);
        $ssccVariations = $this->getVariantcodesBySscc($sscc, $stock->getId());

        $variantCheck = $this->checkIfVariantsMatch($variantsOnPosition, $ssccVariations);

        if($variantCheck === false){
            return 'The variant from the sscc is not available at this position. ('.$ssccVariations[0]['VariantCode'].')';
        }

        $items = $this->getItemsByContainer($oldcontainer->getId());

        foreach ($items as $key => $item) {
            $dbItem = $this->em->getRepository('AppBundle\Entity\Item')->find($item['Id']);
            $dbItem->setContainerid($replenishContainerId);
            $this->em->persist($dbItem);
            $this->em->flush();
        }

        $replenishStock = $this->createReplenishStock($ssccVariations[0]['VariantCode'], $position);


        $this->occupieStockPosition($oldcontainer->getPosition(), '', $stock->getId(), $oldcontainer->getStockid());

        $oldcontainer->setStockid($replenishStock);
        $oldcontainer->setPosition('');
        $this->em->persist($oldcontainer);
        $this->em->flush();



        return true;
    }

    public function getItemsByContainer($_containerid){
        $query = sprintf ("SELECT item.Id, v.VariantCode FROM item LEFT JOIN variantcode v on v.Active=1 ANd item.VariantCodeId=v.Id WHERE ContainerId=%d", $_containerid) ;

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            return array();
        }
        else{
            return $fetch;
        }
    }

    public function generateSscc($id){
        $totalprefix = $this->parameterGet('ssccprefix');
        $containerNumberWZero = sprintf('%09d', $id);
        $checkDigit = $this->GetCheckDigitSscc((string) $totalprefix.$containerNumberWZero);
        return (string) $totalprefix.$containerNumberWZero.$checkDigit;
    }

    public function parameterGet($mark){
        $query = sprintf ("SELECT Value FROM Parameter WHERE Mark='%s'", $mark) ;

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return '';
        }
        else{
            return $fetch['Value'];
        }

    }

    public function GetCheckDigitSscc($barcode){
        //Compute the check digit
        $sum=0;
        $barcodeArray = str_split($barcode);
        $i = 0;
        foreach ($barcodeArray as $key => $value) {
            $i ++;
            # code...
            if($i%2 == 0){
                $sum += ( 1 * $value);
                continue;
            }

            $sum += ( 3 * $value);
            continue;
        }
        $r=$sum%10;
        if($r>0)
            $r=10-$r;
        return $r;
    }

    public function getPositionsByVariant($variant){
         $query = sprintf ('
            SELECT c.* 
            FROM (item i, container c, stocklayout s)
            LEFT JOIN variant_ai va on va.AiId=4 AND va.ItemId=i.Id
            WHERE
            i.VariantCodeId=%d AND i.ContainerId=c.Id AND i.Active=1 AND
            c.Position=s.Position AND c.Active=1 AND
            s.StockId=%d AND s.Active=1 AND s.Type="sscc"
            ORDER BY case when va.Value is null OR va.Value="" then 999999 else va.Value end, va.Value ASC
            LIMIT 1', 
            $variant['Id'], 
            $variant['StockId']
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            $variant['position'] = 'No more on stock';
        }
        else{
            $variant['position'] = $fetch['Position'];
        }

        return $variant;
    }

    public function createStocklayout($stock, $position){

        $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
            'position' => $position,
            'stockid' => $stock
        ]);

        if(getType($stocklayout) !== 'object'){
            $stocklayout = new Stocklayout();
            $stocklayout->setOccupied(0)
                    ->setStockid($stock)
                    ->setPosition($position)
                    ->setMax(1)
                    ->setType('gtin')
            ;
            $this->em->persist($stocklayout);
            $this->em->flush();

            return $stocklayout->getId();
        }

        return $stocklayout->getId();

    }

    public function isPositonEmpty($oldpositioncontainerid, $stockId = 0){

        $query =  
        sprintf('SELECT i.Id FROM (item i, container c) WHERE i.ContainerId=c.Id AND i.Active=1 AND c.Id=%d',
        $oldpositioncontainerid);

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        
        if((int)count($fetch) > 0){

        }
        else{
            $query =  
            sprintf('SELECT s.Id as stocklayoutId  FROM (stocklayout s, container c) WHERE s.Position=c.Position AND s.StockId=c.StockId AND s.Active=1 AND c.Id=%d',
            $oldpositioncontainerid);

            $ids = $this->em->getConnection()->prepare($query);
            $ids->execute();
            $fetch  = $ids->fetch();

            if(isset($fetch['stocklayoutId']) === true){
                $layout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->find($fetch['stocklayoutId']);
                $layout->setOccupied(0);
                $this->em->persist($layout);
                $this->em->flush();
            }
        }


    }

    public function replenishFromPickContainer($_position, $_variant, $_qty, $_userid){
        $newPosition = substr($_position, 2);
        $stockNo = substr($_position, 0, 2);

        $sku = $_variant;
        $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
           'variantcode'    => $sku,
           'active'         => 1
        ]);
        
        if(getType($variantcode) !== 'object'){
            return 'Variantcode not found. Variantcode :'. $sku;
        }

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno'    => $stockNo,
           'active'     => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'Stock not found.';
        }

        $newPositionContainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
           'position'    => $newPosition,
           'stockid'     => $stock->getid(),
           'active'      => 1
        ]);

        if(getType($newPositionContainer) !== 'object') {
            $this->createPickingContainer($stock->getid(), $newPosition);
        }

        $newPositionContainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
           'position'    => $newPosition,
           'stockid'     => $stock->getid(),
           'active'      => 1
        ]);

        if(getType($newPositionContainer) !== 'object') {
            return 'Container with position: '.$_position.' not found in the warehouse 1';
        }

        $variantcode->setReplenishcontainerid($newPositionContainer->getId());
        $variantcode->setModifydate(new \DateTime());
        $variantcode->setModifyuserid($_userid);
        $this->em->persist($variantcode);
        $this->em->flush();  


        return true;
        return;
        $newPosition = substr($_position, 2);
        $stockNo = substr($_position, 0, 2);

        $sku = $_variant;
        $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
           'variantcode'    => $sku,
           'active'         => 1
        ]);
        
        if(getType($variantcode) !== 'object'){
            return 'Variantcode not found. Variantcode :'. $sku;
        }

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno'    => $stockNo,
           'active'     => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'Stock not found.';
        }

        $oldpositioncontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find($variantcode->getPickcontainerid());

        if(getType($oldpositioncontainer) !== 'object') {
            return 'Container not found';
        }

        // $stockLayoutId = $this->createStocklayout($stock->getId(), $newPosition);

        $newPositionContainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
           'position'    => $newPosition,
           'stockid'     => $stock->getid(),
           'active'      => 1
        ]);

        if(getType($newPositionContainer) !== 'object') {
            $this->createPickingContainer($stock->getid(), $newPosition);
            //return 'Container with position: '.$_position.' not found in the warehouse';
        }

        $newPositionContainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
           'position'    => $newPosition,
           'stockid'     => $stock->getid(),
           'active'      => 1
        ]);

        if(getType($newPositionContainer) !== 'object') {
            return 'Container with position: '.$_position.' not found in the warehouse';
        }

        $items = $this->em->getRepository('AppBundle\Entity\Item')->findBy(
            array(
                'active' => 1, 
                'containerid' => $oldpositioncontainer,
                'articleid' => $variantcode->getArticleid(),
                'articlecolorid' => $variantcode->getArticlecolorid(),
                'articlesizeid' => $variantcode->getArticlesizeid() 
        ));

        foreach ($items as $item) {
            if($_qty === 0){
                continue;
            }
            if($item->getQuantity() <= $_qty){
                // $tempQty = $item->getQuantity();
                $item->setPreviouscontainerid($item->getContainerid());
                $item->setContainerid($newPositionContainer->getId());
                $this->em->persist($item);
                $this->em->flush();   
                $_qty -= $item->getQuantity();
                continue;
            }
            elseif($item->getQuantity() > $_qty){
                $item->setQuantity($item->getQuantity() - $_qty);
                $item->setCreatedate(new \DateTime());
                $item->setCreateuserid(1);
                $item->setModifydate(new \DateTime());
                $item->setModifyuserid(1);
                $this->em->persist($item);
                $this->em->flush();

                $newItem = clone $item;
                $newItem->setCreatedate(new \DateTime());
                $newItem->setCreateuserid(1);
                $newItem->setModifydate(new \DateTime());
                $newItem->setModifyuserid(1);
                $newItem->setContainerid($newPositionContainer->getId());
                $newItem->setParentid($item->getId());
                $newItem->setQuantity($_qty);
                $this->em->persist($newItem);
                $this->em->flush();
                $_qty = 0;
                continue;
            }
        }

        return true;
    }

    public function moveGTIN($data)
    {
        // $position = strpos($data['position'], $stockNo);
        // $newPosition = substr_replace($data['position'], $replace, $pos);
        $newPosition = substr($data['newposition'], 2);
        $stockNo = substr($data['newposition'], 0, 2);

        $sku = $data['scanned'];


        $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
           'variantcode'    => $sku,
           'active'         => 1
        ]);
        
        if(getType($variantcode) !== 'object'){
            return 'Variantcode not found. Variantcode :'. $sku;
        }

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno'    => $stockNo,
           'active'     => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'Stock not found.';
        }

        $oldpositioncontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find($variantcode->getPickcontainerid());

        if(getType($oldpositioncontainer) !== 'object') {
            return 'Container not found';
        }

        $stockLayoutId = $this->createStocklayout($stock->getId(), $newPosition);

        $newPositionContainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
           'position'    => $newPosition,
           'stockid'     => $stock->getid(),
           'active'      => 1
        ]);

        if(getType($newPositionContainer) !== 'object') {
            $newPositionContainerId = $this->createPickingContainer($stock->getId(), $newPosition);
            $newPositionContainer = $this->em->getRepository('AppBundle\Entity\Containers')->find($newPositionContainerId);
        }

        $items = $this->em->getRepository('AppBundle\Entity\Item')->findBy(
            array(
                'active' => 1, 
                'containerid' => $oldpositioncontainer,
                'articleid' => $variantcode->getArticleid(),
                'articlecolorid' => $variantcode->getArticlecolorid(),
                'articlesizeid' => $variantcode->getArticlesizeid() 
        ));

        foreach ($items as $item) {
            $item->setPreviouscontainerid($item->getContainerid());
            $item->setContainerid($newPositionContainer->getId());
            $this->em->persist($item);
            $this->em->flush();
        }

        $this->isPositonEmpty($oldpositioncontainer->getId(), $stock->getId());


        $variantcode->setPickcontainerid($newPositionContainer->getId());
        $this->em->persist($variantcode);
        $this->em->flush(); 


        $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->find($stockLayoutId);
        $stocklayout->setOccupied(1);
        $this->em->persist($stocklayout);
        $this->em->flush(); 

        return $newPositionContainer->getId();
    }

    public function getPositionsByVariants($variants){
        $positions = array();
        
        foreach ($variants as $key => $variant) {
            array_push($positions, $this->getPositionsByVariant($variant));
        }

        return $positions;
    }

    public function getVariantReplenishPositions($_position){
        $variants = $this->getVariantsByPosition($_position);

        $positions = $this->getPositionsByVariants($variants);

        return $positions;
    }

    public function getSsccFromStockByItemOrPosition($_type, $_scanned){

        if($_type == 'gtin'){

        }

        if($_type == 'position'){
            $data = $this->getSsccAndPositionByPosition($_scanned);
        }

        return $data;

    }

    public function cotainersAtPosition($stock, $position){
        $query = sprintf ('SELECT count(c.Id) as stacked FROM container c WHERE c.Position="%s" AND c.StockId=%d and active=1', $position, $stock) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return 0;
        } else {
            return $fetch['stacked'];
        } 
    }


    public function updatePosition($data){
        $stockNo = substr($data['position'], 0, 2);
        $pos = strpos($data['position'], $stockNo);
        $position = substr_replace($data['position'], $replace, $pos, strlen($stockNo));

        // return $position;
        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'This stock: '.$stockNo.' does not exist';
        }

        $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
            'position' => $position,
            'stockid' => $stock->getId()
        ]);

        if(getType($stocklayout) !== 'object'){
            $stocklayout = new Stocklayout();
            $stocklayout->setOccupied(0)
                    ->setStockid($stock->getId())
                    ->setPosition($position)
                    ->setMax($data['max'])
                    ->setType($data['type'])
            ;
            $this->em->persist($stocklayout);
            $this->em->flush();

            return true;
        }

        return 'Position: '.$data['position'].' already exists.';
    }

    public function deletePosition($data){
        $stockNo = substr($data['position'], 0, 2);
        $pos = strpos($data['position'], $stockNo);
        $position = substr_replace($data['position'], $replace, $pos, strlen($stockNo));

        // return $position;
        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'This stock: '.$stockNo.' does not exist';
        }

        $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
            'position' => $position,
            'stockid' => $stock->getId()
        ]);

        if(getType($stocklayout) !== 'object'){
            return 'This position: '.$data['position'].' does not exist';  
        }


        $this->em->remove($stocklayout);
        $this->em->flush();
        return true;
    }

    public function getContainerToStore($sscc){
        $query = sprintf ("
            SELECT c.* 
            FROM (container c, stock s)
            WHERE 
                c.Sscc='%s' AND 
                c.Active=1 AND
                s.Type NOT IN('shipment','replenish')  AND 
                c.StockId=s.Id", 
            $sscc
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            $lines = false;
        } else {
            $lines = $fetch;
        } 
        return $lines;
    }

    public function getAllContainerNotOnShipments($sscc){
        $query = sprintf ("
            SELECT c.* 
            FROM (container c, stock s)
            WHERE 
                c.Sscc='%s' AND 
                c.Active=1 AND
                s.Type NOT IN('shipment','replenish') AND 
                s.Active=1 AND
                c.StockId=s.Id", 
            $sscc
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            $count = 0;
            $lines = 0;
        } else {
            $count = count($fetch);
            $lines = $fetch;
        } 
        return array($count,$lines);
    }

    private function getAllVariantPositions($_scanned, $_stockId, $_onlyPick){
        $query = sprintf ("
            SELECT v.Id
            FROM (variantcode v)
            WHERE 
                v.VariantCode='%s' AND v.Active=1
            ", 
            $_scanned
        );
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();

        if($fetch === false){
            return 'SKU not found in system';
        }

        $query = sprintf ("
            SELECT SUM(i.Quantity) as qty, CONCAT(s.StockNo,c.Position) as pick, if(cc.Id IS NULL, '', CONCAT(ss.StockNo,cc.Position)) as replenish
            FROM (container c, item i, variantcode v, stock s)
            LEFT JOIN container cc on cc.Id=v.ReplenishContainerId
            LEFT JOIN stock ss on ss.Id=cc.StockId
            WHERE 
                v.VariantCode='%s' AND v.Active=1 AND
                i.ArticleId=v.ArticleId AND i.Active=1 AND i.ArticleColorId=v.ArticleColorId AND i.ArticleSizeId=v.ArticleSizeId AND
                c.Id=i.ContainerId AND c.Active=1 AND c.Id=v.PickContainerId AND i.Active=1 AND s.Id=c.StockId
                GROUP BY c.Id
            ", 
            $_scanned
        );

        if($_onlyPick === true){
            $query = sprintf ("
                SELECT CONCAT(picks.StockNo,pickc.Position) as pick, if(replenishc.Id IS NULL, '', CONCAT(replenishs.StockNo,replenishc.Position)) as replenish 
                FROM (variantcode v)
                LEFT JOIN container pickc ON pickc.Id=v.PickContainerId AND pickc.Active=1
                LEFT JOIN stock picks ON picks.Id=pickc.StockId AND picks.Active=1
                LEFT JOIN container replenishc ON replenishc.Id=v.ReplenishContainerId AND replenishc.Active=1
                LEFT JOIN stock replenishs ON replenishs.Id=pickc.StockId AND replenishs.Active=1
                WHERE 
                    v.VariantCode='%s' AND v.Active=1
                ", 
                $_scanned
            );

            $ids = $this->em->getConnection()->prepare($query);
            $ids->execute();
            return $fetch  = $ids->fetch();
        }

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        return $fetch  = $ids->fetch();
    }

    public function getStockCount($_scanned, $_stockId, $_withReplenish = false, $_onlyPick = false){
        if($_withReplenish === true){
            return $this->getAllVariantPositions($_scanned, $_stockId, $_onlyPick);
        }
        $query = sprintf ("
            SELECT v.Id
            FROM (variantcode v)
            WHERE 
                v.VariantCode='%s' AND v.Active=1
            ", 
            $_scanned
        );
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();

        if($fetch === false){
            return 'SKU not found in system';
        }

        if((int)$_stockId === 0){
            $query = sprintf ("
                SELECT SUM(i.Quantity) as qty, CONCAT(s.StockNo,c.Position) as Position
                FROM (container c, item i, variantcode v, stock s)
                WHERE 
                    v.VariantCode='%s' AND v.Active=1 AND
                    i.ArticleId=v.ArticleId AND i.Active=1 AND i.ArticleColorId=v.ArticleColorId AND i.ArticleSizeId=v.ArticleSizeId AND
                    c.Id=i.ContainerId AND c.Active=1 AND c.StockId=%d AND i.Active=1 AND i.ContainerId=v.PickContainerId AND s.Id=c.StockId
                ", 
                $_scanned,
                $_stockId
            );
        }
        else{
           $query = sprintf ("
                SELECT SUM(i.Quantity) as qty, CONCAT(s.StockNo,cc.Position) as Position
                FROM (container c, item i, variantcode v)
                LEFT JOIN container cc on cc.Id=v.PickContainerId
                LEFT JOIN stock s on s.Id=cc.StockId
                WHERE 
                    v.VariantCode='%s' AND v.Active=1 AND
                    i.ArticleId=v.ArticleId AND i.Active=1 AND i.ArticleColorId=v.ArticleColorId AND i.ArticleSizeId=v.ArticleSizeId AND
                    c.Id=i.ContainerId AND c.Active=1 AND c.StockId=%d
                ", 
                $_scanned,
                $_stockId
            );
        }


        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();

        if((string)$fetch['Position'] === ''){
            $query = sprintf ("
                SELECT 0 as qty, CONCAT(s.StockNo,c.Position) as Position 
                FROM (container c,variantcode v, stock s)
                WHERE 
                    v.VariantCode='%s' AND v.Active=1 AND
                    c.Id=v.PickContainerId AND s.Id=c.StockId
                ", 
                $_scanned,
                $_stockId
            );
            $po = $this->em->getConnection()->prepare($query);
            $po->execute();
            $pos  = $po->fetch();
            $fetch['Position'] = $pos['Position'];
        }


        return array('qty' =>(int)$fetch['qty'], 'storedat' => (string)$fetch['Position']);
    }


    public function getStockByPosition($_scanned, $_position){


        $stockNo = substr($_position, 0, 2);
        $position = substr($_position, 2);

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'Stock not found.';
        }

        $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
            'variantcode'    => $_scanned,
            'active'         => 1
         ]);

        if(getType($variantcode) !== 'object'){
            return 'GTIN not found in system.';
        }


        $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
            'position'      => $position,
            'stockid'       => $stock->getId(),
            'active'        => 1
         ]);

        if(getType($container) !== 'object'){
            return 'Position not found in system.';
        }


        $query = sprintf ("
            SELECT SUM(i.Quantity) as qty
            FROM item i
            WHERE 
                i.ContainerId=%d AND i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND i.Active=1
            ", 
            $container->getId(),
            $variantcode->getArticleid(),
            $variantcode->getArticlecolorid(),
            $variantcode->getArticlesizeid()
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if($fetch === false) {
            return "Not found.";
        }

        $qty = (int)$fetch['qty'];

        if($qty == 0) {
            return "Variantcode not found in this position";
        }

        return array('qty' =>(int)$fetch['qty']);
    }


    public function storeSku($_scanned, $_position, $_type, $_quantity, $_stockId){

        $sscc = '';
        $sku = $_scanned;   
        // if(substr($_scanned, 0, 2) === '00'){
        //     $sscc = $this->convertSingleCode($_scanned);
        //     $sku = '';
        // }
        // else{
        //     $sscc = '';
        //     $sku = $_scanned;   
        // }
        // if(substr($_scanned, 0, 2) === '02'){
        //     $sscc = '';
        //     $first2 = substr($_scanned, 0, 2);
        //     $skufull = strpos($_scanned, $first2);
        //     $sku = substr_replace($_scanned, $replace, $skufull, strlen($first2));
        // }

        $stockNo = substr($_position, 0, 2);
        $pos = strpos($_position, $stockNo);
        $newPosition = substr_replace($_position, $replace, $pos, strlen($stockNo));

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'Position not found. '.$newPosition;
        }

        $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
            'position' => $newPosition,
            'stockid' => $stock->getId()
        ]);

        if(getType($stocklayout) !== 'object'){
            return 'Position not found.';
        }

        if($_type == 'gtin'){

			if (strlen($sku==14) ) {
				$sku=preg_replace('/^0+/', '', $sku) ;
			} 
			$variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
			   'variantcode'    => $sku,
			   'active'         => 1
			]);
			
			if(getType($variantcode) == 'object'){
				$_variantcodeId = $variantcode->getId();
			}
			else{
				$_item['02'] = $sku ;
				$_articleId = $this->createArticle($_item);
				$_variantcodeId = $this->createVariantcode($_item, $_articleId);
				$variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
				   'variantcode'    => $sku,
				   'active'         => 1
				]);
			}

            $positioncontainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
               'position'    => $newPosition,
               'stockid'     => $stock->getid(),
               'active'      => 1
            ]);

            if(getType($positioncontainer) === 'object'){

                $PickContainerId = 0;
                $Different = 'Pick';
                $checkStockLayoutCapacity = false;
                if((int)$positioncontainer->getId() !== (int)$variantcode->getPickcontainerid()){
                    $checkStockLayoutCapacity = true;
                    if((int)$variantcode->getPickcontainerid() > 0){
                        $PickContainerId = (int)$variantcode->getPickcontainerid();
                        $varcontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find((int)$variantcode->getPickcontainerid());
                        $Different = 'replenish';
                        return 'The variant is supposed to be stored at position: '.$varcontainer->getPosition();
                    }
                }

                if($checkStockLayoutCapacity === true){
                    if($this->checkStockLayoutCapacity($stocklayout->getId()) !== true){
                        return 'Max capacity on this position have been reached, please choose another position.';                        
                    }
                }
                // $this->createSimpleProduct($variantcode->getId(), $variantcode->getArticleid(), $positioncontainer->getId(), $_quantity);
                $moveItems = $this->moveItems($variantcode->getArticlecolorid(), $variantcode->getArticleid(), $variantcode->getArticlesizeid(), $positioncontainer->getId(), $_stockId);
                if($moveItems !== true){
                    return $moveItems;
                }

                if((int)$PickContainerId === 0){
                    $variantcode->setPickcontainerid($positioncontainer->getId());
                    $this->em->persist($variantcode);
                    $this->em->flush();
                }

                $this->occupieStockPosition($newPosition, $positioncontainer->getPosition(), $stock->getId(), $positioncontainer->getStockid());
                
                $stocklayout->setType('gtin');
                $this->em->persist($stocklayout);
                $this->em->flush();

                if((int)$stock->getId() === 14321){
                    $this->createInventoryLine($variantcode->getId());
                }

                return array('return' => true, 'position' => $Different);
            }

            if((int)$variantcode->getPickcontainerid() > 0){
                    $varcontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find((int)$variantcode->getPickcontainerid());
                    return 'The variant is supposed to be stored at position: '.$varcontainer->getPosition();
            }

            $newContainerId = $this->createPickingContainer($stock->getId(), $newPosition);

            $moveItems = $this->moveItems($variantcode->getArticlecolorid(), $variantcode->getArticleid(), $variantcode->getArticlesizeid(), $newContainerId, $_stockId);
            if($moveItems !== true){
                return $moveItems;
            }

            $Different = 'replenish';
            if((int)$PickContainerId === 0){
                $variantcode->setPickcontainerid($newContainerId);
                $this->em->persist($variantcode);
                $this->em->flush();
                $Different = 'Pick';
            }

            $this->occupieStockPosition($newPosition, '', $stock->getId(), 0);

            $stocklayout->setType('gtin');
            $this->em->persist($stocklayout);
            $this->em->flush();

            if((int)$stock->getId() === 14321){
                // create inventory line here. 
                $this->createInventoryLine($variantcode->getId());
            }


            return array('return' => true, 'position' => $Different);
        }

   //      if($_type == 'sscc'){
   //          // move container to position,setContainertypeid(39) == pickcontainer and set stock at position stock
   //          $dbcontainer = $this->getContainerToStore($sscc);
   //          $container = $this->em->getRepository('AppBundle\Entity\Containers')->find((int)$dbcontainer['Id']);
   //          if(getType($container) !== 'object'){
   //              return 'SSCC not found in system.';
   //          }

   //          $items = $this->getItemsByContainer($container->getId());
   //          $variantcheck = array();
   //          foreach ($items as $key => $item) {
   //              if($key == 0){
   //                  array_push($variantcheck, $item['VariantCode']);
   //                  continue;
   //              }
   //              if(in_array($item['VariantCode'], $variantcheck) === false){
   //                  // array_push($variantcheck, $item['VariantCode']);
   //                  return 'The SSCC can only contain a single SKU.';
   //              }
   //          }

			// if (strlen ($items[0]['VariantCode']==14) ) {
			// 	$variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
			// 	   'variantcode'    => preg_replace('/^0+/', '', $items[0]['VariantCode']),
			// 	   'active'         => 1
			// 	]);
			// } else {
			// 	$variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
			// 	   'variantcode'    => $items[0]['VariantCode'],
			// 	   'active'         => 1
			// 	]);
			// }

   //          if(getType($variantcode) !== 'object'){
   //              return 'SKU not found in system 2.';
   //          }  

   //          $positioncontainer = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
   //             'position'    => $newPosition,
   //             'stockid'     => $stock->getid(),
   //             'active'      => 1
   //          ]);

   //          if(getType($positioncontainer) === 'object'){
   //              if((int)$positioncontainer->getId() !== (int)$variantcode->getPickcontainerid()){
   //                  $varcontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find((int)$variantcode->getPickcontainerid());
   //                  return 'The variant is supposed to be stored at position: '.$varcontainer->getPosition();
   //              }

   //              $variantcode->setPickcontainerid($positioncontainer->getId());
   //              $this->em->persist($variantcode);
   //              $this->em->flush();

   //              $this->occupieStockPosition($newPosition, $container->getPosition(), $stock->getId(), $container->getStockid());

   //              foreach ($items as $key => $item) {
   //                  $dbItem = $this->em->getRepository('AppBundle\Entity\Item')->find($item['Id']);
   //                  $dbItem->setContainerid($positioncontainer->getId());
   //                  $this->em->persist($dbItem);
   //                  $this->em->flush();
   //              }

   //              $replenishStockId = $this->createReplenishStock($items[0]['VariantCode'], $newPosition);
   //              $container->setPosition('')
   //              ->setStockId($replenishStockId);
   //              $this->em->persist($container);
   //              $this->em->flush();
                
   //              $stocklayout->setType('gtin');
   //              $this->em->persist($stocklayout);
   //              $this->em->flush();

   //              return true;
   //          }

   //          if((int)$variantcode->getPickcontainerid() > 0){
   //              $varcontainer = $this->em->getRepository('AppBundle\Entity\Containers')->find((int)$variantcode->getPickcontainerid());
   //              return 'The variant is supposed to be stored at position: '.$varcontainer->getPosition();
   //          }

   //          $newContainerId = $this->createPickingContainer($stock->getId(), $newPosition);

   //          $variantcode->setPickcontainerid($newContainerId);
   //          $this->em->persist($variantcode);
   //          $this->em->flush();

   //          $this->occupieStockPosition($newPosition, '', $stock->getId(), 0);

   //          foreach ($items as $key => $item) {
   //              $dbItem = $this->em->getRepository('AppBundle\Entity\Item')->find($item['Id']);
   //              $dbItem->setContainerid($newContainerId);
   //              $this->em->persist($dbItem);
   //              $this->em->flush();
   //          }

   //          $replenishStockId = $this->createReplenishStock($items[0]['VariantCode'], $newPosition);
   //          $container->setPosition('')
   //          ->setStockId($replenishStockId);
   //          $this->em->persist($container);
   //          $this->em->flush();

   //          $stocklayout->setType('gtin');
   //          $this->em->persist($stocklayout);
   //          $this->em->flush();
   //          return true;
   //      }

        return 'No GTIN found.';

    }

    private function checkStockLayoutCapacity($stocklayoutId){
        $query = sprintf (
            "
                SELECT count(v.Id) as count
                FROM (stocklayout sl, container c, variantcode v, collectionmember cm)
                WHERE sl.Id=%d AND c.Position=sl.Position AND c.StockId=sl.StockId AND c.Active=1 AND v.PickContainerId=c.Id AND v.Active=1 AND cm.Active=1 AND cm.ArticleId=v.ArticleId AND cm.ArticleColorId=v.ArticleColorId
            ", 
            $stocklayoutId
        ) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        $variantsOnPosition = 0;
        if(isset($fetch['count']) === true){
            $variantsOnPosition = (int)$fetch['count'];
        }

        $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->find($stocklayoutId);

        if((int)$stocklayout->getMax() >= ($variantsOnPosition + 1)){
            return true;
        }

        return false;
    }


    public function createInventoryLine($variantCodeId){

        $ch = curl_init('https://eyda.passon.dk/eyda/s4/public/inventory/update/counters/variantcodeid/'.(int)$variantCodeId);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        return $result = curl_exec($ch);    
        return $resultObj = json_decode($result, true);

    }

    public function createPickingContainer($_stockId, $_position){
        $container = new Containers();
        $container->setCreatedate(new \DateTime())
                ->setCreateuserid(self::API_USER)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(self::API_USER)
                ->setActive(true)
                ->setStockid($_stockId)
                ->setContainertypeid(39)
                ->setPosition($_position)
                ->setPrevioustransportid(0)
                ->setPreviousstockid(0)
                ->setTaraweight(0)
                ->setGrossweight(0)
                ->setDescription('Pick container at Position: '.$_position)
                ->setVolume(0)
                //->setSscc('')
                //->setInboundstockid(0)
        ;
        $this->em->persist($container);
        $this->em->flush();

        return $container->getId();
    }

    public function storeContainer($ssccFull, $position){
        $sscc = $this->convertSingleCode($ssccFull);
        $stockNo = substr($position, 0, 2);
        $pos = strpos($position, $stockNo);
        $newPosition = substr_replace($position, $replace, $pos, strlen($stockNo));

        $dbcontainer = $this->getContainerToStore($sscc);

        if($dbcontainer === false){
            return 'No container with this sscc exists.';
        }

        $container = $this->em->getRepository('AppBundle\Entity\Containers')->find($dbcontainer['Id']);
        // $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
        //    'id' => $sscc,
        //    'active' => 1
        // ]);

        $stock = $this->em->getRepository('AppBundle\Entity\Stock')->findOneBy([
           'stockno' => $stockNo,
           'active' => 1
        ]);

        if(getType($stock) !== 'object'){
            return 'This stock does not exist';
        }

        if(getType($container) == 'object'){
            $stackedItems = $this->cotainersAtPosition($stock->getId(), $newPosition);

            $stocklayout = $this->em->getRepository('AppBundle\Entity\Stocklayout')->findOneBy([
                'position' => $newPosition,
                'stockid' => $stock->getId()
            ]);

            if(getType($stocklayout) !== 'object'){
                return 'Position not found.';
            }

            $stocklayout->setType('sscc');
            $this->em->persist($stocklayout);
            $this->em->flush();

            if(((int)$stackedItems + 1 ) >  $stocklayout->getMax()){
                return 'No more space at position.';
            }

            $oldPosition = $container->getPosition();
            $oldStockId = $container->getStockid();
            $container->setStockid($stock->getId())
            ->setPosition($newPosition);
            $this->em->persist($container);
            $this->em->flush();

            $this->occupieStockPosition($newPosition, $oldPosition, $stock->getId(), $oldStockId);

            return array($container->getId(), $container->getPosition());
        }
        else{
            return 'No container with this sscc exists.';
        }
        
    }

    public function covertCodes($data){
        $codes = array();
        foreach ($data as $sscckey => $ssccobj) {
            $sscc['sscc'] = $ssccobj->sscc;
            // $sscc['items'] = [];
            $sscc['items'] = array();

            $donestr = $ssccobj->sscc;
            while ($donestr !== '') {                    
                $firstAiStr = substr($donestr, 0, 2);
                $ai = $this->em->getRepository('AppBundle\Entity\Ai')->findOneBy([
                   'name' => $firstAiStr,
                ]);
                $value = substr($donestr, 2, ($ai->getLength()));
                $sscc[$firstAiStr] = $value;

                $donestr = str_replace(substr($donestr, 0, (2 + $ai->getLength())), '', $donestr);
            }
            foreach ($ssccobj->items as $key => $code) {
                // $components = array();
                $donestr = $code;
                $count = 0;
                $set = true;
                while ($donestr !== '') {                    
                    $firstAiStr = substr($donestr, 0, 2);
                    $ai = $this->em->getRepository('AppBundle\Entity\Ai')->findOneBy([
                       'name' => $firstAiStr,
                    ]);

                    if(getType($ai) !== 'object'){
                        $donestr = '';
                        return 'The Ai: '.$firstAiStr.' does not exist.';
                    }

                    $value = substr($donestr, 2, ($ai->getLength()));
                    // $components[$key][$firstAiStr] = $value;
                    $sscc['items'][0][$firstAiStr] = $value;

                    $donestr = str_replace(substr($donestr, 0, (2 + $ai->getLength())), '', $donestr);
                }
                // array_push($sscc['items'], $components);
            }
            if(isset($sscc['items'][0])){
                if(isset($sscc['items'][0]['01']) || $sscc['items'][0]['02']){
                }
                else{
                    $sscc['items'][0]['02'] = '9999999999999';
                }
            }
            else{
                $sscc['items'][0]['02'] = '9999999999999';
            }

            array_push($codes, $sscc);
        }
        return $codes;
    }

    public function updatePickorderToId($_id, $_shipmentId){
        $query = sprintf ('UPDATE `pickorder` SET `ToId`=%d WHERE Id=%d', $_shipmentId, $_id ) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
    }

    public function updatePickorder($_id, $_shipmentId){
        $query = sprintf ('UPDATE `pickorder` SET `ToId`=%d, `Packed`=1 WHERE Id=%d', $_shipmentId, $_id ) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
    }

    public function getContainersData($_sscc){
        $sscc = $this->convertSingleCode($_sscc);


        $query = sprintf ('SELECT c.Id, c.Sscc, c.Position, v.VariantCode, a.Description
            FROM container c
            LEFT JOIN item i on i.ContainerId=c.Id AND i.Active=1
            LEFT JOIN article a on i.ArticleId=a.Id
            LEFT JOIN variantcode v on v.Id=i.VariantcodeId AND v.Active=1
            WHERE c.Sscc="%s" AND c.Active=1 LIMIT 1', 
            $sscc
        ) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return array(
                'Sscc' => $sscc, 
                'Ssccfull' => $_sscc, 
                'Position' => 'No',
                'VariantCode' => 'No',
                'Description' => 'This Sscc is not on stock and cant be shipped.',
                'Cansave'     => false,
            );
        } else {
            $fetch['Sscc'] = (string)$fetch['Sscc'];
            $fetch['Ssccfull'] = $_sscc;
            $fetch['Position'] = ((string)$fetch['Position'] == '' ? 'No' : $fetch['Position']);
            $fetch['VariantCode'] = ((string)$fetch['VariantCode'] == '' ? 'No' : $fetch['VariantCode']);
            $fetch['Description'] = ((string)$fetch['Description'] == '' ? 'No' : $fetch['Description']);
            $fetch['Cansave'] = true;

            return $fetch;
        } 
    }

    public function getPickorderline($id){
        $query = sprintf ('
            SELECT 
            p.Id as pickorderid, 
            if(ISNULL(p.PackedQuantity), 0, p.PackedQuantity) as sqty, 
            p.VariantCode as variant, 
            a.Description as description, 
            p.OrderedQuantity as qty
            FROM pickorderline p 
            LEFT JOIN variantcode v on v.Id=p.VariantcodeId 
            LEFT JOIN article a on a.Id=v.ArticleId 
            WHERE p.PickorderId=%d AND a.SsccLabel=1', 
            $id
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            $lines = false;
        } else {
            $lines = $fetch;
        } 

        $query = sprintf ('
            SELECT 
            p.Id as pickorderid, 
            if(ISNULL(p.PackedQuantity), 0, p.PackedQuantity) as sqty, 
            p.VariantCode as variant, 
            a.Description as description, 
            p.OrderedQuantity as qty,
            if(p.PackedQuantity > 0, 1, 0) as alreadysaved
            FROM pickorderline p 
            LEFT JOIN variantcode v on v.Id=p.VariantcodeId 
            LEFT JOIN article a on a.Id=v.ArticleId 
            WHERE p.PickorderId=%d AND a.SsccLabel=0', 
            $id
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            $notscanable = false;
        } else {
            $notscanable = $fetch;
        }

        $query = sprintf ('SELECT p.ToId, c.Sscc, c.Position, a.Description, v.VariantCode
            FROM (pickorder p, container c, item i, article a, variantcode v)
            WHERE 
            p.Id=%d AND 
            c.StockId=p.ToId AND c.Active=1 AND
            i.ContainerId=c.Id AND i.Active=1 AND
            a.Id=i.ArticleId AND a.Active=1 AND
            v.Id=i.VariantCodeId AND v.Active=1 AND a.SsccLabel=1', $id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            $items = array();
        } else {
            $items = $fetch;
        } 

        if($lines === false){
            //return false;
        }

        return array(
            'lines' => $lines,
            'items' => $items,
            'notscanable' => $notscanable
        );


    }

    public function getContainerData($id){
        $query = sprintf ('SELECT * FROM containertype WHERE Id=%d LIMIT 1', $id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            die('no cpntainertype with this id' . $id);
            return false;
        } else {
            return $fetch;
        } 
    }

    public function checkInbouncContainers($_ssccContainers){
        foreach ($_ssccContainers as $Skey => $ssccParcel) {
            $sscc = $ssccParcel['sscc'];
            
            $containersOnStock = $this->getAllContainerNotOnShipments($ssccParcel['00']);
            if($containersOnStock[0] > 0){
                return array(false, $containersOnStock[1][0]['Sscc']);
            }
        }

        return true;
    }

    public function createInboundContainers($_ssccContainers, $_shipmentId, $_containerTypeId = 33){
        date_default_timezone_set("Europe/Copenhagen");
        foreach ($_ssccContainers as $Skey => $ssccParcel) {
            $sscc = $ssccParcel['sscc'];
            // $containerTypeId = 33; // Pallet
            // $containerTypeId = 22; // Medium parcel
            $cValue = $this->getContainerData($_containerTypeId);

            // foreach ($ssccParcel['items'] as $ckey => $containers) {
            // $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
            //    'sscc' => $ssccParcel['00'],
            //    'active' => 1
            // ]);


            // if(getType($container) == 'object'){
            //     $shipment = $this->em->getRepository('AppBundle\Entity\Stock')->find($container->getStockid());
            //     if(getType($shipment) === 'object'){
            //         if((string)$shipment->getType() !== 'shipment'){
            //             return 'This SSCC does already exist in stock.';
            //         }
            //     }
            // }


            $container = new Containers();
            $container->setCreatedate(new \DateTime())
                    ->setCreateuserid(self::API_USER)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(self::API_USER)
                    ->setActive(true)
                    ->setStockid($_shipmentId)
                    ->setContainertypeid($_containerTypeId)
                    ->setPosition('')
                    ->setPrevioustransportid(0)
                    ->setPreviousstockid(0)
                    ->setTaraweight($cValue['TaraWeight'])
                    ->setGrossweight($cValue['TaraWeight'])
                    ->setDescription('SSCC: '.$sscc)
                    ->setVolume($cValue['Volume'])
                    ->setSscc($ssccParcel['00'])
                    ->setInboundstockid($_shipmentId)
            ;
            $this->em->persist($container);
            $this->em->flush();

            foreach ($ssccParcel['items'] as $key => $item) {
                if(isset($ssccParcel['10'])){
                    $item['10'] = $ssccParcel['10'];
                }
                else{
                //     $batch = '';
                    if(isset($item['10'])){
                    }
                    else{
                        $item['10'] = '';
                    }
                }
                $this->createItem($item, $container->getId(), $sscc, $_shipmentId);
            }
            // }
        }
        return true;

    }

    public function createPrintNoteRecord($id){
        $StateField = 'IF(Stock.Done,"Done",IF(Stock.Departed,"Departed",IF(Stock.Invoiced,"Invoiced",IF(NOT Stock.Ready,"Defined","Ready"))))' ;

        $DelNoteField = 'IF(Stock.Ready=1,Stock.Id,"")' ;

        $queryFields =  'Stock.*, 
                Owner.Name as Owner,
                If(Invoice.id is null,"No","Yes") AS InvoiceMade,
                CONCAT(DeliveryTerm.Description," (",DeliveryTerm.Name,")") AS DeliveryTermName,
                Carrier.Name AS CarrierName,
                CONCAT(Country.Name," - ",Country.Description) AS CountryName, 
                ' . $StateField . ' As State,' . $DelNoteField . ' As DelNote';

        $queryTables = '(Stock)
            left join Invoice ON Invoice.stockid=Stock.Id and Invoice.active=1
            LEFT JOIN Company ON Company.Id=Stock.CompanyId
            LEFT JOIN Company Owner ON Owner.Id=Stock.FromCompanyId
            LEFT JOIN DeliveryTerm ON DeliveryTerm.Id=Stock.DeliveryTermId
            LEFT JOIN Carrier ON Carrier.Id=Stock.CarrierId
            LEFT JOIN Country ON Country.Id=Stock.CountryId' ;


        $query = sprintf ('SELECT %s FROM %s WHERE Stock.Id=%d AND Stock.Active=1', $queryFields, $queryTables, $id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch;
        } 
    }

    public function createVariantcode($_item, $_articleId){
        date_default_timezone_set("Europe/Copenhagen");

        $variant = '';
        if(isset($_item['02'])){
            $variant = $_item['02'];
        }
        if(isset($_item['01'])){
            $variant = $_item['01'];
        }

        if($variant == ''){
            $variant = 58;
        }

        $variantcode = new Variantcode();
        $variantcode->setCreatedate(new \DateTime())
                    ->setCreateuserid(self::API_USER)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(self::API_USER)
                    ->setActive(true)
                    ->setType('case')
                    ->setReference('')
                    ->setArticlecolorid(0)
                    ->setArticlesizeid(0)
                    ->setArticlecertificateid(0)
                    ->setVariantmodelref(0)
                    ->setVariantdescription('Auto generated')
                    ->setVariantcolordesc(0)
                    ->setVariantcolorcode(0)
                    ->setVariantsize(0)
                    ->setVariantunit(0)
                    ->setPickcontainerid(0)
                    ->setArticleid($_articleId)
                    ->setApireferenceid(0)
                    ->setVariantcode(preg_replace('/^0+/', '', $variant))
        ;

        $this->em->persist($variantcode);
        $this->em->flush();

        return $variantcode->getId();
    }

    public function getVariantcodeId($item){
        if(isset($item['02'])){
            $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
               'variantcode' => preg_replace('/^0+/', '', $item['02']),
			   'active'      => 1
            ]);
        }
        if(isset($item['01'])){
            $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->findOneBy([
               'variantcode' => preg_replace('/^0+/', '', $item['01']),
			   'active'      => 1
            ]);
        }

        if(getType($variantcode) == 'object'){
            return $variantcode->getId();
        }
        else{

            $articleId = $this->createArticle($item);
            $variantcodeId = $this->createVariantcode($item, $articleId);
            return $variantcodeId;
        }
        
    }

    public function createArticle($item){
        date_default_timezone_set("Europe/Copenhagen");

         $variant = '';

        if(isset($item['02'])){
            $variant = $item['02'];
        }
        if(isset($item['01'])){
            $variant = $item['01'];
        }

        if($variant == ''){
             return 2;
        }

        $article = new Article();
        $article->setCreatedate(new \DateTime())
                ->setCreateuserid(self::API_USER)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(self::API_USER)
                ->setActive(true)   
                ->setComment('')
                ->setDefaultstockid(14321)
                ->setArticletypeid(1)
                ->setSuppliercompanyid(787)
                ->setSuppliernumber(0)
                ->setCustomspositionid(0)
                ->setUnitid(0)
                ->setMaterialcountryid(0)
                ->setWorkcountryid(0)
                ->setKnitcountryid(0)
                ->setVariantcolor(0)
                ->setVariantdimension(0)
                ->setVariantsize(0)
                ->setVariantsortation(0)
                ->setVariantcertificate(0)
                ->setPricedimensionadjust(0)
                ->setPricestock(0)
                ->setSketchtype(0)
                ->setSketchuserid(0)
                ->setSketchdate(new \DateTime)
                ->setPrintnumber(0)
                ->setWidthusable(0)
                ->setWidthfull(0)
                ->setM2weight(0)
                ->setShrinkagewashlength(0)
                ->setShrinkagewashwidth(0)
                ->setShrinkageworklength(0)
                ->setShrinkageworkwidth(0)
                ->setWashinginstruction(0)
                ->setColorfastnesswater(0)
                ->setColorfastnesswash(0)
                ->setColorfastnesslight(0)
                ->setColorfastnessperspiration(0)
                ->setColorfastnesschlor(0)
                ->setColorfastnesssea(0)
                ->setColorfastnessdurability(0)
                ->setRubbingdry(0)
                ->setRubbingwet(0)
                ->setFabricarticleid(0)
                ->setDeliverycomment(0)
                ->setPublic(1)
                ->setSalesprice(0)
                ->setListhidden(0)
                ->setReceiveddate(new \DateTime)
                ->setOrdereddate(new \DateTime)
                ->setRejecteddate(new \DateTime)
                ->setApproveddate(new \DateTime)
                ->setSalesuserid(0)
                ->setUsepdmstyle(0)
                ->setHasstyle(0)
                ->setRequisitiontext(0)
                ->setCostcurrencyid(1)
                ->setCustomercompanyid(0)
                ->setImportidref(0)
                ->setNumber('No')
                ->setDescription('Auto generated: '.$variant)
        ;   

        $this->em->persist($article);
        $this->em->flush();  

        return $article->getId();
    }

    public function getArticleId($variantcode){
        $query = sprintf ('SELECT a.* FROM variantcode v LEFT JOIN article a ON a.Id=v.ArticleId WHERE v.VariantCode="%s" AND v.Active=1 LIMIT 1', $variantcode) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            die('no variantcode with this number' . $variantcode);
            return false;
        } else {
            return $fetch;
        } 
    }
    public function createEmptyItem($_sscc, $_containerId, $_shipmentId){
        $item = new Item();
        $item->setCreatedate(new \DateTime())
                ->setCreateuserid(self::API_USER)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(self::API_USER)
                ->setActive(true)
                ->setStyleid(0)
                ->setPreviouscontainerid(0)
                ->setPurchaseid(0)
                ->setCaseid(0)
                ->setFromproductionid(0)
                ->setConsumeproductionid(0)
                ->setParentid(0)
                ->setOrderlineid(0)
                ->setTransportid(0)
                ->setComment(0)
                ->setPrice(0)
                ->setArticlecolorid(0)
                ->setArticlesizeid(0)
                ->setArticlecertificateid(0)
                ->setDimension(0)
                ->setSortation(0)
                ->setTestitemid(0)
                ->setTestdone(0)
                ->setTestdoneuserid(self::API_USER)
                ->setTestdonedate(new \DateTime())
                ->setWidthusable(0)
                ->setWidthfull(0)
                ->setM2weight(0)
                ->setShrinkagewashlength(0)
                ->setShrinkagewashwidth(0)
                ->setShrinkageworklength(0)
                ->setShrinkageworkwidth(0)
                ->setWashinginstruction(0)
                ->setColorfastnesswater(0)
                ->setColorfastnesswash(0)
                ->setColorfastnesslight(0)
                ->setColorfastnessperspiration(0)
                ->setColorfastnesschlor(0)
                ->setColorfastnesssea(0)
                ->setColorfastnessdurability(0)
                ->setRubbingdry(0)
                ->setRubbingwet(0)
                ->setReceivalnumber(0)
                ->setItemoperationid(0)
                ->setPreviousid(0)
                ->setRequisitionid(0)
                ->setRequisitionlineid(0)
                ->setProductionid(0)
                ->setReservationid(0)
                ->setInventorypct(0)
                ->setReservationdate(new \DateTime())
                ->setReservationuserid(0)
                ->setPurchasedforid(0)
                ->setAltstockid(0)
                ->setTwisting(0)
                ->setOwnerid(787)
                ->setQuantity(1) // set
                ->setBatchnumber('') // set
                ->setVariantcodeid(0) // set
                ->setArticleid(0) // set
                ->setContainerid($_containerId) // set
        ;
        $this->em->persist($item);
        $this->em->flush();

        $this->createVariantAiValue($item->getId(), 'RDT', date_format($this->em->getRepository('AppBundle\Entity\Stock')->find($_shipmentId)->getCreateDate(), 'Y-m-d H:i:s'));
    }

    public function getAllItemsToMove($articleColorId, $articleId, $articleSizeId, $_stockId){
        $query = sprintf ("
            SELECT i.* 
            FROM (item i, container c) 
            WHERE 
            i.active=1 AND 
            i.ArticleId=%d AND i.ArticleColorId=%d AND i.ArticleSizeId=%d AND c.Id=i.ContainerId AND c.StockId=%d AND c.Active=1", 
            $articleId,
            $articleColorId,
            $articleSizeId,
            $_stockId
        );

        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetchAll();
        if (empty($fetch)) {
            $lines = false;
        } 

        return $fetch;
    }

    public function moveItems($articleColorId, $articleId, $articleSizeId, $containerId, $_fromStockId){
        $items = $this->getAllItemsToMove($articleColorId, $articleId, $articleSizeId, $_fromStockId);
        if($items === false){
            return 'No items to move';
        }

        foreach ($items as $key => $value) {
            $item = $this->em->getRepository('AppBundle\Entity\Item')->find((int)$value['Id']);
            $item->setContainerid($containerId);
            $this->em->persist($item);
            $this->em->flush();
        }

        return true;
    }

    public function createSimpleProduct($_variantcodeId, $_articleId, $_containerId, $_quantity){

        $item = new Item();
        $item->setCreatedate(new \DateTime())
                ->setCreateuserid(self::API_USER)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(self::API_USER)
                ->setActive(true)
                ->setStyleid(0)
                ->setPreviouscontainerid(0)
                ->setPurchaseid(0)
                ->setCaseid(0)
                ->setFromproductionid(0)
                ->setConsumeproductionid(0)
                ->setParentid(0)
                ->setOrderlineid(0)
                ->setTransportid(0)
                ->setComment(0)
                ->setPrice(0)
                ->setArticlecolorid(0)
                ->setArticlesizeid(0)
                ->setArticlecertificateid(0)
                ->setDimension(0)
                ->setSortation(0)
                ->setTestitemid(0)
                ->setTestdone(0)
                ->setTestdoneuserid(self::API_USER)
                ->setTestdonedate(new \DateTime())
                ->setWidthusable(0)
                ->setWidthfull(0)
                ->setM2weight(0)
                ->setShrinkagewashlength(0)
                ->setShrinkagewashwidth(0)
                ->setShrinkageworklength(0)
                ->setShrinkageworkwidth(0)
                ->setWashinginstruction(0)
                ->setColorfastnesswater(0)
                ->setColorfastnesswash(0)
                ->setColorfastnesslight(0)
                ->setColorfastnessperspiration(0)
                ->setColorfastnesschlor(0)
                ->setColorfastnesssea(0)
                ->setColorfastnessdurability(0)
                ->setRubbingdry(0)
                ->setRubbingwet(0)
                ->setReceivalnumber(0)
                ->setItemoperationid(0)
                ->setPreviousid(0)
                ->setRequisitionid(0)
                ->setRequisitionlineid(0)
                ->setProductionid(0)
                ->setReservationid(0)
                ->setInventorypct(0)
                ->setReservationdate(new \DateTime())
                ->setReservationuserid(0)
                ->setPurchasedforid(0)
                ->setAltstockid(0)
                ->setTwisting(0)
                ->setOwnerid(787)
                ->setBatchnumber('')
                ->setQuantity($_quantity) // set
                ->setVariantcodeid($_variantcodeId) // set
                ->setArticleid($_articleId) // set
                ->setContainerid($_containerId) // set
        ;

        $this->em->persist($item);
        $this->em->flush();

        return $item->getId();
    }

    public function createItem($itemData, $_containerId, $_sscc, $_shipmentId){
        // $variantCodeId =  $this->getVariantcodeId($itemData);
        $variantCodeId =  false;
        $article = false;
        if(isset($itemData['02'])){
            $variantCodeId =  $this->getVariantcodeId($itemData);
            $article = $this->getArticleId(preg_replace('/^0+/', '', $itemData['02']));
        }
        if(isset($itemData['01'])){
            $variantCodeId =  $this->getVariantcodeId($itemData);
            $article = $this->getArticleId(preg_replace('/^0+/', '', $itemData['01']));
        }

        if($article === false && $variantCodeId === false){
            $variantCodeId = 58;
            $article = $this->getArticleid('8888888888888');
        }
        // if($variantCodeId === false){
        //     // create variant 9999999999999
        //     $variantCodeId = 6;
        //     $article = $this->getArticleId('9999999999999');   
        // }

        date_default_timezone_set("Europe/Copenhagen");

        if(isset($itemData['37'])){
            $qty = $itemData['37'];
        }
        else{
            $qty = 1;   
        }

        $item = new Item();
        $item->setCreatedate(new \DateTime())
                ->setCreateuserid(self::API_USER)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(self::API_USER)
                ->setActive(true)
                ->setStyleid(0)
                ->setPreviouscontainerid(0)
                ->setPurchaseid(0)
                ->setCaseid(0)
                ->setFromproductionid(0)
                ->setConsumeproductionid(0)
                ->setParentid(0)
                ->setOrderlineid(0)
                ->setTransportid(0)
                ->setComment(0)
                ->setPrice(0)
                ->setArticlecolorid(0)
                ->setArticlesizeid(0)
                ->setArticlecertificateid(0)
                ->setDimension(0)
                ->setSortation(0)
                ->setTestitemid(0)
                ->setTestdone(0)
                ->setTestdoneuserid(self::API_USER)
                ->setTestdonedate(new \DateTime())
                ->setWidthusable(0)
                ->setWidthfull(0)
                ->setM2weight($article['M2Weight'])
                ->setShrinkagewashlength(0)
                ->setShrinkagewashwidth(0)
                ->setShrinkageworklength(0)
                ->setShrinkageworkwidth(0)
                ->setWashinginstruction(0)
                ->setColorfastnesswater(0)
                ->setColorfastnesswash(0)
                ->setColorfastnesslight(0)
                ->setColorfastnessperspiration(0)
                ->setColorfastnesschlor(0)
                ->setColorfastnesssea(0)
                ->setColorfastnessdurability(0)
                ->setRubbingdry(0)
                ->setRubbingwet(0)
                ->setReceivalnumber(0)
                ->setItemoperationid(0)
                ->setPreviousid(0)
                ->setRequisitionid(0)
                ->setRequisitionlineid(0)
                ->setProductionid(0)
                ->setReservationid(0)
                ->setInventorypct(0)
                ->setReservationdate(new \DateTime())
                ->setReservationuserid(0)
                ->setPurchasedforid(0)
                ->setAltstockid(0)
                ->setTwisting(0)
                ->setOwnerid(787)
                ->setQuantity($qty) // set
                ->setBatchnumber($itemData['10']) // set
                ->setVariantcodeid($variantCodeId) // set
                ->setArticleid($article['Id']) // set
                ->setContainerid($_containerId) // set
        ;
        $this->em->persist($item);
        $this->em->flush();


        $this->createVariantAi($itemData, $_containerId, $_sscc, $_shipmentId, $item->getId());

        return $item->getId();
    }

    public function createVariantAiValue($_itemId, $_type, $_value){
        $ai = $this->em->getRepository('AppBundle\Entity\Ai')->findOneBy([
           'name' => $_type,
        ]);
        if(getType($ai) == 'object'){
            $variantAi = new VariantAi();
            $variantAi->setItemid($_itemId)
                ->setAiId($ai->getId())
                ->setValue($_value)
            ;
            $this->em->persist($variantAi);
            $this->em->flush();
        }
    }

    public function createVariantAi($ailist, $_containerId, $_sscc, $_shipmentId, $_itemId){
        foreach ($ailist as $key => $value) {

            $ai = $this->em->getRepository('AppBundle\Entity\Ai')->findOneBy([
               'name' => $key,
            ]);

            if(getType($ai) == 'object'){
                $variantAi = new VariantAi();
                $variantAi->setItemid($_itemId)
                    ->setAiId($ai->getId())
                    ->setValue($value)
                ;
                $this->em->persist($variantAi);
                $this->em->flush();
            }
        }
        
        // if($_batchNumber !== ''){
        //     $this->createVariantAiValue($_itemId, '10', $_batchNumber);
        // }
        // $this->createVariantAiValue($_itemId, '00', $_sscc);

        date_default_timezone_set("Europe/Copenhagen");
        $this->createVariantAiValue($_itemId, 'RDT', date_format($this->em->getRepository('AppBundle\Entity\Stock')->find($_shipmentId)->getCreateDate(), 'Y-m-d H:i:s'));
    }

    public function getNextAvailablePosition($stock = 14321, $position = 'A000A'){
        $query = sprintf ('SELECT sl.*, s.StockNo, c.Id as ContainerId, COUNT(c.Id) as count
            FROM `stocklayout` sl
            LEFT JOIN container c on c.Position=sl.Position AND c.Active=1
            LEFT JOIN stock s on s.Id=%d
            WHERE sl.StockId=%d AND sl.Type="sscc"
            GROUP By sl.Position
            HAVING COUNT(c.Id) < sl.Max
            ORDER BY sl.Position ASC LIMIT 1', $stock, $stock) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return $this->getNextAvailablePosition();
        } else {
            return $fetch['StockNo'].$fetch['Position'];
        } 
    }

    public function getDefaultStockIdBySscc($sscc){
        $query = sprintf ('SELECT 
            a.DefaultStockId
            FROM (container c, item i, article a)
            WHERE 
            c.Sscc = "%s" AND 
            c.Active=1 AND 
            i.ContainerId=c.Id AND 
            i.Active=1 AND 
            a.Id=i.ArticleId AND 
            a.Active=1', $sscc) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return 14321;
        } else {
            return $fetch['DefaultStockId'];
        } 
    }

    public function getNextAvailablePositionByArticle($sscc){
        $ssccCode = $this->convertSingleCode($sscc);
        $next = $this->getNextAvailablePosition($this->getDefaultStockIdBySscc($ssccCode));
        $dbcontainer = $this->getContainerToStore($ssccCode);

        if($dbcontainer === false){
            return 'No container with this sscc exists.';
        }

        $container = $this->em->getRepository('AppBundle\Entity\Containers')->find($dbcontainer['Id']);

        // $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
        //    'sscc' => $ssccCode,
        //    'active' => 1,
        // ]);
        
        if(getType($container) == 'object'){
            $shipment = $this->em->getRepository('AppBundle\Entity\Stock')->find($container->getStockid());
            $stockNo = '';
            if(getType($container) == 'object'){
                $stockNo = $shipment->getStockno();
            }
            $old = $stockNo.$container->getPosition();
        }
        else{
            $old = '';
        }
        return array(
            'next' => $next,
            'old' => $old,
        );
        // return $this->getNextAvailablePosition($this->getDefaultStockIdBySscc($ssccCode));
    }

    public function doneShipment($id){
        $shipment = $this->em->getRepository('AppBundle\Entity\Stock')->find($id);

        $shipment->setDeparted(1)
                ->setDeparteduserid(self::API_USER)
                ->setDeparteddate(new \DateTime())
                ->setReady(1)
                ->setReadydate(new \DateTime())
                ->setReadyuserid(self::API_USER)
                ->setInvoiced(1)
                ->setInvoiceddate(new \DateTime())
                ->setInvoiceduserid(self::API_USER)
                ->setDone(1)
                ->setDonedate(new \DateTime())
                ->setDoneuserid(self::API_USER);

        $this->em->persist($shipment);
        $this->em->flush();

    }

    public function getItemAndContainerWithoutSscc($variantcodeid){
        $query = sprintf ('
            SELECT c.*
            FROM (item i, container c, stock s)
            WHERE i.Active=1 AND i.VariantCodeId=%d AND s.Type="fixed" AND s.Id=c.StockId AND c.Sscc="" AND i.ContainerId=c.Id AND c.Active=1
            ORDER BY c.CreateDate ASC LIMIT 1', 
            $variantcodeid
        ) ;
        /*$query = sprintf ('
            
            SELECT c.*
            FROM (item i, container c)
            WHERE i.Active=1 AND i.ArticleId=%d AND c.Type="fixed" AND c.Sscc="" AND i.ContainerId=c.Id AND c.Active=1
            ORDER BY c.CreateDate ASC LIMIT 1', 
            $articleId
        ) ;*/
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            return false;
        } else {
            return $fetch['Id'];
        } 

    }

    public function createNewOutboundWithoutSscc($items, $stockId, $pickorderId){
        date_default_timezone_set("Europe/Copenhagen");

        foreach ($items as $key => $item) {
            if((int)$item->alreadysaved === 1){
                continue;
            }

            $pickorderline = $this->em->getRepository('AppBundle\Entity\Pickorderline')->find($item->lineid);
            if(getType($pickorderline) == 'object'){
                $pickorderline->setPackedquantity( $pickorderline->getOrderedquantity() );
                $this->em->persist($pickorderline);
                $this->em->flush();
            }

            $variantcodeId =  $pickorderline->getVariantcodeid();
            $variantcode = $this->em->getRepository('AppBundle\Entity\Variantcode')->find($variantcodeId);
            $count = 1;
            while ( $count <= ( $pickorderline->getOrderedquantity() ) ) {
                $containerId = $this->getItemAndContainerWithoutSscc($variantcodeId);
                
                if($containerId === false){
                    $count = 999;
                    continue;
                    return 'error';
                }

                $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
                   'id' => $containerId,
                ]);

                $container->setStockid($stockId);
                $this->em->persist($container);
                $this->em->flush();

                $count ++;
            }
        }

        return $stockId;

    }

    public function createNewOutboundWithSscc($items, $stockId, $pickorderId, $done){
        date_default_timezone_set("Europe/Copenhagen");

        if($stockId > 0){
            $shipmentId = $stockId;
        }
        else{
            $shipmentId = $this->createOutboundShipment($pickorderId, $done);
        }

        if(count($items) > 0){

        }
        else{
            if($done === true){
                $this->doneShipment($shipmentId);
            }
            return $shipmentId;
        }

        if($done === true){
            $this->doneShipment($shipmentId);
        }

        $cValue = $this->getContainerData(33);
        foreach ($items as $key => $item) {
            if($item->alreadysaved === true){
                continue;
            }
            $tempsscc = $item->sscc;
            if(strlen($tempsscc) > 18){
                $sscc = $this->convertSingleCode($item->sscc);
            }
            else{
                $sscc = $item->sscc;
            }
            
            // $container = $this->em->getRepository('AppBundle\Entity\Containers')->findOneBy([
            //    'sscc' => $sscc,
            //    'active' => 1,
            // ]);
            $dbcontainer = $this->getContainerToStore($sscc);

            if($dbcontainer === false){
                return 'No container with this sscc exists.';
            }

            $container = $this->em->getRepository('AppBundle\Entity\Containers')->find($dbcontainer['Id']);


            $pickorderline = $this->em->getRepository('AppBundle\Entity\Pickorderline')->find($item->lineid);

            if(getType($pickorderline) == 'object'){
                $pickorderline->setPackedquantity( ( 1 + $pickorderline->getPackedquantity()) );
                $this->em->persist($pickorderline);
                $this->em->flush();
            }

            if(getType($container) == 'object'){
                $oldPosition = $container->getPosition();
                $newPosition = 'none';
                $oldStockid = $container->getStockid();

                $container->setStockid($shipmentId)
                ->setPosition('');
                $this->em->persist($container);
                $this->em->flush();
                
                if($oldPosition !== 'none'){
                    $this->occupieStockPosition('', $oldPosition, 0, $oldStockid);
                }
                // return array($container->getId(), $container->getPosition());
            }
            else{
                $container = new Containers();
                $container->setCreatedate(new \DateTime())
                        ->setCreateuserid(self::API_USER)
                        ->setModifydate(new \DateTime())
                        ->setModifyuserid(self::API_USER)
                        ->setActive(true)
                        ->setStockid($shipmentId)
                        ->setContainertypeid(33)
                        ->setPosition('')
                        ->setPrevioustransportid(0)
                        ->setPreviousstockid(0)
                        ->setTaraweight($cValue['TaraWeight'])
                        ->setGrossweight($cValue['TaraWeight'])
                        ->setDescription('SSCC: '.$item->sscc)
                        ->setVolume($cValue['Volume'])
                        ->setSscc($sscc)
                        ->setInboundstockid($shipmentId)
                ;
                $this->em->persist($container);
                $this->em->flush();

                $this->createEmptyItem($sscc, $container->getId(), $shipmentId);
                // return 'No container with this sscc exists';
            }
        }
        return $shipmentId;
    }

    public function getAddressDataFromPickorder($id){
        $query = sprintf ('SELECT * FROM pickorder WHERE Id=%d AND Active=1', $id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            // die('no consolidated order with that id ' . $Id);
            $fetch = array(
                'CompanyName'   => '',
                'Address1'      => '',
                'Address2'      => '',
                'ZIP'           => '',
                'City'          => '',
                'CountryId'     => 0,
                'PickupAddress' => '',
            );
            return $fetch;
        } else {
            return $fetch;
        }   
    }

    public function createReplenishStock($_variant, $_position){
        date_default_timezone_set("Europe/Copenhagen");
            $stockFrom = 14321;
            //$addressInfo = $this->getAddressDataFromPickorder($pickorderId);
            // create shipment
            $outboundStock = new Stock();
            $outboundStock->setCreatedate(new \DateTime())
                    ->setCreateuserid(self::API_USER)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(self::API_USER)
                    ->setActive(1)
                    // ->setType("inbound")
                    ->setType("replenish")
                    ->setName('Replenish stock ' . date('Y-m-d H:i:s')) 
                    ->setDescription('Replenish variant: '.$_variant.' at position: '.$_position)
                    ->setDeparturedate(new \DateTime())
                    ->setArrivaldate(new \DateTime())
                    ->setStockdate(new \DateTime())
                    ->setAccountid(0)
                    ->setStockno('00')
                    ->setBatchid(0)
                    ->setPartship(0)
                    ->setRangeassign(0)
                    ->setOnstockonly(0)
                    ->setInventorymode(0)
                    ->setPickstock(0)
                    ->setInventorypct(100)
                    ->setArrived(1)
                    ->setArriveduserid(self::API_USER)
                    ->setArriveddate(new \DateTime())
                    ->setVerified(1)
                    ->setVerifieduserid(self::API_USER)
                    ->setVerifieddate(new \DateTime())
                    ->setDeparted(1)
                    ->setDeparteduserid(self::API_USER)
                    ->setDeparteddate(new \DateTime())
                    ->setReady(1)
                    ->setReadydate(new \DateTime())
                    ->setReadyuserid(self::API_USER)
                    ->setInvoiced(1)
                    ->setInvoiceddate(new \DateTime())
                    ->setInvoiceduserid(self::API_USER)
                    ->setDone(1)
                    ->setDonedate(new \DateTime())
                    ->setDoneuserid(self::API_USER)
                    ->setCompanyid(7560)
                    ->setFromcompanyid(7560)
                    ->setDeliverytermid(0)
                    ->setCarrierid(0)
                    ->setAltcompanyname('')
                    ->setCompanyname('')
                    ->setAddress1('')
                    ->setAddress2('')
                    ->setZip('')
                    ->setCity('')
                    ->setCountryid(0)
                    ->setCarrierurl('')
                    ->setPickupaddress('')
            ;

            $this->em->persist($outboundStock);
            $this->em->flush();

            return $outboundStock->getId();
    }

    public function createOutboundShipment($pickorderId){
            
            date_default_timezone_set("Europe/Copenhagen");
            $stockFrom = 14321;
            $addressInfo = $this->getAddressDataFromPickorder($pickorderId);
            // create shipment
            $outboundStock = new Stock();
            $outboundStock->setCreatedate(new \DateTime())
                    ->setCreateuserid(self::API_USER)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(self::API_USER)
                    ->setActive(1)
                    // ->setType("inbound")
                    ->setType("shipment")
                    ->setName('Outbound stock ' . date('Y-m-d H:i:s')) 
                    ->setDescription($addressInfo['Reference'].' ')
                    ->setDeparturedate(new \DateTime())
                    ->setArrivaldate(new \DateTime())
                    ->setStockdate(new \DateTime())
                    ->setAccountid(0)
                    ->setStockno('00')
                    ->setBatchid(0)
                    ->setPartship(0)
                    ->setRangeassign(0)
                    ->setOnstockonly(0)
                    ->setInventorymode(0)
                    ->setPickstock(0)
                    ->setInventorypct(100)
                    ->setArrived(0)
                    ->setArriveduserid(self::API_USER)
                    ->setArriveddate(new \DateTime())
                    ->setVerified(1)
                    ->setVerifieduserid(self::API_USER)
                    ->setVerifieddate(new \DateTime())
                    ->setDeparted(0)
                    ->setDeparteduserid(self::API_USER)
                    ->setDeparteddate(new \DateTime())
                    ->setReady(0)
                    ->setReadydate(new \DateTime())
                    ->setReadyuserid(self::API_USER)
                    ->setInvoiced(0)
                    ->setInvoiceddate(new \DateTime())
                    ->setInvoiceduserid(self::API_USER)
                    ->setDone(0)
                    ->setDonedate(new \DateTime())
                    ->setDoneuserid(self::API_USER)
                    ->setCompanyid(7560)
                    ->setFromcompanyid(7560)
                    ->setDeliverytermid(0)
                    ->setCarrierid(0)
                    ->setAltcompanyname('')
                    ->setCompanyname($addressInfo['CompanyName'])
                    ->setAddress1($addressInfo['Address1'])
                    ->setAddress2($addressInfo['Address2'])
                    ->setZip($addressInfo['ZIP'])
                    ->setCity($addressInfo['City'])
                    ->setCountryid($addressInfo['CountryId'])
                    ->setCarrierurl('')
                    ->setPickupaddress($addressInfo['PickupAddress'])
            ;

            $this->em->persist($outboundStock);
            $this->em->flush();

            return $outboundStock->getId();
    }


    public function createInboundShipment(){
            
            date_default_timezone_set("Europe/Copenhagen");
            $stockFrom = 14321;
            // create shipment
            $inboundStock = new Stock();
            $inboundStock->setCreatedate(new \DateTime())
                    ->setCreateuserid(self::API_USER)
                    ->setModifydate(new \DateTime())
                    ->setModifyuserid(self::API_USER)
                    ->setActive(1)
                    ->setType("inbound")
                    // ->setType("shipment")
                    ->setName('Inbound stock ' . date('Y-m-d H:i:s')) 
                    ->setDescription('')
                    ->setDeparturedate(new \DateTime())
                    ->setArrivaldate(new \DateTime())
                    ->setStockdate(new \DateTime())
                    ->setAccountid(0)
                    ->setBatchid(0)
                    ->setPartship(0)
                    ->setStockno('00')
                    ->setRangeassign(0)
                    ->setOnstockonly(0)
                    ->setInventorymode(0)
                    ->setPickstock(0)
                    ->setInventorypct(100)
                    ->setReady(1)
                    ->setReadydate(new \DateTime())
                    ->setReadyuserid(self::API_USER)
                    ->setInvoiced(1)
                    ->setInvoiceddate(new \DateTime())
                    ->setInvoiceduserid(self::API_USER)
                    ->setDone(0)
                    ->setDonedate(new \DateTime())
                    ->setDoneuserid(0)
                    ->setArrived(1)
                    ->setArriveduserid(self::API_USER)
                    ->setArriveddate(new \DateTime())
                    ->setVerified(1)
                    ->setVerifieduserid(self::API_USER)
                    ->setVerifieddate(new \DateTime())
                    ->setDeparted(0)
                    ->setDeparteduserid(self::API_USER)
                    ->setDeparteddate(new \DateTime())
                    ->setCompanyid(7560)
                    ->setFromcompanyid(7560)
                    ->setAltcompanyname('')
                    ->setCompanyname('')
                    ->setDeliverytermid('')
                    ->setCarrierid(0)
                    ->setAddress1('')
                    ->setAddress2('')
                    ->setZip('')
                    ->setCity('')
                    ->setCountryid(1)
                    ->setCarrierurl('')
                    ->setPickupaddress('')
            ;

            $this->em->persist($inboundStock);
            $this->em->flush();

            return $inboundStock->getId();
    }



    public function insertInvoiceInDb($_invoiceData){
    	$invoice = new Invoice();
        $invoice->setCreatedate(new \DateTime())
                ->setCreateuserid(1)
                ->setModifydate(new \DateTime())
                ->setModifyuserid(1)
                ->setActive(true)
                ->setInvoicedate(new \DateTime())
                ->setDuedate(new \DateTime())
                ->setDuebasedate(new \DateTime())
                ->setCredit(0)
                ->setCompanyid($_invoiceData['CompanyId'])
                ->setFromcompanyid($_invoiceData['FromCompanyId'])
                ->setStockid($_invoiceData['StockId'])
                ->setDescription($_invoiceData['Description'])
                ->setCurrencyid($_invoiceData['CurrencyId'])
                ->setCurrencyrate($_invoiceData['CurrencyRate'])
                ->setPaymenttermid($_invoiceData['PaymentTermId'])
                ->setSalesuserid($_invoiceData['SalesUserId'])
                ->setCompanyfooter($_invoiceData['CompanyFooter'])
                ->setEmail($_invoiceData['Email'])
                ->setReference('')
                ->setDiscount(0)
                ->setAccountingno(0)
                ->setInvoiceheader('')
                ->setInvoicefooter('')
                ->setPaidincash(0.00)
                ->setPaidincard(0.00)
                ->setInvoicesummaryid(0)
        ;
		if ($_invoiceData['Ready']) {
			$invoice->setReady(1)
					->setReadyuserid(1)
					->setReadydate(new \DateTime())
					->setNumber($this->getNextInvoiceNumber())
					->setDone(1)
					->setDoneuserid(1)
					->setDonedate(new \DateTime())
			;
		} else {
			$invoice->setReady(0)
					->setReadyuserid(0)
					->setReadydate(new \DateTime())
					->setNumber(0)
					->setDone(0)
					->setDoneuserid(0)
					->setDonedate(new \DateTime())
			;
		}

        try {
        	$this->em->persist($invoice);
	    	$this->em->flush();	
        } catch (Exception $e) {
			throw new ApiException('Some problem with saving DB: ' . $e->getMessage(), ApiException::CODE_PARAMETER_BAD_FORMAT);    	
        }

	    return $invoice->getId();
    }

    public function getConsolidatedOrderData($Id){
        $query = sprintf ('SELECT * FROM consolidatedorder WHERE Id=%d AND Active=1', $Id) ;
        $ids = $this->em->getConnection()->prepare($query);
        $ids->execute();
        $fetch  = $ids->fetch();
        if (empty($fetch)) {
            die('no consolidated order with that id ' . $Id);
            return false;
        } else {
            return $fetch;
        }   
    }

    

	/**
	* set order and orderlines completely done
	*
	* @return array
	*/
	public function orderCompletelyDone($invoiceId){

		$orderIds = $this->getOrderIdsFromInvoice($invoiceId);
		foreach ($orderIds as $key => $row) {
			$order = $this->em->getRepository('AppBundle\Entity\Order')->find($row['orderid']);
			$order->setDone(1)
	                ->setDoneuserid(1)
	                ->setOrderdoneid(5)
	                ->setDonedate(new \DateTime())
	        ;
	        $this->em->persist($order);
	        $this->em->flush();
		}

		$orderlines = $this->getAllOrderLinesFromInvoice($invoiceId);
		foreach ($orderlines as $key => $row) {
			$orderline = $this->em->getRepository('AppBundle\Entity\Orderline')->find($row['Id']);
			$orderline->setDone(1)
	                ->setDoneuserid(1)
	                ->setDonedate(new \DateTime())
	        ;
	        $this->em->persist($orderline);
	        $this->em->flush();
		}

	}



	/**
	* set order and orderlines as done
	*
	* @return array
	*/
	public function setOrderDone($invoiceId, $done){
        $done = $this->getOrdersPartlyDone($invoiceId);
		if($done['PartDelivery'] == 0){
			$this->orderCompletelyDone($invoiceId);
			return $done['InvoiceDraft'];
		}

        if($done['PartDelivery'] == 1 && $done['PartDeliveryLine'] == 0){
            $this->orderPartlyDone($invoiceId, true);
            return $done['InvoiceDraft'];
        }

		$this->orderPartlyDone($invoiceId, false);
		return $done['InvoiceDraft'];

	} //setOrderDone

  

}

