<?php

namespace AppBundle\Module\Export\Collection;
use AppBundle\Module\Mapper\MagentoCategoryMapper;
use Symfony\Component\DependencyInjection\Container;
use AppBundle\Service\Language;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Class ProductCollection
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Export\Collection
 */
class ProductCollection
{
    /**
     * @var ArrayCollection $collection
     */
    private $collection;

    /**
     * @var Container $container
     */
    private $container;

    /**
     * ProductCollection constructor.
     *
     * @param array $elements
     * @param Container $container
     */
    public function __construct(array $elements, Container $container)
    {
        $this->collection = new ArrayCollection($elements);
        $this->container = $container;
    }

    public function isEmpty()
    {
        return $this->collection->isEmpty();
    }

    /**
     * Method return array with all unique categories from products collection
     *
     * @return array
     */
    public function getCategories()
    {
        $categories = [];

        foreach ($this->collection as $product) {
            if (!in_array($product['CATID'], $categories)) {
                array_push($categories, $product['CATID']);
            }
        }

        return $categories;
    }

    /**
     * Method return array with all unique brands from products collection
     *
     * @return array
     */
    public function getBrands()
    {
        $brand = array();

        foreach ($this->collection as $product) {
            if (false == in_array($product['brand_id'], $brand)) {
                array_push($brand, $product['brand_id']);
            }
        }

        return $brand;
    }

    /**
     * Method return array with all unique colors attr from products collection
     *
     * @return array
     */
    public function getColors()
    {
        $colors = [];

        foreach($this->collection as $product) {
            if (false === isset($colors[$product['COLOR_ID']])) {
                $colors[$product['COLOR_ID']] = array(
                    'article_color_id' => $product['ArticleColorId'],
                    'color_id' => $product['COLOR_ID']
                );
            }
        }

        return $colors;
    }

    /**
     * Method return array with all unique size attr from products collection
     *
     * @return array
     */
    public function getSizes($all = false)
    {
        $sizes = [];

        foreach($this->collection as $product) {
            $key = $all === false ? $product['SIZE_NAME'] : $product['SIZE'];

            if (false === isset($sizes[$key])) {
                $sizes[$key] = array(
                    'id' => $product['SIZE'],
                    'name' => $product['SIZE_NAME']
                );
            }
        }

        return $sizes;
    }

    /**
     * Method return all unique variants from products collection
     *
     * @param bool $full return full variants data if true or only variantCode if false
     *
     * @return array
     */
    public function getVariants($full = false)
    {
        $variants = array();

        if ($full) {
            foreach($this->collection as $product) {
                if (false === isset($variants[$product['VariantCode']])) {
                    $variants = [
                        'variant_code' => $product['VariantCode'],
                        'color_id' => $product['COLOR_ID'],
                        'size_id' => $product['SIZE'],
                        'sex' => $product['SexId'],
                    ];
                }
            }

            return $variants;
        }

        foreach($this->collection as $product) {
            if (false === in_array($product['VariantCode'], $variants)) {
                array_push($variants, $product['VariantCode']);
            }
        }

        return $variants;
    }


    public function getUniqueArticles()
    {
        $articles = [];

        foreach ($this->collection as $product) {
            $unique = $product['id_product'];
            if (!in_array($unique, $articles)) {
                $articles[] = $unique;
            }
        }

        return $articles;
    }

    /**
     * Method return array with unique all products and all variants for each product from POT system in Magento API format
     *
     * @param array    $variantsCollection array with variants
     * @param array    $variantsOrder      array with variants orders
     * @param array    $categoryMap        array category mapper POT <-> Magento
     * @param array    $langs              array of supported languages
     * @param null|int $limit              limit of products -- only for tests
     *
     * @return array
     */
    public function getProducts(array $variantsCollection, array $categoryMap, array $langs, $limit = null)
    {
        $products = [];
        $currencies = [];
        $variantsTemp = [];
        $options = $this->container->getParameter('pot_export');
        $counter = 0;


        foreach ($this->collection as $product) {
            $articleId = $product['id_product'];

            $desc = $product['article_desc_lang'] != null ? $product['article_desc_lang'] : $product['article_title_lang'];
            $title = $product['article_title_lang'] != null ? $product['article_title_lang'] : $product['DESCRIPTION'];
            $lang = $product['article_lang'] != null ? $product['article_lang'] : 'default';
            $langKey = $lang . '_';

            if (false === isset($products[$articleId])) {
                $currencies = [];
                $variantsTemp = [];

                $products[$articleId]['category_id'] = $categoryMap[$product['CATID']]['id_magento'];
                $products[$articleId]['product_id'] = $product['PRODUCTID'];
                $products[$articleId]['article_id'] = $product['id_product'];
                $products[$articleId]['brand_id'] = $product['brand_id'];
                $products[$articleId]['season_id'] = $product['SEASON_ID'];
                $products[$articleId]['product_number'] = $product['PRODUCT_NUMBER'];
                $products[$articleId][$langKey . 'description'] = $desc;
                $products[$articleId][$langKey . 'title'] = $title;
                $products[$articleId]['prices'] = [];
                $products[$articleId]['WebFrontpage'] = (boolean)$product['front_page'];
                $products[$articleId]['WebFitGroup'] = $product['WebFitGroup'];
                $products[$articleId]['WebCampaign'] = $product['WebCampaign'];
                $products[$articleId]['variants'] = [];

                $counter++;
            } else {
                $products[$articleId][$langKey . 'description'] = $desc;
                $products[$articleId][$langKey . 'title'] = $title;
            }

            if ( false === isset($products[$articleId]['variants'][$product['VariantCode']])) {
                array_push($variantsTemp, $product['VariantCode']);

                $variantFilter = isset($variantsCollection[$product['VariantCode']]) ? $variantsCollection[$product['VariantCode']] : null;

                $variants = [
                    'category_id' => $categoryMap[$product['CATID']]['id_magento'],
                    'variant_code' => $product['VariantCode'],
                    'color_id' => $product['COLOR_ID'],
                    'size_id' => $product['SIZE'],
                    'sex' => $product['SexId'],
                    'color_number' => $product['COLOR_NUMBER']
                ];

                if ($variantFilter !== null) {
                    $variants['magento_color'] = $variantFilter['colorMagento'];
                    $variants['magento_sex'] = $variantFilter['sexMagento'];
                    $variants['magento_size'] = $variantFilter['sizeMagento'];
                    $variants['magento_brand'] = $variantFilter['brandMagento'];
                    $variants['quantity'] = $variantFilter['quantity'];
                    $variants['shops'] = $variantFilter['shops'];
                }

                $products[$articleId]['variants'][$variants['variant_code']] = $variants;
            }

            if (false === in_array($product['CurrencyId'], $currencies)) {
                array_push($currencies, $product['CurrencyId']);
                $products[$articleId]['prices'][$product['CurrencyName']] = [
                    'regular' => $product['PRICE'],
                    'campaign' => $product['CAMPAIGN_PRICE']
                ];
            }

            if ($limit != null && $counter > $limit) {
                array_pop($products);
                break;
            }
        }

        //update language products
        foreach ($langs as $langOpt) {
            foreach ($products as $articleId => $product) {
                $lang = isset($product['article_lang']) ? $product['article_lang'] : $langOpt['Culture'];
                $langKey = $lang . '_';

                $title = isset($product[$langKey . 'title']) ? $product[$langKey . 'title'] : $product['default_title'];
                $easyTitle = isset($product[$langKey . 'title']) ? true : false;
                $desc = isset($product[$langKey . 'description']) ? $product[$langKey . 'description'] : $product['default_description'];

                $currency = isset($options['currency'][$lang]) ? strtoupper($options['currency'][$lang]) : null;

                if (false === $easyTitle) {
                    $match = array();
                    preg_match("/^#$lang:.*@/m", $title, $match);

                    if (empty($match)) {
                        $title = strtok($title, "\n");
                    } else {
                        $title = str_replace(array("#$lang:", "@"), '', $match[0]);
                    }

                    $title = str_replace(array("\r", "\n", "\t", "@"), '', $title);
                }

                $price = isset($product['prices'][$currency]) ? $product['prices'][$currency]['regular'] : null;
                $special_price = isset($product['prices'][$currency]) ? $product['prices'][$currency]['campaign'] : null;

                $products[$articleId]['title_langs'][$lang] = ['val' => $title];
                $products[$articleId]['description_langs'][$lang] = ['val' => $desc];
                $products[$articleId]['prices']['langs'][$lang] = ['val' => $price];
                $products[$articleId]['special_prices']['langs'][$lang] = ['val' => $special_price];
                $products[$articleId]['visibilities']['langs'][$lang] = ['val' => $price != null ? "4" : "1"];
            }

        }

        $split = 100;

        if (is_numeric($options['products_per_request']) && $options['products_per_request'] > 0) {
            $split = $options['products_per_request'];
        }

        $divProducts = array_chunk($products, $split);

        return $divProducts;
    }

    public function getCampaigns()
    {
        $campaigns = array();

        foreach ($this->collection as $product) {
            if (false == in_array($product['WebCampaign'], $campaigns)) {
                array_push($campaigns, $product['WebCampaign']);
            }
        }

        return $campaigns;
    }
}
