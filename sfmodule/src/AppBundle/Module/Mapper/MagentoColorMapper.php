<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

use AppBundle\Entity\Variantcode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class MagentoColorMapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class MagentoColorMapper implements MapperInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * MagentoColorMapper constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:MagentoColor');
    }

    /**
     * Return a color attribute id from magento
     *
     * @param string $potColorId
     *
     * @return integer|null
     */
    public function map($potColorId)
    {
        $color = $this->repository->findOneBy(['idPot' => $potColorId]);

        if (null === $color) {
            return null;
        }

        return $color->getIdMagento();
    }
}