<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

use AppBundle\Entity\Variantcode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class VariantCodeMapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class VariantCodeMapper implements MapperInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * VariantCodeMapper constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:Variantcode');
    }

    /**
     * {@inheritdoc}
     *
     * @param string $variantCode
     *
     * @return ArrayCollection|null
     */
    public function map($variantCode)
    {
        $variant = $this->repository->findOneBy(['variantcode' => $variantCode]);

        if (null === $variant) {
            return null;
        }

        $data = [
            'article_id' => $variant->getArticleid(),
            'color_id' => $variant->getArticlecolorid(),
            'size_id' => $variant->getArticlesizeid()
        ];

        return new ArrayCollection($data);
    }
}