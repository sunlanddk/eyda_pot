<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

use AppBundle\Entity\Variantcode;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\EntityManager;

/**
 * Class MagentoVariantMapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class MagentoVariantMapper implements MapperInterface
{
    /**
     * @var \Doctrine\ORM\EntityRepository
     */
    private $repository;

    /**
     * MagentoVariantMapper constructor.
     *
     * @param EntityManager $em
     */
    public function __construct(EntityManager $em)
    {
        $this->repository = $em->getRepository('AppBundle:MagentoVariant');
    }

    /**
     * Return a simple product id from Magento
     *
     * @param string $variantCode
     *
     * @return integer|null
     */
    public function map($variantCode)
    {
        $variant = $this->repository->findOneBy(['variantCode' => $variantCode]);

        if (null === $variant) {
            return null;
        }

        return $variant->getIdMagento();
    }
}