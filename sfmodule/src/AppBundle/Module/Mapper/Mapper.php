<?php
/**
 * @author: Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @created: 2016-08-19
 */

namespace AppBundle\Module\Mapper;

/**
 * Class Mapper
 *
 * @author Kamil Kowalski <kamil.kowalski@jcommerce.pl>
 * @package AppBundle\Module\Mapper
 */
class Mapper
{
    /**
     * @var array
     */
    protected $typeList;

    /**
     * Mapper constructor.
     */
    public function __construct()
    {
        $this->typeList = array(
            'variantcode' => __NAMESPACE__.'\\VariantCodeMapper',
            'category'    => __NAMESPACE__.'\\MagentoCategoryMapper',
            'color'       => __NAMESPACE__.'\\MagentoColorMapper',
            'sex'         => __NAMESPACE__.'\\MagentoSexMapper',
            'size'        => __NAMESPACE__.'\\MagentoSizeMapper',
            'variant'     => __NAMESPACE__.'\\MagentoVariantMapper'
        );
    }

    /**
     * Factory service method for creating mapper
     *
     * @param string $type Type of mapper
     * @param array  $args List of arguments with should passed to mapper
     *
     * @return mixed
     */
    public function create($type, array $args = [])
    {
        if (!array_key_exists($type, $this->typeList)) {
            throw new \InvalidArgumentException("$type is not valid mapper");
        }

        $class = new \ReflectionClass($this->typeList[$type]);

        return $class->newInstanceArgs($args);
    }
}