<?php

/* 
 * Utils to help operate on arrays
 */

namespace AppBundle\Utils;

class Arrays
{
    /*
    * Compares two arrays by keys and values
    *
    * @param array $a1
    * @param array $a2
    * @return bool
    */
    public function arrayDiff($a1,$a2)
    {
        return ((is_array($a1) && is_array($a2)) &&
            (array_diff_assoc($a1 , $a2) || array_diff_assoc($a2, $a1))) ? true : false;
    }
    
    /*
    * Extracts and filter values from $array if there is a value then return
     * format e.x.: value of '0.00' transfer to '$.2f' notation
     * format value of 'long' and 'short' to php date format
    *
    * @param array $array
    * @param string $key    name of a key to extract
    * @param string $value_key  kay of a value to extract
    * @param bool $sufix    whether key should have suffix
    * @return array     array of $key => formated($value_key)
    */
   public function arrayAssocKeyValues($array, $key, $value_key, $sufix=true) 
   {
      $_res = array();

      for($i = 0; $i < count($array); $i++) {
         if (!is_null($array[$i]) && !empty($array[$i][$value_key])) {
             if($sufix){
                $idx = $array[$i][$key] . '_'. $i;
             }else{
                 $idx = $array[$i][$key];
             }
             
             switch ($value_key) {
                 case 'format':
                     if(strstr(strtolower($array[$i][$value_key]), 'short')){
                        $_res[$idx] = "Y-m-d";
                     }elseif(strstr(strtolower($array[$i][$value_key]), 'long')){
                        $_res[$idx] = "Y-m-d H:i:s";
                     }else{
                        $_value = str_replace(',','.',$array[$i][$value_key]);
                        $parts = explode('.',$_value);
                        $decDigits = @strlen($parts[1]);
                        if($decDigits){
                           $_res[$idx] = "%.".$decDigits."f";
                        }else{
                           $_res[$idx] = $_value;
                        }
                     }
                     break;

                  default:
                      $_res[$idx] = $array[$i][$value_key];
                      break;
             }
         }
      }

      return $_res;
   }

    /*
    * Extracts array keys values without exluded
    *
    * @param array $array
    * @param string $key
    * @param array $exclude list of excluded values
    * @return array     list of extracted keys
    */
   public function arrayKeyValues($array, $key, $exclude=array()) 
   {
      $_res = array();

      for($i = 0; $i < count($array); $i++) {
         if (!is_null($array[$i]) //&& !empty($array[$i][$key]) 
                 && !in_array($array[$i][$key],$exclude)) {

             $_res[] = $array[$i][$key];
         }
      }

      return $_res;
   }
   
    /*
    * Extracts array values matchin keys
    *
    * @param array $array
    * @param array $keys
    * @return array     list of extracted values
    */
   public function arrayValuesByKey($array,$keys)
   {
       $_res = array();

      for($i = 0; $i < count($keys); $i++) {
          $_res[$keys[$i]] = $array[$keys[$i]];
      }
      
      return $_res;
   }

}