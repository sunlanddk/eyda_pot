<?php

namespace Legacy\Lib;

class NavigationHelper
{

   public function navigationMark ($mark) {
      global $navigationMarkCache ;

      if ($navigationMarkCache["Mark"] != $mark) {
         // Get links
         $navigationMarkCache = null ;
         $link = array () ;
         $navigationMarkCache = current($this->_navigationItems($link, null, null, $mark)) ;
      }
      return (int)$navigationMarkCache['Id'] ;
   }

   public function navigationOnClickMark ($mark=null, $id=0, $param=null) {
      global $nid, $navigationMarkCache;

      $mark = $this->navigationMark($mark);
      if ($navigationMarkCache['Legacy'] || (is_null($mark))) {
         $html = "onclick='location.href=\"index.php?nid=" . ((is_null($mark)) ? $nid : $this->navigationMark($mark)) ;
      } else {
         $html = "onclick='location.href=\"\\\itworx.passon.web\\\index.aspx?nid=" . ((is_null($mark)) ? $nid : navigationMark($mark)) ;
      }

      if ($id > 0) {
         $html .= '&id='.$id ;
      }
      if (!is_null($param)) {
         $html .= '&'.$param ;
      }
      $html .= "\"' style='cursor:pointer;'" ;

      return $html ;
   }

   public function navigationOnClickByMark ($mark, $id=0, $param=null) {
      $link = array () ;
      $t = $this->_htmlOnClick ("appClick", current($this->_navigationItems($link, null, null, $mark)), $id, $param) ;
      if ($t == '') {
         return "style='cursor:default;'" ;
      }

      return $t . " style='cursor:pointer;'" ;
   }

   public function navigationIDsByMark($mark) {

      $link = array () ;
      $result = array_keys($this->_navigationItems($link, null, null, $mark));

      return implode(',', $result);
   }

   public function navigationOnClickLink ($mark, $id=0, $param=null) {
        global $Navigation, $navigationMarkCache ;

        if ($navigationMarkCache["Mark"] != $mark or $navigationMarkCache["ParentId"] != $Navigation["Id"]) {
            // Get links
            $navigationMarkCache = null ;
            $link = array();
            $navigationMarkCache = current($this->_navigationItems ($link, $Navigation["Id"], "l", $mark)) ;
        }
        if (is_null($navigationMarkCache)) 
            return "style='cursor:default;'" ;

        $t = $this->_htmlOnClick ("appClick", $navigationMarkCache, $id, $param) ;
        if ($t == '') 
            return "style='cursor:default;'" ;
        
        return $t . " style='cursor:pointer;'" ;
    }
    
    protected function _navigationItems ($menuarray, $parentid, $type, $mark=null) {
       global $Permission, $User, $Project, $NavigationParam, $PermFields, $PermMap ;

       $query = "SELECT Navigation.*, ".
          "NavigationType.Mark AS Type, NavigationType.LayoutPopup, NavigationType.LayoutRaw, ".
          "Function.PermRecord, Function.PermPublic, Function.PermAdminSystem, Function.PermAdminProject, Function.PermProjectRole";

       foreach ($PermFields AS $perm) {
          $query .= ", Permission.".$perm ;
       }
       $query .= " FROM (Navigation, NavigationType, Function)" ;
       $query .= sprintf (" LEFT JOIN Permission ON Navigation.FunctionId=Permission.FunctionId AND Permission.Active=1 AND ((Permission.RoleId=%d AND (NOT Function.PermProjectRole)) OR (Permission.RoleId=%d AND Function.PermProjectRole)) AND Permission.Active=1", $User['RoleId'], $Project['RoleId']) ;
       $query .= " WHERE" ;
       if ($parentid > 0) {
          $query .= " Navigation.ParentId=".$parentid." AND"  ;
       }
       if (!is_null($mark)) {
          $query .= " Navigation.Mark='".$mark."' AND" ;
       }
       if (!is_null($type)) {
          $query .= " Navigation.RefType='".$type."' AND" ;
       }
       $query .= " Navigation.Active=1 AND Navigation.NavigationTypeId=NavigationType.Id AND Navigation.FunctionId=Function.Id AND Function.Active=1" ;
       $query .= " ORDER BY Navigation.DisplayOrder" ;

       $result = dbQuery ($query) ;

       while ($row = dbFetch ($result)) {
          if ($row["Disable"] && !$NavigationParam[$row["Mark"]]["Enable"] && is_null($mark))
             continue ;
          if (!$User["AdminSystem"]) {
             if ($row["PermAdminSystem"])
                continue ;
             if (!$User["AdminProject"]) {
                if ($row["PermAdminProject"])
                   continue ;
                if (!$row["PermPublic"]) {
                   if ($row["PermRecord"] and $row["RefType"] != "m") {
                      if (!$Permission[$PermMap[$row["Operation"]]])
                         continue ;
                   } else {
                      if (!$row[$PermMap[$row["Operation"]]])
                         continue ;
                   }
                }
             }
          }
          $menuarray[((int)$row["Id"])] = $row ;
       }

       return $menuarray;
    }

   protected function _htmlOnClick ($func, $row, $id=0, $param=null) {
       global $NavigationParam ;

       if (isset($NavigationParam[$row["Mark"]]["Pasive"]) && $NavigationParam[$row["Mark"]]["Pasive"]) {
          return "" ;
       }

       return sprintf ("onclick=\"%s(event,%s)\"", $func, $this->_navigationParameters ($row, $id, $param)) ;
    }

    protected function _navigationParameters (&$row, $id=0, $param=null) {
       global $Navigation, $NavigationParam, $Application, $nid ;

       if (isset($NavigationParam[$row["Mark"]]["Parameter"])) {
          if (is_null($param)) {
             $param = $NavigationParam[$row["Mark"]]["Parameter"] ;
          } else {
             $param .= '&' . $NavigationParam[$row["Mark"]]["Parameter"] ;
          }
       }

       switch ($row["Type"]) {
          case 'back':
             $func = sprintf ("appBack(%d)", $row["Back"]) ;
             break ;

          case 'submit':
             if ($row['Function'] != '') {
                $query = sprintf ("SELECT Id FROM Navigation WHERE Mark='%s'", $row['Function']) ;
                $result = dbQuery ($query) ;
                $r = dbFetch ($result) ;
                dbQueryFree ($result) ;
                $row['Id'] = $r['Id'] ;
             } else {
                $row['Id'] = $row['ParentId'] ;
             }
             // Fall through
          case 'command':
             $func = sprintf("appSubmit(%d,%s,%s)", $row["Id"], ($row["RefIdInclude"])?"id":"null", ($row["RefConfirm"]!="") ? "'".$row["RefConfirm"]."'" : "null") ;
             break ;
          case 'ajax' :
             $func = sprintf("appLoadAjaxBody(%d,%s,%s,null,null,null,%s,%s)",
                $row["Id"],
                $row["RefIdInclude"]        ? "id"                          : "null",
                is_null($param)             ? "null"                        : "'".$param."'",
                $row["RefParameters"] != "" ? $row["RefParameters"]         : "null",
                $row["RefConfirm"]    != "" ? "'".$row["RefConfirm"]."'"    : "null"
             ) ;
             break;
          case 'main':
          case 'raw':
          case 'explore':
             $func = sprintf("appLoadLegacy(%s,%d,%s%s)",($row['Legacy']?'true':'false'), $row["Id"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "" : (",'".$param."'")) ;
             break ;

          case 'parent':
             $func = sprintf("appLoad(%d,%s%s)", $Navigation["ParentId"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "" : (",'".$param."'")) ;
             $id = $Application->ParentId ;	// Some what very dirty !!
             break ;

          case 'reload':
             $func = sprintf("appLoadLegacy(%s,%d,%s%s)",($row['Legacy']?'true':'false'), $nid, "id", (is_null($param)) ? "" : (",'".$param."'")) ;
             break ;

          case 'popup':
          case 'rawpopup':
             $func = sprintf("appLoadLegacy(%s,%d,%s,%s,%d,%d)",($row['Legacy']?'true':'false'), $row["Id"], ($row["RefIdInclude"])?"id":"null", (is_null($param)) ? "null" : ("'".$param."'"), $row["Width"], $row["Height"]) ;
             break ;

          case 'view':
             $func = sprintf("window.open('index.php?nid=%d&id='+id%s,'','')", $row["Id"], (is_null($param)) ? "" : "+'&".$param."'") ;
             break ;

          case 'link':
             $r = array();
             $n = current($this->_navigationItems ($r, NULL, NULL, $row['Function'])) ;
             $n['Id'] = $row['Id'] ;
             $n['Parameter'] = $row['Parameter'] ;
             $n['RefIdInclude'] = $row['RefIdInclude'] ;
             $n['Legacy'] = $row['Legacy'];
             return $this->_navigationParameters ($n, $id, $param) ;

          case 'script':
             $func = $row['Function'] ;
             break ;

          case 'none':
          default:
             $func = '' ;
       }

       return sprintf ("'%s',%s", addslashes($func), ($id > 0) ? $id : "null") ;
    }

    public function getImageIcon($mark)
    {
        $link = array();
        $navItem = current($this->_navigationItems ($link, 0, "l", $mark));
        
        return (isset($navItem['Icon'])) ? $navItem["Icon"] : 'doc.gif';
    }
    
}
