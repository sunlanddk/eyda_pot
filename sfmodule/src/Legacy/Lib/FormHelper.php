<?php


namespace Legacy\Lib;

class FormHelper
{

    public function formStart ($Id, $name=NULL, $encode=NULL) 
    {
        if (is_null($name)) {
            $name = 'appform' ;
        }
        $html = '<form method=POST name="' . $name . '"' ;
        if (!is_null($encode)) {
            $html .= ' enctype="' . $encode . '"' ;
        }
        $html .= ' onsubmit="return validate(this);">';
        $html .= sprintf ("<input type=hidden name=id value=%d>\n<input type=hidden name=nid>\n", $Id) ;
        
        return $html ;
    }

    public function formCalendar () 
    {
        $html = '';
        $html .= sprintf ("<link rel='stylesheet' type='text/css' media='all' href='"._LEGACY_URI."/layout/default/popcal.css'>\n") ;
        $html .= sprintf ("<script type='text/javascript' src='"._LEGACY_URI."/lib/popcal.js'></script>\n") ;
        
        return $html;
    }
    
    public function formLabel ($name,$req,$style='')
    {
        if($req){
            $html = '<label class="itemlabel required"';
        }else{
            $html = '<label class="itemlabel"';
        }
        $html .= ' style="' . $style . '"' ;

        $html .= '>';
        $html .= $name;
        if($req){
            $html .= ' *';
        }
        $html .= '</label>';
        
        return $html;
    }
    
    public function formText ($name, $value, $maxl=50, $size=0, $style='', $param='') 
    {
        $html = '<input type=text name=' . $name ;
        if ($size > 0) {
            $html .= ' maxlength=' . $maxl . ' size=' . $size ;
        }
        if ($param != '') {
            $html .= ' ' . $param ;
        }
        if ($style != '') {
            $html .= ' style="' . $style . '"' ;
        }
        $html .= ' value="' . htmlentities($value) . '">' ;

        return $html ;
    }

    public function formCheckbox ($name, $value=0, $style='', $param='') 
    {
        $html = '<input type=checkbox name=' . $name ;
        if ($style != '') {
            $html .= ' style="' . $style . '"' ;
        }
        if ($value) {
            $html .= ' checked' ;
        }
        if ($param != '') {
            $html .= ' ' . $param ;
        }
        $html .= '>' ;

        return $html ;
    }

    public function formDate ($name, $value, $style='width:68px;', $pos='bR', $param='') 
    {

        $html = "" ;
        $html .= '<input type=text id=Id' . $name . ' name=' . $name . ' size=10 maxlength=10 ' ;
        if ($style != '') {
            $html .= ' style="' . $style . '"' ;
        }
        $html .= '"' ;
        if ($value > 0) {
            $html .= ' value="' . date('Y-m-d', strtotime($value)) . '"' ;
        }
        if ($param != '') {
            $html .= ' ' . $param ;
        }
        $html .= '>' ;
        $html .= '<img src="'._LEGACY_URI.'/image/date.gif" id=Id' . $name . 'Button style="vertical-align:bottom;margin-left:0px;cursor: pointer;">' ;

        // Activate JavaScript calendar
        $html .= '<script type="text/javascript">' ;
        $html .= 'Calendar.setup ( {' ;
        $html .=	'inputField: "Id' . $name . '",' ;
        $html .= 	'button: "Id' . $name . 'Button",' ;
        $html .=	'align: "' . $pos . '"' ;
        $html .= '} ) ;' ;
        $html .= '</script>' ;

        return $html ;
    }
    
    function formHidden ($name, $value) {
	return '<input type=hidden name=' . $name . ' value="' . htmlentities($value) . '">' ;
    }

    public function formSelect ($name, $selectedId, $choices, $style='', $extrafields=NULL, $param='') {
        $selected = false ;
        $html = '<select size=1 name=' . $name ;
        if ($style != '') {
            $html .= ' style="' . $style . '"' ;
        }
        if ($param != '') {
            $html .= ' ' . $param ;
        }
        $html .= '>' ;
        if (is_array($extrafields)) {
            foreach ($extrafields as $id => $text) {
                if ($id == $selectedId) {
                    $selected = true ;
                }
                $html .= sprintf ("<option%s value=%d>%s\n", ($id == $selectedId)?" selected":"", $id, htmlentities($text));
            }
        }

        foreach ($choices as $val => $option) {
            if ($val == $selectedId) {
                $selected = true ;
            }
            $html .= sprintf ("<option%s value=%d>%s</option>\n", ($val == $selectedId)?" selected":"", $val, htmlentities($option));
        }

        if (!$selected) {
            $html .= sprintf ("<option selected value=0>- select -\n") ;
        }
        $html .= sprintf ("</select>\n") ;

        return $html ;
    }

    public function formSubmit()
    {
        // don't change [type='submit'] - check in template
        return "<input type='submit' class='change-button' value='Change' />";
    }

    public function formEnd () {
        return "</form>" ;
    }
    
}
