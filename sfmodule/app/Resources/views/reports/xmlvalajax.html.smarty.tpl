{block name=main}

{include file="reports/xmlview.html.smarty.tpl"}

<script type='text/javascript'>
	{literal}

    tableManager.nid = $('input[name="nid"]').val();

    //Set listeners to table headers (for sorting purposes)
    $('table.xmlDataTable th').click(function(event){
        var sortDir = $(this).attr('sort'),
        sortParam = $(this).attr('sortId');

        if(sortDir == 'ASC') {
            sortParam = - sortParam;
        }
        $.address.history(false);
        pagerLoadAjaxBody(tableManager.nid, null, null, '.main', {sort: sortParam});
        $.address.history(true);
    });
	{/literal}
</script>

</body>
</html>
{/block}