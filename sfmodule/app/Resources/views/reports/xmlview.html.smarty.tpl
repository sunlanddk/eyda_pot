

    {include file="library/filter_btns.html.smarty.tpl"}
    {include file="reports/formparam.html.smarty.tpl"}
    {include file="reports/addheader.html.smarty.tpl"}
    {include file="reports/groupby.html.smarty.tpl"}
    
    {foreach $notices as $notice}
        <div class='notice' style='border:1px solid #7a8e1a ;margin-right:300px;padding:5px;'>{$notice}</div>
     {/foreach}
    
    {include file="library/paging_bar.html.smarty.tpl"}
    <div style="display: none;">
    {include file="library/filter_full.html.smarty.tpl"}
    </div>

    <div class="filter container clearfix">
         <form id="actionform" name="actionform" class="form container" method='post'>
             <table class="tblData xmlDataTable" nid="{$nid}" style="table-layout: fixed">
                <thead>
                    <tr>
                           {foreach $headers as $k =>  $header}
                                {assign "idxKey" value="`$header`_`$k+1`"}
                                {assign "idxFormat" value="`$headersDb[$k]`_`$k+1`"}

                                {assign "class" value=""}
                                {if !empty($format[$idxFormat])  &&  substr($format[$idxFormat],0,1) != 'Y'}
                                    {assign "class" value="right"}
                                {/if}

                                {if $header@iteration eq abs($sort)}
                                   <th class="{$class}" style="width:{$styles[$idxKey]}px;" sortId='{$header@iteration}' sort="{if $sort >= 0}ASC{else}DESC{/if}">{$header}</th>
                                {else}
                                   <th class="{$class}" style="width:{$styles[$idxKey]}px;" sortId='{$header@iteration}' >{$header}</th>
                                {/if}
                            {/foreach}
                            {if count($action)}
                                <th class="center" style="width:50px">Action</th>
                            {/if}
                    </tr>
                    <tr>
                        {include file="library/filter_row.html.smarty.tpl"}
                    </tr>
                </thead>

                    <tbody>
                            {foreach $listItems as $i => $item}
                            <tr {*navigation_on_click_link_by_mark mark=$linkMark id=$item.ArticleNumber*}>
                                    {foreach $headersDb as $j => $header}
                                        {assign "aggregated" "Aggregated{$header}"}

                                        {if isset($item[$aggregated])}
                                            {assign "cellValue" $item[$aggregated]}
                                            {assign "class" value="right"}
                                        {else if $subColomns[$j]}
                                            {assign "cellValue" $item[$header]|default:''}
                                            {assign "class" value="right"}
                                        {else}
                                            {assign "cellValue" $item[$header]|default:''}
                                            {assign "class" value=""}
                                        {/if}

                                        {assign "idxKey" value="`$header`_`$j+1`"}
                                        {assign "imgNext" value="`$header`_`$j+2`"}
                                        {if !empty($hideIdents[$idxKey]) && ($i >=1) && ($cellValue == $listItems[$i-1][$header])}
                                            {assign "cellValue" ""}
                                        {/if}

                                        {if !empty($hideIdents[$imgNext]) && ($i >=1) && ($cellValue == $listItems[$i-1][$header])}
                                            {assign "imgValue" "0"}
                                        {else}
                                            {assign "imgValue" "1"}
                                        {/if}

                                        {if !empty($imgFields[$idxKey])}
                                            {if $imgValue && !empty($item[$headersDb[$j+1]] ) }
                                                <td class="right" >{navigation_on_click_imglink_by_mark mark=$imgFields[$idxKey] id=$cellValue}</td>
                                            {else}
                                                <td></td>
                                            {/if}
                                        {elseif !empty($format[$idxKey]) && !empty($cellValue) &&  substr($format[$idxKey],0,1) == 'Y'}
                                            <td class="" >{date($format[$idxKey],strtotime($cellValue))}</td>
                                        {elseif !empty($format[$idxKey])  && empty($item[$aggregated])}
                                            <td class="right" >{sprintf($format[$idxKey],$cellValue)}</td>
                                        {else}
                                            <td class="{$class}" >{$cellValue}</td>
                                        {/if}
                                    {/foreach}
                                    {if count($action)}
                                        <td style="text-align:center;"><input name='{$item[$action['index']]}' type='checkbox' value='{$item[$action['index']]}' /></td>
                                    {/if}
                            </tr>
                            {/foreach}
                            {if $emptyInfo}
                                    <tr><td colspan="{count($headers)}">{$emptyInfo}</td></tr>
                            {/if}
                    </tbody>

                    {if isset($totals)}
                    <tfoot>
                            <tr>
                                {foreach $headersDb as $j => $header}
                                    {assign "idxKey" value="`$header`_`$j+1`"}
                                    {if empty($totals[$header])}
                                        <td class=""></td>
                                    {else}
                                        {if !empty($format[$idxKey]) && substr($format[$idxKey],0,1) == 'Y'}
                                            <td class="" >{date($format[$idxKey],strtotime($totals[$header]))}</td>
                                        {elseif !empty($format[$idxKey])}
                                            <td class="right" >{sprintf($format[$idxKey],$totals[$header])}</td>
                                        {else}
                                            <td class="">{$totals[$header]}</td>
                                        {/if}
                                    {/if}
                                {/foreach}

                                {if count($action)}
                                    <td></td>
                                {/if}
                            </tr>
                    </tfoot>
                    {/if}
            </table>
            {if count($action)}
            <input type="submit" value="Perform Action" style="margin-top:10px;width:150px;"/>
            {/if}
    </form>
    </div>

    {include file="library/paging_bar.html.smarty.tpl"}

