<div class="reportAddHeader" style="margin:5px;">
    {if count($addHeader)}
        <!--<div><b>Additional header:</b></div>-->
        {foreach $addHeader as $label => $value}
            <div class="add-header-line">
            <label>{$label}</label>:
            <span>{$value}</span>
            </div>
        {/foreach}
    {/if}
</div>