
    {if count($listField)}

          {foreach $headersDb as $k => $header}
                {assign var="i" value=$k+1}
                {if !$listField[$i] || $listField[$i].nofilter }
                   <td style="width:{$listField[$i].width}px;padding: 5px {$listField[$i].width/2}px;"></td>
                   {continue}
                {/if}
                <td style="width:{$listField[$i].width}px;padding: 5px;text-align: center;" title="{$listField[$i].name}">
                      {assign var="listField.type" value=$listField[$i].type|default:0}
                      {assign var="filterValue" value=$filterValues.$i|default:''}
                   {if $listField[$i].type == 1}
                      <input style="width:{$listField[$i].width}px;" type=text name="{$listField[$i].db}" value="{$filterValue}" 
                             onkeypress="upEnter(event,this)" onchange="updateInput(this)" class="datepicker">
                   {elseif $listField[$i].type == 2}
                      <input style="width:{$listField[$i].width}px;" type=text name="{$listField[$i].db}" value="{$filterValue}" 
                             onkeypress="upEnter(event,this)" onchange="updateInput(this)" class="integer">
                   {elseif $listField[$i].type == 3}
                      <input style="width:{$listField[$i].width}px;" type=text name="{$listField[$i].db}" value="{$filterValue}" 
                             onkeypress="upEnter(event,this)" onchange="updateInput(this)" class="float">
                   {elseif $listField[$i].type == 4}
                      <select  style="width:{$listField[$i].width}px;" name="{$listField[$i].name}" 
                               onkeypress="upEnter(event,this)" onchange="updateInput(this)" class="">
                         <option></option>
                         {if isset($listField[$i].options)}
                            {html_options options=$listField[$i].options selected=$filterValue}
                         {else}
                            <option value="Yes">Yes</option>
                            <option value="No">No</option>
                         {/if}
                      </select>
                   {elseif $listField[$i].type == 5 && isset($listField[$i].options)}
                      <select name="{$listField[$i].db}" onkeypress="upEnter(event,this)" onchange="updateInput(this)" class="">
                         {if isset($listField[$i].options)}
                            {html_options options=$listField[$i].options selected=$filterValue}
                         {/if}
                      </select>
                   {else}
                      <input style="width:{$listField[$i].width}px;" type=text name="{$listField[$i].db|escape}" value="{$filterValue}" 
                             onkeypress="upEnter(event,this)" onchange="updateInput(this)" class="" />
                   {/if}

                </td>
             {/foreach}
            {if  count($action)}
                <td style="width:50px;padding: 5px 15px;"></td>
            {/if}

            <script type='text/javascript'>
                    {literal}

                    function updateInput(ish) {  
                        var inputs = document.getElementById("filterform").elements;
                        for (index = 0; index < inputs.length; ++index) {
                            if(inputs[index].name === ish.name){
                                inputs[index].value = ish.value;
                            }
                        }
                    }
                    $('.datepicker').datepicker( {
                        dateFormat : 'yy-mm-dd'
                    });
                    function upEnter(e,el){
                        if(e.keyCode == 13){
                            updateInput(el);
                            //$('#filterform').submit();
                            sendForm();
                        }
                    }
                    
                    {/literal}
            </script>

            <div style="clear: both"></div>

    {/if}


