<div class="filter buttons container">
      <ul  style="float: right; margin-right: 60px; min-width:100px;">
         <li style="margin:5px;">
               {if ($Navigation["LayoutRaw"] || (isset($Navigation["DefaultLayoutRaw"]) && $Navigation["DefaultLayoutRaw"]))}
                    <input type="button" id="submit" value="Search"/>
               {else}
                    <input type="button" onclick="sendForm()" value="Search"/>
               {/if}
         </li>
         <li style="margin:5px;">
            <input type="hidden" id="clearFilter" name="clearFilter" value=""/>
            <input type="button" id="reset" value="Clear"/>
         </li>
      </ul>
</div>
<script type='text/javascript'>
        {literal}
        $("#reset").click(function(){
           $('#filterform').closest('form').find("input[type=text], textarea").val("");
           //$('#filterform').submit();
           sendForm();
        });

        function sendForm() {
            var values = {};

            $.each($('form[name="paramFilters"]').serializeArray(), function(i, field) {
                values[field.name] = field.value;
            });

            $('input[name="filterParams"]').val(JSON.stringify(values));
            filterform.submit();
        }

        {/literal}
</script>


