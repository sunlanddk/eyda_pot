<div style="clear:both;" class="pagging">
	{*link to first page*}
                {if $current_page > 1}
	<a href="#" class="paggingArrow" {if $current_page != 1}{prepare_paging_link offset=0}{/if}>
                        <img src="{_LEGACY_URI}/layout/new/images/firstPage.png" alt="Passon logo" />
	</a>
                {else}
                        <img src="{_LEGACY_URI}/layout/new/images/firstPage.png" alt="Passon logo" />
                {/if}
	
	{*link to previous page*}
                {if $current_page > 1}
	<a href="#" class="paggingArrow" {if $current_page != 1} {prepare_paging_link offset=($current_page-2)*$lines_per_page} {/if}>
                        <img src="{_LEGACY_URI}/layout/new/images/left.png" 
                                    onmouseover="$(this).attr('src', '{_LEGACY_URI}/layout/new/images/leftHover.png');"
                                    onmouseout="$(this).attr('src', '{_LEGACY_URI}/layout/new/images/left.png');" 
                                    alt="Passon logo" />
	</a>
                {else}
                        <img src="{_LEGACY_URI}/layout/new/images/left.png" alt="Passon logo" />
                {/if}
	
	<div>
		<span class="currentPage">Page {$current_page} of {$overall_pages}</span>
	</div>
	
	{*link to next page*}
                {if $current_page < $overall_pages}
	<a href="#" class="paggingArrow" {if $current_page != $overall_pages} {prepare_paging_link offset=$current_page*$lines_per_page} {/if}>
                        <img src="{_LEGACY_URI}/layout/new/images/right.png" onmouseover="$(this).attr('src', '{_LEGACY_URI}/layout/new/images/rightHover.png');"
                                   onmouseout="$(this).attr('src', '{_LEGACY_URI}/layout/new/images/right.png');" 
                                   alt="Passon logo" />
	</a>
                {else}
                        <img src="{_LEGACY_URI}/layout/new/images/right.png" alt="Passon logo" />
                {/if}
	
	{*link to last page*}
                {if $current_page < $overall_pages}
	<a href="#" class="paggingArrow" {if $current_page != $overall_pages} {prepare_paging_link offset=($overall_pages-1)*$lines_per_page} {/if}>
                        <img src="{_LEGACY_URI}/layout/new/images/lastPage.png" alt="Passon logo" />
	</a>
                {else}
                        <img src="{_LEGACY_URI}/layout/new/images/lastPage.png" alt="Passon logo" />
                {/if}
</div>
