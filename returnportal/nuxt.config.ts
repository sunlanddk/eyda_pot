import path from 'path';
import { defineNuxtConfig } from "nuxt/config";
import svgLoader from 'vite-svg-loader'
import { MerchantsEnum } from "./src/enums/merchants";

const APP_MERCHANT: string = [
    MerchantsEnum.eyda,
    MerchantsEnum.passon
].includes(process.env.APP_MERCHANT as string) ? process.env.APP_MERCHANT as string : MerchantsEnum.eyda;

// https://nuxt.com/docs/api/configuration/nuxt-config
export default defineNuxtConfig({
    ssr: false,
    runtimeConfig: {
        // The private keys which are only available within server-side
        example: 'example',
        // Keys within public, will be also exposed to the client-side
        public: {
            APP_MERCHANT: APP_MERCHANT as string,
            APP_LOCALES: process.env.APP_LOCALES as string,
            APP_FALLBACK_LOCALE: process.env.APP_FALLBACK_LOCALE as string,
            APP_RETURN_LABEL_GENERATE_DEFAULT_TYPE: process.env.APP_RETURN_LABEL_GENERATE_DEFAULT_TYPE as string,
            API_BASE_URL: process.env.API_BASE_URL as string
        }
    },
    modules: [
        '@nuxtjs/tailwindcss',
        '@nuxtjs/i18n',
        '@vueuse/nuxt',
        '@pinia/nuxt',
        '@pinia-plugin-persistedstate/nuxt'
    ],
    app: {
        baseURL: '/returnportal/',
        head: {
            charset: 'utf-8',
            viewport: 'width=device-width, initial-scale=1',
            link: [{ rel: 'icon', type: 'image/png', href: `/image/${APP_MERCHANT}/favicon.png?v2` }]
        }
    },
    nitro: {
        output: {
            publicDir: path.join(__dirname, 'returnportal')
        }
    },
    vite: {
        plugins: [
            svgLoader({
                /* ... */
            })
        ]
    },
    postcss: {
        plugins: {
            'postcss-import': {},
            'tailwindcss/nesting': {},
            tailwindcss: path.resolve(__dirname, './tailwind.config.js'),
            'postcss-nested': {}
        },
    },
    purgeCSS: {
        mode: 'postcss',
        enabled: (process.env.NODE_ENV === 'production')
    },
    css: [
        `~/assets/css/themes/${APP_MERCHANT}/main.scss`,
        `~/node_modules/flag-icons/css/flag-icons.min.css`
    ],
    i18n: {}
});
