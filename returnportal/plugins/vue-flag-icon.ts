// @ts-ignore
import FlagIcon from 'vue-flag-icon'

export default defineNuxtPlugin(({ vueApp }) => {
    vueApp.use(FlagIcon)
})
