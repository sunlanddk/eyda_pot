import { createI18n } from 'vue-i18n'

export default defineNuxtPlugin(async ({vueApp}) => {
    const runtimeConfig = useRuntimeConfig()
    const i18nLocale = document.cookie.replace(/(?:(?:^|.*;\s*)i18n_locale\s*\=\s*([^;]*).*$)|^.*$/, '$1') || window.navigator.language

    const messages: { [x: number]: any }[] = []
    const locales = runtimeConfig.public.APP_LOCALES.split('|')
    for (const locale of locales) {
        messages.push({
            [locale]: await import(`~/locales/${locale}.json`)
        })
    }

    const i18n = createI18n({
        legacy: false, // you must set `false`, to use Composition API
        globalInjection: true,
        locale: i18nLocale || runtimeConfig.public.APP_FALLBACK_LOCALE,
        fallbackLocale: runtimeConfig.public.APP_FALLBACK_LOCALE,
        messages: messages.reduce((acc, cur) => {
            return Object.assign(acc, cur)
        }, {})
    })

    vueApp.use(i18n)
})
