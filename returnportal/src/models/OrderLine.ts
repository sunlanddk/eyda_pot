export interface OrderLine {
  id: number
  description: string
  color: string
  size: string
  priceCost: string
  quantity: number
}
