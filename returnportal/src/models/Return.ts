import { ReturnLine } from '@/src/models/ReturnLine'

export interface Return {
  id: number
  orderId: number
  fundsType: number
  labelType: string | null
  createDate: string
  returnLines: ReturnLine[]
}
