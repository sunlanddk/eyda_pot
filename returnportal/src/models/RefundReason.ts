export interface RefundReason {
  id: number
  name: string
  description: string
}
