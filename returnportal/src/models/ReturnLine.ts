export interface ReturnLine {
  id: number
  orderLineId: number
  refundReasonId: number
  quantity: number
  comment: string
}
