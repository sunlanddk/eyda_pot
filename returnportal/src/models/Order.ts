import { OrderLine } from '@/src/models/OrderLine'

export interface Order {
  id: number
  description: string
  email: string
  number: string
  postcode: string
  orderLines: OrderLine[]
}
