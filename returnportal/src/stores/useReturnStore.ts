import { Return } from '@/src/models/Return'
import { ReturnLine } from '@/src/models/ReturnLine'
import { apiClient } from '@/src/modules/ApiClient'
import OrderReturn from '@/src/types/attributes/orderReturn'
import OrderReturnItems from '@/src/types/attributes/orderReturnItems'
import { defineStore } from 'pinia'

export const useReturnStore = defineStore('return', {
  state: () => {
    return {
      return: {} as Return,
      returns: [] as Return[],
      returnLines: [] as ReturnLine[]
    }
  },
  persist: {
    storage: persistedState.sessionStorage
  },
  actions: {
    async fetchReturnByOrderNumberEmailPostcode(
      orderNumber: string,
      orderEmail: string,
      orderPostcode: string
    ): Promise<void> {
      this.returns = []
      this.returnLines = []

      const response = await apiClient().fetchReturnByOrderNumberEmailPostcode(
        orderNumber,
        orderEmail,
        orderPostcode
      )

      if (!response?.data || Object.keys(response?.data).length === 0) {
        this.returns = []

        return
      }

      this.returns = response.data as Return[]

      Object.values(this.returns).forEach((returnItem) => {
        Object.values(returnItem.returnLines).forEach((returnLine) => {
          this.returnLines.push(returnLine)
        })
      })
    },
    async storeReturnOrderItems(
      orderNumber: string,
      orderEmail: string,
      orderPostcode: string,
      orderReturn: OrderReturn,
      orderReturnItems: OrderReturnItems[]
    ): Promise<void> {
      const response = await apiClient().storeReturnOrderItems(
        orderNumber,
        orderEmail,
        orderPostcode,
        orderReturn,
        orderReturnItems
      )

      this.return = response.data as Return
    }
  },
  getters: {
    getReturnData: (state) => state.return as Return,
    getReturnsData: (state) => state.returns as Return[],
    getReturnLinesData: (state) => state.returnLines as ReturnLine[],
    getReturnLinesQuantityByOrderLineId: (state) => (orderLineId: number) => {
      return state.returnLines
        .filter((returnLine) => returnLine.orderLineId === orderLineId)
        .reduce((number: number, returnLine: ReturnLine) => number + returnLine.quantity, 0)
    }
  }
})
