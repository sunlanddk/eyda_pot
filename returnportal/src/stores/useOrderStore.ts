import { Order } from '@/src/models/Order'
import { OrderLine } from '@/src/models/OrderLine'
import { ReturnLine } from '@/src/models/ReturnLine'
import { apiClient } from '@/src/modules/ApiClient'
import OrderReturn from '@/src/types/attributes/orderReturn'
import OrderReturnItems from '@/src/types/attributes/orderReturnItems'
import { defineStore } from 'pinia'

export const useOrderStore = defineStore('order', {
  state: () => {
    return {
      order: {} as Order,
      orderReturn: {
        orderReturnItems: [] as OrderReturnItems[]
      } as OrderReturn,
      orderReturnItemsChecked: [] as OrderReturnItems[]
    }
  },
  persist: {
    storage: persistedState.sessionStorage
  },
  actions: {
    async fetchOrderForReturn(
      orderNumber: string,
      orderEmail: string,
      orderPostcode: string
    ): Promise<void> {
      const response = await apiClient().fetchOrderForReturn(orderNumber, orderEmail, orderPostcode)
      this.order = response.data as Order
    },
    initializeOrderReturnItem(orderReturnLinesItems: ReturnLine[]): boolean {
      this.order?.orderLines?.forEach((item: OrderLine) => {
        const orderReturnLineItemQuantity = orderReturnLinesItems
          .filter((returnLine) => returnLine.orderLineId === item.id)
          .reduce((number: number, returnLine: ReturnLine) => number + returnLine.quantity, 0)
        const orderReturnItemQuantity = orderReturnLineItemQuantity
          ? item.quantity - orderReturnLineItemQuantity
          : item.quantity

        this.orderReturn.orderReturnItems.push({
          orderLineId: item.id,
          refundReasonId: 0,
          quantity: orderReturnItemQuantity,
          comment: '',
          isChecked: false,
          isDisabled: true
        })
      })

      this.orderReturn.orderReturnLabelType = null
      this.orderReturn.orderReturnFundsType = null
      this.orderReturn.orderReturnItems = this.orderReturn.orderReturnItems.filter((item) => item)

      return true
    },
    toggleOrderReturnItemChange(orderLineId: number): void {
      this.orderReturn.orderReturnItems.forEach((item) => {
        if (item.orderLineId === orderLineId) {
          item.isChecked = !item.isChecked
          item.isDisabled = !item.isDisabled
        }
      })
    },
    $resetOrderReturnItems(): void {
      this.orderReturn.orderReturnItems = []
    },
    $resetOrderReturnItemsChecked(): void {
      this.orderReturnItemsChecked = []
    }
  },
  getters: {
    getOrderData: (state) => state.order as Order,
    getOrderReturnData: (state) => state.orderReturn as OrderReturn,
    getOrderReturnItemsData: (state) => state.orderReturn.orderReturnItems as OrderReturnItems[],
    getOrderReturnItemsChecked: (state) => {
      state.orderReturnItemsChecked = Object.values(state.orderReturn.orderReturnItems).filter(
        (item) => {
          return item.isChecked === true
        }
      )

      return state.orderReturnItemsChecked
    },
    findOrderLineById: (state) => (id: number) => {
      return state.order.orderLines.find((orderLine) => orderLine.id === id) as OrderLine
    }
  }
})
