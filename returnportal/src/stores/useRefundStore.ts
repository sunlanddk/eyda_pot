import { RefundReason } from '@/src/models/RefundReason'
import { apiClient } from '@/src/modules/ApiClient'
import { defineStore } from 'pinia'

export const useRefundStore = defineStore('refund', {
  state: () => {
    return {
      reasons: {} as RefundReason[]
    }
  },
  persist: {
    storage: persistedState.sessionStorage
  },
  actions: {
    async fetchRefundReasons() {
      const response = await apiClient().fetchRefundReasons()
      this.reasons = response?.data as RefundReason[]
    }
  },
  getters: {
    getRefundReasonsData: (state) => state.reasons,
    findRefundReasonById: (state) => (id: number) => {
      return state.reasons.find((reason) => reason.id === id) as RefundReason
    }
  }
})
