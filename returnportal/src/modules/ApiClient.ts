import { APIRouteEnum } from '@/src/enums/route'
import OrderReturn from '@/src/types/attributes/orderReturn'
import OrderReturnItems from '@/src/types/attributes/orderReturnItems'

type Api = {
  fetchOrderForReturn: (
    orderNumber: string,
    orderEmail: string,
    orderPostcode: string
  ) => Promise<any>
  fetchReturnByOrderNumberEmailPostcode: (
    orderNumber: string,
    orderEmail: string,
    orderPostcode: string
  ) => Promise<any>
  storeReturnOrderItems: (
    orderNumber: string,
    orderEmail: string,
    orderPostcode: string,
    orderReturn: OrderReturn,
    orderReturnItems: OrderReturnItems[]
  ) => Promise<any>
  fetchRefundReasons: () => Promise<any>
  fetchReturnGenerateLabel: (orderId: number, returnId: number, labelType: string) => Promise<any>
}

export function apiClient(): Api {
  const runtimeConfig = useRuntimeConfig()
  const locale =
    document.cookie.replace(/(?:(?:^|.*;\s*)i18n_locale\s*\=\s*([^;]*).*$)|^.*$/, '$1') ||
    runtimeConfig.public.APP_FALLBACK_LOCALE
  const defaultHeaders = {
    Accept: 'application/json',
    'Content-Type': 'application/json',
    'User-Language': locale
  }

  return {
    async fetchOrderForReturn(
      orderNumber: string,
      orderEmail: string,
      orderPostcode: string
    ): Promise<any> {
      const { error, data } = await useFetch(
        runtimeConfig.public.API_BASE_URL + APIRouteEnum.fetchOrderByOrderNumberEmailPostcode,
        {
          body: { orderNumber, orderEmail, orderPostcode },
          method: 'POST',
          headers: defaultHeaders
        }
      )

      if (data.value) {
        return data.value
      }

      // @ts-ignore
      throw new Error(JSON.stringify(error.value.data))
    },
    async fetchReturnByOrderNumberEmailPostcode(
      orderNumber: string,
      orderEmail: string,
      orderPostcode: string
    ): Promise<any> {
      const { error, data } = await useFetch(
        runtimeConfig.public.API_BASE_URL + APIRouteEnum.fetchReturnByOrderNumberEmailPostcode,
        {
          body: { orderNumber, orderEmail, orderPostcode },
          method: 'POST',
          headers: defaultHeaders
        }
      )

      if (data.value) {
        return data.value
      }

      // @ts-ignore
      throw new Error(JSON.stringify(error.value.data))
    },
    async storeReturnOrderItems(
      orderNumber: string,
      orderEmail: string,
      orderPostcode: string,
      orderReturn: OrderReturn,
      orderReturnItems: OrderReturnItems[]
    ): Promise<any> {
      const { error, data } = await useFetch(
        runtimeConfig.public.API_BASE_URL + APIRouteEnum.storeReturnOrderItems,
        {
          body: {
            orderNumber,
            orderEmail,
            orderPostcode,
            orderReturn,
            orderReturnItems
          },
          method: 'POST',
          headers: defaultHeaders
        }
      )

      if (data.value) {
        return data.value
      }

      // @ts-ignore
      throw new Error(JSON.stringify(error.value.data))
    },
    async fetchRefundReasons(): Promise<any> {
      const { error, data } = await useFetch(
        runtimeConfig.public.API_BASE_URL + APIRouteEnum.fetchRefundReasons,
        {
          method: 'GET',
          headers: defaultHeaders
        }
      )

      if (data.value) {
        return data.value
      }

      // @ts-ignore
      throw new Error(JSON.stringify(error.value.data))
    },
    async fetchReturnGenerateLabel(
      orderId: number,
      returnId: number,
      labelType: string
    ): Promise<any> {
      const { error, data } = await useFetch(
        runtimeConfig.public.API_BASE_URL + APIRouteEnum.fetchReturnGenerateLabel,
        {
          params: {
            'order-id': orderId,
            'return-id': returnId,
            'label-type': labelType
          },
          method: 'GET',
          headers: defaultHeaders
        }
      )

      if (data.value) {
        return data.value
      }

      // @ts-ignore
      throw new Error(JSON.stringify(error.value.data))
    }
  }
}
