import { MerchantsEnum } from '@/src/enums/merchants'
// @ts-ignore
import merchantEyda from '@/src/merchants/eyda.json'
// @ts-ignore
import merchantPasson from '@/src/merchants/passon.json'

const runtimeConfig: any = useRuntimeConfig()

/**
 * Get merchant setting by key
 *
 * @param key
 */
export function getMerchantSettingByKey(key: string): any {
  let merchantSettings = null

  switch (runtimeConfig.public.APP_MERCHANT) {
    case MerchantsEnum.eyda:
      merchantSettings = merchantEyda
      break
    case MerchantsEnum.passon:
      merchantSettings = merchantPasson
      break
    default:
      merchantSettings = merchantPasson
      break
  }

  // @ts-ignore
  return key.split('.').reduce((o, i) => o[i], merchantSettings) || null
}

/**
 * Merchant global configs
 */
export const isReturnProcessingRadioBlockFields = (getMerchantSettingByKey(
  'pages.order.return.processing.components.radioBlockField.refund.active'
) ||
  getMerchantSettingByKey(
    'pages.order.return.processing.components.radioBlockField.giftCard.active'
  )) as boolean
