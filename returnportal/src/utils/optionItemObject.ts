import { OptionItem } from '@/src/types/optionItem'

export const generateOptionItemObject = (number: number): OptionItem[] => {
  const objects: OptionItem[] = []

  for (let i = 0; i <= number; i++) {
    objects.push({
      onClick(): void | null {
        return undefined
      },
      type: null,
      value: i,
      label: `${i}`
    })
  }

  return objects
}

export const makeOptionItemObject = (
  data: any[],
  valueKey: string,
  labelKey: string
): OptionItem[] => {
  const objects: OptionItem[] = []

  for (let i = 0; i < data.length; i++) {
    objects.push({
      onClick(): void | null {
        return undefined
      },
      type: null,
      value: data[i][valueKey],
      label: data[i][labelKey]
    })
  }

  return objects
}
