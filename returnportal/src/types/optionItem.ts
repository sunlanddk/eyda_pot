export type OptionItem = {
  label: string
  value: string | number | null
  type: 'text' | 'link' | null
  onClick: () => void | null
  handler?: (...args: any[]) => number
}
