import OrderReturnItems from '@/src/types/attributes/orderReturnItems'

export default interface OrderReturn {
  orderReturnLabelType: string | null
  orderReturnFundsType: string | null
  orderReturnItems: OrderReturnItems[]
  orderReturnItemsChecked: OrderReturnItems[]
}
