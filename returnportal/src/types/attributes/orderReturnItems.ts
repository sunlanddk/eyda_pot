export default interface OrderReturnItems {
  orderLineId: number
  refundReasonId: number
  quantity: number
  comment: string
  isChecked: boolean
  isDisabled: boolean
}
