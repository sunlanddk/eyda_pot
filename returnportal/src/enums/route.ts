export enum FrontendRouteEnum {
  initReturnOrder = '/order/return',
  detailedReturnOrder = '/order/return/detailed',
  processingReturnOrder = '/order/return/processing',
  processedReturnOrder = '/order/return/processed'
}

export enum APIRouteEnum {
  fetchOrderByOrderNumberEmailPostcode = '/order/return',

  fetchReturnByOrderNumberEmailPostcode = '/return/order',
  fetchReturnGenerateLabel = '/return/label/generate',
  storeReturnOrderItems = '/return/order/store',

  fetchRefundReasons = '/refund/reasons'
}
