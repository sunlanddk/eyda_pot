export enum ButtonTypeEnum {
  'default' = 'default',
  'primary' = 'primary',
  'secondary' = 'secondary',
  'plain' = 'plain',
  'link' = 'link'
}
