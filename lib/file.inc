<?php

require_once('lib/log.inc');

   function fileName($type, $id, $create = false, $extension = false) {
      global $Config;

      $idstring = sprintf('%08d', $id);
      $file     = $Config['fileStore'] . '/' . $type;

      // Verify directory existance
      while ($idstring != '') {
         if ($create) {
            if (!is_dir($file)) {
               if (!mkdir($file, 0750)) {
                  return NULL;
               }
            }
         }
         $file .= '/' . substr($idstring, 0, 2);
         $idstring = substr($idstring, 2);
      }
      if($extension !== false){
      	return $file.$extension;	
      }
      return $file;
   }

	function array2csv($array) {
		$csv = array();
		foreach ($array as $item) {
			if (is_array($item)) {
				$csv[] = array_2_csv($item);
			} else {

//				$p = ereg_replace("\r\n?" "\n", $item);
				$p = str_replace(";", ',', $item);
				$csv[] = $p;
//				$csv[] = $item;
			}
		}
		return implode(';', $csv);
	} 
   
   
	function ftpUpload($logininfo, $fileinfo, $fullpath = false) {  
		if($fullpath !== false){
			$ftp_file 	=  $fileinfo['ftpPath'];
			$local_file =  $fileinfo['localePath'];
		}
		else{
			$ftp_file 	=  $fileinfo['ftpPath'] . $fileinfo['fileName'] ;
			$local_file =  $fileinfo['localePath'] . $fileinfo['fileName'];
		}

		// connect to FTP server (port 21)
		$conn_id = ftp_connect($logininfo['host'], 21) or die ("Cannot connect to host");
		 
		// send access parameters
		ftp_login($conn_id, $logininfo['usr'], $logininfo['pwd']) or die("Cannot login " . $login['usr'] . ' - ' . $login['pwd']);
		 
		// turn on passive mode transfers (some servers need this)
		ftp_pasv ($conn_id, true);
		 
		// perform file upload
//		$upload = ftp_put($conn_id, $ftp_file, $local_file, FTP_ASCII);
      $_timeOfDay = gettimeofday() ;
      $tmp_file = $ftp_file . '.' . $_timeOfDay['sec'] . $_timeOfDay['usec'] ;
      $upload = ftp_put($conn_id, $tmp_file, $local_file, FTP_BINARY);
        
      // check upload status:
      if (!$upload) {
         print('Cannot upload file ' . $local_file . ' to ftp ' . $ftp_file) ;
         logprintf(1, 'Cannot upload file ' . $local_file . ' to ftp ' . $ftp_file) ;
      } else {
         logprintf(4, 'upload file ' . $local_file . ' to ftp ' . $ftp_file) ;
         ftp_rename($conn_id, $tmp_file, $ftp_file) ;
      }
		
		// check upload status:
		if (!$upload) print('Cannot upload ' . ftp_file) ;
//		print (!$upload) ? 'Cannot upload: ' . $upload : 'Upload complete';
//		print "\n";
		 
		/*
		** Chmod the file (just as example)
		*/
		 
		// If you are using PHP4 then you need to use this code:
		// (because the "ftp_chmod" command is just available in PHP5+)
		if (!function_exists('ftp_chmod')) {
		   function ftp_chmod($ftp_stream, $mode, $filename){
				return ftp_site($ftp_stream, sprintf('CHMOD %o %s', $mode, $filename));
		   }
		}
		 
		// try to chmod the new file to 666 (writeable)
		if (ftp_chmod($conn_id, 0666, $ftp_path) !== false) {
			$_prut = 1 ;
//			print $ftp_path . " chmod successfully to 666<br>";
		} else {
			$_prut = 0 ;
//			print "could not chmod $file\n";
		}
		 


		// close the FTP stream
		ftp_close($conn_id);
		
//		print ('Order csv file succesfully created<br>') ;
		return 0 ;	
	}

?>
