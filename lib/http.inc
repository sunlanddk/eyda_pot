<?php

    function httpNoCache ($type=NULL) {
	// No Caching
	switch ($_SERVER['SERVER_PROTOCOL']) {
	    case 'HTTP/1.1':
		if ($type == 'pdf') {
		    header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT') ;
		    header ('Last-Modified: ' . gmdate('D, d M Y H:i:s', time() + 60*60) . ' GMT') ;
		    header ('Cache-Control: private, must-revalidate, post-check=0, pre-check=0') ;
		} else {
		    header ('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
		}
		break ;
		
	    case 'HTTP/1.0':
		header ('Pragma: no-cache') ; 
		break ;

	    default:
		sprintf ("%s(%d) invalid protocol, type %s<br>\n", __FILE__, __LINE__, $_SERVER['SERVER_PROTOCOL']) ;
	}
    }

    function httpContent ($type, $name, $size=NULL, $disposition='inline') {
	if (!is_null($size)) header ('Content-Length: '.$size) ; 	// Can't be used when compressing
	header ('Content-Type: ' . $type) ;
	header ('Content-Disposition: ' . $disposition . '; filename="' . $name . '"') ;
    }
?>
