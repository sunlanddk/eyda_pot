<?php
   function array_key_values($array, $key, $exclude=array()) {
      $_res = array();

      for($i = 0; $i < count($array); $i++) {
         if (!is_null($array[$i])) {
            foreach ($array[$i] as $_key => $_value) {
               if ($_key === $key) {
                  $_push = true;

                  foreach ($exclude as $_skey => $_sval) {
                     $_push = $_push && (array_key_exists($_skey, $array[$i]) ? $array[$i][$_skey] != $_sval : true);
                  }
                  if ($_push) {
                     array_push($_res, $_value);
                  }
               }
            }
         }
      }
      return $_res;
   }

   function array_assoc_key_values($array, $key, $value_key, $exclude=array()) {
      $_res = array();

      for($i = 0; $i < count($array); $i++) {
         if (!is_null($array[$i])) {
            foreach ($array[$i] as $_key => $_value) {
               if ($_key === $value_key) {
                  $_push = true;

                  foreach ($exclude as $_skey => $_sval) {
                     $_push = $_push && (array_key_exists($_skey, $array[$i]) ? $array[$i][$_skey] != $_sval : true);
                  }
                  if ($_push) {
                     $_res[$array[$i][$key]] = $_value;
                  }
               }
            }
         }
      }
      return $_res;
   }

   function array_get_index($array, $key, $value) {
      $_res = 0;

      for($i = 0; $i < count($array); $i++) {
         if (
            !is_null($array[$i])
            && array_key_exists($key, $array[$i])
            && (strtolower($array[$i][$key]) == strtolower($value)
               || (str_replace(" ", "_", strtolower($array[$i][$key]))) == strtolower($value))
         ) {
            $_res = $i;
            break;
         }
      }
      return $_res;
   }

?>