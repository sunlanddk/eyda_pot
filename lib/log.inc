<?php


define ('logFATAL', 0) ;
define ('logERROR', 1) ;
define ('logWARNING', 2) ;
define ('logINFO', 3) ;
define ('logTRANSACTION', 4) ;
define ('logMAIL', 5) ;
define ('logDUMP', 6) ;


$logQuerys = array () ;


function logSave ($query) {
    global $logQuerys ;
    $logQuerys[] = $query ;
    foreach ($logQuerys as $i => $query) {
	$r = dbRawQuery ($query) ;
	if ($r !== true) break ;
	unset ($logQuerys[$i]) ;
    }
}

function logLevelText ($level) {
    $leveltext = array (0 => "FATAL", 1 => "ERROR", 2 => "WARNING", 3 => "INFO", 4 => "TRANSACTION", 5 => "MAIL", 6 => 'DUMP') ;
    return $leveltext[$level] ;
}

function logPrintf ($level, $text) {
    $args = func_get_args() ;
    unset ($args[0]) ;
    unset ($args[1]) ;
    $text = vsprintf ($text, $args) ;

    if ($level == logMAIL) {
		$query = sprintf ("INSERT INTO LogMail SET Date='%s', Level=%d, Text='%s'", dbDateEncode(time()), $level, addslashes($text)) ;
	} else {
		$query = sprintf ("INSERT INTO Log SET Date='%s', Level=%d, Text='%s'", dbDateEncode(time()), $level, addslashes($text)) ;
	}
    global $Session ;
    if ($Session != 0) {
	$query .= sprintf (", SessionId=%d", $Session["Id"]) ;
    }

    global $User ;
    if ($User != 0) {
	$query .= sprintf (", UserId=%d", $User["Id"]) ;
    }

    global $Project ;
    if ($Project != 0) {
	$query .= sprintf (", ProjectId=%d", $Project["Id"]) ;
    }

    logSave ($query) ;
    
    if ($level <= logFATAL) {
	printf ("<b>%s:</b> ", logLevelText($level)) ;
	print ($text) ;
	print ("<br>\n") ;
    }
}

function logDump ($vars=array()) {
    global $Session, $Login, $Navigation, $Permission, $Project, $Process, $Activity, $User, $Id, $Record, $Objects, $startexec ;

    list($usec, $sec) = explode(" ",microtime());
    
    $vars['Exectime'] = ((float)$usec + (float)$sec - $startexec)*1000 ;
    $vars['Id'] = $Id ;
    $vars['Record'] = $Record ;
    $vars['ParentObject'] = $Objects[$Record['ParentId']] ;
    $vars['Navigation'] = $Navigation ;
    $vars['Project'] = $Project ;
    $vars['User'] = $User ;
    $vars['Permission'] = $Permission ;
    $vars['Login'] = $Login ;
    $vars['_POST'] = $_POST ;
    $vars['_GET'] = $_GET ;
    $vars['Activity'] = $Activity ;
    $vars['Process'] = $Process ;
    logPrintf (logDUMP, '%s', serialize($vars)) ;
}

function logPrintTime ($mark) {
    global $logTime, $startexec ;
    if ($logTime == 0) $logTime = $startexec ;
    list($usec, $sec) = explode(" ",microtime()) ; 
    $t = (float)$usec + (float)$sec ;
    printf ("stamp %s, last %d mS, start %d mS<br>\n", $mark, ($t - $logTime)*1000, ($t - $startexec)*1000) ;
    $logTime = $t ;
}

// Display any structured variable in html-format
function logPrintVar ($value, $name='', $level=0) {
    $type = gettype ($value) ;

    for ($n = 0 ; $n < $level ; $n++) printf ("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;") ;
    if ($name != '') printf ("%s: ", htmlentities($name)) ;
    
    switch ($type) {
	case 'array':
	    printf ("array<br>\n") ;
	    foreach ($value as $i => $v) {
		logPrintVar ($v, (string)$i, $level+1) ;
	    }
	    return ;
	case 'object':
	    printf ("object<br>\n") ;
	    $value = get_object_vars ($value) ;
	    foreach ($value as $i => $v) {
		logPrintVar ($v, (string)$i, $level+1) ;
	    }
	    return ;
    }

    // Special interperation
    switch ($name) {
	default:
	    if (is_null($value)) {
		print ('NULL') ;
	    } if (is_bool($value)) {
		print (($value) ? 'true' : 'false') ;
	    } else {
		$v = strval($value) ;
		if (strlen($v) > 20) $v = substr ($v, 0, 20) . '...' ;
		printf ("%s", htmlentities(strval($v))) ;
	    }
	    break ;
    }

    printf ("<br>\n") ;
}

?>
