<?php

// remove time-part of timestamp
function utsDateOnly ($t) {
//    $t = (int)$t + 12*60*60 - 1 ;
    $t = (int)($t / (24*60*60)) ;
    $t *= 24*60*60 ;
    return $t ;
}

function utsDateString ($s) {
    $t = (int)strtotime($s) ;
    if ($s != date('Y-m-d', $t)) return NULL ;
    return $t ;
}

function utsTimeString ($s) {
    $res = ereg ("^([0-9]{2})\:{0,1}([0-9]{2})$", $s, $val) ;
    if (!$res or $val[1] > 23 or $val[2] > 59) return NULL ;
    return 60*(60*(int)$val[1]+(int)$val[2]) ;
}

?>
