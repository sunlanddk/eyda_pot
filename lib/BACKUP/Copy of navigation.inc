<?php

    $navigationMarkCache = null ;

    function navigationEnable($mark) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Enable"] = true;
    }

    function navigationDisable($mark) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Enable"] = false;
    }

    function navigationPasive($mark) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Pasive"] = true;
    }

    function navigationSetParameter($mark, $value) {
	global $NavigationParam ;
	$NavigationParam[$mark]["Parameter"] = $value ;
    }    

    function navigationLink ($mark) {
    	global $Navigation, $navigationMarkCache ;
	if ($navigationMarkCache["Mark"] != $mark or $navigationMarkCache["ParentId"] != $Navigation["Id"]) {
	    // Get links
	    $navigationMarkCache = null ;	    
	    $link = array () ;
	    navigationItems ($link, $Navigation["Id"], "l", $mark) ;
	    $navigationMarkCache = current($link) ;
	}
	if (is_null($navigationMarkCache)) return 0 ;
	return (int)$navigationMarkCache['Id'] ;
    }

    function navigationMark ($mark) {
    	global $navigationMarkCache ;
	if ($navigationMarkCache["Mark"] != $mark) {
	    // Get links
	    $navigationMarkCache = null ;	    
	    $link = array () ;
	    navigationItems ($link, null, null, $mark) ;
	    $navigationMarkCache = current($link) ;
	}
	return (int)$navigationMarkCache['Id'] ;
    }
   
    function navigationOnClickLink ($mark, $id=0, $param=null) {
    	global $Navigation, $navigationMarkCache ;
	if ($navigationMarkCache["Mark"] != $mark or $navigationMarkCache["ParentId"] != $Navigation["Id"]) {
	    // Get links
	    $navigationMarkCache = null ;	    
	    $link = array () ;
	    navigationItems ($link, $Navigation["Id"], "l", $mark) ;
	    $navigationMarkCache = current($link) ;
	}
	if (is_null($navigationMarkCache)) return "style='cursor:default;'" ;
	$t = htmlOnClick ("appClick", $navigationMarkCache, $id, $param) ;
	if ($t == '') return "style='cursor:default;'" ;
	return $t . " style='cursor:pointer;'" ;
    }
   
        function navigationOnClickMark ($mark=null, $id=0, $param=null) {
	global $nid ;
  //added by ray ----
  global $navigationMarkCache; 
  navigationMark($mark); 
  if ($navigationMarkCache['Legacy'] || (is_null($mark))) {
    $html = "onclick='location.href=\"index.php?nid=" . ((is_null($mark)) ? $nid : navigationMark($mark)) ;
    }
  else {
    $html = "onclick='location.href=\"\\\itworx.passon.web\\\index.aspx?nid=" . ((is_null($mark)) ? $nid : navigationMark($mark)) ;
  }
  //------------------
  if ($id > 0) $html .= '&id='.$id ;
  if (!is_null($param)) $html .= '&'.$param ;
	$html .= "\"' style='cursor:pointer;'" ;
	return $html ;
    }


    function navigationCommandMark ($mark, $id=0) {
	global $Navigation, $navigationMarkCache ;

	// Any unexpected application messages
	if (headers_sent()) return 0 ;
	
	// Validate type og navigation
	if ($Navigation['Type'] != 'command') return sprintf ('navigationCommandViev invalid type "%s"', $Navigation['Type']) ;

	// Construct URL
	$nid = navigationMark ($mark) ;
	if ($nid == 0) return sprintf ('%s(%d) navigation ot found, mark \'%s\'', __FILE__, __LINE__, $mark) ;
	if ($navigationMarkCache['Legacy'])
		$href = 'index.php?nid=' . $nid ;
	else 
		$href = '\\\itworx.passon.web\\\index.aspx?nid='  . $nid ;
	if ($id > 0) $href .= '&id=' . $id ;

	// Redirect to new page	
	printf ("<script type='text/javascript'>\n") ;
	if (!$navigationMarkCache['LayoutPopup']) {
	    // The new page is not a popup, so close current window if it is a popup
	    printf ("if (window.name.substring(0,5)=='popup') {\n") ;
	    printf ("\tif (!window.opener.closed) {\n") ;
	    printf ("\t\twindow.opener.location.href='%s';\n", $href) ;
	    printf ("\t\twindow.opener.focus();\n") ;
	    printf ("\t}\n") ;
	    printf ("\twindow.close();\n") ;
	    printf ("} else \n") ;
	}
	printf ("\twindow.location.href='%s' ;\n", $href) ;
	printf ("</script>\n") ;

	return -1 ;
    }
    function NewnavigationCommandMark ($mark, $id=0) {
	global $Navigation, $navigationMarkCache ;

	// Any unexpected application messages
	if (headers_sent()) return 0 ;
	
	// Validate type og navigation
	if ($Navigation['Type'] != 'command') return sprintf ('navigationCommandViev invalid type "%s"', $Navigation['Type']) ;

	// Construct URL
	$nid = navigationMark ($mark) ;
	if ($nid == 0) return sprintf ('%s(%d) navigation ot found, mark \'%s\'', __FILE__, __LINE__, $mark) ;
	$href = 'index.php?nid=' . $nid ;
	if ($id > 0) $href .= '&id=' . $id ;

	// Redirect to new page	
	printf ("<script type='text/javascript'>\n") ;
	if (!$navigationMarkCache['LayoutPopup']) {
	    // The new page is not a popup, so close current window if it is a popup
	    printf ("if (window.name.substring(0,5)=='popup') {\n") ;
	    printf ("\tif (!window.opener.closed) {\n") ;
	    printf ("\t\twindow.opener.location.href='%s';\n", $href) ;
	    printf ("\t\twindow.opener.focus();\n") ;
	    printf ("\t}\n") ;
	    printf ("\twindow.close();\n") ;
	    printf ("} else \n") ;
	}
	printf ("\twindow.location.href='%s' ;\n", $href) ;
	printf ("</script>\n") ;

	return -1 ;
    }
?>
