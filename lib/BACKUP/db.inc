<?php

// Variables
$dbInsertId		= 0 ;
$dbRowsAffected	= 0 ;
$dbLogQuery		= array () ;

// Connect
if (($dbLink = mysql_connect($Config["dbHost"], $Config["dbUser"], $Config["dbPassword"])) == False) {
  printf ("<b>Error:</b> connecting to database, host %s, user %s, %s\n", $Config["dbHost"], $Config["dbUser"], mysql_error()) ;
  exit () ;
}
if (($dbConn = mysql_select_db($Config["dbName"], $dbLink)) == False) {
  printf ("<b>Error:</b> selecting database, host %s, user %s, name %s, %s\n", $Config["dbHost"], $Config["dbUser"], $Config["dbName"], mysql_error()) ;
  exit () ;
}

function dbRawQuery ($query) {
  return mysql_query ($query) ;
}

function dbQuery ($query) {
  $result = mysql_query ($query) ;
	
  if ($result == False) {
    require_once 'lib/log.inc' ;
    $text = mysql_error () ;
    mysql_query('UNLOCK TABLES') ;
    logPrintf (logFATAL, "dbQuery '%s', %s\n", $query, $text) ;
    logDump () ;
    exit () ;
  }

  if ($result === true) {
    global $Session, $User, $Project, $dbInsertId, $dbRowsAffected, $dbLogQuery ;
    $dbInsertId = mysql_insert_id () ;
    $dbRowsAffected = mysql_affected_rows() ;

    require_once 'lib/log.inc' ;
    logSave (sprintf('INSERT INTO Log SET Date="%s", Level=%d, Text="%s", SessionId=%d, UserId=%d, ProjectId=%d, Count=%d', dbDateEncode(time()), logTRANSACTION, addslashes($query), $Session['Id'], $User['Id'], $Project['Id'], mysql_affected_rows())) ;
  }
	
  return $result ;
}

function dbNumRows ($result) {
  global $dbRowsAffected ;
  if ($result === true) return $dbRowsAffected ;
  return mysql_num_rows ($result);
}

function dbFetch ($result) {
  return mysql_fetch_assoc ($result);
}

function dbQueryFree ($result) {
  if (isset ($result) and $result != false) {
    mysql_free_result($result) ;
  }
}

function dbInsertedId () {
  global $dbInsertId ;
  return $dbInsertId ;
}

function dbDateOnlyEncode ($time) {
  return gmdate ("Y-m-d", $time) ;
}

function dbDateEncode ($time) {
  return gmdate ("Y-m-d H:i:s", $time) ;
}

function dbDateDecode ($date) {
  if (is_null($date) || $date === '0000-00-00') return NULL ;
  if (ereg ("^([0-9]{4})-([0-9]{2})-([0-9]{2})$", $date, $regs)) {
    return gmmktime (0, 0, 0, (int)$regs[2], (int)$regs[3], (int)$regs[1]) ;
  }
  if (ereg ("([0-9]{4})-([0-9]{2})-([0-9]{2}) ([0-9]{2}):([0-9]{2}):([0-9]{2})", $date, $regs)) {
    return gmmktime ((int)$regs[4], (int)$regs[5], (int)$regs[6], (int)$regs[2], (int)$regs[3], (int)$regs[1]) ;
  }
  printf ("<br>Invalid dateformat in database<br>\n") ;
  exit ;
}

function dbRead($query) {
  $tab = array();
  $res = dbQuery($query);
  while($row = dbFetch($res)) {
    $tab[] = $row;
  }
  return $tab;
}

?>
