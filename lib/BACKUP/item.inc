<?php

    require_once 'lib/navigation.inc' ;

    function itemStart () {
	print "<table class=item>\n" ;
    }
   
    function itemSpace () {
	print "<tr><td class=itemspace></td></tr>\n" ;
    }

    function itemHeader ($h1=NULL, $t1=NULL, $t2=NULL) {
	if (!$t1) $t1 = 'Field' ;
	if (!$t2) $t2 = 'Content' ;
	if ($h1) {
	    printf ("<tr><td class=\"itemheader super\" colspan=2 >%s</td></tr>\n", htmlentities($h1)) ;
	    print "</table>\n<table class=item>\n" ;
	}
	print '<tr><td class=itemheader width=80>' . htmlentities($t1) . '</td><td class=itemheader>' . htmlentities($t2) . "</td></tr>\n" ;
	itemSpace () ;
    }

    function itemField ($name, $value, $param='') {
	$html = '<tr><td class=itemlabel>' . $name . '</td><td class=itemfield' ;
	if ($param != '') $html .= ' '.$param ;
	$html .= '>' . htmlentities($value) . "</td></tr>\n" ;
	print $html ;
    }

    function itemFieldRaw ($name, $value, $param='') {
	$html = '<tr><td class=itemlabel>' . htmlentities($name) . '</td><td' ;
	if ($param != '') $html .= ' '.$param ;
	$html .= '>' . $value . "</td></tr>\n" ;
	print $html ;
    }

    function itemFieldText ($name, $value, $param='') {
	$html = '<tr><td class=itemlabel>' . htmlentities($name) . '</td><td><p style="padding-right:8px;"' ;
	if ($param != '') $html .= ' '.$param ;
	$html .= '>' . str_replace('  ', '&nbsp;&nbsp;', nl2br(htmlentities($value))) . "</p></td></tr>\n" ;
	print $html ;
    }

    function itemFieldIcon ($name, $value, $icon, $mark, $id, $param=null) {
    	$html = '<tr>' ;
	$html .= '<td class=itemlabel style="vertical-align:bottom;">' . $name . '</td>' ;
	$html .= '<td class=itemfield><div style="display:inline;vertical-align:bottom;">' . htmlentities($value) . '</div>' ; 
        if ($id > 0) $html .= '<img class=list src="./image/toolbar/' . $icon . '" ' . navigationOnClickMark ($mark, $id, $param) . '>' ;
	$html .= "</td></tr>\n" ;
	print $html ;
    }
 
    function itemInfo ($row) {
	global $Id ;
	require_once 'lib/html.inc' ;
	print htmlItemInfo ($row) ;
    }

    function itemEnd () {
	print "</table>\n" ;
    }
?>
