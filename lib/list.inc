<?php

    require_once 'lib/navigation.inc';
    require_once 'lib/string.inc';
    require_once 'lib/fieldtype.inc';
    require_once 'lib/array.inc';

    define('LINES', 49);

    $listRowNo = 0;
    $listSort = 0;
    $listFilterField = 0;
    $listFilterValue = '';
    $listParam = '';
    $listOffset = 0;
    $listLines = 1000;
    $listField = array();
    $isFullFilter = ($Config['filterType'] === 'full' || $_GET['filterType'] === 'full');
    $filterValue = array();
    $listSettings = json_decode(str_replace('\\', '', $_COOKIE['listSettings']), true);
    $totalRecords = 0;

    function listLoad($fields, $queryFields, $queryTables, $queryClause, $queryOrder = '', $queryGroup = '', $usepaging = false)
    {
        global $Navigation, $Config;
        global $listField, $listSort, $listFilterField, $listFilterValue, $listOffset, $listLines, $listParam, $isFullFilter, $filterValue, $listSettings, $totalRecords;

        // Save list fields
        if ($fields) {
            $listField = $fields;
        }

        // Using offsets (look for the first button) ?
        $link = array();
        navigationItems($link, $Navigation['Id'], 't', 'first');
        if (count($link) > 0) {
            $row = current($link);
            if ($row['Disable'] == 0) {
                $usepaging = true;
                $listLines = LINES;
            }
        }

        $queryClause = listFiltering($listField, $queryClause);
        // Construct sortation
        $sort = listSorting($listField, $queryOrder);
        //logPrintTime ('point 3.1') ;


        if ($usepaging) {
            // Get Offset parameter for paging
            $listOffset = (int)$_GET['offset'];

            if ($queryGroup != '') {
                $queryGroupString = ' GROUP BY ' . $queryGroup;
            }
            $query = sprintf('SELECT COUNT(*) AS Count FROM (SELECT %s FROM %s WHERE %s %s) a', $queryFields, $queryTables, $queryClause, $queryGroupString);

            $result = dbQuery($query);
            $row = dbFetch($result);
            dbQueryFree($result);
            //logPrintTime ('point 3.2 ' ) ;

            $totalRecords = $row['Count'];

            // Last page ?
            if ($listOffset == -1) {
                $listOffset = $totalRecords - LINES;
                if ($listOffset < 0) {
                    $listOffset = 0;
                }
            }
        }


        // Query records to list
        $query = 'SELECT ' . $queryFields . ' FROM ' . $queryTables . ' WHERE ' . $queryClause;
        if ($queryGroup != '') {
            $query .= ' GROUP BY ' . $queryGroup;
        }
        if ($sort != '') {
            $query .= ' ORDER BY ' . $sort;
        }
        if ($usepaging) {
            $query .= ' LIMIT ' . $listOffset . ', ' . (LINES);
        }
        $result = dbQuery($query);
        //logPrintTime ('point 3.3') ;

        // Construct URL parameters
        $listParam = '';
        if ($listSort < 0 or $listSort > 1) {
            $listParam .= 'sort=' . $listSort . '&';
        }
        $listParam .= getFilterUri();

        if ($queryGroup != '') {
            $listParam .= 'group=1' . '&';
        }

        if ($usepaging) {
            // Handle pageing buttons
            if ($listOffset > 0) {
                if ($listOffset < LINES) {
                    $listOffset = LINES;
                }
                navigationSetParameter("first", $listParam);
                navigationSetParameter("previous", sprintf("%soffset=%d", $listParam, ((int)(($listOffset + ((LINES - ($listOffset % LINES)) % LINES) - LINES) / LINES) * LINES)));
            } else {
                navigationPasive("first");
                navigationPasive("previous");
            }

            if ($listOffset + LINES < $totalRecords) {
                navigationSetParameter("next", sprintf("%soffset=%d", $listParam, ((int)(($listOffset + LINES) / LINES) * LINES)));
                navigationSetParameter("last", $listParam . 'offset=-1');
            } else {
                navigationPasive("next");
                navigationPasive("last");
            }
        }

        setcookie("listSettings", json_encode($listSettings));
        return $result;
    }

    function listFiltering($listField, $queryClause = '')
    {
        global $isFullFilter, $filterValue, $listFilterField, $listFilterValue, $Navigation, $listSettings, $isAjax;

        // Get Filter parameter
        if ($isFullFilter) {
            $_sql = '';
            $isDirect = true;
            $filterValue = array();
            $_filter = array();
            $_clearFilter = false;

            if (isset($_GET['filter']) && $isAjax) {
                $_filter = $_GET['filter'];
                $_newFilter = array();
                foreach ($_filter as $_name => $_utfValue) {
                    $_value = mb_convert_encoding($_utfValue, "ISO-8859-1", "UTF-8");
                    $_index = array_get_index($listField, "name", $_name);
                    if ($_index > 0) {
                        if ($listField[$_index]['type'] == FieldType::SELECT) {
                            $_val = array('id' => $_value, 'value' => $_value != '' ? $listField[$_index]['options'][$_value] : '');
                        } else {
                            $_val = array('id' => '', 'value' => $_value);
                        }
                        $_newFilter[$_name] = $_val;
                    } else {
                        $_newFilter[$_name] = $_value;
                    }
                    $_filter = $_newFilter;
                }
                $_clearFilter = (int)$_filter['clearFilter'];
            } else {
                $_clearFilter = (int)$_GET['clearFilter'];
                foreach ($_GET as $_name => $_value) {
                    $_index = array_get_index($listField, "name", $_name);
                    if ($_index > 0) {
                        if ($listField[$_index]['type'] == FieldType::SELECT) {
                            $_val = array('id' => $_value, 'value' => $_value != '' ? $listField[$_index]['options'][$_value] : '');
                        } else {
                            $_val = array('id' => '', 'value' => $_value);
                        }
                        $_filter[$_name] = $_val;
                    }
                }
            }
            if ($_clearFilter == 1) {
                $listSettings['filter'] = array();
                $_filter = array();
            } else {
                if (count($_filter) > 0) {
                    $listSettings['filter'] = $_filter;
                } else {
                    if (isset($listSettings) && array_key_exists('filter', $listSettings)) {
                        $_filter = $listSettings['filter'];
                    }
                }
            }


            if ($_filter && count($_filter) > 0) {
                foreach ($_filter as $_name => $_value) {
                    $_index = array_get_index($listField, "name", $_name);
                    if ($_index > 0) {
                        if (!isset($listField[$_index]) or $listField[$_index]['nofilter']) {
                            //return sprintf ('%s(%d) invalid filter specification, value %d', __FILE__, __LINE__, $_index) ;
                            continue;
                        }
                        if ($listField[$_index]['type'] == FieldType::SELECT) {
                            $_val = stripslashes($_value['id']);
                        } else {
                            $_val = stripslashes($_value['value']);
                        }
                        if ($listField[$_index]['type'] == FieldType::SELECT) {
                            if (!array_key_exists($_val, $listField[$_index]['options'])) {
                                continue;
                            }
                        }


                        $isDirect = $isDirect && ($listField[$_index]['direct'] != '' and strpos($_val, '*') === false and strpos($_val, '%') === false);
                        if ($_val != '') {
                            $queryClauseItem = getFilterQuery($_index, $_val, null);
                            if ($queryClauseItem != '') {
                                $_sql .= ' ' . $queryClauseItem . ' AND';
                            }
                            $filterValue[$_index] = $_val;
                        }
                    }
                }
            }
            if ((strlen($_sql) > 0)) {
                $queryClause = $_sql . ' ' . ($isDirect ? '' : $queryClause);
            }
            if (endsWith($queryClause, 'AND')) {
                $queryClause = substr($queryClause, 0, -4);
            }
        } else {
            $listFilterField = (int)$_GET['field'];
            $listFilterValue = stripslashes($_GET['filter']);
            if ($listFilterField > 0) {
                if (!isset($listField[$listFilterField]) or $listField[$listFilterField]['nofilter']) {
                    return sprintf('%s(%d) invalid filter specification, value %d', __FILE__, __LINE__, $listFilterField);
                }
                if ($listFilterValue != '') {
                    $queryClause = getFilterQuery($listFilterField, $listFilterValue, $queryClause);
                }
            }
        }

        return $queryClause;
    }

    function listSorting($listField, $queryOrder = '')
    {
        global $listSort, $Config, $listSettings;

        $sort = '';
        // Get Sortation parameter
        if ($Config['newLayout'] && !isset($_GET['sort'])) {
            $listSort = array_get_index($listField, 'name', $listSettings['sortField']);

            if ($listSort <> 0 && $listField[$listSort]['nosort']) {
                $listSort = 0;
            }
            if ($listSettings['sortDir'] == "DESC") {
                $listSort = -$listSort;
            }
        } else {
            $listSort = (int)$_GET['sort'];
        }

        if ($listSort == 0) {
            // Find first sort field
            foreach ($listField as $i => $field) {
                if ($i == 0) {
                    continue;
                }
                if ($field['nosort']) {
                    continue;
                }
                $listSort = $field['desc'] ? -$i : $i;
                break;
            }
        } else {
            if (!isset($listField[abs($listSort)]) or $listField[abs($listSort)]['nosort']) {
                return sprintf('%s(%d) invalid sort specification, value %d', __FILE__, __LINE__, $listSort);
            } else {
                $listSettings['sortField'] = $listField[abs($listSort)]['name'];
                $listSettings['sortDir'] = ($listSort < 0 ? "DESC" : "ASC");
            }
        }


        if ($listSort != 0) {
            $sort .= $listField[abs($listSort)]['db'];
        }
        if ($listSort < 0) {
            $sort .= ' DESC';
        }
        if ($queryOrder != '') {
            if ($sort != '') {
                $sort .= ', ';
            }
            $sort .= $queryOrder;
        }

        return $sort;
    }

    function getFilterQuery($index, $value, $queryClause)
    {
        global $listField;
        $result = '';
        if (isset($listField[$index]['db'])) {
            if ($listField[$index]['direct'] != '' and strpos($value, '*') === false and strpos($value, '%') === false) {
                // Direct query without normal clause
                $result = sprintf(
                    '%s="%s" AND %s'
                    ,
                    $listField[$index]['db']
                    ,
                    addslashes($value)
                    ,
                    $listField[$index]['direct']
                );
            } else {
                $ftype = FieldType::STRING;
                if (isset($listField[$index]['type'])) {
                    $ftype = $listField[$index]['type'];
                }

                $operation = '=';
                if (isset($listField[$index]['operation'])) {
                    $operation = $listField[$index]['operation'];
                }

                switch ($ftype) {
                    case FieldType::DATE:
                        // Apply filter to date field
                        $result = sprintf(
                            'CAST((%s) AS DATE) %s "%s" %s'
                            ,
                            $listField[$index]['db']
                            ,
                            $operation
                            ,
                            addslashes(str_replace('*', '%', $value))
                            ,
                            isset($queryClause) ? ' AND ' . $queryClause : ''
                        );
                        break;
                    default:
                        // Apply filter together with normal clause
                        $result = sprintf(
                            'CAST((%s) AS CHAR) LIKE "%s" %s'
                            ,
                            $listField[$index]['db']
                            //		                , addslashes(str_replace('*', '%', $value))
                            ,
                            addslashes('%' . str_replace('*', '%', $value) . '%')
                            ,
                            isset($queryClause) ? ' AND ' . $queryClause : ''
                        );
                }
            }
        }

        return $result;
    }

    function getFilterUri()
    {
        global $listField, $isFullFilter, $filterValue, $listFilterField, $listFilterValue;

        $_result = '';
        if ($isFullFilter) {
            foreach ($filterValue as $_index => $_value) {
                $_result .= sprintf('ft%s=%s&', $_index, urlencode($_value));
            }
        } else {
            if ($listFilterField != 0) {
                $_result .= 'field=' . $listFilterField . '&';
            }
            if ($listFilterValue != '') {
                $_result .= 'filter=' . urlencode($listFilterValue) . '&';
            }
        }

        return $_result;
    }

    function listClear()
    {
        global $listField;
        $listField = array();
    }

    function listView($result, $mark = 'view')
    {
        global $Id, $Record, $Navigation, $listOffset, $listFilterValue, $User;

        // Single wiew ?
        if (dbNumRows($result) == 1 and $listOffset == 0 and $listFilterValue != '') {
            // Switch directly to the view page
            $row = dbFetch($result);
            dbQueryFree($result);
            $Id = $row['Id'];

            // Get Navigation
            $query = sprintf("SELECT * FROM Navigation WHERE Id=%d", navigationLink($mark));
            $result = dbQuery($query);
            $row = dbFetch($result);
            dbQueryFree($result);
            if (!$row) {
                return sprintf('%s(%d) navigation not found, id %d', __FILE__, __LINE__, navigationLink('view'));
            }
            $Navigation = array_merge($Navigation, $row);

            // Re-execute load module
            // Note: it is now executed within a function requiring all variables to be declared global !
            return include sprintf('module/%s/load.inc', $Navigation['Module']);
        } else {
            return 0;
        }
    }

    function listFilterBar()
    {
        global $Config, $Id, $nid, $smarty, $listField, $listFilterField, $listFilterValue, $listSort, $isFullFilter, $filterValue, $Navigation;

        // Find number for filter fields
        if (!isset($listField)) {
            return;
        }
        $n = 0;
        $fid = $listFilterField;
        foreach ($listField as $i => $field) {
            if (!$field['nofilter']) {
                $n++;
                if ($fid == 0) {
                    $fid = $i;
                }
            }
        }
        if ($n == 0) {
            return;
        }

        $ShowNorwayOption = ($nid == 2921 ? true : false );
         $Norway = (isset($_COOKIE['norway']) ? $_COOKIE['norway'] : true);

         $smarty->assign(array(
           'nid'         => $nid,
             'fid'         => $fid,
             'Id'          => $Id,
            'listSort'    => $listSort,
            'n'           => $n,
            'listField'   => $listField,
            'filterValues'=> $filterValue,
            'Navigation'  => $Navigation,
            'Norway'       => $Norway,
            'ShowNorwayOption' => $ShowNorwayOption
        ));

        if ($isFullFilter) {
            $smarty->display('lib/filter_full_compact.tpl');
        } else {
            // Filter toolbar
            printf("<div class=barsuper>\n");
            printf("<form method=GET name=filterform>\n");
            printf("<input type=hidden name=nid value=%d>\n", $nid);
            if ($Id > 0) {
                printf("<input type=hidden name=id value=%d>\n", $Id);
            }
            if ($listSort != 0) {
                printf("<input type=hidden name=sort value=%d>\n", $listSort);
            }

            printf("<table height=100%%>\n<tr>\n");
            if ($n > 1) {
                printf("<td class=barfield>Field</td>\n");

                printf("<td class=barfield style='padding-left:0px;padding-top:1px;padding-bottom:0px;''>\n");

                $selected = false;
                printf("<select size=1 name=field style='margin:0px;padding:0px;width=150px;'>\n");
                foreach ($listField as $i => $field) {
                    if (!$field) {
                        continue;
                    }
                    if ($field['nofilter']) {
                        continue;
                    }
                    if ($i == $fid) {
                        $selected = true;
                    }
                    printf("<option value=%d%s>%s\n", $i, ($i == $fid) ? ' selected' : '', htmlentities($field['name']));
                }
                if (!$selected) {
                    printf("<option selected value=0>- select -\n");
                }
                printf("</select>\n");

                printf("</td>\n");
            } else {
                printf("<input type=hidden name=field value=%d>\n", $fid);
            }

            printf("<td class=barfield>Filter</td>\n");
            printf("<td class=barfield style='padding-left:0px;padding-top:1px;padding-bottom:0px;''>\n");
            printf("<input type=text name=filter value=\"%s\" style='margin:0px;padding:0px;width=250px;'>\n", htmlentities($listFilterValue));
            printf("</td>\n");

            printf("</tr>\n");
            printf("</table>\n");
            printf("</form>\n");
            printf("</div>\n");
        }

        $smarty->clearAllAssign();
    }

    function leftFilterBar($verticallayout = false)
    {
        global $Config, $Id, $nid, $smarty, $listField, $listFilterField, $listFilterValue, $listSort, $isFullFilter, $filterValue;

        // Find number for filter fields
        if (!isset($listField)) {
            return;
        }
        $n = 0;
        $fid = $listFilterField;
        foreach ($listField as $i => $field) {
            if (!$field['nofilter']) {
                $n++;
                if ($fid == 0) {
                    $fid = $i;
                }
            }
        }
        if ($n == 0) {
            return;
        }

        $smarty->assign(array(
            'nid' => $nid,
            'fid' => $fid,
            'Id' => $Id,
            'verticallayout' => $verticallayout,
            'listSort' => $listSort,
            'n' => $n,
            'listField' => $listField,
            'filterValues' => $filterValue,
            'Navigation' => $Navigation
        ));

        $smarty->display('lib/filter_left.tpl');
        $smarty->clearAllAssign();
    }

    function listStart()
    {
        global $listRowNo;
        $listRowNo = 0;
        print "<table class=list>\n";
    }

    function listHeader($t)
    {
        global $listRowNo;
        if ($listRowNo != 0) {
            return;
        }
        printf("<tr><td class=\"itemheader super\" >%s</td></tr>\n", $t);
        print "</table>\n";
        print "<table class=list>\n";
    }

    function listRow($color = 0)
    {
        global $listRowNo;

        if ($listRowNo++ > 0) {
            print "</tr>\n";
        }

        $rowColor = is_string($color) ? $color : (($listRowNo % 2) == 1 ? '#FFFFFF' : ($color ? '#FFFFFF' : '#EEEEEE'));

        print '<tr class="list" bgcolor="' . $rowColor . '" style="background-color: ' . $rowColor . ';">';
    }

    function listHeadRaw($column = '', $width = 0, $param = '')
    {
        $html = '<td class=listhead';
        if ($width > 0) {
            $html .= ' width=' . $width;
        }
        if ($param != '') {
            $html .= ' ' . $param;
        }
        $html .= '>';
        $html .= '<p class=listhead';
        $html .= '>';
        $html .= $column;
        $html .= '</p>';
        $html .= '</td>';
        print $html;
    }

    function listHead($column = '', $width = 0, $param = '')
    {
        global $Id, $listField, $listFilterField, $listFilterValue, $listSort, $isFullFilter;

        // Locate field id
        $fid = 0;
        if (isset($listField)) {
            foreach ($listField as $i => $field) {
                if (strtolower($field['name']) == strtolower($column)) {
                    if (!$field['nosort']) {
                        $fid = $i;
                    }
                    break;
                }
            }
        }

        // Generate HTML
        $html = '<td class=listhead';
        if ($width > 0) {
            $html .= ' width=' . $width;
        }
        if ($param != '') {
            $html .= ' ' . $param;
        }
        if ($fid > 0) {
            $p = 'sort=' . (($listSort == $fid) ? -$fid : $fid) . '&' . getFilterUri();
            $html .= ' ' . navigationOnClickMark(null, $Id, $p);
        }
        $html .= '>';
        $html .= '<p class=listhead';
        if ($fid > 0) {
            $html .= ' style="cursor:pointer;"';
        }
        $html .= '>';
        $html .= $column;
        if ($fid > 0 and $fid == $listSort) {
            $html .= "<img class=list style='margin-bottom:2px;' src=./image/menu/sort.gif>";
        }
        if ($fid > 0 and $fid == -$listSort) {
            $html .= "<img class=list style='margin-bottom:2px;' src=./image/menu/sort_desc.gif>";
        }
        $html .= '</p>';
        $html .= '</td>';
        print $html;
    }

    function listHeadIcon($value = 23)
    {
        listHead('', $value);
    }

    function listField($value = '', $param = '', $wrapheight = 0)
    {
        $html = '<td class=list';
        if ($param != '') {
            $html .= ' ' . $param;
        }
        $html .= '>';
        if ($wrapheight > 0) {
            if ($wrapheight == 1) {
                $html .= '<p class=listwrap>' . htmlentities($value) . '</p>';
            } else {
                $html .= '<p class=listwrap>' . htmlentities($value) . '</p>';
            }
        } else {
            $html .= '<p class=list>' . htmlentities($value) . '</p>';
        }
        $html .= '</td>';
        print $html;
    }

    function listFieldRaw($value = '', $param = '')
    {
        $html = '<td class=list';
        if ($param != '') {
            $html .= ' ' . $param;
        }
        $html .= '>';
        $html .= $value;
        $html .= '</td>';
        print $html;
    }

    function listStyleIcon($mark, $item, $param = null)
    {
        global $Config;
        $image_link = sprintf("%s/thumbnails/%s_%s_mini.jpg", $Config['imageStore'], $item['ArticleNumber'], $item['ColorNumber']);
        if (!file_exists($image_link)) {
            $image_link = "image/no_image.png";
        }
        printf('<td class=list %s><img class="orderlineStyle" class=list src="%s"></td>', navigationOnClickLink($mark, (int)$item['Id'], $param), $image_link);
    }

    function listFieldIcon($icon, $mark, $id = 0, $param = null)
    {
        printf('<td class=list %s><img class=list src="./image/toolbar/%s"></td>', navigationOnClickLink($mark, $id, $param), $icon);
    }

    function listEnd()
    {
        print "</tr>\n</table>\n";
    }
