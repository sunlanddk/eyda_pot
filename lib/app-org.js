
var appFocusField = null ;

function appFocus (name) {
    appFocusField = name ;
}

function appBack(back) {
    if ((back==0) && (window.name.substring(0,5) == 'popup')) {
	if (!window.opener.closed) {
	    window.opener.focus() ;
	}
	window.close() ;
    } else {
	if (back==0) back=1;
	history.go(-back) ;
    }
}

function appCommandEnd(back) {
    if ((back<=0)&&(window.name.substring(0,5)=='popup')) {
	if (!window.opener.closed) {
	    if (back==0) {
		// reload current page without reloading all referanced items
		href=window.opener.location.href ;
		window.opener.location.replace(href) ;
	    } else {
		window.opener.history.go(back);
	    }
	    window.opener.focus();
	}
	window.close();
    } else {
	history.go(-back);
    }
}

function appSubmit(nid,id,confirm) {
    var form = document.appform ;

    if (confirm != null) {
	if (!window.confirm(confirm)) return false ;
    }

    if (!form) {
	form = document.createElement("FORM");
	form.method = "POST" ;
	form.name = "appform" ;
	document.body.appendChild(form) ;

	var field = document.createElement("INPUT") ;
	field.name = "nid" ;
	field.type = "hidden" ;
	field.value = nid ;
	form.appendChild(field) ;

	if (id > 0) {
	    var field = document.createElement("INPUT") ;
	    field.name = "id" ;
	    field.type = "hidden" ;
	    field.value = id ;
	    form.appendChild(field) ;
	}
    } else {
	form.nid.value = nid ;
	if (id > 0) form.id.value = id ;
    }

    var cookie = "field=" ;
    cookie += escape(window.location.search) + ','
    for (j = 0 ; j < form.elements.length ; j++) {
	var element = form.elements[j] ;
	if (j > 0) cookie += ","
	switch (element.type) {
	    case 'text' :
	    case 'file' :
	    case 'hidden' :
	    case 'textarea' :
		cookie += escape(element.value) ;
		break ;

	    case 'checkbox' :
		cookie += (element.checked) ? 1 : 0 ;
		break ;

	    case 'select-one' :
		cookie += parseInt(element.value) ;
		break ;

	    default :
		cookie += 'x' ;
		break ;
	}
    }
    cookie += '; path=/' ;
    cookie += '; domain=' + window.location.hostname ;
    var expires = new Date() ;
    expires.setTime(expires.getTime() + 10*60*1000) ;
    cookie += '; expires=' + expires.toGMTString() ;
    document.cookie = cookie ;

    form.submit() ;
    return true;
}

function appLoad (nid,id,param,width,height) {
    var href = "index.php?nid="+nid ;
    if (id > 0) href += "&id="+id ;
    if (param != null) href += "&"+param ;
    var now = new Date () ;
    href += '&inst=' + now.getTime() ;

    if (width > 0 && height > 0) {
	var LeftPosition=(screen.width)?(screen.width-width)/2:100;
	var TopPosition=(screen.height)?(screen.height-height)/2:100;
	var settings='width='+width+',height='+height+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes,dependent=yes';
	var win=window.open(href,'popup_win'+nid,settings);
    } else {
	location.href=href ;
    }
}

function appLoadLegacy (legacy,nid,id,param,width,height) {
    var href ="";
    if (legacy) {
        href = "/index.php?nid="+nid ;
    } else {
        href="/itworx.passon.web/index.aspx?nid="+nid;
    }
    if (id > 0) href += "&id="+id ;
    if (param != null) href += "&"+param ;
    var now = new Date () ;
    href += '&inst=' + now.getTime() ;

    if (width > 0 && height > 0) {
	var LeftPosition=(screen.width)?(screen.width-width)/2:100;
	var TopPosition=(screen.height)?(screen.height-height)/2:100;
	var settings='width='+width+',height='+height+',top='+TopPosition+',left='+LeftPosition+',scrollbars=yes,location=no,directories=no,status=yes,menubar=no,toolbar=no,resizable=yes,dependent=yes';
	var win=window.open(href,'popup_win'+nid,settings);
    } else {
	location.href=href ;
    }
}
function appClick(event,func,id,param) {
	var result;
    eval("result = " + func);
    return result;
}

function appLoaded () {
    window.focus() ;

    if (appFocusField) {
	var collection = document.getElementsByName (appFocusField) ;
	var element = collection[0] ;
	if (element) {
	    element.focus () ;
	    if (element.select) element.select () ;
	}
    }
}

function appRestore() {
    // Get field cookie
    var cookie = 'field=' ;
    var posStart = document.cookie.indexOf(cookie) ;
    if (posStart != -1) {
	posStart += cookie.length ;
	var posEnd = document.cookie.indexOf(';', posStart) ;
	if (posEnd == -1) posEnd = document.cookie.length ;
	var fields = document.cookie.substring(posStart, posEnd).split(',') ;
	
	if (fields.length > 1) {
	    if (unescape(fields.shift()) == window.location.search) {
		var form = document.appform ;
		if (form) {
		    if (fields.length == form.length) {
			for (j = 0 ; j < form.elements.length ; j++) {
			    var element = form.elements[j] ;

			    switch (element.name) {
				case 'id' :
				case 'nid' :
				    continue ;
			    }

			    switch (element.type) {
				case 'text' :
				case 'file' :
				case 'textarea' :
				    element.value = unescape(fields[j]) ;
				    break ;

				case 'checkbox' :
				    element.checked = (fields[j] == '1') ;
				    break ;

				case 'select-one' :
				    element.value = parseInt(fields[j]) ;
				    break ;
			    }
			}
		    }
		}
	    }
	}

	// Delete cookie
	var cookie = 'field=expired' ;
	cookie += '; path=/' ;
	cookie += '; domain=' + window.location.hostname ;
	var expires = new Date() ;
	expires.setTime(expires.getTime() - 24*60*60*1000) ;
	cookie += '; expires=' + expires.toGMTString() ;
	document.cookie = cookie ;
    }
}
