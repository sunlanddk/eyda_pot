
const url = weburl; 
const appstore = new Vue({
	el: "#appStore",
	data: {
		str: '',
		position : '',
		message: '',
		done: '',
		donesecond: '',
		url: url,
		max: 1,
		type: 'gtin',
		preloader: false
	},
	beforeMount(){
		var vm = this;
		// var getItems = getData('http://aub.dk/aub/sfmodule/sw/api/shipping/testalex');
		// vm.getAvailablePosition();
		window.addEventListener("keyup", function(e) {
			if (e.keyCode == 8 || e.keyCode == 46) {
				vm.str = vm.str.substring(0, vm.str.length - 1);
			}
		});
		window.addEventListener("keypress", function(e) {
			vm.message = '';
			vm.done = '';
			vm.donesecond = '';
      		var key = e.which;
	        if (key==13) {
	        	if(vm.str.substring(0,6) == '99SEND'){
	        		if(vm.str != ''){
	        			vm.addPosition();
	        		}
	        		else{
	        			vm.message = 'Please scan a position.';
	        		}	        
	        		vm.str = '';
	        		return;
	        	}
	        	vm.position = vm.str;
	        	vm.str = '';
	        }
	        else{
	        	vm.str += String.fromCharCode(key);
	        }
    	});
	},
	methods: {
		removeitem: function (index){
			this.items.splice(index, 1);
		},
		removetempitem: function (index){
			this.items.splice(index, 1);
		},
	    testfunction: function (item, index) {
	      console.log(item);
	      console.log(index);
	    },
	    addPosition(){
	    	var vm = this;
	    	vm.preloader = true;

	    	var form = {position: vm.position, max: vm.max, delete: 0, type: vm.type};
	    	vm.postData(url+'shipping/stocklayout/update/position', form, vm.addPositionResponse);
	    },
	    addPositionResponse(response){
	    	console.log(response);
	    	var vm = this;
	    	vm.preloader = false;
	    	if(response == 'You do not have permission to this route'){
	    		vm.message = 'You do not have permission to this route';
	    		vm.position = '';
	    		vm.str = '';  
	    		return   		
	    	}
	    	
	    	if(response.error == true){
	    		vm.message = response.errorcode;
	    		vm.str = '';
	    	}
	    	else{
	    		vm.done = 'Position Added to stock, please scan new position.';
	    		vm.message = '';
	    		vm.position = '';
	    		vm.str = '';
	    	}
	    },
	    postData(url, data, callback){
	 		var vm = this;
			$.ajax({
			    url : url,
			    type: "POST",
			    data : JSON.stringify(data),
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			    	console.log('Error: ');
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		},
	    getData(url, callback){
			$.ajax({
			    url : url,
			    type: "GET",
			    success: function(data, textStatus, jqXHR)
			    {
			    	// console.log(data);
			    	callback(data);
			     	// return data;
			    },
			    error: function (jqXHR, textStatus, errorThrown)
			    {
			 		console.log(jqXHR);
			 		console.log(textStatus);
			 		console.log(errorThrown);
			    }
			});

			return;
		}
	    
	  }
});






// function getData(url){
	 
// 	$.ajax({
// 	    url : url,
// 	    type: "GET",
// 	    success: function(data, textStatus, jqXHR)
// 	    {
// 	     	console.log(data);
// 	    },
// 	    error: function (jqXHR, textStatus, errorThrown)
// 	    {
// 	 		console.log(jqXHR);
// 	 		console.log(textStatus);
// 	 		console.log(errorThrown);
// 	    }
// 	});

// 	return;
// }

// function postData(url, data){
	 
// 	$.ajax({
// 	    url : url,
// 	    type: "POST",
// 	    data : data,
// 	    success: function(data, textStatus, jqXHR)
// 	    {
// 	     	console.log(data);
// 	    },
// 	    error: function (jqXHR, textStatus, errorThrown)
// 	    {
// 	 		console.log(jqXHR);
// 	 		console.log(textStatus);
// 	 		console.log(errorThrown);
// 	    }
// 	});

// 	return;
// }


// $(document).ready(function(){

// 	var str = '';
// 	$(document).keypress(function(e){
// 		var key = e.which;
// 		str += String.fromCharCode(key);
//         if (key==13) {
//             console.log(str);
//             str = '';
//         }
// 	});
// });