<?php

    require_once 'lib/perm.inc' ;

    function saveFields ($tablename, $id, &$fields, $update=false) {
	global $Permission, $User, $Project, $Navigation, $Record ;

	// Verify user login
	if (!$User) return "You must login to save any information" ;

	// Get current record
	if ($id > 0) {
	    $query = sprintf ("SELECT * FROM `%s` WHERE Id=%d", $tablename, $id) ;
	    $result = dbQuery ($query) ;
	    $db = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	}

	// Permission - double check !!
	if (!permAdminSystem() and !permAdminProject()) {
	    if ($id > 0) {
		if (!$Permission['PermWrite']) return "You do not have permission to modify this record (save, step 1)" ;
		if ($tablename == 'User') {  // Special case for the user table
		    if (!$Permission['PermWriteOther'] and ($User['Id'] != $db['Id'])) return "You do not have permission to modify this record (save, step 2)" ;
		} else {
		    if (!$Permission['PermWriteOther'] and ($User['Id'] != $db['CreateUserId'])) return "You do not have permission to modify this record (save, step 3)" ;
		}
	    } else {
		if (!$Permission['PermCreate'] and ($tablename <> 'OrderQuantity')) return "You do not have permission to create this record (save, step 4)" ;
	    }
	}

	$n = 0 ;
	$sqlfields = '' ;
	foreach ($fields as $fieldname => $field) {
	    $fieldtype = (isset($field['type'])) ? $field['type'] : gettype($_POST[$fieldname]) ;

	    switch ($fieldtype) {
		case 'checkbox':
		    $fieldvalue = ($_POST[$fieldname] == 'on') ? 1 : 0 ;
		    break ;

		case 'string':
		    $fieldvalue = trim(stripslashes($_POST[$fieldname])) ;
		    break ;

		case 'integer':
		    if (!isset($_POST[$fieldname]) or ($_POST[$fieldname] == '')) {
		       $fieldvalue = NULL ;
		       break ;
		    }
		    $fieldvalue = (int)$_POST[$fieldname] ;
		    $s = ltrim(trim($_POST[$fieldname]), '0') ;
		    if ($s == '') $s = '0' ;
		    if ((string)$fieldvalue !== $s) return sprintf ("invalid value in int field '%s'", $fieldname) ;
		    break ;

		case 'decimal' :
		    // Get field
		    $fieldvalue = trim($_POST[$fieldname]) ;
		    if ($fieldvalue == '') $fieldvalue = '0' ;

		    // Get and check format specification
		    if (!ereg ("^([0-9]{1,2})\.([0-9]{1})$", ($field['format']), $spec)) {
			return sprintf ("invalid decimal format specification, field %s, format %s", $fieldname, $field['format']) ;
		    }

		    // Generat and verify format
		    $format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", $spec[1]-$spec[2], $spec[1]-$spec[2], $spec[2]) ;
		    if (!ereg($format,$fieldvalue)) return sprintf ("invalid value in dec field '%s' %f", $fieldname, $fieldvalue) ;

		    // Substitute , -> .
		    $fieldvalue = str_replace (',', '.', $fieldvalue) ;
		    break ;

		case 'date' :
		    $fieldvalue = trim($_POST[$fieldname]) ;
		    if ($fieldvalue == '') {
			$fieldvalue = NULL ;
			break ;
		    }
		    $t = strtotime($fieldvalue) ;
//printf ("field '%s', date '%s'", $fieldvalue, date("Y-m-d", strtotime($fieldvalue))) ;
		    if ($fieldvalue != date('Y-m-d', $t)) return sprintf ("invalid date in field '%s'", $fieldname) ;
// 		    $fieldvalue = dbDateEncode ($t) ;     Do not use timezone for dates
		    break ;

		case 'internal':
		    switch ($fieldname) {
			case 'ProjectId':
			    global $Project ;
			    $fieldvalue = (int)$_POST[$fieldname] ;
			    if (!permAdminSystem() and $fieldvalue != $Project['Id']) return 'access only to selected project' ;
			    if ($id > 0 and $db['ProjectId'] != $fieldvalue) return 'project Id can not be changed' ;
			    break ;
			default:
			    return sprintf ("%s(%d), invalid internal field, name '%s'", __FILE__, __LINE__, $fieldname) ;
		    }
		    break ;

		case 'ignore' :
		    continue ;

	    case 'set' :
    		    $fieldvalue = $fields[$fieldname]['value'] ;
		    break ;

	    case 'nullable':
	      $fieldvalue = $fields[$fieldname]['value'] ;
		    break ;

		case "NULL":
		    $fieldvalue = NULL ;
		    break ;

		default:
		    return sprintf ("%s(%d), invalid type of field, type '%s', field '%s'", __FILE__, __LINE__, $fieldtype, $fieldname) ;
	    }

	    $fields[$fieldname]['value'] = $fieldvalue ;
	    $fields[$fieldname]['db'] = $db[$fieldname] ;

//printf ("id %d, name %s, type %s, value %s, db '%s'<br>\n", $id, $fieldname, $fieldtype, (is_null($fieldvalue))?"NULL":"'".$fieldvalue."'", $db[$fieldname]) ;

	    if ($field['check']) {
		$res = checkfield ($fieldname, $fieldvalue, ($db[$fieldname] != $fieldvalue) or ($id <= 0)) ;
		if (gettype($res) == 'string') return $res ;
		if ($res === false) continue ;
	    }

	    $fieldvalue = $fields[$fieldname]['value'] ;

	    if ($id <= 0) {
		// New record
		if (is_null($fieldvalue) or $fieldvalue === "") {
		    if ($field["mandatory"]) return sprintf ("field '%s' must be specified", $fieldname) ;
		    continue ;
		}
	    } else {
		// Update record
		if (is_null($fieldvalue) && $fieldtype!=='date' && $fieldtype!== 'nullable') continue ;
		if ($db[$fieldname] == $fieldvalue) continue ;
	    }

	    switch ($field['permission']) {
		case 'admin' :
		    if (!permAdminSystem()) return "You do not have permission to modify this record (save, step 5) ".$fieldname ;
		    break ;

		case 'proadmin' :
		    if (!permAdminSystem() and !$permAdminProject()) return "You do not have permission to modify this record (save, step 6)" ;
		    break ;

		case 'function' :
		    if ($field['mark'] != $Navigation['FunctionMark']) return "You do not have permission to modify this record (save, step 7)" ;
		    break ;

		case NULL :
		    break ;

		default :
		    return sprintf ("%s(%d), invalid permission, field '%s', parameter '%s'", __FILE__, __LINE__, $fieldname, $field["permission"]) ;
	    }

	    // Build SQL statement
	    if (is_null($fieldvalue)) {
	      $sqlfields .= sprintf ('`%s`= NULL,', $fieldname) ;
	    } else {
	      $sqlfields .= sprintf ('`%s`="%s",', $fieldname, addslashes($fieldvalue)) ;
	    }
	    $n++ ;

	    // Save fieldvalue in current Record
	    if ($update) $Record[$fieldname] = $fieldvalue ;
	}

	// Validate
	if (function_exists('saveValidate')) {
	    $res = saveValidate ($n > 0) ;
	    if (gettype($res) == 'string') return $res ;
	    if ($res === false) return 0 ;
	}

	// Any changes ?
	if ($n == 0) return 0 ;

	$t = dbDateEncode(time()) ;
	if ($id <= 0) {
	    $query = sprintf ('INSERT INTO `%s` SET %s Active=1, CreateDate="%s", CreateUserId=%d, ModifyDate="%s", ModifyUserId=%d', $tablename, $sqlfields, $t, $User["Id"], $t, $User["Id"]) ;
	    if ($update) {
		$Record['CreateDate'] = $t ;
		$Record['CreateUserId'] = $User['Id'] ;
	    }
	} else {
	    $query = sprintf ('UPDATE `%s` SET %s ModifyDate="%s", ModifyUserId=%d WHERE Id=%d', $tablename, $sqlfields, $t, $User["Id"], $id) ;
	}
	if ($update) {
	    $Record['ModifyDate'] = $t ;
	    $Record['ModifyUserId'] = $User['Id'] ;
	}

	if (!is_null($tablename)) {
	    dbQuery ($query) ;
	    if ($id <= 0 and $update) $Record['Id'] = dbInsertedId () ;
	}

//printf ("%s<br>\n", $query) ;
//return "halt" ;

	return 0 ;
    }

    function saveFieldsStrict ($tablename, $id, &$fields, $update=false) {
	global $Permission, $User, $Project, $Navigation, $Record ;

	// Verify user login
	if (!$User) return "You must login to save any information" ;

	// Get current record
	if ($id > 0) {
	    $query = sprintf ("SELECT * FROM `%s` WHERE Id=%d", $tablename, $id) ;
	    $result = dbQuery ($query) ;
	    $db = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	}

	// Permission - double check !!
	if (!permAdminSystem() and !permAdminProject()) {
	    if ($id > 0) {
		if (!$Permission['PermWrite']) return "You do not have permission to modify this record (save, step 1)" ;
		if ($tablename == 'User') {  // Special case for the user table
		    if (!$Permission['PermWriteOther'] and ($User['Id'] != $db['Id'])) return "You do not have permission to modify this record (save, step 2)" ;
		} else {
		    if (!$Permission['PermWriteOther'] and ($User['Id'] != $db['CreateUserId'])) return "You do not have permission to modify this record (save, step 3)" ;
		}
	    } else {
		if (!$Permission['PermCreate'] and ($tablename <> 'OrderQuantity')) return "You do not have permission to create this record (save, step 4)" ;
	    }
	}

	$n = 0 ;
	$sqlfields = '' ;
	foreach ($fields as $fieldname => $field) {
	    $fieldtype = (isset($field['type'])) ? $field['type'] : gettype($_POST[$fieldname]) ;

	    switch ($fieldtype) {
		case 'checkbox':
		    $fieldvalue = ($_POST[$fieldname] == 'on') ? 1 : 0 ;
		    break ;

		case 'string':
		    $fieldvalue = trim(stripslashes($_POST[$fieldname])) ;
		    break ;

		case 'integer':
		    if (!isset($_POST[$fieldname]) or ($_POST[$fieldname] == '')) {
		       $fieldvalue = NULL ;
		       break ;
		    }
		    $fieldvalue = (int)$_POST[$fieldname] ;
		    $s = ltrim(trim($_POST[$fieldname]), '0') ;
		    if ($s == '') $s = '0' ;
		    if ((string)$fieldvalue !== $s) return sprintf ("invalid value in int field '%s'", $fieldname) ;
		    break ;

		case 'decimal' :
		    // Get field
		    $fieldvalue = trim($_POST[$fieldname]) ;
		    if ($fieldvalue == '') $fieldvalue = '0' ;

		    // Get and check format specification
		    if (!ereg ("^([0-9]{1,2})\.([0-9]{1})$", ($field['format']), $spec)) {
			return sprintf ("invalid decimal format specification, field %s, format %s", $fieldname, $field['format']) ;
		    }

		    // Generat and verify format
		    $format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", $spec[1]-$spec[2], $spec[1]-$spec[2], $spec[2]) ;
		    if (!ereg($format,$fieldvalue)) return sprintf ("invalid value in dec field '%s' %f", $fieldname, $fieldvalue) ;

		    // Substitute , -> .
		    $fieldvalue = str_replace (',', '.', $fieldvalue) ;
		    break ;

		case 'date' :
		    $fieldvalue = trim($_POST[$fieldname]) ;
		    if ($fieldvalue == '') {
			$fieldvalue = NULL ;
			break ;
		    }
		    $t = strtotime($fieldvalue) ;
//printf ("field '%s', date '%s'", $fieldvalue, date("Y-m-d", strtotime($fieldvalue))) ;
		    if ($fieldvalue != date('Y-m-d', $t)) return sprintf ("invalid date in field '%s'", $fieldname) ;
// 		    $fieldvalue = dbDateEncode ($t) ;     Do not use timezone for dates
		    break ;

		case 'internal':
		    switch ($fieldname) {
			case 'ProjectId':
			    global $Project ;
			    $fieldvalue = (int)$_POST[$fieldname] ;
			    if (!permAdminSystem() and $fieldvalue != $Project['Id']) return 'access only to selected project' ;
			    if ($id > 0 and $db['ProjectId'] != $fieldvalue) return 'project Id can not be changed' ;
			    break ;
			default:
			    return sprintf ("%s(%d), invalid internal field, name '%s'", __FILE__, __LINE__, $fieldname) ;
		    }
		    break ;

		case 'ignore' :
		    continue ;

	    case 'set' :
    		    $fieldvalue = $fields[$fieldname]['value'] ;
		    break ;

	    case 'nullable':
	      $fieldvalue = $fields[$fieldname]['value'] ;
		    break ;

		case "NULL":
		    $fieldvalue = NULL ;
		    break ;

		default:
		    return sprintf ("%s(%d), invalid type of field, type '%s', field '%s'", __FILE__, __LINE__, $fieldtype, $fieldname) ;
	    }

	    $fields[$fieldname]['value'] = $fieldvalue ;
	    $fields[$fieldname]['db'] = $db[$fieldname] ;

//printf ("id %d, name %s, type %s, value %s, db '%s'<br>\n", $id, $fieldname, $fieldtype, (is_null($fieldvalue))?"NULL":"'".$fieldvalue."'", $db[$fieldname]) ;

	    if ($field['check']) {
		$res = checkfield ($fieldname, $fieldvalue, ($db[$fieldname] != $fieldvalue) or ($id <= 0)) ;
		if (gettype($res) == 'string') return $res ;
		if ($res === false) continue ;
	    }

	    $fieldvalue = $fields[$fieldname]['value'] ;

	    if ($id <= 0) {
		// New record
		if (is_null($fieldvalue) or $fieldvalue === "") {
		    if ($field["mandatory"]) return sprintf ("field '%s' must be specified", $fieldname) ;
		    continue ;
		}
	    } else {
		// Update record
		if (is_null($fieldvalue) && $fieldtype!=='date' && $fieldtype!== 'nullable') continue ;
		if ($db[$fieldname] === $fieldvalue) continue ;
	    }

	    switch ($field['permission']) {
		case 'admin' :
		    if (!permAdminSystem()) return "You do not have permission to modify this record (save, step 5) ".$fieldname ;
		    break ;

		case 'proadmin' :
		    if (!permAdminSystem() and !$permAdminProject()) return "You do not have permission to modify this record (save, step 6)" ;
		    break ;

		case 'function' :
		    if ($field['mark'] != $Navigation['FunctionMark']) return "You do not have permission to modify this record (save, step 7)" ;
		    break ;

		case NULL :
		    break ;

		default :
		    return sprintf ("%s(%d), invalid permission, field '%s', parameter '%s'", __FILE__, __LINE__, $fieldname, $field["permission"]) ;
	    }

	    // Build SQL statement
	    if (is_null($fieldvalue)) {
	      $sqlfields .= sprintf ('`%s`= NULL,', $fieldname) ;
	    } else {
	      $sqlfields .= sprintf ('`%s`="%s",', $fieldname, addslashes($fieldvalue)) ;
	    }
	    $n++ ;

	    // Save fieldvalue in current Record
	    if ($update) $Record[$fieldname] = $fieldvalue ;
	}

	// Validate
	if (function_exists('saveValidate')) {
	    $res = saveValidate ($n > 0) ;
	    if (gettype($res) == 'string') return $res ;
	    if ($res === false) return 0 ;
	}

	// Any changes ?
	if ($n == 0) return 0 ;

	$t = dbDateEncode(time()) ;
	if ($id <= 0) {
	    $query = sprintf ('INSERT INTO `%s` SET %s Active=1, CreateDate="%s", CreateUserId=%d, ModifyDate="%s", ModifyUserId=%d', $tablename, $sqlfields, $t, $User["Id"], $t, $User["Id"]) ;
	    if ($update) {
		$Record['CreateDate'] = $t ;
		$Record['CreateUserId'] = $User['Id'] ;
	    }
	} else {
	    $query = sprintf ('UPDATE `%s` SET %s ModifyDate="%s", ModifyUserId=%d WHERE Id=%d', $tablename, $sqlfields, $t, $User["Id"], $id) ;
	}
	if ($update) {
	    $Record['ModifyDate'] = $t ;
	    $Record['ModifyUserId'] = $User['Id'] ;
	}

	if (!is_null($tablename)) {
	    dbQuery ($query) ;
	    if ($id <= 0 and $update) $Record['Id'] = dbInsertedId () ;
	}

//printf ("%s<br>\n", $query) ;
//return "halt" ;

	return 0 ;
    }

    function saveFields2 ($tablename, $id, &$fields, $update=false) {
	global $Permission, $User, $Project, $Navigation, $Application, $isAjax ;

	// Verify user login
	if (!$User) return "You must login to save any information" ;

	// Get current record
	if ($id > 0) {
	    $query = sprintf ("SELECT * FROM `%s` WHERE Id=%d", $tablename, $id) ;
	    $result = dbQuery ($query) ;
	    $db = dbFetch ($result) ;
	    dbQueryFree ($result) ;
	}

	// Permission - double check !!
	if (!permAdminSystem() and !permAdminProject()) {
	    if ($id > 0) {
		if (!$Permission['PermWrite']) return "You do not have permission to modify this record (save, step 1)" ;
		if ($tablename == 'User') {  // Special case for the user table
		    if (!$Permission['PermWriteOther'] and ($User['Id'] != $db['Id'])) return "You do not have permission to modify this record (save, step 2)" ;
		} else {
		    if (!$Permission['PermWriteOther'] and ($User['Id'] != $db['CreateUserId'])) return "You do not have permission to modify this record (save, step 3)" ;
		}
	    } else {
		if (!$Permission['PermCreate']) return "You do not have permission to create this record (save, step 4)" ;
	    }
	}

	$n = 0 ;
	$sqlfields = '' ;
	foreach ($fields as $fieldname => $field) {
	    $fieldtype = (isset($field['type'])) ? $field['type'] : gettype($_POST[$fieldname]) ;

	    switch ($fieldtype) {
		case 'checkbox':
		    $fieldvalue = ($_POST[$fieldname] == 'on') ? 1 : 0 ;
		    break ;

		case 'string':
		    $fieldvalue = trim(stripslashes($isAjax ? utf8_decode($_POST[$fieldname]) : $_POST[$fieldname])) ;

		    break ;

		case 'integer':
		    if (!isset($_POST[$fieldname]) or ($_POST[$fieldname] == '')) {
		       $fieldvalue = NULL ;
		       break ;
		    }
		    $fieldvalue = (int)$_POST[$fieldname] ;
		    $s = ltrim(trim($_POST[$fieldname]), '0') ;
		    if ($s == '') $s = '0' ;
		    if ((string)$fieldvalue !== $s) return sprintf ("invalid value in field '%s'", $fieldname) ;
		    break ;

		case 'decimal' :
		    // Get field
		    $fieldvalue = trim($_POST[$fieldname]) ;
		    if ($fieldvalue == '') $fieldvalue = '0' ;

		    // Get and check format specification
		    if (!ereg ("^([0-9]{1,2})\.([0-9]{1})$", ($field['format']), $spec)) {
			return sprintf ("invalid decimal format specification, field %s, format %s", $fieldname, $field['format']) ;
		    }

		    // Generat and verify format
		    $format = sprintf ("(^-{0,1}[0-9]{1,%d}$)|(^-{0,1}[0-9]{0,%d}[\\,\\.][0-9]{0,%d}$)", $spec[1]-$spec[2], $spec[1]-$spec[2], $spec[2]) ;
		    if (!ereg($format,$fieldvalue)) return sprintf ("invalid value in field '%s'", $fieldname) ;

		    // Substitute , -> .
		    $fieldvalue = str_replace (',', '.', $fieldvalue) ;
		    break ;

		case 'date' :
		    $fieldvalue = trim($_POST[$fieldname]) ;
		    if ($fieldvalue == '') {
			$fieldvalue = NULL ;
			break ;
		    }
		    $t = strtotime($fieldvalue) ;

		    if ($fieldvalue != date('Y-m-d', $t)) return sprintf ("invalid date in field '%s'", $fieldname) ;

		    break ;

		case 'internal':
		    switch ($fieldname) {
			case 'ProjectId':
			    global $Project ;
			    $fieldvalue = (int)$_POST[$fieldname] ;
			    if (!permAdminSystem() and $fieldvalue != $Project['Id']) return 'access only to selected project' ;
			    if ($id > 0 and $db['ProjectId'] != $fieldvalue) return 'project Id can not be changed' ;
			    break ;
			default:
			    return sprintf ("%s(%d), invalid internal field, name '%s'", __FILE__, __LINE__, $fieldname) ;
		    }
		    break ;

		case 'ignore' :
		    continue ;

		case 'set' :
    		    $fieldvalue = $fields[$fieldname]['value'] ;
		    break ;

		case "NULL":
		    $fieldvalue = NULL ;
		    break ;

		default:
		    return sprintf ("%s(%d), invalid type of field, type '%s', field '%s'", __FILE__, __LINE__, $fieldtype, $fieldname) ;
	    }

	    $fields[$fieldname]['value'] = $fieldvalue ;
	    $fields[$fieldname]['db'] = $db[$fieldname] ;



	    if ($field['check']) {
		$res = $Application->checkfield ($fieldname, $fieldvalue, ($db[$fieldname] != $fieldvalue) or ($id <= 0)) ;
		if (gettype($res) == 'string') return $res ;
		if ($res === false) continue ;
	    }

	    $fieldvalue = $fields[$fieldname]['value'] ;

	    if ($id <= 0) {
		// New record
		if (is_null($fieldvalue) or $fieldvalue === "") {
		    if ($field["mandatory"]) return sprintf ("field '%s' must be specified", $fieldname) ;
		    continue ;
		}
	    } else {
		// Update record
		if (is_null($fieldvalue)) continue ;
		if ($db[$fieldname] == $fieldvalue) continue ;
	    }

	    switch ($field['permission']) {
		case 'admin' :
		    if (!permAdminSystem()) return "You do not have permission to modify this record (save, step 5) ".$fieldname ;
		    break ;

		case 'proadmin' :
		    if (!permAdminSystem() and !$permAdminProject()) return "You do not have permission to modify this record (save, step 6)" ;
		    break ;

		case 'function' :
		    if ($field['mark'] != $Navigation['FunctionMark']) return "You do not have permission to modify this record (save, step 7)" ;
		    break ;

		case NULL :
		    break ;

		default :
		    return sprintf ("%s(%d), invalid permission, field '%s', parameter '%s'", __FILE__, __LINE__, $fieldname, $field["permission"]) ;
	    }

	    // Build SQL statement
	    $sqlfields .= sprintf ('`%s`="%s",', $fieldname, addslashes($fieldvalue)) ;
	    $n++ ;

	    // Save fieldvalue in current Record
	    if ($update) $Application->$fieldname = $fieldvalue ;
	}

	// Validate
	if (function_exists('saveValidate')) {
	    $res = saveValidate ($n > 0) ;
	    if (gettype($res) == 'string') return $res ;
	    if ($res === false) return 0 ;
	}

	// Any changes ?
	if ($n == 0) return 0 ;

	$t = dbDateEncode(time()) ;
	if ($id <= 0) {
	    $query = sprintf ('INSERT INTO `%s` SET %s Active=1, CreateDate="%s", CreateUserId=%d, ModifyDate="%s", ModifyUserId=%d', $tablename, $sqlfields, $t, $User["Id"], $t, $User["Id"]) ;
	    if ($update) {
		$Application->CreateDate = $t ;
		$Application->CreateUserId = $User['Id'] ;
	    }
	} else {
	    $query = sprintf ('UPDATE `%s` SET %s ModifyDate="%s", ModifyUserId=%d WHERE Id=%d', $tablename, $sqlfields, $t, $User["Id"], $id) ;
	}
	if ($update) {
	    $Application->ModifyDate = $t ;
	    $Application->ModifyUserId = $User['Id'] ;
	}

	if (!is_null($tablename)) {
	    dbQuery ($query) ;
	    if ($id <= 0 and $update) $Application->Id = dbInsertedId () ;
	}


	return 0 ;
    }
?>
