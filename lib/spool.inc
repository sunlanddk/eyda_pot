<?php

function Spool ($queue=NULL, &$output) {

    // Check printer
    if (is_null($queue)) return ;

	$phdl = printer_open($queue);
	printer_set_option($phdl, PRINTER_MODE, 'RAW');
	printer_write($phdl, $output);
	printer_close($phdl);
/*

    // Start spooler
    $descriptorspec = array(
	0 => array('pipe', 'r'),  						// stdin is a pipe that the child will read from
	1 => array('file', sprintf ('/tmp/lp.%s-stdout', $queue), 'w'), 	// stdout is a pipe that the child will write to
	2 => array('file', sprintf ('/tmp/lp.%s-stderr', $queue), 'w')		// stderr is a file to write to
    ) ;

    // End process
    for ($n = 0 ; $n < 3 ; $n++) {
        $process = proc_open(sprintf ('lp -d %s', $queue), $descriptorspec, $pipes);
	if (!is_resource($process)) return sprintf ('%s(%d) proc_open failed, res %d<br>', __FILE__, __LINE__, $process) ;

	// Output data
	fwrite($pipes[0], $output);
	fclose($pipes[0]);

	// Terminate process
	$res = proc_close($process);
	if ($res == 0) return 0 ;
	
	// Retry in a while
	sleep (1) ;
    }
    sprintf ('%s(%d) proc_close failed, res %d<br>', __FILE__, __LINE__, $res) ;
*/
    return 0 ;
}
?>
