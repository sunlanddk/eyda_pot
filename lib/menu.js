//
//  Based on JSCookMenu v1.23.  (c) Copyright 2002 by Heng Yuan

var _cmIDCount = 0;
var _cmIDName = 'cmSubMenuID';		// for creating submenu id
var _cmPrefix = 'ThemeIE';		// CSS class prefix
var _cmTimeOut = null;			// how long the menu would stay
var _cmCurrentItem = null;		// the current menu item being selected;
var _cmNoAction = new Object ();	// indicate that the item cannot be hovered.
var _cmSplit = new Object ();		// indicate that the item is a menu split
var _cmItemList = new Array ();		// a simple list of items
var _cmActive = false ;			// submenus first after click 


// Drawing Functions and Utility Functions


// produce a new unique id
function cmNewID () {
	return _cmIDName + (++_cmIDCount);
}

// return the property string for the menu item
function cmActionItem (item, idSub) {
	_cmItemList[_cmItemList.length] = item;
	var index = _cmItemList.length - 1;
	idSub = (!idSub) ? 'null' : ('\'' + idSub + '\'');
	return ' onmouseover="cmItemMouseOver (this,' + idSub + ',' + index + ')" onmouseout="cmItemMouseOut(this)" onmousedown="cmItemMouseDown(this,' + idSub + ',' + index + ')" onmouseup="cmItemMouseUp(this,' + index + ')"';
}

function cmNoActionItem (item) {
	return item[1];
}

function cmSplitItem () {
	return '<td colspan="3" style="height: 3px; overflow: hidden"><div class="ThemeIEMenuSplit"></div></td>' ;
}

// draw the sub menu recursively
function cmDrawSubMenu (subMenu, id) {
	var str = '<div class="' + _cmPrefix + 'SubMenu" id="' + id + '"><table summary="sub menu" cellspacing=0 class="' + _cmPrefix + 'SubMenuTable">';
	var strSub = '';
	var item;
	var idSub;
	var hasChild;
	var i;
	var classStr;

	for (i = 4; i < subMenu.length; ++i) {
		item = subMenu[i];
		if (!item) continue;

		hasChild = (item.length > 4);
		idSub = hasChild ? cmNewID () : null;

		str += '<tr class="' + _cmPrefix + 'MenuItem"' + cmActionItem (item, idSub) ;
		if (item[3] == null) str += ' style="cursor: default;color:#7f7c73;"' ;
		str += '>';

		if (item == _cmSplit) {
			str += cmSplitItem () ;
			str += '</tr>';
			continue;
		}

		if (item[0] == _cmNoAction) {
			str += cmNoActionItem (item);
			str += '</tr>';
			continue;
		}

		classStr = _cmPrefix + 'MenuItem';

		str += '<td class="' + classStr + 'Left">';
		if (item[0] != null && item[0] != _cmNoAction) str += '<img src=image/toolbar/' + item[0] + '>' ;

		str += '<td class="' + classStr + 'Text"' ;
		str += '>' ; 
		str += item[1] ;

		str += '<td class="' + classStr + 'Right">';
		if (hasChild) {
			str += '<img alt="" src="image/menu/arrow.gif">';
			strSub += cmDrawSubMenu (item, idSub);
		}

		str += '</td></tr>';
	}

	str += '</table></div>' + strSub;
	return str;
}

// The function that builds the menu inside the specified element id.
//
// @param	id	id of the element
//		menu	the menu object to be drawn
//
function cmDraw (id, menu) {
	var obj = cmGetObject (id);
	var str = '<table summary="main menu" class="' + _cmPrefix + 'Menu" cellspacing=0><tr>';
	var strSub = '';
	var i;
	var item;
	var idSub;
	var hasChild;
	var classStr;

	for (i = 0; i < menu.length; ++i) {
		item = menu[i];
		if (!item) continue;

		hasChild = (item.length > 4);
		idSub = hasChild ? cmNewID () : null;

		str += '<td class="' + _cmPrefix + 'MainItem"';
		str += cmActionItem (item, idSub) ;
		if ((item[0] != null) && (hasChild || (item[3] != null))) str += ' style="cursor:pointer;"' ;
		str += '>';

		if (item[0] == _cmNoAction) {
			str += cmNoActionItem (item);
			str += '</td>';
			continue;
		}

		if (item[0] == null) {
		    str += '<div style="margin-left:2px">' + item[1] + '</div>';
		} else {
		    var name = item[1] ;
		    if ((item[2] != null) && (item[3] != null)) {
			// Underline AccessKey letter
			var re = new RegExp ('('+item[2]+')', 'i') ;
			name = name.replace (re, '<U>$1</U>') ;
		    }
		    str += '<table><tr>' ;
		    str += '<td><img src="image/toolbar/' + item[0] + '"></td>' ;
		    str += '<td style="padding-left:4px;padding-right:4px;vertical-align:middle;"'
		    if (!hasChild && (item[3] == null)) str += ' style="color:#7f7c73;"'
		    str += '>' + name + '</td>' ;
		    if (hasChild) str += '<td><img src="image/toolbar/arrow.gif"></td>' ;
		    if ((item[2] != null) && (item[3] != null)) str += '<td style="width:0;"><button accesskey="' + item[2] + '" style="width:0;position:absolute; top:-1000px" onclick="cmItemKey(this,' + (_cmItemList.length - 1) + ')"></button></td>' ;
		    str += '</tr></table>' ;
		}

		str += '</td>';

		if (hasChild) strSub += cmDrawSubMenu (item, idSub);
	}
	str += '</tr></table>' + strSub;
	obj.innerHTML = str;
	// document.write ("<xmp>" + str + "</xmp>");
}


// Mouse Event Handling Functions

// action should be taken for mouse moving in to the menu item
function cmItemMouseOver (obj, idSub, index) {
	clearTimeout (_cmTimeOut);

	var thisMenu = cmGetThisMenu (obj);
	obj.cmIsMain = thisMenu.cmIsMain ;

	// insert obj into cmItems if cmItems doesn't have obj
	if (!thisMenu.cmItems)
		thisMenu.cmItems = new Array ();
	var i;
	for (i = 0; i < thisMenu.cmItems.length; ++i) {
		if (thisMenu.cmItems[i] == obj)
			break;
	}
	if (i == thisMenu.cmItems.length) {
		thisMenu.cmItems[i] = obj;
	}

	// hide the previous submenu that is not this branch
	if (_cmCurrentItem) {
		// occationally, we get this case when user
		// move the mouse slowly to the border
		if (_cmCurrentItem == thisMenu)	return;

		var thatMenu = cmGetThisMenu (_cmCurrentItem);
		if (thatMenu != thisMenu.cmParentMenu) {
			if (_cmCurrentItem.cmIsMain)
				_cmCurrentItem.className = _cmPrefix + 'MainItem';
			else
				_cmCurrentItem.className = _cmPrefix + 'MenuItem';
			if (thatMenu.id != idSub)
				cmHideMenu (thatMenu, thisMenu);
		}
	}

	// okay, set the current menu to this obj
	_cmCurrentItem = obj;

	// just in case, reset all items in this menu to MenuItem
	cmResetMenu (thisMenu);

	var item = _cmItemList[index];
	var isDefaultItem = cmIsDefaultItem (item);
	var hasChild = (item.length > 4);

	if (isDefaultItem) {
		if (obj.cmIsMain) {
			// Only activate items witout icon or with submenus
			if ((hasChild && _cmActive) || (item[0] == null)) obj.className = _cmPrefix + 'MainItemActive';
		} else {
			obj.className = _cmPrefix + 'MenuItemHover';
		}
	}

	if (idSub && _cmActive) {
		var subMenu = cmGetObject (idSub);
		cmShowSubMenu (obj, subMenu);
	}
}

// action should be taken for mouse moving out of the menu item
function cmItemMouseOut (obj) {
	_cmTimeOut = window.setTimeout ('cmHideMenuTime ()', 500);
}

// action should be taken for mouse button down at a menu item
function cmItemMouseDown (obj, idSub, index) {
	var item = _cmItemList[index];
	var hasChild = (item.length > 4);

	if (!_cmActive) {
	    _cmActive = true ;
	    cmItemMouseOver (obj, idSub, index)
	}

	if (cmIsDefaultItem (_cmItemList[index])) {
		if (obj.cmIsMain) {
			// Only activate items witout icon or with submenus
			if (hasChild || (item[0] == null)) obj.className = _cmPrefix + 'MainItemActive';
		} else {
			obj.className = _cmPrefix + 'MenuItemActive';
		}
	}
}

// action should be taken for mouse button up at a menu item
function cmItemMouseUp (obj, index) {
	var item = _cmItemList[index];

	var p = item[3] ;
	if (p != null) appClick (null,p[0],p[1])

	var thisMenu = cmGetThisMenu (obj);

	var hasChild = (item.length > 4);
	if (!hasChild) {
		if (cmIsDefaultItem (item)) {
			if (obj.cmIsMain)
				obj.className = _cmPrefix + 'MainItem';
			else
				obj.className = _cmPrefix + 'MenuItem';
		}
		cmHideMenu (thisMenu, null);
		_cmActive = false ;
	} else {
		if (cmIsDefaultItem (item)) {
			if (obj.cmIsMain) {
				obj.className = _cmPrefix + 'MainItemHover';
			} else {
				obj.className = _cmPrefix + 'MenuItemHover';
			}
		}
	}
}

// action should be taken for special AccessKey
function cmItemKey (obj, index) {
	var item = _cmItemList[index];
	var p = item[3] ;
	if (p != null) appClick (null,p[0],p[1])
}


// Mouse Event Support Utility Functions

// move submenu to the appropriate location
//
// @param	obj	the menu item that opens up the subMenu
//		subMenu	the sub menu to be shown
//		orient	the orientation of the subMenu
//
function cmMoveSubMenu (obj, subMenu, orient) {
	var mode = String (orient);
	var p = subMenu.offsetParent;
	if (mode.charAt (0) == 'h') {
		if (mode.charAt (1) == 'b')
			subMenu.style.top = (cmGetYAt (obj, p) + obj.offsetHeight) + 'px';
		else
			subMenu.style.top = (cmGetYAt (obj, p) - subMenu.offsetHeight) + 'px';
		if (mode.charAt (2) == 'r')
			subMenu.style.left = (cmGetXAt (obj, p)) + 'px';
		else
			subMenu.style.left = (cmGetXAt (obj, p) + obj.offsetWidth - subMenu.offsetWidth) + 'px';
	} else {
		if (mode.charAt (2) == 'r')
			subMenu.style.left = (cmGetXAt (obj, p) + obj.offsetWidth) + 'px';
		else
			subMenu.style.left = (cmGetXAt (obj, p) - subMenu.offsetWidth) + 'px';
		if (mode.charAt (1) == 'b')
			subMenu.style.top = (cmGetYAt (obj, p)) + 'px';
		else
			subMenu.style.top = (cmGetYAt (obj, p) + obj.offsetHeight - subMenu.offsetHeight) + 'px';
		//alert (subMenu.style.top + ', ' + cmGetY (obj) + ', ' + obj.offsetHeight);
	}
}

//
// show the subMenu
// also move it to the correct coordinates
//
// @param	obj	the menu item that opens up the subMenu
//		subMenu	the sub menu to be shown
//
function cmShowSubMenu (obj, subMenu) {
	if (!subMenu.cmParentMenu) {
		// establish the tree w/ back edge
		var thisMenu = cmGetThisMenu (obj);
		subMenu.cmParentMenu = thisMenu;
		if (!thisMenu.cmSubMenu) thisMenu.cmSubMenu = new Array ();
		thisMenu.cmSubMenu[thisMenu.cmSubMenu.length] = subMenu;
	}
	
	if (obj.cmIsMain) {
	//if (subMenu.cmParentMenu.className == 'ThemeIEMenu') {
		var orient = 'hbr' ;
	} else {
		var orient = 'vbr' ;
	}

	// position the sub menu
	cmMoveSubMenu (obj, subMenu, orient);
	subMenu.style.visibility = 'visible';

	// On IE, controls such as SELECT, OBJECT, IFRAME (before 5.5)
	// are window based controls.  So, if sub menu and these controls
	// overlap, sub menu would be hid behind them.  Thus, one needs to
	// turn the visibility of these controls off when the
	// sub menu is showing, and turn their visibility back on
	if (document.all) {	// it is IE
		subMenu.cmOverlap = new Array ();
//		cmHideControl ("IFRAME", subMenu);
		cmHideControl ("SELECT", subMenu);
//		cmHideControl ("OBJECT", subMenu);
	}
}

// reset all the menu items to class MenuItem in thisMenu
function cmResetMenu (thisMenu) {
	if (thisMenu.cmItems) {
		var i;
		var str;
		var items = thisMenu.cmItems;
		for (i = 0; i < items.length; ++i) {
			if (items[i].cmIsMain)
				str = _cmPrefix + 'MainItem';
			else
				str = _cmPrefix + 'MenuItem';
			if (items[i].className != str)
				items[i].className = str;
		}
	}
}

// called by the timer to hide the menu
function cmHideMenuTime () {
	if (_cmCurrentItem) {
		cmHideMenu (cmGetThisMenu (_cmCurrentItem), null) ;
	}
	_cmActive = false ;
}

// hide thisMenu, children of thisMenu, as well as the ancestor
// of thisMenu until currentMenu is encountered.  currentMenu
// will not be hidden
function cmHideMenu (thisMenu, currentMenu) {
	var str = _cmPrefix + 'SubMenu';

	// hide the down stream menus
	if (thisMenu.cmSubMenu) {
		var i;
		for (i = 0; i < thisMenu.cmSubMenu.length; ++i) {
			cmHideSubMenu (thisMenu.cmSubMenu[i]);
		}
	}

	// hide the upstream menus
	while (thisMenu && thisMenu != currentMenu) {
		cmResetMenu (thisMenu);
		if (thisMenu.className == str) {
			thisMenu.style.visibility = 'hidden';
			cmShowControl (thisMenu);
		} else {
			break ;
		}
		thisMenu = cmGetThisMenu (thisMenu.cmParentMenu);
	}

	_cmCurrentItem = null ;
}

// hide thisMenu as well as its sub menus if thisMenu is not
// already hidden
function cmHideSubMenu (thisMenu) {
	if (thisMenu.style.visibility == 'hidden') return ;
	if (thisMenu.cmSubMenu) {
		var i;
		for (i = 0; i < thisMenu.cmSubMenu.length; ++i) {
			cmHideSubMenu (thisMenu.cmSubMenu[i]);
		}
	}
	cmResetMenu (thisMenu);
	thisMenu.style.visibility = 'hidden';
	cmShowControl (thisMenu);
}

// hide a control such as IFRAME
function cmHideControl (tagName, subMenu) {
	var x = cmGetX (subMenu);
	var y = cmGetY (subMenu);
	var w = subMenu.offsetWidth;
	var h = subMenu.offsetHeight;

	var i;
	for (i = 0; i < document.all.tags(tagName).length; ++i) {
		var obj = document.all.tags(tagName)[i];
		if (!obj || !obj.offsetParent) continue;

		// check if the object and the subMenu overlap
		var ox = cmGetX (obj);
		var oy = cmGetY (obj);
		var ow = obj.offsetWidth;
		var oh = obj.offsetHeight;

		if (ox > (x + w) || (ox + ow) < x) continue;
		if (oy > (y + h) || (oy + oh) < y) continue;

		subMenu.cmOverlap[subMenu.cmOverlap.length] = obj;
		obj.style.visibility = "hidden";
	}
}

// show the control hidden by the subMenu
function cmShowControl (subMenu) {
	if (subMenu.cmOverlap) {
		var i;
		for (i = 0; i < subMenu.cmOverlap.length; ++i)
			subMenu.cmOverlap[i].style.visibility = "";
	}
	subMenu.cmOverlap = null;
}

// returns the main menu or the submenu table where this obj (menu item) is in
function cmGetThisMenu (obj) {
	var str1 = _cmPrefix + 'SubMenu';
	var str2 = _cmPrefix + 'Menu';
	while (obj) {
		if (obj.className == str1) {
			obj.cmIsMain = false ;
			return obj ;
		}
		if (obj.className == str2) {
			obj.cmIsMain = true ;
			return obj ;
		}
		obj = obj.parentNode;
	}
	return null;
}

// return true if this item is handled using default handlers
function cmIsDefaultItem (item) {
	if (item == _cmSplit || item[0] == _cmNoAction) return false;
	return true;
}

// returns the object baring the id
function cmGetObject (id) {
	if (document.all) return document.all[id];
	return document.getElementById (id);
}

// functions that obtain the coordinates of an HTML element
function cmGetX (obj) {
	var x = 0;

	do
	{
		x += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	while (obj);
	return x;
}

function cmGetXAt (obj, elm) {
	var x = 0;

	while (obj && obj != elm)
	{
		x += obj.offsetLeft;
		obj = obj.offsetParent;
	}
	return x;
}

function cmGetY (obj) {
	var y = 0;
	do
	{
		y += obj.offsetTop;
		obj = obj.offsetParent;
	}
	while (obj);
	return y;
}

function cmGetYAt (obj, elm) {
	var y = 0;

	while (obj && obj != elm)
	{
		y += obj.offsetTop;
		obj = obj.offsetParent;
	}
	return y;
}

// debug function, ignore :)
function cmGetProperties (obj) {
	if (obj == undefined) return 'undefined';
	if (obj == null) return 'null';
	var msg = obj + ':\n';
	var i;
	for (i in obj) msg += i + ' = ' + obj[i] + '; ' ;
	return msg;
}
