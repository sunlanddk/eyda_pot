<?php

/**
 * @return array
 */
function getSupportedLanguages()
{
//    TODO: Refactor code to make it dynamic from database
//    $languages = array();
//    $query = 'select * from language';
//    $result = dbQuery($query);
//    while ($row = dbFetch($result)) {
//        $languages[$row['id']] = $row['isoCode'];
//    }
//    return $languages;

    return [
        4 => 'DA',
        6 => 'EN',
        8 => 'SE',
        7 => 'NO',
    ];
}
