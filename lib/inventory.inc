<?php

function updateInventoryOnArticles($articles, $checkIfStockWasZero = false){
    global $User;
	$post = array(
		'userId' => $User['Id'],
		'articles' => $articles,
        'checkIfStockWasZero' => $checkIfStockWasZero
	);

	$ch = curl_init(_SYSTEM_S4_URL_.'inventory/update/counters');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    $result = curl_exec($ch);

    return true;
}

function updateStockCountByVariant($variants, $comment = ''){
    global $User;
    $post = array(
        'userId' => $User['Id'],
        'variants' => $variants,
        'description' => $comment
    );

    $ch = curl_init(_SYSTEM_S4_URL_.'inventory/update/by/changed');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($post));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
    return $result = curl_exec($ch);

    return true;
}