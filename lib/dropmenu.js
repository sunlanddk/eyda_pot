
var menuobj ;

function dropmenuShow (e, userindex) {

    // ignore right buttom
    if ((e.button) && (e.button == 2)) return true
    
    menuobj = document.getElementById ("dropmenu")

    // Compatibility
    if (!menuobj.style) menuobj.style = menuobj
    if (!document.body.clientWidth) document.body.clientWidth = window.innerWidth
    if (!document.body.clientHeight) document.body.clientHeight = window.innerHeight
    if (!document.body.scrollLeft) document.body.scrollLeft = (window.pageXOffset) ? window.pageXOffset : 0
    if (!document.body.scrollTop) document.body.scrollTop = (window.pageYOffset) ? window.pageYOffset : 0

    menuobj.userindex = userindex
    
    if (menuobj.hidetimer) {
	clearTimeout (menuobj.hidetimer)
	menuobj.hidetimer = 0 
    }
    
    var eventX=e.x
    var eventY=e.y+10

    //Find out how close the mouse is to the corner of the window
    var rightedge = document.body.clientWidth - eventX
    var bottomedge = document.body.clientHeight - eventY

    //if the distances isn't enough to accomodate the width of the context menu
    menuobj.style.left = document.body.scrollLeft + eventX - ((rightedge < menuobj.offsetWidth) ? menuobj.offsetWidth : 0)
    menuobj.style.top=document.body.scrollTop + eventY - ((bottomedge<menuobj.offsetHeight) ? menuobj.offsetHeight : 0)

    menuobj.style.visibility="visible"
    return false
}

function dropmenuHide(e) {
    function contains_ns6(a, b) {
	// Determines if 1 element in contained in an other
	while (b.parentNode) {
	    if ((b = b.parentNode) == a) {
		return true
	    }
	}	
	return false;
    }

    if (e) {
	if (menuobj.contains) {
	    if (menuobj.contains(e.toElement)) return 
	} else if (e.currentTarget== e.relatedTarget || contains_ns6(e.currentTarget, e.relatedTarget)) {
	    return
	}
    }

    if (window.menuobj) {
	menuobj.style.visibility = "hidden"
    }

    if (menuobj.hidetimer) {
	clearTimeout(menuobj.hidetimer)
	menuobj.hidetimer = 0 
    }

    menuobj = 0   
}

function dropmenuHideDelayed() {
    if (menuobj) menuobj.hidetimer = setTimeout("dropmenuHide(0)",1000)
}

function dropmenuHighlight (e,state) {
    if (document.all) {
	source_el=e.srcElement
    } else if (document.getElementById) {
	source_el=e.target
    } else {
        return
    }

    if (menuobj.hidetimer) {
	clearTimeout(menuobj.hidetimer)
	menuobj.hidetimer = 0 
    }   

    while(source_el.id!="dropmenu") {
        if (source_el.className=="dropitem") {
	    source_el.id=(state)? "drophighlight" : ""
	    break ;
	}
	source_el=document.getElementById? source_el.parentNode : source_el.parentElement
    }
}

function dropmenuClick(event,func) {
    if (!menuobj) return true ;
    var id = menuobj.userindex ;
    dropmenuHide(0) ;
    eval(func) ;
    return false ;
}
