<?php

    function tableMark2Id ($table, $mark) {
	$query = sprintf ("SELECT Id FROM `%s` WHERE `Mark`='%s' AND `Active`=1", $table, $mark) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	return $row["Id"] ;
    }   

    function tableGetFieldWhere ($table, $field, $clause) {
	$query = sprintf ("SELECT %s AS Value FROM %s WHERE %s", $field, $table, $clause) ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	return $row["Value"] ;
    }
    
    function tableGetField ($table, $field, $id) {
	return tableGetFieldWhere ($table, $field, 'Id='.$id) ;
    }

    function tableGetType ($table, $field) {
	$type = NULL ;
	$query = sprintf ("SHOW COLUMNS FROM `%s`", $table) ;
	$result = dbQuery ($query) ;
	while ($row = dbFetch ($result)) {
	    if ($row['Field'] == $field) {
		$type = $row['Type'] ;
		break ;
	    }
	}
	dbQueryFree ($result) ;
	return $type ;
    }
    
    function tableEnumExplode ($type) {
	// Explode values of enum types into array
	// Example: enum('text','boolean','integer','date','time','reference','week','combo')
	$type = str_replace (' ','',$type) ;		// Remove spaces
	if (strncmp($type,'enum',4)) return NULL ;
	$type = str_replace ('enum(','',$type) ;	// Remove header
	$type = str_replace (')','',$type) ;		// Remove trailer
	$type = str_replace ('\'','', $type) ;		// Remove quoutes	
	return explode (',',$type) ;
    }

    function tableDefaultId ($table, $where=null) {
	global $Project ;
	$query = sprintf ("SELECT `Id` FROM `%s` WHERE (`ProjectId`=%d OR `ProjectId`=0) AND `DefaultRecord`=1%s ORDER BY `ProjectId` DESC", $table, $Project["Id"], (!is_null($where)) ? " AND ".$where : "") ;
	$result = dbQuery ($query) ;
	$row = dbFetch ($result) ;
	dbQueryFree ($result) ;
	return $row["Id"] ;
    }

    function tableDeleteWhere ($table, $where) {
	global $User ;
	if (!is_null($where) and $where != "") {
	    $query = sprintf ("UPDATE `%s` SET `Active`=0, `ModifyDate`='%s', `ModifyUserId`=%d WHERE %s", $table, dbDateEncode(time()), $User["Id"], $where) ;
	    dbQuery ($query) ;
	}
    }
    
    function tableDelete ($table, $id) {
	if ($id > 0) tableDeleteWhere ($table, "Id=".$id) ;
    }

    function tableWrite ($table, $row, $id=0) {
	global $User ;

	$n = 0 ;
	$sqlfields = "" ;
	foreach ($row as $field => $value) {
	    if (is_array($value)) continue ;
	    
	    switch ($field) {
		case 'Active' :
		    if ($value == 0) return 0 ;
		    continue ;

		case 'Id' :
		case 'CreateDate' :
		case 'CreateUserId' :
		case 'ModifyDate' :
		case 'ModifyUserId' :
		    break ;

		default:
		    $sqlfields .= sprintf ('`%s`="%s",', $field, addslashes($value)) ;
		    $n++ ;
		    break ;
	    }
	}
	if ($n == 0) return 0 ;
	
	if ($id > 0) {
	    $query = sprintf ('UPDATE `%s` SET %s `ModifyDate`="%s", `ModifyUserId`=%d WHERE `Id`=%d', $table, $sqlfields, dbDateEncode(time()), (int)$User['Id'], $id) ;
	    dbQuery ($query) ;
	    return $id ;
	} else {
	    $query = sprintf ('INSERT INTO `%s` SET %s `Active`=1, `CreateDate`="%s", `CreateUserId`=%d, `ModifyDate`="%s", `ModifyUserId`=%d', $table, $sqlfields, dbDateEncode(time()), (int)$User['Id'], dbDateEncode(time()), (int)$User['Id']) ;
	    dbQuery ($query) ;
	    return dbInsertedId () ;
	}
    }
    function tableWriteCron ($table, $row, $id=0) {
    	global $User ;

		$n = 0 ;
		$sqlfields = "" ;
		foreach ($row as $field => $value) {
		    if (is_array($value)) continue ;
		    
		    switch ($field) {

			case 'Id' :
			case 'CreateDate' :
			case 'CreateUserId' :
			case 'ModifyDate' :
			case 'ModifyUserId' :
			    break ;

			default:
			    $sqlfields .= sprintf ('`%s`="%s",', $field, addslashes($value)) ;
			    $n++ ;
			    break ;
		    }
		}
		if ($n == 0) return 0 ;
		
		if ($id > 0) {
		    $query = sprintf ('UPDATE `%s` SET %s WHERE `Id`=%d', $table, substr($sqlfields, 0, -1), $id) ;
		    dbQuery ($query) ;
		    return $id ;
		} else {
		    $query = sprintf ('INSERT INTO `%s` SET %s ', $table, substr($sqlfields, 0, -1)) ;
		    dbQuery ($query) ;
		    return dbInsertedId () ;
		}
    }

    function tableWriteNew ($table, $row, $id=0) {
	global $User ;

	$n = 0 ;
	$sqlfields = "" ;
	foreach ($row as $field => $value) {
	    if (is_array($value)) continue ;
	    
	    switch ($field) {

		case 'Id' :
		case 'CreateDate' :
		case 'CreateUserId' :
		case 'ModifyDate' :
		case 'ModifyUserId' :
		    break ;

		default:
		    $sqlfields .= sprintf ('`%s`="%s",', $field, addslashes($value)) ;
		    $n++ ;
		    break ;
	    }
	}
	if ($n == 0) return 0 ;
	
	if ($id > 0) {
	    $query = sprintf ('UPDATE `%s` SET %s `ModifyDate`="%s", `ModifyUserId`=%d WHERE `Id`=%d', $table, $sqlfields, dbDateEncode(time()), (int)$User['Id'], $id) ;
	    dbQuery ($query) ;
	    return $id ;
	} else {
	    $query = sprintf ('INSERT INTO `%s` SET %s `CreateDate`="%s", `CreateUserId`=%d, `ModifyDate`="%s", `ModifyUserId`=%d', $table, $sqlfields, dbDateEncode(time()), (int)$User['Id'], dbDateEncode(time()), (int)$User['Id']) ;
	    dbQuery ($query) ;
	    return dbInsertedId () ;
	}
    }

    function tableWriteSimple ($table, $row, $id=0) {
	global $User ;

	$n = 0 ;
	$sqlfields = "" ;
	foreach ($row as $field => $value) {
	    if (is_array($value)) continue ;
	    
	    switch ($field) {

		case 'Id' :
		case 'CreateDate' :
		case 'CreateUserId' :
		case 'ModifyDate' :
		case 'ModifyUserId' :
		    break ;

		default:
		    $sqlfields .= sprintf ('`%s`="%s",', $field, addslashes($value)) ;
		    $n++ ;
		    break ;
	    }
	}
	if ($n == 0) return 0 ;
	
	if ($id > 0) {
	    $query = sprintf ('UPDATE `%s` SET %s WHERE `Id`=%d', $table, substr($sqlfields, 0, -1), $id) ;
	    dbQuery ($query) ;
	    return $id ;
	} else {
	    $query = sprintf ('INSERT INTO `%s` SET %s ', $table, substr($sqlfields, 0, -1)) ;
	    dbQuery ($query) ;
	    return dbInsertedId () ;
	}
    }
    
    function tableCreate ($table, $row) {
	return tableWrite ($table, $row) ;
    }
	
    function tableTouch ($table, $id) {
	global $User ;
	$query = sprintf ('UPDATE `%s` SET `ModifyDate`="%s", `ModifyUserId`=%d WHERE `Id`=%d', $table, dbDateEncode(time()), $User['Id'], $id) ;
	dbQuery ($query) ;
    }
?>
