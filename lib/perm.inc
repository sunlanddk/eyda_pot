<?php

    function permAdminSystem () {
	global $User ;
	return $User['AdminSystem'] ;
    }

    function permAdminProject () {
	global $User ;
	return $User['AdminProject'] ;
    }

    function permAdmin () {
	global $User ;
	return $User['AdminSystem'] or $User['AdminProject'] ;
    }

    function permClauseExternal ($tabel="Object") {
	global $User ;
	if (!permAdminSystem() and !permAdminProject() and !$User["Internal"]) return sprintf (" AND %s.External=1", $tabel) ;
	return "" ;	 
    }

    function permClauseOwner ($tabel="Object") {
	global $User, $Permission ;
	if (!permAdminSystem() and !permAdminProject() and !$Permission["PermReadOther"]) return sprintf(" AND %s.CreateUserId=%d", $tabel, $User["Id"]) ;
	return "" ;
    }
?>
