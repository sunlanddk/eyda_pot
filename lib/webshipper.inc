<?php 

require_once('lib/file.inc');
require_once('lib/table.inc');
require_once('lib/inventory.inc');
require_once('lib/log.inc');

function shipItemsInWebshipper($shipmentId, $autoprint = false){
	$webshipper = array(
		"data" => array(
			"type" 			=> "shipments",
			"attributes" 	=> array(
				"packages"	=> [
				],
			),
			"relationships" 	=> array(
	            "order" 	=> array(
	                "data" => array(
	                    "id" 	=> 0,
	                    "type" 	=> "orders"
	                )
	            )
	        )
		)
	);
	$packages = array();
	$query = sprintf (
		'
		SELECT v.VariantCode, v.Id as VariantCodeId, SUM(i.Quantity) as ConsQty, o.WebshipperId as OWebshipperId, oq.WebshipperId as lineWebshipperId, c.Id as ContainerId, ol.ArticleId as aId, ol.ArticleColorId as acId, oq.ArticleSizeId as asId, o.Id as OrderId
		FROM (`stock` s, `container` c, `item` i, `variantcode` v, `orderline` ol, `order` o, `orderquantity` oq)
		WHERE 
			s.Id=%d AND 
			c.StockId=s.Id AND 
			i.ContainerId=c.Id AND 
			i.Active=1 AND 
			c.Active=1 AND 
			ol.Id=i.OrderLineId AND o.Id=ol.OrderId AND
			v.ArticleId=i.ArticleId AND v.ArticleColorId=i.ArticleColorId AND v.ArticleSizeId=i.ArticleSizeId AND v.Active=1 AND
			oq.OrderLineId=i.OrderLineId AND oq.Active=1
			GROUP BY 
			i.ArticleId, i.ArticleColorId, i.ArticleSizeId, c.Id
		', 
		$shipmentId
	) ;
	$result = dbQuery ($query) ;
	$articlesToUpdate = [];
	$webshipperId = 0;
	while ( $row = dbFetch ($result) ) {
		$webshipperId = $row['OWebshipperId'];
		$webshipper['data']['relationships']['order']['data']['id'] = $row['OWebshipperId'];
    	$packages[$row['ContainerId']]['ext_ref'] = $row['ContainerId'];
    	$packages[$row['ContainerId']]['order_lines'][] = array(
    		'id' 		=> $row['lineWebshipperId'],
    		'quantity' 	=> $row['ConsQty']
    	);
    	array_push($articlesToUpdate, $row);
	}
	dbQueryFree ($result) ;

	foreach ($packages as $key => $package) {
    	$webshipper['data']['attributes']['packages'][] = $package;
    }

logprintf(1, 'update shopify inventory ') ;
    updateInventoryOnArticles($articlesToUpdate);
logprintf(1, 'requesting label ') ;

    $ch = curl_init('https://eyda-aps.api.webshipper.io/v2/shipments?include=labels');
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer 5f1ec30a4aee3be406c9160070bd5929667d19b7f5843fdc5cac67b1239b9a76', 'Content-Type: application/vnd.api+json'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($webshipper));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch);    
	$resultObj = json_decode($result, true);

	if(isset($resultObj['included']) === true){
		foreach ($resultObj['included'] as $key => $label) {
			if($label['type'] == 'labels'){
				$webshipperLabelId = (int)tableGetFieldWhere('webshipperlabel', 'Id',  sprintf("Active=1 AND StockId=%d AND LabelId=%d", $shipmentId, $label['id']));
				$post = array(
					'StockId' => $shipmentId,
					'LabelId' => $label['id'],
					'LabelUrl' => $label['links']['self'],
					'Type' => 'Shipment'
				);
				tableWrite('webshipperlabel', $post, $webshipperLabelId);
			}
		}
		createReturnShipment($webshipperId, $shipmentId);
		uploadWebshipperLabel($shipmentId);
	}
	else{
		return 'An error occured when creating shipment in webshipper:' . json_encode($result);
	}
	return true;
}

function uploadWebshipperLabel($shipmentId){
	$query = sprintf ('SELECT * FROM webshipperlabel WHERE StockId=%d AND Active=1', $shipmentId) ;
    $res = dbQuery ($query) ;
    while ($row = dbFetch ($res)) {
        $ch = curl_init($row['LabelUrl'].$row['LabelId']);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "GET");
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer 5f1ec30a4aee3be406c9160070bd5929667d19b7f5843fdc5cac67b1239b9a76', 'Content-Type: application/vnd.api+json'));
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
		$result = curl_exec($ch);    
		$resultObj = json_decode($result, true);

		$pdfdoc =  base64_decode($resultObj['data']['attributes']['base64']);
		if($pdfdoc !== ''){
			$return = ((string)$row['Type'] === 'Return' ? 1 : '');
			$file = fileName ('webshipper', (int)$shipmentId, true, '.pdf') ;
			$resource = fopen ($file, 'w') ;
			fwrite ($resource, $pdfdoc) ;
			fclose ($resource) ;
			chmod ($file, 0640) ;
			UploadLabelToServer($shipmentId, $file, $return);
			//unlink();
		}
    }
}

function UploadLabelToServer($shipmentId, $filePath, $return = ''){
	global $Login, $Config;
	$fileinfo['localePath'] = $filePath;
	$_wsid = (int)TableGetField('login','WorkstationId', (int)$Login['Id']) ;
	//$_wsid = 1 ;

	if ($_wsid>0) {
		if ($Config['Instance']=='Production') {
			$_folder = 'NetSolution/CU1' ;
		} else {
			$_folder = 'Test/Alex';
		}
		
		$_ws = TableGetField('Workstation','Name', $_wsid) ;

		$fileinfo['ftpPath'] = $_folder.'/'. $_ws .'_Address' . substr($_ws, 2, 2) . '_' . $shipmentId. $return.'.pdf';
		$logininfo['host'] = 'vion.dk';
		$logininfo['usr'] = 'vion.dk04';
		$logininfo['pwd'] = 'Nutrinic1' ;
		ftpUpload($logininfo, $fileinfo, true) ;
	}

	return true;
}

function createReturnShipment($order_id, $_stockId){
	$PassOnOrderId = (int)tableGetFieldWhere('`order`','Id', sprintf("Active=1 AND WebshipperId='%s'",$order_id));
	if($PassOnOrderId === 1){
		return true;
	}
	$ShopNo  = (int)tableGetField('`order`', 'WebShopNo', $PassOnOrderId);
	$reference = (string)tableGetField('`order`', 'Reference', $PassOnOrderId);

	$notAReturn = true;
	$returnText = 'Retur';
	if(strrpos(tablegetfield('`order`','Comment', $PassOnOrderId), 'claim') !== false){
		$notAReturn = false;
		$returnText = 'Reklamation';
	}
	else if(strrpos(tablegetfield('`order`','Comment', $PassOnOrderId), 'claims') !== false){
		$notAReturn = false;	
		$returnText = 'Reklamation';
	}
	else if(strrpos(tablegetfield('`order`','Comment', $PassOnOrderId), 'reklamation') !== false){
		$notAReturn = false;
		$returnText = 'Reklamation';
	}
	else if((int)$ShopNo === 5){
		$notAReturn = false;
	}

	if($notAReturn === true){
		return true;
	}

	$data = array(
		'data' => array(
			'type' 						=> 'shipments',
			'attributes' 				=> array(
				'reference' 			=> $reference.' '.$returnText,
				'packages'				=> [],
				// 'test_mode' 			=> true,
				'is_return' 			=> true,				
			),
			'relationships' 	=> array(
	            'order' 		=> array(
	                'data' 		=> array(
	                    'id' 	=> $order_id,
	                    'type' 	=> 'orders'
	                )
	            )
	        )
		)
	);

	if(strrpos(tablegetfield('`order`','Comment', $PassOnOrderId), 'claim') !== false){
		$data['data']['attributes']['delivery_address'] = array(
			'company_name' 	=> 'Eyda Aps',
			'address_1'		=> 'Lene Haus Vej 10A',
			'zip'			=> '7430',
			'city'			=> 'Ikast',
			'country_code' 	=> 'DK'
		);
	}
	else if(strrpos(tablegetfield('`order`','Comment', $PassOnOrderId), 'claims') !== false){
		$data['data']['attributes']['delivery_address'] = array(
			'company_name' 	=> 'Eyda Aps',
			'address_1'		=> 'Lene Haus Vej 10A',
			'zip'			=> '7430',
			'city'			=> 'Ikast',
			'country_code' 	=> 'DK'
		);
	}
	else if(strrpos(tablegetfield('`order`','Comment', $PassOnOrderId), 'reklamation') !== false){
		$data['data']['attributes']['delivery_address'] = array(
			'company_name' 	=> 'Eyda Aps',
			'address_1'		=> 'Lene Haus Vej 10A',
			'zip'			=> '7430',
			'city'			=> 'Ikast',
			'country_code' 	=> 'DK'
		);
	}
	

	$url = 'https://eyda-aps.api.webshipper.io/v2/shipments?include=labels';
	$ch = curl_init($url);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
	curl_setopt($ch, CURLOPT_HTTPHEADER, array('Authorization: Bearer 5f1ec30a4aee3be406c9160070bd5929667d19b7f5843fdc5cac67b1239b9a76', 'Content-Type: application/vnd.api+json'));
	curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data));
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
	$result = curl_exec($ch); 
	$ch = null;
	$resultObj = json_decode($result, true);

	if(isset($resultObj['included']) === true){
		foreach ($resultObj['included'] as $key => $label) {
			if($label['type'] == 'labels'){
				$shipmentId = $_stockId;
				$webshipperLabelId = (int)tableGetFieldWhere('webshipperlabel', 'Id',  sprintf("Active=1 AND StockId=%d AND LabelId=%d", $shipmentId, $label['id']));
				$post = array(
					'StockId' => $shipmentId,
					'LabelId' => $label['id'],
					'LabelUrl' => $label['links']['self'],
					'Type' => 'Return' //FIX in enum in DB `webshipperlabel`
				);
				tableWrite('webshipperlabel', $post, $webshipperLabelId);
			}
		}		
	}
	else{
		return 'An error occured when creating shipment in webshipper:' . json_encode($result);
	}

	return true;
}